<%
	' change to address of your own SMTP server
	strHost = "smtp.broadviewnet.net"

	If Request("Send") <> "" Then
		
		Set Mail = Server.CreateObject("Persits.MailSender")
		' enter valid SMTP host
		Mail.Host = strHost

		Mail.From = "info@persits.com"
		Mail.FromName = "Persits Software, Inc."
		Mail.AddAddress Request("To")

		' Attach two files in the same directory (any file type can be attached)
		strPath = Server.MapPath(".")
		Mail.AddAttachment strPath & "\ps_logo.gif"
		Mail.AddAttachment strPath & "\wehave.gif"
		
		' message subject
		Mail.Subject = "Logo & Motto"
		' message body
		Mail.Body = "Persits Software logo and motto images are attached."

		Mail.Send	' send message

		Response.Write "Success!<P>"
	End If
%>


<HTML>
<HEAD>
<TITLE>AspEmail: Attachments.asp</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: Attachments.asp</h2>
<h3>Demonstrates sending a message with attachments</h3>

<FORM ACTION="Attachments.asp">
Enter email: <INPUT TYPE="TEXT" NAME="To">
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send">
</FORM>


</BODY>
</HTML>
