<%
	' change to address of your own SMTP server
	strHost = "smtp.broadviewnet.net"

	If Request("Send") <> "" Then

		' Connect to database
		strDbPath = Server.MapPath(".") & "\images.mdb"
		ConnectStr = "DRIVER={Microsoft Access Driver (*.mdb)};DBQ=" & strDbPath

		Set rs = Server.CreateObject("adodb.recordset")
		rs.Open "images", ConnectStr, 2, 3

		If rs.EOF Then
			Response.Write "Recordset is empty."
			Response.End
		End If

		' Create instance of AspEmail
		Set Mail = Server.CreateObject("Persits.MailSender")
		' enter valid SMTP host
		Mail.Host = strHost

		Mail.From = "info@persits.com"
		Mail.FromName = "Persits Software, Inc."
		Mail.AddAddress Request("To")

		' Attach files stored in database as blobs
		While Not rs.EOF
			Mail.AddAttachmentMem rs("name").Value, rs("blob").Value
			rs.MoveNext
		Wend
		
		' message subject
		Mail.Subject = "Memory Attachments"
		' message body
		Mail.Body = "To buy these paintings, visit www.oiloncanvas.com."

		Mail.Send	' send message

		Response.Write "Success!<P>"
	End If
%>


<HTML>
<HEAD>
<TITLE>AspEmail: AttachBlobs.asp</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: AttachBlobs.asp</h2>
<h3>Attaching Files Stored in the Database as Blobs</h3>

<FORM ACTION="AttachBlobs.asp">
Enter email: <INPUT TYPE="TEXT" NAME="To">
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send">
</FORM>


</BODY>
</HTML>