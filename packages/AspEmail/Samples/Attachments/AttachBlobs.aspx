<!-- AspEmail .NET Code samples: AttachBlobs.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->


<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="System.Reflection" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Data.OleDb" %>

<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";	
	
	txtHost.InnerHtml = strHost;	
	
	if( IsPostBack )
	{
		// Connect to database
		String strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("images.mdb");
		OleDbConnection myConnection = new OleDbConnection(strConn);
		myConnection.Open();
		
		OleDbCommand myCommand = new OleDbCommand("select name, blob from images", myConnection);    
		OleDbDataReader myReader = myCommand.ExecuteReader();


		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;

		objMail.From = "info@persits.com";				// From Address
		objMail.FromName = "Persits Software, Inc.";	// From Name

		// To address, 2nd argument optional
		objMail.AddAddress(txtTo.Value, Missing.Value);
		
		// message subject
		objMail.Subject = "Memory Attachments";

		// Attach files stored in database as blobs
		while( myReader.Read() )
		{
			objMail.AddAttachmentMem(myReader["name"], myReader["blob"]);
		}
		
		// message body
		objMail.Body = "To buy these paintings, visit www.oiloncanvas.com.";

		try
		{
			objMail.Send(Missing.Value);
			txtMsg.InnerHtml = "<font color=green>Success! Message sent.</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
}

</script>

<HTML>
<HEAD>
<TITLE>AspEmail: AttachBlobs.aspx</TITLE>
</HEAD>
<BODY>

<H2>AspEmail: AttachBlobs.asp</h2>
<h3>Attaching Files Stored in the Database as Blobs</h3>

<div id="txtMsg" runat="server"/><P>

<FORM ACTION="HtmlFormat.asp" RUNAT="Server">
Host: <B><span id="txtHost" runat="server"/></B> (Change to your own SMTP host in script)<P>
Enter email: <INPUT TYPE="TEXT" ID="txtTo" RUNAT="Server">
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send" RUNAT="Server">
</FORM>

</BODY>
</HTML>