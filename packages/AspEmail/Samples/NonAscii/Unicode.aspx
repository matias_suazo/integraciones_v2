<!-- AspEmail .NET Code samples: Unicode.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->

<%@ Page CodePage="65001" %>

<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="System.Reflection" %>

<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";
	
	txtHost.InnerHtml = strHost;

	Session.CodePage = 65001;
	
	if( IsPostBack )
	{
		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;
		
		objMail.From = "info@persits.com";				// From Address
		objMail.FromName = objMail.EncodeHeader(txtFromName.Value, "utf-8"); 

		// To address, 2nd argument optional
		objMail.AddAddress(txtTo.Value, Missing.Value);
		
		// message subject
		objMail.Subject = objMail.EncodeHeader(txtSubject.Value, "utf-8");
		
		// message body
		objMail.Body = txtBody.Value;

		// Charset and encoding
		objMail.CharSet = "utf-8";
		objMail.ContentTransferEncoding = "Quoted-Pintable";


		try
		{
			objMail.Send( Missing.Value );
			txtMsg.InnerHtml = "<font color=green>Success! Message sent.</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
}

</script>

<HTML>
<HEAD>
<!-- Important tag, specifies charset to be UTF-8-->
<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">

<TITLE>AspEmail: Unicode.aspx</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: Unicode.aspx</h2>

<P>

<div id="txtMsg" runat="server"/><P>


<FORM RUNAT="Server">
<TABLE CELLSPACING=0 CELLPADDING=0>
<TR><TD>Host:</TD><TD><B><span id="txtHost" runat="server"/></B> (Change to your own SMTP host in script)</TD></TR>
<TR><TD>Enter email:</TD><TD><INPUT RUNAT="Server" ID="txtTo"/></TD></TR>
<TR><TD>Enter your name:</TD><TD><INPUT RUNAT="Server" ID="txtFromName"/></TD></TR>
<TR><TD>Enter Subject:</TD><TD><INPUT RUNAT="Server" ID="txtSubject"/></TD></TR>
<TR><TD>Enter Body:</TD><TD><TEXTAREA RUNAT="Server" cols="50" rows="10" ID="txtBody"/></TD></TR>
<TR><TD COLSPAN=2><INPUT RUNAT="Server" TYPE=SUBMIT ID="Send" VALUE="Send"/></TD></TR>
</TABLE>
</FORM>


</BODY>
</HTML>