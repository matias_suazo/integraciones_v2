<%
	' change to address of your own SMTP server
	strHost = "smtp.broadviewnet.net"

	' Enable UTF-8 -> Unicode translation for form items
	Session.CodePage = 65001 ' UTF-8 code

	If Request("Send") <> "" Then
		
		Set Mail = Server.CreateObject("Persits.MailSender")
		' enter valid SMTP host
		Mail.Host = strHost

		Mail.From = "info@aspemail.com" ' From address
		Mail.FromName = Mail.EncodeHeader(Request("FromName"), "utf-8")
		Mail.AddAddress Request("To")
		
		' message subject
		Mail.Subject = Mail.EncodeHeader( Request("Subject"), "utf-8")
		
		' message body
		Mail.Body = Request("Body")

		' UTF-8 parameters
		Mail.CharSet = "UTF-8"
		Mail.ContentTransferEncoding = "Quoted-Printable"

		Mail.Send	' send message

		Response.Write "Message sent to " & Request("To")
	End If
%>

<HTML>
<HEAD>
<!-- Important tag, specifies charset to be UTF-8-->
<META HTTP-EQUIV="Content-Type" content="text/html; charset=utf-8">
<TITLE>AspEmail: Unicode.asp</TITLE>
</HEAD>

<BODY>

<H2>AspEmail: Unicode.asp</h2>


<FORM METHOD="POST" ACTION="Unicode.asp">
<TABLE CELLSPACING=0 CELLPADDING=0>
<TR><TD>Enter email:</TD><TD><INPUT TYPE="TEXT" NAME="To"></TD></TR>
<TR><TD>Enter your name:</TD><TD><INPUT TYPE="TEXT" NAME="FromName"></TD></TR>
<TR><TD>Enter Subject:</TD><TD><INPUT TYPE="TEXT" NAME="Subject"></TD></TR>
<TR><TD>Enter Body:</TD><TD><TEXTAREA cols="50" rows="10" NAME="Body"></TEXTAREA></TD></TR>
<TR><TD COLSPAN=2><INPUT TYPE=SUBMIT NAME="Send" VALUE="Send"></TD></TR>
</TABLE>
</FORM>

</BODY>
</HTML>
