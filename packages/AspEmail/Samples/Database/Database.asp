<%
	' change to address of your own SMTP server
	strHost = "smtp.broadviewnet.net"
%>

<HEAD>
<TITLE>AspEmail: Database.asp</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: Database.asp</h2>
<h3>Demonstrates sending a message to multiple recipients<BR>whose names and addresses
are stored in a database.</h3>

<%
	' connect to the MS Access database in the same directory
	strDbPath = Server.MapPath(".") & "\users.mdb"
	ConnectStr = "DRIVER={Microsoft Access Driver (*.mdb)};DBQ=" & strDbPath

	Set rs = Server.CreateObject("adodb.recordset")
	rs.Open "users", ConnectStr, 2, 3

	If Request("Send") <> "" Then
		' send email to all users
		Set Mail = Server.CreateObject("Persits.MailSender")
		Mail.Host = strHost
		
		Mail.From = "info@aspemail.com"
		Mail.FromName = "AspEmail Demo"
		Mail.Subject = "Thank-you for your business"
		Mail.Body = "Dear Sir/Madam, you have been a great customer, please come again."

		' read address from DB and put them in the BCC field
		While not rs.EOF
			Mail.AddBcc rs("email"), rs("name")
			rs.MoveNext
		Wend

		If Request("Queue") <> "" Then
			Mail.Queue = True
		End if

		' finally: send message
		Mail.Send

		Response.Write "Success!"
	Else
		' simply display the list of users in the database
		Response.Write "<B>Currently in the user database:</B><P>"
		While not rs.EOF
			Response.Write rs("name") & " (" & rs("email") & ")<BR>"
			rs.MoveNext
		Wend
	End If
%>

<FORM ACTION="Database.asp">

Send to Queue: <input type="checkbox" name="Queue" checked><BR>

<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send a message to them all">
</FORM>

</BODY>
</HTML>