<!-- AspEmail .NET Code samples: Database.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->

<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="System.Reflection" %>
<%@ import Namespace="System.Data.OleDb" %>

<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";	
	
	txtHost.InnerHtml = strHost;

	// Connect to the MS Access database in the same directory
	String strDbPath = Server.MapPath(".") + "\\users.mdb";
	String strConnect = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDbPath;

    OleDbConnection myConnection = new OleDbConnection(strConnect);
    myConnection.Open();
    
    OleDbCommand myCommand = new OleDbCommand("select * from users", myConnection);
    OleDbDataReader myReader = myCommand.ExecuteReader();
	
	if( IsPostBack )
	{
		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;

		objMail.From = "info@aspemail.com";				// From Address
		objMail.FromName = "AspEmail Demo";	// From Name

		// Send message to all addressed in database
		while( myReader.Read() )
		{		
			objMail.AddBcc( (String)myReader["email"], Missing.Value );
		}
		
		// message subject
		objMail.Subject = "Thank-you for your business";

		// message body
		objMail.Body = "Dear Sir/Madam, you have been a great customer, please come again.";

		if( Queue.Checked )
			objMail.Queue = 1;
		
		try
		{
			objMail.Send( Missing.Value );
			txtMsg.InnerHtml = "<font color=green>Success! Message sent.</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
	else
	{
		// simply display all emails in the database
		lstEmails.DataSource = myReader;
		lstEmails.DataMember = "emasil";
		lstEmails.DataBind();
	}
}

</script>

<HTML>
<HEAD>
<TITLE>AspEmail: Database.aspx</TITLE>
</HEAD>
<BODY>
<H2>AspEmail: Database.asp</h2>

<div id="txtMsg" runat="server"/><P>

<B>Database records:</B><BR>
<ASP:DataGrid ID="lstEmails" RUNAT="Server"/>

<FORM ACTION="HtmlFormat.asp" RUNAT="Server">
Host: <B><span id="txtHost" runat="server"/></B> (Change to your own SMTP host in script)<P>
Send to Queue: <input type="checkbox" id="Queue" checked runat="server"><BR>
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send message to them all" RUNAT="Server">
</FORM>

</BODY>
</HTML>