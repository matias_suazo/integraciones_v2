<!-- AspEmail .NET Code samples: HtmlFormat.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->


<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="System.Reflection" %>

<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";	
	
	txtHost.InnerHtml = strHost;	
	
	if( IsPostBack )
	{
		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;

		// Body in HTML format
		String strHTML;

		strHTML  = "<HTML><BODY><CENTER>\r\n";
		strHTML += "<A HREF=\"http://www.persits.com\"><IMG SRC=\"http://www.persits.com/images/logo.gif\" BORDER=\"0\"></A><BR>\r\n";
		strHTML += "<TABLE WIDTH=\"300\" BGCOLOR=\"#E0E0E0\" BORDER=1>\r\n";
		strHTML += "<CAPTION>Persits Software Price List</CAPTION>\r\n";
		strHTML += "<TR><TH>Product</TH><TH>Price</TH></TR>\r\n";
		strHTML += "<TR><TD><A HREF=\"http://www.aspemail.com\">AspEmail</A></TD><TD>Free</TD></TR>\r\n";
		strHTML += "<TR><TD><A HREF=\"http://www.aspupload.com\">AspUpload</A></TD><TD>$149.00</TD></TR>\r\n";
		strHTML += "<TR><TD><A HREF=\"http://www.aspuser.com\">AspUser</A></TD><TD>$120.00</TD></TR>\r\n";
		strHTML += "<TR><TD><A HREF=\"http://www.aspencrypt.com\">AspEncrypt</A></TD><TD>$249.00</TD></TR>\r\n";
		strHTML += "<TR><TD><A HREF=\"http://www.aspgrid.com\">AspGrid</A></TD><TD>$199.00</TD></TR>\r\n";
		strHTML += "<TR><TD><A HREF=\"http://www.aspupload.com/aspjpeg.html\">AspJpeg</A></TD><TD>$150.00</TD></TR>\r\n";
		strHTML += "</TABLE></CENTER></BODY></HTML>";


		objMail.From = "info@aspemail.com";				// From Address
		objMail.FromName = "Persits Software Sales";	// From Name

		// To address, 2nd argument optional
		objMail.AddAddress(txtTo.Value, Missing.Value);
		
		// message subject
		objMail.Subject = "Price List";

		// HTML format
		objMail.IsHTML = 1; // true
		
		// message body
		objMail.Body = strHTML;

		try
		{
			objMail.Send(Missing.Value);
			txtMsg.InnerHtml = "<font color=green>Success! Message sent.</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
}

</script>

<HTML>
<HEAD>
<TITLE>AspEmail: HtmlFormat.aspx</TITLE>
</HEAD>
<BODY>
<H2>AspEmail: HtmlFormat.asp</h2>

<h3>Sending messages in the HTML format</h3>

<div id="txtMsg" runat="server"/><P>

<FORM ACTION="HtmlFormat.asp" RUNAT="Server">
Host: <B><span id="txtHost" runat="server"/></B> (Change to your own SMTP host in script)<P>
Enter email: <INPUT TYPE="TEXT" ID="txtTo" RUNAT="Server">
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send" RUNAT="Server">
</FORM>

</BODY>
</HTML>