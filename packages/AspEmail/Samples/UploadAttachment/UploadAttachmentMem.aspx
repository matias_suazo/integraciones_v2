<!-- AspEmail .NET Code samples: UploadAttachmentMem.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->

<!-- We have to use the old ASP compatibility mode because we use aspupload -->
<%@ Page aspCompat="True" %>

<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="ASPUPLOADLib" %>
<%@ Import Namespace="System.Reflection" %>

<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";	
	
	txtHost.InnerHtml = strHost;	
	
	if( IsPostBack )
	{
		// We are using AspUpload object to capture an attachment (if one is selected)
		ASPUPLOADLib.IUploadManager objUpload;

		objUpload = new ASPUPLOADLib.UploadManager();
		
		// Perform upload to memory. Save method returns number of uploaded files
		int nCount = objUpload.Save(Missing.Value, Missing.Value, Missing.Value);

		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;

		objMail.From = "info@persits.com";				// From Address
		objMail.FromName = "Persits Software, Inc.";	// From Name

		// To address, 2nd argument optional
		objMail.AddAddress(txtTo.Value, Missing.Value);
		
		// message subject
		objMail.Subject = txtSubject.Value;
		
		// message body
		objMail.Body = txtBody.Value;

		// attach uploaded file from memory, if there is one
		if( nCount > 0 )
		{
			objMail.AddAttachmentMem( 
				objUpload.Files.Item("Attachment").FileName, 
				objUpload.Files.Item("Attachment").Binary );
		}

		try
		{
			objMail.Send( Missing.Value );
			txtMsg.InnerHtml = "<font color=green>Success! Message sent.</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
}

</script>

<HTML>
<HEAD>
<TITLE>AspEmail: UploadAttachmentMem.aspx</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: UploadAttachmentMem.aspx</h2>
<H3>This sample uses the <A HREF="http://www.aspupload.com">AspUpload</A> component.</H3>
An evaluation copy of aspupload.dll can be found in the \Bin directory of the AspEmail installation.
For this demo to work, you must register aspupload.dll using regsvr32 unless it has already
been registered on your system.

<P>

<div id="txtMsg" runat="server"/><P>


<!-- Note special ENCTYPE attribute: it is necessary to upload a file-->

<FORM METHOD="POST" ACTION="UploadAttachmentMem.asp" ENCTYPE="multipart/form-data" RUNAT="Server">

<TABLE CELLSPACING=0 CELLPADDING=0>
<TR><TD>Host:</TD><TD><B><span id="txtHost" runat="server"/></B> (Change to your own SMTP host in script)</TD></TR>
<TR><TD>Enter email:</TD><TD><INPUT TYPE="TEXT" ID="txtTo" RUNAT="Server"></TD></TR>
<TR><TD>Enter Subject:</TD><TD><INPUT TYPE="TEXT" ID="txtSubject" RUNAT="Server"></TD></TR>
<TR><TD>Enter Body:</TD><TD><TEXTAREA ID="txtBody" RUNAT="Server"></TEXTAREA></TD></TR>
<TR><TD>Select File Attachment:</TD><TD><INPUT TYPE="FILE" ID="Attachment" RUNAT="Server"></TD></TR>
<TR><TD COLSPAN=2><INPUT TYPE=SUBMIT NAME="Send" VALUE="Send" RUNAT="Server"></TD></TR>
</TABLE>
</FORM>


</BODY>
</HTML>