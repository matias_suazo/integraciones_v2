<%
	' change to address of your own SMTP server
	strHost = "smtp.broadviewnet.net"

	' We use AspUpload component to capture uploaded file and access other form items.
	' Because of the special ENCTYPE attribute we can no longer use Request.Form,
	' we must use Upload.Form instead.
	' More more info on AspUpload, visit www.aspupload.com.

	Set Upload = Server.CreateObject("Persits.Upload")
	Upload.IgnoreNoPost = True

	' Perform upload to memory
	Upload.Save

	' Note that we cannot use Upload.Form or Upload.Files until Upload.Save is called.
	If Upload.Form("Send") <> "" Then

		Set Mail = Server.CreateObject("Persits.MailSender")
		Mail.From = "info@persits.com"
		Mail.FromName = "Memory Attachment Demo"
		Mail.Host = strHost
		Mail.Subject = Upload.Form("Subject")
		Mail.Body = Upload.Form("Body")
		Mail.AddAddress Upload.Form("To")

		' Handle attached file via Upload.Files collection.
		' Check if a file was ineed uploaded
		Set File = Upload.Files("Attachment")
		If Not File Is Nothing Then
			Mail.AddAttachmentMem File.FileName, File.Binary
		End If

		' We are done. Send message
		Mail.Send

		Response.Write "Success!"
	End If

%>


<HTML>
<HEAD>
<TITLE>AspEmail: UploadAttachmentMem.asp</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: UploadAttachment.asp</h2>
<H3>This sample uses the <A HREF="http://www.aspupload.com">AspUpload</A> component.</H3>
An evaluation copy of aspupload.dll can be found in the \Bin directory of the AspEmail installation.
For this demo to work, you must register aspupload.dll using regsvr32 unless it has already
been registered on your system.
<!-- Note special ENCTYPE attribute: it is necessary to upload a file-->
<FORM METHOD="POST" ACTION="UploadAttachmentMem.asp" ENCTYPE="multipart/form-data">
<TABLE CELLSPACING=0 CELLPADDING=0>
<TR><TD>Enter email:</TD><TD><INPUT TYPE="TEXT" NAME="To"></TD></TR>
<TR><TD>Enter Subject:</TD><TD><INPUT TYPE="TEXT" NAME="Subject"></TD></TR>
<TR><TD>Enter Body:</TD><TD><TEXTAREA NAME="Body"></TEXTAREA></TD></TR>
<TR><TD>Select File Attachment:</TD><TD><INPUT TYPE=FILE NAME="Attachment"></TD></TR>
<TR><TD COLSPAN=2><INPUT TYPE=SUBMIT NAME="Send" VALUE="Send"></TD></TR>
</TABLE>
</FORM>


</BODY>
</HTML>