<!-- AspEmail .NET Code samples: Simple.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->


<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="System.Reflection" %>


<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";	
	
	txtHost.InnerHtml = strHost;	
	
	if( IsPostBack )
	{
		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;

		objMail.From = txtFrom.Value;		// From address
		objMail.FromName = txtFromName.Value;	// optional

		// To address, 2nd argument optional
		objMail.AddAddress(txtTo.Value, Missing.Value);		
		
		// message subject
		objMail.Subject = txtSubject.Value;
		
		// message body
		objMail.Body = txtBody.Value;

		try
		{
			objMail.Send(Missing.Value);
			txtMsg.InnerHtml = "<font color=green>Success! Message sent to " + txtTo.Value + ".</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
}

</script>

<HTML>
<HEAD>
<TITLE>AspEmail: Simple.aspx</TITLE>
</HEAD>
<BODY>

<div id="txtMsg" runat="server"/><BR>

<FORM METHOD="POST" RUNAT="Server">

<TABLE CELLSPACING=0 CELLPADDING=2 BGCOLOR="#E0E0E0">
	<TR>
		<TD>Host (change as necessary in script):</TD>
		<TD><B><div id="txtHost" runat="server"/></B></TD>
	</TR>
	<TR>
		<TD>From (enter sender's address):</TD>
		<TD><INPUT TYPE="TEXT" ID="txtFrom" RUNAT="Server"></TD>
	</TR>
	<TR>
		<TD>FromName (optional, enter sender's name):</TD>
		<TD><INPUT TYPE="TEXT" ID="txtFromName" RUNAT="Server"></TD>
	</TR>
	<TR>
		<TD>To: (enter one recipient's address):</TD>
		<TD><INPUT TYPE="TEXT" ID="txtTo" RUNAT="Server"></TD>
	</TR>
	<TR>
		<TD>Subject:</TD>
		<TD><INPUT TYPE="TEXT" ID="txtSubject" RUNAT="Server"></TD>
	</TR>
	<TR>
		<TD>Body:</TD>
		<TD><TEXTAREA ID="txtBody" RUNAT="Server"></TEXTAREA></TD>
	</TR>
	<TR>
		<TD COLSPAN=2><INPUT TYPE="SUBMIT" NAME="Send" VALUE="Send Message" RUNAT="Server"></TD>
	</TR>

</TABLE>

</FORM>

</BODY>
</HTML>
