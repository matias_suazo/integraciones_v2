<%
	' change to address of your own SMTP server
	strHost = "smtp.broadviewnet.net"

	If Request("Send") <> "" Then
		
		Set Mail = Server.CreateObject("Persits.MailSender")
		' enter valid SMTP host
		Mail.Host = strHost

		Mail.From = "sales@persits.com"
		Mail.FromName = "Persits Software Sales"
		Mail.AddAddress Request("To")
		
		' message subject
		Mail.Subject = "Thank-you for your business"
		' message body

		' append body from file
		strBodyPath = Server.MapPath(".") & "\SampleBody.htm"
		Mail.AppendBodyFromFile strBodyPath
		
		' Add embedded image for background
		strImagePath = Server.MapPath(".") & "\margin.gif"
		Mail.AddEmbeddedImage strImagePath, "My-Background-Image"

		' Add embedded image for logo
		strImagePath = Server.MapPath(".") & "\ps_logo.gif"
		Mail.AddEmbeddedImage strImagePath, "Persits-Software-Logo"

		Mail.Send	' send message

		Response.Write "Success!<P>"
	End If
%>
<HTML>
<HEAD>
<TITLE>AspEmail: EmbeddedImages.asp</TITLE>
</HEAD>
<BODY BGCOLOR="#FFFFFF">
<H2>AspEmail: EmbeddedImages.asp</h2>
<h3>Demonstrates using embedded images in messages</h3>

<FORM ACTION="EmbeddedImages.asp">
Enter email: <INPUT TYPE="TEXT" NAME="To">
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send">
</FORM>

</BODY>
</HTML>
