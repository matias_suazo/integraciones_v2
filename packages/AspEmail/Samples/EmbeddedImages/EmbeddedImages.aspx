<!-- AspEmail .NET Code samples: EmbeddedImages.aspx -->
<!-- Copyright (c) 2002 Persits Software, Inc. -->
<!-- http://www.persits.com -->


<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="ASPEMAILLib" %>
<%@ Import Namespace="System.Reflection" %>

<script runat="server" LANGUAGE="C#">

void Page_Load(Object Source, EventArgs E)
{
	// Change this to your own SMTP server
	String strHost = "smtp.broadviewnet.net";	
	
	txtHost.InnerHtml = strHost;	
	
	if( IsPostBack )
	{
		// MailSender object declaration
		ASPEMAILLib.IMailSender objMail;
		objMail = new ASPEMAILLib.MailSender();

		objMail.Host = strHost;

		objMail.From = "info@aspemail.com";				// From Address
		objMail.FromName = "Persits Software Sales";	// From Name

		// To address, 2nd argument optional
		objMail.AddAddress(txtTo.Value, Missing.Value);
		
		// message subject
		objMail.Subject = "Thank-you for your business";

		// Append body from file
		String strBodyPath = Server.MapPath(".") + "\\SampleBody.htm";
		objMail.AppendBodyFromFile(strBodyPath, true);

		// Add embedded image for logo
		String strImagePath = Server.MapPath(".") + "\\ps_logo.gif";
		objMail.AddEmbeddedImage(strImagePath, "Persits-Software-Logo");

		// Add embedded image for background
		strImagePath = Server.MapPath(".") + "\\margin.gif";
		objMail.AddEmbeddedImage(strImagePath, "My-Background-Image");

		// HTML format
		objMail.IsHTML = 1; // true

		try
		{
			objMail.Send( Missing.Value );
			txtMsg.InnerHtml = "<font color=green>Success! Message sent.</font>";
		}
		catch(Exception e)
		{
			txtMsg.InnerHtml = "<font color=red>Error occurred: " + e.Message + "</font>";
		}
	}
}

</script>

<HTML>
<HEAD>
<TITLE>AspEmail: EmbeddedImages.aspx</TITLE>
</HEAD>
<BODY>
<H2>AspEmail: EmbeddedImages.asp</h2>
<h3>Demonstrates using embedded images in messages</h3>

<div id="txtMsg" runat="server"/><P>

<FORM ACTION="HtmlFormat.asp" RUNAT="Server">
Host: <B><span id="txtHost" runat="server"/></B> (Change to your own SMTP host in script)<P>
Enter email: <INPUT TYPE="TEXT" ID="txtTo" RUNAT="Server">
<INPUT TYPE=SUBMIT NAME="Send" VALUE="Send" RUNAT="Server">
</FORM>

</BODY>
</HTML>