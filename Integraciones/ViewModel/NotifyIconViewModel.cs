﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Integraciones.Main.Email;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.View;

namespace Integraciones.ViewModel
{
    /// <summary>
    /// Clase que Interactua con el TaskbarIcon.DataContext de Model/GeneradorNotifyIcon
    /// </summary>
    public class NotifyIconViewModel
    {
        private static bool _canShowConfigurations = true;
        private static bool _canIntegrarOrdenes = true;
        private static bool _canIntegrarOrdenesExcel = true;
        private static bool _canIntegrarOrdenesEmail = true;
        private static bool _canIntegrarOrdenesEmailIconstruye = true;
        private static bool _canShowDebug = true;
        private static bool _canShowAbout = true;
        private static Thread _hiloPdf;
        private static Thread _hiloEmail;

        public static void SetCanDebugCommand()
        {
            _canShowDebug = true;
        }

        public static void SetCanShowConfigurationsCommand()
        {
            _canShowConfigurations = true;
        }
        public static void SetCanShowAboutCommand()
        {
            _canShowAbout = true;
        }


        public static void SetCanProcessOrderCommand()
        {
            //IntegracionesView.Instance.PDF_STATE = IntegracionesView.AppState.INACTIVO;
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
            _canIntegrarOrdenes = true;
        }
        public static void SetCanProcessOrderExcelCommand()
        {
            //IntegracionesView.Instance.EXCEL_STATE = IntegracionesView.AppState.INACTIVO;
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
            _canIntegrarOrdenesExcel = true;
        }
        public static void SetCanProcessOrderMailCommand()
        {
            //IntegracionesView.Instance.MAIL_STATE = IntegracionesView.AppState.INACTIVO;
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
            _canIntegrarOrdenesEmail = true;
        }
        public static void SetCanProcessOrderMailIconstruyeCommand()
        {
            //IntegracionesView.Instance.MAIL_ICONSTRUYE_STATE = IntegracionesView.AppState.INACTIVO;
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
            _canIntegrarOrdenesEmailIconstruye = true;
        }
        public static void SetCantProcessOrderCommand()
        {
            //IntegracionesView.Instance.PDF_STATE = IntegracionesView.AppState.ANALIZANDO_ORDENES_PDF;
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_PDF;
            _canIntegrarOrdenes = false;
        }
        public static void SetCantProcessOrderExcelCommand()
        {
            //IntegracionesView.Instance.EXCEL_STATE = IntegracionesView.AppState.ANALIZANDO_ORDENES_EXCEL;
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_EXCEL;
            _canIntegrarOrdenesExcel = false;
        }

        public static void SetCantProcessOrderMailCommand()
        {
            //IntegracionesView.Instance.MAIL_STATE = IntegracionesView.AppState.ANALIZANDO_ORDENES_MAIL;
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_MAIL;
            _canIntegrarOrdenesEmail = false;
        }


        public static void SetCantProcessOrderMailIconstruyeCommand()
        {
            //IntegracionesView.Instance.MAIL_ICONSTRUYE_STATE = IntegracionesView.AppState.ANALIZANDO_ORDENES_MAIL;
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_MAIL;
            _canIntegrarOrdenesEmailIconstruye = false;
        }

        public static ICommand IntegrarOrdenesCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canIntegrarOrdenes,
            CommandAction = () =>
            {
                _canIntegrarOrdenes = false;
                _hiloPdf = new Thread(Main.Main.ExecuteLecturaPdf);
                _hiloPdf.Start();
            }
        };

        public static ICommand IntegrarOrdenesExcelCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canIntegrarOrdenesExcel,
            CommandAction = () =>
            {
                _canIntegrarOrdenesExcel = false;
                _hiloPdf = new Thread(Main.Main.ExecuteLecturaExcel);
                _hiloPdf.Start();
            }
        };

        public static ICommand IntegrarOrdenesEmailCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canIntegrarOrdenesEmail,
            CommandAction = () =>
            {
                _canIntegrarOrdenesEmail = false;
                _hiloEmail = new Thread(Main.Main.ExecuteLecturaMail);
                _hiloEmail.Start();
            }
        };

        public static ICommand IntegrarOrdenesEmailIconstruyeCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canIntegrarOrdenesEmailIconstruye,
            CommandAction = () =>
            {
                _canIntegrarOrdenesEmailIconstruye = false;
                _hiloEmail = new Thread(Main.Main.ExecuteLecturaMailIconstruye);
                _hiloEmail.Start();
            }
        };

        public static ICommand ShowConfigurationCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canShowConfigurations,
            CommandAction = () =>
            {
                new Configuraciones().Show();
                _canShowConfigurations = false;

            }
        };

        public ICommand DebugCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canShowDebug,
            CommandAction = () =>
            {
                new DebugWindow().Show();
                _canShowDebug = false;
            }
        };
        public ICommand ShowLogCommand { get; } = new DelegateCommand { CommandAction = () => Process.Start(InternalVariables.GetLogFolder()) };
        public ICommand ExitApplicationCommand { get; } = new DelegateCommand { CommandAction = () => Application.Current.Shutdown() };

        public static ICommand ShowOrdenesProcesadasCommand { get; } = new DelegateCommand { CommandAction = () => Process.Start(InternalVariables.GetOcProcesadasFolder()) };
        
        public static ICommand UpdateTelemarketingCommand { get; } = new DelegateCommand { CommandAction = () => OracleDataAccess.GetNull() };
        public static ICommand ShowOrdenesAProcesarCommand { get; } = new DelegateCommand { CommandAction = () => Process.Start(InternalVariables.GetOcAProcesarFolder()) };

        public ICommand AboutCommand { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => _canShowAbout,
            CommandAction = () =>
            {
                Console.WriteLine("IMPLEMENTAR ABOUT");
                _canShowAbout = false;
            }
        };
    }


    public class DelegateCommand : ICommand
    {
        public Action CommandAction { get; set; }
        public Func<bool> CanExecuteFunc { get; set; }

        public void Execute(object parameter)
        {
            CommandAction();
        }

        public bool CanExecute(object parameter)
            {
            return CanExecuteFunc == null || CanExecuteFunc();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
