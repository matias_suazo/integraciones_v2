﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.Readers.Xml;
using Limilabs.Client.IMAP;
using Limilabs.Client.POP3;
using Limilabs.Mail;
using Limilabs.Mail.Headers;
using InternalVariablesPDF = Integraciones.Utils.InternalVariables.InternalVariables;


namespace Integraciones.Utils.Readers.Email
{
    public static class EmailReader
    {
        public static Dictionary<long, IMail> GetAllMailLecturaMailSubject()
        {
            string path = "C:/Debug_Registro_Correos_Entrantes.txt";
            string texto = "Entro a GetAllMailLecturaMailSubject()";
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
            {
                writer.WriteLine(texto);
            }

            

            using (var imap = new Imap())
            {
                imap.Connect(InternalVariablesPDF.GetServidorCorreoLectura());
                imap.Login(InternalVariablesPDF.GetUsuarioServidorCorreoLectura()
                    , InternalVariablesPDF.GetPassServidorCorreoLectura());
                imap.SelectInbox();
                var uidList = imap.GetAll();
                var mails = new Dictionary<long, IMail>();
                var formato = "";
                foreach (var uid in uidList)
                {


                    var email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                    var zxc = email.From.ToString();


                    var arr = zxc.Split(' ');
                    var address = arr[arr.Length - 1];
                    address = address.Replace("Address=", "");
                    address = address.Replace("'", "").Trim();
                    Console.WriteLine("address" + address);
                    texto = $"Empiezo a Recorrer los mail {address}";
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
                    {
                        writer.WriteLine(texto);
                    }


                    //Console.WriteLine(zxc);
                    var attstring = "";
                    if (!address.Contains("procesosxml@dimerc.cl"))
                    {
                        if (address.Contains("ebswf@vtr.cl"))
                        {
                            email.Subject = "LECTURA MAIL";
                        }
                        foreach (var att in email.Attachments)
                        {
                            attstring += att.FileName + ",";
                        }
                        texto = $"Obtengo los adjuntos {attstring} tamaño: {attstring.Length}";
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
                        {
                            writer.WriteLine(texto);
                        }
                        OracleDataAccess.InsertCorreoRecibido(email.Subject, attstring, address, "");
                    }

                    // var formatoarr = email.Attachments.ToString().Split('.');
                    //formato = formatoarr[formatoarr.Length - 1].Trim();
                    // OracleDataAccess.InsertCorreoRecibido(email.Subject,email.Attachments.ToString(),email.To.ToString(),formato);
                    var frommail = email.From.ToString();//apardo
                    if (frommail.Contains("@vtr.cl") || email.Subject.Contains("74010") || email.Text.Contains("76350666")
                        || email.Text.Contains("76114143"))
                    {
                        if (email.Text.Contains("76114143"))
                        {
                            email.Subject = "LECTURA MAIL";
                        }
                            email.Subject = InternalVariablesPDF.GetSubjectMail();
                    }
                    Console.WriteLine($"Subject: {email.Subject} \n");
                    Console.WriteLine($"From: {frommail}");
                    Console.WriteLine($"attachments:{email.Attachments}");


                    if (!email.Subject.Equals(@"Please purchase Mail.dll license at https://www.limilabs.com/mail")
                        && !email.Subject.ToUpper().Equals(InternalVariablesPDF.GetSubjectMail())
                        && !email.Subject.ToUpper().Equals(InternalVariablesPDF.GetSubjectMailDebug())
                        )
                    {
                        email.MoveProcesosMailTo(uid, InternalVariablesPDF.GetCarpetaSinLectura(), removeTrashmail: true);
                        continue;
                    }
                    if (email.Subject.ToUpper().Equals(InternalVariablesPDF.GetSubjectMail()) || (email.Subject.Contains("74010") || email.Text.Contains("76350666") || frommail.Contains("@vtr.cl"))
                      || (email.Subject.ToUpper().Equals(InternalVariablesPDF.GetSubjectMailDebug())
                      && InternalVariablesPDF.LeerMailDebug()))
                    {
                        mails.Add(uid, email);

                    }


                }
                imap.Close();
                return mails;
            }
        }


        public static Dictionary<string, IMail> GetAllMailFromIconstruyePop3()
        {
            using (var pop3 = new Pop3())
            {
                pop3.Connect(InternalVariablesPDF.GetHostIconstruye());
                pop3.Login(InternalVariablesPDF.GetUserIconstruye(), InternalVariablesPDF.GetPasswordIcostruye());
                var uidList = pop3.GetAll();
                var mails = new Dictionary<string, IMail>();
                foreach (var uid in uidList)
                {

                    Console.WriteLine($"UIDS: {uid}");
                    var email = new MailBuilder().CreateFromEml(
                        pop3.GetMessageByUID(uid));
                    Console.WriteLine(email.From.ToString());
                    //if (email.From.T)
                    //    //.Subject.ToUpper().Equals(InternalVariablesPDF.GetSubjectMail()))
                    //{
                    mails.Add(uid, email);
                    //    //ProcessMessage(email);
                    //}
                }
                pop3.Close();
                return mails;
            }
        }


        public static Dictionary<long, IMail> GetAllMailFromIconstruyeImap()
        {
            using (var imap = new Imap())
            {
                imap.Connect(InternalVariablesPDF.GetHostIconstruye());
                imap.Login(InternalVariablesPDF.GetUserIconstruye(), InternalVariablesPDF.GetPasswordIcostruye());
                imap.SelectInbox();
                var uidList = imap.GetAll();
                var mails = new Dictionary<long, IMail>();
                foreach (var uid in uidList)
                {
                    var email = new MailBuilder().CreateFromEml(
                        imap.GetMessageByUID(uid));
                    ////mails.Add(uid, email);
                }
                imap.Close();
                return mails;
            }
        }



        public static void DeleteMail(this IMail email, string uid)
        {
            if (InternalVariablesPDF.IsDebug()) return;
            using (var pop3 = new Pop3())
            {
                pop3.Connect(InternalVariablesPDF.GetServidorCorreoLectura());
                pop3.Login(InternalVariablesPDF.GetUsuarioServidorCorreoLectura()
                    , InternalVariablesPDF.GetPassServidorCorreoLectura());
                pop3.DeleteMessageByUID(uid);
                pop3.Close();
            }
        }

        public static void ListFolderProcesosXml()
        {
            using (var imap = new Imap())
            {
                Console.WriteLine("CARPETAS PROCESOS XML");
                imap.Connect(InternalVariablesPDF.GetServidorCorreoLectura()); // or ConnectSSL for SSL
                imap.UseBestLogin(InternalVariablesPDF.GetUsuarioServidorCorreoLectura()
                    , InternalVariablesPDF.GetPassServidorCorreoLectura());
                foreach (var folder in imap.GetFolders())
                {
                    Console.WriteLine($"CARPETA: {folder.Name}");
                }
                imap.Close();

                Console.WriteLine("CARPETAS ICONSTRUYE");
                var imap2 = new Imap();
                imap2.Connect(InternalVariablesPDF.GetHostIconstruye());
                imap2.UseBestLogin(InternalVariablesPDF.GetUserIconstruye()
                                    , InternalVariablesPDF.GetPasswordIcostruye());
                foreach (var folder in imap2.GetFolders())
                {
                    Console.WriteLine($"CARPETA: {folder.Name}");
                }
                imap2.Close();
            }
        }

        public static void CreateFoler()
        {
            using (var imap = new Imap())
            {
                imap.Connect(InternalVariablesPDF.GetHostIconstruye());
                imap.UseBestLogin(InternalVariablesPDF.GetUserIconstruye()
                                    , InternalVariablesPDF.GetPasswordIcostruye());
                imap.SelectInbox();
                var all = imap.GetFolders();
                var existsProcesadas = all.Find(x => x.Name == InternalVariablesPDF.GetEmailFolderProcesadas()) != null;
                if (!existsProcesadas)
                {
                    imap.CreateFolder(InternalVariablesPDF.GetEmailFolderProcesadas());
                }
                var existsError = all.Find(x => x.Name == InternalVariablesPDF.GetEmailErrorFolderProcesadas()) != null;
                if (!existsError)
                {
                    imap.CreateFolder(InternalVariablesPDF.GetEmailErrorFolderProcesadas());
                }
                imap.SelectInbox();

                all = imap.GetFolders();
                imap.Close();
            }
            using (var imap = new Imap())
            {
                imap.Connect(InternalVariablesPDF.GetServidorCorreoLectura()); // or ConnectSSL for SSL
                imap.UseBestLogin(InternalVariablesPDF.GetUsuarioServidorCorreoLectura()
                    , InternalVariablesPDF.GetPassServidorCorreoLectura());
                imap.SelectInbox();
                var all = imap.GetFolders();
                var existsProcesadas = all.Find(x => x.Name == InternalVariablesPDF.GetEmailFolderProcesadas()) != null;
                if (!existsProcesadas)
                {
                    imap.CreateFolder(InternalVariablesPDF.GetEmailFolderProcesadas());
                }
                if (all.Find(x => x.Name == InternalVariablesPDF.GetCarpetaSinLectura()) == null)
                {
                    imap.CreateFolder(InternalVariablesPDF.GetCarpetaSinLectura());
                }
                var existsError = all.Find(x => x.Name == InternalVariablesPDF.GetEmailErrorFolderProcesadas()) != null;
                if (!existsError)
                {
                    imap.CreateFolder(InternalVariablesPDF.GetEmailErrorFolderProcesadas());
                }
                imap.SelectInbox();
                //foreach (var x in all)
                //{
                //    Console.WriteLine($"{x.Name}");
                //}
                imap.Close();
            }

        }

        public static void MoveProcesosMailTo(this IMail email, long uid, string folder, bool removeTrashmail = false)
        {
            if (InternalVariablesPDF.IsDebug() && !InternalVariablesPDF.DeleteDebugMail() && !removeTrashmail) return;
            using (var imap = new Imap())
            {
                imap.Connect(InternalVariablesPDF.GetServidorCorreoLectura()); // or ConnectSSL for SSL
                imap.UseBestLogin(InternalVariablesPDF.GetUsuarioServidorCorreoLectura()
                    , InternalVariablesPDF.GetPassServidorCorreoLectura());
                imap.SelectInbox();
                imap.MoveByUID(uid, folder);
                imap.Close();
            }

        }


        public static void MoveIcontruyeMailTo(this IMail email, long uid, string folder)
        {
            if (InternalVariablesPDF.IsDebug()) return;
            using (var imap = new Imap())
            {
                imap.Connect(InternalVariablesPDF.GetHostIconstruye()); // or ConnectSSL for SSL
                imap.UseBestLogin(InternalVariablesPDF.GetUserIconstruye()
                    , InternalVariablesPDF.GetPasswordIcostruye());
                imap.SelectInbox();
                imap.MoveByUID(uid, folder);
                imap.Close();
            }

        }


        public static void DeleteIconstruyeMail(this IMail email, string uid)
        {
            if (InternalVariablesPDF.IsDebug()) return;
            using (var pop3 = new Pop3())
            {
                pop3.Connect(InternalVariablesPDF.GetHostIconstruye());
                pop3.Login(InternalVariablesPDF.GetUserIconstruye(), InternalVariablesPDF.GetPasswordIcostruye());
                pop3.DeleteMessageByUID(uid);
                pop3.Close();
            }
        }

        public static List<string> GetBodyAsList(this IMail email)
        {
            var splitText = email.Text.Split('\n').BorrarLineasBlancas();
            return splitText.ToList();
        }
        public static void ProcessIconstruyeMessage(IMail email)
        {
            Console.WriteLine("Subject: " + email.Subject);
            Console.WriteLine("From: " + JoinAddresses(email.From));
            Console.WriteLine("To: " + JoinAddresses(email.To));
            Console.WriteLine("Cc: " + JoinAddresses(email.Cc));
            Console.WriteLine("Bcc: " + JoinAddresses(email.Bcc));
            var text = email.Text.Split('\n').BorrarLineasBlancas();
            var asunto = email.Subject.ToString();
            //Console.WriteLine("HTML: " + email.Html);

            Console.WriteLine("Attachments: ");
            foreach (var attachment in email.Attachments)
            {
                var extension = attachment.FileName.Substring(attachment.FileName.LastIndexOf(".", StringComparison.Ordinal)).ToUpper();
                if (extension.Equals(".XML"))
                {
                    var num = new Random();
                    var fileName = $"{DateTime.Now:dd-MM-yyyy-HH-mm-ss}_{num.Next(0, 99999)}_{attachment.SafeFileName}";
                    var xmlPath = InternalVariablesPDF.GetRutaXmlProcesados() + fileName;
                    attachment.Save(xmlPath);
                    var xmlReader = new XmlReader(xmlPath, attachment.SafeFileName);
                    if (asunto.Equals(@"Please purchase Mail.dll license at https://www.limilabs.com/mail"))
                    {
                        asunto = xmlReader.XmlRawFileName;
                    }
                    OracleDataAccess.InsertLecturaXml(asunto, xmlReader.ExtractTextAsString());
                }
            }
        }


        private static void ProcessMessage(IMail email)
        {
            Console.WriteLine("Subject: " + email.Subject);
            Console.WriteLine("From: " + JoinAddresses(email.From));
            Console.WriteLine("To: " + JoinAddresses(email.To));
            Console.WriteLine("Cc: " + JoinAddresses(email.Cc));
            Console.WriteLine("Bcc: " + JoinAddresses(email.Bcc));
            var text = email.Text.Split('\n').BorrarLineasBlancas();
            //Console.WriteLine("HTML: " + email.Html);

            //Console.WriteLine("Attachments: ");
            //foreach (MimeData attachment in email.Attachments)
            //{
            //    Console.WriteLine(attachment.FileName);
            //    attachment.Save(@"c:\" + attachment.SafeFileName);
            //}
        }

        public static string ConvertToString(this IMail email)
        {
            var splitText = email.Text.Split('\n').BorrarLineasBlancas();
            var ret = $"{email.Subject} {JoinAddresses(email.From)} {JoinAddresses(email.To)} {JoinAddresses(email.Cc)} {JoinAddresses(email.Bcc)} {splitText.ArrayToString(0, splitText.Length - 1)}";
            return ret;
        }

        private static string JoinAddresses(IList<MailAddress> addresses)
        {
            StringBuilder builder = new StringBuilder();

            foreach (MailAddress address in addresses)
            {
                if (address is MailGroup)
                {
                    MailGroup group = (MailGroup)address;
                    builder.AppendFormat("{0}: {1};, ", group.Name, JoinAddresses(group.Addresses));
                }
                if (address is MailBox)
                {
                    MailBox mailbox = (MailBox)address;
                    builder.AppendFormat("{0} <{1}>, ", mailbox.Name, mailbox.Address);
                }
            }
            return builder.ToString();
        }

        private static string JoinAddresses(IList<MailBox> mailboxes)
        {
            return string.Join(",",
                new List<MailBox>(mailboxes).ConvertAll(m => string.Format("{0} <{1}>", m.Name, m.Address))
                .ToArray());
        }
    }
}