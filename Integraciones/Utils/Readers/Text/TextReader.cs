﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Readers.Text
{
    public class TextReader
    {
        private readonly string _txtPath;

        /// <summary>
        /// Ruta Completa de Archivo Txt
        /// </summary>
        public string TxtPath => _txtPath;

        /// <summary>
        /// Ruta Base de Archivo Txt
        /// </summary>
        public string TxtRootPath => _txtPath.Substring(0, _txtPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

        /// <summary>
        /// Nombre de Archivo Txt
        /// </summary>
        public string TxtFileName => _txtPath.Substring(_txtPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

        /// <summary>
        /// Nombre de Orden de Compra de Txt
        /// </summary>
        public string TxtFileNameOC
            =>
                _txtPath.Substring(_txtPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1)
                    .Replace(".TXT", "")
                    .Replace(".txt", "");

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="txtPath">Ruta de Archivo Txt</param>
        public TextReader(string txtPath)
        {
            _txtPath = txtPath;
        }

        public override string ToString()
        {
            return ExtractToString();
        }



        public string ToStringWithoutContinuousSpace()
        {
            return ExtractToStringWithoutContinuousSpace();
        }

        private string ExtractToString()
        {
            var text = new StringBuilder();
            try
            {
                using (var sr = new StreamReader(_txtPath, false))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        text.Append(line);
                    }
                }
            }
            catch (Exception e)
            {
                text.Append("\n\n");
                text.Append("============================================\n");
                text.Append("============================================\n");
                text.Append($"  NO ES POSIBLE LEER EL ARCHIVO : {TxtFileName}\n");
                text.Append("============================================\n");
                text.Append("============================================\n");
                text.Append($"  ERROR : {e.Message}\n");
                text.Append("============================================\n");
                text.Append("============================================");
            }
            return text.ToString();
        }


        private string ExtractToStringOracle()
        {
            var text = new StringBuilder();
            try
            {
                using (var sr = new StreamReader(_txtPath, false))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        text.Append($"\n{line}");
                    }
                }
            }
            catch (Exception e)
            {
                text.Append("\n\n");
                text.Append("============================================\n");
                text.Append("============================================\n");
                text.Append($"  NO ES POSIBLE LEER EL ARCHIVO : {TxtFileName}\n");
                text.Append("============================================\n");
                text.Append("============================================\n");
                text.Append($"  ERROR : {e.Message}\n");
                text.Append("============================================\n");
                text.Append("============================================");
            }
            return text.ToString().Trim();
        }


        private string ExtractToStringWithoutContinuousSpace()
        {
            var text = new StringBuilder();
            try
            {
                using (var sr = new StreamReader(_txtPath, false))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        text.Append(line.DeleteContoniousWhiteSpace());
                    }
                }
            }
            catch (Exception e)
            {
                text.Append("\n\n");
                text.Append("============================================\n");
                text.Append("============================================\n");
                text.Append($"  NO ES POSIBLE LEER EL ARCHIVO : {TxtFileName}\n");
                text.Append("============================================\n");
                text.Append("============================================\n");
                text.Append($"  ERROR : {e.Message}\n");
                text.Append("============================================\n");
                text.Append("============================================");
            }
            return text.ToString();
        }



        /// <summary>
        /// Extrae Texto de PDF y retorna un Arreglo con dicho texto.
        /// </summary>
        /// <returns>Arreglo con Texto del PDF</returns>
        public string[] ExtractTextToArray()
        {
            var text = new List<string>();
            try
            {
                using (var sr = new StreamReader(_txtPath, false))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        text.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                text.Add("\n\n");
                text.Add("============================================\n");
                text.Add("============================================\n");
                text.Add($"  NO ES POSIBLE LEER EL ARCHIVO : {TxtFileName}\n");
                text.Add("============================================\n");
                text.Add("============================================\n");
                text.Add($"  ERROR : {e.Message}\n");
                text.Add("============================================\n");
                text.Add("============================================");
            }
            return text.ToArray();
        }

        /// <summary>
        /// Borra Lineas Repetidass de un Arreglo
        /// </summary>
        /// <param name="ret">Arreglo con Elementos Repetidos</param>
        /// <returns>Arreglo sin Elementos Repetidos</returns>
        private static string[] BorrarRepetidos(string[] ret)
        {
            for (var i = 0; i < ret.Length; i++)
            {
                for (var j = i + 1; j < ret.Length - 1; j++)
                {
                    if (ret[i].Equals(ret[j]))
                    {
                        ret[j] = "-1*";
                    }
                }
            }
            return ret.Where(x => !x.Equals("-1*")).ToArray();
        }

        /// <summary>
        /// Extraer el Texto del PDF como una sola linea para Insertar en Oracle
        /// </summary>
        /// <returns>Texto PDF</returns>
        public string ExtractTextToStringToOracle()
        {
            return ExtractToStringOracle().Replace("'", "''");
        }
    }

}