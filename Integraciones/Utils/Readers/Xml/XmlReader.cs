﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace Integraciones.Utils.Readers.Xml
{
    class XmlReader
    {
        public string XmlPath { get; set; }

        public string XmlRawFileName { get; set; }

        public string XmlFileName => XmlPath.Substring(XmlPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

        public string XmlText { get; set; }

        public string Test { get; set; }
        public XmlReader(string pathXml, string rawFilename)
        {
            XmlPath = pathXml;
            XmlRawFileName = rawFilename;
        }

        public string ExtractTextAsString()
        {
            var xDoc = new XmlDocument();
            xDoc.Load(XmlPath);
            XmlText = xDoc.OuterXml;
            return XmlText;
        }

        public string ExtractFolioDoc()
        {
            var folio = "";
            try
            {
                var xDoc = new XmlDocument();
                xDoc.Load(XmlPath);
                folio += $" Folio: {xDoc.GetElementsByTagName("Folio")[0].InnerXml}";
            }
            catch
            {
                folio = $"No es posible Reconocer el Folio del documento: {XmlFileName}";
            }
            return folio;

        }


        private Encoding GetFileEncoding()
        {
            Encoding enc = Encoding.Default;
            StreamReader sr = new StreamReader(XmlPath, true);
            enc = sr.CurrentEncoding;
            return enc;
        }
        public string TextUTF8()
        {
            var encoding = GetFileEncoding();
            Console.WriteLine($"Encoding :{encoding}");
            string s = File.ReadAllText(XmlPath, Encoding.ASCII);
            byte[] ansiBytes = File.ReadAllBytes(XmlPath);
            var utf8String = Encoding.Default.GetString(ansiBytes);
            XmlText = utf8String;
            return XmlText;
        }

    }
}
