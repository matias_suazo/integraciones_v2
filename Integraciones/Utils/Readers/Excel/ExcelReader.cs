﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using Range = Microsoft.Office.Interop.Excel.Range;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.FileUtils;

namespace Integraciones.Utils.Readers.Excel
{
    public class ExcelReader
    {
        public int NumerOfPages { get; private set; }

        public List<Hoja> Hojas { get; private set; }
        
        
        private readonly string _excelPath;

        /// <summary>
        /// Ruta Completa de Archivo Excel
        /// </summary>78.861.790
        public string ExcelPath => _excelPath;

        /// <summary>
        /// Ruta Base de Archivo PDF
        /// </summary>
        public string ExcelRootPath => _excelPath.Substring(0, _excelPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

        /// <summary>
        /// Nombre de Archivo PDF
        /// </summary>
        public string ExcelFileName => _excelPath.Substring(_excelPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

        /// <summary>
        /// Nombre de Orden de Compra de PDF
        /// </summary>
        public string ExcelFileNameOC => _excelPath.Substring(_excelPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1).Replace(".PDF", "").Replace(".pdf", "");


        public List<Worksheet> Worksheets { get; private set; }

        public ExcelReader(string excelPath, bool borrarFilasBlancasConsecutivas = false, bool soloPrimeraHoja = true
            , ColumnaExcel leerHastaColuma = ColumnaExcel.AL, int cantidadFilasLectura = -1)
        {
            _excelPath = excelPath;
            leerHastaColuma = ColumnaExcel.AL;
            Hojas = new List<Hoja>();
            SetVariables(soloPrimeraHoja, leerHastaColuma, cantidadFilasLectura);
            if (borrarFilasBlancasConsecutivas)
                DeleteConsecutiveEmptyRow();
                


        }

        

        public string ExtractTextFromExcelToStringToOracle()
        {
            return ToString().Replace("'", "''");
        }

        /// <summary>
        /// Borra las Filas consecutivas vacias
        /// </summary>
        public void DeleteConsecutiveEmptyRow()
        {
            foreach (var hoja in Hojas)
            {
                for (var i = 0; i < hoja.Filas.Count - 1; i++)
                {
                    if (hoja.Filas[i].IsEmpty() && hoja.Filas[i + 1].IsEmpty())

                    {
                        hoja.Filas.RemoveAt(i + 1);
                        i--;
                    }
                }
            }
        }


        /// <summary>
        /// Inicializa las Variables necesarias para ExcelReader
        /// </summary>
        private void SetVariables(bool soloPrimeraHoja, ColumnaExcel leerHastaColuma = ColumnaExcel.AL, int cantidadFilasLectura = -1)
        {

            try
            {

            

            var excelObj = new Application();
            var theWorkbook = excelObj.Workbooks.Open(ExcelPath);
            var sheets = theWorkbook.Sheets;
            Worksheets = sheets.Cast<Worksheet>().ToList();
            NumerOfPages = Worksheets.Count;
            var count = 0;
            foreach (var xlWorkSheet in Worksheets) //Hojas
            {
                //Se verifica si la hoja actual esta oculta dentro del archivo excel, si es asi, entonces continua con la siguiente interacion.
                if (xlWorkSheet.Visible == Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden) {                    
                    continue;
                }
                
                var range = xlWorkSheet.UsedRange;
                var rw = cantidadFilasLectura == -1 ? range.Rows.Count : cantidadFilasLectura;
                //var cl = leerHastaColuma == ColumnaExcel.FLAG ? range.Columns.Count : (int) leerHastaColuma;

                var filas = new List<Fila>();
                var cl = (int)ColumnaExcel.GK;
                    //if (filas[1].ToString().Contains("76230088-5"))
                    //{
                    //    cl = (int)ColumnaExcel.N;
                    //}else
                    //{
                    //    continue;
                    //}
                Console.WriteLine($"Worksheet: {count}, WorksheetName: {xlWorkSheet.Name}");
                    bool flagterminalectura = false;
                    bool flagAfubach = false;
                for (var rCnt = 1; rCnt <= rw; rCnt++) //Fila
                {
                     var columnas = new List<Columna>();
                    for (var cCnt =1; cCnt <= cl; cCnt++) //Columa
                    {
                            Console.WriteLine("cCnt:" + cCnt);
                        var range1 = range.Cells[rCnt, cCnt] as Range;
                        if (range1 == null) continue;
                        
                        var str = range1.Value2;
                        //if (!$"{str}".Equals(""))
                        var valor = $"{str}";
                        valor = valor.Replace(" ", "").Equals("") ? "" : valor;
                            Console.WriteLine($"FILAS COUNT{filas.Count}");
                            if (filas.Count > 0)
                            {
                                if (flagterminalectura == false)
                                {
                                    List<ExcelReaderCabecera> objCabeceras = OracleDataAccess.GetColumnsByHeader();
                                    foreach (var item in objCabeceras)
                                    {
                                        if (filas[0].ToString().Contains(item.cabecera))
                                        {
                                            if (filas[0].ToString().Contains("76792110-1"))
                                            {
                                                flagAfubach = true;
                                            }
                                            if (filas[0].ToString().Contains("76230088-5"))
                                            {
                                                cl = 14;
                                            }
                                            cl = item.columnas;
                                            break;
                                        }
                                        
                                    }

                                }
                                if (cCnt == 1 && (valor.Equals("") || valor == string.Empty))
                                {
                                    flagterminalectura = true;
                                    break;
                                }



                                /*
                                if (filas[0].ToString().Contains("RUT(*) CC(*) OC (*) SKU(*) CANTIDAD(*)") || filas[0].ToString().Contains("Nomina Rut Razon social Cui Desc_Cui Direccion Ciudad Tipo Region Ped_Numero Rut_contacto Dv_contacto Nom_contacto Item Item_Desc Cantidad Valor Fecha_sist. Fecha_emision N/U") || filas[0].ToString().Contains("Rut CentroCosto Item Cantidad Precio Neto Iva Total N°Solicitud Fecha Solicitud Solicitante Ubicacion") || filas[0].ToString().Contains("RUT EMPRESA FECHA OC INSTALACION CC ITEMS") || filas[0].ToString().Contains(" RUT	Empresa	Fecha	O/C	Instalacion	cc"))
                                {
                                    if (filas[0].ToString().Contains("RUT(*) CC(*) OC (*) SKU(*) CANTIDAD(*)"))
                                    {
                                        cl = 11;
                                    }
                                    else if (filas[0].ToString().Contains("Nomina Rut Razon social Cui Desc_Cui Direccion Ciudad Tipo Region Ped_Numero Rut_contacto Dv_contacto Nom_contacto Item Item_Desc Cantidad Valor Fecha_sist. Fecha_emision N/U"))
                                    {
                                        cl = 21;
                                    }
                                    else if (filas[0].ToString().Contains("Rut CentroCosto Item Cantidad Precio Neto Iva Total N°Solicitud Fecha Solicitud Solicitante Ubicacion"))
                                    {
                                        cl = 12;
                                    }
                                    if (filas[0].ToString().Contains("RUT EMPRESA FECHA OC INSTALACION CC ITEMS"))
                                    {
                                        cl = 13;
                                    }
                                    if (filas[0].ToString().Contains(" RUT	Empresa	Fecha	O/C	Instalacion	cc"))
                                    {
                                        cl = 13;
                                    }

                                    if (cCnt == 1 && (valor.Equals("") || valor == string.Empty))
                                    {
                                        flagterminalectura = true;
                                        break;
                                    }


                                }*/
                            }
                            if (flagterminalectura)
                            {
                                break;
                            }
                            columnas.Add(new Columna {Valor =valor});
                            
                    }
                        if (flagterminalectura)
                        {
                            break;
                        }
                        //if (columnas.Count > 0)
                        filas.Add(new Fila {Columnas = columnas});

                }
                //if (filas.Count > 0)
                var hoja = new Hoja {Nombre = xlWorkSheet.Name.DeleteContoniousWhiteSpace(), Filas = filas};
                if (!hoja.IsEmpty())
                    Hojas.Add(hoja);
                if (soloPrimeraHoja && !flagAfubach) break;
                count++;
            }
            theWorkbook.Close(false);
            excelObj.Quit();
            Console.WriteLine(@"Fin");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        /// <summary>
        /// Obtiene el valor de una Celda de Excel
        /// </summary>
        /// <param name="excelPoint">Punto de Excel (Hoja,Fila, Columna)</param>
        /// <param name="fila">[Opcional, no requiere Fila] Fila que se requiere extraer la Columna de la Hoja deseada</param>
        /// <param name="hoja">[Opcional, no requiere Hola] Hoja que se requiere extraer la Columna deseada</param>
        /// <returns>Valor de Hoja,Fila,Columna</returns>
        public string GetExcelPoint(ExcelPoint excelPoint, int fila = -1, int hoja = -1)
        {

            var ret = "";
            try
            {
                excelPoint.Hoja = hoja == -1 ? excelPoint.Hoja : hoja;
                fila = fila == -1 ? excelPoint.Fila : fila;
                ret = fila >= Hojas[excelPoint.Hoja]
                    .Filas.Count
                    ? ""
                    : (int) excelPoint.Columna
                      > Hojas[excelPoint.Hoja]
                          .Filas[fila]
                            .Columnas.Count
                        ? ""
                        : Hojas[excelPoint.Hoja]
                            .Filas[fila]
                            .Columnas[(int) excelPoint.Columna].Valor;
            }catch (ArgumentOutOfRangeException)
            {
                ret = "";
            }catch (Exception)
            {
                ret = "";
            }
            return ret;
        }

        public string GetDateFromExcelPoint(ExcelPoint excelPoint, int fila = -1, int hoja = -1)
        {
            var ret = "";
            excelPoint.Hoja = hoja == -1 ? excelPoint.Hoja : hoja;
            var stringDate = GetExcelPoint(excelPoint, fila,excelPoint.Hoja);
            var date = FromExcelSerialDate(stringDate);
            ret = date.ToShortDateString();
            //Console.WriteLine($"ToShortDateString: {date.ToShortDateString()}:\nToLongDateString:{date.ToLongDateString()}");
            return ret;
        }

        private DateTime FromExcelSerialDate(string stringDate)
        {
            var serialDate = 0;
            int.TryParse(stringDate, out serialDate);
            if (serialDate > 59) serialDate -= 1; //Excel/Lotus 2/29/1900 bug   
            return new DateTime(1899, 12, 31).AddDays(serialDate);
        }


        /// <summary>
        /// Devuelve los valores de Hojas, Filas y Columna
        /// </summary>
        /// <returns>String</returns>
        public override string ToString()
        {
            return
                Hojas.Aggregate("", (current, hoja) => current + $"{hoja}\n")
                    .DeleteContoniousWhiteSpace();
        }

        public void SaveTextToPlainFile()
        {
            FileUtils.FileUtils.SaveStringToTxtFile($"{ExcelRootPath}{ExcelFileName}.txt", ToString());
        }

        public string GetNameExcel()
        {
            var excelObj = new Application();
            var workBook = excelObj.Workbooks.Open(ExcelPath);
            var nameFileExc = workBook.Name;
            return nameFileExc;
        }
    }

    public class Hoja
    {
        public string Nombre { get; set; }
        public List<Fila> Filas { get; set; }

        public override string ToString()
        {
            return Filas.Aggregate("", (current, fila) => current + $"{fila}\n").DeleteContoniousWhiteSpace();
        }

        public bool IsEmpty()
        {
            var count = Filas.Count(fila => fila.IsEmpty());
            return count == Filas.Count;
        }

    }

    public class Fila
    {
        public List<Columna> Columnas { get; set; }

        public override string ToString()
        {
            return Columnas.Aggregate("", (current, col) => current + $"{col} ").DeleteContoniousWhiteSpace();
        }

        public bool IsEmpty()
        {
            var count = Columnas.Count(col => col.Valor.Equals(""));
            return count == Columnas.Count;
        }



        public bool IsNaNEmpty()
        {
            var count = Columnas.Count(col => col.Valor.Equals(""));
            return count >= Columnas.Count-1;
        }
        public int GetColumnasNotEmpty()
        {
            return Columnas.Count(col => !col.Valor.Equals(""));
        }
    }

    public class Columna
    {
        public string Valor { get; set; }

        public override string ToString()
        {
            return Valor;
        }
    }

    public class ExcelPoint
    {
        public int Hoja { get; set; }
        public int Fila { get; set; }

        public ColumnaExcel Columna { get; set; }

        public ExcelPoint()
        {
            Hoja = 0;
            Fila = -1;
            Columna = ColumnaExcel.A;
        }
    }

    public enum ColumnaExcel
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        F = 5,
        G = 6,
        H = 7,
        I = 8,
        J = 9,
        K = 10,
        L = 11,
        M = 12,
        N = 13,
        O = 14,
        P = 15,
        Q = 16,
        R = 17,
        S = 18,
        T = 19,
        U = 20,
        V = 21,
        W = 22,
        X = 23,
        Y = 24,
        Z = 25,
        AA = 26,
        AB = 27,
        AC = 28,
        AD = 29,
        AE = 30,
        AF = 31,
        AG = 32,
        AH = 33,
        AI = 34,
        AJ = 35,
        AK = 36,
        AL = 37,
        AM = 38,
        AN = 39,
        AO = 40,
        AP = 41,
        AQ = 42,
        AR = 43,
        AS = 44,
        AT = 45,
        AU = 46,
        AV = 47,
        AW = 48,
        AX = 49,
        AY = 50,
        AZ = 51,
        BA = 52,
        BB = 53,
        BC = 54,
        BD = 55,
        BE = 56,
        BF = 57,
        BG = 58,
        BH = 59,
        BI = 60,
        BJ = 61,
        BK = 62,
        BL = 63,
        BM = 64,
        BN = 65,
        BO = 66,
        BP = 67,
        BQ = 68,
        BR = 69,
        BS = 70,
        BT = 71,
        BU = 72,
        BV = 73,
        BW = 74,
        BX = 75,
        BY = 76,
        BZ = 77,
        CA = 78,
        CB = 79,
        CC = 80,
        CD = 81,
        CE = 82,
        CF = 83,
        CG = 84,
        CH = 85,
        CI = 86,
        CJ = 87,
        CK = 88,
        CL = 89,
        CM = 90,
        CN = 91,
        CO = 92,
        CP = 93,
        CQ = 94,
        CR = 95,
        CS = 96,
        CT = 97,
        CU = 98,
        CV = 99,
        CW = 100,
        CX = 101,
        CY = 102,
        CZ = 103,
        DA = 104,
        DB = 105,
        DC = 106,
        DD = 107,
        DE = 108,
        DF = 109,
        DG = 110,
        DH = 111,
        DI = 112,
        DJ = 113,
        DK = 114,
        DL = 115,
        DM = 116,
        DN = 117,
        DO = 118,
        DP = 119,
        DQ = 120,
        DR = 121,
        DS = 122,
        DT = 123,
        DU = 124,
        DV = 125,
        DW = 126,
        DX = 127,
        DY = 128,
        DZ = 129,
        EA = 130,
        EB = 131,
        EC = 132,
        ED = 133,
        EE = 134,
        EF = 135,
        EG = 136,
        EH = 137,
        EI = 138,
        EJ = 139,
        EK = 140,
        EL = 141,
        EM = 142,
        EN = 143,
        EO = 144,
        EP = 145,
        EQ = 146,
        ER = 147,
        ES = 148,
        ET = 149,
        EU = 150,
        EV = 151,
        EW = 152,
        EX = 153,
        EY = 154,
        EZ = 155,
        FA = 156,
        FB = 157,
        FC = 158,
        FD = 159,
        FE = 160,
        FF = 161,
        FG = 162,
        FH = 163,
        FI = 164,
        FJ = 165,
        FK = 166,
        FL = 167,
        FM = 168,
        FN = 169,
        FO = 170,
        FP = 171,
        FQ = 172,
        FR = 173,
        FS = 174,
        FT = 175,
        FU = 176,
        FV = 177,
        FW = 178,
        FX = 179,
        FY = 180,
        FZ = 181,
        GA = 182,
        GB = 183,
        GC = 184,
        GD = 185,
        GE = 186,
        GF = 187,
        GG = 188,
        GH = 189,
        GI = 190,
        GJ = 191,
        GK = 192,
        GL = 193,
        GM = 194,
        GN = 195,
        GO = 196,
        GP = 197,
        GQ = 198,
        GR = 199,
        GS = 200,
        GT = 201,
        GU = 202,
        GV = 203,
        GW = 204,
        GX = 205,
        GY = 206,
        GZ = 207,
        HA = 208,
        HB = 209,
        HC = 210,
        HD = 211,
        HE = 212,
        HF = 213,
        HG = 214,
        HH = 215,
        HI = 216,
        HJ = 217,
        HK = 218,
        HL = 219,
        HM = 220,
        HN = 221,
        HO = 222,
        HP = 223,
        HQ = 224,
        HR = 225,
        HS = 226,
        HT = 227,
        HU = 228,
        HV = 229,
        HW = 230,
        HX = 231,
        HY = 232,
        HZ = 233,
        FLAG = -1
    }
}