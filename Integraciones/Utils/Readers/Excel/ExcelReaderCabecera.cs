﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Readers.Excel
{
    public class ExcelReaderCabecera
    {
        public string cabecera { get; set; }
        public int columnas { get; set; }
        public int estado { get; set; }
        
    }
}
