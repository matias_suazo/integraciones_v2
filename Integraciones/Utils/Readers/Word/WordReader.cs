﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;

namespace Integraciones.Utils.Readers.Word
{
    public class WordReader
    {

        private readonly string _wordPath;

        /// <summary>
        /// Ruta Completa de Archivo Word
        /// </summary>
        public string WordPath => _wordPath;

        /// <summary>
        /// Ruta Base de Archivo Word
        /// </summary>
        public string WordRootPath => _wordPath.Substring(0, _wordPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);

        /// <summary>
        /// Nombre de Archivo PDF
        /// </summary>
        public string WordFileName
        {
            get
            {
                var file = _wordPath
                    .Substring(_wordPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
                return file.Substring(0, file.LastIndexOf(".", StringComparison.Ordinal));
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="wordPath">Ruta de Word</param>
        public WordReader(string wordPath)
        {
            _wordPath = wordPath;
        }

        /// <summary>
        /// Extrae Texto de Word como un sola Linea
        /// </summary>
        /// <returns>Texto Word</returns>
        public string ExtractTextFromWordToString()
        {
            return ExtractTextFromWordToList()
                .Aggregate("", (current, linea) => current + $"{linea} ")
                .DeleteContoniousWhiteSpace();
        }


        /// <summary>
        /// Normaliza las Palabras de Word con Formato Genérico
        /// </summary>
        /// <returns>Lista de Lineas</returns>
        private IEnumerable<string> ExtractTextFromWordToList()
        {
            var textList = ExtractTextByWords();
            var linea = "";
            var textListReturn = new List<string>();
            var vaciosConsecutivos = 0;
            foreach (var text in textList)
            {
                if (text.DeleteBelHexadecimalValues().Length == 0) vaciosConsecutivos++;
                else vaciosConsecutivos = 0;
                if (vaciosConsecutivos >= 2)
                {
                    textListReturn.Add(linea.Replace(" . ", ".").Replace(" – ", "-").Replace(" - ", "-").Replace(" ( ", "(").Replace(" ) ", ")").Replace(" / ", "/").DeleteContoniousWhiteSpace());
                    linea = "";
                    vaciosConsecutivos = 0;
                }
                else
                {
                    linea += $"{(text.Replace("\a", ""))} ";
                }
            }
            return textListReturn;
        }


        /// <summary>
        /// Extrae palabras de Word como una Lista sin Formato
        /// </summary>
        /// <returns>Lista palabras</returns>
        private IEnumerable<string> ExtractTextByWords()
        {
            var application = new Application();
            var document = application.Documents.Open(WordPath, ReadOnly: true);
            var textReturn = new List<string>();
            var count = document.Words.Count;
            for (var i = 1; i <= count; i++)
            {
                var text = document.Words[i].Text.Replace("\n", "").DeleteContoniousWhiteSpace();
                textReturn.Add(text);
            }
            application.Quit();
            return textReturn;
        }


        /// <summary>
        /// Normaliza Texto en Word para ser Insertado en Oracle
        /// </summary>
        /// <returns>Texto Normalizado para Oracle</returns>
        public string ExtractTextFromWordToStringToOracle()
        {
            return ExtractTextFromWordToString().Replace("'", "''");
        }


        public string[] ExtractTextFromWordToArray()
        {
            return ExtractTextFromWordToList().ToArray();
        }


        /// <summary>
        /// Guarda el Contenido del Documento Word como un Fichero de texto Plano en la misma carpeta del documento Word
        /// </summary>
        public void SaveTextToPlainFile()
        {
            foreach (var line in ExtractTextFromWordToList())
            {
                FileUtils.FileUtils.SaveStringToTxtFile($"{WordRootPath}{WordFileName}_default_mode.txt", line);
            }
        }

    }
}