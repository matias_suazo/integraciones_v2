﻿namespace Integraciones.Utils.Respuesta
{
    public class ResultadoCantiadPrecioItem
    {
        public int Cantidad { get; set; }
        public int Precio { get; set; }

    }
}