﻿using Integraciones.Utils.OrdenCompra.Integracion;

namespace Integraciones.Utils.Respuesta
{
    public class ResultadoCentroCosto
    {
        public string Cencos { get; set; }
        public EstadoOrden EstadoOrden { get; set; }

        public ResultadoCentroCosto()
        {
            Cencos = "0";
            EstadoOrden = EstadoOrden.POR_PROCESAR;
        }
    }
}