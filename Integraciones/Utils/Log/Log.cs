﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using Integraciones.Utils.Email;
using Integraciones.Utils.Error;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Popup;
using InternalVariablesPDF = Integraciones.Utils.InternalVariables.InternalVariables;
using Integraciones.Utils.RegistroErroresEmail;

namespace Integraciones.Utils.Log
{
    public static class Log
    {

        #region VARIABLES
        private static RegistroErroresModel rem = new RegistroErroresModel();
        private static List<MailCencosFaltantes> CencosFaltantes { get; set; }

        private static List<MailProblemaConversionUnidades> ProblemaConversionUnidades { get; set; }

        private static List<MailSkuFaltante> SkuFaltantes { get; set; }

        private static List<OrdenCompraIntegracion> OrdenesProcesadas { get; set; }

        private static List<OrdenCompraIntegracionBCH> OrdenesProcesadasBCH { get; set; }

        private static List<OrdenCompraIntegracion> OrdenesProcesadasRepetidas { get; set; }

        private static List<OrdenCompraIntegracionBCH> OrdenesProcesadasRepetidasBCH { get; set; }

        private static List<OrdenCompraIntegracion> OrdenesProcesadasFaltaPareoCentroCosto { get; set; }

        private static List<OrdenCompraIntegracionBCH> OrdenesProcesadasFaltaPareoCentroCostoBCH { get; set; }

        private static List<OrdenCompraIntegracion> OrdenesProcesadasFaltaPareoSku { get; set; }

        private static List<OrdenCompraIntegracionBCH> OrdenesProcesadasFaltaPareoSkuBCH { get; set; }
        
        private static List<OrdenCompraIntegracion> OrdenesProcesadasProblemaDetalle { get; set; }


        private static List<OrdenCompraIntegracionBCH> OrdenesProcesadasProblemaDetalleBCH { get; set; }

        private static List<Popup.Popup> PopupsEjecutivos { get; set; }
        
        //private static List<OrdenCompraIntegracion>  
        #endregion

        public static void Save(string title, string save)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {
                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\Integración_Log.txt", true);
                writer.WriteLine($"{DateTime.Now} - {title} - {save}");
                writer.Close();
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                Save(title, save);
            }

        }

        public static void Save(string save)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {
                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\Integración_Log.txt", true);
                writer.WriteLine($"{DateTime.Now} - {save}");
                writer.Close();
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                Save(save);
            }

        }

        public static void SaveItemFaltantes(string rutCli,string cab, string save)
        {
            try
            {
                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\FaltaPareoSKU_Cliente.txt", true);
                writer.WriteLine($"{DateTime.Now} - {cab} {save}");
                writer.Close();
                AddMailSkuFaltantes(rutCli, save);
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                SaveItemFaltantes(rutCli,cab, save);
            }
        }

        public static void TryError(string save)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {
                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\Error_Log.txt", true);
                writer.WriteLine($"{DateTime.Now} - {save}");
                writer.Close();
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                TryError(save);
            }
        }

        public static void SaveUnknownFile(string filePath)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {
                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\UnknownFile.txt", true);
                writer.WriteLine(filePath);
                writer.Close();
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                TryError(filePath);
            }
        }

        public static void SaveErrorFile(string filePath)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {
                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\ErrorFile.txt", true);
                writer.WriteLine(filePath);
                writer.Close();
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                TryError(filePath);
            }
        }

        public static string GetUnkwnownFileLog()
        {
            return InternalVariablesPDF.GetLogFolder() + @"UnknownFile.txt";
        }

        public static string GetErrorFileLog()
        {
            return InternalVariablesPDF.GetLogFolder() + @"\ErrorFile.txt";
        }

        public static void StartApp()
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            var fecha = DateTime.Now;
            Save("Inicio",
                $"Aplicación Iniciada el {fecha.ToShortDateString()}" + $" a las {fecha.ToLongTimeString()} Hrs.");
        }

        public static void FinalizeApp()
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            var fecha = DateTime.Now;
            Save("Fin",
                $"Aplicación Finalizada el {fecha.ToShortDateString()} a las {fecha.ToLongTimeString()} Hrs.");
        }

        public static void SaveCentroCostoFaltantes(string ocCliente, string rutCli, string ccosto)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {

                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\FaltaPareoCC_Cliente.txt", true);
                writer.WriteLine(
                    $"{DateTime.Now} - No existe Centro de Costo para el Cliente rut: {rutCli}, con Descripción: {ccosto}");
                writer.Close();
                AddMailCencosFaltantes(ocCliente, rutCli, ccosto);
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                SaveCentroCostoFaltantes(ocCliente, rutCli, ccosto);
            }
        }

        
        public static void SaveProblemaConversionUnidades(string ocNumber, string rutCli,string codigoCliente, string codPro, string subPdf, int subMult)
        {
            if (!InternalVariables.InternalVariables.UseLog()) return;
            try
            {

                var writer = new StreamWriter(InternalVariablesPDF.GetLogFolder() + @"\ProblemaUnidades.txt", true);
                var subP = subMult/int.Parse(subPdf);
                var asunto =
                    $"Orden de Compra: {ocNumber}, producto: {codigoCliente} / {codPro}, el Sub Total del PDF es: {subPdf}, el Sub Total de Tabla de Conversión es: {subMult}";
                var query = $"update dm_ventas.re_codcli set multiplo = {subP} where rutcli = {rutCli} and codpro = '{codPro}'";
                writer.WriteLine(
                    $"{DateTime.Now}\t - Problema Subtotal para el Cliente: {rutCli}.\n" +
                    $"\t\t\t\t\t - {asunto}\n" +
                    "\t\t\t\t\t - Revisar Tabla de Conversión de Unidades.\n" +
                    $"\t\t\t\t\t - Si el sistema está en los correcto, el multiplo debe ser: {subP},\n" +
                    "\t\t\t\t\t - Favor de Actualizar la Tabla de Conversiones con el siguiente Comando SQL:\n" +
                    $"\t\t\t\t\t - {query}");
                writer.Close();
                asunto += $". El multiplo debe ser: {subP}.";
                AddMailProblemaConversionUnidades(rutCli, asunto, query);
            }
            catch (Exception)
            {
                Thread.Sleep(2000);
                SaveProblemaConversionUnidades(ocNumber, rutCli, codigoCliente, codPro, subPdf, subMult);
            }
        }


        #region Mail's

        /// <summary>
        /// Agrega orden de Compra para enviar Correo Resumen y actualizacion en BDD
        /// </summary>
        /// <param name="oc">Orden de Compra Integración</param>
        public static void AddMailUpdateTelemarketing(OrdenCompraIntegracion oc)
        {
            if (oc.Flujo == TipoFlujo.FLUJO_NUEVO)
                AddMailUpdateTelemarketingNewFlow(oc);
            else if (oc.DetallesCompra.Count == 0)
                AddMailOrdenesProcesadasProblemaDetalle(oc);
            else if (oc.EstadoOrden == EstadoOrden.ORDEN_REPETIDA)
                OrdenesProcesadasRepetidas.Add(oc);
            else
                OrdenesProcesadas.Add(oc);
        }

        public static void AddMailUpdateTelemarketingBCH(OrdenCompraIntegracionBCH oc)
        {
            if (oc.Flujo == TipoFlujo.FLUJO_NUEVO)
                AddMailUpdateTelemarketingNewFlowBCH(oc);
            else if (oc.DetallesCompra.Count == 0)
                AddMailOrdenesProcesadasProblemaDetalleBCH(oc);
            else if (oc.EstadoOrden == EstadoOrden.ORDEN_REPETIDA)
                OrdenesProcesadasRepetidasBCH.Add(oc);
            else
                OrdenesProcesadasBCH.Add(oc);
        }

        private static void AddMailUpdateTelemarketingNewFlow(OrdenCompraIntegracion oc)
        {
            if (oc.DetallesCompra.Count == 0)
                AddMailOrdenesProcesadasProblemaDetalle(oc);
            switch (oc.EstadoOrden)
            {
                case EstadoOrden.ORDEN_REPETIDA:
                    OrdenesProcesadasRepetidas.Add(oc);
                    break;
                case EstadoOrden.CENTRO_COSTO_NO_PAREADO:
                    OrdenesProcesadasFaltaPareoCentroCosto.Add(oc);
                    break;
                case EstadoOrden.SKU_NO_PAREADO:
                    OrdenesProcesadasFaltaPareoSku.Add(oc);
                    break;
                default:
                    OrdenesProcesadas.Add(oc);
                    break;
            }

        }

        private static void AddMailUpdateTelemarketingNewFlowBCH(OrdenCompraIntegracionBCH oc)
        {
            try
            {

            
            if (oc.DetallesCompra.Count == 0)
                AddMailOrdenesProcesadasProblemaDetalleBCH(oc);
            switch (oc.EstadoOrden)
            {
                case EstadoOrden.ORDEN_REPETIDA:
                    OrdenesProcesadasRepetidasBCH.Add(oc);
                    break;
                case EstadoOrden.CENTRO_COSTO_NO_PAREADO:
                    OrdenesProcesadasFaltaPareoCentroCostoBCH.Add(oc);
                    break;
                case EstadoOrden.SKU_NO_PAREADO:
                    OrdenesProcesadasFaltaPareoSkuBCH.Add(oc);
                    break;
                default:
                    OrdenesProcesadasBCH.Add(oc);
                    break;
            }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }

        private static void AddMailOrdenesProcesadasProblemaDetalle(OrdenCompraIntegracion oc)
        {
            OrdenesProcesadasProblemaDetalle.Add(oc);
        }

        private static void AddMailOrdenesProcesadasProblemaDetalleBCH(OrdenCompraIntegracionBCH oc)
        {
            OrdenesProcesadasProblemaDetalleBCH.Add(oc);
        }

        private static void AddPopupTelemarketing(Popup.Popup p)
        {
            PopupsEjecutivos.Add(p);
        }

        private static void AddDetallePopupToRutUsuario(string rutUsuario, DetallePopup det)
        {
            foreach (var pop in from pop in
                PopupsEjecutivos.Where(pop => pop.RutUsuario.Equals(rutUsuario))
                from d in pop.Detalles.Where(d => d.RutCli.Equals(det.RutCli))
                select pop)
            {
                pop.AddDetallePopup(det);
            }
        }

       /* private static void AddMailOrdenesProcesadasProblemaDetalleBCH(OrdenCompraIntegracionBCH oc)
        {
            OrdenesProcesadasProblemaDetalle.Add(oc);
        }*/


       

        private static void AddMailCencosFaltantes(string ocCliente, string rutCli, string ccosto)
        {
            var m = CencosFaltantes.Find(mail => mail.RutCliente.Equals(rutCli));
            if (m != null)
            {
                m.Body.Add($"Para la Orden : {ocCliente}, no se puede reconocer el Centro de Costo con Descripción: {ccosto}");
            }
            else
            {
                var razon = OracleDataAccess.GetRazonSocial(rutCli);
                var email = OracleDataAccess.GetEmailFromRutCliente(rutCli);
                CencosFaltantes.Add(new MailCencosFaltantes(rutCli, 
                    razon,
                    email,
                    $"Para la Orden :{ocCliente}, no se puede reconocer el Centro de Costo con Descripción: {ccosto}"));
            }
        }


        private static void AddMailProblemaConversionUnidades(string rutCli, string asunto, string query)
        {
            var m = ProblemaConversionUnidades.Find(mail => mail.RutCliente.Equals(rutCli));
            if (m != null)
            {
                m.Body.Add(asunto);
                m.Querys.Add(query);
            }
            else
            {
                var razon = OracleDataAccess.GetRazonSocial(rutCli);
                var email = OracleDataAccess.GetEmailFromRutCliente(rutCli);
                ProblemaConversionUnidades.Add(new MailProblemaConversionUnidades(rutCli,
                    razon,
                    email,
                    asunto, query));
            }
        }

        private static void AddMailSkuFaltantes(string rutCli, string asunto)
        {
            var m = SkuFaltantes.Find(mail => mail.RutCliente.Equals(rutCli));
            if (m != null)
            {
                m.Body.Add(asunto);
            }
            else
            {
                var razon = OracleDataAccess.GetRazonSocial(rutCli);
                var email = OracleDataAccess.GetEmailFromRutCliente(rutCli);
                SkuFaltantes.Add(new MailSkuFaltante(rutCli,
                    razon,
                    email,
                    asunto));
            }
        }

        private static void SendMailCencosFaltantes()
        {
            if (CencosFaltantes.Count == 0) return;
            foreach (var mail in CencosFaltantes)
            {
                EmailSender.
                    SendEmailFromProcesosXmlDimerc(
                        InternalVariablesPDF.GetMainEmail()
                        , null
                        , mail.Subject
                        , mail.MailBody);
            }
            CencosFaltantes = new List<MailCencosFaltantes>();
        }


        private static void SendMailProblemasConversion()
        {
            if (ProblemaConversionUnidades.Count == 0) return;
            foreach (var mail in ProblemaConversionUnidades)
            {
                EmailSender.
                    SendEmailFromProcesosXmlDimerc(
                        InternalVariablesPDF.GetMainEmail(),
                        null,
                        InternalVariablesPDF.IsDebug()
                            ? $"DEBUG {InternalVariablesPDF.GetSubjectDebug()}: {mail.Subject}"
                            : mail.Subject,
                        mail.MailBody);
            }
            ProblemaConversionUnidades = new List<MailProblemaConversionUnidades>();
        }
        
        private static void SendMailSkuFaltantes()
        {
            if (SkuFaltantes.Count == 0) return;
            foreach (var mail in SkuFaltantes)
            {
                
                EmailSender.
                    SendEmailFromProcesosXmlDimerc(
                        InternalVariablesPDF.GetMainEmail(),
                        null,
                        InternalVariablesPDF.IsDebug()
                            ? $"DEBUG {InternalVariablesPDF.GetSubjectDebug()}: {mail.Subject}"
                            : mail.Subject,
                        mail.MailBody);
            }
            SkuFaltantes = new List<MailSkuFaltante>();
        }



        private static void SendMailOrdenesProcesadasProblemaDetalle()
        {
            if (OrdenesProcesadasProblemaDetalle.Count == 0) return;
            var lastRut = 0;
            var subject = $"Fecha: {DateTime.Now} - Ordenes con Problema en Detalle: {OrdenesProcesadasProblemaDetalle.Count}";
            var body = $"Fecha: {DateTime.Now} - Ordenes con Problema en Detalle: {OrdenesProcesadasProblemaDetalle.Count}\n";
            var first = true;

            foreach (var oc in OrdenesProcesadasProblemaDetalle.OrderBy(o=> o.RutCli))
            {
                oc.EmailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(oc.RutCli.ToString());
                oc.RutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.RutCli.ToString());
                oc.Razon = OracleDataAccess.GetRazonSocial(oc.RutCli.ToString());
                if (!lastRut.Equals(oc.RutCli))
                {
                    lastRut = oc.RutCli;
                    body += 
                        $"Para las siguientes Ordenes en PDF no se puede leer correctamente el Detalle del Pedido.\n\n{(first ? "\n\n" : "\n\n\n")}Empresa: {oc.Razon}, Rut: {oc.RutCli}, Mail ejecutivo: {oc.EmailEjecutivo}\n\n" +
                        $"OC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}\n";
                    var re = new RegistroErrores
                    {
                        rutcli = oc.RutCli,
                        ejecutiva = OracleDataAccess.GetEjecutivoFromRutCliente(oc.RutCli.ToString()),
                        email_ejecutiva = oc.EmailEjecutivo,
                        descripcion = body,
                        tipoError = "Ordenes con Problema en Detalle",
                        tipoIntegracion = OrdenCompraIntegracion.getTipoByEnum(Convert.ToInt32(oc.TipoIntegracion))                        
                    };
                    rem.insertarRegistro(re);
                    first = false;
                }
                else
                {
                    body +=
                        $"OC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}\n";
                }

            }
            EmailSender.
               SendEmailFromProcesosXmlDimerc(
                   InternalVariablesPDF.GetMainEmail(),
                   InternalVariablesPDF.IsDebug()
                       ? null
                       : InternalVariablesPDF.GetEmailCc(),
                   InternalVariablesPDF.IsDebug()
                       ? $"DEBUG {InternalVariablesPDF.GetSubjectDebug()}: {subject}"
                       : subject,
                   body);
            SendMailOrdenesProcesadasProblemaDetalleToEjecutivo();
            OrdenesProcesadasProblemaDetalle = new List<OrdenCompraIntegracion>();
        }

        private static void ThrowInternalError(Exception e, string linea)
        {
            Save($"Error al Momento de Enviar el Correo de aviso de Error a los Ejecutivos - Modulo {linea}" +
                         $"\n{e.Message}");
            TryError(e.Message);
            TryError(e.ToString());
            EmailSender.SendEmailFromProcesosXmlDimerc(
                InternalVariablesPDF.GetMainEmail(),
                null,
                "ERROR INTERNO",
                $"Error al Momento de Enviar el Correo de aviso de Error a los Ejecutivos - Modulo {linea}" +
                         $"\n{e.Message}");
        }

        /// <summary>
        /// Enviar Error de Ordenes con Problemas en Detalle a los Ejecutivos
        /// </summary>
        private static void SendMailOrdenesProcesadasProblemaDetalleToEjecutivo()
        {
            if (!InternalVariablesPDF.SendMailErrorEjecutivos()) return;
            if (InternalVariablesPDF.IsDebug()) return;
            if (OrdenesProcesadasProblemaDetalle.Count == 0) return;
            try
            {
                var lastEjecutivo = "";
                var body = "";
                var first = true;
                foreach (var oc in OrdenesProcesadasProblemaDetalle)
                {
                    oc.EmailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(oc.RutCli.ToString());
                    oc.RutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.RutCli.ToString());
                    oc.Razon = OracleDataAccess.GetRazonSocial(oc.RutCli.ToString());
                }
                foreach (var oc in OrdenesProcesadasProblemaDetalle.OrderBy(o => o.EmailEjecutivo))
                {
                    if (lastEjecutivo.Equals(oc.EmailEjecutivo))
                    {
                        body += $"\t\tOC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}\n";
                    }
                    else
                    {
                        if (first)
                        {
                            first = false;
                        }
                        else
                        {
                            body =
                                "Para las siguientes Ordenes en PDF, no es posible leer correctamente el Detalle del Pedido.\n\n" +
                                $"{body}\n\n" +
                                "Esto probablemente se deba a un cambio en el formato del Archivo Cargado.\n" +
                                "En estos momentos se enviará una copia de este correo al Especialista a cargo.\n" +
                                "Para mayor información, contactarse con Matias Suazo (Matias.suazo@dimec.cl)";
                            EmailSender.
                                SendEmailFromProcesosXmlDimerc(
                                    new[] {oc.EmailEjecutivo}
                                    , InternalVariablesPDF.GetMainEmail()
                                    , "ERROR INTEGRACIÓN ORDENES PDF",
                                    body);
                            body = $"\t\tOC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}\n";
                        }
                        lastEjecutivo = oc.EmailEjecutivo;
                    }
                }
            }
            catch (Exception e)
            {
                ThrowInternalError(e, "SendMailOrdenesProcesadasProblemaDetalleToEjecutivo");
            }
        }



        private static void SendMailOrdenesProcesadasRepetidas()
        {
            if (OrdenesProcesadasRepetidas.Count == 0) return;
            var lastRut = 0;
            var subject = $"Fecha: {DateTime.Now} - Ordenes Repetidas: {OrdenesProcesadasRepetidas.Count}";
            var body = $"Fecha: {DateTime.Now} - Ordenes Repetidas: {OrdenesProcesadasRepetidas.Count}\n";
            var first = true;
            foreach (var oc in OrdenesProcesadasRepetidas.OrderBy(o => o.RutCli))
            {
                oc.EmailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(oc.RutCli.ToString());
                oc.RutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.RutCli.ToString());
                oc.Razon = OracleDataAccess.GetRazonSocial(oc.RutCli.ToString());
                if (!lastRut.Equals(oc.RutCli))
                {
                    lastRut = oc.RutCli;
                    body +=
                        $"Las siguientes Ordenes en PDF se han intentado procesar mas de una vez.{(first ? "\n\n" : "\n\n\n")}Empresa: {oc.Razon}, Rut: {oc.RutCli}, Mail ejecutivo: {oc.EmailEjecutivo}\n\n" +
                        $"OC Cliente: {oc.OcCliente}, CC: {oc.CenCos}, Items: {oc.DetallesCompra.Count}, Total: {oc.DetallesCompra.Sum(it => it.SubTotal).ToString("C0", CultureInfo.CreateSpecificCulture("es-CL"))}\n";
                    first = false;
                }
                else
                {
                    body +=
                        $"OC Cliente: {oc.OcCliente}, CC: {oc.CenCos}, Items: {oc.DetallesCompra.Count}, Total: {oc.DetallesCompra.Sum(it => it.SubTotal).ToString("C0", CultureInfo.CreateSpecificCulture("es-CL"))}\n";
                }

            }
            EmailSender.
               SendEmailFromProcesosXmlDimerc(
                   InternalVariablesPDF.GetMainEmail(),
                   InternalVariablesPDF.IsDebug()
                       ? null
                       : InternalVariablesPDF.GetEmailCcError(),
                   InternalVariablesPDF.IsDebug()
                       ? $"DEBUG {InternalVariablesPDF.GetSubjectDebug()}: {subject}"
                       : subject,
                   body);
            OrdenesProcesadasRepetidas = new List<OrdenCompraIntegracion>();
        }



        private static void SendMailOrdenesProcesadas()
        {
            if (OrdenesProcesadas.Count == 0) return;
            var lastRut = 0;
            var totalProcesado = 0;
            var body = "";
            var first = true;
            foreach (var oc in OrdenesProcesadas.OrderBy(o => o.RutCli))
            {
                oc.EmailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(oc.RutCli.ToString());
                oc.RutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.RutCli.ToString());
                oc.Razon = OracleDataAccess.GetRazonSocial(oc.RutCli.ToString());
                if (!lastRut.Equals(oc.RutCli))
                {
                    lastRut = oc.RutCli;
                    var pop = new Popup.Popup
                    {
                        RutUsuario = oc.RutUsuario
                    };
                    var d = new DetallePopup
                    {
                        RutCli = oc.RutCli.ToString(),
                        Razon = oc.Razon,
                        CantidadOrdenes = 1
                        
                    };
                    //Console.WriteLine($"DETALLE POPUP: {d.ToString()}");
                    pop.AddDetallePopup(d);
                    AddPopupTelemarketing(pop);
                    totalProcesado += (from det in oc.DetallesCompra select det.SubTotal).Sum();
                    body +=
                        $"{(first ? "\n\n" : "\n\n\n")}Empresa: {oc.Razon}, Rut: {oc.RutCli}, Mail ejecutivo: {oc.EmailEjecutivo}\n\n" +
                        $"N° Pedido: {oc.NumPed}, OC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}, Items: {oc.DetallesCompra.Count}, Subtotal: " +
                        $"{(from det in oc.DetallesCompra select det.SubTotal).Sum().ToString("C0",CultureInfo.CreateSpecificCulture("es-CL"))}, Integración: {oc.TipoIntegracion}\n";
                    first = false;
                }
                else
                {
                    var d = new DetallePopup
                    {
                        RutCli = oc.RutCli.ToString(),
                        Razon = oc.Razon,
                        CantidadOrdenes = 1
                    };
                    AddDetallePopupToRutUsuario(oc.RutUsuario, d);
                    totalProcesado += (from det in oc.DetallesCompra select det.SubTotal).Sum();
                    body +=
                        $"N° Pedido: {oc.NumPed}, OC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}, Items: {oc.DetallesCompra.Count}, Subtotal: " +
                        $"{(from det in oc.DetallesCompra select det.SubTotal).Sum().ToString("C0", CultureInfo.CreateSpecificCulture("es-CL"))}, Integración: {oc.TipoIntegracion}\n";
                }
            }
            //Console.WriteLine("==============PROCESADAS================");

            var subject = $"Fecha: {DateTime.Now} - N° Ordenes: {OrdenesProcesadas.Count} - Total Procesado: {totalProcesado.ToString("C0", CultureInfo.CreateSpecificCulture("es-CL"))}";
            body = $"{subject}\n{body}";
            EmailSender.
                SendEmailFromProcesosXmlDimerc(
                    InternalVariablesPDF.GetMainEmail()
                    ,InternalVariablesPDF.IsDebug()
                        ? null
                        : InternalVariablesPDF.GetEmailCc()
                     ,InternalVariablesPDF.IsDebug()
                        ? $"DEBUG {InternalVariablesPDF.GetSubjectDebug()}: {subject}"
                        : subject,
                    body);
            OrdenesProcesadas = new List<OrdenCompraIntegracion>();
        }

        public static void SendMailErrorEjecutivos(ErrorIntegracion integracionError)
        {
            if (InternalVariablesPDF.IsDebug()) return;
            if (!InternalVariablesPDF.SendMailErrorEjecutivos()) return;
            try
            {
                var mailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(integracionError.RutCliente);
                var ejecutivo = OracleDataAccess.GetEjecutivoFromRutCliente(integracionError.RutCliente);
                var razonSocialCliente = OracleDataAccess.GetRazonSocial(integracionError.RutCliente);
                EmailSender.SendEmailFromProcesosXmlDimerc(
                    new[] { mailEjecutivo }
                    , InternalVariablesPDF.GetMainEmail()
                , "ERROR INTEGRACIÓN ORDENES PDF"
                , $"Estimada(o) {ejecutivo}\n\nAl momento de procesar la Orden :{integracionError.OcCliente}, del cliente {razonSocialCliente} (Rut: {integracionError.RutCliente})" +
                  "\nEl sistema de Integración de Ordenes de PDF, arroja el siguiente error:\n\n" +
                  $"\t{integracionError.Mensaje}\n\t" +
                  (integracionError.TipoError == TipoError.ERROR_PRECIO_ITEM
                  || integracionError.TipoError == TipoError.ERROR_CANTIDAD_ITEM
                  ? "Esto probablemente se deba a un cambio en el formato del Archivo Cargado." : "")
                  + "\n\n" +
                  "Para mayor información, contactarse con  (maatias.suazo@dimerc.cl)");
                OracleDataAccess.InsertErrorIntegracionLog(integracionError);
            }
            catch (Exception e)
            {
                ThrowInternalError(e, "SendMailErrorEjecutivos");
            }
        }


        /// <summary>
        /// Envia Mail Pendientes
        /// </summary>
        public static void SendMails()
        {
            SendMailCencosFaltantes();
            SendMailProblemasConversion();
            SendMailSkuFaltantes();
            SendMailOrdenesProcesadas();
            SendPopupTelemarketing();
            SendMailOrdenesProcesadasProblemaDetalle();
            SendMailOrdenesProcesadasRepetidas();
            //SendMailOrdenesProcesadasPareoSkuToEjecutivo();
            //SendMailOrdenesProcesadasPareoCentroCostoToEjecutivo();
        }

        //private static void SendMailOrdenesProcesadasPareoSkuToEjecutivo()
        //{
        //    if (!InternalVariablesPDF.SendMailToEjecutivo() && !InternalVariablesPDF.SendMailDebugNewFlow()) return;
        //    if (InternalVariablesPDF.IsDebug()) return;
        //    if (OrdenesProcesadasFaltaPareoSku.Count == 0) return;
        //    try
        //    {
        //        var lastEjecutivo = "";
        //        var body = "";
        //        var first = true;
        //        foreach (var oc in OrdenesProcesadasFaltaPareoSku)
        //        {
        //            oc.EmailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(oc.RutCli.ToString());
        //            oc.RutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.RutCli.ToString());
        //            oc.Razon = OracleDataAccess.GetRazonSocial(oc.RutCli.ToString());
        //            oc.Hash = OracleDataAccess.GetHashFromOrdenCompra(oc);
        //        }
        //        foreach (var oc in OrdenesProcesadasFaltaPareoSku.OrderBy(o => o.EmailEjecutivo))
        //        {
        //            if (lastEjecutivo.Equals(oc.EmailEjecutivo))
        //            {
        //                body += $"\t\tOC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}, Enlace: {InternalVariablesPDF.GetUrlBasePareoEjecutivo()}{oc.Hash}\n";
        //            }
        //            else
        //            {
        //                //if (first)
        //                //{
        //                //    first = false;
        //                //}
        //                //else
        //                //{
        //                body = $"\n\n\t\tOC Cliente: {oc.OcCliente}, C.C.: {oc.CenCos}, Enlace: {InternalVariablesPDF.GetUrlBasePareoEjecutivo()}{oc.Hash}\n";
        //                body =
        //                        $"Estimada(o): {OracleDataAccess.GetEjecutivoFromRutCliente(oc.RutCli.ToString())} \n" +
        //                        "Para las siguientes Ordenes en PDF, no es posible realizar un pareo de Códigos correctamente.\n" +
        //                        "En el (los) siguiente(s) enlace(s) puede realizar el Pareo de los códigos. Considerar que este proceso se debe realizar solo una vez por producto nuevo o que el sistema no encuentre el Pareo de Códigos\n" +
        //                        $"{body}\n\n" +
        //                        "Esto probablemente se deba a un cambio en la Descripcion/Código del Producto.\n" +
        //                        "En estos momentos se enviará una copia de este correo al Especialista a cargo.\n" +
        //                        "Para mayor información, contactarse con Aaron Pardo (apardo@ofimarket.cl) o al anexo: 8103";
        //                    EmailSender.
        //                        SendEmailFromProcesosXmlDimerc(
        //                            //new[] { InternalVariablesPDF.IsDebug()?
        //                            //    InternalVariablesPDF.GetMainEmail()[0]
        //                            //    :oc.EmailEjecutivo }
        //                            InternalVariablesPDF.GetQaEmailError()
        //                            , InternalVariablesPDF.GetMainEmail()
        //                            , (InternalVariablesPDF.IsDebug() ? $"DEBUG {InternalVariablesPDF.GetSubjectDebug()} - ":"")+
        //                            "INTEGRACIÓN ORDENES PDF - FALTA PAREO DE PRODUCTOS",
        //                            body);
        //                //}
        //                lastEjecutivo = oc.EmailEjecutivo;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        ThrowInternalError(e, "SendMailOrdenesProcesadasPareoSkuToEjecutivo");
        //    }
        //    OrdenesProcesadasFaltaPareoSku = new List<OrdenCompraIntegracion>();


        //}


        private static void SendMailOrdenesProcesadasPareoCentroCostoToEjecutivo()
        {
            if (!InternalVariablesPDF.SendMailToEjecutivo() && !InternalVariablesPDF.SendMailDebugNewFlow()) return;
            if (InternalVariablesPDF.IsDebug()) return;
            if (OrdenesProcesadasFaltaPareoCentroCosto.Count == 0) return;
            try
            {
                var lastEjecutivo = "";
                var body = "";
                var first = true;
                foreach (var oc in OrdenesProcesadasFaltaPareoCentroCosto)
                {
                    oc.EmailEjecutivo = OracleDataAccess.GetEmailFromRutCliente(oc.RutCli.ToString());
                    oc.RutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.RutCli.ToString());
                    oc.Razon = OracleDataAccess.GetRazonSocial(oc.RutCli.ToString());
                    oc.Hash = OracleDataAccess.GetHashFromOrdenCompra(oc);
                }
                foreach (var oc in OrdenesProcesadasFaltaPareoCentroCosto.OrderBy(o => o.EmailEjecutivo))
                {
                    if (lastEjecutivo.Equals(oc.EmailEjecutivo))
                    {
                        body += $"\t\tOC Cliente: {oc.OcCliente}, Enlace: {InternalVariablesPDF.GetUrlBasePareoEjecutivo()}{oc.Hash}\n";
                    }
                    else
                    {
                        if (first)
                        {
                            first = false;
                            body += $"\t\tOC Cliente: {oc.OcCliente}, Enlace: {InternalVariablesPDF.GetUrlBasePareoEjecutivo()}{oc.Hash}\n";
                        }
                        //else
                        //{
                        body =
                                $"Estimada(o): {OracleDataAccess.GetEjecutivoFromRutCliente(oc.RutCli.ToString())} \n" +
                                "Para las siguientes Ordenes en PDF, no es posible realizar un pareo de Centro de Costos correctamente.\n" +
                                "En el (los) siguiente(s) enlace(s) puede realizar el Pareo de los Centros de Costos. " +
                                "Considerar que este proceso se debe realizar solo una vez por Centro de Costo nuevo o " +
                                "que el sistema no encuentre dicho pareo.\n\n" +
                                $"{body}\n\n" +
                                "Esto probablemente se deba a un cambio en la Descripcion/Código del Centro de Costo.\n" +
                                "En estos momentos se enviará una copia de este correo al Especialista a cargo.\n" +
                                "Para mayor información, contactarse con Aaron Pardo (apardo@ofimarket.cl) o al anexo: 8103";
                        var mail = InternalVariablesPDF.GetMainEmail();
                        foreach(var e in InternalVariablesPDF.GetEmailCcError())
                            
                            EmailSender.
                                SendEmailFromProcesosXmlDimerc(
                                    //new[] {
                                    //    InternalVariablesPDF.IsDebug()?
                                    //    InternalVariablesPDF.GetMainEmail()[0]
                                    //    :oc.EmailEjecutivo }
                                    InternalVariablesPDF.GetQaEmailError()
                                    , InternalVariablesPDF.GetMainEmail()
                                    , "INTEGRACIÓN ORDENES PDF - FALTA PAREO DE CENTRO DE COSTOS",
                                    body);
                        //body = $"\t\tN° OC Cliente: {oc.OcCliente}, Enlace: {InternalVariablesPDF.GetUrlBasePareoEjecutivo()}{oc.Hash}\n";
                        //}
                        lastEjecutivo = oc.EmailEjecutivo;
                    }
                }
            }
            catch (Exception e)
            {
                ThrowInternalError(e, "SendMailOrdenesProcesadasPareoCentroCostoToEjecutivo");
            }
            OrdenesProcesadasFaltaPareoCentroCosto = new  List<OrdenCompraIntegracion>();


        }

        /// <summary>
        /// Insertar Popup en Telemarketing
        /// </summary>
        private static void SendPopupTelemarketing()
        {
            if (InternalVariablesPDF.SendPopup())
            {
                foreach (var pop in PopupsEjecutivos)
                {
                    Console.WriteLine($"POPUP: {pop}");
                   // OracleDataAccess.InsertPopupTelemarketing(pop);
                }
            }
            PopupsEjecutivos = new List<Popup.Popup>();
        }

        #endregion


        /// <summary>
        /// Inicializa Variables de Entorno
        /// </summary>
        public static void InitializeVariables()
        {
            CencosFaltantes = new List<MailCencosFaltantes>();
            ProblemaConversionUnidades = new List<MailProblemaConversionUnidades>();
            SkuFaltantes = new List<MailSkuFaltante>();
            OrdenesProcesadas = new List<OrdenCompraIntegracion>();
            OrdenesProcesadasProblemaDetalle = new List<OrdenCompraIntegracion>();
            PopupsEjecutivos = new List<Popup.Popup>();
            OrdenesProcesadasRepetidas = new List<OrdenCompraIntegracion>();
            OrdenesProcesadasFaltaPareoCentroCosto = new List<OrdenCompraIntegracion>();
            OrdenesProcesadasFaltaPareoSku = new List<OrdenCompraIntegracion>();
            CreateFolder();
        }

        /// <summary>
        /// Crea Carpeta de Log si no existe
        /// </summary>
        private static void CreateFolder()
        {
            if (!Directory.Exists(InternalVariablesPDF.GetLogFolder()))
                Directory.CreateDirectory(InternalVariablesPDF.GetLogFolder());
        }
    }
}
