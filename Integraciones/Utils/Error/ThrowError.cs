﻿using System;
using Hardcodet.Wpf.TaskbarNotification;
using Integraciones.Utils.Email;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.View;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Readers.Text;

namespace Integraciones.Utils.Error
{
    public static class ThrowError
    {
        public static void ThrowInsertError(string filename)
        {

            Main.Main.ShowBalloon("Error",
                "Ha ocurrido un error al momento de insertar " +
                $"las Ordenes de Compra del archivo {filename}",
                BalloonIcon.Error);
            Log.Log.Save("Error",
                "Ha ocurrido un error al momento de insertar " +
                $"las Ordenes de Compra del archivo {filename}");
            EmailSender.SendEmailFromProcesosXmlDimerc(
                InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Error de Inserción de Datos",
                "Ha ocurrido un error al momento de insertar " +
                $"las Ordenes de Compra del archivo {filename}");
        }

        public static void ThrowAnalysisTextError(TextReader txtReader, Exception e)
        {
            Log.Log.Save("Ha ocurrio un error en el analisis de las Ordenes de Compra...");
            Log.Log.Save($"Archivo de Orden de Compra: {txtReader.TxtFileName}");
            Main.Main.ShowBalloon("Error",
                "Ha ocurrido un error al momento de Procesar " +
                $"el archivo: {txtReader.TxtFileName}, " +
                "para mayor información, revisar el archivo de Registro de Errores.",
                BalloonIcon.Error);
            Log.Log.TryError(e.Message);
            Log.Log.TryError(e.ToString());
            EmailSender.SendEmailFromProcesosXmlDimercWithAttachments(
                InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Error Procesado TXT: Error al momento de Procesar",
                "Ha ocurrido un error al momento de Procesar " +
                $"el archivo: {txtReader.TxtFileName}, " +
                "para mayor información, revisar el archivo de Registro de Errores."
                , new[]
                {
                    txtReader.TxtPath
                });
            InternalVariables.InternalVariables.AddCountError(txtReader.TxtFileName);
        }


        public static void ThrowAnalysisError(PDFReader pdfReader, Exception e)
        {
            
            Log.Log.Save("Ha ocurrio un error en el analisis de las Ordenes de Compra...");
            Log.Log.Save($"Archivo de Orden de Compra: {pdfReader.PdfFileName}");
            Main.Main.ShowBalloon("Error",
                "Ha ocurrido un error al momento de Procesar " +
                $"el archivo: {pdfReader.PdfFileName}, " +
                "para mayor información, revisar el archivo de Registro de Errores.",
                BalloonIcon.Error);
            Log.Log.TryError(e.Message);
            Log.Log.TryError(e.ToString());
            EmailSender.SendEmailFromProcesosXmlDimercWithAttachments(
                InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Error Procesado PDF: Error al momento de Procesar",
                "Ha ocurrido un error al momento de Procesar " +
                $"el archivo: {pdfReader.PdfFileName}, " +
                "para mayor información, revisar el archivo de Registro de Errores."
                , new[]
                {
                    pdfReader.PdfPath
                });
            InternalVariables.InternalVariables.AddCountError(pdfReader.PdfFileName);
        }
        public static void ThrowAnalysisErrorExcel(ExcelReader excelReader, Exception e)
        {
            Log.Log.Save("Ha ocurrio un error en el analisis de las Ordenes de Compra...");
            Log.Log.Save($"Archivo de Orden de Compra: {excelReader.ExcelFileName}");
            Main.Main.ShowBalloon("Error",
                "Ha ocurrido un error al momento de Procesar " +
                $"el archivo: {excelReader.ExcelFileName}, " +
                "para mayor información, revisar el archivo de Registro de Errores.",
                BalloonIcon.Error);
            Log.Log.TryError(e.Message);
            Log.Log.TryError(e.ToString());
            EmailSender.SendEmailFromProcesosXmlDimercWithAttachments(
                InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Error Procesado Excel: Error al momento de Procesar",
                "Ha ocurrido un error al momento de Procesar " +
                $"el archivo: {excelReader.ExcelFileName}, " +
                "para mayor información, revisar el archivo de Registro de Errores."
                , new[]
                {
                    excelReader.ExcelPath
                });
            InternalVariables.InternalVariables.AddCountErrorExc(excelReader.ExcelFileName);
        }

        public static void ThrowFormatError(string pdfFileName, string pdfPath)
        {
            Log.Log.Save($"El formato del PDF: {pdfFileName}, " +
                         "no es posible" +
                     " reconocerlo...");
            EmailSender.SendEmailFromProcesosXmlDimercWithAttachments(
               InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Formato de PDF Desconocido", $"El Formato del PDF: {pdfFileName}, " +
                                       "no es posible reconocerlo, favor de Revisar...",
                new[]
                {
                    pdfPath
                });
        }

        public static void ThrowFormatErrorAdjunto(string pdfFileName, string pdfPath)
        {
            Log.Log.Save($"El formato del PDF: {pdfFileName}, " +
                         "no es posible" +
                     " reconocerlo...");
            EmailSender.SendEmailFromProcesosXmlDimercWithAttachments(
               InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Formato de PDF Desconocido", $"El Formato del PDF: {pdfFileName}, " +
                                       "no es posible reconocerlo, favor de Revisar...",
                new[]
                {
                    pdfPath
                });
        }

        public static void SendAlertError()
        {
            EmailSender.SendEmailFromProcesosXmlDimerc(
                InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Alerta de Error Consecutivo",
                "Han ocurrido más de 3 errores consecutivos con " +
                $"el archivo: {InternalVariables.InternalVariables.LastFilePathError}. " +
                "Por motivos de seguridad, el sistema no seguirá procesando los ficheros.");
        }


    }
}