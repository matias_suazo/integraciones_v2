﻿namespace Integraciones.Utils.Error
{
    public class ErrorIntegracion : System.Exception
    {
        public TipoError TipoError { get; set; }
        public string Mensaje { get; set; }

        public string RutCliente { get; set; }

        public string OcCliente { get; set; }

        public string FileName { get; set; }

        public ErrorIntegracion(TipoError tipo, string mensaje, string rutCli, string ocCliente, string fileName)
        {
            TipoError = tipo;
            Mensaje = mensaje;
            RutCliente = rutCli;
            OcCliente = ocCliente;
            FileName = fileName;
        }
        public ErrorIntegracion(TipoError tipo, string rutCli, string ocCliente, string fileName)
        {
            TipoError = tipo;
            var m = "";
            switch (tipo)
            {
                case TipoError.FALTA_VALIDAR_PAREO_Y_MULTIPLOS:
                    m =
                        $"Para la Integración del Rut: {rutCli} falta validar el Pareo de Códigos Generado y los Multiplos de los Productos";
                    break;
                case TipoError.FALTA_VALIDAR_MULTIPLOS:
                    m = $"Para la Integración del Rut: {rutCli} falta validar los Multiplos de los Productos";
                    break;
                case TipoError.FALTA_VALIDAR_PAREO_CODIGO:
                    m = $"Para la Integración del Rut: {rutCli} falta validar el Pareo de Códigos Generado";
                    break;
                case TipoError.INTEGRACION_NO_TERMINADA:
                    m = $"La Integración del Rut: {rutCli} aún no está Terminada";
                    break;
            }
            Mensaje = m;
            RutCliente = rutCli;
            OcCliente = ocCliente;
            FileName = fileName;
        }
    }

    public enum TipoError
    {
        ERROR_CANTIDAD_ITEM = 0,
        ERROR_PRECIO_ITEM = 1,
        ERROR_ORDEN_REPETIDA  = 2,
        ERROR_LECTURA_META_DATA = 3,
        FALTA_VALIDAR_PAREO_CODIGO = 4,
        FALTA_VALIDAR_MULTIPLOS = 5,
        FALTA_VALIDAR_PAREO_Y_MULTIPLOS = 6,
        INTEGRACION_NO_TERMINADA = 7
    }
}