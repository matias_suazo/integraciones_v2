﻿namespace Integraciones.Utils.OrdenCompra
{
    public class Item
    {
        public Item()
        {
            Sku = "";
            Descripcion = "";
            Cantidad = "";
            Precio = "";
            TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
        }

        public Item Clone()
        {
            return new Item
            {
                Sku = Sku,
                Descripcion = Descripcion,
                Cantidad = Cantidad,
                Precio = Precio,
                TipoPareoProducto = TipoPareoProducto,
                TipoPrecioProducto = TipoPrecioProducto
            };
        }

        public TipoPrecioProducto TipoPrecioProducto { get; set; }

        public TipoPareoProducto TipoPareoProducto { get; set; }

        public string Sku { get; set; }

        public string Descripcion { get; set; }

        public string Cantidad
        {
            get { return _cantidad.Replace(".", ""); }
            set { _cantidad = value; }
        }
        private string _cantidad;

        public string SubTotal
        {
            get
            {
                var precio = 0;
                int.TryParse(Precio, out precio);
                var cantidad = 0;
                int.TryParse(Cantidad, out cantidad);
                return (cantidad*precio).ToString();
            }
        }

        public string Precio
        {
            get { return _precio.Replace(".", "").Split(',')[0]; }
            set { _precio = value; }
        }
        
        private string _precio;
             
        public override string ToString()
        {
            return $"Sku: {Sku}, Cantidad: {Cantidad}, Precio: {Precio}, Descripción: {Descripcion}, TipoPrecio:{TipoPrecioProducto}, TipoPareo: {TipoPareoProducto}";
        }

    }

    /// <summary>
    /// Tipo de Pareo de Producto
    /// </summary>
    public enum TipoPareoProducto
    {
        SIN_PAREO = 0,
        PAREO_CODIGO_CLIENTE = 1,
        PAREO_DESCRIPCION_TELEMARKETING = 2,
        PAREO_DESCRIPCION_CLIENTE = 3,
        PAREO_SKU_CLIENTE_DESCRIPCION_TELEMARKETING = 4,
        PAREO_CODIGO_CLIENTE_O_CODIGO_DIMERC = 5,
        PAREO_DESCRIPCION_HISTORICO_COMPRAS_CON_CODIGO_CLIENTE = 6,
        INSERT_PAREO_DESCRIPCION_HISTORICO_COMPRAS_CON_CODIGO_CLIENTE = 7,
        PAREO_DESCRIPCION_TELEMARKETING_PRODUCTOS_ACTIVOS = 8,
        PAREO_DESCRIPCION_HISTORICO_COMPRAS_CLIENTE = 9,
        PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE = 10,
        PAREO_CODIGO_UNI = 11,
        INSERTAR_PAREO_BDD = 99
    }

    public enum TipoPrecioProducto
    {
        TELEMARKETING = 0,
        ARCHIVO_ADJUNTO = 1
    }

    public class ItemCarozzi : Item
    {
        public double PrecioDecimal { get; set; }
        public string SubTotal { get; set; }
        public override string ToString()
        {
            return base.ToString() + $", SubTotal: {SubTotal}";
        }

    }
    
    public class ItemSecuritas : Item
    {
        public string CodigoProyectoSecuritas { get; set; }
        public override string ToString()
        {
            return base.ToString() + $", Codigo Proyecto: {CodigoProyectoSecuritas}";
        }

    }
    
    public class ItemDavila : Item
    {
        public ItemDavila() : base()
        {
            Descripcion = "";

        }
        
        public string Descripcion
        {
            get { return _descripcion.Replace("'", "''"); }
            set { _descripcion = value; }
        }

        private string _descripcion;
        public override string ToString()
        {
            return base.ToString() + $", Descripción: {Descripcion}";
        }
    }
}