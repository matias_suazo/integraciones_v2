﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.OrdenCompra.Integracion
{
    public class OrdenCompraIntegracionPrv
    {
        /*public OrdenCompraIntegracionPrv()
        {
            DetallesCompraPrv = new List<DetalleOrdenCompraIntegracionPrv>();
        }

        private CodigoEmpresa codigoEmpresa { get; set; }

        public CodigoEmpresa CodigoEmpresa
        {
            get
            {
                return codigoEmpresa == 0 ? CodigoEmpresa.DIMERC : codigoEmpresa;
            }
            set { codigoEmpresa = value; }
        }

        public TipoIntegracion TipoIntegracion { get; set; }

        public string NumPed { get; set; }

        public int RutCli { get; set; }
        public int RutCliPrv { get; set; }

        public string CenCos { get; set; }

        public string Direccion { get; set; }

        public string OcCliente { get; set; }

        public string Observaciones { get; set; }

        public List<DetalleOrdenCompraIntegracionPrv> DetallesCompraPrv { get; set; }

        public string Razon { get; set; }

        public string RutUsuario { get; set; }

        public string EmailEjecutivo { get; set; }

        public EstadoOrden EstadoOrden { get; set; }

        public WarningOrden WarningOrden
        {
            get
            {
                return DetallesCompra.Any(detale => detale.WarningDetalle != WarningDetallePrv.SIN_WARNING) ? WarningOrden.CON_WARNING : WarningOrden.SIN_WARNING;
            }

        }

        public string Hash { get; set; }

        public TipoFlujo Flujo { get; set; }


        private ErrorOrden _errorOrden { get; set; }

        public ErrorOrden ErrorOrden
        {
            get
            {
                return DetallesCompra.Any(detale => detale.ErrorDetalle != ErrorDetallePrv.SIN_ERROR) ? ErrorOrden.CON_ERROR : ErrorOrden.SIN_ERROR;
            }
            set { _errorOrden = value; }
        }



        public void AddDetalleCompra(DetalleOrdenCompraIntegracionPrv detPrv)
        {
            DetallesCompra.Add(detPrv);
        }

        public override string ToString()
        {
            var cont = 1;
            var items = DetallesCompra.Aggregate("", (current, item) => current + $"{cont++}.-{item}\n");
            return $"Flujo: {Flujo}, NumPed: {NumPed},Rut: { RutCli},RutCliPrv: {RutCliPrv} N° Compra: { OcCliente}, " +
                   $"Centro Costo: { CenCos}, Estado Orden: {EstadoOrden}, Estado Warning: {WarningOrden}, " +
                   $"Estado Error: {ErrorOrden}, Observaciones: { Observaciones}, Dirección: { Direccion}, " +
                   $"\nCodigo Empresa:{CodigoEmpresa}, TipoIntegración: {TipoIntegracion}\nItems:\n{ items}";
        }

        public bool EqualsOrdenCompraProcesada(OrdenCompraIntegracionPrv oc)
        {
            if (oc == null) return false;
            if (DetallesCompra.Count != oc.DetallesCompra.Count) return false;
            if (DetallesCompra.Where((t, i) => !t.EqualDetalleProcesado(oc.DetallesCompra[i])).Any())
            {
                return false;
            }
            return CenCos.Equals(oc.CenCos) && RutCli.Equals(oc.RutCli) && RutCliPrv.Equals(oc.RutCliPrv)&& Direccion.Equals(oc.Direccion) && Observaciones.Equals(oc.Observaciones) && OcCliente.Equals(oc.OcCliente);
        }

        public static string getTipoByEnum(int num)
        {
            string ret = "";
            switch (num)
            {
                case 0: ret = "PDF"; break;
                case 1: ret = "WORD"; break;
                case 2: ret = "EXCEL"; break;
                case 3: ret = "MAIL"; break;
                case 4: ret = "CARGA_ESTANDAR"; break;
                case 5: ret = "PDF_ESCANEADO"; break;
                case 6: ret = "TEXT"; break;
                case 7: ret = "PAINT"; break;
                case 8: ret = "BRAILLE"; break;
                case 10: ret = "CARGA_ESTANDAR_PROVEEDOR"; break;
                default: ret = "NO DETECTADO"; break;
            }
            return ret;
        }


    }
    public enum EstadoOrdenPrv
    {
        POR_PROCESAR = 0,
        PROCESADO = 1,
        ORDEN_REPETIDA = 2,
        CENTRO_COSTO_NO_PAREADO = 3,
        SKU_NO_PAREADO = 4,
        DEBUG = 5,
        NO_EXISTE_CLIENTE_SAP = 6,
        NO_EXISTE_CCOSTO_SAP = 7
    }

    public enum WarningOrdenPrv
    {
        SIN_WARNING = 0,
        CON_WARNING = 1
    }

    public enum ErrorOrdenPrv
    {
        SIN_ERROR = 0,
        CON_ERROR = 1
    }

    public enum TipoFlujoPrv
    {
        FLUJO_ANTIGUO = 0,
        FLUJO_NUEVO = 1
    }

    public enum CodigoEmpresaPrv
    {
        DIMERC = 3,
        OFIMARKET = 6
    }



    //public enum TipoIntegracion
    //{
    //    TELEMARKETING = 0,
    //    WEB_1 = 1,
    //    WEB_2 = 2,
    //    INTEGRACION = 3,
    //    ROBOT_CHILE_COMPRA = 4,
    //    PDF = 5,
    //    OFIMARKET = 6,
    //    CARGA_ESTANDAR = 7,
    //    ICONSTRUYE = 8,
    //    SENEGOCIA = 9,
    //    MAIL = 10,
    //    CAMPANAS_WEB = 11,
    //    EXCEL = 12,
    //    PDF_ESCANEADO = 13,
    //    POWER_POINT = 14,
    //    PAINT = 15,
    //    BRAILLE = 16,
    //    WORD = 17
    //}

    public enum TipoIntegracionPrv
    {
        PDF = 0,
        WORD = 1,
        EXCEL = 2,
        MAIL = 3,
        CARGA_ESTANDAR = 4,
        PDF_ESCANEADO = 5,
        TEXT = 6,
        PAINT = 7,
        BRAILLE = 8,
        COTIZACION = 9,
        CARGA_ESTANDAR_PROVEEDOR = 10
        */
    }

}
