﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.OrdenCompra.Integracion
{
        public class DetalleOrdenCompraIntegracionPrv
        {
        public int ID { get; set; }
        public string NumPed { get; set; }

        public string SkuDimerc { get; set; }

        public int Cantidad { get; set; }

        public int Precio { get; set; }
        public int SubTotal { get; set; }

        public TipoBodegaPrv CodigoBodegaPrv = TipoBodegaPrv.SANTIAGO;

        public override string ToString()
        {
            return $"NumPed: {NumPed}, SKU: {SkuDimerc}, Cantidad: {Cantidad}, " +
                   $"Precio: {Precio}, SubTotal: {SubTotal}, Estado Pareo: {EstadoPareoDetalle}, " +
                   $"Estado Warning: {WarningDetalle}, Estado Error: {ErrorDetalle}, " +
                   $"Bodega: {CodigoBodegaPrv}";
        }

        public EstadoPareoDetallePrv EstadoPareoDetalle { get; set; }

        public WarningDetallePrv WarningDetalle { get; set; }

        public ErrorDetallePrv ErrorDetalle { get; set; }

        public bool EqualDetalleProcesado(DetalleOrdenCompraIntegracionPrv det)
        {
            return SkuDimerc.Equals(det.SkuDimerc) && Cantidad == det.Cantidad && Precio == det.Precio;
        }
    }

    public enum TipoBodegaPrv
    {
        SANTIAGO = 1,
        ANTOFAGASTA = 66
    }
    public enum EstadoPareoDetallePrv
    {
        SKU_PAREADO = 0,
        SKU_NO_PAREADO = 1,
        SKU_PAREADO_AUTOMATICAMENTE = 2,
        SKU_NO_EXISTE = 3,
        NO_ENCUENTRA_DESCRIPCION_PRODUCTO = 4,
        NO_ENCUENTRA_PAREO_AUTOMATICO = 5,
    }

    public enum WarningDetallePrv
    {
        SIN_WARNING = 0,
        POSIBLE_ERROR_MULTIPLOS = 1,
        POSIBLE_ERROR_CANTIDAD = 2,
        POSIBLE_ERROR_PRECIO = 3,
        PRECIO_MAYOR_QUE_PDF = 4,
        PRECIO_MENOR_QUE_PDF = 5,
    }

    public enum ErrorDetallePrv
    {
        SIN_ERROR = 0,
        ERROR_TOTAL_PRODUCTO = 1,
        ERROR_CANTIDAD_PRODUCTO = 2,
        SKU_NO_ENCONTRADO_EN_ORDEN = 3
    }

    public enum TipoPareoReCodCliPrv
    {
        PAREO_MANUAL = 0,
        PAREO_HISTORIAL_CLIENTE = 1,
        PAREO_PRODUCTOS_ACTIVOS = 2,
        PAREO_PRODUCTOS_MAESTRA = 3,
        EJECUTIVO_DEBE_PAREAR_CODIGO = 4,
        PAREO_POR_SKU_DIMERC = 5,
        PAREO_EXISTENTE_DESCRIPCION_DIFERENTE = 6
    }

}
