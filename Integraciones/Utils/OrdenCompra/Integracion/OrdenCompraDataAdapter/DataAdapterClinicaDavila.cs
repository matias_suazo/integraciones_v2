﻿using Integraciones.Utils.Oracle.DataAccess;

namespace Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter
{
    public static class DataAdapterClinicaDavila
    {
        public static OrdenCompraIntegracion AdapterClinicaDavilaFormatToCompraIntegracion(this OrdenCompraClinicaDavila oc)
        {
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            var cencos = OracleDataAccess.GetCenCosFromRutClienteAndDescCencos(oc.NumeroCompra, oc.Rut, oc.CentroCosto.DeleteAcent().Trim(), true);
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.CentroCosto,
                CenCos = cencos,
                Direccion = oc.Direccion,
                EstadoOrden = estadoOrden
            };

            foreach (var it in oc.ItemsClinicaDavila)
            {
                var estadoDeltalle = EstadoPareoDetalle.SKU_PAREADO;
                var sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                if (sku.Equals("Z446482")) estadoDeltalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                var empresa = "3";
                var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, ret.CenCos, sku,empresa);
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var precio = int.Parse(pConv);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? int.Parse(it.Precio)
                        : precio, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precio) / multiplo,
                    SkuDimerc = sku,
                    EstadoPareoDetalle = estadoDeltalle
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }

        public static OrdenCompraIntegracion AdapterClinicaDavilaFormatToCompraIntegracionWithMatchCencos(this OrdenCompraClinicaDavila oc)
        {
            //Console.WriteLine($"CC: {oc.CentroCosto}, ocHX: {oc.CentroCosto.ConvertStringToHex()}\n");
            //oc.CentroCosto = oc.CentroCosto.ConvertStringToHex().Replace("c2", "").ConvertHexToString();
            if(oc.CentroCosto.Contains("RECUPERACION")
                && oc.CentroCosto.Contains("SECUNDARIA")
                && oc.CentroCosto.Contains("4")
                && oc.CentroCosto.Contains("PISO")
                && oc.CentroCosto.Contains("EDIF"))
            {
                oc.CentroCosto = "RECUPERACION SECUNDARIA 4 PISO EDIF G";
            }else if (oc.CentroCosto.Contains("LAB")
                && oc.CentroCosto.Contains("CENTRAL")
                && oc.CentroCosto.Contains("CITROMETRIA")
                && oc.CentroCosto.Contains("FLUJO")
                && oc.CentroCosto.Contains("DE"))
            {
                oc.CentroCosto = "LAB CENTRAL - CITROMETRIA DE FLUJO";
            }

            //LAB CENTRAL - CITROMETRIA DE FLUJO
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            bool flagSap = true;
            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                flagSap = false;
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }
            
            var cencos = OracleDataAccess.GetCenCosFromRutClienteAndDescCencos(oc.NumeroCompra, oc.Rut, oc.CentroCosto.DeleteAcent().Trim(), true);
            if (flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
            }
                var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.CentroCosto,
                CenCos = cencos,
                Direccion = oc.Direccion,
                EstadoOrden = estadoOrden
            };
            DataAdapterGeneric.SumarIguales(oc.Items);
            foreach (var it in oc.ItemsClinicaDavila)
            {
                var estadoDeltalle = EstadoPareoDetalle.SKU_PAREADO;
                var sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                if (sku.Equals("Z446482")) estadoDeltalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                var empresa = "3";
                var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, ret.CenCos, sku, empresa);
                var precio = int.Parse(pConv);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = sku.Equals("Z446482") ? int.Parse(it.Cantidad)
                    : int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? int.Parse(it.Precio)
                        : precio, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precio) / multiplo,
                    SkuDimerc = sku,
                    EstadoPareoDetalle = estadoDeltalle
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }
    }
}