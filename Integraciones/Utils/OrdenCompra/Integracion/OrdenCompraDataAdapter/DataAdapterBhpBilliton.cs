﻿using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;

namespace Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter
{
    public static class DataAdapterBhpBilliton
    {
        public static OrdenCompraIntegracion AdapterBhpBillitonFormatToCompraIntegracion(this Integraciones.Utils.OrdenCompra.OrdenCompra oc)
        {
            return oc.AdapterGenericFormatToCompraIntegracion();

        }
    }
}