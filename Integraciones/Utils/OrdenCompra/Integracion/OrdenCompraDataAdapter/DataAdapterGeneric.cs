﻿using System;
using System.Text.RegularExpressions;
using Integraciones.Utils.Error;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Readers.Word;
using Integraciones.Utils.Readers.Text;
using Integraciones.Utils.Respuesta;
using System.Collections.Generic;

namespace Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter
{
    public static class DataAdapterGeneric
    {
        /// <summary>
        /// Carga estandar para proveedores
        /// </summary>
        /// <param name="oc"></param>
        /// <returns></returns>
        public static OrdenCompraIntegracion AdapterGenericFormatToCompraIntegracion(this OrdenCompra oc)
        {
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            bool flagSap = true;

            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                flagSap = false;
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }
            var cencos = OracleDataAccess.GetCenCosFromRutCliente(oc.NumeroCompra, oc.Rut, oc.CentroCosto);
            if(flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
            }
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = cencos,
                Direccion = oc.Direccion,
                EstadoOrden = estadoOrden
            };
            SumarIguales(oc.Items);
            foreach (var it in oc.Items)
            {
                
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")
                    ))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                var sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                if (!OracleDataAccess.ExistProduct(sku))
                {
                    sku = "Z446482";
                }
                if (sku.Equals("Z446482")) estadoDetalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                var empresa = "3";
                var precioTlmk = 0;
                var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                {
                    sku = "Z446482";
                    precioTlmk = 1;
                }
                else
                {
                    precioTlmk = int.Parse(PrecioUltimate);
                }
                if (precioTlmk.Equals("999999"))
                {
                    precioTlmk = 1;
                    sku = "Z446482";
                }
                //var pConvenioN = OracleDataAccess.GetPrecioProductoConvenio(it.Sku, oc.Rut);
                //var pSap = OracleDataAccess.GetPrecioProducto1(empresa, sku, oc.Rut);//get precio nuevo
                //var pPreciosProd = OracleDataAccess.GetPrecioProducto(oc.Rut, oc.CentroCosto, sku, empresa);
                
                //if (!pConvenioN.Equals(0) || !pConvenioN.Equals(null))
                //{
                //    precioTlmk = int.Parse(pConvenioN);
                //
                //    if (precioTlmk.Equals(0) || precioTlmk.Equals(null))
                //    {
                //        precioTlmk = int.Parse(pSap);
                //
                //        if (precioTlmk.Equals(0) || precioTlmk.Equals(null))
                //        {
                //            precioTlmk = int.Parse(pPreciosProd);
                            
                //        }
                //    }
                //}
                /* Cambios 12-08-2021 Matias Suazo
                var precio = 0;
                //var pConvenio = OracleDataAccess.GetPrecioConvenio
                var pConvenio = OracleDataAccess.GetPrecioProductoConvenio(sku, oc.Rut);
                var pConv = OracleDataAccess.GetPrecioProducto1(empresa, sku, oc.Rut);
                if (!pConvenio.Equals("0"))
                {
                    precio = int.Parse(pConvenio);
                } else
                {
                    if (pConvenio.Equals("0"))
                    {
                        precio = int.Parse(pConv);
                    }
                }
                */

                it.Precio = precioTlmk.ToString();
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? int.Parse(it.Precio)
                        : precioTlmk, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precioTlmk) / multiplo,
                    SkuDimerc = sku,
                    EstadoPareoDetalle = estadoDetalle
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }
        
        

        /// <summary>
        /// Convierte una Orden de Compra en una Orden de Integración
        /// </summary>
        /// <param name="oc"></param>
        /// <returns></returns>
        public static OrdenCompraIntegracion AdapterGenericFormatWithSkuToCompraIntegracion(this OrdenCompra oc)
        {
            Console.WriteLine($"CC: {oc.CentroCosto}");
            var auxint = 0;
            bool flagSap = true;            
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                flagSap = false;
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }

            var cencos = int.TryParse(oc.CentroCosto, out auxint)
                ? oc.CentroCosto
                : OracleDataAccess.GetCenCosFromRutCliente(oc.NumeroCompra, oc.Rut, oc.CentroCosto);
            if(flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
            }
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = cencos,
                Direccion = oc.Direccion,
                EstadoOrden = estadoOrden
            };
            SumarIguales(oc.Items);
            foreach (var it in oc.Items)
            {
                
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")
                    ))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                var existSku = OracleDataAccess.ExistProduct(it.Sku);
                var precio = int.Parse(it.Precio);
                if (existSku)
                {
                    var empresa = "3";
                    var precioTlmk = precio;
                    var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, it.Sku);
                    if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                    {
                        it.Sku = "Z446482";
                        precioTlmk = 1;
                    }
                    else
                    {
                        precioTlmk = int.Parse(PrecioUltimate);
                    }

                    it.Precio = precioTlmk.ToString();
                }
                else
                {
                    it.Sku = "Z446482";
                    estadoDetalle = EstadoPareoDetalle.SKU_NO_EXISTE;
                }

                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad),
                    Precio = precio, //int.Parse(it.Precio),
                    SubTotal = int.Parse(it.Cantidad) * precio,
                    SkuDimerc = it.Sku,
                    EstadoPareoDetalle = estadoDetalle
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }

        
        /// <summary>
        /// Busca el Pareo de SKU y hace Match con Descripción de Centro de Costo
        /// </summary>
        // <param name="oc"></param>
        /// <returns></returns>
        public static OrdenCompraIntegracion AdapterGenericFormatWithDescriptionCencosToCompraIntegracion(this OrdenCompra oc)
        {
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = OracleDataAccess.GetCenCosFromRutClienteAndDescCencosWithMatch(oc.NumeroCompra, oc.Rut, oc.CentroCosto, estadoOrden),
                Direccion = oc.Direccion,
                EstadoOrden = estadoOrden
            };
            SumarIguales(oc.Items);
            foreach (var it in oc.Items)
            {
                
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")
                    ))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                if (sku.Equals("Z446482")) estadoDetalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                var empresa = "3";
                var precioTlmk = 0;
                var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                if (PrecioUltimate.Equals(0) || PrecioUltimate.Equals(null))
                {
                    sku = "Z446482";
                    precioTlmk = 1;
                }

                it.Precio = precioTlmk.ToString();
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? int.Parse(it.Precio)
                        : precioTlmk, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precioTlmk) / multiplo,

                    //Precio = precio, //int.Parse(it.Precio),
                    //SubTotal = (int.Parse(it.Cantidad) * precio)/multiplo,
                    SkuDimerc = sku,
                    EstadoPareoDetalle = estadoDetalle
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }

        public static OrdenCompraIntegracion TraspasoUltimateIntegracion(this OrdenCompra oc
            , PDFReader pdfReader = null, TextReader textReader = null)
        {
            CompraAux compraAux = new CompraAux();
            if (pdfReader != null)
            {
                compraAux.archivo = pdfReader.PdfFileName;
                compraAux.cencos_cliente = oc.CentroCosto;
            }                     

            var cencos = "";
            bool flagSap = true;
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                flagSap = false;
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }
            cencos = oc.CentroCosto;
            if ((!oc.Rut.Equals("99231000") && (!oc.Rut.Equals("76183638")) && (!oc.Rut.Equals("76350666")) && (!oc.Rut.Equals("76114143") && 
                (!oc.Rut.Equals("79705390")))))
            {
                cencos = oc.CentroCosto.Replace("-", " ").Trim();
            }
            
            
            switch (oc.TipoPareoCentroCosto)
            {
                case TipoPareoCentroCosto.SIN_PAREO:
                    cencos = oc.CentroCosto;
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH:
                    cencos = OracleDataAccess.GetCenCosFromRutClienteAndDescCencosWithMatch(oc.NumeroCompra, oc.Rut, cencos, estadoOrden);
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA:
                    cencos = OracleDataAccess.GetCenCosFromRutCliente(oc.NumeroCompra, oc.Rut, cencos);
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE:
                    cencos = OracleDataAccess.GetCenCosFromRutClienteAndDescCencos(oc.NumeroCompra, oc.Rut, cencos, true);
                    if (cencos.Equals("-1")){
                        cencos = OracleDataAccess.getCencosByDescripcionDeCliente(oc.Rut, oc.CentroCosto);
                        if (cencos.Equals("-1") || cencos.Equals(""))
                        {
                            if (oc.Rut.Equals("60503000"))
                            {                            
                                cencos = "0";
                            }
                        }
                    }
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE:
                    cencos = OracleDataAccess.getCencosByDescripcionDeCliente(oc.Rut,oc.CentroCosto);
                    break;
                case TipoPareoCentroCosto.PAREO_CCOSTO_CONTACTO:
                    cencos = OracleDataAccess.GetCencosByCCostoContacto(oc.Rut, oc.CentroCosto);
                    break;
                case TipoPareoCentroCosto.PAREO_CCOSTO_TATA:
                    var cc = oc.CentroCosto;
                    //cencos = OracleDataAccess.getCencosByDescripcionDeClienteTATA(oc.Rut, cc);
                    cencos = OracleDataAccess.getCencosByDescripcionDeClienteTATA(oc.Rut, oc.CentroCosto).Cencos;
                    break;
                case TipoPareoCentroCosto.PAREO_CCOSTO_HDI:
                    Console.WriteLine("PAREO HDI");
                    cencos = OracleDataAccess.getCencosByDescripcionDeClienteHDI(oc.Rut, oc.CentroCosto).Cencos;
                    //cencos = OracleDataAccess.GetCencosByCCostoContactoHDI(oc.Rut, oc.CentroCosto);
                    break;
                case TipoPareoCentroCosto.PAREO_CCOSTO_CONTACTO_HDI:
                    Console.WriteLine("PAREO HDI CONTACTO");
                    //cencos = OracleDataAccess.getCencosByDescripcionDeClienteHDI(oc.Rut, oc.CentroCosto).Cencos;
                    cencos = OracleDataAccess.GetCencosByCCostoContactoHDI(oc.Rut, oc.CentroCosto);
                    if (cencos.Equals("-1"))
                    {
                        cencos = OracleDataAccess.getCencosByDescripcionDeClienteHDIContacto(oc.Rut, oc.CentroCosto);
                    }
                    break;
                case TipoPareoCentroCosto.PAREO_DESCCO_CONTACTO_TLMK:
                    //var cc = oc.CentroCosto;
                    //cencos = OracleDataAccess.getCencosByDescripcionDeClienteTATA(oc.Rut, cc);
                    cencos = OracleDataAccess.getCencosByDesccoContac(oc.Rut, oc.CentroCosto);
                    break;
                case TipoPareoCentroCosto.PAREO_GILDEMEISTER:
                    cencos = OracleDataAccess.getCencosByDesccoContac(oc.Rut, oc.CentroCosto);
                    if (cencos.Equals(""))
                    {
                        cencos = OracleDataAccess.GetCencosByCCostoContacto(oc.Rut, oc.CentroCosto);
                    }
                    if (cencos.Equals("-1")) 
                    {
                        cencos = OracleDataAccess.getCencosByDescripcionDeCliente(oc.Rut, oc.CentroCosto.Split('-')[0]);
                    }
                    break;
                case TipoPareoCentroCosto.PAREO_COTIZACION:
                    cencos = OracleDataAccess.GetCencosByCotizac(oc.Rut,oc.NumeroCompra);
                    break;
                case TipoPareoCentroCosto.PAREO_RE_DDESCLI:
                    cencos = OracleDataAccess.GetCencosByRe_ddescli(oc.Rut, oc.CentroCosto);
                    break;

            }

            if (flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
            }

             var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                EstadoOrden = estadoOrden,
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = cencos,
                Flujo = TipoFlujo.FLUJO_ANTIGUO,
                Direccion = oc.Direccion,
                TipoIntegracion = oc.TipoIntegracion
            };
            if (ret.RutCli == 805368002)
            {
                ret.RutCli = 80536800;
            }
            SumarIguales(oc.Items);

            foreach (var it in oc.Items)
            {
                
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && (Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")) /*|| oc.Rut.Equals("89853600")*/))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                    



                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                var sku = it.Sku.ToUpper().DeleteSymbol();
                switch (it.TipoPareoProducto)
                {
                    case TipoPareoProducto.SIN_PAREO:
                        sku = it.Sku;
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_CLIENTE:
                        sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING:
                        sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_DESCRIPCION_PRODUCTO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_CLIENTE:
                        sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_DESCRIPCION_PRODUCTO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_HISTORICO_COMPRAS_CLIENTE:
                        var skuHistorial = OracleDataAccess.GetSkuWithMatchHistorialClientProductDescription(oc.Rut, it.Descripcion, true);
                        sku = skuHistorial;
                        if (skuHistorial.Equals("Z446482"))
                        {
                            var skuDimercActivo = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, true);
                            sku = skuDimercActivo;
                            if (skuDimercActivo.Equals("Z446482"))
                            {
                                var skuDimercTodo = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                                sku = skuDimercTodo;

                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE:


                        if (!it.Sku.Equals("Z446482") && !it.Descripcion.Equals(""))
                        {
                            var skuMatchDescripcionCliente =
                                OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Sku, it.Descripcion);
                            Console.WriteLine(skuMatchDescripcionCliente);

                            var cantidad = OracleDataAccess.GetCantidadPareoCodigoCliente(oc.Rut, it.Sku);
                            var cantidad2 = OracleDataAccess.GetCantidadPareoCodigoCliente(oc.Rut, it.Sku, it.Descripcion);
                            Console.WriteLine("=================== CLIENTE===================");
                            var tamañoMinimo = ((it.Descripcion.Length - 1) / 2);
                            OracleDataAccess._tamanioMinimoPalabras = 3;//tamañoMinimo == 0 ? 1 : tamañoMinimo;
                            var skuPareoDescripcionRelacionada =
                                cantidad == 1 && cantidad == cantidad2
                                    ? OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false)
                                    : cantidad == 1 && cantidad2 == 0
                                        ? skuMatchDescripcionCliente
                                        : OracleDataAccess.GetProductAllLikeWordDescriptionCliente(oc.Rut, it.Sku,
                                            it.Descripcion);
                            if (skuPareoDescripcionRelacionada.Equals("Z446482"))
                            {
                                //Console.WriteLine("===================CODIGO CLIENTE===================");
                                //var skuPareoCodigloCliente = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, mailFaltantes: false);
                                //if (skuPareoCodigloCliente.Equals("Z446482"))
                                //{

                                if (!OracleDataAccess.ExistPurchaseHistory(oc.Rut))
                                    OracleDataAccess.CreatePurchaseHistory(oc.Rut);

                                Console.WriteLine("===================HISTORICO CLIENTE===================");
                                var skuHistorialCliente = OracleDataAccess.GetSkuWithMatchHistorialClientProductDescription(oc.Rut, it.Descripcion, true);
                                if (skuHistorialCliente.Equals("Z446482"))
                                {

                                    Console.WriteLine("===================MAESTRA ACTIVOS===================");
                                    var skuMaestraActivos =
                                         OracleDataAccess
                                         .GetSkuWithMatcthDimercProductDescription(
                                             it.Descripcion
                                             , true
                                             , true);
                                    if (skuMaestraActivos.Equals("Z446482"))
                                    {

                                        Console.WriteLine("===================MAESTRA UNIVERSAL===================");
                                        var skuMaestraUniversal =
                                         OracleDataAccess
                                         .GetSkuWithMatcthDimercProductDescription(
                                             it.Descripcion
                                             , true
                                             , false);
                                        if (skuMaestraUniversal.Equals("Z446482"))
                                        {
                                            sku = "Z446482";
                                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_PAREO_AUTOMATICO;
                                            ret.EstadoOrden = EstadoOrden.SKU_NO_PAREADO;
                                            OracleDataAccess.InsertIntoReCodCli(
                                                    oc.Rut
                                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                                    , sku.Equals("Z446482") ? "SIN_SKU" : sku
                                                    , it.Descripcion
                                                    , TipoPareoReCodCli.EJECUTIVO_DEBE_PAREAR_CODIGO);

                                        }
                                        else //Encuentra Sku Maestra Universal
                                        {
                                            Console.WriteLine("===================ENCONTRADO EN MAESTRA UNIVERSAL===================");
                                            sku = skuMaestraUniversal;
                                            OracleDataAccess.InsertIntoReCodCli(
                                                oc.Rut
                                                , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                                , sku
                                                , it.Descripcion
                                                , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                                            estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                        }
                                    }
                                    else//Encuentra SKU en Maestra Activos
                                    {
                                        Console.WriteLine("===================ENCONTRADO EN MAESTRA ACTIVOS===================");
                                        sku = skuMaestraActivos;
                                        OracleDataAccess.InsertIntoReCodCli(
                                            oc.Rut
                                            , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                            , sku
                                            , it.Descripcion
                                            , TipoPareoReCodCli.PAREO_PRODUCTOS_ACTIVOS);
                                        estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                    }
                                }
                                else //Encuentra SKU Historia de Compra Cliente
                                {
                                    Console.WriteLine("===================ENCONTRADO EN HISTORIAL CLIENTE===================");
                                    sku = skuHistorialCliente;
                                    OracleDataAccess.InsertIntoReCodCli(
                                        oc.Rut
                                        , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                        , sku
                                        , it.Descripcion
                                        , TipoPareoReCodCli.PAREO_HISTORIAL_CLIENTE);
                                    estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                }
                                //}
                                //else
                                //{
                                //    Console.WriteLine("===================ENCONTRADO EN CODIGO CLIENTE===================");
                                //    sku = skuPareoCodigloCliente;
                                //    estadoDetalle = EstadoPareoDetalle.SkuPareado;
                                //}
                            }
                            else
                            {
                                Console.WriteLine(
                                    "==================ENCONTRADO CON DESCRIPCION CLIENTE======================");
                                sku = skuPareoDescripcionRelacionada;
                            }
                        }
                        else if (!it.Sku.Equals("Z446482") && !it.Sku.Equals("SIN_SKU"))
                        {
                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            var skuPareoCodigloCliente = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra,
                                oc.Rut, it.Sku, mailFaltantes: false);
                            if (!skuPareoCodigloCliente.Equals("Z446482"))
                            {
                                sku = skuPareoCodigloCliente;
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_SKU_CLIENTE_DESCRIPCION_TELEMARKETING:
                        if (!sku.Equals("Z446482"))
                        {//CON SKU CLIENTE
                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false);
                        }
                        else
                        {//CON DESCRIPCION CLIENTE

                            Console.WriteLine("===================DESCRIPCION CLIENTE===================");
                            sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion);
                        }
                        if (sku.Equals("Z446482") || sku.Equals(""))
                        {//NO POSEE PAREO DEFINIDO

                            Console.WriteLine("===================DESCRIPCION TELEMARKETING===================");
                            sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                            if (!sku.Equals("Z446482"))
                            {
                                OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_CLIENTE_O_CODIGO_DIMERC:
                        if (!sku.isPatternSKUDimerc())
                        {//CON SKU CLIENTE

                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false);
                        }
                        if (sku.Equals("Z446482") || sku.Equals(""))
                        {//NO POSEE PAREO DEFINIDO

                            Console.WriteLine("===================DESCRIPCION TELEMARKETING===================");
                            sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                            if (!sku.Equals("Z446482"))
                            {
                                OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                            }
                        }
                        break;
                }
                sku = sku.Replace(".", "").Replace(" ", "");
                if (!OracleDataAccess.ExistProduct(sku))
                {
                    sku = "Z446482";
                }
                //var pConv = "0";
                var precioTlmk = 0;
                
                switch (it.TipoPrecioProducto)
                {
                    case TipoPrecioProducto.TELEMARKETING:
                        var empresa = "3";
                        var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                        if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                        {
                            sku = "Z446482";
                            precioTlmk = 1;
                        }
                        else
                        {
                            precioTlmk = int.Parse(PrecioUltimate);
                        }
                        it.Precio = precioTlmk.ToString();
                        break;
                }
                
                it.Precio = it.Precio;
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var cantidadPdf = 0;
                if (!int.TryParse(it.Cantidad, out cantidadPdf))
                {
                    var fileName = pdfReader == null ? oc.NumeroCompra : pdfReader.PdfFileName;
                    continue;
                    throw new ErrorIntegracion(
                        TipoError.ERROR_CANTIDAD_ITEM,
                        $"Error en Cantidad de Items, el Sistema no reconoce correctamente la Cantidad del Producto {sku}"
                        , ret.RutCli.ToString()
                        , ret.OcCliente
                        , fileName);
                }
                var precioPdf = 0;
                if (sku.Equals("Z446482"))
                {
                    it.Precio = "1";
                }
                if (!int.TryParse(it.Precio, out precioPdf) && sku.Equals("Z446482"))
                {
                    var fileName = pdfReader == null ? oc.NumeroCompra : pdfReader.PdfFileName;
                    throw new ErrorIntegracion(
                        TipoError.ERROR_PRECIO_ITEM,
                        $"Error en Precio de Items, el Sistema no reconoce correctamente el Precio del Producto: {sku}."
                        , ret.RutCli.ToString()
                        , ret.OcCliente
                        , fileName);
                }
                
                var subtotalTlmk = sku.Equals("Z446482")
                    ? int.Parse(it.Cantidad) * int.Parse(it.Precio)
                    : int.Parse(it.Cantidad) * precioTlmk;

                subtotalTlmk = subtotalTlmk / multiplo;


                

                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    EstadoPareoDetalle = estadoDetalle,
                    //WarningDetalle = warning,
                    Cantidad = cantidadPdf / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? precioPdf
                        : precioTlmk == 0 ?
                         precioPdf
                         : precioTlmk, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (precioPdf * cantidadPdf)
                        : precioTlmk == 0 ?
                        (precioPdf * cantidadPdf)
                        : (precioTlmk * cantidadPdf) / multiplo,
                    SkuDimerc = sku
                };
                ret.AddDetalleCompra(dt);
            }
            compraAux.numped = Convert.ToInt32(ret.NumPed);
            OracleDataAccess.saveAuxOrdenCompra(compraAux);
            return ret;
        }


        /// <summary>
        /// Trasnpaso a Orden Compra de Integración, con respectivos procesos
        /// </summary>
        /// <param name="oc">Orden de Compra</param>
        /// <param name="pdfReader">PdfReader</param>
        /// <param name="wordReader">WordReader</param>
        /// <param name="excelReader">ExcelReader</param>
        /// <returns></returns>
        public static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    
                    if (items[i].Sku.Equals(items[j].Sku) && !items[i].Sku.Equals("Z446482"))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }

                }
            }
        }

        public static OrdenCompraIntegracion TraspasoUltimateIntegracionNuevoFlujo(
            this OrdenCompra oc
            , PDFReader pdfReader = null
            , WordReader wordReader = null
            , ExcelReader excelReader = null
            , TextReader textReader = null)
        {
            var fileName = pdfReader != null
                ? pdfReader.PdfFileName
                : wordReader != null
                    ? wordReader.WordFileName
                    : excelReader != null
                        ? excelReader.ExcelFileName
                        : "";
            CompraAux compraAux = new CompraAux();
            if (excelReader != null)
            {
                compraAux.archivo = excelReader.ExcelFileName;
                compraAux.cencos_cliente = oc.CentroCosto;
            }
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            bool flagSap = true;
            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                flagSap = false;
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }
            var cencos = oc.CentroCosto.Replace("-", " ");
            var resultadoCentroCosto = new ResultadoCentroCosto
            {
                EstadoOrden = EstadoOrden.CENTRO_COSTO_NO_PAREADO,
                Cencos = "0"                
            };
            switch (oc.TipoPareoCentroCosto)
            {
                case TipoPareoCentroCosto.SIN_PAREO:
                    resultadoCentroCosto = new ResultadoCentroCosto
                    {
                        EstadoOrden = EstadoOrden.POR_PROCESAR,
                        Cencos = oc.CentroCosto
                    };
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH:
                    resultadoCentroCosto = OracleDataAccess.GetCenCosFromRutClienteAndDescCencosWithMatchToResultadoCentroCosto(oc.NumeroCompra, oc.Rut, cencos, estadoOrden);
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA:
                    resultadoCentroCosto = OracleDataAccess.GetCenCosDescripcionExactaFromRutClienteToResultadoCentroCosto(oc.NumeroCompra, oc.Rut, cencos);
                    if (oc.TipoIntegracion == TipoIntegracion.CARGA_ESTANDAR)
                    {
                        if (resultadoCentroCosto.EstadoOrden == EstadoOrden.CENTRO_COSTO_NO_PAREADO)
                        {
                            var cc = Regex.Match(oc.CentroCosto.DeleteContoniousWhiteSpace(), @"\d{1,}").Success ?
                                oc.CentroCosto.DeleteContoniousWhiteSpace() : "-1";
                            OracleDataAccess.InsertIntoReCcToCli(oc.Rut, cc);
                            //var rutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.Rut);
                            //var detalle = $"El centro de costo {cc} del Cliente con Rut: {oc.Rut}, no existe, crear para procesar las O/C {oc.NumeroCompra}";
                            //OracleDataAccess.InsertPopupTelemarketing(rutUsuario,detalle);
                            resultadoCentroCosto.Cencos = cc;
                            resultadoCentroCosto.EstadoOrden = EstadoOrden.POR_PROCESAR;
                        }
                    }
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE:
                    resultadoCentroCosto = OracleDataAccess.GetCenCosFromRutClienteAndDescCencosToResultadoCentroCosto(oc.NumeroCompra, oc.Rut, cencos, true);
                    break;
                case TipoPareoCentroCosto.PAREO_CCOSTO_TATA:
                    resultadoCentroCosto = OracleDataAccess.getCencosByDescripcionDeClienteTATA(oc.Rut, cencos);
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE:
                    resultadoCentroCosto.Cencos = OracleDataAccess.getCencosByDescripcionDeCliente(oc.Rut,cencos);
                    resultadoCentroCosto.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    break;
            }
            var cencosAntofagasta = OracleDataAccess.TieneBodegaAntofagasta(oc.Rut, resultadoCentroCosto.Cencos);
            var tipoBodega = cencosAntofagasta
                             && resultadoCentroCosto.EstadoOrden == EstadoOrden.POR_PROCESAR
                ? TipoBodega.ANTOFAGASTA
                : TipoBodega.SANTIAGO;

            if(flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, resultadoCentroCosto.Cencos))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
            }
            if (oc.Rut.Equals("77398220"))
            {
                oc.TipoIntegracion = TipoIntegracion.CARGA_ESTANDAR_ML;
            }
            
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                EstadoOrden = resultadoCentroCosto.EstadoOrden,
                RutCli = int.Parse(oc.Rut),
                RutCliPrv = oc.RutProveedor != ""  ? int.Parse(oc.RutProveedor)  : 0,
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                Flujo = TipoFlujo.FLUJO_NUEVO,
                CenCos = resultadoCentroCosto.Cencos,
                Direccion = oc.Direccion,
                TipoIntegracion = oc.TipoIntegracion,
                
        };
            SumarIguales(oc.Items);
            foreach (var it in oc.Items)
            {
                
                if (it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                   && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO"))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                if (oc.Rut.Equals("99579260") || oc.Rut.Equals("77398220") || oc.Rut.Equals("84000000") || oc.Rut.Equals("65154021")
                || oc.Rut.Equals("99507130") || oc.Rut.Equals("23633144") || oc.Rut.Equals("99545180")
                )
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                var sku = it.Sku.ToUpper().DeleteSymbol().Trim();
                
                if (OracleDataAccess.ExistProduct(sku))
                {
                    OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_POR_SKU_DIMERC);
                }
                switch (it.TipoPareoProducto)
                {
                    case TipoPareoProducto.SIN_PAREO:
                        sku = it.Sku;
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_CLIENTE:
                        sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING:
                        sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_DESCRIPCION_PRODUCTO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_CLIENTE:
                        sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_DESCRIPCION_PRODUCTO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_HISTORICO_COMPRAS_CLIENTE:
                        var skuHistorial = OracleDataAccess.GetSkuWithMatchHistorialClientProductDescription(oc.Rut, it.Descripcion, true);
                        sku = skuHistorial;
                        if (skuHistorial.Equals("Z446482"))
                        {
                            var skuDimercActivo = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, true);
                            sku = skuDimercActivo;
                            if (skuDimercActivo.Equals("Z446482"))
                            {
                                var skuDimercTodo = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                                sku = skuDimercTodo;

                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE:

                        //if (it.Descripcion.Equals(""))
                        //{
                        //    it.Descripcion = "LAPIZ BICOLOR FABER";
                        //    //TODO CORREGIR ERROR NO ENCUENTRA DESCRIPCION EN RVC
                        //}
                        if (!it.Sku.Equals("Z446482") && !it.Descripcion.Equals(""))
                        {
                            var skuMatchDescripcionCliente =
                                OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Sku, it.Descripcion);
                            Console.WriteLine(skuMatchDescripcionCliente);

                            var cantidad = OracleDataAccess.GetCantidadPareoCodigoCliente(oc.Rut, it.Sku);
                            var cantidad2 = OracleDataAccess.GetCantidadPareoCodigoCliente(oc.Rut, it.Sku, it.Descripcion);
                            Console.WriteLine("=================== CLIENTE===================");
                            var tamañoMinimo = ((it.Descripcion.Length - 1) / 2);
                            OracleDataAccess._tamanioMinimoPalabras = 3;//tamañoMinimo == 0 ? 1 : tamañoMinimo;
                            var skuPareoDescripcionRelacionada =
                                cantidad == 1 && cantidad == cantidad2
                                    ? OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false)
                                    : (cantidad == 1 && cantidad2 == 0) || (cantidad2 == 1 && cantidad > 1 && it.Sku.Equals("SIN_SKU"))
                                        ? skuMatchDescripcionCliente
                                        : OracleDataAccess.GetProductAllLikeWordDescriptionCliente(oc.Rut, it.Sku,
                                            it.Descripcion);
                            if (skuPareoDescripcionRelacionada.Equals("Z446482"))
                            {
                                //Console.WriteLine("===================CODIGO CLIENTE===================");
                                //var skuPareoCodigloCliente = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, mailFaltantes: false);
                                //if (skuPareoCodigloCliente.Equals("Z446482"))
                                //{

                                if (!OracleDataAccess.ExistPurchaseHistory(oc.Rut))
                                    OracleDataAccess.CreatePurchaseHistory(oc.Rut);

                                Console.WriteLine("===================HISTORICO CLIENTE===================");
                                var skuHistorialCliente = OracleDataAccess.GetSkuWithMatchHistorialClientProductDescription(oc.Rut, it.Descripcion, true);
                                if (skuHistorialCliente.Equals("Z446482"))
                                {

                                    Console.WriteLine("===================MAESTRA ACTIVOS===================");
                                    var skuMaestraActivos =
                                         OracleDataAccess
                                         .GetSkuWithMatcthDimercProductDescription(
                                             it.Descripcion
                                             , true
                                             , true);
                                    if (skuMaestraActivos.Equals("Z446482"))
                                    {

                                        Console.WriteLine("===================MAESTRA UNIVERSAL===================");
                                        var skuMaestraUniversal =
                                         OracleDataAccess
                                         .GetSkuWithMatcthDimercProductDescription(
                                             it.Descripcion
                                             , true
                                             , false);
                                        if (skuMaestraUniversal.Equals("Z446482"))
                                        {
                                            sku = "Z446482";
                                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_PAREO_AUTOMATICO;
                                            ret.EstadoOrden = EstadoOrden.SKU_NO_PAREADO;
                                            OracleDataAccess.InsertIntoReCodCli(
                                                    oc.Rut
                                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                                    , sku.Equals("Z446482") ? "SIN_SKU" : sku
                                                    , it.Descripcion
                                                    , TipoPareoReCodCli.EJECUTIVO_DEBE_PAREAR_CODIGO);

                                        }
                                        else //Encuentra Sku Maestra Universal
                                        {
                                            Console.WriteLine("===================ENCONTRADO EN MAESTRA UNIVERSAL===================");
                                            sku = skuMaestraUniversal;
                                            OracleDataAccess.InsertIntoReCodCli(
                                                oc.Rut
                                                , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                                , sku
                                                , it.Descripcion
                                                , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                                            estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                        }
                                    }
                                    else//Encuentra SKU en Maestra Activos
                                    {
                                        Console.WriteLine("===================ENCONTRADO EN MAESTRA ACTIVOS===================");
                                        sku = skuMaestraActivos;
                                        OracleDataAccess.InsertIntoReCodCli(
                                            oc.Rut
                                            , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                            , sku
                                            , it.Descripcion
                                            , TipoPareoReCodCli.PAREO_PRODUCTOS_ACTIVOS);
                                        estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                    }
                                }
                                else //Encuentra SKU Historia de Compra Cliente
                                {
                                    Console.WriteLine("===================ENCONTRADO EN HISTORIAL CLIENTE===================");
                                    sku = skuHistorialCliente;
                                    OracleDataAccess.InsertIntoReCodCli(
                                        oc.Rut
                                        , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                        , sku
                                        , it.Descripcion
                                        , TipoPareoReCodCli.PAREO_HISTORIAL_CLIENTE);
                                    estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                }
                                //}
                                //else
                                //{
                                //    Console.WriteLine("===================ENCONTRADO EN CODIGO CLIENTE===================");
                                //    sku = skuPareoCodigloCliente;
                                //    estadoDetalle = EstadoPareoDetalle.SkuPareado;
                                //}
                            }
                            else
                            {
                                Console.WriteLine(
                                    "==================ENCONTRADO CON DESCRIPCION CLIENTE======================");
                                sku = skuPareoDescripcionRelacionada;
                            }
                        }
                        else if (!it.Sku.Equals("Z446482") && !it.Sku.Equals("SIN_SKU"))
                        {
                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            var skuPareoCodigloCliente = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra,
                                oc.Rut, it.Sku, mailFaltantes: false);
                            if (!skuPareoCodigloCliente.Equals("Z446482"))
                            {
                                sku = skuPareoCodigloCliente;
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_SKU_CLIENTE_DESCRIPCION_TELEMARKETING:
                        if (!sku.Equals("Z446482"))
                        {//CON SKU CLIENTE
                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false);
                        }
                        else
                        {//CON DESCRIPCION CLIENTE

                            Console.WriteLine("===================DESCRIPCION CLIENTE===================");
                            sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion);
                        }
                        if (sku.Equals("Z446482") || sku.Equals(""))
                        {//NO POSEE PAREO DEFINIDO

                            Console.WriteLine("===================DESCRIPCION TELEMARKETING===================");
                            sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                            if (!sku.Equals("Z446482"))
                            {
                                OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_CLIENTE_O_CODIGO_DIMERC:
                        if (!sku.isPatternSKUDimerc())
                        {//CON SKU CLIENTE

                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false);
                        }
                        if (sku.Equals("Z446482") || sku.Equals(""))
                        {//NO POSEE PAREO DEFINIDO

                            Console.WriteLine("===================DESCRIPCION TELEMARKETING===================");
                            sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                            if (!sku.Equals("Z446482"))
                            {
                                OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_UNI:                        
                        sku = OracleDataAccess.GetSkuDimercByCodigoUni(it.Sku);
                        break;
                }
                //sku = it.Sku.Equals("P.02.01.04.004") ? "Z446482" : sku;
                sku = sku.Replace(".", "").Replace(" ", "").Trim();
                if (!OracleDataAccess.ExistProduct(sku))
                {
                    sku = "Z446482";
                }
                int precioTlmk = 0;
                if (it.TipoPrecioProducto != TipoPrecioProducto.ARCHIVO_ADJUNTO && precioTlmk == 0)
                {
                    string empresa = "3";
                    //var pConvenioN = OracleDataAccess.GetPrecioProductoConvenio(it.Sku, oc.Rut);
                    //var pSap = OracleDataAccess.GetPrecioProducto1(empresa, sku, oc.Rut);//get precio nuevo
                    //var pPreciosProd = OracleDataAccess.GetPrecioProducto(oc.Rut, oc.CentroCosto, sku, empresa);//precio de lista


                    //if (!pConvenioN.Equals(0) || !pConvenioN.Equals(null))
                    //{
                    //    precioTlmk = int.Parse(pConvenioN);
                    //    
                    //    if (precioTlmk.Equals(0) || precioTlmk.Equals(null))
                    //    {
                    //        precioTlmk = int.Parse(pSap);
                    //
                    //           if (precioTlmk.Equals(0) || precioTlmk.Equals(null))
                    //            {
                    //                precioTlmk = int.Parse(pPreciosProd);
                    //                
                    //            }
                    //    }
                    //}
                    var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                    if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                    {
                        sku = "Z446482";
                        precioTlmk = 1;
                    }
                    else
                    {
                        precioTlmk = int.Parse(PrecioUltimate);
                    }
                //else
                //{
                //    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                it.Precio = precioTlmk.ToString();
                }

                //{
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                //var pConv = OracleDataAccess.GetPrecioProducto1(empresa, sku, oc.Rut);//get precio nuevo
                //var pConvenio = OracleDataAccess.GetPrecioConvenio


                /*string empresa = "3";
                    var pConvenio = OracleDataAccess.GetPrecioProductoConvenio(it.Sku, oc.Rut);
                    var pConv = OracleDataAccess.GetPrecioProducto1(empresa, it.Sku, oc.Rut);
                    var precioTlmk = int.Parse(pConvenio);
                    var prec = 0;
                    if (!pConvenio.Equals("0"))
                    {
                        precioTlmk = 0;
                    }
                    else
                    {
                        if (precioTlmk.Equals(0))
                        {
                            prec = int.Parse(pConv);
                        }
                    }
                    if (sku.Equals("Z446482"))
                    {
                        prec = 1;
                    }
                    
                    it.Precio = prec.ToString();
                //}
                */


                //get precio antiguo
                //if (oc.Rut.Equals("99507130"))
                //{
                //    pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, ret.CenCos, sku); 
                //    precioTlmk = int.Parse(pConv);
                //}
                

                if (oc.Rut.Equals("97004000"))
                {
                    var multiplobch = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                }
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var cantidadArchivoAdjunto = 0;
                //if (it.Cantidad.Contains("AVERY"))
                //{
                //    it.Cantidad = "1";
                //}
                if (!int.TryParse(it.Cantidad, out cantidadArchivoAdjunto))
                {
                    if (InternalVariables.InternalVariables.IsDebug())
                        cantidadArchivoAdjunto = -1;
                    else
                        throw new ErrorIntegracion(
                            TipoError.ERROR_CANTIDAD_ITEM
                            ,
                            $"Error en Cantidad de Items, el Sistema no reconoce correctamente la Cantidad del Producto {sku}"
                            , ret.RutCli.ToString()
                            , ret.OcCliente
                            , fileName);
                }
                var precioArchivoAdjunto = 0;
                if (!int.TryParse(it.Precio, out precioArchivoAdjunto))
                {
                    //if (InternalVariables.InternalVariables.IsDebug())
                    precioArchivoAdjunto = 1000;
                    //else
                    //    throw new ErrorIntegracion(
                    //    TipoError.ERROR_PRECIO_ITEM
                    //    , $"Error en Precio de Items, el Sistema no reconoce correctamente el Precio del Producto: {sku}."
                    //    , ret.RutCli.ToString()
                    //    , ret.OcCliente
                    //    , fileName);
                }
                var subtotalTlmk = sku.Equals("Z446482")
                    ? cantidadArchivoAdjunto * precioArchivoAdjunto
                    : cantidadArchivoAdjunto * precioTlmk;

                subtotalTlmk = subtotalTlmk / multiplo;


                var subTotalPdf = int.Parse(it.SubTotal);
                subTotalPdf = subTotalPdf == 0
                    //&& InternalVariables.InternalVariables.IsDebug()
                    ? subtotalTlmk
                    : subTotalPdf;
                var posibleErrorUnidades = subtotalTlmk / subTotalPdf == 0 ? 1 : subtotalTlmk / subTotalPdf;
                var warning = WarningDetalle.SIN_WARNING;
                if (posibleErrorUnidades >= 2)
                {
                    var precioPdf = 1;
                    if (!int.TryParse(it.Precio, out precioPdf))
                    {
                        precioPdf = 1;
                    }
                    if (precioPdf > 1)
                    {
                        warning = WarningDetalle.POSIBLE_ERROR_MULTIPLOS;
                        OracleDataAccess.UpdateReCodCli(
                                       oc.Rut
                                       , it.Sku
                                       , sku
                                       , it.Descripcion);
                        Log.Log.SaveProblemaConversionUnidades(oc.NumeroCompra, oc.Rut, it.Sku, sku, it.SubTotal, subtotalTlmk);
                    }
                }
                else if (precioArchivoAdjunto > precioTlmk)
                {
                    warning = WarningDetalle.PRECIO_MENOR_QUE_PDF;
                }
                else if (precioArchivoAdjunto < precioTlmk)
                {
                    warning = WarningDetalle.PRECIO_MAYOR_QUE_PDF;
                }
                else if (it.Precio.Length >= 5 && cantidadArchivoAdjunto >= 300)
                {
                    warning = WarningDetalle.POSIBLE_ERROR_CANTIDAD;
                }

                var dt = new DetalleOrdenCompraIntegracion
                {
                    CodigoBodega = tipoBodega,
                    NumPed = ret.NumPed,
                    EstadoPareoDetalle = estadoDetalle,
                    WarningDetalle = warning,
                    Cantidad = cantidadArchivoAdjunto / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? precioArchivoAdjunto
                        : precioTlmk == 0
                            ? precioArchivoAdjunto
                            : it.TipoPrecioProducto == TipoPrecioProducto.TELEMARKETING
                                ? precioTlmk
                                : precioArchivoAdjunto, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (precioArchivoAdjunto * cantidadArchivoAdjunto)
                        : precioTlmk == 0
                            ? (precioArchivoAdjunto * cantidadArchivoAdjunto)
                            : it.TipoPrecioProducto == TipoPrecioProducto.TELEMARKETING
                                ? (precioTlmk * cantidadArchivoAdjunto) / multiplo
                                : (precioArchivoAdjunto * cantidadArchivoAdjunto) / multiplo,
                    SkuDimerc = sku
                };
                ret.AddDetalleCompra(dt);
            }
            


            if (ret.OcCliente.Equals(""))
                ret.OcCliente = ret.NumPed;
            compraAux.numped = Convert.ToInt32(ret.NumPed);
            OracleDataAccess.saveAuxOrdenCompra(compraAux);
            
            return ret;
            
        }

        
        public static OrdenCompraIntegracionBCH TraspasoUltimateIntegracionNuevoFlujoBancoChile(
            this OrdenCompra oc
            , PDFReader pdfReader = null
            , WordReader wordReader = null
            , ExcelReader excelReader = null
            , TextReader textReader = null)
        {
            var fileName = pdfReader != null
                ? pdfReader.PdfFileName
                : wordReader != null
                    ? wordReader.WordFileName
                    : excelReader != null
                        ? excelReader.ExcelFileName
                        : "";
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }
            var cencos = oc.CentroCosto.Replace("-", " ");
            var resultadoCentroCosto = new ResultadoCentroCosto
            {
                EstadoOrden = EstadoOrden.CENTRO_COSTO_NO_PAREADO,
                Cencos = "0"
            };
            switch (oc.TipoPareoCentroCosto)
            {
                case TipoPareoCentroCosto.SIN_PAREO:
                    resultadoCentroCosto = new ResultadoCentroCosto
                    {
                        EstadoOrden = EstadoOrden.POR_PROCESAR,
                        Cencos = oc.CentroCosto                         
                    };
                    if (!Oracle.DataAccess.OracleDataAccess.ExisteCcTelemarketingBCH(oc.Rut, oc.CentroCosto))
                    {
                        resultadoCentroCosto.Cencos = "1";
                    }
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH:
                    resultadoCentroCosto = OracleDataAccess.GetCenCosFromRutClienteAndDescCencosWithMatchToResultadoCentroCosto(oc.NumeroCompra, oc.Rut, cencos, estadoOrden);
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA:
                    resultadoCentroCosto = OracleDataAccess.GetCenCosDescripcionExactaFromRutClienteToResultadoCentroCosto(oc.NumeroCompra, oc.Rut, cencos);
                    if (oc.TipoIntegracion == TipoIntegracion.CARGA_ESTANDAR)
                    {
                        if (resultadoCentroCosto.EstadoOrden == EstadoOrden.CENTRO_COSTO_NO_PAREADO)
                        {
                            var cc = Regex.Match(oc.CentroCosto.DeleteContoniousWhiteSpace(), @"\d{1,}").Success ?
                                oc.CentroCosto.DeleteContoniousWhiteSpace() : "-1";
                            OracleDataAccess.InsertIntoReCcToCli(oc.Rut, cc);
                            //var rutUsuario = OracleDataAccess.GetRutUsuarioFromRutCliente(oc.Rut);
                            //var detalle = $"El centro de costo {cc} del Cliente con Rut: {oc.Rut}, no existe, crear para procesar las O/C {oc.NumeroCompra}";
                            //OracleDataAccess.InsertPopupTelemarketing(rutUsuario,detalle);
                            resultadoCentroCosto.Cencos = cc;
                            resultadoCentroCosto.EstadoOrden = EstadoOrden.POR_PROCESAR;
                        }
                    }
                    break;
                case TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE:
                    resultadoCentroCosto = OracleDataAccess.GetCenCosFromRutClienteAndDescCencosToResultadoCentroCosto(oc.NumeroCompra, oc.Rut, cencos, true);
                    break;
                case TipoPareoCentroCosto.PAREO_CCOSTO_TATA:
                    resultadoCentroCosto = OracleDataAccess.getCencosByDescripcionDeClienteTATA(oc.Rut, cencos);
                    break;
            }
            var cencosAntofagasta = OracleDataAccess.TieneBodegaAntofagasta(oc.Rut, resultadoCentroCosto.Cencos);
            var tipoBodega = cencosAntofagasta
                             && resultadoCentroCosto.EstadoOrden == EstadoOrden.POR_PROCESAR
                ? TipoBodega.ANTOFAGASTA
                : TipoBodega.SANTIAGO;


            var ret = new OrdenCompraIntegracionBCH
            {
                NumPed = OracleDataAccess.GetNumPed(),
                EstadoOrden = resultadoCentroCosto.EstadoOrden,
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                Flujo = TipoFlujo.FLUJO_NUEVO,
                CenCos = resultadoCentroCosto.Cencos,
                Direccion = oc.Direccion,
                TipoIntegracion = oc.TipoIntegracion
            };
            foreach (var it in oc.Items)
            {
                
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")
                   ))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                var sku = it.Sku.ToUpper().DeleteSymbol().Trim();

                if (OracleDataAccess.ExistProduct(sku))
                {
                    OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_POR_SKU_DIMERC);
                }
                switch (it.TipoPareoProducto)
                {
                    case TipoPareoProducto.SIN_PAREO:
                        sku = it.Sku;
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_CLIENTE:
                        sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING:
                        sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_DESCRIPCION_PRODUCTO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_CLIENTE:
                        sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion);
                        if (sku.Equals("Z446482"))
                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_DESCRIPCION_PRODUCTO;
                        break;
                    case TipoPareoProducto.PAREO_DESCRIPCION_HISTORICO_COMPRAS_CLIENTE:
                        var skuHistorial = OracleDataAccess.GetSkuWithMatchHistorialClientProductDescription(oc.Rut, it.Descripcion, true);
                        sku = skuHistorial;
                        if (skuHistorial.Equals("Z446482"))
                        {
                            var skuDimercActivo = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, true);
                            sku = skuDimercActivo;
                            if (skuDimercActivo.Equals("Z446482"))
                            {
                                var skuDimercTodo = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                                sku = skuDimercTodo;

                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE:

                        //if (it.Descripcion.Equals(""))
                        //{
                        //    it.Descripcion = "LAPIZ BICOLOR FABER";
                        //    //TODO CORREGIR ERROR NO ENCUENTRA DESCRIPCION EN RVC
                        //}
                        if (!it.Sku.Equals("Z446482") && !it.Descripcion.Equals(""))
                        {
                            var skuMatchDescripcionCliente =
                                OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Sku, it.Descripcion);
                            Console.WriteLine(skuMatchDescripcionCliente);

                            var cantidad = OracleDataAccess.GetCantidadPareoCodigoCliente(oc.Rut, it.Sku);
                            var cantidad2 = OracleDataAccess.GetCantidadPareoCodigoCliente(oc.Rut, it.Sku, it.Descripcion);
                            Console.WriteLine("=================== CLIENTE===================");
                            var tamañoMinimo = ((it.Descripcion.Length - 1) / 2);
                            OracleDataAccess._tamanioMinimoPalabras = 3;//tamañoMinimo == 0 ? 1 : tamañoMinimo;
                            var skuPareoDescripcionRelacionada =
                                cantidad == 1 && cantidad == cantidad2
                                    ? OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false)
                                    : (cantidad == 1 && cantidad2 == 0) || (cantidad2 == 1 && cantidad > 1 && it.Sku.Equals("SIN_SKU"))
                                        ? skuMatchDescripcionCliente
                                        : OracleDataAccess.GetProductAllLikeWordDescriptionCliente(oc.Rut, it.Sku,
                                            it.Descripcion);
                            if (skuPareoDescripcionRelacionada.Equals("Z446482"))
                            {
                                //Console.WriteLine("===================CODIGO CLIENTE===================");
                                //var skuPareoCodigloCliente = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, mailFaltantes: false);
                                //if (skuPareoCodigloCliente.Equals("Z446482"))
                                //{

                                if (!OracleDataAccess.ExistPurchaseHistory(oc.Rut))
                                    OracleDataAccess.CreatePurchaseHistory(oc.Rut);

                                Console.WriteLine("===================HISTORICO CLIENTE===================");
                                var skuHistorialCliente = OracleDataAccess.GetSkuWithMatchHistorialClientProductDescription(oc.Rut, it.Descripcion, true);
                                if (skuHistorialCliente.Equals("Z446482"))
                                {

                                    Console.WriteLine("===================MAESTRA ACTIVOS===================");
                                    var skuMaestraActivos =
                                         OracleDataAccess
                                         .GetSkuWithMatcthDimercProductDescription(
                                             it.Descripcion
                                             , true
                                             , true);
                                    if (skuMaestraActivos.Equals("Z446482"))
                                    {

                                        Console.WriteLine("===================MAESTRA UNIVERSAL===================");
                                        var skuMaestraUniversal =
                                         OracleDataAccess
                                         .GetSkuWithMatcthDimercProductDescription(
                                             it.Descripcion
                                             , true
                                             , false);
                                        if (skuMaestraUniversal.Equals("Z446482"))
                                        {
                                            sku = "Z446482";
                                            estadoDetalle = EstadoPareoDetalle.NO_ENCUENTRA_PAREO_AUTOMATICO;
                                            ret.EstadoOrden = EstadoOrden.SKU_NO_PAREADO;
                                            OracleDataAccess.InsertIntoReCodCli(
                                                    oc.Rut
                                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                                    , sku.Equals("Z446482") ? "SIN_SKU" : sku
                                                    , it.Descripcion
                                                    , TipoPareoReCodCli.EJECUTIVO_DEBE_PAREAR_CODIGO);

                                        }
                                        else //Encuentra Sku Maestra Universal
                                        {
                                            Console.WriteLine("===================ENCONTRADO EN MAESTRA UNIVERSAL===================");
                                            sku = skuMaestraUniversal;
                                            OracleDataAccess.InsertIntoReCodCli(
                                                oc.Rut
                                                , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                                , sku
                                                , it.Descripcion
                                                , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                                            estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                        }
                                    }
                                    else//Encuentra SKU en Maestra Activos
                                    {
                                        Console.WriteLine("===================ENCONTRADO EN MAESTRA ACTIVOS===================");
                                        sku = skuMaestraActivos;
                                        OracleDataAccess.InsertIntoReCodCli(
                                            oc.Rut
                                            , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                            , sku
                                            , it.Descripcion
                                            , TipoPareoReCodCli.PAREO_PRODUCTOS_ACTIVOS);
                                        estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                    }
                                }
                                else //Encuentra SKU Historia de Compra Cliente
                                {
                                    Console.WriteLine("===================ENCONTRADO EN HISTORIAL CLIENTE===================");
                                    sku = skuHistorialCliente;
                                    OracleDataAccess.InsertIntoReCodCli(
                                        oc.Rut
                                        , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                        , sku
                                        , it.Descripcion
                                        , TipoPareoReCodCli.PAREO_HISTORIAL_CLIENTE);
                                    estadoDetalle = EstadoPareoDetalle.SKU_PAREADO_AUTOMATICAMENTE;
                                }
                                //}
                                //else
                                //{
                                //    Console.WriteLine("===================ENCONTRADO EN CODIGO CLIENTE===================");
                                //    sku = skuPareoCodigloCliente;
                                //    estadoDetalle = EstadoPareoDetalle.SkuPareado;
                                //}
                            }
                            else
                            {
                                Console.WriteLine(
                                    "==================ENCONTRADO CON DESCRIPCION CLIENTE======================");
                                sku = skuPareoDescripcionRelacionada;
                            }
                        }
                        else if (!it.Sku.Equals("Z446482") && !it.Sku.Equals("SIN_SKU"))
                        {
                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            var skuPareoCodigloCliente = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra,
                                oc.Rut, it.Sku, mailFaltantes: false);
                            if (!skuPareoCodigloCliente.Equals("Z446482"))
                            {
                                sku = skuPareoCodigloCliente;
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_SKU_CLIENTE_DESCRIPCION_TELEMARKETING:
                        if (!sku.Equals("Z446482"))
                        {//CON SKU CLIENTE
                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false);
                        }
                        else
                        {//CON DESCRIPCION CLIENTE

                            Console.WriteLine("===================DESCRIPCION CLIENTE===================");
                            sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion);
                        }
                        if (sku.Equals("Z446482") || sku.Equals(""))
                        {//NO POSEE PAREO DEFINIDO

                            Console.WriteLine("===================DESCRIPCION TELEMARKETING===================");
                            sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                            if (!sku.Equals("Z446482"))
                            {
                                OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_CLIENTE_O_CODIGO_DIMERC:
                        if (!sku.isPatternSKUDimerc())
                        {//CON SKU CLIENTE

                            Console.WriteLine("===================CODIGO CLIENTE===================");
                            sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, false);
                        }
                        if (sku.Equals("Z446482") || sku.Equals(""))
                        {//NO POSEE PAREO DEFINIDO

                            Console.WriteLine("===================DESCRIPCION TELEMARKETING===================");
                            sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(it.Descripcion, true, false);
                            if (!sku.Equals("Z446482"))
                            {
                                OracleDataAccess.InsertIntoReCodCli(
                                    oc.Rut
                                    , sku
                                    , it.Sku.Equals("Z446482") ? "SIN_SKU" : it.Sku
                                    , it.Descripcion
                                    , TipoPareoReCodCli.PAREO_PRODUCTOS_MAESTRA);
                            }
                        }
                        break;
                    case TipoPareoProducto.PAREO_CODIGO_UNI:
                        sku = OracleDataAccess.GetSkuDimercByCodigoUni(it.Sku);
                        break;
                }
                //sku = it.Sku.Equals("P.02.01.04.004") ? "Z446482" : sku;
                sku = sku.Replace(".", "").Replace(" ", "");
                if (!OracleDataAccess.ExistProduct(sku))
                {
                    sku = "Z446482";
                }
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                var empresa = "3";
                var pConv = OracleDataAccess.GetPrecioProducto1(empresa, sku, oc.Rut);
                var precioTlmk = int.Parse(pConv);
                var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                {
                    sku = "Z446482";
                    precioTlmk = 1;
                }
                else
                {
                    precioTlmk = int.Parse(PrecioUltimate);
                }
                /*if (oc.Rut.Equals("97004000"))
                {
                    var multiplobch = OracleDataAccess.GetMultiploFromRutClienteCodProbch(oc.Rut, sku);
                }*/
                //var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodProbch(oc.Rut, sku);
                var cantidadArchivoAdjunto = 0;
                //if (it.Cantidad.Contains("AVERY"))
                //{
                //    it.Cantidad = "1";
                //}
                if (!int.TryParse(it.Cantidad, out cantidadArchivoAdjunto))
                {
                    if (InternalVariables.InternalVariables.IsDebug())
                        cantidadArchivoAdjunto = -1;
                    else
                        throw new ErrorIntegracion(
                            TipoError.ERROR_CANTIDAD_ITEM
                            ,
                            $"Error en Cantidad de Items, el Sistema no reconoce correctamente la Cantidad del Producto {sku}"
                            , ret.RutCli.ToString()
                            , ret.OcCliente
                            , fileName);
                }
                var precioArchivoAdjunto = 0;
                if (!int.TryParse(it.Precio, out precioArchivoAdjunto))
                {
                    //if (InternalVariables.InternalVariables.IsDebug())
                    precioArchivoAdjunto = 1000;
                    //else
                    //    throw new ErrorIntegracion(
                    //    TipoError.ERROR_PRECIO_ITEM
                    //    , $"Error en Precio de Items, el Sistema no reconoce correctamente el Precio del Producto: {sku}."
                    //    , ret.RutCli.ToString()
                    //    , ret.OcCliente
                    //    , fileName);
                }
                var subtotalTlmk = sku.Equals("Z446482")
                    ? cantidadArchivoAdjunto * (double)precioArchivoAdjunto
                    : cantidadArchivoAdjunto * (double)precioTlmk;

                subtotalTlmk = subtotalTlmk / multiplo;


                var subTotalPdf = Double.Parse(it.SubTotal);
                subTotalPdf = subTotalPdf == 0
                    //&& InternalVariables.InternalVariables.IsDebug()
                    ? subtotalTlmk
                    : subTotalPdf;
                var posibleErrorUnidades = subtotalTlmk / subTotalPdf == 0 ? 1 : subtotalTlmk / subTotalPdf;
                var warning = WarningDetalle.SIN_WARNING;
                if (posibleErrorUnidades >= 2)
                {
                    var precioPdf = 1;
                    if (!int.TryParse(it.Precio, out precioPdf))
                    {
                        precioPdf = 1;
                    }
                    if (precioPdf > 1)
                    {
                        warning = WarningDetalle.POSIBLE_ERROR_MULTIPLOS;
                        OracleDataAccess.UpdateReCodCli(
                                       oc.Rut
                                       , it.Sku
                                       , sku
                                       , it.Descripcion);
                       // Log.Log.SaveProblemaConversionUnidades(oc.NumeroCompra, oc.Rut, it.Sku, sku, it.SubTotal, subtotalTlmk);
                    }
                }
                else if (precioArchivoAdjunto > precioTlmk)
                {
                    warning = WarningDetalle.PRECIO_MENOR_QUE_PDF;
                }
                else if (precioArchivoAdjunto < precioTlmk)
                {
                    warning = WarningDetalle.PRECIO_MAYOR_QUE_PDF;
                }
                else if (it.Precio.Length >= 5 && cantidadArchivoAdjunto >= 300)
                {
                    warning = WarningDetalle.POSIBLE_ERROR_CANTIDAD;
                }

                var dt = new DetalleOrdenCompraIntegracionBCH
                {
                    CodigoBodega = tipoBodega,
                    NumPed = ret.NumPed,
                    EstadoPareoDetalle = estadoDetalle,
                    WarningDetalle = warning,
                    Cantidad = cantidadArchivoAdjunto / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? precioArchivoAdjunto
                        : precioTlmk == 0
                            ? precioArchivoAdjunto
                            : it.TipoPrecioProducto == TipoPrecioProducto.TELEMARKETING
                                ? precioTlmk
                                : precioArchivoAdjunto, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (precioArchivoAdjunto * cantidadArchivoAdjunto)
                        : precioTlmk == 0
                            ? (precioArchivoAdjunto * cantidadArchivoAdjunto)
                            : it.TipoPrecioProducto == TipoPrecioProducto.TELEMARKETING
                                ? (precioTlmk * cantidadArchivoAdjunto) / multiplo
                                : (precioArchivoAdjunto * cantidadArchivoAdjunto) / multiplo,
                    SkuDimerc = sku
                };
                ret.AddDetalleCompra(dt);
            }
            if (ret.OcCliente.Equals(""))
                ret.OcCliente = ret.NumPed;
            return ret;
        }


        /// <summary>
        /// Deja Centro de Costo como viene en PDF
        /// Hace Pareo de SKU
        /// </summary>
        /// <param name="oc"></param>
        /// <returns></returns>
        /// Parear solo Sku
        public static OrdenCompraIntegracion ParearSoloSKU(this OrdenCompra oc)
        {
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = oc.CentroCosto,
                Direccion = oc.Direccion
            };

            foreach (var it in oc.Items)
            {
               
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")
                   ))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra, oc.Rut, it.Sku, true);
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                var empresa = "3";
                var precioTlmk = 1;
                /*
                var pConvenioN = OracleDataAccess.GetPrecioProductoConvenio(it.Sku, oc.Rut);
                var pSap = OracleDataAccess.GetPrecioProducto1(empresa, sku, oc.Rut);//get precio nuevo
                var pPreciosProd = OracleDataAccess.GetPrecioProducto(oc.Rut, oc.CentroCosto, sku, empresa);

                if (!pConvenioN.Equals(0) || !pConvenioN.Equals(null))
                {
                    precioTlmk = int.Parse(pConvenioN);

                    if (precioTlmk.Equals(0) || precioTlmk.Equals(null))
                    {
                        precioTlmk = int.Parse(pSap);

                        if (precioTlmk.Equals(0) || precioTlmk.Equals(null))
                        {
                            precioTlmk = int.Parse(pPreciosProd);
                        }
                    }
                }*/
                /*Cambios 12-08-2021 Matias Suazo
        var pConvenio = OracleDataAccess.GetPrecioProductoConvenio(it.Sku, oc.Rut);
        var pConv = OracleDataAccess.GetPrecioProducto1(empresa, it.Sku, oc.Rut);
        if (!pConvenio.Equals("0"))
        {
            precio = int.Parse(pConvenio);
        }
        else
        {
            if (pConvenio.Equals("0"))
            {
                precio = int.Parse(pConv);
            }
        }
        */
                var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                {
                    sku = "Z446482";
                    precioTlmk = 1;
                }
                else
                {
                    precioTlmk = int.Parse(PrecioUltimate);
                }
                it.Precio = precioTlmk.ToString();
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? int.Parse(it.Precio)
                        : precioTlmk, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precioTlmk) / multiplo,

                    //Precio = precio, //int.Parse(it.Precio),
                    //SubTotal = (int.Parse(it.Cantidad) * precio)/multiplo,
                    SkuDimerc = sku
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }

        /// <summary>
        /// Parea solo Descripción asociada al Cliente
        /// </summary>
        /// <param name="oc">Orden de Compra</param>
        /// <returns></returns>
        public static OrdenCompraIntegracion ParearSoloDescripcionCliente(this OrdenCompra oc)
        {
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = oc.CentroCosto,
                Direccion = oc.Direccion
            };

            foreach (var it in oc.Items)
            {
                
                if ((it.TipoPrecioProducto == TipoPrecioProducto.ARCHIVO_ADJUNTO
                    && Oracle.DataAccess.OracleDataAccess.GetUniNegocio(oc.Rut).Equals("GOBIERNO")
                   ))
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO;
                }
                else
                {
                    it.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                }
                var sku = OracleDataAccess.GetSkuWithMatchClientProductDescription(oc.Rut, it.Descripcion.ReplaceSymbolWhiteSpace());
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio);
                var empresa = "3";
                var precio = 0;
                var precioTlmk = 1;
                //var pConvenio = OracleDataAccess.GetPrecioProductoConvenio(it.Sku, oc.Rut);
                //var pConv = OracleDataAccess.GetPrecioProducto1(empresa, it.Sku, oc.Rut);
                //if (!pConvenio.Equals("0"))
                //{
                //    precio = int.Parse(pConvenio);
                //}
                //else
                //{
                //    if (pConvenio.Equals("0"))
                //    {
                //        precio = int.Parse(pConv);
                //    }
                //}
                var PrecioUltimate = OracleDataAccess.GetPrecioProductoUltimate(empresa, oc.Rut, sku);
                if (PrecioUltimate.Equals("0") || PrecioUltimate.Equals(null))
                {
                    sku = "Z446482";
                    precioTlmk = 1;
                }
                else
                {
                    precioTlmk = int.Parse(PrecioUltimate);
                }


                it.Precio = precio.ToString();
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482")
                        ? int.Parse(it.Precio)
                        : precio, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precio) / multiplo,

                    //Precio = precio, //int.Parse(it.Precio),
                    //SubTotal = (int.Parse(it.Cantidad) * precio)/multiplo,
                    SkuDimerc = sku
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }



    }


}