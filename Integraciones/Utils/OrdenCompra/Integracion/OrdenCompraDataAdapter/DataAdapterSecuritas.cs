﻿using System.Collections.Generic;
using System.Linq;
using Integraciones.Utils.Integracion.Pdf.Securitas;
using Integraciones.Utils.Oracle.DataAccess;

namespace Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter
{
    public static class DataAdapterSecuritas
    {
        public static List<OrdenCompraIntegracion> AdapterSecuritasFormatToCompraIntegracion
            (this OrdenCompraSecuritas oc)
        {
            var ret = new List<OrdenCompraIntegracion>();
            foreach (var o in oc.GetSecuritasAdapterOrdenCompra())
            {
                var estadoOrden = EstadoOrden.POR_PROCESAR;
                var cencos = OracleDataAccess.GetCenCosFromRutCliente(o.NumeroCompra, o.Rut, o.CentroCosto);
                var aux = new OrdenCompraIntegracion
                {
                    NumPed = OracleDataAccess.GetNumPed(),
                    RutCli = int.Parse(o.Rut),
                    OcCliente = o.NumeroCompra,
                    Observaciones = o.Observaciones,
                    CenCos = cencos,
                    Direccion = o.Direccion,
                    EstadoOrden = estadoOrden
                };
                foreach (var dt in o.ItemsSecuritas.Select(it => new DetalleOrdenCompraIntegracion
                {
                    NumPed = aux.NumPed,
                    Cantidad = int.Parse(it.Cantidad),
                    Precio = int.Parse(it.Precio),
                    SubTotal = int.Parse(it.Cantidad)*int.Parse(it.Precio),
                    SkuDimerc = it.Sku
                }))
                {
                    aux.AddDetalleCompra(dt);
                }
                ret.Add(aux);
            }

            return ret;
        }

        public static List<OrdenCompraIntegracion> AdapterSecuritasAustralFormatToCompraIntegracion(
            this OrdenCompraSecuritas oc)
        {
            var ret = new List<OrdenCompraIntegracion>();
            foreach (var o in oc.GetSecuritasAdapterOrdenCompra())
            {
                var estadoOrden = EstadoOrden.POR_PROCESAR;
                bool flagSap = true;
                if (!OracleDataAccess.EsClienteSap(oc.Rut))
                {
                    flagSap = false;
                    estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
                }
                var cencos = OracleDataAccess.GetCenCosFromRutCliente(o.NumeroCompra, o.Rut, o.CentroCosto);
                if(flagSap == true && !OracleDataAccess.EsCcostoSap(o.Rut, cencos))
                {
                    estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
                }
                var aux = new OrdenCompraIntegracion
                {
                    NumPed = OracleDataAccess.GetNumPed(),
                    RutCli = int.Parse(o.Rut),
                    OcCliente = o.NumeroCompra,
                    Observaciones = o.Observaciones,
                    CenCos = cencos,
                    Direccion = o.Direccion,
                    EstadoOrden = estadoOrden
                };
                DataAdapterGeneric.SumarIguales(oc.Items); 
                foreach (var it in o.ItemsSecuritas)
                {
                    if (it.Sku.Equals("R687102"))
                    {
                        it.Sku = "Z446482";
                    }
                        var precio = int.Parse(it.Precio);
                    if (!it.Sku.Equals("Z446482"))
                    {
                        var empresa = "3";
                        //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, aux.CenCos, it.Sku, it.Precio);
                        var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, oc.CentroCosto,it.Sku,empresa);
                        precio = int.Parse(pConv);

                    }
                    else
                    {
                        continue;
                    }
                    var dt = new DetalleOrdenCompraIntegracion
                    {
                        NumPed = aux.NumPed,
                        Cantidad = int.Parse(it.Cantidad),
                        Precio = precio, //int.Parse(it.Precio),
                        SubTotal = int.Parse(it.Cantidad)*precio,
                        SkuDimerc = it.Sku
                    };
                    aux.AddDetalleCompra(dt);
                }
                ret.Add(aux);
            }
            return ret;
        }

        public static List<OrdenCompraIntegracion> AdapterSecuritasAustralFormatToCompraIntegracionWithBodega(
            this OrdenCompraSecuritas oc)
        {
            var ret = new List<OrdenCompraIntegracion>();

            foreach (var o in oc.GetSecuritasAdapterOrdenCompra())
            {
                var estadoOrden = EstadoOrden.POR_PROCESAR;
                bool flagSap = true;
                if (!OracleDataAccess.EsClienteSap(oc.Rut))
                {
                    flagSap = false;
                    estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
                }
                var cencos = OracleDataAccess.GetCenCosFromRutCliente(o.NumeroCompra, o.Rut, o.CentroCosto);
                if(flagSap==true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos)){
                    estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
                }
                var antofa = OracleDataAccess.TieneBodegaAntofagasta(o.Rut, cencos);
                if (antofa)
                {
                    var itemsAntofa = new List<DetalleOrdenCompraIntegracion>();
                    var itemsSantiago = new List<DetalleOrdenCompraIntegracion>();
                    foreach (var it in o.ItemsSecuritas)
                    {
                        var stock = OracleDataAccess.GetStockAntofagasta(it.Sku);
                        var antofaItem = (stock > int.Parse(it.Cantidad));
                        var precio = int.Parse(it.Precio);
                        if (!it.Sku.Equals("Z446482"))
                        {
                            var empresa = "3";
                            //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, cencos, it.Sku, it.Precio);
                            var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, cencos, it.Sku, empresa);
                            precio = int.Parse(pConv);
                        }
                        else
                        {
                            continue;
                        }
                        var dt = new DetalleOrdenCompraIntegracion
                        {
                            Cantidad = int.Parse(it.Cantidad),
                            Precio = precio, //int.Parse(it.Precio),
                            SubTotal = int.Parse(it.Cantidad) * precio,
                            SkuDimerc = it.Sku,
                            CodigoBodega = antofaItem ? TipoBodega.ANTOFAGASTA : TipoBodega.SANTIAGO
                        };
                        if (antofaItem) itemsAntofa.Add(dt);
                        else itemsSantiago.Add(dt);
                    }
                    if (itemsAntofa.Count > 0)
                    {
                        var num = OracleDataAccess.GetNumPed();
                        var cencos2 = OracleDataAccess.GetCenCosFromRutCliente(o.NumeroCompra, o.Rut, o.CentroCosto);
                        if (flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos2))
                        {
                            estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
                        }
                        var aux = new OrdenCompraIntegracion
                        {
                            NumPed = num,
                            RutCli = int.Parse(o.Rut),
                            OcCliente = o.NumeroCompra,
                            Observaciones = o.Observaciones,
                            CenCos = cencos2,
                            Direccion = o.Direccion
                        };
                        foreach (var d in itemsAntofa)
                        {
                            d.NumPed = num;
                            aux.AddDetalleCompra(d);
                        }
                        ret.Add(aux);
                    }
                    if (itemsSantiago.Count > 0)
                    {
                        var num = OracleDataAccess.GetNumPed();
                        estadoOrden = EstadoOrden.POR_PROCESAR;
                        var cencos3 = OracleDataAccess.GetCenCosFromRutCliente(o.NumeroCompra, o.Rut, o.CentroCosto);
                        if (flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos3))
                        {
                            estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
                        }
                        var aux = new OrdenCompraIntegracion
                        {
                            NumPed = num,
                            RutCli = int.Parse(o.Rut),
                            OcCliente = o.NumeroCompra,
                            Observaciones = o.Observaciones,
                            CenCos = cencos3,
                            Direccion = o.Direccion,
                            EstadoOrden = estadoOrden
                        };
                        foreach (var d in itemsSantiago)
                        {
                            d.NumPed = num;
                            aux.AddDetalleCompra(d);
                        }
                        ret.Add(aux);
                    }
                }
                else
                {
                    estadoOrden = EstadoOrden.POR_PROCESAR;
                    var cencos4 = OracleDataAccess.GetCenCosFromRutCliente(o.NumeroCompra, o.Rut, o.CentroCosto);
                    if (flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos4))
                    {
                        estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
                    }
                    var aux = new OrdenCompraIntegracion
                    {
                        NumPed = OracleDataAccess.GetNumPed(),
                        RutCli = int.Parse(o.Rut),
                        OcCliente = o.NumeroCompra,
                        Observaciones = o.Observaciones,
                        CenCos = cencos4,
                        Direccion = o.Direccion,
                        EstadoOrden = estadoOrden
                    };
                    foreach (var it in o.ItemsSecuritas)
                    {
                        var precio = int.Parse(it.Precio);
                        if (!it.Sku.Equals("Z446482"))
                        {
                            if (it.Sku.Equals("H433203"))
                            {
                                it.Sku = "Z446482";
                            }
                            var empresa = "3";
                            //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, cencos, it.Sku, it.Precio);
                            var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, cencos, it.Sku, empresa);
                            if (pConv.Equals(""))
                            {
                                pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, cencos, "Z446482", it.Precio);
                            }
                            precio = int.Parse(pConv);
                        }
                        else
                        {
                            continue;
                        }
                        var dt = new DetalleOrdenCompraIntegracion
                        {
                            NumPed = aux.NumPed,
                            Cantidad = int.Parse(it.Cantidad),
                            Precio = precio, //int.Parse(it.Precio),
                            SubTotal = int.Parse(it.Cantidad) * precio,
                            SkuDimerc = it.Sku
                        };
                        aux.AddDetalleCompra(dt);
                    }
                    ret.Add(aux);
                }
            }
            
            return ret;
        }
    }


}