﻿using Integraciones.Utils.Oracle.DataAccess;

namespace Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter
{
    public static class DataAdapterIansa
    {
        public static OrdenCompraIntegracion AdapterIansaFormatToCompraIntegracion(this global::Integraciones.Utils.OrdenCompra.OrdenCompra oc)
        {
            var estadoOrden = EstadoOrden.POR_PROCESAR;
            var cencos = OracleDataAccess.GetCenCosFromRutClienteAndDescCencos(oc.NumeroCompra, oc.Rut,
                    oc.CentroCosto.Replace(".", "").ToUpper().DeleteAcent(), true);
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = cencos,
                Direccion = oc.Direccion,
                EstadoOrden = estadoOrden
            };
            DataAdapterGeneric.SumarIguales(oc.Items);
            foreach (var it in oc.Items)
            {
                var estadoDetalle = EstadoPareoDetalle.SKU_PAREADO;
                var empresa = "3";
                var sku = OracleDataAccess.GetSkuDimercFromCodCliente(oc.NumeroCompra,oc.Rut, it.Sku, true);
                if (sku.Equals("Z446482")) estadoDetalle = EstadoPareoDetalle.SKU_NO_PAREADO;
                //var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut, ret.CenCos, sku, it.Precio); string codEmp, string codpro, string rutCli
                
                var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut,oc.CentroCosto, sku, empresa);
                var precio = int.Parse(pConv);
                var multiplo = OracleDataAccess.GetMultiploFromRutClienteCodPro(oc.Rut, sku);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad) / multiplo,
                    Precio = sku.Equals("Z446482") ? int.Parse(it.Precio) : precio, //int.Parse(it.Precio),
                    SubTotal = sku.Equals("Z446482")
                        ? (int.Parse(it.Precio) * int.Parse(it.Cantidad)) / multiplo
                        : (int.Parse(it.Cantidad) * precio) / multiplo,
                    SkuDimerc = sku,
                    EstadoPareoDetalle = estadoDetalle
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }
    }
}