﻿using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;

namespace Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter
{
    public static class DataAdapterUnab
    {
        public static OrdenCompraIntegracion AdapterUnabFormatToCompraIntegracion(this global::Integraciones.Utils.OrdenCompra.OrdenCompra oc)
        {

            var estadoOrden = EstadoOrden.POR_PROCESAR;
            bool flagSap = true;
            if (!OracleDataAccess.EsClienteSap(oc.Rut))
            {
                flagSap = false;
                estadoOrden = EstadoOrden.NO_EXISTE_CLIENTE_SAP;
            }
            var cencos = OracleDataAccess.GetCenCosFromRutCliente(oc.NumeroCompra, oc.Rut, oc.CentroCosto);
            if(flagSap == true && !OracleDataAccess.EsCcostoSap(oc.Rut, cencos))
            {
                estadoOrden = EstadoOrden.NO_EXISTE_CCOSTO_SAP;
            }
            var ret = new OrdenCompraIntegracion
            {
                NumPed = OracleDataAccess.GetNumPed(),
                RutCli = int.Parse(oc.Rut),
                OcCliente = oc.NumeroCompra,
                Observaciones = oc.Observaciones,
                CenCos = cencos,
                Direccion = oc.Direccion,
                EstadoOrden = EstadoOrden.POR_PROCESAR
            };
            DataAdapterGeneric.SumarIguales(oc.Items);
            foreach (var it in oc.Items)
            {
                it.Sku = it.Sku.Equals("") ? "Z446482" : it.Sku;
                if (!OracleDataAccess.ExistProduct(it.Sku))
                {
                    it.Sku = "Z446482";
                }
                var empresa = "3";
                // var pConv = OracleDataAccess.GetPrecioConvenio(oc.Rut,ret.CenCos, it.Sku, it.Precio);
                var pConv = OracleDataAccess.GetPrecioProducto(oc.Rut, oc.CentroCosto, it.Sku, empresa);
                var precio = int.Parse(pConv);
                var dt = new DetalleOrdenCompraIntegracion
                {
                    NumPed = ret.NumPed,
                    Cantidad = int.Parse(it.Cantidad),
                    Precio = it.Sku.Equals("Z446482") ? int.Parse(it.Precio) : precio,
                    SubTotal =
                        it.Sku.Equals("Z446482")
                            ? int.Parse(it.Cantidad)*int.Parse(it.Precio)
                            : int.Parse(it.Cantidad)*precio,
                    SkuDimerc = it.Sku
                };
                ret.AddDetalleCompra(dt);
            }
            return ret;
        }

        public static OrdenCompraIntegracion AdapterClinicaAlemanaFormatToCompraIntegracion(this global::Integraciones.Utils.OrdenCompra.OrdenCompra oc)
        {
            return oc.AdapterGenericFormatToCompraIntegracion();
        }
    }
}