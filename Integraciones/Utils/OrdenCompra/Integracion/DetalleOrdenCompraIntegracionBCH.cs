﻿namespace Integraciones.Utils.OrdenCompra.Integracion
{
    public class DetalleOrdenCompraIntegracionBCH
    {
        public int ID { get; set; }
        public string NumPed { get; set; }

        public string SkuDimerc { get; set; }

        public double Cantidad { get; set; }

        public double Precio { get; set; }
        public double SubTotal { get; set; }

        public TipoBodega CodigoBodega = TipoBodega.SANTIAGO;

        public override string ToString()
        {
            return $"NumPed: {NumPed}, SKU: {SkuDimerc}, Cantidad: {Cantidad}, " +
                   $"Precio: {Precio}, SubTotal: {SubTotal}, Estado Pareo: {EstadoPareoDetalle}, " +
                   $"Estado Warning: {WarningDetalle}, Estado Error: {ErrorDetalle}, " +
                   $"Bodega: {CodigoBodega}";
        }

        public EstadoPareoDetalle EstadoPareoDetalle { get; set; }

        public WarningDetalle WarningDetalle { get; set; }

        public ErrorDetalle ErrorDetalle { get; set; }

        public bool EqualDetalleProcesado(DetalleOrdenCompraIntegracion det)
        {
            return SkuDimerc.Equals(det.SkuDimerc) && Cantidad == det.Cantidad && Precio == det.Precio;
        }
    }

    public enum TipoBodegaBCH
    {
        SANTIAGO = 1,
        ANTOFAGASTA = 66
    }
    public enum EstadoPareoDetalleBCH
    {
        SKU_PAREADO = 0,
        SKU_NO_PAREADO = 1,
        SKU_PAREADO_AUTOMATICAMENTE = 2,
        SKU_NO_EXISTE = 3,
        NO_ENCUENTRA_DESCRIPCION_PRODUCTO = 4,
        NO_ENCUENTRA_PAREO_AUTOMATICO = 5,
    }

    public enum WarningDetalleBCH
    {
        SIN_WARNING = 0,
        POSIBLE_ERROR_MULTIPLOS= 1,
        POSIBLE_ERROR_CANTIDAD = 2,
        POSIBLE_ERROR_PRECIO = 3,
        PRECIO_MAYOR_QUE_PDF = 4,
        PRECIO_MENOR_QUE_PDF = 5,
    }

    public enum ErrorDetalleBCH
    {
        SIN_ERROR= 0,
        ERROR_TOTAL_PRODUCTO = 1,
        ERROR_CANTIDAD_PRODUCTO = 2,
        SKU_NO_ENCONTRADO_EN_ORDEN = 3
    }

    public enum TipoPareoReCodCliBCH
    {
        PAREO_MANUAL = 0,
        PAREO_HISTORIAL_CLIENTE = 1,
        PAREO_PRODUCTOS_ACTIVOS = 2,
        PAREO_PRODUCTOS_MAESTRA = 3,
        EJECUTIVO_DEBE_PAREAR_CODIGO = 4,
        PAREO_POR_SKU_DIMERC = 5,
        PAREO_EXISTENTE_DESCRIPCION_DIFERENTE = 6
    }
}