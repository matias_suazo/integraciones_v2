﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.OrdenCompra.TraspasoWeb
{
    class OrdenWebRutPrv
    {
        public int Numped { get; set; }
        public int RutCli { get; set; }
        public int Cencos { get; set; }
        public string OcCliente { get; set; }
        public string Obs { get; set; }
        public int CodigoBodega { get; set; }
        public int Empresa { get; set; }

        public int ClaVta { get; set; }

        public string SkuDimerc { get; set; }

        public int Cantidad { get; set; }

        public int Precio { get; set; }
        public int RutCliPrv { get; set; }
    }
}
