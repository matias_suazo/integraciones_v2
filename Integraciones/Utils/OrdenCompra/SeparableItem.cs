﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.OrdenCompra
{
    public class SeparableItem : Item
    {
        
        public string SeparableFilter { get; set; }
        public string CentroCosto { get; set; }

        public override string ToString()
        {
            return base.ToString() + $", SeparableFilter: {SeparableFilter}";
        }
    }
}
