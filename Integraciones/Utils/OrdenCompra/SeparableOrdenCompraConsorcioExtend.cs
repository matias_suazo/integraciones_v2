﻿using System.Collections.Generic;
using System.Linq;

namespace Integraciones.Utils.OrdenCompra
{
    public class SeparableOrdenCompraConsorcioExtend : SeparableOrdenCompra
    {
        public List<OrdenCompra> GetAllOrdenCompraConsorcio()
        {
            var sortedItems = new List<SeparableItem>(SeparableItems.OrderByDescending(it => it.SeparableFilter));
            var retunList = new List<OrdenCompra>();
            var lastFilter = "";
            foreach (SeparableItem item in sortedItems)
            {
                if (lastFilter.Equals(item.SeparableFilter))
                {
                    retunList.ElementAt(retunList.Count - 1).AddItem(item.Clone());
                }
                else
                {
                    var oc = ConsorcioClone(item.SeparableFilter);
                    lastFilter = item.SeparableFilter;
                    retunList.Add(oc);
                    retunList.ElementAt(retunList.Count - 1).CentroCosto = item.CentroCosto;
                    retunList.ElementAt(retunList.Count - 1).AddItem(item.Clone());
                }

            }
            return retunList;
        }

        public OrdenCompra ConsorcioClone(string tissue)
        {
            return new OrdenCompra
            {
                CentroCosto = this.CentroCosto,
                CodigoEmpresa = this.CodigoEmpresa,
                Direccion = this.Direccion,
                NumeroCompra = bool.Parse(tissue) ? $"TISSUE: {this.NumeroCompra}": this.NumeroCompra,
                Observaciones = this.Observaciones,
                Rut = this.Rut,
                TipoIntegracion = this.TipoIntegracion,
                TipoPareoCentroCosto = this.TipoPareoCentroCosto,
                Repeticiones = this.Repeticiones
            };

        }
    }
}
