﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.OrdenCompra
{
    public class SeparableOrdenCompra : OrdenCompra
    {
        public SeparableOrdenCompra()
        {
            SeparableItems = new List<SeparableItem>();
        }

        public List<SeparableItem> SeparableItems { get; set; }

        public void AddItemSecuritas(SeparableItem it)
        {
            SeparableItems.Add(it);
        }

        public List<OrdenCompra> GetAllOrdenCompra()
        {
            var sortedItems = new List<SeparableItem>(SeparableItems.OrderByDescending(it => it.SeparableFilter));
            var retunList = new List<OrdenCompra>();
            var lastFilter = "";
            foreach (SeparableItem item in sortedItems)
            {
                if (lastFilter.Equals(item.SeparableFilter))
                {
                    retunList.ElementAt(retunList.Count - 1).AddItem(item.Clone());
                }
                else
                {
                    lastFilter = item.SeparableFilter;
                    retunList.Add(Clone());
                    retunList.ElementAt(retunList.Count - 1).CentroCosto = item.CentroCosto;
                    retunList.ElementAt(retunList.Count - 1).AddItem(item.Clone());
                }

            }
            return retunList;
        }
    
        public override string ToString()
        {
            var items = SeparableItems.Aggregate("", (current, item) => current + string.Format($"{item}\n"));
            return $"Rut: {Rut}, N° Compra: {NumeroCompra}, Centro Costo: {CentroCosto}, Observaciones: {Observaciones}, Dirección: {Direccion}\nItems:\n{items}";
        }

        public void AddSeparableItem(SeparableItem it)
        {
            SeparableItems.Add(it);
        }

        public OrdenCompra Clone()
        {
            return new OrdenCompra
            {
                CentroCosto = this.CentroCosto,
                CodigoEmpresa = this.CodigoEmpresa,
                Direccion = this.Direccion,
                NumeroCompra = this.NumeroCompra,
                Observaciones = this.Observaciones,
                Rut = this.Rut,
                TipoIntegracion = this.TipoIntegracion,
                TipoPareoCentroCosto = this.TipoPareoCentroCosto,
                Repeticiones = this.Repeticiones
            };

        }
    }
}
