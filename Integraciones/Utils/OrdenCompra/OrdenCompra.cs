﻿using System;
using System.Collections.Generic;
using System.Linq;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Readers.Text;

namespace Integraciones.Utils.OrdenCompra
{
    public class OrdenCompra
    {
        public OrdenCompra()
        {
            Items = new List<Item>();
            Rut = "";
            NumeroCompra = "";
            CentroCosto = "";
            Observaciones = "";
            Direccion = "";
            CodigoEmpresa = CodigoEmpresa.DIMERC;
            TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
            TipoIntegracion = TipoIntegracion.PDF;
            RutProveedor = "";//solo para clientes gobierno linea aseo y tissue
        }

        public TipoIntegracion TipoIntegracion { get; set; }

        public TipoPareoCentroCosto TipoPareoCentroCosto { get; set; }

        public string Direccion
        {
            get { return _direccion.Replace("'", "''"); }
            set { _direccion = value; }
        }

        private string _direccion;
        
        public string Rut
        {
            get { return _rut; }
            set
            {
                _rut = value.Replace(" ","");
                if (_rut.Contains('-'))
                {
                    _rut = _rut.Split('-')[0];
                }
                _rut = _rut.Replace(".", "");
            }
        }

        private string _rut;
        public string NumeroCompra { get; set; }
        public string CentroCosto { get; set; }
        public string Observaciones { get; set; }

        public string RutProveedor { get; set; }

        public List<Item> Items { get; set; }

        private CodigoEmpresa codigoEmpresa { get; set; }

        public CodigoEmpresa CodigoEmpresa
        {
            get
            {
                return codigoEmpresa == 0 ? CodigoEmpresa.DIMERC : codigoEmpresa;
            }
            set { codigoEmpresa = value; }
        }

        
        public string Repeticiones { get; internal set; }

        public void AddItem(Item it)
        {
            Items.Add(it);
        }

        public override string ToString()
        {
            //var items = Items.Aggregate("", (current, item) => current + string.Format($"{item}\n"));
            var cont = 1;
            var items = Items.Aggregate("", (current, ir) => current + $"{cont++}.- {ir}\n");
            return $"Rut: {Rut}, N° Compra: {NumeroCompra}, Centro Costo: {CentroCosto}, " +
                   $"Observaciones: {Observaciones}, Dirección: {Direccion}\nTipoIntegración: {TipoIntegracion}\nItems:\n{items}";
        }

        public void ConsoleWriteLine()
        {
            Console.WriteLine(ToString());

        }

    }

    /// <summary>
    /// Tipo de Pareo de Centro de Costo
    /// </summary>
    public enum TipoPareoCentroCosto
    {
        SIN_PAREO = 0,
        PAREO_DESCRIPCION_EXACTA = 1,
        PAREO_DESCRIPCION_LIKE = 2,
        PAREO_DESCRIPCION_MATCH = 3,
        PAREO_DESCRIPCION_DE_CLIENTE = 4,
        PAREO_CCOSTO_CONTACTO = 5  ,
        PAREO_CCOSTO_TATA = 6    ,
        PAREO_DESCCO_CONTACTO_TLMK = 7,
        PAREO_GILDEMEISTER = 8,
        PAREO_CCOSTO_HDI = 9,
        PAREO_CCOSTO_CONTACTO_HDI = 10,
        PAREO_COTIZACION = 11,
        PAREO_RE_DDESCLI = 12

    }

    public class OrdenCompraSecuritas : OrdenCompra
    {

        public OrdenCompraSecuritas()
        {
            ItemsSecuritas = new List<ItemSecuritas>();
        }

        public List<ItemSecuritas> ItemsSecuritas { get; set; }

        public void AddItemSecuritas(ItemSecuritas it)
        {
            ItemsSecuritas.Add(it);
        }

        public override string ToString()
        {
            var items = ItemsSecuritas.Aggregate("", (current, item) => current + string.Format($"{item}\n"));
            return $"Rut: {Rut}, N° Compra: {NumeroCompra}, Centro Costo: {CentroCosto}, Observaciones: {Observaciones}, Dirección: {Direccion}\nItems:\n{items}";
        }
    }

    public class OrdenCompraCarozzi : OrdenCompra
    {
        public OrdenCompraCarozzi()
        {
            ItemsCarozzi = new List<ItemCarozzi>();
        }

        public List<ItemCarozzi> ItemsCarozzi { get; set; }

        public void AddItemCarozzi(ItemCarozzi it)
        {
            ItemsCarozzi.Add(it);
        }

        public override string ToString()
        {
            var items = ItemsCarozzi.Aggregate("", (current, item) => current + string.Format($"{item}\n"));
            return $"Rut: {Rut}, N° Compra: {NumeroCompra}, Centro Costo: {CentroCosto}, Observaciones: {Observaciones}, Dirección: {Direccion}\nItems:\n{items}";
        }
    }

    public class OrdenCompraClinicaDavila : OrdenCompra
    {
        public OrdenCompraClinicaDavila()
        {
            ItemsClinicaDavila = new List<ItemDavila>();
        }
        public List<ItemDavila> ItemsClinicaDavila { get; set; }

        public void AddItemDavila(ItemDavila it)
        {
            ItemsClinicaDavila.Add(it);
        }
        public override string ToString()
        {
            var items = ItemsClinicaDavila.Aggregate("", (current, item) => current + string.Format($"{item}\n"));
            return $"Rut: {Rut}, N° Compra: {NumeroCompra}, Centro Costo: {CentroCosto}, Observaciones: {Observaciones}, Dirección: {Direccion}\nItems:\n{items}";
        }
    }
}