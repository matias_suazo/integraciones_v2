﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;

//using MODI;

namespace Integraciones.Utils
{
    public static class StringUtils
    {
        private static int _index;

        public static string ConvertirPrimeraLetraEnMayuscula(this string texto)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(texto.ToLower());
        }

        public static string ConvertStringToHex(this string asciiString)
        {
            return asciiString.Aggregate("", (current, c) => current + $"{Convert.ToUInt32(((int) c).ToString()):x2}").Replace("a0","20").Replace("c2a0", "");
        }

        private static string ConvertStringToHexWithoutBelValue(this string asciiString)
        {
            return asciiString.Aggregate("", (current, c) => current + $"{Convert.ToUInt32(((int)c).ToString()):x2}").Replace("07", "20");
        }

        public static string DeleteDuplicateWords(this string words)
        {
            var ret = "";
            foreach (var word in words.Split(' ').Where(word => !ret.Contains(word)))
            {
                ret += $" {word}";
            }
            return ret;
        }

        public static bool ContainsHexadecimalBelValue(this string asciiString)
        {
            return asciiString.Any(c => $"{Convert.ToUInt32(((int) c).ToString()):x2}".Equals("07"));
        }

        public static string DeletePlurals(this string words)
        {
            var ret = "";
            foreach (var split in words.ToUpper().Split(' '))
            {
                if (split.EndsWith("TES"))
                    ret += $" {split.Substring(0, split.Length - 1)}";
                else if (split.EndsWith("CES"))
                    ret += $" {split.Substring(0, split.Length - 3)}Z";
                else if (split.EndsWith("UES"))
                    ret += $" {split.Substring(0, split.Length - 3)}U";
                else if (split.EndsWith("ES"))
                    ret += $" {split.Substring(0, split.Length - 2)}";
                else if (split.EndsWith("AS"))
                    ret += $" {split.Substring(0, split.Length - 1)}";
                else if (split.EndsWith("OS"))
                    ret += $" {split.Substring(0, split.Length - 1)}";
                else if (split.EndsWith("PS"))
                    ret += $" {split.Substring(0, split.Length - 1)}";
                else
                    ret += $" {split}";
            }
            return ret;
        }

        public static string[] BorrarLineasBlancas(this string[] ret)
        {
            for (var i = 0; i < ret.Length; i++)
            {
                if (ret[i].Trim().Equals(""))
                {
                    ret[i] = "-1*";
                }
            }
            var ret2 = new List<string>();
            foreach (var x in ret)
            {
                if (!x.Equals("-1*"))
                {
                    ret2.Add(x);
                }
            }
            return ret2.ToArray();
        }


        public static bool isPatternSKUDimerc(this string sku)
        {
            return Regex.Match(sku, @"[a-zA-Z]{1,2}\d{4,6}").Success;

        }
        public static string ExtractTextFromImage(this string filePath)
        {
            //var modiDocument = new Document();
            //modiDocument.Create(Convert.ToString(filePath));
            //modiDocument.OCR(MiLANGUAGES.miLANG_ENGLISH);
            //var modiImage = (modiDocument.Images[0] as MODI.Image);
            //var extractedText = modiImage.Layout.Text;
            //modiDocument.Close();
            //return extractedText;
            return "Libreria no Encontrada";
        }

        public static string ConvertHexToString(this string hexValue)
        {
            var strValue = "";
            if (hexValue.Length < 2) return "";
            while (hexValue.Length > 0)
            {
                strValue += Convert.ToChar(Convert.ToUInt32(hexValue.Substring(0, 2), 16)).ToString();
                hexValue = hexValue.Substring(2, hexValue.Length - 2);
            }
            return strValue;
        }

        public static string DeleteNullHexadecimalValues(this string str)
        {
            return str.ConvertStringToHex().ConvertHexToString();
        }

        public static string DeleteBelHexadecimalValues(this string str)
        {
            return str.ConvertStringToHexWithoutBelValue().ConvertHexToString().DeleteContoniousWhiteSpace();
        }

        public static string NormalizeCentroCostoDavila(this string str)
        {
            return str.DeleteAcent().Replace("-", "").Replace("°", "").Replace("(", "").Replace(")", "").Replace("º","").Trim();
        }

        /// <summary>
        /// Elimina decimales en Precios o cantidades
        /// </summary>
        /// <param name="str">22.00</param>
        /// <returns>22</returns>
        public static string DeleteDecimal(this string str)
        {
            return str.Split('.')[0];
        }

        public static string DeleteDotComa(this string str)
        {
            return str.Replace(",", "").Replace(".","").Replace("$","");
        }

        /// <summary>
        /// Reemplaza una com "," por un punto "."
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ReplaceDot(this string str)
        {
            return str.Replace(",",".");
        }

        
        /// <summary>
        /// Elimina Espacios en Blanco repetidos
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DeleteContoniousWhiteSpace(this string str)
        {
            while (str.Contains("  "))
             {
                 str = str.Replace("  ", " ");
             }
            //str = System.Text.RegularExpressions.Regex.Replace(str, @"\s+", " ");
            //_index = 0;
            //var st = "";
            //var aux = str.ToCharArray();
            //for (; _index < aux.Length; _index++)
            //{
            //    if (aux[_index].Equals(' '))
            //    {
            //        st += DeleteAux2(aux);
            //    }
            //    else
            //    {
            //        st += aux[_index];
            //    }
            //}
            return str.Trim();
        }

        /// <summary>
        /// Borrar Tilde de String
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DeleteAcent(this string str)
        {
            return str
                .Replace("Á", "A")
                .Replace("É", "E")
                .Replace("Í", "I")
                .Replace("Ó", "O")
                .Replace("Ú", "U")
                .Replace("á", "a")
                .Replace("é", "e")
                .Replace("í", "i")
                .Replace("ó", "o")
                .Replace("ú", "u");
        }

        public static string FormattFolderName(this string str)
        {
            return str
                .Replace("Á", "A")
                .Replace("É", "E")
                .Replace("Í", "I")
                .Replace("Ó", "O")
                .Replace("Ú", "U")
                .Replace("á", "a")
                .Replace("é", "e")
                .Replace("í", "i")
                .Replace("ó", "o")
                .Replace("ú", "u")
                .Replace("/", "")
                .Replace("&", "")
                .Replace(":", "")
                .Replace(";", "")
                .Replace(".", "")
                .DeleteContoniousWhiteSpace();
        }

        public static string DeleteSymbol(this string str)
        {
            return str?.Replace("+", "").Replace("*", "").Replace("¨", "")
                .Replace("´", "").Replace("{", "").Replace("}", "")
                .Replace("[", "").Replace("]", "").Replace("^", "")
                .Replace("`", "").Replace("-", "").Replace("_", "")
                .Replace(":", "").Replace(".", "").Replace(",", "")
                .Replace(";", "").Replace("(", "").Replace(")", "")
                .Replace("=", "").Replace("?", "").Replace(@"\", "")
                .Replace("¿", "").Replace("¡", "").Replace("'", "")
                .Replace(@"/", "").Replace("&", "").Replace("%", "")
                .Replace("$", "").Replace("#", "").Replace("\"", "")
                .Replace("!", "").Replace("°", "").Replace("|", "")
                .Replace("¬", "").Replace("<", "").Replace(">", "")
                .Replace("~", "").Replace("{", "");
        }

        public static string ReplaceSymbolWhiteSpace(this string str)
        {
            return str.Replace("+", " ").Replace("*", " ").Replace("¨", " ")
                .Replace("´", " ").Replace("{", " ").Replace("}", "")
                .Replace("[", " ").Replace("]", " ").Replace("^", " ")
                .Replace("`", " ").Replace("-", " ").Replace("_", " ")
                .Replace(":", " ").Replace(".", " ").Replace(",", " ")
                .Replace(";", " ").Replace("(", " ").Replace(")", " ")
                .Replace("=", " ").Replace("?", " ").Replace("\\", " ")
                .Replace("¿", " ").Replace("¡", " ").Replace("'", " ")
                .Replace("/", " ").Replace("&", " ").Replace("%", " ")
                .Replace("$", " ").Replace("#", " ").Replace("\"", " ")
                .Replace("!", " ").Replace("°", " ").Replace("|", " ")
                .Replace("¬", " ").Replace("<", " ").Replace(">", " ")
                .Replace("~", " ").Replace("{", " ").DeleteContoniousWhiteSpace();
        }

        public static string DeleteNumber(this string str)
        {
            return str?.Replace("0", "").Replace("1", "").Replace("2", "")
                .Replace("3", "").Replace("4", "").Replace("5", "")
                .Replace("6", "").Replace("7", "").Replace("8", "")
                .Replace("9", "") ?? "";
        }

        private static string DeleteAux2(char[] c)
        {
            if (_index > c.Length) return "";
            if (!c[_index].Equals(' ') 
                || !c[_index + 1].Equals(' '))
                return c[_index++] + DeleteAux2(c);
            _index++;
            return "" + DeleteAux2(c);
        }

        /// <summary>
        /// Retorna las palabras consecutivas que más se repiten
        /// </summary>
        /// <param name="st1"></param>
        /// <param name="st2"></param>
        /// <returns></returns>
        private static string GetModaString(this string st1, string st2)
        {
            var print = "";
            var str1 = st1.Trim().Split(' ');
            var str2 = st2.Trim().Split(' ');
            for (var i = 1; i < str1.Length && i < str2.Length; i++)
            {
                if (str1[str1.Length - i].Equals(str2[str2.Length - i]))
                {
                    print += str1[str1.Length - i] + " ";
                }
            }
            return print.RevertString();
        }

        /// <summary>
        /// Invierte un String
        /// </summary>
        /// <param name="st">ANITA LAVA LA TINA</param>
        /// <returns>ANIT AL AVAL ATINA</returns>
        private static string RevertString(this string st)
        {
            var ret = "";
            var str = st.Split(' ');
            for (var i = str.Length - 1; i >= 0; i--)
            {
                ret += str[i] + " ";
            }
            return ret.Trim();
        }


       
        /// <summary>
        /// Retorna un String desde un Arrego, entre las posiciones dadas
        /// </summary>
        /// <param name="arg">Arreglo</param>
        /// <param name="from">Indice inicio</param>
        /// <param name="to">Indice fin</param>
        /// <returns></returns>
        public static string ArrayToString(this string[] arg, int from, int to = -1)
        {
            to = to == -1 ? arg.Length - 1 : to;
            var ret = "";
            if (to >= arg.Length)
                to = arg.Length - 1;
            for (; from <= to; from++)
            {
                ret += " " + arg[from];
            }
            return ret.Trim();
        }

        private static string GetMax(this IEnumerable<Lista> lista)
        {
            int[] max = {0};
            var ret = "";
            foreach (var l in lista.Where(l => l.Repeticiones > max[0] && !l.Cadena.Equals("")))
            {
                max[0] = l.Repeticiones;
                ret = l.Cadena;
            }
            return ret;
        }

        /// <summary>
        /// Obtiene Direccion de Formato Cencosud
        /// </summary>
        /// <param name="it">Lista de Items</param>
        /// <returns>Dirección</returns>
        public static string GetDireccionCencosud(this List<string> it)
        {
            var aux1 = new List<Lista>();
            var aux2 = new List<Lista>();
            for (var i = 0; i < it.Count; i += 2)
            {
                for (var j = i + 1; j < it.Count; j++)
                {
                    var str = it[i].GetModaString(it[j]);
                    aux1.Add(new Lista { Repeticiones = 0, Cadena = str });
                }
            }
            for (var i = 1; i < it.Count; i += 2)
            {
                for (var j = i + 1; j < it.Count; j++)
                {
                    var str = it[i].GetModaString(it[j]);
                    aux2.Add(new Lista { Repeticiones = 0, Cadena = str });
                }
            }
            foreach (var a in from a in aux1 from i in it.Where(i => i.Contains(a.Cadena)) select a)
            {
                a.Repeticiones++;
            }
            foreach (var a in from a in aux2 from i in it.Where(i => i.Contains(a.Cadena)) select a)
            {
                a.Repeticiones++;
            }
            var re1 = aux1.GetMax();
            var re2 = aux2.GetMax();
            if (!re1.Contains(re2)) return re1.Trim() +" "+ re2.Trim();
            return re1;
        }

        //public static string GetPalabraRepetida(this List<string> listaDescripciones, int tamañoMinimoPalabra, float porcentajeRepeticion)
        //{
        //    var ret = "";
        //    var firstSplit = listaDescripciones[0].Split(' ');
        //    var middleSplit = listaDescripciones[(listaDescripciones.Count-1)/2].Split(' ');
        //    var lastSplit = listaDescripciones[listaDescripciones.Count-1].Split(' ');
        //    var repeticionFirst = listaDescripciones.GetPalabraRepedida(firstSplit, tamañoMinimoPalabra, porcentajeRepeticion);
        //    var repeticionMiddle = listaDescripciones.GetPalabraRepedida(middleSplit, tamañoMinimoPalabra, porcentajeRepeticion);
        //    var repeticionLast = listaDescripciones.GetPalabraRepedida(lastSplit, tamañoMinimoPalabra, porcentajeRepeticion);
        //    if(repeticionFirst != null)
        //    {
        //        if (repeticionMiddle != null)
        //        {
        //            if (repeticionFirst.Equals(repeticionMiddle)) return repeticionFirst;
        //        }
        //        else if (repeticionLast != null)
        //        {
        //            if (repeticionFirst.Equals(repeticionMiddle)) return repeticionFirst;
        //        }
        //    }
        //    if (repeticionMiddle.Equals(repeticionLast)) return repeticionMiddle;
        //    return ret;
        //}

        public static void DeleteWord(this List<string> listaDescripciones, string delete)
        {
            if (string.IsNullOrEmpty(delete)) return;
            for (var i = 0; i < listaDescripciones.Count; i++)
            {
                listaDescripciones[i] = listaDescripciones[i]
                                    .Replace($" {delete} ", " ")
                                    .DeleteContoniousWhiteSpace();
            }
        }

        public static string GetPalabraRepedida(this List<string> listaDescripciones, string[] split, int tamañoMinimoPalabra, float porcentajeRepeticion, bool borrarArticulos = true)
        {
            var count = 0;
            foreach(var palabra in split)
            {
                if (Articulos.Contains(palabra) && borrarArticulos) continue;
                if (palabra.Length < tamañoMinimoPalabra) continue;
                foreach(var linea in listaDescripciones)
                {
                    foreach(var lin in linea.Split(' '))
                    {
                        if (palabra.Equals(lin))
                        {
                            count++;
                            break;
                        }
                    }
                }
                var porcentajeRepeticionReal = ((count + 1)*100)/ listaDescripciones.Count;
                if (porcentajeRepeticionReal >= porcentajeRepeticion) return palabra;                
            }
            return null;
        }


        public static void DeletePalabrasConsecutivasRepetidas(this List<Item> items)
        {
            var descripciones = items.Select(it => it.Descripcion).ToList();
            var rep2 = descripciones.GetPalabrasConsecutivasRepedidas(3);
            if (rep2.Equals("")) return;
            foreach (var t in items)
            {
                t.Descripcion = t.Descripcion.Replace(rep2, "").DeletePlurals().DeleteContoniousWhiteSpace();
            }
        }

        public static string GetPalabrasConsecutivasRepedidas(this List<string> listaDescripciones, int tamañoMinimoPalabra)
        {
            var listaArreglo = listaDescripciones.Select(descripcion => new List<string>(descripcion.Split(' '))).ToList();
            var repeticiones = "";
            var c = -1;
            var ret = "";
            var minLenght = listaArreglo.GetMinLengthGetFromArrayArray();
            var minLenghtSplit = listaArreglo.GetMinLength(minLenght);
            for (var i = 0; i < minLenght; i++)
            {
                var lastRep = minLenghtSplit[i];
                c = 0;
                for (var j = 0; j < listaArreglo.Count; j++)
                {
                    for (var k = 0; k < listaArreglo[j].Count; k++)
                    {
                        var actualPosicion = listaArreglo[j][k];
                        if (lastRep.Equals(actualPosicion)) c++;
                    }
                    if (c == listaArreglo.Count)
                        repeticiones += $" {lastRep}";
                }
            }
            return repeticiones.DeleteContoniousWhiteSpace();
        }

        public static int GetMinLengthGetFromArrayArray(this List<List<string>> lista)
        {
            return lista.Select(linea => linea.Count).Concat(new[] {int.MaxValue}).Min();
        }


        public static List<string> GetMinLength(this List<List<string>> lista, int length)
        {
            foreach (List<string> t in lista.Where(t => t.Count == length))
            {
                return t;
            }
            return null;
        }

        public static List<string> Articulos = new List<string>
        {
            "DE",
            "CON",
            "LA",
            "LOS",
        };
        
        public static string GetPatronRepeticion(this List<string> it)
        {
            var aux1 = new List<Lista>();
            var aux2 = new List<Lista>();
            for (var i = 0; i < it.Count; i += 2)
            {
                for (var j = i + 1; j < it.Count; j++)
                {
                    var str = it[i].GetModaString(it[j]);
                    aux1.Add(new Lista { Repeticiones = 0, Cadena = str });
                }
            }
            for (var i = 1; i < it.Count; i += 2)
            {
                for (var j = i + 1; j < it.Count; j++)
                {
                    var str = it[i].GetModaString(it[j]);
                    aux2.Add(new Lista { Repeticiones = 0, Cadena = str });
                }
            }
            foreach (var a in from a in aux1 from i in it.Where(i => i.Contains(a.Cadena)) select a)
            {
                a.Repeticiones++;
            }
            foreach (var a in from a in aux2 from i in it.Where(i => i.Contains(a.Cadena)) select a)
            {
                a.Repeticiones++;
            }
            var re1 = aux1.GetMax();
            var re2 = aux2.GetMax();
            if (!re1.Contains(re2)) return re1.Trim() + " " + re2.Trim();
            return re1;
        }

        public static string GetPatronRepeticionSinConsideracion(this List<string> it, List<string> replace)
        {
            var aux1 = new List<Lista>();
            var aux2 = new List<Lista>();
            for (var i = 0; i < it.Count; i++)
            {
                foreach (var rp in replace)
                    it[i] = it[i].Replace(rp, "").DeleteContoniousWhiteSpace();
            }
            for (var i = 0; i < it.Count; i += 2)
            {
                for (var j = i + 1; j < it.Count; j++)
                {
                    var str = it[i].GetModaString(it[j]);
                    aux1.Add(new Lista { Repeticiones = 0, Cadena = str });
                }
            }
            for (var i = 1; i < it.Count; i += 2)
            {
                for (var j = i + 1; j < it.Count; j++)
                {
                    var str = it[i].GetModaString(it[j]);
                    aux2.Add(new Lista { Repeticiones = 0, Cadena = str });
                }
            }
            foreach (var a in from a in aux1 from i in it.Where(i => i.Contains(a.Cadena)) select a)
            {
                a.Repeticiones++;
            }
            foreach (var a in from a in aux2 from i in it.Where(i => i.Contains(a.Cadena)) select a)
            {
                a.Repeticiones++;
            }
            var re1 = aux1.GetMax();
            var re2 = aux2.GetMax();
            if (!re1.Contains(re2)) return re1.Trim() + " " + re2.Trim();
            return re1;
        }
    }

    public class Lista
    {
        public int Repeticiones { get; set; }
        public string Cadena { get; set; }
    }
}