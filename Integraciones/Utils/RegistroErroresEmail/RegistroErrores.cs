﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.RegistroErroresEmail
{
    public class RegistroErrores
    {
        public RegistroErrores()
        { }

        public int id { get; set; }
        public int rutcli { get; set; }
        public string ejecutiva { get; set; }
        public string email_ejecutiva { get; set; }
        public string tipoError { get; set; } // problema lectura detalle de items, error al momento de procesar, error pdf desconocido.
        public string tipoIntegracion { get; set; } // excel, pdf,lectura mail,carga estandar
        public string descripcion { get; set; }
        public string nombre_archivo { get; set; }
        public int estado { get; set; } // enviado o no, por defecto 0
        public string fecha_creacion { get; set; }
        public string fecha_envio { get; set; }


    }
}
