﻿using Integraciones.Utils.Oracle.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.RegistroErroresEmail
{
    class RegistroErroresModel
    {
        public bool insertarRegistro(RegistroErrores objRegistroErrores)
        {            
            return OracleDataAccess.insertarRegistroErrores(objRegistroErrores);            
        }
        public List<RegistroErrores> listarRegistroErroresSinLeer()
        {
            return OracleDataAccess.listarRegistroErroresSinLeer();
        }

        public List<RegistroErrores> listarRegistroErroresSinLeerPorEmail(string email)
        {
            return OracleDataAccess.listarRegistroErroresSinLeerPorEmail(email);
        }
        public  List<string> obtenerEmailsEjecutivosNoLeidos()
        {
            return OracleDataAccess.obtenerEmailsEjecutivosNoLeidos();
        }

        public bool actualizarEstadoEnvio(RegistroErrores objEmail)
        {
            return OracleDataAccess.actualizarRegistroErroresEmail(objEmail);
        }

        public int EnvioCorreosErrores()
        {
            int correosEnviados = 0;
            List<string> emails = OracleDataAccess.obtenerEmailsEjecutivosNoLeidos();
            
            foreach (var email in emails)
            {
                List<RegistroErrores> listaErroresemail = listarRegistroErroresSinLeerPorEmail(email);
                string body = "";
                //string[] emailarray = new string[] {email };
                string[] emailarray = new string[] { email };
                //string[] ccarray = new string[] { "rreyes@dimerclabs.com" };
                var ccarray = InternalVariables.InternalVariables.GetMainEmail();
                foreach (var item in listaErroresemail)
                {
                    body += $"Tipo de error:{ item.tipoError}\r\n";
                    body += $"Tipo de Integracion:{item.tipoIntegracion}\r\n";
                    body += $"Descripción:{item.descripcion}\r\n";
                    body += $"Fecha de creación:{item.fecha_creacion}'\r\n";
                    body += $"______________________________________________________\r\n";
                    if (actualizarEstadoEnvio(item))
                    {

                    }

                }
                Email.EmailSender.SendEmailFromProcesosXmlDimerc(emailarray, ccarray, "Errores de integración", body);
                

                correosEnviados++;
            }
            return correosEnviados;

        }
    }
}
