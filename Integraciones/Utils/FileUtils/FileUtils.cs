﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.View;
using System.Security.Cryptography;

namespace Integraciones.Utils.FileUtils
{
    public static class FileUtils
    {

        public static string GetMD5FromFile(string fileName)
        {
            using (var md5 = MD5.Create())
            {//System.IO.Stream input = new System.IO.FileStream(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite)
                using (var stream = new FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))//File.OpenRead(fileName)
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty).ToLower();
                }
            }
        }
        public static void SaveStringToTxtFile(string path, string save)
        {
            try
            {
                var writer = new StreamWriter($"{path}", true);
                writer.WriteLine(save);
                writer.Close();
            }
            catch (Exception e)
            {
                Thread.Sleep(2000);
                SaveStringToTxtFile(path, save);
                Console.WriteLine(e.Message);
            }
        }

        public static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }
            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;
            return Encoding.ASCII;
        }

        public static Encoding ReadFile(string fileName)
        {
            var enc = Encoding.Default;
            var sr = new StreamReader(fileName, true);
            enc = sr.CurrentEncoding;
            return enc;
        }


        public static string ReaderFile(string fileName)
        {
            var ret = "";

            return ret;
        }
        public static void SaveUnknownFile(string filePath)
        {
            Log.Log.SaveUnknownFile(filePath);
        }

        public static void SaveErrorFile(string filePath)
        {
            Log.Log.SaveErrorFile(filePath);
        }


        public static void DeleteFile(string pdfPath)
        {
            //if (_onlyOne) return;
            //if (InternalVariables.IsDebug()) return;
            var fileName = pdfPath.Substring(pdfPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            try
            {
                Console.WriteLine($"Delete: {pdfPath}");
                File.Delete(pdfPath);
            }
            catch (Exception e)
            {
                Log.Log.TryError($"Error al momento de Eliminar el archivo: {fileName}...");
                Log.Log.TryError(e.Message);
                Log.Log.TryError(e.ToString());
            }
        }

        public static string MoveFileToProcessFolder(string pdfPath
            , OrdenCompra.OrdenCompra o, OrdenCompraIntegracion oIntegracion
            , bool soloDebug = false, bool cargaEstandar = false)
        {
            Console.WriteLine($"ESTADO ORDEN: {oIntegracion.EstadoOrden}");

            if (oIntegracion.EstadoOrden == EstadoOrden.ORDEN_REPETIDA)
            {
                return MoveFileToRepetidasFolder(pdfPath, o, soloDebug);
            }

            var tmpFileName = pdfPath.Substring(pdfPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            try
            {
                var folderRooth = "";
                var razon = OracleDataAccess.GetRazonSocial(o.Rut).DeleteSymbol();
                var folderName = cargaEstandar ? "CARGA ESTANDAR" : $"{o.Rut}-{razon.FormattFolderName()}";
                if (folderName.Contains("CARGA ESTANDAR"))
                {
                    folderName = $"{o.Rut}-{razon.FormattFolderName()}";
                    folderRooth = $@"{InternalVariables.InternalVariables.GetOcProcesadasFolderExc()}{folderName}\";
                }
                else {
                    folderRooth = $@"{InternalVariables.InternalVariables.GetOcProcesadasFolder()}{folderName}\";
                }
                var fileName = $"{DateTime.Now:dd-MM-yyyy-HH-mm-ss}_{tmpFileName}";
                //var fileName = $"{tmpFileName}";
                if (InternalVariables.InternalVariables.IsDebug()) return fileName;
                if (soloDebug) return fileName;
                if (!Directory.Exists(folderRooth))
                    Directory.CreateDirectory(folderRooth);
                Console.WriteLine($"Move: {pdfPath} \n to: {folderRooth}{fileName}");
                File.Move(pdfPath, $"{folderRooth}{fileName}");
                return fileName;
            }
            catch (Exception e)
            {
                Log.Log.TryError(
                    $"Error al momento de mover el archivo: {tmpFileName}, a carpetas de Ordenes Procesadas..");
                Log.Log.TryError(e.Message);
                Log.Log.TryError(e.ToString());
                return "";
            }
        }
        public static string MoveFileToProcessFolder(string pdfPath
            , OrdenCompra.OrdenCompra o, OrdenCompraIntegracionBCH oIntegracion
            , bool soloDebug = false, bool cargaEstandar = false)
        {
            Console.WriteLine($"ESTADO ORDEN: {oIntegracion.EstadoOrden}");
            if (oIntegracion.EstadoOrden == EstadoOrden.ORDEN_REPETIDA)
            {
                return MoveFileToRepetidasFolder(pdfPath, o, soloDebug);
            }

            var tmpFileName = pdfPath.Substring(pdfPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            try
            {
                var folderRooth = "";
                var razon = OracleDataAccess.GetRazonSocial(o.Rut).DeleteSymbol();
                var folderName = cargaEstandar ? "CARGA ESTANDAR" : $"{o.Rut}-{razon.FormattFolderName()}";
                if (folderName.Contains("CARGA ESTANDAR"))
                {
                    folderName = $"{o.Rut}-{razon.FormattFolderName()}";
                    folderRooth = $@"{InternalVariables.InternalVariables.GetOcProcesadasFolderExc()}{folderName}\";
                }
                else
                {
                    folderRooth = $@"{InternalVariables.InternalVariables.GetOcProcesadasFolder()}{folderName}\";
                }
                var fileName = $"{DateTime.Now:dd-MM-yyyy-HH-mm-ss}_{tmpFileName}";
                //var fileName = $"{tmpFileName}";
                if (InternalVariables.InternalVariables.IsDebug()) return fileName;
                if (soloDebug) return fileName;
                if (!Directory.Exists(folderRooth))
                    Directory.CreateDirectory(folderRooth);
                Console.WriteLine($"Move: {pdfPath} \n to: {folderRooth}{fileName}");
                File.Move(pdfPath, $"{folderRooth}{fileName}");
                return fileName;
            }
            catch (Exception e)
            {
                Log.Log.TryError(
                    $"Error al momento de mover el archivo: {tmpFileName}, a carpetas de Ordenes Procesadas..");
                Log.Log.TryError(e.Message);
                Log.Log.TryError(e.ToString());
                return "";
            }
        }


        /// <summary>
        /// Mover Archivo a Carpeta de Ordenes Repetidas
        /// </summary>
        /// <param name="pdfPath">Ruta PDF</param>
        /// <param name="o">Orden de Compra</param>
        /// <param name="onlyOne">Solo Procesar 1 Archivo?</param>
        /// <returns>Nombre del Fichero movido</returns>
        private static string MoveFileToRepetidasFolder(string pdfPath, OrdenCompra.OrdenCompra o, bool onlyOne)
        {
            var tmpFileName = pdfPath.Substring(pdfPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            try
            {
                var razon = OracleDataAccess.GetRazonSocial(o.Rut);
                var folderName = $"{o.Rut}-{razon.FormattFolderName()}";
                var folderRooth = $@"{InternalVariables.InternalVariables.GetOcRepetidasFolder()}{folderName}\";
                var fileName = $"{DateTime.Now:dd-MM-yyyy-HH-mm-ss}_{tmpFileName}";
                //var fileName = $"{tmpFileName}";
                if (onlyOne) return fileName;
                //if (InternalVariables.IsDebug()) return fileName;
                if (!Directory.Exists(folderRooth))
                    Directory.CreateDirectory(folderRooth);
                Console.WriteLine($"Move: {pdfPath} \n to: {folderRooth}{fileName}");
                File.Move(pdfPath, $"{folderRooth}{fileName}");
                return fileName;
            }
            catch (Exception e)
            {
                Log.Log.TryError($"Error al momento de mover el archivo: {tmpFileName}, a carpetas de Ordenes Procesadas..");
                Log.Log.TryError(e.Message);
                Log.Log.TryError(e.ToString());
                return "";
            }
        }



        public static void MoveUnknownFile()
        {
            var fichero = Log.Log.GetUnkwnownFileLog();
            if (!File.Exists(fichero)) return;
            string linea;
            var file = new StreamReader(fichero);
            while ((linea = file.ReadLine()) != null)
            {
                if (!File.Exists(linea)) continue;
                MoveUnknownFileToProcessFolder(linea);
            }
            file.Close();
            File.Delete(fichero);
        }

        public static void MoveErrorFile()
        {
            var fichero = Log.Log.GetErrorFileLog();
            if (!File.Exists(fichero)) return;
            string linea;
            var file = new StreamReader(fichero);
            while ((linea = file.ReadLine()) != null)
            {
                if (!File.Exists(linea)) continue;
                MoveErrorFileToProcessFolder(linea);
            }
            file.Close();
            File.Delete(fichero);
        }

        /// <summary>
        /// Mover un archivo con Formato Desconocido
        /// </summary>
        /// <param name="pdfPath">Ruta Archivo</param>
        private static void MoveUnknownFileToProcessFolder(string pdfPath)
        {
            //if (OnlyOne) return;
            //if (InternalVariables.IsDebug()) return;
            var fileName = pdfPath.Substring(pdfPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            try
            {
                var folderRooth = $@"{InternalVariables.InternalVariables.GetUnknownOcFolder()}";
                //var fileName = $"{DateTime.Now:dd-MM-yyyy-HH-mm-ss}_{tmpFileName}";
                if (!Directory.Exists(folderRooth))
                    Directory.CreateDirectory(folderRooth);
                Console.WriteLine($"Move: {pdfPath} \n to: {folderRooth}{fileName}");
                File.Move(pdfPath, $"{folderRooth}{fileName}");
            }
            catch (Exception e)
            {
                Log.Log.TryError($"Error al momento de mover el archivo: {fileName}, a carpetas de Formatos Desconocidos..");
                Log.Log.TryError(e.Message);
                Log.Log.TryError(e.ToString());
                App.RestarApplication();
            }
        }



        private static void MoveErrorFileToProcessFolder(string pdfPath)
        {
            //if (OnlyOne) return;
            //if (InternalVariables.IsDebug()) return;
            var fileName = pdfPath.Substring(pdfPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            try
            {
                var folderRooth = $@"{InternalVariables.InternalVariables.GetErrorOcFolder()}";
                if (!Directory.Exists(folderRooth))
                    Directory.CreateDirectory(folderRooth);
                Console.WriteLine($"Move: {pdfPath} \n to: {folderRooth}{fileName}");
                File.Move(pdfPath, $"{folderRooth}{fileName}");
            }
            catch (Exception e)
            {
                Log.Log.TryError($"Error al momento de mover el archivo: {fileName}, a carpetas de Formatos Desconocidos..");
                Log.Log.TryError(e.Message);
                Log.Log.TryError(e.ToString());
                App.RestarApplication();
            }
        }
    }
}
