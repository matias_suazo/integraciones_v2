﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Text.DragPharma
{
    class DragPharma
    {
        #region
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string> {
            {0, @"(\d{1,}\-\d{1,}\-\d{1,}|\d{1,}\-\d{1,})" },
            {1,@"(\d{1,}\/\d{1,}\/\d{1,}|\d{1,}\/\d{1,})" }

        };

        private const string RutPattern = "R.U.T.";
        private const string OrdenCompraPattern = "Nro.";

        private const string ItemsHeaderPattern = "Lin Producto";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Tienda :";

        //private bool _readCentroCosto;
        private bool _readOrdenCompra;

        private bool _readRut;

        //private bool _readObs;
        private bool _readItem;

        private readonly TextReader _txtReader;
        private readonly string[] _txtLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

#endregion

        public DragPharma(TextReader txtReader)
        {
            _txtReader = txtReader;
            _txtLines = _txtReader.ExtractTextToArray();
        }

        #region Funciones Get

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "80494200",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.TEXT
            };
            for (var i = 0; i < _txtLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_txtLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_txtLines[i]);
                        _readOrdenCompra = true;
                    }
                }
               /* if (!_readRut)
                {
                    if (IsRutPattern(_txtLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_txtLines[i]);
                        _readRut = true;
                    }
                }*/
                //if (!_readCentroCosto) {
                //    if (IsCentroCostoPattern(_txtLines[i])) {
                //        OrdenCompra.CentroCosto = GetCentroCosto(_txtLines[i+1].Trim());
                //        _readCentroCosto = true;

                //    }

                //}
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_txtLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_txtLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_txtLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_txtLines[i]))
                    {
                        var items = GetItems(_txtLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _txtReader.TxtFileNameOC;
            }
            return OrdenCompra;
        }

        private List<Item> GetItems(string[] txtLines, int i)
        {
            var items = new List<Item>();
            for (; i < txtLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = txtLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[1],
                            Cantidad = test0[test0.Length - 5].Trim().Split(',')[0],
                            Precio = test0[test0.Length - 4].Trim().Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE     ,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO                       
                        };

                        //OrdenCompra.CentroCosto = item0.SeparableFilter;
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var sku1 = GetSku(txtLines, i);
                        var item1 = new Item
                        {
                            Sku = sku1,
                            Cantidad = test1[test1.Length - 5].Trim().Split(',')[0],
                            Precio = test1[test1.Length - 4].Trim().Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };
                        if (!item1.Cantidad.Equals("0"))
                        {
                            items.Add(item1);
                        }
                        //OrdenCompra.CentroCosto = item0.SeparableFilter;
                        
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var ret = "";
            Match match = Regex.Match(str, @"OC\d{1,}");
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Is

        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }

        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private string GetSku(string[] pdfLInes, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLInes.Length; contador++)
            {
                Match match = Regex.Match(pdfLInes[contador], @"[aA-zZ]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }
            return ret;
        }
        #endregion
    }
}
#endregion