﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Text
{
    class B2bFalabella
    {
        #region
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string> {
            {0, @"^\d{6}\s|[A-Z]{1,2}\d{5,6}" }

        };

        private const string RutPattern = "R.U.T. :";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA :";

        private const string ItemsHeaderPattern = "NRO_OD|FECHA_EMISION_OD|NRO_OC|RUT|DV_RUT|RAZON_SOCIAL|FECHA_EMISION|UPC|SKU|DESCRIPCION_LARGA|MODELO|TALLA|COLOR|NRO_LOCAL|LOCAL|UNIDADES|EMPAQUES";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Tienda :";

        //private bool _readCentroCosto;
        private bool _readOrdenCompra;

        private bool _readRut;

        //private bool _readObs;
        private bool _readItem;

        private readonly TextReader _txtReader;
        private readonly string[] _txtLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public B2bFalabella(TextReader txtReader)
        {
            _txtReader = txtReader;
            _txtLines = _txtReader.ExtractTextToArray();
        }

        #region Funciones Get

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            List<OrdenCompra.OrdenCompra> listaOrdenes = new List<OrdenCompra.OrdenCompra>();
            
            for (var i = 1; i < _txtLines.Length; i++)
            {
                var aux = _txtLines[i].Split('|');
                if (aux.Equals(""))
                {
                    break;
                }
                var oc = aux[2];
                var sku = aux[10];
                var cantidad = aux[15];
                if (cantidad.Equals("0"))
                {
                    continue;
                }
                var cencos = aux[13];
                var items = new List<Item>();
                var item = new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                };
                items.Add(item);
                OrdenCompra = new OrdenCompra.OrdenCompra
                {
                    NumeroCompra = oc,
                    Rut = "77261280",
                    CentroCosto = cencos,
                    Items = items,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                    TipoIntegracion = TipoIntegracion.TEXT
                };
                 
            
                
                listaOrdenes.Add(OrdenCompra);
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _txtReader.TxtFileNameOC;
            }


            listaOrdenes = arregladorDeListadeOrdenes(listaOrdenes);
            return listaOrdenes;
        }

        private List<OrdenCompra.OrdenCompra>  arregladorDeListadeOrdenes(List<OrdenCompra.OrdenCompra> listaOrdenes)
        {
            List<OrdenCompra.OrdenCompra> listaNueva = new List<OrdenCompra.OrdenCompra>();
            
            var listaPorNumeroOC = obtenerNumeroCompraEnLaLista(listaOrdenes);

            foreach (var numerooc in listaPorNumeroOC)
            {
                var ordenesPorOc = obtenerOrdenesPorNumeroCompra(listaOrdenes, numerooc);

                var listaCencos = obtenerCentrosDeCostosEnLaLista(ordenesPorOc);
                foreach (var cc in listaCencos)
                {
                    var ocsbycc = obtenerOrdenesPorCC(ordenesPorOc, cc);
                    bool flagFinOrden = false;
                    var items = new List<Item>();

                    for (int i = 0; i < ocsbycc.Count; i++)
                    {
                        items.AddRange(ocsbycc[i].Items);
                        if (i == ocsbycc.Count - 1)
                        {
                            flagFinOrden = true;
                        }
                        if (flagFinOrden)
                        {
                            var oc = new OrdenCompra.OrdenCompra
                            {
                                NumeroCompra = ocsbycc[i].NumeroCompra,
                                Rut = ocsbycc[i].Rut,
                                CentroCosto = ocsbycc[i].CentroCosto,
                                Items = items,
                                TipoPareoCentroCosto = ocsbycc[i].TipoPareoCentroCosto,
                                TipoIntegracion = ocsbycc[i].TipoIntegracion
                            };
                            listaNueva.Add(oc);
                        }
                    }
                }
            }

            return listaNueva;
        }

        private List<OrdenCompra.OrdenCompra> obtenerOrdenesPorCC(List<OrdenCompra.OrdenCompra> listaOrdenes,string cencos)
        {
            List<OrdenCompra.OrdenCompra> listaNueva = new List<OrdenCompra.OrdenCompra>();
            listaNueva = listaOrdenes.Where(x => x.CentroCosto == cencos).ToList() ;
            return listaNueva;
        }

        private List<OrdenCompra.OrdenCompra> obtenerOrdenesPorNumeroCompra(List<OrdenCompra.OrdenCompra> listaOrdenes, string numeroCompra)
        {
            List<OrdenCompra.OrdenCompra> listaNueva = new List<OrdenCompra.OrdenCompra>();
            listaNueva = listaOrdenes.Where(x => x.NumeroCompra == numeroCompra).ToList();
            return listaNueva;
        }


        private List<string> obtenerCentrosDeCostosEnLaLista(List<OrdenCompra.OrdenCompra> listaOrdenes)
        {
            var listaCencos = new List<string>();
            foreach (var oc in listaOrdenes)
            {
                listaCencos.Add(oc.CentroCosto);
            }
            listaCencos = listaCencos.Distinct().ToList();
            return listaCencos;
        }

        private List<string> obtenerNumeroCompraEnLaLista(List<OrdenCompra.OrdenCompra> listaOrdenes)
        {
            var listaNumeroCompra = new List<string>();
            foreach (var oc in listaOrdenes)
            {
                listaNumeroCompra.Add(oc.NumeroCompra);
            }
            listaNumeroCompra = listaNumeroCompra.Distinct().ToList();
            return listaNumeroCompra;
        }

        private List<Item> GetItems(string[] txtLines, int i)
        {
            var items = new List<Item>();
            for (; i < txtLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = txtLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[0],
                            Cantidad = test0[test0.Length - 2].Trim().Split(',')[0],
                            Precio = test0[test0.Length - 9].Trim().Split(',')[0].Replace(".", ""),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_UNI,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };

                        if (EsSkuDimerc(item0.Sku))
                        {
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }

                        //OrdenCompra.CentroCosto = item0.SeparableFilter;
                        items.Add(item0);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        /// 
        private static bool EsSkuDimerc(string sku)
        {
            bool respuesta = false;
            Match match = Regex.Match(sku, @"[A-Z]{1,2}\d{5,6}");
            if (match.Success)
            {
                respuesta = true;
            }

            return respuesta;
        }
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var ret = "";
            var aux = str.Split(':');
            ret = aux[aux.Length - 1];
            return ret.Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Is

        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }

        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}
#endregion
