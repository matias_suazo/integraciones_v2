﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.AmgSA
{
    public class Maui
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.L

        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };
        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };
        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Maui(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 2;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                var ocCliente = "860"; // excelReader.GetExcelPoint(_ocCliente, 0); 
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Split(':')[0];
                var sku = excelReader.GetExcelPoint(_sku, fila).Split(':')[0];
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                var rutstr = excelReader.GetExcelPoint(_rut, 0);
                var rut = cutRut(rutstr);
                //var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                // var observacion = excelReader.GetExcelPoint(_observacion, fila);
                var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                //var fono = excelReader.GetExcelPoint(_fono, fila);
                var empresa = 3;

                if (!cantidad.Equals("")) { 
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    //TipoPrecioProducto = kk.Equals("99") ? TipoPrecioProducto.ARCHIVO_ADJUNTO : TipoPrecioProducto.TELEMARKETING
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                });
                }
                fila++;
                if (cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))

                    continue;

                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut.ToString(),
                    CentroCosto = cencos.ToUpper().Replace("LOCAL","").Replace("-"," ").Replace("LOC",""),
                    Items = listaItems,
                    Direccion = cencos,
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: {ocCliente}, CENCOS: {cencos}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH,
                   
                };
                fila++;
                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();

            }
            return OrdenesCompra;
        }

        private object cutRut(string rutstr)
        {
            var aux = rutstr.Split(':');
            var aux1 = aux[1].Replace(".","");
            return aux1;

        }
    }
}