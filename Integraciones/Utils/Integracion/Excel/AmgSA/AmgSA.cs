﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.AmgSA
{
    public class AmgSA
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.M
            
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.N
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _comuna = new ExcelPoint
        {
            Columna = ColumnaExcel.L
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public AmgSA(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 1;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Split(':')[0];
                var sku = excelReader.GetExcelPoint(_sku, fila).Split(':')[0];
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                var rut = excelReader.GetExcelPoint(_rut, fila);
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = excelReader.GetExcelPoint(_observacion, fila);
                var comuna = excelReader.GetExcelPoint(_comuna, fila);
                var empresa = 3;//excelReader.GetExcelPoint(_empresa, fila);
                var fono = excelReader.GetExcelPoint(_fono, fila).ReplaceSymbolWhiteSpace();
                //var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto =  TipoPrecioProducto.TELEMARKETING,
                    
                    
                });
                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila))) continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: {ocCliente}, RESPONSABLE: {observacion}, TELÉFONO: {fono} , DIRECCIÓN: {direccion}, {comuna}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE    
                };
                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }

            return OrdenesCompra;
        }
    }
}