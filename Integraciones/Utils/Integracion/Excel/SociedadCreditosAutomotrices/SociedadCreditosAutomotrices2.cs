﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.SociedadCreditosAutomotrices
{
    public class SociedadCreditosAutomotrices2
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.G,
            Fila = 1
        };
        private readonly ExcelPoint _sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.H,
            Fila = 1
        };
        private readonly ExcelPoint _sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.I,
            Fila = 1
        };
        private readonly ExcelPoint _sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.J,
            Fila = 1
        };
        private readonly ExcelPoint _sku4 = new ExcelPoint
        {
            Columna = ColumnaExcel.K,
            Fila = 1
        };
        private readonly ExcelPoint _sku5 = new ExcelPoint
        {
            Columna = ColumnaExcel.L,
            Fila = 1
        };
        private readonly ExcelPoint _sku6 = new ExcelPoint
        {
            Columna = ColumnaExcel.M,
            Fila = 1
        };
        private readonly ExcelPoint _sku7 = new ExcelPoint
        {
            Columna = ColumnaExcel.N,
            Fila = 1
        };
        private readonly ExcelPoint _sku8 = new ExcelPoint
        {
            Columna = ColumnaExcel.O,
            Fila = 1
        };
        private readonly ExcelPoint _sku9 = new ExcelPoint
        {
            Columna = ColumnaExcel.P,
            Fila = 1
        };
        private readonly ExcelPoint _sku10 = new ExcelPoint
        {
            Columna = ColumnaExcel.Q,
            Fila = 1
        };
        private readonly ExcelPoint _sku11 = new ExcelPoint
        {
            Columna = ColumnaExcel.R,
            Fila = 1
        };
        private readonly ExcelPoint _sku12 = new ExcelPoint
        {
            Columna = ColumnaExcel.S,
            Fila = 1
        };
        private readonly ExcelPoint _sku13 = new ExcelPoint
        {
            Columna = ColumnaExcel.T,
            Fila = 1
        };
        private readonly ExcelPoint _sku14 = new ExcelPoint
        {
            Columna = ColumnaExcel.U,
            Fila = 1
        };
        private readonly ExcelPoint _sku15 = new ExcelPoint
        {
            Columna = ColumnaExcel.V,
            Fila = 1
        };
        private readonly ExcelPoint _sku16 = new ExcelPoint
        {
            Columna = ColumnaExcel.W,
            Fila = 1
        };
        private readonly ExcelPoint _sku17 = new ExcelPoint
        {
            Columna = ColumnaExcel.X,
            Fila = 1
        };
        private readonly ExcelPoint _sku18 = new ExcelPoint
        {
            Columna = ColumnaExcel.Y,
            Fila = 1
        };
        private readonly ExcelPoint _sku19 = new ExcelPoint
        {
            Columna = ColumnaExcel.Z,
            Fila = 1
        };
        private readonly ExcelPoint _sku20 = new ExcelPoint
        {
            Columna = ColumnaExcel.AA,
            Fila = 1
        };
        private readonly ExcelPoint _sku21 = new ExcelPoint
        {
            Columna = ColumnaExcel.AB,
            Fila = 1
        };
        private readonly ExcelPoint _sku22 = new ExcelPoint
        {
            Columna = ColumnaExcel.AC,
            Fila = 1
        };
        private readonly ExcelPoint _sku23 = new ExcelPoint
        {
            Columna = ColumnaExcel.AD,
            Fila = 1
        };
        private readonly ExcelPoint _sku24 = new ExcelPoint
        {
            Columna = ColumnaExcel.AE,
            Fila = 1
        };
        private readonly ExcelPoint _sku25 = new ExcelPoint
        {
            Columna = ColumnaExcel.AF,
            Fila = 1
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.O
        };
        private readonly ExcelPoint _cantidad_sku = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };
        private readonly ExcelPoint _cantidad_sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };
        private readonly ExcelPoint _cantidad_sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };
        private readonly ExcelPoint _cantidad_sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        private readonly ExcelPoint _cantidad_sku4 = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };
        private readonly ExcelPoint _cantidad_sku5 = new ExcelPoint
        {
            Columna = ColumnaExcel.L
        };
        private readonly ExcelPoint _cantidad_sku6 = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };
        private readonly ExcelPoint _cantidad_sku7 = new ExcelPoint
        {
            Columna = ColumnaExcel.N
        };
        private readonly ExcelPoint _cantidad_sku8 = new ExcelPoint
        {
            Columna = ColumnaExcel.O
        };
        private readonly ExcelPoint _cantidad_sku9 = new ExcelPoint
        {
            Columna = ColumnaExcel.P
        };
        private readonly ExcelPoint _cantidad_sku10 = new ExcelPoint
        {
            Columna = ColumnaExcel.Q
        };
        private readonly ExcelPoint _cantidad_sku11 = new ExcelPoint
        {
            Columna = ColumnaExcel.R
        };
        private readonly ExcelPoint _cantidad_sku12 = new ExcelPoint
        {
            Columna = ColumnaExcel.S
        };
        private readonly ExcelPoint _cantidad_sku13 = new ExcelPoint
        {
            Columna = ColumnaExcel.T
        };
        private readonly ExcelPoint _cantidad_sku14 = new ExcelPoint
        {
            Columna = ColumnaExcel.U
        };
        private readonly ExcelPoint _cantidad_sku15 = new ExcelPoint
        {
            Columna = ColumnaExcel.V
        };
        private readonly ExcelPoint _cantidad_sku16 = new ExcelPoint
        {
            Columna = ColumnaExcel.W
        };
        private readonly ExcelPoint _cantidad_sku17 = new ExcelPoint
        {
            Columna = ColumnaExcel.X
        };
        private readonly ExcelPoint _cantidad_sku18 = new ExcelPoint
        {
            Columna = ColumnaExcel.Y
        };
        private readonly ExcelPoint _cantidad_sku19 = new ExcelPoint
        {
            Columna = ColumnaExcel.Z
        };
        private readonly ExcelPoint _cantidad_sku20 = new ExcelPoint
        {
            Columna = ColumnaExcel.AA
        };
        private readonly ExcelPoint _cantidad_sku21 = new ExcelPoint
        {
            Columna = ColumnaExcel.AB
        };
        private readonly ExcelPoint _cantidad_sku22 = new ExcelPoint
        {
            Columna = ColumnaExcel.AC
        };
        private readonly ExcelPoint _cantidad_sku23 = new ExcelPoint
        {
            Columna = ColumnaExcel.AD
        };
        private readonly ExcelPoint _cantidad_sku24 = new ExcelPoint
        {
            Columna = ColumnaExcel.AE
        };
        private readonly ExcelPoint _cantidad_sku25 = new ExcelPoint
        {
            Columna = ColumnaExcel.AF
        };
        //private readonly ExcelPoint _cantidad_sku23 = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.AB
        //};

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.B

        };
        private readonly ExcelPoint _nxtobservacion = new ExcelPoint
        {
            Columna = ColumnaExcel.C

        };


        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _local = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _comuna = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public SociedadCreditosAutomotrices2(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {

            var fila = 2;
            var listaItems = new List<Item>();
            var contadorFila = 1;
            while (fila <= excelReader.Hojas[0].Filas.Count - 1 && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                //var ocCliente = excelReader.ExcelFileName;
                DateTime now = DateTime.Now;

                var ocCliente = $"{now.ToString("MMM")}_{ excelReader.GetExcelPoint(_comuna, fila)}";
                var cencos = "";
                try
                {

                    cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                    cencos = cencos.Substring(0, 5);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }


                var sku = excelReader.GetExcelPoint(_sku);
                var sku1 = excelReader.GetExcelPoint(_sku1);
                var sku2 = excelReader.GetExcelPoint(_sku2);
                var sku3 = excelReader.GetExcelPoint(_sku3);
                var sku4 = excelReader.GetExcelPoint(_sku4);
                var sku5 = excelReader.GetExcelPoint(_sku5);
                var sku6 = excelReader.GetExcelPoint(_sku6);
                var sku7 = excelReader.GetExcelPoint(_sku7);
                var sku8 = excelReader.GetExcelPoint(_sku8);
                var sku9 = excelReader.GetExcelPoint(_sku9);
                var sku10 = excelReader.GetExcelPoint(_sku10);
                var sku11 = excelReader.GetExcelPoint(_sku11);
                var sku12 = excelReader.GetExcelPoint(_sku12);
                var sku13 = excelReader.GetExcelPoint(_sku13);
                var sku14 = excelReader.GetExcelPoint(_sku14);
                var sku15 = excelReader.GetExcelPoint(_sku15);
                var sku16 = excelReader.GetExcelPoint(_sku16);
                var sku17 = excelReader.GetExcelPoint(_sku17);
                var sku18 = excelReader.GetExcelPoint(_sku18);
                var sku19 = excelReader.GetExcelPoint(_sku19);
                var sku20 = excelReader.GetExcelPoint(_sku20);
                var sku21 = excelReader.GetExcelPoint(_sku21);
                var sku22 = excelReader.GetExcelPoint(_sku22);
                var sku23 = excelReader.GetExcelPoint(_sku23);
                var sku24 = excelReader.GetExcelPoint(_sku24);
                var sku25 = excelReader.GetExcelPoint(_sku25);
                //var sku22 = "Z441204";
                //var sku23 = "A430421";
                //var sku24 = "E246004";

                var cantidad = excelReader.GetExcelPoint(_cantidad_sku, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku, fila);
                var cantidad1 = excelReader.GetExcelPoint(_cantidad_sku1, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku1, fila);
                var cantidad2 = excelReader.GetExcelPoint(_cantidad_sku2, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku2, fila);
                var cantidad3 = excelReader.GetExcelPoint(_cantidad_sku3, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku3, fila);
                var cantidad4 = excelReader.GetExcelPoint(_cantidad_sku4, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku4, fila);
                var cantidad5 = excelReader.GetExcelPoint(_cantidad_sku5, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku5, fila);
                var cantidad6 = excelReader.GetExcelPoint(_cantidad_sku6, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku6, fila);
                var cantidad7 = excelReader.GetExcelPoint(_cantidad_sku7, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku7, fila);
                var cantidad8 = excelReader.GetExcelPoint(_cantidad_sku8, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku8, fila);
                var cantidad9 = excelReader.GetExcelPoint(_cantidad_sku9, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku9, fila);
                var cantidad10 = excelReader.GetExcelPoint(_cantidad_sku10, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku10, fila);
                var cantidad11 = excelReader.GetExcelPoint(_cantidad_sku11, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku11, fila);
                var cantidad12 = excelReader.GetExcelPoint(_cantidad_sku12, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku12, fila);
                var cantidad13 = excelReader.GetExcelPoint(_cantidad_sku13, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku13, fila);
                var cantidad14 = excelReader.GetExcelPoint(_cantidad_sku14, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku14, fila);
                var cantidad15 = excelReader.GetExcelPoint(_cantidad_sku15, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku15, fila);
                var cantidad16 = excelReader.GetExcelPoint(_cantidad_sku16, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku16, fila);
                var cantidad17 = excelReader.GetExcelPoint(_cantidad_sku17, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku17, fila);
                var cantidad18 = excelReader.GetExcelPoint(_cantidad_sku18, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku18, fila);
                var cantidad19 = excelReader.GetExcelPoint(_cantidad_sku19, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku19, fila);
                var cantidad20 = excelReader.GetExcelPoint(_cantidad_sku20, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku20, fila);
                var cantidad21 = excelReader.GetExcelPoint(_cantidad_sku21, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku21, fila);
                var cantidad22 = (excelReader.GetExcelPoint(_cantidad_sku22, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku22, fila));
                
                var cantidad23 = (excelReader.GetExcelPoint(_cantidad_sku23, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku23, fila));
                var cantidad24 = (excelReader.GetExcelPoint(_cantidad_sku24, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku24, fila));
                var cantidad25 = (excelReader.GetExcelPoint(_cantidad_sku25, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku25, fila));
                //var cantidad22 = excelReader.GetExcelPoint(_cantidad_sku22, fila);
                //var cantidad23 = excelReader.GetExcelPoint(_cantidad_sku23, fila);

                var precio = "1";//excelReader.GetExcelPoint(_precio, fila);
                var rut = "76547410"; //excelReader.GetExcelPoint(_rut, fila);
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var telefono = excelReader.GetExcelPoint(_fono, fila);
                //var observacion = $"CC: {cencos} CONTACTO: { excelReader.GetExcelPoint(_observacion, fila)} Local: {excelReader.GetExcelPoint(_local, fila)}";
                var observacion = $"{ excelReader.GetExcelPoint(_centroCosto, fila)}/{ excelReader.GetExcelPoint(_observacion, fila)}/{excelReader.GetExcelPoint(_fono, fila)}/{excelReader.GetExcelPoint(_comuna, fila)}";
                var empresa = 3;//excelReader.GetExcelPoint(_empresa, fila);
                                //var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                if (!cantidad.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad1.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku1,
                        Cantidad = cantidad1,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad2.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku2,
                        Cantidad = cantidad2,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad3.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku3,
                        Cantidad = cantidad3,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad4.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku4,
                        Cantidad = cantidad4,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad5.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku5,
                        Cantidad = cantidad5,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad6.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku6,
                        Cantidad = cantidad6,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad7.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku7,
                        Cantidad = cantidad7,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad8.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku8,
                        Cantidad = cantidad8,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad9.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku9,
                        Cantidad = cantidad9,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad10.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku10,
                        Cantidad = cantidad10,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad11.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku11,
                        Cantidad = cantidad11,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad12.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku12,
                        Cantidad = cantidad12,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad13.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku13,
                        Cantidad = cantidad13,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad14.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku14,
                        Cantidad = cantidad14,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad15.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku15,
                        Cantidad = cantidad15,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad16.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku16,
                        Cantidad = cantidad16,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad17.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku17,
                        Cantidad = cantidad17,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad18.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku18,
                        Cantidad = cantidad18,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad19.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku19,
                        Cantidad = cantidad19,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad20.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku20,
                        Cantidad = cantidad20,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad21.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku21,
                        Cantidad = cantidad21,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad22.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku22,
                        Cantidad = cantidad22,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad23.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku23,
                        Cantidad = cantidad23,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad24.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku24,
                        Cantidad = cantidad24,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad25.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku25,
                        Cantidad = cantidad25,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                //if (!cantidad22.Contains("0") || !cantidad22.Contains(""))
                //{
                //    listaItems.Add(new Item
                //    {
                //        Sku = sku22,
                //        Cantidad = cantidad22,
                //        Descripcion = "",
                //        Precio = precio,
                //        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                //        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                //    });


                //}
                //if (!cantidad23.Contains("0") || !cantidad22.Contains(""))
                //{
                //    listaItems.Add(new Item
                //    {
                //        Sku = sku23,
                //        Cantidad = cantidad23,
                //        Descripcion = "",
                //        Precio = precio,
                //        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                //        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                //    });


                //}

                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    rut.Contains(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Contains(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = $"{ocCliente}",
                    Observaciones = $"{observacion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE
                };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
                Console.WriteLine("FILA " + contadorFila);
                contadorFila++;
            }
            return OrdenesCompra;
        }
    }
}