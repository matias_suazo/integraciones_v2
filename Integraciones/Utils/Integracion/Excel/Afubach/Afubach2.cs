﻿using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.Afubach
{
    class Afubach2
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 0
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 4
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 1
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A    
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Afubach2(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public bool esSKuDimerc(string sku)
        {
            bool respuesta = false;
            Match match = Regex.Match(sku, @"[A-Z]{1,2}\d{5,6}");
            respuesta = match.Success;
            return respuesta;
        }

        public string getCC(string valor)
        {
            if (Regex.Match(valor, @"Despacho").Success)
            {
                return "0";
            }
            else if (Regex.Match(valor, @"Retiro").Success)
            {
                return "1";
            }
            else return "-1";
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 1;
            
            var canthojas = excelReader.Worksheets.Count;
            Console.WriteLine($"======= CANTIDAD DE HOJAS {canthojas}");
            for (int contHoja = 0; contHoja < canthojas; contHoja++)
            {
                fila = 0;
                var listaItems = new List<Item>();
                Console.WriteLine($"========== HOJA {contHoja} =============");
                // foreach (var row in excelReader.Worksheets[contHoja].Rows.Row)
               
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, -1, contHoja).ToString();
                var cencos = excelReader.GetExcelPoint(_centroCosto, -1,contHoja).Trim();
                //cencos = getCC(cencos);
                //var rut = "76792110";
                var rut = excelReader.GetExcelPoint(_rut, -1, contHoja);

                var direccion = excelReader.GetExcelPoint(_direccionEntrega, 3);
                var observacion = $"Contacto {excelReader.GetExcelPoint(_observacion, 2)}, Cui {direccion}";

                while (fila < excelReader.Hojas[contHoja].Filas.Count)
                {
                    if (fila >= 9)
                    {
                        var sku = excelReader.GetExcelPoint(_sku, fila,contHoja).Trim().ToUpper();
                        if (!esSKuDimerc(sku))
                        {
                            fila++;
                            continue;
                        }
                        var cantidad = excelReader.GetExcelPoint(_cantidad, fila, contHoja).Trim();
                        cantidad = cantidad.Split(' ')[0];
                        if (cantidad.Equals(""))
                        {
                            cantidad = "0";
                        }
                        if (!cantidad.Equals("0"))
                        {
                            listaItems.Add(new Item
                            {
                                Sku = sku,
                                Cantidad = cantidad,
                                Descripcion = "",
                                Precio = "1",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                                // TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                            });
                        }
                        else
                        {
                            fila++;
                            continue;
                        }
                    }
                    else {
                        fila++;
                        continue;
                    }


                    fila++;
                    
                }
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut.Trim(),
                    CentroCosto = (cencos.Equals("") ? "0" : cencos),
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: {ocCliente}, {observacion}",
                    CodigoEmpresa = (CodigoEmpresa)int.Parse("3"),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,

                };

                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}
