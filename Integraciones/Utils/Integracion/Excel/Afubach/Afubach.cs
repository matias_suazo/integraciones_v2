﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.Afubach
{
    class Afubach
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            //Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Fila = 4,
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Fila = 1,
            Columna = ColumnaExcel.B
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 2
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            //Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Afubach(ExcelReader _excelReader)
        {
            excelReader = _excelReader;

        }

        public List<Item> GetItems(int fila, int filafin)
        {
            List<Item> lista = new List<Item>();
            for (; fila < filafin; fila++)
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);

                if (esSku(sku))
                {
                    var item = new Item
                    {
                        Sku = sku,
                        Precio = precio,
                        TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                    };
                    lista.Add(item);
                }
            }
            return lista;
        }
        public bool esSku(string valor)
        {
            Match match = Regex.Match(valor, @"[A-Z]{1,2}\d{5,6}");
            return (match.Success);
        }

        private static string GetOrdenCompra(string str)
        {
            var linea = str.Split(':');
            return linea[linea.Length - 1];
        }

        public int getCC(string valor)
        {
            if (Regex.Match(valor, @"Despacho").Success)
            {
                return 0;
            }
            else if (Regex.Match(valor, @"Retiro").Success)
            {
                return 1;
            }
            else return -1;            
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            List<OrdenCompra.OrdenCompra> OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
            var fila = 1;

            var columna = 6;
            // while (fila < excelReader.Hojas[0].Filas.Count)
            //{
            //aqui quiero recorrer las columnas
            var contadorFila = 2;
            var cencos = "0";
            var sku = "";
            // var ocCliente = GetOrdenCompra(excelReader.GetExcelPoint(_ocCliente, 1).ToString());

            var cantidad = "0";



            var canthojas = excelReader.Worksheets.Count;
            Console.WriteLine($"======= CANTIDAD DE HOJAS {canthojas}");
            
            for (int contHoja = 0; contHoja < canthojas; contHoja++)
            {
                
                Console.WriteLine($"========== HOJA {contHoja} =============");
                // foreach (var row in excelReader.Worksheets[contHoja].Rows.Row)
                var countRow = 0;
                var filaDondeEmpiezaLosCodigos = 0;
                
                foreach (var row in excelReader.Hojas[contHoja].Filas)
                {
                    bool codigoIdentificado = false;
                    List<Item> codigos = new List<Item>();
                    var ocCliente = GetOrdenCompra(excelReader.GetExcelPoint(_ocCliente, 1,contHoja).ToString());
                    for (int colu = 0; colu < 3; colu++)
                    {
                        var CabeceraDetalle = row.Columnas[colu].ToString();
                        if (!CabeceraDetalle.Contains("Codigo"))
                        {
                            continue;
                        }
                        else
                        {
                            Console.WriteLine("asdsad");
                            Console.ReadLine();
                            //var filaDondeEmpiezaLosCodigos = row;
                            filaDondeEmpiezaLosCodigos = countRow + 1;
                            codigoIdentificado = true;
                        }
                    }
                    if (!codigoIdentificado)
                    {
                        continue;
                    }else
                    {
                        codigos = GetItems(filaDondeEmpiezaLosCodigos, excelReader.Hojas[contHoja].Filas.Count);
                        var contadorFilaSku = filaDondeEmpiezaLosCodigos;
                        _centroCosto.Fila = 5;
                        _centroCosto.Columna = (ColumnaExcel)1;
                        cencos = getCC(excelReader.GetExcelPoint(_centroCosto)).ToString();
                        List<Item> listaItems = new List<Item>();
                        foreach (var cod in codigos)
                        {
                            cantidad = "0";
                            Console.WriteLine($"{cod.Sku} es igual a {excelReader.GetExcelPoint(_sku, contadorFilaSku)}? {cod.Sku.Equals(excelReader.GetExcelPoint(_sku, contadorFilaSku))}");
                            if (cod.Sku.Equals(excelReader.GetExcelPoint(_sku, contadorFilaSku)))
                            {
                                _cantidad.Fila = contadorFilaSku;
                                _cantidad.Columna = (ColumnaExcel)contadorFila;
                                cantidad = excelReader.GetExcelPoint(_cantidad);
                                cod.Cantidad = cantidad;
                            }
                            if (!(cod.Cantidad.Equals("") || cod.Cantidad.Equals("0") || cod.Cantidad.Equals(" ")))
                            {
                                listaItems.Add(cod);
                            }

                            contadorFilaSku++;
                        }
                        Console.WriteLine("fin comparacion de codigos, es hora de generar la oc");
                        var obsstr = $"SUCURSAL { excelReader.GetExcelPoint(_observacion, 3)} CONTACTO {excelReader.GetExcelPoint(_observacion)} ";

                        var occ = new OrdenCompra.OrdenCompra
                        {
                            Rut = "76792110",
                            Items = (from l in listaItems
                                     select new Item
                                     {
                                         Cantidad = l.Cantidad,
                                         Descripcion = l.Descripcion,
                                         Precio = l.Precio,
                                         Sku = l.Sku,
                                         TipoPareoProducto = l.TipoPareoProducto,
                                         TipoPrecioProducto = l.TipoPrecioProducto
                                     }
                                            ).ToList(),

                            CentroCosto = cencos,
                            TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                            NumeroCompra = ocCliente,
                            CodigoEmpresa = (CodigoEmpresa)int.Parse("3"),
                            TipoIntegracion = TipoIntegracion.EXCEL,
                            Observaciones = obsstr
                        };

                        Console.WriteLine($"OC: {occ}");
                        OrdenesCompra.Add(occ);
                    } // if codigo identificado
                        break;
                        Console.WriteLine($"LISTA OC: {OrdenesCompra.ToString()}");

                        contadorFila++;

                    
                    // break;
                    countRow++;
                    //break;
                }
            }
            return OrdenesCompra;
        }
    }
}
