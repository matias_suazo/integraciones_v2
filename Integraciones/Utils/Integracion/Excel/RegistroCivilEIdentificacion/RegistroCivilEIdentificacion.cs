﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Integracion.Excel.RegistroCivilEIdentificacion
{
    class RegistroCivilEIdentificacion
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila = 1
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Fila = 7
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public RegistroCivilEIdentificacion(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public List<Item> GetItems(int fila, int filafin)
        {
            List<Item> lista = new List<Item>();
            for (; fila < filafin; fila++)
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);

                if (esSku(sku))
                {
                    var item = new Item
                    {
                        Sku = sku,
                        Precio = precio,
                        Descripcion = "desc",
                        TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                    };
                    lista.Add(item);
                }
            }
            return lista;
        }
        public bool esSku(string valor)
        {
            //Match match = Regex.Match(valor, @"[A-Z]{1,2}\d{5,6}");
            //return (match.Success);
            return Oracle.DataAccess.OracleDataAccess.ExistProduct(valor);
        }



        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            List<OrdenCompra.OrdenCompra> OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
            var fila = 1;

            var columna = 4;
            // while (fila < excelReader.Hojas[0].Filas.Count)
            //{
            //aqui quiero recorrer las columnas
            var contadorFila = 1;
            var cencos = "0";
            var sku = "";

            var codigos = GetItems(2, excelReader.Hojas[0].Filas.Count);
            var cantidad = "0";
            bool fincencos = false;
            var ocCliente = "";
            foreach (var row in excelReader.Hojas[0].Filas)
            {


                //recorriendo fila
                var contadorColumna = 0;
                /* if (contadorFila >= 4)
                 {
                     sku = excelReader.GetExcelPoint(_sku, contadorFila);
                 }*/
                //var listaItems = new List<Item>();
                //List<Item> listaItems = new List<Item>();
                foreach (var column in row.Columnas)
                {

                    //recorriendo columnas      
                    if (contadorColumna >= 4)
                    {
                        _centroCosto.Fila = 5;
                        _centroCosto.Columna = (ColumnaExcel)contadorColumna;
                        cencos = excelReader.GetExcelPoint(_centroCosto);
                        if (cencos.Equals(""))
                        {
                            fincencos = true;
                            break;
                        }
                        var contadorFilaSku = 6;
                        List<Item> listaItems = new List<Item>();

                        foreach (var cod in codigos)
                        {
                            ocCliente = excelReader.GetExcelPoint(_ocCliente, 3);
                            Console.WriteLine("OC CLIENTE " + ocCliente);
                            cantidad = "0";
                            Console.WriteLine($"{cod.Sku} es igual a {excelReader.GetExcelPoint(_sku, contadorFilaSku)}? {cod.Sku.Equals(excelReader.GetExcelPoint(_sku, contadorFilaSku))}");
                            if (cod.Sku.Equals(excelReader.GetExcelPoint(_sku, contadorFilaSku)))
                            {
                                _cantidad.Fila = contadorFilaSku;
                                _cantidad.Columna = (ColumnaExcel)contadorColumna;
                                cantidad = excelReader.GetExcelPoint(_cantidad);
                                cod.Cantidad = cantidad;
                            }
                            if (!(cod.Cantidad.Equals("") || cod.Cantidad.Equals("0") || cod.Cantidad.Equals(" ")))
                            {
                                listaItems.Add(cod);
                            }

                            contadorFilaSku++;
                        }
                        Console.WriteLine("fin comparacion de codigos, es hora de generar la oc");
                        var obsstr = Oracle.DataAccess.OracleDataAccess.GetCencosDetalle(70072600, cencos);

                        var occ = new OrdenCompra.OrdenCompra
                        {
                            Rut = "61002004",
                            Items = (from l in listaItems
                                     select new Item
                                     {
                                         Cantidad = l.Cantidad,
                                         Descripcion = l.Descripcion,
                                         Precio = l.Precio,
                                         Sku = l.Sku,
                                         TipoPareoProducto = l.TipoPareoProducto,
                                         TipoPrecioProducto = l.TipoPrecioProducto
                                     }
                                         ).ToList(),

                            CentroCosto = cencos,
                            TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                            NumeroCompra = ocCliente,
                            CodigoEmpresa = (CodigoEmpresa)int.Parse("3"),
                            TipoIntegracion = TipoIntegracion.EXCEL,
                            Observaciones = ""
                        };

                        Console.WriteLine($"OC: {occ}");
                        if (occ.Items.Count > 0)
                        {
                            OrdenesCompra.Add(occ);
                        }
                        
                    }
                    //Console.WriteLine($"LISTA OC: {OrdenesCompra.ToString()}");
                    contadorColumna++;
                }


                contadorFila++;
                if (fincencos)
                {

                    break;
                }

            }
            return OrdenesCompra;
        }
    }
}
