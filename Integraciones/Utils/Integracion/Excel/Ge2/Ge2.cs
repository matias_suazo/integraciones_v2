﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.Utils.Integracion.Excel.Ge2 {
    public class Ge2 {
        #region ExcelColumns
        private readonly ExcelPoint _centroCosto = new ExcelPoint {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _sku = new ExcelPoint {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _precio = new ExcelPoint {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _responsable = new ExcelPoint {
            Columna = ColumnaExcel.D

        };

        private readonly ExcelPoint _ubicacion = new ExcelPoint {
            Columna = ColumnaExcel.E
        };
        #endregion

        private readonly ExcelReader _excelReader;

        private List<SeparableOrdenCompra> OrdenesCompra { get; }

        public Ge2(ExcelReader excelReader) {
            _excelReader = excelReader;
            OrdenesCompra = new List<SeparableOrdenCompra>();
        }

        public List<SeparableOrdenCompra> GetOrdenCompra() {
            var fila = 5;
            const string rut = "99545180";

            const int empresa = 3;
            var listaItems = new List<SeparableItem>();

            const string ocCliente = "GE2";

            while (fila < _excelReader.Hojas[0].Filas.Count) {
                //ocCliente = _excelReader.GetExcelPoint(_ocCliente, fila);
                var cencos = _excelReader.GetExcelPoint(_centroCosto,0);
                Console.WriteLine("CENTRO DE COSTO! -> " + cencos);
                var skuTmp = _excelReader.GetExcelPoint(_sku, fila);
                var cantidad = _excelReader.GetExcelPoint(_cantidad, fila);
                if(cantidad.Equals("") || cantidad.Equals("0"))
                {
                    continue;
                }
                var precio = _excelReader.GetExcelPoint(_precio, fila);
                //var ubicacion = _excelReader.GetExcelPoint(_ubicacion, fila);
                //var responsable = _excelReader.GetExcelPoint(_responsable, fila);
                //if (ocCliente.Equals("") || ocCliente.Equals("Sub-Total")) {
                //    fila++;
                //    continue;
                //}

                /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
                //var codigoCategoria = OracleDataAccess.getCodCatConsorcio(rut, skuTmp);
                //

                listaItems.Add(new SeparableItem {
                    Sku = skuTmp,
                    Cantidad = cantidad,
                    Descripcion = "",
                    SeparableFilter = cencos,
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,
                    CentroCosto = cencos
                });

                fila++;
                //if (ocCliente.Equals(_excelReader.GetExcelPoint(_ocCliente, fila)) &&
                //    cencos.Equals(_excelReader.GetExcelPoint(_centroCosto, fila)) || ocCliente.Equals(" "))
                //    continue;

                var ordenCompra = new SeparableOrdenCompra {
                    Rut = rut,
                    CentroCosto = cencos,
                    SeparableItems = listaItems,
                    Direccion = "",
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: GE2 CENTRO COSTO {cencos}  Fecha Recep.: {DateTime.Now.ToString("dd/MM/yyyy")}",
                    CodigoEmpresa = (CodigoEmpresa)empresa,
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE
                };

                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<SeparableItem>();
            }
            return OrdenesCompra;
        }
    }
}
