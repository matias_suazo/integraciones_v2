﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.AmgSA
{
    public class AdRetail
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };
        private readonly ExcelPoint _cantidad_sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.U
        }; private readonly ExcelPoint _cantidad_sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.V
        }; private readonly ExcelPoint _cantidad_sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.W
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.T

        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public AdRetail(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }
        
        


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 1;
            DateTime now = DateTime.Now;
            var mess = now.Month.ToString();
           
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                var sku1 = "H350120";
                var sku2 = "Z289340"; // este tenia multiplo 4
                var sku3 = "Z291620"; // este tenia multiplo 2
                var cantidad1 = excelReader.GetExcelPoint(_cantidad_sku1, fila);
                var cantidad2 = excelReader.GetExcelPoint(_cantidad_sku2, fila);
                var cantidad3 = excelReader.GetExcelPoint(_cantidad_sku3, fila);
                var precio = "1";
                var rut = excelReader.GetExcelPoint(_rut, fila);
                var razons = "0";
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = excelReader.GetExcelPoint(_observacion, fila);
                var fono = excelReader.GetExcelPoint(_fono, fila);
                var empresa = 3;
                if (rut.Contains("77555730"))
                {
                    razons = "DIJON";
                }
                else if (rut.Contains("82982300"))
                {
                    razons = "ABC DIN";
                }

                switch (mess)
                {
                    case "1": mess = "Enero"; break;
                    case "2": mess = "Feb"; break;
                    case "3": mess = "Marzo"; break;
                    case "4": mess = "Abril"; break;
                    case "5": mess = "Mayo"; break;
                    case "6": mess = "Junio"; break;
                    case "7": mess = "Julio"; break;
                    case "8": mess = "Agosto"; break;
                    case "9": mess = "Septiembre"; break;
                    case "10": mess = "Octubre"; break;
                    case "11": mess = "Noviembre"; break;
                    case "12": mess = "Diciembre"; break;
                }

                if (!cantidad1.Equals("0") && !cantidad1.Equals("")) {
                    listaItems.Add(new Item
                    {
                        Sku = sku1,
                        Cantidad = cantidad1,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });
                }
                if (!cantidad2.Equals("0") && !cantidad2.Equals(""))
                {
                listaItems.Add(new Item
                {
                    Sku = sku2,
                    Cantidad = cantidad2,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING


                });
                     }
                if (!cantidad3.Equals("0") && !cantidad3.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku3,
                        Cantidad = cantidad3,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,
                 });
                }
                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;


                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion.Replace("O'Higgins", "OHiggins"),
                    NumeroCompra = $"{ocCliente.Replace("'","")} - {mess} - {razons}",
                    Observaciones = $"Timbre Obligatorio  {direccion.Replace("'", "")} - {observacion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}