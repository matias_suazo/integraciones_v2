﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;

namespace Integraciones.Utils.Integracion.Excel.CorporacionEducacionalBC
{
    class BC
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };
        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        }; 

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public BC(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            //OrdenesCompra = new List<OrdenCompra.OrdenCompra>(); 
        }

        public List<Item> GetItems(int fila, int filafin)
        {
            fila = 2;
            List<Item> lista = new List<Item>();
            for (; fila < filafin; fila++)
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);

                if (esSku(sku))
                {
                    var xsku = GetSku(sku);
                    var item = new Item
                    {
                        Sku = xsku,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    lista.Add(item);
                }
            }

            return lista;
        }
        public string GetPrecio(string prec)
        {
            Match matchPr = Regex.Match(prec, @"PRECIO");
            return matchPr.Value;
        }

        public bool esPrecio(string prec)
        {
            Match matchPr = Regex.Match(prec, @"PRECIO");
            return (matchPr.Success);
        }

        public string GetSku(string valor)
        {
            Match match = Regex.Match(valor, @"[A-Z]{1,2}\d{5,6}");
            return match.Value;
        }
        public bool esSku(string valor)
        {
            Match match = Regex.Match(valor, @"[A-Z]{1,2}\d{5,6}");
            return (match.Success);
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            List<OrdenCompra.OrdenCompra> OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
            var contadorFila = 0;

            var cencos = "0";
            var sku = "";
            var ocCliente = excelReader.GetExcelPoint(_ocCliente, 0);
            var codigos = GetItems(2, excelReader.Hojas[0].Filas.Count);
            var cantidad = "0";
            bool fincencos = false;

            foreach (var row in excelReader.Hojas[0].Filas)
            {
                var occ = new OrdenCompra.OrdenCompra();
                Console.WriteLine(contadorFila);
                if (contadorFila == 0)
                {
                    contadorFila++;
                    continue;
                }

                var contadorColum = 2;
                //Recorriendo Columnas
                foreach (var column in row.Columnas)
                {
                    if (contadorColum >= 2)
                    {
                        _centroCosto.Fila = 1;
                        _centroCosto.Columna = (ColumnaExcel)contadorColum;
                        cencos = excelReader.GetExcelPoint(_centroCosto);
                        occ.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                        if (!Regex.IsMatch(cencos, @"\d"))
                        {
                            _centroCosto.Fila = 0;
                            _centroCosto.Columna = (ColumnaExcel)contadorColum;
                            cencos = excelReader.GetExcelPoint(_centroCosto);
                            occ.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;
                        }
                        if (cencos.Equals("") && _centroCosto.Fila == 0)
                        {
                            _centroCosto.Fila = 0;
                            cencos = excelReader.GetExcelPoint(_centroCosto);
                        }
                        if (cencos.Equals("4") || cencos.Equals("HUECHURABA"))
                        {
                            occ.Rut = "76017191";
                            cencos = "0";
                        }
                        if (cencos.Equals(""))
                        {
                            fincencos = true;
                            break;
                        }
                        
                        var contadorFilaSku = 2;
                        List<Item> listaItems = new List<Item>();
                        foreach (var cod in codigos)
                        {
                            cantidad = "0";
                            Console.WriteLine($"{cod.Sku} es igual a {GetSku(excelReader.GetExcelPoint(_sku, contadorFilaSku))}?{cod.Sku.Equals(GetSku(excelReader.GetExcelPoint(_sku, contadorFilaSku)))}");
                            if (cod.Sku.Equals(GetSku(excelReader.GetExcelPoint(_sku, contadorFilaSku))))
                            {
                                _cantidad.Fila = contadorFilaSku;
                                _cantidad.Columna = (ColumnaExcel)contadorColum;
                                cantidad = excelReader.GetExcelPoint(_cantidad);
                                cod.Cantidad = cantidad;
                            }
                            if (!(cod.Cantidad.Equals("") || cod.Cantidad.Equals("0") || cod.Cantidad.Equals("")))
                            {
                                listaItems.Add(cod);
                            }
                            contadorFilaSku++;
                        }

                        Console.WriteLine("fin comparación de codigos, Generar OC");

                        if (listaItems.Count > 0)
                        {
                            occ = new OrdenCompra.OrdenCompra
                            {
                                Rut = "96920840",
                                Items = (from l in listaItems
                                         select new Item
                                         {
                                             Cantidad = l.Cantidad,
                                             Precio = l.Precio,
                                             Sku = l.Sku,
                                             TipoPareoProducto = l.TipoPareoProducto,
                                             TipoPrecioProducto = l.TipoPrecioProducto
                                         }
                                ).ToList(),
                                CentroCosto = cencos,
                                NumeroCompra = ocCliente,
                                CodigoEmpresa = (CodigoEmpresa)int.Parse("3"),
                                TipoIntegracion = TipoIntegracion.EXCEL,
                                Observaciones = ""


                            };
                            Console.WriteLine($"OC: {occ}");
                            OrdenesCompra.Add(occ);
                            Console.WriteLine($"LISTA OC: {OrdenesCompra.ToString()}");
                        }
                        contadorColum++;

                    }

                }

                contadorFila++;
                if (fincencos)
                {
                    break;
                }
            }

            return OrdenesCompra;
        }
    }
}
