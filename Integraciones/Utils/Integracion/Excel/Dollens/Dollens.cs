﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Integracion.Excel.Dollens
{
    class Dollens
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.H,
            Fila = 171
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila = 5
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Dollens(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            var fila = 34;
            var listaItems = new List<Item>();
            var ocCliente = excelReader.GetExcelPoint(_ocCliente);
            //var rut = excelReader.GetExcelPoint(_rut);
            var rut = "52000190";
            var cencos = "0";
            var empresa = "3";
            OrdenCompra.OrdenCompra ordenCompra = null;
            while (fila < excelReader.Hojas[0].Filas.Count)
            {               
                //
                var sku = excelReader.GetExcelPoint(_sku, fila).Trim().ToUpper();
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = $"{excelReader.GetExcelPoint(_observacion, fila)}, {direccion}";
                
                //var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                //var rep = excelReader.GetExcelPoint(_repeticiones, 1);

                if (!sku.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    });

                }
                else
                {
                    break;
                }


                fila++;
                
               
                //OrdenesCompra.Add(ordenCompra);
                //listaItems = new List<Item>();
            }
            ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos,
                Items = listaItems,
                NumeroCompra = ocCliente,
                Observaciones = $"OC: {ocCliente}",
                CodigoEmpresa = (CodigoEmpresa)int.Parse(empresa),
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO

            };
            return ordenCompra;
        }
    }
}
