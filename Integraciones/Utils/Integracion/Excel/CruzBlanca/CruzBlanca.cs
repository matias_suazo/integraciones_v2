﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.AmgSA
{
    public class CruzBlanca
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.C,
            Fila = 0
        };
        private readonly ExcelPoint _contacto = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.C,
            Fila = 1
        };
        private readonly ExcelPoint _cantidad_sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };
        private readonly ExcelPoint _cantidad_sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };
        private readonly ExcelPoint _cantidad_sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        private readonly ExcelPoint _cantidad_sku4 = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };
        private readonly ExcelPoint _cantidad_sku5 = new ExcelPoint
        {
            Columna = ColumnaExcel.L
        };
        private readonly ExcelPoint _cantidad_sku6 = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };
        
        private readonly ExcelPoint _comuna = new ExcelPoint {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.C

        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public CruzBlanca(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }




        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 4;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                //var rut = "76005001";
                var rut = excelReader.GetExcelPoint(_rut);
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                if(cencos == "")
                {
                    break;
                }
                var ocCliente = excelReader.GetExcelPoint(_ocCliente);
                var sku1 = "H401391";
                var sku2 = "H401393";
                var sku3 = "Z436108";
                var sku4 = "Z402178";
                var sku5 = "Z205320";
                var sku6 = "Z402666";
                var cantidad1 = excelReader.GetExcelPoint(_cantidad_sku1, fila);
                var cantidad2 = excelReader.GetExcelPoint(_cantidad_sku2, fila);
                var cantidad3 = excelReader.GetExcelPoint(_cantidad_sku3, fila);
                var cantidad4 = excelReader.GetExcelPoint(_cantidad_sku4, fila);
                var cantidad5 = excelReader.GetExcelPoint(_cantidad_sku5, fila);
                var cantidad6 = excelReader.GetExcelPoint(_cantidad_sku6, fila);
                
                var precio = "1";
                var observacion = excelReader.GetExcelPoint(_observacion, fila)  ;
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var contacto = excelReader.GetExcelPoint(_contacto,fila);
                
               
                var empresa = 3;
               
                if (!cantidad1.Equals("0") && !cantidad1.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku1,
                        Cantidad = cantidad1,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });
                }
                if (!cantidad2.Equals("0") && !cantidad2.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku2,
                        Cantidad = cantidad2,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING


                    });
                }
                if (!cantidad3.Equals("0") && !cantidad3.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku3,
                        Cantidad = cantidad3,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING


                    });
                }
                if (!cantidad4.Equals("0") && !cantidad4.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku4,
                        Cantidad = cantidad4,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING


                    });
                }
                if (!cantidad5.Equals("0") && !cantidad5.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku5,
                        Cantidad = cantidad5,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING


                    });
                }
                if (!cantidad6.Equals("0") && !cantidad6.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku6,
                        Cantidad = cantidad6,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING


                    });
                }
                fila++;
                if (cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"CONTACTO: {contacto} -DIRECCION: {direccion.DeleteContoniousWhiteSpace()} - OC: {ocCliente}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();   
            }

            return OrdenesCompra;
        }
    }
}