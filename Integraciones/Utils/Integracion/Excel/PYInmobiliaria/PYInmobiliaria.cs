﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.PYInmobiliaria
{
    public class PYInmobiliaria
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.C

        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public PYInmobiliaria(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 11;
            var ocCliente = "";
            var cencos = "0";//excelReader.GetExcelPoint(_centroCosto, fila).Split(':')[0];
            var sku = "";
            var cantidad = "";
            var precio = "";
            var rut = "";
            var direccion = "";
            var observacion = "";
            var empresa = 3;

            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                     ocCliente = excelReader.GetExcelPoint(_ocCliente, 5);
                     cencos = excelReader.GetExcelPoint(_centroCosto, 3);
                     sku = excelReader.GetExcelPoint(_sku, fila);
                     cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                     precio = excelReader.GetExcelPoint(_precio, fila);
                     rut = excelReader.GetExcelPoint(_rut, 5);
                     direccion = excelReader.GetExcelPoint(_direccionEntrega, 6);
                     observacion = excelReader.GetExcelPoint(_observacion, 7);
                     empresa = 3;

                if (cantidad.Equals("") ||  cantidad.Equals("0") || cantidad.Equals("SUBTOTAL") || cantidad.Trim().Equals("IVA") || cantidad.Equals("TOTAL") || sku.Equals("")) { fila++; continue; }

                listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,

                    });
                fila++;

                    //if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    //    rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                    //    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    //    continue;
            }

            var ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos.ToUpper(),
                Items = listaItems,
                Direccion = direccion,
                NumeroCompra = $"{ocCliente} - {cencos}",
                Observaciones = $"OC: {ocCliente}, RECEPCIONISTA: {observacion}",
                CodigoEmpresa = (CodigoEmpresa)(empresa),
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH
            };

            OrdenesCompra.Add(ordenCompra);
            listaItems = new List<Item>();
            return OrdenesCompra;
        }
    }
}