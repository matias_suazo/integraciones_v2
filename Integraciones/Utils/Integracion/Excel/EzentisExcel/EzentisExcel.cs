﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Integracion.Excel.EzentisExcel
{
    class EzentisExcel
    {
       /* private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };*/

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.E,
            Fila = 4
        };
        private readonly ExcelPoint _descripcion = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelPoint _totales = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };



        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public EzentisExcel(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            var fila = 5;
            var listaItems = new List<Item>();
            var rut = "99548940";
            var cencos = "0";
            var direccion = "";
            var observacion = "";
            //var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
            var ocCliente = "ADJUDICACION ENERO A DICIEMBRE 2018";
            var empresa = "3";
            var totales = "";
            while (fila < excelReader.Hojas[0].Filas.Count)
            {
                
                var sku = "Z446482";
                
                //var cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                var descripcion = excelReader.GetExcelPoint(_descripcion, fila).Trim().ToUpper();
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                
                
               // var rut = excelReader.GetExcelPoint(_rut, fila);
                //var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                //var observacion = $"{excelReader.GetExcelPoint(_observacion, fila)}, {direccion}";
               /* var empresa = excelReader.GetExcelPoint(_empresa, fila);
                if (empresa == "")
                {
                    empresa = "3";
                }*/
               /* var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                var rep = excelReader.GetExcelPoint(_repeticiones, 1);*/

                if ((!cantidad.Equals("0") && !cantidad.Equals(""))  && !totales.Equals("TOTALES") )
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = descripcion,
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        //TipoPrecioProducto = kk.Equals("99") ? TipoPrecioProducto.ARCHIVO_ADJUNTO : TipoPrecioProducto.TELEMARKETING
                    });

                }


                fila++;
                totales = excelReader.GetExcelPoint(_totales, fila);
                if (totales.Equals("TOTALES"))
                {
                    break;
                }

                /* if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                     rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                     cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                     continue;*/

                //OrdenesCompra.Add(ordenCompra);
                //listaItems = new List<Item>();

            }
            var ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos,
                Items = listaItems,
                Direccion = direccion,
                NumeroCompra = ocCliente,
                Observaciones = $"OC: {ocCliente}, {observacion}",
                CodigoEmpresa = (CodigoEmpresa)int.Parse(empresa),
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO

            };
            return ordenCompra;
        }
    }
}
