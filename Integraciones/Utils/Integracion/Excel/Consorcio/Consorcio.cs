﻿using System.Collections.Generic;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.Utils.Integracion.Excel.Consorcio {
    public class Consorcio {
        #region ExcelColumns
        private readonly ExcelPoint _centroCosto = new ExcelPoint {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _sku = new ExcelPoint {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _precio = new ExcelPoint {
            Columna = ColumnaExcel.L
        };

        private readonly ExcelPoint _responsable = new ExcelPoint {
            Columna = ColumnaExcel.D

        };

        private readonly ExcelPoint _ubicacion = new ExcelPoint {
            Columna = ColumnaExcel.E
        };


        private readonly ExcelPoint _solicitud = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };
        #endregion

        private readonly ExcelReader _excelReader;

        private List<SeparableOrdenCompraConsorcioExtend> OrdenesCompra { get; }

        public Consorcio(ExcelReader excelReader) {
            _excelReader = excelReader;
            OrdenesCompra = new List<SeparableOrdenCompraConsorcioExtend>();
        }

        public List<SeparableOrdenCompraConsorcioExtend> GetOrdenCompra() {
            

            var fila = 5;
            string rut;
            var ocCliente = _excelReader.GetExcelPoint(_ocCliente, fila);
            var idSolicitud = _excelReader.GetExcelPoint(_solicitud, fila);
            if (ocCliente.Substring(0, 1) == "7" || ocCliente.Substring(0, 1) == "8") {
                rut = "99012000";
            }
            else {
                rut = "96654180";
            }

            const int empresa = 3;
            var listaItems = new List<SeparableItem>();

            while (fila < _excelReader.Hojas[0].Filas.Count && _excelReader.GetExcelPoint(_ocCliente, fila) != "Total Neto OC") {
                ocCliente = _excelReader.GetExcelPoint(_ocCliente, fila);
                idSolicitud = _excelReader.GetExcelPoint(_solicitud, fila);
                var cencos = _excelReader.GetExcelPoint(_centroCosto, fila);
                var skuTmp = _excelReader.GetExcelPoint(_sku, fila);
                var cantidad = _excelReader.GetExcelPoint(_cantidad, fila);
                var precio = _excelReader.GetExcelPoint(_precio, fila);
                var ubicacion = _excelReader.GetExcelPoint(_ubicacion, fila);
                var responsable = _excelReader.GetExcelPoint(_responsable, fila);
                if (ocCliente.Equals("") || ocCliente.Equals("Sub-Total")) {
                    fila++;
                    continue;
                }
                var codigoCategoria = OracleDataAccess.getCodCatConsorcio(rut, skuTmp);
                listaItems.Add(new SeparableItem {
                    Sku = skuTmp,
                    Cantidad = cantidad,
                    Descripcion = "",
                   // SeparableFilter = codigoCategoria.ToString(),
                    SeparableFilter = OracleDataAccess.isTissueProduct(ocCliente, rut, skuTmp).ToString(),
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,
                    CentroCosto = cencos
                });

                fila++;
                if (ocCliente.Equals(_excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    cencos.Equals(_excelReader.GetExcelPoint(_centroCosto, fila)) || ocCliente.Equals(" "))
                    continue;

                var ordenCompra = new SeparableOrdenCompraConsorcioExtend
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    SeparableItems = listaItems,
                    Direccion = ubicacion,
                    // if order contains tissue                    
                    NumeroCompra =  $"{ ocCliente }",
                    Observaciones = $"ID-SOLICITUD: {idSolicitud}, RESPONSABLE: {responsable}, UBICACION: {ubicacion}", //POR DEFECTO SIEMPRE SE ANTEPONE EL OC (EN ORACLEDATAACCES.CS)
                    CodigoEmpresa = (CodigoEmpresa) empresa,
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE                  
                };

                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<SeparableItem>();
            }
            return OrdenesCompra;
        }
    }
}