﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.AmgSA
{
    public class Ripley
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad_sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };
        private readonly ExcelPoint _cantidad_sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        }; private readonly ExcelPoint _cantidad_sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _cantidad_sku4 = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        }; private readonly ExcelPoint _cantidad_sku5 = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        }; private readonly ExcelPoint _cantidad_sku6 = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };
        private readonly ExcelPoint _cantidad_sku7 = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        }; private readonly ExcelPoint _cantidad_sku8 = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        }; 

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.K

        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Ripley(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            
            var fila = 3;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count  && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {   var x = 0;
                var rut = excelReader.GetExcelPoint(_rut, fila);
                var rutlast = "rut";
                if (int.TryParse(rut,out x))
                {
                    rut = rutlast; 
                }
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
                var cencos = "0";
                var sku1 = "U445641";
                var sku2 = "U445642";
                var sku3 = "U445643";
                var sku4 = "U445644";
                var sku5 = "U445645";
                var sku6 = "U445646";
                var sku7 = "U445647";
                var sku8 = "U445648";
                var cantidad1 = excelReader.GetExcelPoint(_cantidad_sku1, fila);
                var cantidad2 = excelReader.GetExcelPoint(_cantidad_sku2, fila);
                var cantidad3 = excelReader.GetExcelPoint(_cantidad_sku3, fila);
                var cantidad4 = excelReader.GetExcelPoint(_cantidad_sku4, fila);
                var cantidad5 = excelReader.GetExcelPoint(_cantidad_sku5, fila);
                var cantidad6 = excelReader.GetExcelPoint(_cantidad_sku6, fila);
                var cantidad7 = excelReader.GetExcelPoint(_cantidad_sku7, fila);
                var cantidad8 = excelReader.GetExcelPoint(_cantidad_sku8, fila);
                var precio = "1";//excelReader.GetExcelPoint(_precio, fila);
                var observacion = excelReader.GetExcelPoint(_observacion, fila);
                var empresa = 3;
                if (!cantidad1.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku1,
                        Cantidad = cantidad1,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad2.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku2,
                        Cantidad = cantidad2,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad3.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku3,
                        Cantidad = cantidad3,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad4.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku4,
                        Cantidad = cantidad4,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad5.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku5,
                        Cantidad = cantidad5,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad6.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku6,
                        Cantidad = cantidad6,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad7.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku7,
                        Cantidad = cantidad7,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad8.Contains("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku8,
                        Cantidad = cantidad8,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }

                fila++;
                if (!rut.Equals(excelReader.GetExcelPoint(_rut, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rutlast,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = ".",
                    NumeroCompra = $"{ocCliente}",
                    Observaciones = $"{observacion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}