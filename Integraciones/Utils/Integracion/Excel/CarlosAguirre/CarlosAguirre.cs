﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;

namespace Integraciones.Utils.Integracion.Excel.CarlosAguirre
{
    class CarlosAguirre
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.B, Fila = 5
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.A, Fila = 2
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A, Fila = 2
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A, Fila = 10
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.B, Fila = 10
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.G, Fila = 10
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.A, Fila = 2
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.A, Fila = 2
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        
        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }
        public CarlosAguirre(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }
        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 1;
            var contadorHojas = 0;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[contadorHojas].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila).Trim();
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Trim();
                var sku = excelReader.GetExcelPoint(_sku, fila).Trim();
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila).Trim();
                var precio = excelReader.GetExcelPoint(_precio, fila).Trim();
                var rut = excelReader.GetExcelPoint(_rut, fila).Trim();
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = excelReader.GetExcelPoint(_observacion, fila);
                var empresa = 3;
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,


                });
                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: {ocCliente}, RESPONSABLE: {observacion}, DIRECCIÓN: {direccion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE
                };
                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}



//recorriendo fila
/*foreach (var column in row.Columnas)
{

    //recorriendo columnas      
    if (contadorColumna >= 3)
    {
        _centroCosto.Fila = 1;
        _centroCosto.Columna = (ColumnaExcel)contadorColumna;
        cencos = excelReader.GetExcelPoint(_centroCosto);
        if (!Regex.IsMatch(cencos, @"\d"))
        {
            cencos = "";
            _centroCosto.Fila = 0;
        }
        if (cencos.Equals("") && _centroCosto.Fila == 0)
        {
            _centroCosto.Fila = 1;
            cencos = excelReader.GetExcelPoint(_centroCosto);
        }

        if (cencos.Equals(""))
        {
            fincencos = true;
            break;
        }
        Console.WriteLine("fin comparacion de codigos, es hora de generar la oc");
        //var obsstr = Oracle.DataAccess.OracleDataAccess.GetCencosDetalle(70072600, cencos);

    }

    contadorColumna++;
}*/
