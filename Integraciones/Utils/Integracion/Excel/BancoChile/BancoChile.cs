﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.BancoChile
{
    public class BancoChile
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.X
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _ocCliente2 = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.P
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.R
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _solicitante = new ExcelPoint
        {
            Columna = ColumnaExcel.M

        };
        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };
        private readonly ExcelPoint _ciudad = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };
        private readonly ExcelPoint _region = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public BancoChile(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 1;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                var oc1 = excelReader.GetExcelPoint(_ocCliente, fila);
                var oc2 = excelReader.GetExcelPoint(_ocCliente2, fila);
                var ocCliente = $"{oc1} - {oc2}";
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Split(':')[0];
                
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = "1";
                var rut = "97004000";
               
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var ciudad  = excelReader.GetExcelPoint(_ciudad, fila);
                var region = excelReader.GetExcelPoint(_region, fila);
                var solicitante = excelReader.GetExcelPoint(_solicitante, fila);
                //var fono = excelReader.GetExcelPoint(_fono, fila);
                var empresa = 3;
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,


                });
                fila++;
                if (oc2.Equals(excelReader.GetExcelPoint(_ocCliente2, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"CC: {cencos}, O/C: {ocCliente}, SOL: {solicitante}, DESP: {direccion} {ciudad} {region}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}