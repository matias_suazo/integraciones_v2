﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.EandY
{
    class EandY
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.C,
            
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.E,
            Fila = 3
           
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C,
            Fila = 14
        };
        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public EandY(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {

            var fila = 6;
            var listaItems = new List<Item>();
            var rut = "77802430";
            var cencos = "0";
            var ocCliente = excelReader.GetExcelPoint(_ocCliente);
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                fila++;
                if (!cantidad.Equals("0") && !cantidad.Equals("")) { 

                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                });
                }
            }
            var ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos,
                Items = listaItems,
                Direccion = ".",
                NumeroCompra = ocCliente,
                Observaciones = $"OC: {ocCliente}",
                CodigoEmpresa = CodigoEmpresa.DIMERC,
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            return ordenCompra;
        }

    }
}