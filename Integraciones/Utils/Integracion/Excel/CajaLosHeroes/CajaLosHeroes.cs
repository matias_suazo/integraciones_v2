﻿using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Integracion.Excel.CajaLosHeroes
{
    class CajaLosHeroes
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,            
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public CajaLosHeroes(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public bool esSKuDimerc(string sku)
        {
            /*bool respuesta = false;
            Match match = Regex.Match(sku, @"[A-Z]{1,2}\d{5,6}");
            respuesta = match.Success;*/
            return OracleDataAccess.ExistProduct(sku);
            //return respuesta;
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 6;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count)
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila).ToUpper();
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Trim();
                var sku = excelReader.GetExcelPoint(_sku, fila).Trim().ToUpper();
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila).Trim();
                var precio = excelReader.GetExcelPoint(_precio, fila);
                if (precio.Equals("") || precio.Equals("."))
                {
                    precio = "1";
                }
                cantidad = cantidad.Split(' ')[0];
                if (cantidad.Equals(""))
                {
                    cantidad = "0";
                }
                var rut = excelReader.GetExcelPoint(_rut, fila);
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = $"{excelReader.GetExcelPoint(_observacion, fila)}, {direccion}";
                var empresa = excelReader.GetExcelPoint(_empresa, fila);
                if (empresa == "" || (!empresa.Equals("3") && !empresa.Equals("6")))
                {
                    empresa = "3";
                }
                var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                if (!Oracle.DataAccess.OracleDataAccess.GetUniNegocio(rut).Equals("GOBIERNO") && (!OracleDataAccess.GetCodCNLByRutusu(OracleDataAccess.GetRutUsuarioFromRutCliente(rut)).Equals("506")))
                {
                    kk = "";
                }

                var rep = excelReader.GetExcelPoint(_repeticiones, 1);

                if (!cantidad.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        // TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    });
                    fila++;
                }
                else
                {
                    fila++;
                    //continue;
                }



                Console.WriteLine("ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila).ToUpper()): " + ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila).ToUpper()));
                Console.WriteLine("rut.Equals(excelReader.GetExcelPoint(_rut, fila)): " + rut.Equals(excelReader.GetExcelPoint(_rut, fila)));
                Console.WriteLine("cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)): " + cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)));
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila).ToUpper()) &&
                    rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut.Trim(),
                    CentroCosto = (cencos.Equals("") ? "0" : cencos),
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: {ocCliente}, {observacion}",
                    CodigoEmpresa = (CodigoEmpresa)int.Parse(empresa),
                    TipoIntegracion = TipoIntegracion.CARGA_ESTANDAR,
                    //TipoPareoCentroCosto = rut.Equals("96555510") || rut.Equals("96.555.510") ? TipoPareoCentroCosto.PAREO_CCOSTO_TATA : TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                    Repeticiones = rep
                };
                if (ordenCompra.Rut.Equals("76042014") || ordenCompra.Rut.Equals("76232647") || ordenCompra.Rut.Equals("76134941") || ordenCompra.Rut.Equals("84000000") || ordenCompra.Rut.Equals("99577400") || ordenCompra.Rut.Equals("99577390") || ordenCompra.Rut.Equals("76134946") || ordenCompra.Rut.Equals("96867130") || ordenCompra.Rut.Equals("96900150") 
                    || ordenCompra.Rut.Equals("76833720") || ordenCompra.Rut.Equals("76473580"))
                {
                    ordenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                }
                if (ordenCompra.Rut.Equals("76434444") || ordenCompra.Rut.Equals("96555510") || ordenCompra.Rut.Equals("76547440"))
                {
                    //ordenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                    ordenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;
                    // ordenCompra.Observaciones = cencos;
                }
                OrdenesCompra.Add(ordenCompra);

                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}
