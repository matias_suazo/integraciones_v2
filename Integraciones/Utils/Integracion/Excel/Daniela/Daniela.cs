﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.Utils.Integracion.Excel.Daniela
{
    public class Daniela
    {

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _descripcion = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Daniela(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 18;
            var listaItems = new List<Item>();
            var rut = "82411500";
            while (fila < 111)
            {
                var sku = "SIN_SKU";
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var descripcion = excelReader.GetExcelPoint(_descripcion, fila);
                var precio = "1";
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = descripcion,
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                });
                fila++;
            }
            var ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = "0",
                Items = listaItems,
                Direccion = "",
                NumeroCompra = "OC_PAREO",
                Observaciones = $"OC: OC_PAREO",
                CodigoEmpresa = CodigoEmpresa.DIMERC,
                TipoIntegracion = TipoIntegracion.CARGA_ESTANDAR,
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            OrdenesCompra.Add(ordenCompra);
            return OrdenesCompra;
        }
    }
}