﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.Utils.Integracion.Excel.CargaEstandar
{
    public class Integramedica
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.P
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.N
        };

        //private readonly ExcelPoint _observacion = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.G
        //};

        //private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.H
        //};

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        //private readonly ExcelPoint _empresa = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.I
        //};

        //private readonly ExcelPoint _repeticiones = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.J
        //};

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Integramedica(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 2;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count)
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                var rut = "76098454";
                if (cencos.Contains("8100") || cencos.Contains("8110") || cencos.Contains("8120") || cencos.Contains("8130") || cencos.Contains("8140") 
                    || cencos.Contains("8150") || cencos.Contains("8190") || cencos.Contains("8301") || cencos.Contains("8300") || cencos.Contains("8302"))
                {
                    rut = "79716500";
                }
                //var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = $"Integramédica - {RelacionaCencos(cencos)}";
                var empresa = "3";
                var kk = "99";
                //var rep = excelReader.GetExcelPoint(_repeticiones, 1);

                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                    TipoPrecioProducto = kk.Equals("99")
                        ? TipoPrecioProducto.ARCHIVO_ADJUNTO
                        : TipoPrecioProducto.TELEMARKETING
                });
                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = ".",
                    NumeroCompra = ocCliente,
                    Observaciones = $"{observacion}",
                    CodigoEmpresa = (CodigoEmpresa)int.Parse(empresa),
                    TipoIntegracion = TipoIntegracion.CARGA_ESTANDAR,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH,
                    
                };
                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }

        private string RelacionaCencos(string cencos)
        {
            if (cencos.Contains("4500 MKT")){ cencos = "45001";}
            if (cencos.Contains("4500 CALL")) { cencos = "45002"; }
            if (cencos.Contains("4500 IMAG")) {cencos = "45003";}
            if (cencos.Contains("4500 UGP")) { cencos = "45004"; }
            if (cencos.Contains("4500 KINE")) { cencos = "45005"; }
            if (cencos.Contains("4500 GER")){cencos = "45006";}
            if (cencos.Contains("4500 GM")) { cencos = "45007";}
            if (cencos.Contains("4500 GG")) {cencos = "45008";}
            if (cencos.Contains("4500 GC")){ cencos = "45009";}
            var ret = "";
            var cencosParsed= 0;
            cencosParsed = int.Parse(cencos);
            switch (cencosParsed)
            {
                case 4010: ret = "IntegraMédica Barcelona-4010-IBA";break;
                case 4020: ret = "IntegraMédica Las Condes-4020-ICO"; break;
                case 4030: ret = "IntegraMédica Tobalaba-4030-ITO"; break;
                case 4040: ret = "IntegraMédica Oeste-4040-IMO"; break;
                case 4050: ret = "IntegraMédica Est. Central-4050-IEC"; break;
                case 4070: ret = "IntegraMédica La Florida-4070-IFS"; break;
                case 4080: ret = "IntegraMédica Centro-4080-ICE"; break;
                case 4090: ret = "IntegraMédica Alameda-4090-IPF"; break;
                case 4100: ret = "IntegraMédica Norte-4100-INO"; break;
                case 4120: ret = "IntegraMédica San Miguel-4120-ISM"; break;
                case 4130: ret = "IntegraMédica La Serena-4130-ILS"; break;
                case 4140: ret = "IntegraMédica El Trébol-4140-ITR"; break;
                case 4150: ret = "IntegraMédica Manquehue-4150-IMQ"; break;
                case 4160: ret = "IntegraMédica Puente Alto-4160-IPA"; break;
                case 4180: ret = "IntegraMédica Talca-4180-ITA"; break;
                case 4190: ret = "IntegraMédica Maipú-4190-IMP"; break;
                case 4200: ret = "IntegraMédica Bío- Bío-4200-IBB"; break;
                case 4210: ret = "IntegraMédica Bandera-4210-IBN"; break;
                case 4220: ret = "IntegraMédica Santa Lucía-4220-ISL"; break;
                case 4230: ret = "Integramedica Plaza Vespucio-4230-IPV"; break;
                case 4240: ret = "Integramedica Plaza Sur-4240-IPS"; break;
                case 4250: ret = "IntregraMédica Plaza Egaña-4250-IPE"; break;
                case 4260: ret = "Integramedica Viña del Mar-4260-IVM"; break;
                case 4280: ret = "IntegraMédica Rancagua-4280-IRA"; break;
                case 4300: ret = "Pilar Gazmuri-4300-PG"; break;
                case 45001: ret = "Gerencia de Marketing-4500-ISA"; break;
                case 45002: ret = "Call Center IntegraMédica-4500-ISA"; break;
                case 45003: ret = "Gerencia de Imágenes-4500-ISA"; break;
                case 45004: ret = "Unidad Gestión Paciente-4500-ISA"; break;
                case 45005: ret = "Kinevid-4500-ISA"; break;
                case 45006: ret = "IntegraMédica S.A.-4500-ISA"; break; 
                case 45007: ret = "Gerencia IntegraMédica-4500 - ISA"; break;
                case 45008: ret = "Gerencia Comercial-4500-ISA"; break;
                case 4600: ret = "CD INTEGRAMEDICA"; break;
                case 8100: ret = "Sonorad Bellotas-8100";break;
                case 8110: ret = "Sonorad Huerfanos-8110"; break;
                case 8120: ret = "Sonorad La Florida-8120"; break;
                case 8130: ret = "Sonorad Maipu-8130"; break;
                case 8140: ret = "Sonorad Viña-8140"; break;
                case 8150: ret = "Puente Alto-8150"; break;
                case 8190: ret = "Sonorad Independencia-8190"; break;
                case 8301:ret = "8301 Sonorad Alameda"; break;
                case 8300:ret = "8300 Sonorad Irarrazabal"; break;
                case 8302: ret = "8302 CD Sonorad I"; break;


            }
            return ret;

        }
    }
}