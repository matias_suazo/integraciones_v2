﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.Utils.Integracion.Excel.MineraAntucoya
{
    class MineraAntucoya
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila = 0
            //Fila = 1
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 6
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.G-1
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.F-1
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        private readonly ExcelPoint _solicitante = new ExcelPoint
        {
            Columna = ColumnaExcel.B-1,
            Fila = 2
        };


        private readonly ExcelReader excelReader;

        


        public MineraAntucoya(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
           
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            
            var fila = 8;
            var listaItems = new List<Item>();
            
            var rut = "76079669";
            
            rut = rut.Replace("RUT", "");

            var ocCliente = excelReader.GetExcelPoint(_solicitante);
            ocCliente = ocCliente.Split(':')[1];

            
            var cencos = "3";
            var solicitante = excelReader.GetExcelPoint(_solicitante);
            solicitante = solicitante.Split(':')[1];
            //var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
           
            var observacion = $", solicitante: {solicitante}";

            var ordenCompra = new OrdenCompra.OrdenCompra
            {

                Rut = rut,
                CentroCosto = cencos,                              
                NumeroCompra = ocCliente,
                Observaciones = $"OC: {ocCliente}, {observacion}",
                CodigoEmpresa = CodigoEmpresa.DIMERC,
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA
            };
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsNaNEmpty())
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = "1";

                if (!cantidad.Equals("0") && !cantidad.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku.Trim(),
                        Cantidad = cantidad.Trim(),
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    });
                }
                fila++;
                
            }
            ordenCompra.Items = listaItems;
                
            return ordenCompra;
        }
    }
}
