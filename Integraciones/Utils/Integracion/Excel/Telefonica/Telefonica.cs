﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.ProAndes
{
    class Telefonica
    {
        //private readonly ExcelPoint _rut = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.A,
        //    Fila = 2
        //};

        //private readonly ExcelPoint _centroCosto = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.C,
        //    Fila = 14
        //};

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.M,
            Fila = 3
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

       private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.AB
       };

        //private readonly ExcelPoint _observacion = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.B
        //};

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.D,
            Fila = 36
            
        };

        private readonly ExcelPoint _contacto = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 33
        };

        //private readonly ExcelPoint _poderOculto = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.K
        //};

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Telefonica(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            var fila = 46;
            var listaItems = new List<Item>();
            var ocCliente = excelReader.GetExcelPoint(_ocCliente);
            var cencos = "0";
            var contacto = excelReader.GetExcelPoint(_contacto);
            var rut = "59083900";
            var direccion = excelReader.GetExcelPoint(_direccionEntrega);
            var observacion = $", direccion: {direccion}, contacto: {contacto}";
            while(!excelReader.GetExcelPoint(_sku, fila).Equals(""))
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = "1";
                precio = excelReader.GetExcelPoint(_precio, fila);
                fila++;
                //if (sku.Equals("") || cantidad.Equals("") || precio.Equals(""))
                //    continue;
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    

                });
               
            }
            var ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos,
                Items = listaItems,
                Direccion = direccion,
                NumeroCompra = ocCliente,
                Observaciones = $"OC: {ocCliente}, {observacion}",
                CodigoEmpresa = CodigoEmpresa.DIMERC,
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA
            };
            return ordenCompra;
        }
    }
}