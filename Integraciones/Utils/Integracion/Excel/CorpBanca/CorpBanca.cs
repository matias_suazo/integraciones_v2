﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.CorpBanca
{
    class CorpBanca
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila = 0
            //Fila = 1
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 1
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        private readonly ExcelPoint _solicitante = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };


        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public CorpBanca(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            //excelReader.DeleteConsecutiveEmptyRow();
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 5;
            var listaItems = new List<Item>();
            //var rut = excelReader.GetExcelPoint(_rut);
            var rut = "97023000";
            //rut = rut.Replace("RUT:", "");
            rut = rut.Replace("RUT", "");

            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente);
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                var solicitante = excelReader.GetExcelPoint(_solicitante, fila);
                var direccion = excelReader.GetExcelPoint(_direccionEntrega,fila);
                var observacion = $", direccion: {direccion}, solicitante: {solicitante}";
  
            if(!cantidad.Equals("0") && !cantidad.Equals("")) { 
                listaItems.Add(new Item
                {
                    Sku = sku.Trim(),
                    Cantidad = cantidad.Trim(),
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                });
                }
                fila++;
                if (cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
            
            var ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos,
                Items = listaItems,
                Direccion = direccion,
                NumeroCompra = ocCliente,
                Observaciones = $"OC: {ocCliente}, {observacion}",
                CodigoEmpresa = CodigoEmpresa.DIMERC,
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA
            };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return  OrdenesCompra;
        }

        }
}