﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.Utils.Integracion.Excel.Logistex
{
    class Logistex
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.H,
            Fila = 171
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.K,
            Fila = 56
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.E,
            Fila = 2
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C,
            Fila = 16
        };

        private readonly ExcelPoint _poderOculto = new ExcelPoint
        {
            Columna = ColumnaExcel.K
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Logistex(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {

            var listaItems = new List<Item>();
            //var ocCliente = 

            //var rut = excelReader.GetExcelPoint(_rut);
            var rut = "76340240";
            var cencos = "0";
            var empresa = "3";
            var direccion = excelReader.GetExcelPoint(_direccionEntrega);
            var oc_cliente = excelReader.GetExcelPoint(_ocCliente);
            //var observacion = $"{excelReader.GetExcelPoint(_observacion, fila)}, {direccion}";
            OrdenCompra.OrdenCompra ordenCompra = null;
            var fila = 20;
            while (fila < excelReader.Hojas[0].Filas.Count)
            {
                //
                var sku = excelReader.GetExcelPoint(_sku, fila).Trim().ToUpper();
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);



                //var kk = excelReader.GetExcelPoint(_poderOculto, fila);
                //var rep = excelReader.GetExcelPoint(_repeticiones, 1);

                if (!sku.Equals(""))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = precio.Equals("") ? TipoPrecioProducto.TELEMARKETING : TipoPrecioProducto.ARCHIVO_ADJUNTO
                    });

                }
                else
                {
                    break;
                }


                fila++;


            }
            ordenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = rut,
                CentroCosto = cencos,
                Items = listaItems,
                NumeroCompra = oc_cliente,
                Observaciones = $"OC: {oc_cliente}",
                CodigoEmpresa = (CodigoEmpresa)int.Parse(empresa),
                TipoIntegracion = TipoIntegracion.EXCEL,
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA

            };
            return ordenCompra;
        }
    }
}
