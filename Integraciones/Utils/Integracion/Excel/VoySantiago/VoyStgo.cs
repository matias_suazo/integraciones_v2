﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using System;

namespace Integraciones.Utils.Integracion.Excel.VoySantiago
{
    class VoyStgo
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _repeticiones = new ExcelPoint
        {
            Columna = ColumnaExcel.O
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public VoyStgo(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        public bool esSKuDimerc(string sku)
        {
            /*bool respuesta = false;
            Match match = Regex.Match(sku, @"[A-Z]{1,2}\d{5,6}");
            respuesta = match.Success;*/
            return OracleDataAccess.ExistProduct(sku);
            //return respuesta;
        }

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 2;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count)
            {
                var ocCliente = excelReader.GetExcelPoint(_ocCliente, 0).ToUpper().Trim().Replace("oc ","").Replace("PS ","").Replace("OC ", "").Replace("ps ", "");
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Trim();
                var sku = excelReader.GetExcelPoint(_sku, fila).Trim().ToUpper();
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila).Trim();
                var precio = excelReader.GetExcelPoint(_precio, fila);
                if (precio.Equals("") || precio.Equals("."))
                {
                    precio = "1";
                }
                cantidad = cantidad.Split(' ')[0];
                if (cantidad.Equals(""))
                {
                    cantidad = "0";
                }
                var rut = excelReader.GetExcelPoint(_rut, 0).Replace("rut ","");
                if (rut.Equals("77166811"))
                {
                    ocCliente = excelReader.GetExcelPoint(_ocCliente, 0).ToUpper().Trim().Replace("oc ", "");
                }
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
                var observacion = $"{excelReader.GetExcelPoint(_observacion, fila)}";
                var empresa = "3";




                var rep = excelReader.GetExcelPoint(_repeticiones, 1);

                if (!cantidad.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "ItemDescripcion",
                        Precio = precio,
                        TipoPareoProducto = (esSKuDimerc(sku)) ? TipoPareoProducto.SIN_PAREO : TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                        TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        // TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    });
                    fila++;
                }
                else
                {
                    fila++;
                    //continue;
                }



                Console.WriteLine("ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila).ToUpper()): " + ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila).ToUpper()));
                Console.WriteLine("rut.Equals(excelReader.GetExcelPoint(_rut, fila)): " + rut.Equals(excelReader.GetExcelPoint(_rut, fila)));
                Console.WriteLine("cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)): " + cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)));
                if (cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila).Trim()) &&
                    direccion.Equals(excelReader.GetExcelPoint(_direccionEntrega, fila).Trim()))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut.Trim(),
                    CentroCosto = (cencos.Equals("") ? "0" : cencos),
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = observacion,
                    CodigoEmpresa = (CodigoEmpresa)int.Parse(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,

                    //TipoPareoCentroCosto = rut.Equals("96555510") || rut.Equals("96.555.510") ? TipoPareoCentroCosto.PAREO_CCOSTO_TATA : TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA,

                     
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA,
                    Repeticiones = rep
                };

                OrdenesCompra.Add(ordenCompra);

                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}

