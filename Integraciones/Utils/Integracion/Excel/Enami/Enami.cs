﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.PYInmobiliaria
{
    public class Enami
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
           
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.O
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.AD
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.AJ
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.J,
            

        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.M
        };

        private readonly ExcelPoint _empresa = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public Enami(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
         
            var fila = 1;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
               var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
               if (ocCliente.Contains("N° Orden de Compra"))  ocCliente = excelReader.GetExcelPoint(_ocCliente, fila+1);

                var cencos = excelReader.GetExcelPoint(_centroCosto, fila).Trim();
                var sku = excelReader.GetExcelPoint(_sku, fila);
                //if(sku.)
                var cantidad = excelReader.GetExcelPoint(_cantidad, fila); ;
                var precio = excelReader.GetExcelPoint(_precio, fila);
                var rut = "61703000";
                var observacion = excelReader.GetExcelPoint(_observacion);
                var direccion = ".";
                var empresa = 3;

                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,

                });
                fila++;

                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                   rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;

                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = $"{ocCliente}",
                    Observaciones = $"OC: {ocCliente}, DIRECCIÓN: {observacion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH
                };

                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
            }
            return OrdenesCompra;
        }
    }
}