﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Integraciones.Utils.Integracion.Excel.Tata
{
    public class Tata
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };
        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };
        private readonly ExcelPoint _atencion = new ExcelPoint
        {
            Columna = ColumnaExcel.A

        };
        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _mnttotal = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        //private readonly ExcelPoint _empresa = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.B
        //};

        private readonly ExcelReader excelReader;
        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }
        public Tata(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }
        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
           var fila = 4;
            //DateTimeFormatInfo thisDay = CultureInfo.CurrentCulture.DateTimeFormat; 
            //var mes = thisDay.GetMonthName(numeroMes);
            var ocCliente = "";
            var cencosFinal = "";
            var cencos = "0";
            var sku = "";
            var cantidad = "";
            var precio = "";
            var mnttotal = "";
            var rut = "76385060";
            var observacion = excelReader.GetExcelPoint(_atencion, fila);
            var observacionFinal = excelReader.GetExcelPoint(_atencion, fila);
            var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila);
            var direccionFinal = excelReader.GetExcelPoint(_direccionEntrega, fila);
            string dirF = "", obsF = "";
            var empresa = 3;
            var nameFileExc = excelReader.GetNameExcel();
            var listaItems = new List<Item>();
            var i = 0;
            bool direcionRead = false, observacionRead = false;
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                ocCliente = excelReader.GetExcelPoint(_ocCliente, fila);
                cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                cencos = GetCencos(cencos);
                observacion = excelReader.GetExcelPoint(_atencion, fila);
                direccion = excelReader.GetExcelPoint(_direccionEntrega, fila); 
                observacionFinal = excelReader.GetExcelPoint(_atencion, fila);
                direccionFinal = excelReader.GetExcelPoint(_direccionEntrega, fila);
                if(i == 28)
                {
                    Console.Write("SSS");
                }
                if(direccion.Contains("Favor entregar:") && !direcionRead)
                {
                    dirF = excelReader.GetExcelPoint(_atencion, fila+1);
                    direcionRead = true;
                }
                if (observacion.Contains("Atención:") && !observacionRead)
                {
                    obsF= excelReader.GetExcelPoint(_direccionEntrega, fila+1);
                    observacionRead = true;
                }

                if (Regex.Match(ocCliente, @"\s\d{7}$|\s\d{7}\s").Success)
                {
                    cencosFinal = ocCliente.ToUpper().Trim();
                   // cencosFinal = cencos;
             
                }
                sku = excelReader.GetExcelPoint(_sku, fila);
                cantidad = excelReader.GetExcelPoint(_cantidad, fila);
                precio = excelReader.GetExcelPoint(_precio, fila);
                mnttotal = excelReader.GetExcelPoint(_mnttotal, fila);
                empresa = 3;
                Console.WriteLine($"SKU:{sku} CANTIDAD:{cantidad} PRECIO:{precio} ");
                if (Regex.Match(sku, @"[a-zA-Z]{1,2}\d{5,6}").Success)
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });
                }

                //if (precio == "" && cantidad != "" && mnttotal != "" && sku == "")
                var largoVariables = sku + cantidad + precio;
                largoVariables = largoVariables.Trim();
                Console.WriteLine($"LARGO VARIABLES -- {largoVariables.Length}");
                if (largoVariables.Length == 0)
                {
                    Console.WriteLine();
                }
                if(cantidad.Contains("Total") || (largoVariables.Length ==0 && !mnttotal.Equals("")))
                {
                    direcionRead = false;
                    observacionRead = false;
                    var ordenCompra = new OrdenCompra.OrdenCompra
                    {
                        Rut = rut,
                        CentroCosto = cencosFinal,
                        Items = listaItems,
                        Direccion = direccion,
                        NumeroCompra = $"ABRIL MAYO JUNIO - TATA",
                        Observaciones = $"CC: {cencosFinal}, FAVOR ENTREGAR: {dirF}, ATENCIÓN: {obsF}",
                        CodigoEmpresa = (CodigoEmpresa)(empresa),
                        TipoIntegracion = TipoIntegracion.EXCEL,
                        // TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE
                        TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA
                    };
                    ordenCompra.ConsoleWriteLine();

                    OrdenesCompra.Add(ordenCompra);
                    listaItems = new List<Item>();
                  
                }

                fila++;
                i++;
            }

            return OrdenesCompra;
        }

        private static string GetCencos(string str)
        {
            var split = str.Split(' ');
            var cc = "0";
            var x=  0;
            int posicion = 0;
            for(var i=0;i< split.Length; i++)
            {
                if(Regex.Match(split[i], @"\d{7}").Success)
                {
                    cc = split[i];
                    if(i==split.Length-1)
                    posicion = i;
                    break;
                }
            }
            return cc;

            
        }
    }
}