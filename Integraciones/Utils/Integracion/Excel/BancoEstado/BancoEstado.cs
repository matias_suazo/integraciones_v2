﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;

namespace Integraciones.Utils.Integracion.Excel.BancoEstado
{
    public class BancoEstado
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.K

        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.L
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _fecha = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }

        public BancoEstado(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }
        public static DateTime Now { get; }

        DateTime hoy = DateTime.Today;

        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            //var ordenCompra = new OrdenCompra.OrdenCompra() ;
            for(var indexHoja = 0; indexHoja< excelReader.Hojas.Count; indexHoja++)
            {
                var fila = 1;
                var listaItems = new List<Item>();
                var ocCliente = "";
                var cencos = "";
                var rut = "";
                var direccion = "";
                var observacion = "";
                var fecha = "";
                var fecha_actual = "";
               /* var ocCliente = excelReader.GetExcelPoint(_ocCliente, fila, indexHoja).Trim();
                var cencos = excelReader.GetExcelPoint(_centroCosto, fila, indexHoja).Trim();
                var rut = excelReader.GetExcelPoint(_rut, fila, indexHoja);
                var direccion = excelReader.GetExcelPoint(_direccionEntrega, fila, indexHoja);
                var observacion = excelReader.GetExcelPoint(_observacion, fila, indexHoja);
                var fecha = excelReader.GetDateFromExcelPoint(_fecha, fila, indexHoja);
                fecha = fecha.ToString().Replace("/", "-");
                var fecha_actual = hoy.ToString("d").Replace("/", "-");*/
                var empresa = 3;//excelReader.GetExcelPoint(_empresa, fila);
                while (fila < excelReader.Hojas[indexHoja].Filas.Count && !excelReader.Hojas[indexHoja].Filas[fila].IsEmpty())
                {
                    if (excelReader.Hojas[indexHoja].Filas[fila].GetColumnasNotEmpty() == 1) break;
                    ocCliente = excelReader.GetExcelPoint(_ocCliente, fila, indexHoja).Trim();
                    cencos = excelReader.GetExcelPoint(_centroCosto, fila, indexHoja).Trim();
                     rut = excelReader.GetExcelPoint(_rut, fila, indexHoja);
                     direccion = excelReader.GetExcelPoint(_direccionEntrega, fila, indexHoja);
                     observacion = excelReader.GetExcelPoint(_observacion, fila, indexHoja);
                     fecha = excelReader.GetDateFromExcelPoint(_fecha, fila, indexHoja);
                    fecha = fecha.ToString().Replace("/", "-");
                     fecha_actual = hoy.ToString("d").Replace("/", "-");
                    var sku = excelReader.GetExcelPoint(_sku, fila, indexHoja);
                    var cantidad = excelReader.GetExcelPoint(_cantidad, fila, indexHoja);
                    var precio = excelReader.GetExcelPoint(_precio, fila, indexHoja);
                    listaItems.Add(new Item
                    {
                        Sku = sku,
                        Cantidad = cantidad,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    });
                    fila++;
                    Console.WriteLine($"oc cliente {ocCliente}  es igual a {excelReader.GetExcelPoint(_ocCliente, fila)}");
                    Console.WriteLine($"rut {rut}  es igual a {excelReader.GetExcelPoint(_rut, fila)}");
                    Console.WriteLine($"cencos {cencos}  es igual a {excelReader.GetExcelPoint(_centroCosto, fila)}");
                    Console.WriteLine(ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                        rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                        cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)));
                    if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                        rut.Equals(excelReader.GetExcelPoint(_rut, fila)) &&
                        cencos.Equals(excelReader.GetExcelPoint(_centroCosto, fila)))
                    {
                        continue;
                    }
                    else
                    {
                        var ordenCompra = new OrdenCompra.OrdenCompra
                        {
                            Rut = rut,
                            CentroCosto = cencos,
                            Items = listaItems,
                            Direccion = direccion,
                            NumeroCompra = ocCliente,
                            Observaciones = $"OC: {ocCliente}, RESPONSABLE: {observacion}, FECHA SOLICITUD: {fecha}, FECHA RECEPCIÓN: {fecha_actual}",
                            CodigoEmpresa = (CodigoEmpresa)(empresa),
                            TipoIntegracion = TipoIntegracion.EXCEL,
                            TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE
                        };
                        OrdenesCompra.Add(ordenCompra);
                        listaItems = new List<Item>();
                    }

                }
                listaItems = new List<Item>();


            }
            return OrdenesCompra;
        }
    }
}