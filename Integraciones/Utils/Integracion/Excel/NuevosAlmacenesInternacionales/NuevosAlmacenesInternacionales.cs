﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.Utils.Integracion.Excel.NuevosAlmacenesInternacionales
{
    public class NuevosAlmacenesInternacionales
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila = 0
        };
        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 1
        };
        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 1
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };
        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };
        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 3
        };
        private readonly ExcelPoint _jefeTienda = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 4
        };

        private readonly ExcelReader _excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }
        public NuevosAlmacenesInternacionales(ExcelReader excelReader)
        {
            _excelReader = excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }
        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var indexHoja = 0;
            foreach (var hoja in _excelReader.Hojas)
            {
                if (hoja.Nombre.ToUpper().Contains("RESUMEN") || hoja.Nombre.ToUpper().Contains("HOJA")) break;
                var fila = 8;
                var ocCliente = _excelReader.GetExcelPoint(_ocCliente);
                var direccion = _excelReader.GetExcelPoint(_direccionEntrega, hoja:indexHoja);
                var rut = "92171000";//_excelReader.GetExcelPoint(_rut, hoja:indexHoja);
                var jefeTienda = _excelReader.GetExcelPoint(_jefeTienda, hoja: indexHoja);
                var split = rut.DeleteContoniousWhiteSpace().Split();
                rut = split[split.Length - 1];
                var nombreHoja = hoja.Nombre;
                var splitNombreHoja = nombreHoja.Split();
                var cencos = splitNombreHoja[splitNombreHoja.Length - 1].Replace("tda", "").Replace("td","");
                var listaItems = new List<Item>();
                while (fila < hoja.Filas.Count)
                {
                    var sku = _excelReader.GetExcelPoint(_sku, fila, indexHoja);
                    var cantidad = _excelReader.GetExcelPoint(_cantidad, fila, indexHoja);
                    var precio = _excelReader.GetExcelPoint(_precio, fila, indexHoja);
                    if (Regex.Match(sku, @"[a-zA-Z]{1,2}\d{5,6}").Success)
                    {
                        if (!cantidad.Equals("") && !cantidad.Equals("0"))
                            listaItems.Add(new Item
                            {
                                Sku = sku,
                                Cantidad = cantidad,
                                Descripcion = "",
                                Precio = precio,
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            });
                    }
                    fila++;
                }
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    NumeroCompra = ocCliente,
                    Observaciones = $"Dirección: {direccion.ToUpper()}, Jefe de Tienda: {jefeTienda.ToUpper()}",
                    CodigoEmpresa = CodigoEmpresa.DIMERC,
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA
                };
                ordenCompra.ConsoleWriteLine();

                OrdenesCompra.Add(ordenCompra); 
                indexHoja++;
            }

            return OrdenesCompra;
        }
    }
}