﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.GasAtacama
{
    class GasAtacama
    {
        //private readonly ExcelPoint _rut = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.A
        //};

        //private readonly ExcelPoint _centroCosto = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.C
        //};

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.A
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };

        private readonly ExcelPoint _cantidadg= new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _contacto = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }

        public GasAtacama(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }
        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            var fila = 5;
            var ocCliente = excelReader.GetExcelPoint(_ocCliente, 1);
            var cencos = "0";
            var cantidad = ""
;            var rut = "78932860";
            var direccion = ".";
            var observacion = ".";
            var empresa = 3;
            var listaItems = new List<Item>();
            while (fila < excelReader.Hojas[0].Filas.Count && !excelReader.Hojas[0].Filas[fila].IsEmpty() && excelReader.GetExcelPoint(_sku, fila) != "")
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);
                if (!excelReader.GetExcelPoint(_cantidad, fila - 1).Trim().Contains("PAQUETES"))
                {

                   cantidad = excelReader.GetExcelPoint(_cantidadg, fila);
                }
  
                if (cantidad.Contains("0"))
                {
                    fila++;
                    continue;
                }
                var precio = "1";
              
                listaItems.Add(new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Descripcion = "",
                    Precio = precio,
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,


                });
                fila++;
                if (excelReader.GetExcelPoint(_sku, fila) != "" )
                    continue;
            }
            var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = ocCliente,
                    Observaciones = $"OC: {ocCliente}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE
                };
                OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
         
            return OrdenesCompra;
        }
    
    }
}