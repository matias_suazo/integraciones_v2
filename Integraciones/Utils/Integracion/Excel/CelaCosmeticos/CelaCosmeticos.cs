﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.CelaCosmeticos
{
    class CelaCosmeticos
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila = 3
        };
        private readonly ExcelPoint sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.D,
            Fila = 0
        };
        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.D,
            Fila = 0
        };
        private readonly ExcelPoint _sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.E,
            Fila = 0
        };
        private readonly ExcelPoint _sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.F,
            Fila = 0
        };
        private readonly ExcelPoint _sku4 = new ExcelPoint
        {
            Columna = ColumnaExcel.G,
            Fila = 0
        };
        private readonly ExcelPoint _sku5 = new ExcelPoint
        {
            Columna = ColumnaExcel.H,
            Fila = 0
        };
        private readonly ExcelPoint _sku6 = new ExcelPoint
        {
            Columna = ColumnaExcel.I,
            Fila = 0
        };
        private readonly ExcelPoint _sku7 = new ExcelPoint
        {
            Columna = ColumnaExcel.J,
            Fila = 0
        };
       

        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.B,
            Fila = 1
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.O
        };
        private readonly ExcelPoint _cantidad_sku1 = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _cantidad_sku2 = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };
        private readonly ExcelPoint _cantidad_sku3 = new ExcelPoint
        {
            Columna = ColumnaExcel.F
        };
        private readonly ExcelPoint _cantidad_sku4 = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };
        private readonly ExcelPoint _cantidad_sku5 = new ExcelPoint
        {
            Columna = ColumnaExcel.H
        };
        private readonly ExcelPoint _cantidad_sku6 = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };
        private readonly ExcelPoint _cantidad_sku7 = new ExcelPoint
        {
            Columna = ColumnaExcel.J
        };
        //private readonly ExcelPoint _cantidad_sku23 = new ExcelPoint
        //{
        //    Columna = ColumnaExcel.AB
        //};

        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.I
        };

        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Columna = ColumnaExcel.B

        };
        private readonly ExcelPoint _nxtobservacion = new ExcelPoint
        {
            Columna = ColumnaExcel.C

        };


        private readonly ExcelPoint _direccionEntrega = new ExcelPoint
        {
            Columna = ColumnaExcel.E
        };

        private readonly ExcelPoint _fono = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };
        private readonly ExcelPoint _local = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        private readonly ExcelPoint _comuna = new ExcelPoint
        {
            Columna = ColumnaExcel.G
        };

        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public CelaCosmeticos(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        private string GetSku(string cadena)
        {
            string ret = cadena;
            Match match = Regex.Match(cadena, @"[A-Z]{1,2}\d{5,6}");
            if (match.Success)
            {
                ret = match.Value.Trim();
            }
            return ret;
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {

            var fila = 0;
            var listaItems = new List<Item>();
            var contadorFila = 1;
            while (fila <= excelReader.Hojas[0].Filas.Count - 1 && !excelReader.Hojas[0].Filas[fila].IsEmpty())
            {
                //var ocCliente = excelReader.ExcelFileName;
                DateTime now = DateTime.Now;

                var ocCliente = $"{now.ToString("MMM")}_{excelReader.ExcelFileName}";
                var cencos = "";
                try
                {

                    cencos = excelReader.GetExcelPoint(_centroCosto, fila);
                    //cencos = cencos.Substring(0, 5);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }



                var sku1 = excelReader.GetExcelPoint(_sku);
                sku1 = GetSku(sku1);
                var sku2 = excelReader.GetExcelPoint(_sku2);
                sku2 = GetSku(sku2);
                var sku3 = excelReader.GetExcelPoint(_sku3);
                sku3 = GetSku(sku3);
                var sku4 = excelReader.GetExcelPoint(_sku4);
                sku4 = GetSku(sku4);
                var sku5 = excelReader.GetExcelPoint(_sku5);
                sku5 = GetSku(sku5);
                var sku6 = excelReader.GetExcelPoint(_sku6);
                sku6 = GetSku(sku6);
                var sku7 = excelReader.GetExcelPoint(_sku7);
                sku7 = GetSku(sku7);



                var cantidad1 = excelReader.GetExcelPoint(_cantidad_sku1, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku1, fila);
                var cantidad2 = excelReader.GetExcelPoint(_cantidad_sku2, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku2, fila);
                var cantidad3 = excelReader.GetExcelPoint(_cantidad_sku3, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku3, fila);
                var cantidad4 = excelReader.GetExcelPoint(_cantidad_sku4, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku4, fila);
                var cantidad5 = excelReader.GetExcelPoint(_cantidad_sku5, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku5, fila);
                var cantidad6 = excelReader.GetExcelPoint(_cantidad_sku6, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku6, fila);
                var cantidad7 = excelReader.GetExcelPoint(_cantidad_sku7, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku7, fila);
               

                

                var precio = "1";//excelReader.GetExcelPoint(_precio, fila);
                var rut = "96920840"; //excelReader.GetExcelPoint(_rut, fila);
                var direccion = "";
                var telefono = "";
                //var observacion = $"CC: {cencos} CONTACTO: { excelReader.GetExcelPoint(_observacion, fila)} Local: {excelReader.GetExcelPoint(_local, fila)}";
                var observacion = "";
                var empresa = 3;//excelReader.GetExcelPoint(_empresa, fila);
                                //var kk = excelReader.GetExcelPoint(_poderOculto, fila);

                if (!cantidad1.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku1,
                        Cantidad = cantidad1,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad2.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku2,
                        Cantidad = cantidad2,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad3.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku3,
                        Cantidad = cantidad3,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad4.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku4,
                        Cantidad = cantidad4,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad5.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku5,
                        Cantidad = cantidad5,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad6.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku6,
                        Cantidad = cantidad6,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad7.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku7,
                        Cantidad = cantidad7,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }

                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    rut.Contains(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Contains(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = $"{ocCliente}",
                    Observaciones = $"{observacion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
                Console.WriteLine("FILA " + contadorFila);
                contadorFila++;
            }
            return OrdenesCompra;
        }
    }
}

