﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Excel.CelaCosmeticos
{
    class CelaCosmeticosV2
    {
        private readonly ExcelPoint _rut = new ExcelPoint
        {
            Columna = ColumnaExcel.A,
            Fila  = 3
        };              
        
        private readonly ExcelPoint _centroCosto = new ExcelPoint
        {
            Columna = ColumnaExcel.D,
            Fila = 1
        };

        private readonly ExcelPoint _ocCliente = new ExcelPoint
        {
            Columna = ColumnaExcel.O
        };

        private readonly ExcelPoint _sku = new ExcelPoint
        {
            Columna = ColumnaExcel.C
        };

        private readonly ExcelPoint _cantidad = new ExcelPoint
        {
            Columna = ColumnaExcel.D
        };
        
        private readonly ExcelPoint _precio = new ExcelPoint
        {
            Columna = ColumnaExcel.AJ
        };

        //sin observacion
        private readonly ExcelPoint _observacion = new ExcelPoint
        {
            Fila = 2
        };
        
        private readonly ExcelReader excelReader;

        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }


        public CelaCosmeticosV2(ExcelReader _excelReader)
        {
            excelReader = _excelReader;
            //OrdenesCompra = new List<OrdenCompra.OrdenCompra>(); 
        }

        public List<Item> GetItems(int fila, int filafin)
        {
            fila = 2;
            List<Item> lista = new List<Item>();
            for(; fila < filafin; fila++)
            {
                var sku = excelReader.GetExcelPoint(_sku, fila);
                var precio = excelReader.GetExcelPoint(_precio, fila);
                //var cantidad = excelReader.GetExcelPoint(_cantidad, fila);

                if (esSku(sku))
                {
                    var xsku = GetSku(sku);
                    var item = new Item
                    {
                        Sku = xsku,
                        
                    //Cantidad = cantidad,
                    Precio = precio,
                    TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                    };
                    lista.Add(item);
                }
            }
            
            return lista;
        }
        public string GetPrecio(string prec)
        {
            Match matchPr = Regex.Match(prec, @"PRECIO");
            return matchPr.Value;
        }

        public bool esPrecio(string prec)
        {
            Match matchPr = Regex.Match(prec, @"PRECIO");
            return (matchPr.Success);
        }

        public string GetSku(string valor)
        {
            Match match = Regex.Match(valor, @"[A-Z]{1,2}\d{5,6}");
            return match.Value;
        }
        public bool esSku(string valor)
        {
            Match match = Regex.Match(valor, @"[A-Z]{1,2}\d{5,6}");
            return (match.Success);
        }


        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            List<OrdenCompra.OrdenCompra> OrdenesCompra = new List<OrdenCompra.OrdenCompra>();

            var fila = 0;
            var columna = 3;

            var contadorFila = 0;

            var cencos = "0";
            var sku = "";
            var ocCliente = excelReader.ExcelFileName;
            var codigos = GetItems(3, excelReader.Hojas[0].Filas.Count);
            var precios = GetItems(2, excelReader.Hojas[0].Filas.Count);
            var cantidad = "0";
            var precio = "0";
            bool fincencos = false;
            
            foreach (var row in excelReader.Hojas[0].Filas)
            {
                var occ = new OrdenCompra.OrdenCompra();
                Console.WriteLine(contadorFila);
                if (contadorFila == 0)
                {
                    contadorFila++;
                    continue;
                }

                var contadorColum = 3;
                //Recorriendo Columnas
                foreach (var column in row.Columnas)
                {
                    if (contadorColum >= 3)
                    {
                        _centroCosto.Fila = 0;
                        _centroCosto.Columna = (ColumnaExcel)contadorColum;
                        cencos = excelReader.GetExcelPoint(_centroCosto);
                        if(!Regex.IsMatch(cencos, @"\d"))
                        {
                            cencos = "";
                            _centroCosto.Fila = 0;
                        }
                        if (cencos.Equals("") && _centroCosto.Fila == 0)
                        {
                            _centroCosto.Fila = 0;
                            cencos = excelReader.GetExcelPoint(_centroCosto);
                        }
                        if (cencos.Equals(""))
                        {
                            fincencos = true;
                            break;
                        }
                        var contadorFilaSku = 2;
                        //contadorColum = 3;
                        List<Item> listaItems = new List<Item>();
                        foreach (var cod in codigos)
                        {
                            //_sku.Fila = 0;
                            cantidad = "0";
                            //contadorColum = 3;
                            Console.WriteLine($"{cod.Sku} es igual a {GetSku(excelReader.GetExcelPoint(_sku, contadorFilaSku))}?{cod.Sku.Equals(GetSku(excelReader.GetExcelPoint(_sku, contadorFilaSku)))}");
                            if (cod.Sku.Equals(GetSku(excelReader.GetExcelPoint(_sku, contadorFilaSku))))
                            {
                                _cantidad.Fila = contadorFilaSku;
                                _cantidad.Columna = (ColumnaExcel)contadorColum;
                                cantidad = excelReader.GetExcelPoint(_cantidad);
                                cod.Cantidad = cantidad;
                            }
                            if (!(cod.Cantidad.Equals("") || cod.Cantidad.Equals("0") || cod.Cantidad.Equals("")))
                            {
                                listaItems.Add(cod);
                            }
                            contadorFilaSku++;
                        }

                        Console.WriteLine("fin comparación de codigos, Generar OC");

                        if (listaItems.Count > 0)
                        {
                            occ = new OrdenCompra.OrdenCompra
                            {
                                Rut = "96920840",
                                Items = (from l in listaItems
                                        select new Item
                                    {
                                        Cantidad = l.Cantidad,
                                        Precio = l.Precio,
                                        Sku = l.Sku,
                                        TipoPareoProducto = l.TipoPareoProducto,
                                        TipoPrecioProducto = l.TipoPrecioProducto
                                    }
                                ).ToList(),
                                CentroCosto = cencos,
                                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                                NumeroCompra = ocCliente,
                                CodigoEmpresa = (CodigoEmpresa)int.Parse("3"),
                                TipoIntegracion = TipoIntegracion.EXCEL,
                                Observaciones = ""


                            };
                            Console.WriteLine($"OC: {occ}");
                            OrdenesCompra.Add(occ);
                            Console.WriteLine($"LISTA OC: {OrdenesCompra.ToString()}");
                        }
                        contadorColum++;

                    }
                    
                }

                contadorFila++;
                if (fincencos)
                {
                    break;
                }
            }
                
            return OrdenesCompra;
        }
    }
}


/*cela1
                var sku1 = excelReader.GetExcelPoint(_sku1);
                sku1 = GetSku(sku1);
                var sku2 = excelReader.GetExcelPoint(_sku2);
                sku2 = GetSku(sku2);
                var sku3 = excelReader.GetExcelPoint(_sku3);
                sku3 = GetSku(sku3);
                var sku4 = excelReader.GetExcelPoint(_sku4);
                sku4 = GetSku(sku4);
                var sku5 = excelReader.GetExcelPoint(_sku5);
                sku5 = GetSku(sku5);
                var sku6 = excelReader.GetExcelPoint(_sku6);
                sku6 = GetSku(sku6);
                var sku7 = excelReader.GetExcelPoint(_sku7);
                sku7 = GetSku(sku7);



                var cantidad1 = excelReader.GetExcelPoint(_cantidad_sku1, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku1, fila);
                var cantidad2 = excelReader.GetExcelPoint(_cantidad_sku2, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku2, fila);
                var cantidad3 = excelReader.GetExcelPoint(_cantidad_sku3, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku3, fila);
                var cantidad4 = excelReader.GetExcelPoint(_cantidad_sku4, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku4, fila);
                var cantidad5 = excelReader.GetExcelPoint(_cantidad_sku5, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku5, fila);
                var cantidad6 = excelReader.GetExcelPoint(_cantidad_sku6, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku6, fila);
                var cantidad7 = excelReader.GetExcelPoint(_cantidad_sku7, fila).Equals("") ? "0" : excelReader.GetExcelPoint(_cantidad_sku7, fila);




                var precio = "1";//excelReader.GetExcelPoint(_precio, fila);
                var rut = "96920840"; //excelReader.GetExcelPoint(_rut, fila);
                var direccion = "";
                var telefono = "";
                //var observacion = $"CC: {cencos} CONTACTO: { excelReader.GetExcelPoint(_observacion, fila)} Local: {excelReader.GetExcelPoint(_local, fila)}";
                var observacion = "";
                var empresa = 3;//excelReader.GetExcelPoint(_empresa, fila);
                                //var kk = excelReader.GetExcelPoint(_poderOculto, fila);

                if (!cantidad1.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku1,
                        Cantidad = cantidad1,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad2.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku2,
                        Cantidad = cantidad2,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad3.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku3,
                        Cantidad = cantidad3,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad4.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku4,
                        Cantidad = cantidad4,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad5.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku5,
                        Cantidad = cantidad5,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad6.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku6,
                        Cantidad = cantidad6,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }
                if (!cantidad7.Equals("0"))
                {
                    listaItems.Add(new Item
                    {
                        Sku = sku7,
                        Cantidad = cantidad7,
                        Descripcion = "",
                        Precio = precio,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                    });

                }

                fila++;
                if (ocCliente.Equals(excelReader.GetExcelPoint(_ocCliente, fila)) &&
                    rut.Contains(excelReader.GetExcelPoint(_rut, fila)) &&
                    cencos.Contains(excelReader.GetExcelPoint(_centroCosto, fila)))
                    continue;
                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                    Rut = rut,
                    CentroCosto = cencos,
                    Items = listaItems,
                    Direccion = direccion,
                    NumeroCompra = $"{ocCliente}",
                    Observaciones = $"{observacion}",
                    CodigoEmpresa = (CodigoEmpresa)(empresa),
                    TipoIntegracion = TipoIntegracion.EXCEL,
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                if (ordenCompra.Items.Count > 0)
                    OrdenesCompra.Add(ordenCompra);
                listaItems = new List<Item>();
                Console.WriteLine("FILA " + contadorFila);
                contadorFila++;
            }*/


//while (fila <= excelReader.Hojas[0].Filas.Count - 1 && !excelReader.Hojas[0].Filas[fila].IsEmpty())
//{
//    //var ocCliente = excelReader.ExcelFileName;
//    DateTime now = DateTime.Now;

//    ocCliente = $"{now.ToString("MMM")}_{excelReader.ExcelFileName}";
//    cencos = "";
//    try
//    {

//        cencos = excelReader.GetExcelPoint(_centroCosto, fila);
//        //cencos = cencos.Substring(0, 5);
//    }
//    catch (Exception e)
//    {
//        Console.WriteLine(e.ToString());
//    }
//}
//recorriendo filas


/*var contadorFilaPrecio = 2;
                    foreach (var pre in precios)
                    {
                        precio = "0";
                        contadorFilaPrecio = 2;
                        Console.WriteLine($"{pre.Precio} es igual a {GetPrecio(excelReader.GetExcelPoint(_precio, contadorFilaPrecio))}?{pre.Precio.Equals(GetPrecio(excelReader.GetExcelPoint(_precio, contadorFilaPrecio)))}");
                        if (pre.Precio.Equals(GetPrecio(excelReader.GetExcelPoint(_precio, contadorFilaPrecio))))
                        {
                            _precio.Fila = contadorFilaPrecio;
                            _precio.Columna = (ColumnaExcel)contadorColum;
                            precio = excelReader.GetExcelPoint(_precio);
                            pre.Precio = precio;
                        }
                        if (!(pre.Precio.Equals("") || pre.Precio.Equals("0") || pre.Precio.Equals("")))
                        {
                            listaItems.Add(pre);
                        }
                        contadorFilaPrecio++;
                    }*/
