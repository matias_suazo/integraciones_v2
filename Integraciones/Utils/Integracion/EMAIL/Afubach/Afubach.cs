﻿using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Email;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Email.Afubach
{
    class Afubach
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[aA-zZ]{1,2}\d{5,6}" },
           // {0,@"\d{6}" }
        };

        private readonly Dictionary<int, string> _cantidadItemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s(CADA|UNIDAD)"},
        };

        private readonly Dictionary<int, string> _precioItemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\(CLP\)\d{1,}\s"},
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Información de tu orden:";
        private const string ItemsHeaderPattern =
            "*Nombre**SKU**Cantidad*";
        private const string ItemsFooterPattern = "Total Neto ";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Nombre: ";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly IMail _email;
        private readonly string[] _emailBody;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Afubach(IMail mail)
        {
            _email = mail;
            _emailBody = _email.GetBodyAsList().ToArray();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "76792110",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.MAIL
            };
            for (var i = 0; i < _emailBody.Length; i++)
            {

                Console.WriteLine(_emailBody[i].Replace("\r", ""));
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_emailBody[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_emailBody[i]).Replace("\r","");
                        _readOrdenCompra = true;
                    }
                }
               /* if (!_readRut)
                {
                    if (IsRutPattern(_emailBody[i]))
                    {
                        OrdenCompra.Rut = GetRut(_emailBody[i]);
                        _readRut = true;
                    }
                }*/

                //if (!_readCentroCosto)
                //{
                //    if (IsCentroCostoPattern(_emailBody[i]))
                //    {
                //        OrdenCompra.CentroCosto = GetCentroCosto(_emailBody[i]);
                //        _readCentroCosto = true;
                //    }
                //}
                if (!_readObs)
               {
                    if (IsObservacionPattern(_emailBody[i]))
                    {

                        OrdenCompra.Observaciones = GetObservacion(_emailBody[i]);
                        _readObs = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_emailBody[i]))
                    {
                        var items = GetItems(_emailBody, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                //OrdenCompra.NumeroCompra = _pdfReader.TxtFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            
            for (; i < pdfLines.Length; i++)
            {
                var aux1 = pdfLines[i].Trim().DeleteContoniousWhiteSpace().Replace("\t","");
                if(aux1.Contains("Total a Cancelar:"))
                {
                    break;
                }
                var optItem = GetFormatItemsPattern(aux1);

                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test0 = aux1.Split(' ');
                        var sku = GetSku(pdfLines, i);
                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = test0[2],
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        
                        items.Add(item0);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] pdfLInes, int i)
        {
            var ret = "";
            var contador = i;
            for (; contador < pdfLInes.Length; contador++)
            {
                Match match = Regex.Match(pdfLInes[contador], _itemsPatterns[0]);
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }

            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var linea = str.Split(':');
            return linea[linea.Length - 1];
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1];
        }

        private int GetFormatItemsPattern(string str)
        {
            var test2 = str.Split(' ');
            if (test2.Length == 2 && str.ToUpper().Contains("CADA")) return -1;
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, (string)it.Value).Success))
            {
                Console.WriteLine($"STR: {str}, RET: {it.Key}");
                return ret = it.Key;
            }
            Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private int GetCantidad(string str)
        {
            var cantidad = -1;
            var optCantidad = GetFormatCantidadItemPattern(str);
            var split = str.Split(' ');
            switch (optCantidad)
            {
                case 0:
                    cantidad = int.Parse(split[0].Split(',')[0]);
                    break;
            }
            return cantidad;
        }

        private string GetObservacion(string str)
        {
            var ret = "";
            var linea = str.Split(':');
            ret = linea[linea.Length - 1].Trim();
            return ret;
        }


        private int GetPrecio(string str)
        {
            var precio = -1;
            var optCantidad = GetFormatPrecioItemPattern(str);
            var split = str.Split(' ');
            switch (optCantidad)
            {
                case 0:
                    precio = int.Parse(split[0].Split(',')[0].Replace("(CLP)", ""));
                    break;
            }
            return precio;
        }
        private int GetFormatCantidadItemPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "").ToUpper();
            foreach (var it in _cantidadItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            Console.WriteLine($"CANTIDAD_PATTERN: {str}, RET: {ret}");
            return ret;
        }

        private int GetFormatPrecioItemPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "").ToUpper();
            foreach (var it in _precioItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"PRECIO_PATTERN: {str}, RET: {ret}");
            return ret;
        }




        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    
}
}
