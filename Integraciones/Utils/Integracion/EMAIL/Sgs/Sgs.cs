﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Email;
using Limilabs.Mail;

namespace Integraciones.Utils.Integracion.Email.Sgs
{
    public class Sgs
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^[a-zA-Z]{1}\d{5}\s"},//@"Nº:\s[a-zA-Z]{1,2}\d{4,6}"}, //^[a-zA-Z]{1}\d{5}\s
            {1,@"^\d{1,}\s[a-zA-Z]{1,2}\d{5,6}\/"  }, //
            {3, @"^\d{1,}\sSu\sNº:\s[a-zA-Z]{1,2}\d{5,6}$"} ,
            {4, @"^\d{1,}\s(\w{1,}\W{0,}\w{0,}\s?)*$" },
        };

        private readonly Dictionary<int, string> _cantidadItemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s(CADA|UNIDAD)"},
        };

        private readonly Dictionary<int, string> _precioItemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\(CLP\)\d{1,}\s"},
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Número de Pedido";
        private const string ItemsHeaderPattern =
            "Número de Pieza/Descripción";
        private const string ItemsHeaderPattern2 =
            "Fecha de Entrega";
        private const string ItemsFooterPattern = "Total Neto ";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "*Envío*";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly IMail _email;
        private readonly string[] _emailBody;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Sgs(IMail mail)
        {
            _email = mail;
            _emailBody = _email.GetBodyAsList().ToArray();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.MAIL
            };
            for (var i = 0; i < _emailBody.Length; i++)
            {
                
                Console.WriteLine(_emailBody[i].Replace("\r",""));
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_emailBody[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_emailBody[++i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_emailBody[i]))
                    {
                        OrdenCompra.Rut = GetRut(_emailBody[i]);
                        _readRut = true;
                    }
                }

                //if (!_readCentroCosto)
                //{
                //    if (IsCentroCostoPattern(_emailBody[i]))
                //    {
                //        OrdenCompra.CentroCosto = GetCentroCosto(_emailBody[i]);
                //        _readCentroCosto = true;
                //    }
                //}
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_emailBody[i]))
                //    {
                //        for (var j = i + 1; j < i + 10 && !_emailBody[j].Contains("*Pedido*"); j++)
                //        {
                //            OrdenCompra.Observaciones += $"{_emailBody[j].Trim().DeleteContoniousWhiteSpace()}, ";
                //        }
                //        OrdenCompra.Observaciones = OrdenCompra.Observaciones.Substring(0,
                //            OrdenCompra.Observaciones.Length - 4);
                //       _readObs = true;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_emailBody[i]))
                    {
                        var items = GetItems(_emailBody, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                //OrdenCompra.NumeroCompra = _pdfReader.TxtFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            var firtItem = 1;
            for (; i < pdfLines.Length - 4; i++)
            {
                var aux1 = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var aux2 = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                var pattern = $"{aux1} {aux2}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                var x = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                var y = pdfLines[i + 3].Trim().DeleteContoniousWhiteSpace();
                var pattern2 = $"{x} {y}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                var opt = -1;
                //Console.WriteLine($"PATRON: {pattern}");
                var optItem = GetFormatItemsPattern(pattern);
                var optItem2 = GetFormatItemsPattern(pattern2);
                opt = optItem != -1 ? optItem2 != -1 ? optItem2 : optItem: optItem;

                switch (opt)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        i = optItem2 != -1 ? i + 2 : i;
                        var test0 = optItem2 != -1 ? pattern2.Split(' ') : pattern.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[test0.Length-1],
                            Cantidad = "0",
                            Precio = "0",
                            Descripcion = test0.ArrayToString(1),
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        var sku = GetSku(optItem2 != -1 ? pattern.Split(' '):pdfLines[i-1].Trim().DeleteContoniousWhiteSpace().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku))
                        {
                            item0.Sku = sku;
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        var aux11 = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                        var aux21 = pdfLines[i + 3].Trim().DeleteContoniousWhiteSpace();
                        var pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                        var readCantidad = false;
                        var readPrecio = true;
                        for (var j =  i + 6; j < _emailBody.Length-2 && GetFormatItemsPattern(pattern1) == -1; j++)
                        {
                           
                            if (!readCantidad)
                            {
                                var cantidad = GetCantidad(pattern1);
                                if (cantidad != -1)
                                {
                                    item0.Cantidad = cantidad.ToString();
                                    readCantidad = true;
                                    readPrecio = false;
                                }
                            }
                            if (!readPrecio)
                            {
                                var precio = GetPrecio(pattern1);
                                if (precio != -1)
                                {
                                    item0.Precio = precio.ToString();
                                    break;
                                }
                            }
                            aux11 = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                            aux21 = pdfLines[j + 1].Trim().DeleteContoniousWhiteSpace();
                            pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();

                        }
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine("==================ITEM CASE 1=====================");
                        var test1 = pattern.Split(' ');
                        var firstDescription = test1[1].Split('/')[1];
                        var item1 = new Item
                        {
                            Sku = GetSku(test1),
                            Cantidad = "0",
                            Precio = "0",
                            Descripcion = $"{firstDescription} {test1.ArrayToString(2)}",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        //sku = GetSku(pdfLines[i - 1].Trim().DeleteContoniousWhiteSpace().Split(' '));
                        //if (OracleDataAccess.ExistProduct(sku))
                        //{
                        //    item1.Sku = sku;
                        //    item1.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        //}
                        aux11 = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                        aux21 = pdfLines[i + 3].Trim().DeleteContoniousWhiteSpace();
                        pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                        //Console.WriteLine($"Pattern2: {pattern1}");
                        readCantidad = false;
                        readPrecio = true;
                        for (var j = i + 6; j < _emailBody.Length - 2 && GetFormatItemsPattern(pattern1) == -1; j++)
                        {
                            //Console.WriteLine($"Pattern2: {pattern1}");
                            if (!readCantidad)
                            {
                                var cantidad = GetCantidad(pattern1);
                                if (cantidad != -1)
                                {
                                    item1.Cantidad = cantidad.ToString();
                                    readCantidad = true;
                                    readPrecio = false;
                                }
                            }
                            if (!readPrecio)
                            {
                                var precio = GetPrecio(pattern1);
                                if (precio != -1)
                                {
                                    item1.Precio = precio.ToString();
                                    break;
                                }
                            }
                            aux11 = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                            aux21 = pdfLines[j + 1].Trim().DeleteContoniousWhiteSpace();
                            pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();

                        }
                        items.Add(item1);
                        break;
                    case 4:
                        var test2 = pattern.Split(' ');
                        var position = 0;
                        int.TryParse(test2[0], out position);
                        if (firtItem != position) break;
                        if (test2.Length == 2 && pattern.ToUpper().Contains("CADA")) break;
                        Console.WriteLine("==================ITEM CASE 2=====================");
                        firtItem++;
                        var item2 = new Item
                        {
                            Sku = "SIN_SKU",
                            Cantidad = "0",
                            Precio = "0",
                            Descripcion = $"{test2.ArrayToString(1)}",
                            TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE
                        };
                        aux11 = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                        aux21 = pdfLines[i + 3].Trim().DeleteContoniousWhiteSpace();
                        pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                        //Console.WriteLine($"Pattern2: {pattern1}");
                        readCantidad = false;
                        readPrecio = true;
                        for (var j = i + 6; j < _emailBody.Length - 2; j++)
                        {
                            if (GetFormatItemsPattern(pattern1) != -1)
                                if(GetFormatCantidadItemPattern(pattern1)== -1)
                                    break;
                            //Console.WriteLine($"Pattern2: {pattern1}");
                            if (!readCantidad)
                            {
                                var cantidad = GetCantidad(pattern1);
                                if (cantidad != -1)
                                {
                                    item2.Cantidad = cantidad.ToString();
                                    readCantidad = true;
                                    readPrecio = false;
                                }
                            }
                            if (!readPrecio)
                            {
                                var precio = GetPrecio(pattern1);
                                if (precio != -1)
                                {
                                    item2.Precio = precio.ToString();
                                    break;
                                }
                            }
                            aux11 = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                            aux21 = pdfLines[j + 1].Trim().DeleteContoniousWhiteSpace();
                            pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();

                        }
                        items.Add(item2);
                        break;
                    case 3:
                        Console.WriteLine("==================ITEM CASE 3=====================");
                        test1 = pattern.Split(' ');
                        var item3 = new Item
                        {
                            Sku = GetSku(test1),
                            Cantidad = "0",
                            Precio = "0",
                            Descripcion = $"",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        //sku = GetSku(pdfLines[i - 1].Trim().DeleteContoniousWhiteSpace().Split(' '));
                        //if (OracleDataAccess.ExistProduct(sku))
                        //{
                        //    item1.Sku = sku;
                        //    item1.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        //}
                        aux11 = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                        aux21 = pdfLines[i + 3].Trim().DeleteContoniousWhiteSpace();
                        pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                        var split = pattern1.Split(' ');
                        item3.Descripcion = $"{split.ArrayToString(0,split.Length-2)}";
                        //Console.WriteLine($"Pattern2: {pattern1}");
                        readCantidad = false;
                        readPrecio = true;
                        for (var j = i + 6; j < _emailBody.Length - 2; j++)
                        {
                            if (GetFormatItemsPattern(pattern1) != -1)
                                if (GetFormatCantidadItemPattern(pattern1) == -1)
                                    break;
                            //Console.WriteLine($"Pattern2: {pattern1}");
                            if (!readCantidad)
                            {
                                var cantidad = GetCantidad(pattern1);
                                if (cantidad != -1)
                                {
                                    item3.Cantidad = cantidad.ToString();
                                    readCantidad = true;
                                    readPrecio = false;
                                }
                            }
                            if (!readPrecio)
                            {
                                var precio = GetPrecio(pattern1);
                                if (precio != -1)
                                {
                                    item3.Precio = precio.ToString();
                                    break;
                                }
                            }
                            aux11 = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                            aux21 = pdfLines[j + 1].Trim().DeleteContoniousWhiteSpace();
                            pattern1 = $"{aux11} {aux21}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();

                        }
                        items.Add(item3);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            return str.Replace("\r", "").Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1];
        }

        private int GetFormatItemsPattern(string str)
        {
            var test2 = str.Split(' ');
            if (test2.Length == 2 && str.ToUpper().Contains("CADA")) return -1;
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, (string)it.Value).Success))
            {
                Console.WriteLine($"STR: {str}, RET: {it.Key}");
                return ret = it.Key;
            }
            Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private int GetCantidad(string str)
        {
            var cantidad = -1;
            var optCantidad = GetFormatCantidadItemPattern(str);
            var split = str.Split(' ');
            switch (optCantidad)
            {
                case 0:
                    cantidad = int.Parse(split[0].Split(',')[0]);
                    break;
            }
            return cantidad;
        }


        private int GetPrecio(string str)
        {
            var precio = -1;
            var optCantidad = GetFormatPrecioItemPattern(str);
            var split = str.Split(' ');
            switch (optCantidad)
            {
                case 0:
                    precio = int.Parse(split[0].Split(',')[0].Replace("(CLP)",""));
                    break;
            }
            return precio;
        }
        private int GetFormatCantidadItemPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "").ToUpper();
            foreach (var it in _cantidadItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            Console.WriteLine($"CANTIDAD_PATTERN: {str}, RET: {ret}");
            return ret;
        }

        private int GetFormatPrecioItemPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "").ToUpper();
            foreach (var it in _precioItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"PRECIO_PATTERN: {str}, RET: {ret}");
            return ret;
        }




        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}