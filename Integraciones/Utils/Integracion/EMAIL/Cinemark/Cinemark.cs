﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Email;
using Limilabs.Mail;
using Integraciones.Utils.Oracle.DataAccess;

namespace Integraciones.Utils.Integracion.Email.Cinemark
{
    class Cinemark
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\s\d{1,}\s\d{1,}\s\d{1,}$"}//,  {1, @"\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}$"},
            //{0, @"[a-zA-Z]{1,2}\d{4,6}"}
            // FORMATO ANTERIOR **  {0, @"\s\d{1,}\s\d{1,}\s\d{1,}$"},  {1, @"\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}$"}, 
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Nº";
        private const string ItemsHeaderPattern =
           "Artículo";// "Cantidad";

        private bool secondFormat = false;

        private const string ItemsFooterPattern = "_____";

        private const string CentroCostoPattern = "CINE:";
        private const string CentroCostoPattern2 = "CINE ";
        private const string ObservacionesPattern = "Tienda :";
        private string orden = "";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItems;
        private readonly IMail _email;
        private readonly string[] _emailBodyLines;
        private object pdfLines;

        private SeparableOrdenCompra OrdenCompra { get; set; }

        #endregion

        public Cinemark(IMail mail)
        {
            _email = mail;
            _emailBodyLines = _email.GetBodyAsList().ToArray();
        }

        #region Funciones Get
        public List<OrdenCompra.OrdenCompra> GetListOrdenCompra()
        {
            OrdenCompra = new SeparableOrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH,
                TipoIntegracion = TipoIntegracion.MAIL
            };

            var listOrdenesCompra = new List<SeparableOrdenCompra>();
            var lastOc = "";
            var rut = "96659800";
            var obs = "";
            var ordenC = "";
            var cc = "";
            var items = new List<SeparableItem>();
            for (var i = 0; i < _emailBodyLines.Length; i++)
            {
                Console.WriteLine($"STR: {_emailBodyLines[i]}");
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_emailBodyLines[i]))
                    {
                        if (!lastOc.Equals(_emailBodyLines[i]))
                        {
                            lastOc = _emailBodyLines[i];
                            ordenC = GetOrdenCompra(_emailBodyLines[i]);
                        }
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_emailBodyLines[i]))
                    {
                        cc = GetCentroCosto(_emailBodyLines[i]);
                    }
                }
                if (!_readItems)
                {
                 
                    if (IsItemHeaderPattern(_emailBodyLines[i]))
                    {
                  
                        var itemsAux = GetItems(_emailBodyLines, i+2, lastOc, cc);
                        if (itemsAux.Count > 0)
                        {
                          
                            items.AddRange(itemsAux);
                            Console.WriteLine($"OC: {ordenC}, Items.Count: {items.Count}, ");
                            for (var j = 0; j < items.Count; j++)
                            {
                                if (!items[j].Descripcion.Contains("SERVILLETA DISP. CINEMARK ELITE")) continue;
                                listOrdenesCompra.Add(new SeparableOrdenCompra
                                {
                                    Rut = rut,
                                    NumeroCompra = ordenC,
                                    CentroCosto = cc,
                                    Observaciones = obs,
                                    SeparableItems = new List<SeparableItem> {items[j]},
                                    TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA,
                                    TipoIntegracion = TipoIntegracion.MAIL
                                });
                                items.RemoveAt(j);
                            }
                            listOrdenesCompra.Add(new SeparableOrdenCompra
                            {
                                Rut = rut,
                                NumeroCompra = ordenC,
                                CentroCosto = cc,
                                Observaciones = obs,
                                SeparableItems = items,
                                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA,
                                TipoIntegracion = TipoIntegracion.MAIL
                            });
                            items = new List<SeparableItem>();
                            //_readRut = false;
                        }
                        //else _readRut = false;
                        //_readItems = true;
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                //OrdenCompra.NumeroCompra = _pdfReader.TxtFileNameOC;
            }
            var finalOrdenCompra = new List<OrdenCompra.OrdenCompra>();
            foreach (var ocSeparable in listOrdenesCompra)
            {
                var oc_aux = ocSeparable.GetAllOrdenCompra();
                finalOrdenCompra.AddRange(oc_aux);
            }
            return finalOrdenCompra;
            
        }
        //Console.WriteLine($"Rut: {rut} ordenC: {ordenC} CC: {cc} ");

        private List<SeparableItem> GetItems(string[] pdfLines, int i,string ocCliente,string cencos="0")
        {
            var items = new List<SeparableItem>();
            for (; i < pdfLines.Length-5; i++)
            {
                var aux1 = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Console.WriteLine($"AUX: {aux1}");
                var aux2 = pdfLines[i+1].Trim().DeleteContoniousWhiteSpace();
                var aux3 = pdfLines[i+2].Trim().DeleteContoniousWhiteSpace();
                var aux4 = pdfLines[i+3].Trim().DeleteContoniousWhiteSpace();
                var aux5 = pdfLines[i + 4].Trim().DeleteContoniousWhiteSpace();
                var pattern = $"{aux1} {aux2} {aux3} {aux4} {aux5}".Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
                if (IsOrdenCompraPattern(pattern)) break;
                if (IsItemsFooterPattern(aux1)) break;
                var optItem = GetFormatItemsPattern(pattern.Replace(".", ""));
                //Es una linea de Items 
                // FORMATO ANTERIROR ** var optItem = GetFormatItemsPattern(pattern.Replace(".",""));
                //secondFormat = orden.Equals("CANTIDAD_PRECIO_");
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==============CASE 0 =============");
                        var test0 = pattern.Split(' ');
                        var skutmp = GetSku(test0);
                        var item0 = new SeparableItem
                        {
                            Sku = skutmp,
                            Cantidad = test0[test0.Length -3].Trim(),
                            Precio = test0[test0.Length - 2].Trim(),
                            SeparableFilter = OracleDataAccess.isTissueProduct(ocCliente, "96659800", skutmp).ToString(),
                            CentroCosto = cencos,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                            //Sku = "SIN_SKU",
                            //Cantidad = secondFormat ? IsDigit(test0[test0.Length - 4]) ? test0[test0.Length - 2].Replace(".", "") : test0[test0.Length - 3].Replace(".", ""): test0[test0.Length - 3].Replace(".", ""),
                            //Precio = secondFormat?
                            //    IsDigit(test0[test0.Length - 4])?
                            //    test0[test0.Length - 4].Replace(".", "")
                            //    : test0[test0.Length - 2].Replace(".", "")
                            //    : test0[test0.Length - 2].Replace(".", ""),
                            //Descripcion = test0.ArrayToString(0, test0.Length-4).Replace("Servilletas Dispensador Cinemark (2 colores)", "SERVILLETA DISP. CINEMARK ELITE"),
                            //TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE//PareoSkuClienteDescripcionTelemarketing
                        };
                        //if(!item0.Descripcion.Contains("Alberto Pepper"))
                        if (item0.Sku.Equals("Z446482")) { break; }
                            items.Add(item0);
                        //i += i < pdfLines.Length - 4 ? 3 : 0;
                        break;
                    case 1:
                        Console.WriteLine("==============CASE 1 =============");
                        var test1 = pattern.Split(' ');
                        var skutmp1 = GetSku(test1);
                        var item1 = new SeparableItem
                        {
                            Sku = skutmp1,
                            Cantidad = test1[test1.Length - 3].Trim(),
                            Precio = test1[test1.Length - 2].Trim(),
                            SeparableFilter = OracleDataAccess.isTissueProduct(ocCliente, "96659800", skutmp1).ToString(),
                            CentroCosto = cencos,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                            //Sku = "SIN_SKU",
                            //Cantidad = secondFormat ? IsDigit(test0[test0.Length - 4]) ? test0[test0.Length - 2].Replace(".", "") : test0[test0.Length - 3].Replace(".", ""): test0[test0.Length - 3].Replace(".", ""),
                            //Precio = secondFormat?
                            //    IsDigit(test0[test0.Length - 4])?
                            //    test0[test0.Length - 4].Replace(".", "")
                            //    : test0[test0.Length - 2].Replace(".", "")
                            //    : test0[test0.Length - 2].Replace(".", ""),
                            //Descripcion = test0.ArrayToString(0, test0.Length-4).Replace("Servilletas Dispensador Cinemark (2 colores)", "SERVILLETA DISP. CINEMARK ELITE"),
                            //TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE//PareoSkuClienteDescripcionTelemarketing
                        };
                        //if(!item0.Descripcion.Contains("Alberto Pepper"))
                        if (item1.Sku.Equals("Z446482")) { break; }
                        items.Add(item1);
                        //i += i < pdfLines.Length - 4 ? 3 : 0;
                        break;
                        //case 1:
                        //    Console.WriteLine("==============CASE 1 =============");
                        //    var test1 = pattern.Split(' ');
                        //    var item1 = new Item
                        //    {

                        //        Sku = GetSku(test1),
                        //        Cantidad = test1[test1.Length - 3].Trim(),
                        //        Precio = test1[test1.Length - 2].Trim(),

                        //        //Sku = "SIN_SKU",
                        //        //Cantidad = test1[test1.Length - 2].Replace(".", ""),
                        //        //Precio = test1[test1.Length - 3].Replace(".", ""),
                        //        //Descripcion = test1.ArrayToString(0, test1.Length - 4).Replace("Servilletas Dispensador Cinemark (2 colores)", "SERVILLETA DISP. CINEMARK ELITE"),
                        //        //TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE//PareoSkuClienteDescripcionTelemarketing
                        //    };
                        //    //if (!item1.Descripcion.Contains("Alberto Pepper"))
                        //        items.Add(item1);
                        //    //i += i < pdfLines.Length - 4 ? 3 : 0;
                        //    break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private bool IsDigit(string str)
        {
            //var isd = Regex.Match(str.DeleteContoniousWhiteSpace(), @"^\d{1,}\.$");
            var ret = Regex.Match(str.DeleteContoniousWhiteSpace(), @"(^\d{1,}\.\d{3}$|^\d{1,}\.$)").Success;
            return ret;
        }

        

        private bool IsItemsFooterPattern(string str)
        {
            return str.DeleteContoniousWhiteSpace().Trim().Contains(ItemsFooterPattern);
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            
                var str1 = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str1, @"^[a-zA-Z]{2,}\s\d{6}").Success)
                {
                    var index = Regex.Match(str1, @"\d{6}").Index;
                    var length = Regex.Match(str1, @"\d{6}").Length;
                    ret = str1.Substring(index, length).Trim();
                }
                var str2 = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str2, @"^\d{6}\s\w{1,}").Success)
                {
                    var index = Regex.Match(str2, @"\d{6}").Index;
                    var length = Regex.Match(str2, @"\d{6}").Length;
                    ret = str1.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var ret = "-1";
            Console.WriteLine($"CCC: {str}");
            if (str.Contains("CINE"))
            {
                var split0 = str.Split(' ');
                var contador = 0;
                for (; contador < split0.Length; contador++)

                    if (split0[contador].Contains("CINE")) {
                        ret = split0[contador + 1].Replace(",", "").Trim();
                        break;
                    }

            }
            return ret;

        }/*
            if (str.Contains("CINE"))
            {
               var  split = str.Split(':');
                var ret = "";
                if (split.Length > 1)
                {
                    ret = split[1].Split(';')[0].DeleteContoniousWhiteSpace();
                    return ret;
                }
                else
                {
                    var split2 = str.Split(',')[0].Split(' ');
                    ret = split2[split2.Length - 1].Replace("\r","");
                    return ret;
                }
            }
            
            return ret;
        }*/


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            var ret = "";
            ret = split.Length > 1 ? split[1].DeleteContoniousWhiteSpace() : str.Split('º')[1].Split(' ')[0];
            Console.Write(ret);
            return ret;
        }

        private static string GetOrdenCompra2(string str)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[1];
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            str = str.Replace(".", "").Replace("$", "").DeleteContoniousWhiteSpace();
            //if (str.Contains("image: Ofimarket SA"))
            //    return ret;
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //if (ret != -1)
                Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is

        private bool IsItemHeaderPattern(string str)
        {
            //Console.WriteLine(str);
            return str.Trim().Contains(ItemsHeaderPattern);
        }
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            //if (str.DeleteContoniousWhiteSpace().Contains("Orden de pedido N")) 
            //{
            //    secondFormat = true;
            //    return true;
            //}
            return str.DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            if (str.DeleteContoniousWhiteSpace().Contains("al CINE"))
            {
                secondFormat = true;
                return true;
            }else if (Regex.Match(str, @"CINE\s").Success)
            {
                return true;
            }
            return str.DeleteContoniousWhiteSpace().Contains(CentroCostoPattern) || str.DeleteContoniousWhiteSpace().Contains(CentroCostoPattern2);
        }

        #endregion

    }
}