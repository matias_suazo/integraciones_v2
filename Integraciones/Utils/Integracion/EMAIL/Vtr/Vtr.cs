﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Email;
using Limilabs.Mail;

namespace Integraciones.Utils.Integracion.Email.Vtr
{
    class Vtr
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\|\|\d{1,}\|\|\d{1,}\|\|"},
            {1, @"[|]\d{6}[|]"}
        };
        private const string RutPattern = "||DIREC.";
        private const string OrdenCompraPattern = "||DIREC.";
        private const string ItemsHeaderPattern =
           "ARTICULO||CODIGODIMERC|| DESCRIP.||PREC.UNIT.||CANT.||SOLICITANTE";// "Cantidad";

        private const string CentroCostoPattern = "||DIREC.";
        private const string ObservacionesPattern = "";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly IMail _email;
        private readonly string[] _emailBodyLines;
        private object pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Vtr(IMail mail)
        {
            _email = mail;
            _emailBodyLines = _email.GetBodyAsList().ToArray();
        }
        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.MAIL
            };
            for (var i = 0; i < _emailBodyLines.Length; i++)
            {
                Console.WriteLine("str: " + _emailBodyLines[i]);
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_emailBodyLines[i + 1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_emailBodyLines[i+1]);
                        //OrdenCompra.Rut = "76350666";
                        _readRut = true;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_emailBodyLines[i + 1]);
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_emailBodyLines[i]))
                    {
                        var items = GetItems(_emailBodyLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                //OrdenCompra.NumeroCompra = _pdfReader.TxtFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length - 6; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var aux2 = pdfLines[i-1].Trim().DeleteContoniousWhiteSpace();
                var concat = $"{aux2} {aux}";
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {

                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test1 = aux.Split('|');
                        var test2 = aux2.Split('|');
                        var item1 = new Item
                        {
                            Sku = GetSku(test1, test2),
                            Cantidad = test1[test1.Length -3],
                            Precio = test1[test1.Length - 5],
                            //TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //    var concatAll = "";
                        //    aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //    for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //    {
                        //        concatAll += $" {aux}";
                        //        aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //    }
                        //    item1.Sku = GetSku(aux.DeleteContoniousWhiteSpace().Split(' '));
                        
                        
                        items.Add(item1);
                        break;

                    case 1:
                        Console.WriteLine("==================ITEM CASE 1=====================");
                        var test3 = aux.Split('|');
                        var test4 = aux.Split('|');
                        var item3 = new Item
                        {
                            Sku = test3[2],
                            Cantidad = test3[test3.Length - 3],
                            Precio = test3[test3.Length - 5],
                            //TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //    var concatAll = "";
                        //    aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //    for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //    {
                        //        concatAll += $" {aux}";
                        //        aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //    }
                        //    item1.Sku = GetSku(aux.DeleteContoniousWhiteSpace().Split(' '));
                        if (Regex.Match(item3.Sku, @"[|]\d{6}[|]").Success)

                        {
                            var index = Regex.Match(pdfLines[i], @"[|]\d{6}[|]").Index;
                            var length = Regex.Match(pdfLines[i], @"[|]\d{6}[|]").Length;
                            item3.Sku = pdfLines[i].Substring(index, length).Trim();
                        }
                        if (!Regex.Match(item3.Sku, @"[|]\d{6}[|]").Success)

                        {
                            var index = Regex.Match(pdfLines[i], @"[|]\d{6}[|]").Index;
                            var length = Regex.Match(pdfLines[i], @"[|]\d{6}[|]").Length;
                            item3.Sku = pdfLines[i].Substring(index, length).Trim().Replace("|", "");
                        }
                        items.Add(item3);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1, string[] test2)
        {
            var ret = "Z446482";
            //if (test1.Length < 3) return ret;
            var skuDefaultPosition = test1[0].Replace("||", " ");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }else
                if (Regex.Match(skuDefaultPosition, @"[|]\d{6}[|]").Success)
                {
                    var index = Regex.Match(skuDefaultPosition, @"[|]\d{6}[|]").Index;
                    var length = Regex.Match(skuDefaultPosition, @"[|]\d{6}[|]").Length;
                    ret = skuDefaultPosition.Substring(index, length).Trim();
                }
            }
            if (ret.Equals("Z446482")){
                skuDefaultPosition = test2[0].Replace("||", " ");
                if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
                {
                    var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                    var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                    ret = skuDefaultPosition.Substring(index, length).Trim();
                }
                else
                {
                    var str = test2.ArrayToString(0, test2.Length - 1);
                    if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                    {
                        var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                        var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                        ret = str.Substring(index, length).Trim();
                    }
                    else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                    {
                        var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                        var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                        ret = str.Substring(index, length).Trim();
                    }else 
                    if (Regex.Match(skuDefaultPosition, @"[|]\d{6}[|]").Success)
                    {
                        var index = Regex.Match(skuDefaultPosition, @"[|]\d{6}[|]").Index;
                        var length = Regex.Match(skuDefaultPosition, @"[|]\d{6}[|]").Length;
                        ret = skuDefaultPosition.Substring(index, length).Trim();
                    }
                }

            }

            return ret;
        }
        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split('|');
            var raw_cencos = aux[2].Trim();
            var split = raw_cencos.Split(' ');
            var ret = raw_cencos;
            if(split.Length == 2)
            {
                ret = $"{split[0]}-{split[1]}";
            }
            //return raw_cencos.Replace(" ","-");
            return ret;
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split('|');
            Console.WriteLine($"STR:{str}");
            return split[split.Length -5].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            string ret = "";
            if (str.Contains("76114143")) {
                ret = "76114143";
            }
            if (str.Contains("76350666"))
            {
                ret= "76350666";
            }
            return ret;
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
           // str = str.Replace("|","");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"str: {str} ret: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test1)
        {

            var ret = "-100";
            for (var i = 0; i < test1.Length - 3; i++)
            {
                Console.WriteLine("test1: " + test1[0]);
                return ret = test1[0].Trim();
            }
            return ret;
        }


        /// <summary>
        /// Obtiene nuevo formato de sku (solo números)
        ///         sku:555555 \d{6}
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>555555</returns>
        
        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}
