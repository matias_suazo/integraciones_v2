﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Email;
using Limilabs.Mail;

namespace Integraciones.Utils.Integracion.Email.ClinicaAlemanaArtikos
{
    class ClinicaAlemanaArtikos
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            //{0, @"[a-zA-Z]{1,2}\d{5,6}"},
            //{1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s"}
            {1, @"^\d{2}/\d{2}/\d{4}$" },
            //{2, @"^\d{1,}\s[a-zA-Z]{1,2}\d{4,6}\s" },
            {2, @"\s\d{1,}\s\d{1,}\s\d{1,}\s\d{8}$" },
            {3,@"\d{9}" }
            //2 Z119724 AZUCAR GRA 1KG DAMA BLANCA Kilogramo 20 569 11380 07/11/2016
        };

        /*
         * 
         * 
            {0, @"^\d{1,}\s\d{6,10}\s"},
            //1 300250973 H401391
            //{1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s" }
            {1, @"^\d{1,}\s[a-zA-Z]{1,2}\d{5,6}\s" },//1 W102310 

    */
        private const string RutPattern = "Giro";
        private const string OrdenCompraPattern = "PEDIDO N°";
        private const string ItemsHeaderPattern =
            "Fecha de Entrega";

        private const string CentroCostoPattern = "*Centro Entrega*";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly IMail _email;
        private readonly string[] _emailBodyLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public ClinicaAlemanaArtikos(IMail mail)
        {
            _email = mail;
            _emailBodyLines = _email.GetBodyAsList().ToArray();
        }
        
        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.MAIL
            };
            
            
            for (var i = 0; i < _emailBodyLines.Length; i++)
            {

                //Console.WriteLine(_emailBodyLines[i]);
                _emailBodyLines[i].Replace(">", "");
                //Console.WriteLine("str: " + _emailBodyLines[i]);
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_emailBodyLines[i + 1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.Rut = "96770100";//GetRut(_emailBodyLines[i - 1]);
                        _readRut = true;
                    }
                }

                //if (!_readCentroCosto)
                //{
                //    if (IsCentroCostoPattern(_emailBodyLines[i]))
                //    {
                //        OrdenCompra.CentroCosto = GetCentroCosto(_emailBodyLines[i + 1]);
                //        _readCentroCosto = true;
                //    }
                //}
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_emailBodyLines[i]))
                    {
                        var items = GetItems(_emailBodyLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                //OrdenCompra.NumeroCompra = _pdfReader.TxtFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length - 7; i++)
            //foreach(var str in pdfLines)
            {
                var pattern = "";

                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                if (aux.Contains("CONDICIONES GENERALES")) break; //FINAL DE TABLA DE ITEMS

                var aux0 = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                var aux1 = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                var aux2 = pdfLines[i + 3].Trim().DeleteContoniousWhiteSpace();
                var aux3 = pdfLines[i + 4].Trim().DeleteContoniousWhiteSpace();
                var aux4 = pdfLines[i + 5].Trim().DeleteContoniousWhiteSpace();
                var aux5 = pdfLines[i + 6].Trim().DeleteContoniousWhiteSpace();
                var aux6 = pdfLines[i + 7].Trim().DeleteContoniousWhiteSpace();
                var patternAux= $"{aux} {aux0} {aux1} {aux2} {aux3} {aux4} {aux5} {aux6}".Replace(".", "").Replace("$", "").Replace("/", "").DeleteContoniousWhiteSpace();
                Match match = Regex.Match(patternAux, @"^\d{9}");
                if (match.Success)
                {
                    pattern = $"{aux} {aux0} {aux1} {aux2} {aux3} {aux4} {aux5} {aux6}".Replace(".", "").Replace("$", "").Replace("/", "").DeleteContoniousWhiteSpace();
                }
                else
                {
                    continue;
                }
                

               // if (pattern.Contains("Total Neto")) break; //FINAL DE TABLA DE ITEMS
                //Console.WriteLine(pattern);
                //continue;
                //Console.WriteLine($"AUX: {aux}");
                //Console.WriteLine($"AUX1: {aux1}");
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(pattern);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var test00 = aux0.Split(' ');
                        //var test1 = aux1.Trim(); 
                        var item0 = new Item
                        {
                            Sku = test00[0].Trim(),
                            Cantidad = aux1.Trim(),
                            Precio = aux2.Trim().Replace("$", ""),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine("==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var test11 = aux3.Split(' ');
                        //var test1 = aux1.Trim(); 
                        var item1 = new Item
                        {
                            Sku = test1[0].Trim(),
                            Cantidad = aux1.Trim(),
                            Precio = aux2.Trim().Replace("$", ""),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        items.Add(item1);
                        break;
                    case 2:
                        Console.WriteLine("==================ITEM CASE 2=====================");
                        var test2 = pattern.Split(' ');
                        //var test21 = aux3.Split(' ');
                        //var test1 = aux1.Trim(); 
                        var item2 = new Item
                        {
                            Sku = GetSku(pattern.Split(' ')),
                            Cantidad = test2[test2.Length - 4].Trim(),
                            Precio = test2[test2.Length - 3].Trim(),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        items.Add(item2);
                        break;
                    case 3:
                        Console.WriteLine("==================ITEM CASE 3=====================");
                        var test3 = pattern.Split(' ');
                        //var test21 = aux3.Split(' ');
                        //var test1 = aux1.Trim(); 
                        var preciocantidad = GetPrecioCantidad(pattern);
                        var item3 = new Item
                        {
                            Sku = GetSku2(pattern),
                            Cantidad = preciocantidad[0].Trim(),
                            Precio = preciocantidad[1].Trim(),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        if (item3.Precio.Equals("CJ") || item3.Precio.Equals("BOL"))
                        {
                            item3.Precio = test3[test3.Length - 8].Trim();
                        }
                        if (!existeCodigoEnlista(item3.Sku, items))
                        {
                            items.Add(item3);
                        }
                        break;
                }
                

            }
            //SumarIguales(items);
            return items;
        }

        private string[] GetPrecioCantidad(string pattern)
        {
            // ret[0] = cantidad; ret[1] = precio
            string[] ret= new string[] {"",""};
            var arreglo = pattern.Split(' ');
            var tamaño = arreglo.Length;
            for (int i = 0; i < tamaño; i++) {
                if (arreglo[i].Contains("Caja") || arreglo[i].Contains("Unidad") || arreglo[i].Contains("Kit") || arreglo[i].Contains("Rollo") || arreglo[i].Contains("Resma") || arreglo[i].Contains("Kilogramo") || arreglo[i].Contains("Botella") || arreglo[i].Contains("Frasco"))
                {
                    ret[0] = arreglo[i + 1]; // cantidad
                    ret[1] = arreglo[i + 2]; // cantidad
                    break;
                }
            }
            return ret;

        }
        private bool existeCodigoEnlista(string codigo, List<Item> lista)
        {
            bool has = false;
            has = lista.Any(cus => cus.Sku == codigo);
            return has;
        }
        private string GetSku2(string pattern)
        {
            var ret = "Z446482";
            Match match = Regex.Match(pattern, @"\d{9}");
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;

        }
        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            //if (test0.Contains("(PQ)") || test0.Contains("(RESMA)"))
            //{
            //    return ret = test0[1].Trim();

            //}else
            //{

            //    return ret = test0[0].Trim();
            //}
            //return ret;
            //Console.WriteLine($"RAW_TEST1:{test1.ArrayToString(0,test1.Length)}");
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            if (ret.Equals("Z446482"))
            {
                ret = GetSkuClienteFormat(test1);
            }
            return ret;

        }

        private string GetSkuClienteFormat(string[] test1)
        {
            var ret = "Z446482";
            //Console.WriteLine($"TEST1: {test1.ArrayToString(0,test1.Length)}");
            var skuDefaultPosition = test1[1].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"\d{6,10}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"\d{6,10}").Index;
                var length = Regex.Match(skuDefaultPosition, @"\d{6,10}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
                //Console.WriteLine($"SUCCES: {ret}");
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s\d{6,10}\s").Success)
                {
                    var index = Regex.Match(str, @"\s\d{6,10}\s").Index;
                    var length = Regex.Match(str, @"\s\d{6,10}\s").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;

        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split('-');
            return aux[0].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            //  var split = str.Split(' ');
            //  return split[0].Trim(); //split[1].Trim();
            ////  return str.Trim().Replace("*:*","");
            var ret = "Numero de OC No Reconocida";
            if (Regex.Match(str, @"\d{10}").Success)
            {
                var index = Regex.Match(str, @"\d{10}").Index;
                var length = Regex.Match(str, @"\d{10}").Length;
                ret = str.Substring(index, length).Trim();
            }
            return ret;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            //var split = str.Split(' ');
            //return split[split.Length - 1].Trim();
            return str.Trim().Replace("*:*", "");
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test1)
        {

            var ret = "-100";
            for (var i = 0; i < test1.Length - 3; i++)
            {
                Console.WriteLine("test1: " + test1[0]);
                return ret = test1[0].Trim();
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Replace("*","").Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}
