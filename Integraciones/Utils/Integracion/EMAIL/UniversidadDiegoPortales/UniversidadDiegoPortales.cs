﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Email;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Email.UniversidadDiegoPortales
{
    class UniversidadDiegoPortales
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0,@"\d{8}" }
        };


        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA NRO:";
        private const string ItemsHeaderPattern =
            "Centro de Costo:";

        private const string CentroCostoPattern = "Centro de Costo:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly IMail _email;
        private readonly string[] _emailBodyLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public UniversidadDiegoPortales(IMail mail)
        {
            _email = mail;
            _emailBodyLines = _email.GetBodyAsList().ToArray();
        }

        #region Funciones Get
        public List<OrdenCompra.OrdenCompra> GetOrdenCompra()
        {
            List<OrdenCompra.OrdenCompra> OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
            var rutt = "";
            var occ = "";
            var cc = "";


            for (var i = 0; i < _emailBodyLines.Length; i++)
            {
                OrdenCompra = new OrdenCompra.OrdenCompra
                {
                    CentroCosto = "0",
                    TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                    TipoIntegracion = TipoIntegracion.MAIL
                };
                //Console.WriteLine(_emailBodyLines[i]);
                _emailBodyLines[i].Replace(">", "");
                //Console.WriteLine("str: " + _emailBodyLines[i]);
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_emailBodyLines[i]).Replace("*","").Trim();
                         occ = OrdenCompra.NumeroCompra;
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_emailBodyLines[i + 1]).Replace("*","");
                         
                        if (OrdenCompra.Rut.Contains("96670840"))
                        {
                            _readRut = false;
                        }
                        else
                        {
                            rutt = OrdenCompra.Rut;
                            _readRut = true;
                        }


                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_emailBodyLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto1(_emailBodyLines[i + 5]);
                        cc = OrdenCompra.CentroCosto;
                        _readCentroCosto = true;
                    }
                }


                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_emailBodyLines[i]))
                    {
                        var items = GetItems2(_emailBodyLines, i+1);
                        //OrdenCompra.CentroCosto = GetCentroCosto(_emailBodyLines[i]);
                       /* if (OrdenCompra.CentroCosto != "0")
                        {
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;
                            _readCentroCosto = true;
                        }*/
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
                if (_readCentroCosto == true && _readItem == true && _readRut == true && _readOrdenCompra == true)
                {
                    if (OrdenCompra.Rut.Equals(""))
                    {
                        OrdenCompra.Rut = rutt;
                        OrdenCompra.NumeroCompra = occ;
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;                      
                    }
                    OrdenesCompra.Add(OrdenCompra);
                    _readCentroCosto = false;
                    _readItem = false;                    
                }
                if (IsOrdenCompraPattern(_emailBodyLines[i]) && _readOrdenCompra==true)
                {
                    if (OrdenCompra.NumeroCompra != GetOrdenCompra(_emailBodyLines[i]))
                    {
                        break;
                    }
                }

            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                //OrdenCompra.NumeroCompra = _pdfReader.TxtFileNameOC;
            }
            
            //return OrdenCompra;
            return OrdenesCompra;
        }


        private List<Item> GetItems2(string[] pdfLInes,int i)
        {
            var contador = i;
            var items = new List<Item>();
            for (;contador< pdfLInes.Length;contador++)
            {
                if (pdfLInes[contador].Contains("Centro de Costo:"))
                {
                    break;
                }
                var arr = GetCodigos(pdfLInes, contador).Split('|');
                if (arr[0].Equals(""))
                {
                    continue;
                }
                var codigo = arr[0];
                var posicionCodigo = Convert.ToInt32(arr[1]);
                var item = new Item
                {
                    Sku = codigo,
                    Cantidad = pdfLInes[posicionCodigo + 3].Trim(),
                    Precio = pdfLInes[posicionCodigo + 5].Trim(),
                    TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                if (item.Cantidad.Contains("To:"))
                {
                    break;
                }
                contador = posicionCodigo + 5;
                
                items.Add(item);
                

            }
            return items;
        }

        private string GetCodigos(string[] pdfLines, int i)
        {
            string codigos ="";
            var contador = i;

            for (; contador < pdfLines.Length; contador++)
            {
                var largo = pdfLines[contador].Length;
                Match match = Regex.Match(pdfLines[contador], @"\d{8}\s");
                if(pdfLines[contador].Contains("Centro de Costo:"))
                {
                    return "";
                }
                if (match.Success && !pdfLines[contador].Contains("Centro de Costo:") && largo == 11)
                {
                    codigos =match.Value.Trim() +"|"+contador;
                    break;
                    
                }
                
            }

            return codigos;
        }
    
        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //FINAL DE TABLA DE ITEMS                    
                var optItem = GetFormatItemsPattern(_itemsPatterns[0]);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');                                                
                        var item0 = new Item
                        {
                            Sku = test0[0].Trim(),
                            Cantidad = aux.Trim(),
                            Precio = aux.Trim().Replace("$", ""),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        if (pdfLines[i].Contains("Centro de Costo:")) break;
                        items.Add(item0);
                        break;
                    
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            //if (test0.Contains("(PQ)") || test0.Contains("(RESMA)"))
            //{
            //    return ret = test0[1].Trim();

            //}else
            //{

            //    return ret = test0[0].Trim();
            //}
            //return ret;
            //Console.WriteLine($"RAW_TEST1:{test1.ArrayToString(0,test1.Length)}");
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            if (ret.Equals("Z446482"))
            {
                ret = GetSkuClienteFormat(test1);
            }
            return ret;

        }

        private string GetSkuClienteFormat(string[] test1)
        {
            var ret = "Z446482";
            //Console.WriteLine($"TEST1: {test1.ArrayToString(0,test1.Length)}");
            var skuDefaultPosition = test1[1].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"\d{6,10}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"\d{6,10}").Index;
                var length = Regex.Match(skuDefaultPosition, @"\d{6,10}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
                //Console.WriteLine($"SUCCES: {ret}");
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s\d{6,10}\s").Success)
                {
                    var index = Regex.Match(str, @"\s\d{6,10}\s").Index;
                    var length = Regex.Match(str, @"\s\d{6,10}\s").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;

        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            Match match = Regex.Match(str,@"\d{10}\s");
            return match.Value.Trim();
        }

        private static string GetCentroCosto1(string str)
        {
            return str.Trim().ToUpper();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(':');
            var ret = aux[aux.Length - 1].Trim().Replace("\r","");
            return ret;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(':');
            var ret = aux[aux.Length - 1].Replace(".","");
            return ret;
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test1)
        {

            var ret = "-100";
            for (var i = 0; i < test1.Length - 3; i++)
            {
                Console.WriteLine("test1: " + test1[0]);
                return ret = test1[0].Trim();
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Replace("*", "").Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}
