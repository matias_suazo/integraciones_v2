﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.IntegraMedica
{
    class IntegramedicaNuevoFormato
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\d{7,}\s\w{1,}"}

        };
        private const string RutPattern = "ORDEN DE COMPRA";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA";
        private const string ItemsHeaderPattern = "POS. CÓDIGO DESCRIPCIÓN";

        private const string CentroCostoPattern = "CENTRO:";
        private const string ObservacionesPattern = "Tienda :";
        private const string DireccionPattern = "DIRECCIÓN ENTREGA:";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readDireccion;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public IntegramedicaNuevoFormato(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                //CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i-1]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        var Cenco = GetCentroCosto(_pdfLines[i]).ToUpper().Replace("INTEGRAMEDICA","").Replace("INTEGRAMÉDICA","").TrimEnd().TrimStart();
                        //if (_pdfLines[i].Contains("Integramédica") )
                        //{
                        //    Cenco = (Cenco).Replace("INTEGRAMÉDICA ", "").Trim().ToUpper();
                            
                        //}
                        //else
                        //    if(_pdfLines[i].Contains("Integramedica"))
                        //{
                        //    Cenco = (Cenco).Replace("INTEGRAMEDICA", "").Trim().ToUpper();

                        //}
                        OrdenCompra.CentroCosto = Cenco.DeleteAcent().DeleteDuplicateWords().TrimStart().TrimEnd();
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE;
                        if (OrdenCompra.CentroCosto.Equals("CENTRO"))
                        {
                            OrdenCompra.CentroCosto = "6";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                        }
                        if (OrdenCompra.CentroCosto.Equals("NORTE"))
                        {
                            OrdenCompra.CentroCosto = "8";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                        }
                        if (OrdenCompra.CentroCosto.Contains("COPIAP"))
                        {
                            OrdenCompra.CentroCosto = "COPIAPO";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                        }
                        if (OrdenCompra.CentroCosto.Contains("PEÑALOLÉN"))
                        {
                            OrdenCompra.CentroCosto = "PEÑALOLEN";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                        }
                        _readCentroCosto = true;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var direc = _pdfLines[i].Split(':');
                        OrdenCompra.Direccion = direc[1].Trim();
                        _readDireccion = true;
                    }
                }

                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        //var sku = GetSku(pdfLines[i]);
                        //var cantidad = getCantidad(pdfLines, i);
                        //var precio = GetPrecio(pdfLines, i);
                        var item0 = new Item
                        {
                            Sku = test0[1],
                            Cantidad = test0[test0.Length -6],
                            Precio = test0[test0.Length -3],
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                        };
                        if (!test0[test0.Length -5].Equals("UN"))
                        {
                            item0.Cantidad = test0[test0.Length - 5];
                        }
                        items.Add(item0);
                    break;
                }
            }
            //SumarIguales(items);
            return items;
        }
        private string getCantidad(string[] pdfLines, int i)
        {
            var cantidad = "0";

            for (var nuevoContador = i; nuevoContador < pdfLines.Length; nuevoContador++)
            {
                if (pdfLines[nuevoContador].Contains("EA"))
                {
                    if (pdfLines[nuevoContador].Length > 2)
                    {
                        var aux2 = pdfLines[nuevoContador].DeleteContoniousWhiteSpace();
                        var linea = aux2.Split(' ');
                        cantidad = linea[0];
                        break;
                    }
                    else
                    {
                        var aux2 = pdfLines[nuevoContador - 1].DeleteContoniousWhiteSpace();
                        var linea = aux2.Split(' ');
                        cantidad = linea[0];
                        break;
                    }
                }

            }

            return cantidad;
        }

        private string GetSku(string str)
        {
            var ret = "Z446482";
            Match match = Regex.Match(str, _itemsPatterns[0]);
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.ToUpper().Split(':');
            var cc = aux[1].TrimStart().TrimEnd();
            return cc.ToString();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length -1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split('-');
            return split[0].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1";
            for (var nuevoContador = i; nuevoContador < pdfLines.Length; nuevoContador++)
            {
                if (pdfLines[nuevoContador].Contains("EA"))
                {
                    var aux2 = pdfLines[nuevoContador + 1].DeleteContoniousWhiteSpace();
                    var linea = aux2.Split(' ');
                    ret = linea[0].Replace(",", "");
                    break;
                }

            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }
        #endregion
    }
}
