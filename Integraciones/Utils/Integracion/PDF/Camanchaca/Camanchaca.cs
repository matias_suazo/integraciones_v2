﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace Integraciones.Utils.Integracion.Pdf.Camanchaca
{
    class Camanchaca
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{2,3}\s\d{1,}\s[aA-zZ]{1,}" }

        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA N°";
        private const string ItemsHeaderPattern =
            "ARTÍCULOS Y/O SERVICIOS";
        private const string ItemsHeaderPattern2 = "POS CÓDIGO DESCRIPCIÓN";
        private const string CentroCostoPattern = "DESPACHO";
        private const string ObservacionesPattern = "OBSERVACIONES:";
        private const string DireccionPattern = "DIRECCIÓN:";
        private const string PrecioPattern = ",00";



        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private bool _readPrecio;
        private bool _readCantidad;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Camanchaca(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        var Ncompra = _pdfLines[i].Split(' ');
                        OrdenCompra.NumeroCompra = Ncompra[Ncompra.Length -1].Trim();
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i].Trim());
                        _readRut = true;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE;
                        _readCentroCosto = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones = _pdfLines[i].Replace("OBSERVACIONES:", "").Trim();
                        _readObs = true;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = GetDireccion(_pdfLines[i]);//GetDireccion(_pdfLines[i]);
                        _readDireccion = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 

                var optItem = GetFormatItemsPattern(aux);
                //var precio = GetPrecio(pdfLines[i].DeleteContoniousWhiteSpace());
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[1],
                            Cantidad = test0[test0.Length -4].Trim(),
                            Precio = test0[test0.Length - 2].DeleteDotComa(),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        items.Add(item0);
                        break;

                }
            }
            SumarIguales(items);
            return items;
        }

        private string GetSku(string[] pdfLInes, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLInes.Length; contador++)
            {
                Match match = Regex.Match(pdfLInes[contador], @"[aA-zZ]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }
            return ret;
        }
        private static string GetRut(string str)
        {
            var aux = str.Split('-', ' ');
            return aux[1].Trim();
        }
        

        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split('-');
            return aux[0].Trim().Replace("DESPACHO ", "");
        }

        private static string GetDireccion(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[0].Trim();
        }



        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
                break;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static string GetPrecio(string str)
        {
            var split = str.Split(' ');
            return split[0].Trim().Replace(",00", "");
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku) && !items[i].Sku.Equals("Z446482"))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        //private static string GetPrecio(string str)
        //{

        //    var ret = "-1";
        //    for (var i = 0; i < test0.Length; i++)
        //    {
        //        if (test0[i].Equals("CLP"))
        //            return ret = test0[i + 1];
        //    }
        //    return ret;
        //}

        //private string GetCantidad(string[] test1)
        //{
        //    var ret = "-1";
        //    for (var i = 0; i < test0.Length; i++)
        //    {
        //        if (test0[i].Equals("CLP"))
        //            return ret = test0[i - 1];
        //    }
        //    return ret;
        //}


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) | str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsPrecioPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(PrecioPattern);
        }
        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}
