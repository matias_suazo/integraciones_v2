﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.UDLA
{
    public class Udla
    {

        private bool _readOrdenCompra;
        private bool _readRut = true;
        private readonly PDFReader _pdfReader;
        private bool _readObs;
        private bool _readItems = true;
        private readonly string[] _pdfLines;
        private global::Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s-\s\d{1,}\s[a-zA-Z]{1,2}\d{4,6}\s"},//2 - 1 
            {1, @"^\d{1,}\s-\s\d{1,}\s" },
            {2 ,@"\s\d{1,}\s\d{1,}\s\d{1,}$"}
        };

        public Udla(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }


        private List<Item> GetItems(string[] pdfLines, int firstIndex)
        {

            Console.WriteLine("ITEMS ");
            var items = new List<Item>();
            var lastSku = "Z446482";
            for (; firstIndex < pdfLines.Length; firstIndex++)
            {
                
                var str = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Replace("UN","");
                var patron = @"[A-Z]{1,}\d{5,6}\s\d{1,}"; // original
                var descripcion = "";
                if (Regex.Match(str.Trim().DeleteContoniousWhiteSpace(), @"UN\s\d{1,}(\.|\,)").Success)
                {
                    var s = GetSkuR(pdfLines, firstIndex);
                    var c = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    //19 11 2018
                    if (s.Equals("Z446482"))
                    {
                        descripcion = pdfLines[firstIndex + 1].Trim().DeleteContoniousWhiteSpace();
                        Console.WriteLine("no sku");
                    }
                    /*if(c.Length < 8)
                    {
                        auxItem = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace();
                        auxSaltItem = true;
                        continue;
                    }*/

                    var item = new Item
                    {
                        Sku = s,
                        Cantidad = c[c.Length - 5].Trim().Split('.')[0].Replace(",",""),
                        Precio = c[c.Length - 3].Trim().Split('.')[0].Replace(",", ""),
                        TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                    };
                    //19 11 2018
                    if (item.Sku.Equals("Z446482"))
                    {
                        item.Descripcion = descripcion;
                        item.TipoPareoProducto = TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING;
                    }
                    if (item != null || item.Sku != null)
                    {
                        items.Add(item);
                        continue;
                    }
                }
                if (Regex.Match(str, @"\d{1,}\s\-\s[a-zA-Z]{1,2}\d{4,6}").Success && Regex.Match(str, @"\d{1,}\s\d{1,}\s\d{1,}$").Success)
                {
                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');//pdfLines[firstIndex].Trim().Split(' ');
                    var item9 = new Item
                    {
                        Sku = test[2].Trim(),
                        Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", "."),
                        Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                    };
                    items.Add(item9);
                    continue;
                }
                else if (Regex.Match(str, @"\d{1}\s\-\s\w{1}\d{6}").Success && !pdfLines[firstIndex + 1].Equals(" "))
                {
                    //lastSku = GetSku(str);
                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    if (test.Length <= 3)
                    {
                        continue;
                    }
                    if (test.Length == 5)
                    {
                        continue;
                    }
                    var item8 = new Item
                    {
                        Sku = test[test.Length - 6],
                        Cantidad = test[test.Length - 5].Split('.')[0].Replace(",", "").Trim(),
                        Precio = test[test.Length - 3].Split('.')[0].Replace(',', '.').Trim(),
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    if (!Regex.Match(item8.Cantidad, @"\d{1,}").Success)
                    {
                        continue;
                    }
                    if (!Regex.Match(item8.Sku, @"[a-zA-Z]{1,2}\d{4,6}").Success)
                    {
                        item8.Sku = test[test.Length - 5];
                        item8.Cantidad = test[test.Length - 4].Split('.')[0].Replace(",", "").Trim();
                        item8.Precio = "1";
                    }
                    if (!Regex.Match(item8.Sku, @"[a-zA-Z]{1,2}\d{4,6}").Success)
                    {
                        item8.Sku = test[2];
                        item8.Cantidad = test[test.Length - 5].Split('.')[0].Replace(",", "").Trim();
                        item8.Precio = "1";
                    }
                    items.Add(item8);
                    Console.WriteLine($"======================001=============={lastSku}======");
                    continue;
                }
                else if (Regex.Match(str, @"^\d{1,}\s-\s\d{1,}\s\w{1}\d{6}\s").Success)
                {
                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');//pdfLines[firstIndex].Trim().Split(' ');
                    var item = new Item
                    {
                        Sku = test[3].Split('\t')[0],
                        Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", ""),
                        Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                    };
                    items.Add(item);
                    Console.WriteLine($"======================002========{item.Sku}============");
                    continue;

                }
                else if (Regex.Match(str, @"\w{1}\d{6}\s").Success &&
                         Regex.Match(str.Replace(" ,", "").Replace(".", "").Replace(",", ""),
                             @"\d{1,}\s\d{1,}\s\d{1,}$|\d{1,}\s\d{1,}\s\s\d{1,}$").Success)
                {
                    continue;
                    Console.WriteLine("======================003====================");
                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    var item = new Item
                    {
                        Sku = GetSku(test),
                        Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", ""),
                        Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                    };
                    items.Add(item);
                } else if (Regex.Match(str.Replace(",", "").Replace(".", ""),
                     @"\s\w{1}\d{6}\s\d{1,}\s\d{1,}\s\d{1,}$").Success)
                {
                    Console.WriteLine("======================004====================");
                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    var item = new Item
                    {
                        Sku = test[test.Length - 4],
                        Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", ""),
                        Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                    };
                    items.Add(item);
                    continue;
                }
                else if (Regex.Match(str.Replace(",", "").Replace(".", ""),
                             @"\d{1,}\s\d{1,}\s\d{1,}$").Success)
                {
                    Console.WriteLine("======================005====================");
                    //Console.WriteLine(str.Replace(",", "").Replace(".", "") + " === " + lastSku);

                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    var lastOptionSku = GetSku(pdfLines[firstIndex]);
                    var item = new Item
                    {
                        Sku = lastSku.Equals("Z446482") ? lastOptionSku : lastSku,
                        Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", ""),
                        Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                    };
                    if (!item.Sku.Equals(""))
                    {
                        items.Add(item);
                        continue;
                    }

                }
                else if (Regex.Match(str, @"\d{1}\s\-\s\w{1}\d{6}").Success && pdfLines[firstIndex + 1].Equals(" ") && pdfLines[firstIndex + 40].Count() <= _pdfLines[_pdfLines.Length-1].Count())
                {
                    //lastSku = GetSku(str);
                    var test = pdfLines[firstIndex + 40].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    if (test.Length <= 3 || test.Contains("Tatiana"))
                    {
                        continue;
                    }

                    var item10 = new Item
                    {
                        Sku = test[0],
                        Cantidad = test[test.Length - 5].Split('.')[0].Replace(",", "").Trim(),
                        Precio = test[test.Length - 3].Split('.')[0].Replace(',', '.').Trim(),
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    if (item10.Sku.Equals("Línea") || item10.Sku.Equals("Linea") || item10.Sku.Equals("LINEA"))
                    {
                        continue;
                    }
                    if (!Regex.Match(item10.Cantidad, @"\d{1,}").Success)
                    {
                        continue;
                    }
                    items.Add(item10);
                    Console.WriteLine($"======================010=============={lastSku}======");
                    continue;

                }
                else 
                if (Regex.Match(str, @"[aA-zZ]{1,}\d{5,6}\s\w{1,}").Success && Regex.Match(pdfLines[firstIndex - 1], @"\d{1,}\s\-\s\w{1}\d{6}").Success
                        && (pdfLines[firstIndex + 1].Equals(" ")) && (pdfLines[firstIndex + 2].Equals(" ")))
                    {
                    var test = Regex.Match(str, @"[aA-zZ]{1,}\d{5,6}\s\w{1,}").Value.Split(' ');
                    
                        var test41 = pdfLines[firstIndex + 41].Split(' ');
                        if (test41.Count() <= 3)
                        {
                            continue;
                        }
                        var item11 = new Item
                        {
                            Sku = test[0].Replace(",", ""),
                            Cantidad = test41[test41.Length - 5].Split('.')[0].Replace(",",""),
                            Precio = "1",
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        if (!Regex.Match(item11.Cantidad, @"\d{1,}").Success)
                        {
                            continue;
                        }
                        items.Add(item11);
                        Console.WriteLine($"======================010=============={lastSku}======");
                    continue;

                    }

                if (Regex.Match(str, @"\d{1,}\s\-\s\d{4,6}").Success && Regex.Match(str, @"\d{1,}/\d{1,}/\d{1,}$").Success)
                {
                    var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');//pdfLines[firstIndex].Trim().Split(' ');
                    var item9 = new Item
                    {
                        Sku = test[2].Trim(),
                        Cantidad = test[test.Length - 5].Split('.')[0].Replace(",", "."),
                        Precio = test[test.Length - 3].Split('.')[0].Replace(",", ".")
                    };
                    items.Add(item9);
                    continue;
                }

                //var aux = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace();
                ////Es una linea de Items 
                //var optItem = GetFormatItemsPattern(aux);
                //switch (optItem)
                //{
                //    case 0:
                //        Console.WriteLine("==================ITEM CASE 0=====================");
                //        var test0 = aux.Replace("-", "").DeleteContoniousWhiteSpace().Split(' ');
                //        var item0 = new Item
                //        {
                //            Sku = GetSku(test0),
                //            Cantidad = test0[test0.Length-3].Split('.')[0].Replace(",","."),
                //            Precio = test0[test0.Length - 2].Split('.')[0].Replace(",","."),
                //            TipoPareoProducto = TipoPareoProducto.SinPareo
                //        };
                //        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                //        //var concatAll = "";
                //        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                //        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                //        //{
                //        //    concatAll += $" {aux}";
                //        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                //        //}
                //        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                //        items.Add(item0);
                //        break;
                //    case 1:
                //        Console.WriteLine("==================ITEM CASE 1=====================");
                //        var test1 = aux.Replace("-", "").DeleteContoniousWhiteSpace().Split(' ');
                //        var item1 = new Item
                //        {
                //            Sku = GetSku(test1),
                //            Cantidad = test1[test1.Length - 3].Split('.')[0].Replace(",", "."),
                //            Precio = test1[test1.Length - 2].Split('.')[0].Replace(",", "."),
                //            TipoPareoProducto = TipoPareoProducto.SinPareo
                //        };
                //        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                //        //var concatAll = "";
                //        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                //        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                //        //{
                //        //    concatAll += $" {aux}";
                //        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                //        //}
                //        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                //        items.Add(item1);
                //        break;
                //    case 2:
                //        Console.WriteLine("==================ITEM CASE 2=====================");
                //        var test2 = aux.Replace("-", "").DeleteContoniousWhiteSpace().Split(' ');
                //        var item2 = new Item
                //        {
                //            Sku = GetSku(test2),
                //            Cantidad = test2[test2.Length - 3].Split('.')[0].Replace(",", "."),
                //            Precio = test2[test2.Length - 2].Split('.')[0].Replace(",", "."),
                //            TipoPareoProducto = TipoPareoProducto.SinPareo
                //        };
                //        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                //        //var concatAll = "";
                //        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                //        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                //        //{
                //        //    concatAll += $" {aux}";
                //        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                //        //}
                //        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                //        items.Add(item2);
                //        break;
                //}

            }
            return items;
        }

        private string GetSku(string str)
        {
            var index = Regex.Match(str, @"\s\w{1}\d{6}").Index;
            return index == -1 || index == 0 ? "" : str.Substring(index, 8).Trim();
        }


        /// <summary>|
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato: Despachar en ......
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            return str.Substring(str.IndexOf("Despachar en ", StringComparison.Ordinal));
        }

        private string GetSkuR(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value.Trim();
                    break;
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var index = Regex.Match(str, @"\s\w{3}\d{2}-\d{10}\s").Index;
            var length = Regex.Match(str, @"\s\w{3}\d{2}-\d{10}\s").Length;
            return str.Substring(index, length).Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(' ');
            return aux[1];
        }

        public global::Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new global::Integraciones.Utils.OrdenCompra.OrdenCompra {CentroCosto = "0"};
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                //if (!_readObs)
                //{
                //    if (_pdfLines[i].Contains("Entregar bienes en:"))
                //    {
                //        var a1 = _pdfLines[i].Split(':')[1].Trim();
                //        var a2 = _pdfLines[i + 1].Substring(_pdfLines[i + 1].IndexOf("96670840-9", StringComparison.Ordinal) + 11);
                //        var a3 = _pdfLines[i + 2].Substring(11);
                //        var dir = a1 + " " + a2 + " " + a3;
                //        OrdenCompra.Observaciones = "Entregar bienes en: " + dir;
                //        OrdenCompra.Direccion = dir;
                //        _readObs = true;
                //    }
                //}
                if (!_readOrdenCompra)
                {
                    if (_pdfLines[i].Trim().Contains(" Orden de Compra "))
                    {
                        OrdenCompra.CentroCosto = "0";
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[++i]);
                        _readOrdenCompra = true;
                        _readRut = false;
                    }
                }
                if (!_readRut)
                {
                    if (_pdfLines[i].Trim().Contains("R.U.T.: "))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                        _readItems = false;
                    }
                }
                //if (itemForPage < _pdfReader.NumerOfPages)
                //{
                if (!_readItems)
                {
                    if (_pdfLines[i].Trim()
                        .Equals("Lín-Env Art/Descripción Id Fabricante Cantidad Precio U. Neto Total Neto") 
                        || _pdfLines[i].Trim().Equals("Línea Articulo/Servicio Descripción CantidadUM Precio Unitario Importe Total Fecha Entrega") 
                        || _pdfLines[i].Trim().Equals("Línea Articulo/Servicio Descripción Cantidad UM Precio Unitario Importe Total Fecha Entrega"))
                    {
                        //itemForPage++;
                        var items = GetItems(_pdfLines, ++i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItems = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }
        #region Funciones Get
        
        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }
        

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion

    }
}