﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.ClinicaUniversidadAndes
{
    public class ClinicaUniversidadAndes
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s\d{10}\s"},
            {1, @"\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}$" },
            {2, @"\d{2,3}\s\d{8}\s" }
        };
        private const string RutPattern = "RUT ";
        private const string RutPattern2 = "RUT:";
        private const string OrdenCompraPattern = "Número:";
        private const string ItemsHeaderPattern =
            "Pos. Código Descripción Cantidad UM Fecha";
        private const string ItemsHeaderPattern2 = "Precio Neto Neto Total";
        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        public ClinicaUniversidadAndes(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "1",
                TipoIntegracion = TipoIntegracion.PDF,
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };

            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]) || IsRutPattern2(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]) || IsHeaderItemPatterns2(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length -3; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[1].Trim(),//test1[test1.Length - 2].Trim().Replace("material:", ""),//"Z446482",//test0[1],
                            Cantidad = test0[test0.Length - 7].Replace(".", ""),
                            Precio = test0[test0.Length - 4].Replace(".",""),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };

                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        var concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        var sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku))
                        {
                            item0.Sku = sku;
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        items.Add(item0);
                        break;
                    case 1:
                        var test1 = aux.Split(' ');
                        var item1 = new Item
                        {
                            Sku = "Z446482",//test1[test1.Length - 2].Trim().Replace("material:", ""),//"Z446482",//test0[1],
                            Cantidad = test1[test1.Length - 7].Replace(".", ""),
                            Precio = test1[test1.Length - 4].Replace(".", ""),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };

                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku))
                        {
                            item1.Sku = sku;
                            item1.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        items.Add(item1);
                        break;
                    case 2:
                         test0 = aux.Split(' ');
                         item0 = new Item
                        {
                            Sku = "Z446482",//test1[test1.Length - 2].Trim().Replace("material:", ""),//"Z446482",//test0[1],
                            Cantidad = test0[test0.Length - 5].Replace(".", ""),
                            Precio = test0[test0.Length - 2].Replace(".", ""),
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };

                        
                        items.Add(item0);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }

        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(':');
            return aux[1].Replace(" ","");
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(' ')[1];
            return aux.Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "").Replace("$", "");
            foreach (var it in _itemsPatterns)
            {
                Match match = Regex.Match(str, it.Value);
                if (match.Success)
                {
                    ret = it.Key;
                    break;
                }

            }
            
            return ret;
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }
        private bool IsHeaderItemPatterns2(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }
        private bool IsRutPattern2(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern2);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}