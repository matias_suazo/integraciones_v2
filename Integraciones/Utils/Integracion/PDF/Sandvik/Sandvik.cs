﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.Sandvik
{
    class Sandvik
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{4}\-\d{4}\-\s" },
            {1, @"\d{4}\-\d{4}\-\d{1,}" },
            {2, @"\d{2}-\d{2}-\s\d{2,}-\d{2,}-" }
        };
        private const string RutPattern = "Rut:";
        private const string RutPattern1 = "RUT :";
        private const string RutPattern2 = "RODADA :";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA Nro:";
        private const string OrdenCompraPattern1 = "Dirección";
        private const string ItemsHeaderPattern =
            "LINE DUE DATE";
        private const string ItemsHeaderPattern1 =
            "Línea Fecha Vto Nro.Parte";
        private const string ItemsHeaderPattern2 =
            "Listado de ítems";

        private const string CentroCostoPattern = "CeCo:";
        private const string CentroCostoPattern1 = "Centro de Costo";
        private const string ObservacionesPattern = "Tienda :";
        private const string DireccionPattern = "Dirección";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readDir;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Sandvik(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+1]);
                        if (OrdenCompra.Rut.Equals("94879000"))
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i + 3]);
                        }
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;
                        _readCentroCosto = true;
                    }
                }
                if (!_readDir)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var direc = _pdfLines[i];
                        OrdenCompra.Direccion = direc.Split(':')[1].Trim();


                        _readDir = true;
                        _readItem = false;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var aux1 = pdfLines[i + 1].DeleteContoniousWhiteSpace();
                        var aux2 = pdfLines[i + 2].DeleteContoniousWhiteSpace();
                        var test0 = aux.Split(' ');
                        var test01 = aux1.Split(' ');
                        var test02 = aux2.Split(' ');
                        var cantidad = "1";
                        if (test0.Count().Equals(3))
                        {
                            continue; 
                        }
                        var item0 = new Item
                        {
                            Sku = test0[2].Trim() + test01[2].Trim(),
                            Cantidad = test0[test0.Length - 4].Split(',')[0],
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto=TipoPrecioProducto.TELEMARKETING
                        };
                       if (!Regex.Match(item0.Sku, @"\d{4}\-\d{4}\-\d{3}").Success)
                        {
                            item0.Sku = test0[0].Trim() + test01[0];
                        }
                        if (!Regex.Match(item0.Sku, @"\d{4}\-\d{4}\-\d{3}").Success)
                        {
                            item0.Sku = test0[0].Trim() + test02[0];
                        }
                        if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                        {
                            cantidad = GetCantidad(test0);
                            if (!pdfLines.Contains("Unidad"))
                            {

                                if (cantidad.Equals("-1"))
                                {
                                    test0 = pdfLines[i + 1].Split(' ');


                                }
                                cantidad = test0[2];
                            }
                            item0.Cantidad = cantidad;
                        }
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var descripcion = GetDescripcionSku(pdfLines, i);
                        var item1 = new Item
                        {
                            Sku = test1[2].Trim(),
                            Cantidad = test1[test1.Length - 6].Split(',')[0],                            
                            Descripcion = descripcion,
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        items.Add(item1);
                        break;
                    case 2:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        
                        var test3 = GetSkuNuevo(pdfLines, i+2);
                        var test2 = aux.Split(' ');
                        
                        
                        cantidad = GetCantidad(test2);
                        if (!pdfLines.Contains("Unidad"))
                        {
                            
                            if (cantidad.Equals("-1"))
                            {
                                test2 = pdfLines[i + 1].Split(' ');


                            }
                            cantidad = GetCantidad(test2);
                        }
                        
                        var item2 = new Item
                        {
                            Sku = test2[1].Trim()+test3,
                            Cantidad = cantidad/*test2[test2.Length - 6].Split(',')[0]*/,
                            Descripcion = "producto",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        items.Add(item2);
                        break;
                }
            }
            SumarIguales(items);
            return items;
        }

        private string GetDescripcionSku(string[] pdfLines,int i)
        {
            var ret = "";
            var contadorMenos = i - 1;
            var contadorMas = i + 1;
            var linea = pdfLines[contadorMenos] +" "+ pdfLines[contadorMas].DeleteContoniousWhiteSpace().Trim();
            ret = linea;
            return ret;
        }

        private string GetSkuNuevo(string[] pdfLines, int i)
        {
            var ret = "";
            //var contadorMenos = i - 1;
            //var contadorMas = i + 1;
            var linea = pdfLines[i].Split(' ')[1].DeleteContoniousWhiteSpace().Trim();
            ret = linea;
            return ret;
        }
        private string GetCantidad1(string[] pdfLines, int i)
        {
            var ret = "1";
            var linea = pdfLines[i].Split(' ')[1].DeleteContoniousWhiteSpace().Trim();
            ret = linea;
            return ret;
        }


        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            var cc = aux[aux.Length - 1].Trim().Split('-')[0];
            return cc.Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':', ' ');
            return split[split.Length - 1].Trim();
        }

        

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim().Replace(".","").Split('-')[0];
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }
        


        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("Unidad"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern1) 
                || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern)|| str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern1);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern1)
                || str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern2);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern1);
        }

        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}
