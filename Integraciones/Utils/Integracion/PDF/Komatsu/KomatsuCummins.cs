﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Komatsu
{
    public class KomatsuCummins
    {
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs = true;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;
        private global::Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }
        public KomatsuCummins(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private static bool IsItem(string str)
        {
            return Regex.Match(str, @"\d{1,}\sC/U\s\d{1,}").Success;
        }
        private static List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                if (IsItem(aux))
                {
                    var test = aux.Split(' ');
                    var item = new Item
                    {
                        Sku = test[1]
                    };
                    for (var j = 0; j < test.Length; j++)
                    {
                        if (test[j].Equals("C/U") || test[j].Equals("CJ"))
                        {
                            item.Cantidad = test[j - 1];
                            item.Precio = "1";
                        }

                    }
                    items.Add(item);
                }
            }
            SumarIguales(items);

            return items;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    //Console.Write($"|| {i} - {j} => {items[i].Sku} == {items[j].Sku}");
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        //Console.WriteLine($"{i} - {j}\n {items[i]} === {items[j]}");
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = Regex.Match(str, @"\(\w{1}\d{3}\)").Value;
            return aux.Length > 0 ? aux.Substring(1, aux.Length - 2) : aux;
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(':');

            return aux[1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            return str;
        }

        public global::Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new global::Integraciones.Utils.OrdenCompra.OrdenCompra { CentroCosto = "1" };
            var readItem = false;
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                //var obs = "";
                if (!_readRut)
                {
                    if (_pdfLines[i].Trim().Contains("Komatsu Cummins Chile LTDA"))
                    {
                        _readRut = true;
                        OrdenCompra.Rut = GetRut(_pdfLines[++i]);
                    }
                }
                if (!_readOrdenCompra)
                {
                    if (_pdfLines[i].Trim().Contains("Nuestro Pedido"))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                        _readObs = false;
                    }
                }
                if (!_readObs)
                {
                    if (_pdfLines[i].Contains(" Dirección De Entrega para el proveedor:"))
                    {
                        i += 2;
                        OrdenCompra.Observaciones += $"Dirección: {_pdfLines[i].Trim()}, {_pdfLines[i + 2].Trim()}";
                        i += 2;
                        _readObs = true;
                    }
                }
                if (!readItem)
                {
                    if (_pdfLines[i].Trim().Contains("Pos Nº Parte Descripción Cant. UM Valor Unit Total Línea Fe Entrega Solicitante"))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            readItem = true;
                        }
                    }
                }
            }
            OrdenCompra.CentroCosto = "0";
            return OrdenCompra;
        }
    }
}