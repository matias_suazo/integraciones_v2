﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System.Text.RegularExpressions;


namespace Integraciones.Utils.Integracion.Pdf.ConstructoraDVC
{
    class ConstructoraDVC
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @".\d{2,}\s\d{1,}\s\d{1,}.\d{1,}$"}
            //{1, @"[aA-zZ]{4}\d{4}\s\w{1,}" }
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Orden de Compra Nº: Nombre CC:";
        private const string ItemsHeaderPattern = "Forma de Pago";
        private const string CentroCostoPattern = "Orden de Compra Nº: Nombre CC:";
        private const string ObservacionPattern = "Comentarios :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public ConstructoraDVC(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i <_pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = (_pdfLines[i+2]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = _pdfLines[i+1];
                        _readCentroCosto = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionesPattern(_pdfLines[i]))
                    {
                        //var Observa = _pdfLines[i].DeleteContoniousWhiteSpace();
                        //var testObs = Observa.Split(' ');
                        //var iObs = testObs.Length;
                        //var contObs = 1;
                        //var observacion = "";
                        //observacion = String.Join(" ", testObs[0]);

                        //for (; contObs < iObs - 2; contObs++)
                        //{

                        //}

                        OrdenCompra.Observaciones = _pdfLines[i].DeleteContoniousWhiteSpace();
                        _readObs = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i +3);
                        if(items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }

                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }

        private List<Item> GetItems(string[] pdfLine, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLine.Length; i++)
            {
                var aux = pdfLine[i].DeleteContoniousWhiteSpace();
                var optItem = GetFormatItemsPattern(aux);
                
                switch (optItem)
                {
                    case 0:
                        
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var sku = "Z446482";
                        sku = GetSku1(pdfLine, i);
                        if (sku.Equals("Z446482"))
                        {
                            sku = GetSku(pdfLine, i);
                        }//var r = i + 1;
                            var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = test0[test0.Length - 4].Replace(",00",""),
                            Precio = test0[test0.Length - 3].DeleteDotComa(),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };
                        items.Add(item0);
                        break;

                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var sku1 = GetSku(pdfLine, i);
                        var descr = GetDesc(pdfLine, i);
                        var item1 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test1[test1.Length - 5],
                            Precio = test1[test1.Length - 2].Replace(",00", ""),
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };
                        items.Add(item1);
                        break;
                }

            }


            SumarIguales(items);
            return items;
        }

        private string GetDesc(string[] pdfLines, int i)
        {
            var retDesc = "";
            var contadorDesc = i;
            for(; contadorDesc < pdfLines.Length; contadorDesc++)
            {
                Match matchCant = Regex.Match(pdfLines[contadorDesc], @"[aA-zZ]{4}\d{4}\s\w{1,}");
                if (matchCant.Success)
                {
                    retDesc = _pdfLines[contadorDesc +1];
                    break;
                }
            }


            return retDesc;
        }
        private string GetSku1(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (OrdenCompra.Rut.Equals("76259040"))
                {
                    Match match = Regex.Match(pdfLines[contador], @"[aA-zZ]{1,}\d{6,} | COD.\s\d{6}") ;
                    if (match.Success)
                    {
                        ret = match.Value.Replace("COD.", "").Trim();
                        break;
                    }
                }
                
                /*if (OrdenCompra.Rut.Equals("76154079"))
                {

                    Match match3 = Regex.Match(pdfLines[contador], @"[aA-zZ]{1,}\d{5,6}");
                    if (match3.Success)
                    {
                        ret = match3.Value;
                        break;
                    }
                }*/
            }
            return ret;
        }

        private string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (!OrdenCompra.Rut.Equals("76259040"))
                {
                    Match match = Regex.Match(pdfLines[contador], @"[aA-zZ]{1,}\d{6,}");
                    if (match.Success)
                    {
                        ret = match.Value;
                        break;
                    }
                }
                if (OrdenCompra.Rut.Equals("76259040"))
                {

                    Match match2 = Regex.Match(pdfLines[contador], @"[aA-zZ]{4}\d{4}");
                    if (match2.Success)
                    {
                        ret = match2.Value;
                        break;
                    }
                }
                /*if (OrdenCompra.Rut.Equals("76154079"))
                {

                    Match match3 = Regex.Match(pdfLines[contador], @"[aA-zZ]{1,}\d{5,6}");
                    if (match3.Success)
                    {
                        ret = match3.Value;
                        break;
                    }
                }*/
            }
            return ret;
        }

        private static string GetRut(string str)
        {
            var splitRut = str.Split(':', '-');
            return splitRut[splitRut.Length - 2].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones is

        private bool IsOrdenCompraPattern (string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }
        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }
        private bool IsObservacionesPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionPattern);
        }

        #endregion
    }
}
