﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.ClinicaAlemanaValdivia
{
    class ClinicaAlemanaValdivia
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{2,}\s\d{5,}\s"},
            //{1, @"[aA-zZ]{1}\d{5,}" }
        };

        private const string RutPattern = "RUT :";
        private const string OrdenCompraPattern = "PEDIDO N° :";
        private const string OrdenCompraPattern2 = "PEDIDON";
        private const string ItemsHeaderPattern = "N° CódigoProd/Serv DescripcióndelProducto";
        private const string ItemsHeaderPattern2 = "N° Código Prod/Serv Descripción del Producto";

        private const string CentroCostoPattern = "CENTROENTREGA :";
        private const string ObservacionesPattern = "OBSERVACIÓNGENERAL";
        private const string DireccionPattern = "DIRECCIÓN CENTRO:";
        private const string SkuPattern = @"[aA-zZ]{1}\d{5,6}";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public ClinicaAlemanaValdivia(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };

            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE;
                        _readCentroCosto = true;

                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones = _pdfLines[i];
                        _readObs = true;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDirectionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = GetDireccion(_pdfLines[i]);
                        _readDireccion = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.Observaciones.Equals("OBSERVACIÓNGENERAL"))
            {
                OrdenCompra.Observaciones = _pdfReader.PdfFileNameOC;
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.Observaciones = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace().Trim();
                //Es una linea de Items 
                var split = aux.Split(' ');
                var optItem = GetFormatItemsPattern(aux);
                if (aux.Contains("1CJ =") || aux.Contains("1 CJ =") || aux.Contains("1KIT =") || aux.Contains("1 KIT ="))
                {
                    optItem = 1;
                }
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var sku = split[1];
                        var cantidad = GetCantidad(_pdfLines[i].Trim());
                        var precio = GetPrecio(_pdfLines[i].Trim());
                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = cantidad,
                            Precio = precio,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        
                        var sku1 = split[1];
                        var cantidad1 = "0";
                        var precio1 = "0";
                        if (pdfLines[i].Split(' ').Count() <= 3)
                        {
                            //aux = pdfLines[--i].DeleteContoniousWhiteSpace();
                            //cantidad1 = GetCantidad(_pdfLines[--i]);
                            //precio1 = GetPrecio(_pdfLines[--i]);
                            continue;
                        }
                        if (aux.Contains("1CJ ="))
                        {
                            precio1 = split[split.Length - 6];
                            cantidad1 = split[split.Length - 8];
                        }else
                        if (aux.Contains("1 CJ ="))
                        {
                            precio1 = split[split.Length - 8];
                            cantidad1 = split[split.Length - 10];
                        }
                        if (aux.Contains("1KIT ="))
                        {
                            precio1 = split[split.Length - 6];
                            cantidad1 = split[split.Length - 8];
                        }
                        else
                        if (aux.Contains("1 KIT ="))
                        {
                            precio1 = split[split.Length - 8];
                            cantidad1 = split[split.Length - 10];
                        }
                        var item1 = new Item
                        {
                            Sku = sku1,
                            Cantidad = cantidad1,
                            Precio = precio1,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        items.Add(item1);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ', ':');
            return aux[2].Trim();
        }

        private static string GetDireccion(string str)
        {
            var aux = str.Split(':', ' ');
            return aux[2].Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':', '-');
            return split[1].Trim().DeleteDotComa();
        }
        private static string GetSku(string str)
        {
            var ret = "Z446482";
            if (Regex.Match(str, SkuPattern).Success)
            {
                ret = Regex.Match(str, SkuPattern).Value;
            }
            if (!Regex.Match(str, SkuPattern).Success)
            {
                var split = str.Split(' ');
                ret = split[1];
            }
            return ret;
        }


        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            if (Regex.Match(str, SkuPattern).Success & ret == -1)
            {
                ret = 1;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private static string GetCantidad(string str)
        {
            var split = str.Split(' ');
            var splitCant = split[split.Length-8].Trim().TrimEnd();
            if (Regex.Match(splitCant, SkuPattern).Success | !splitCant.Contains(@"\d{1,}"))
            {
                return split[split.Length - 5].Trim();

            }
            else
            {
                return splitCant;
            }
        }

        /*private static string GetCantidad(string str)
        {
            var split = str.Split(' ');
            var splitCant = split[split.Length - 8].Trim();
            string multi = "10 100";
            //int[] cantConv1 = str.Split(' ').Select(int.Parse).ToArray();
            int[] multiplicador = multi.Split(' ').Select(int.Parse).ToArray();
            int[] intSplit = splitCant.Split(' ').Select(int.Parse).ToArray();
            if (Regex.Match(splitCant,SkuPattern).Success)
            {
                if (str.Contains("1") && str.Contains("=") && str.Contains("100"))
                {
                    //string multi = "10 100";
                    int[] cantConv = new int[splitCant.Length];
                    //int[] cantConv1 = str.Split(' ').Select(int.Parse).ToArray();
                    //int[] multiplicador = multi.Split(' ').Select(int.Parse).ToArray();
                    for (int i = 0; i < split.Length; i++)
                    {
                        cantConv[i] = Convert.ToInt32(split[split.Length - 5].ToString());


                    }
                    //return Math.Pow(cantConv1[1]*multiplicador[1]);

                }
                return split[split.Length - 5];
            }
            if (str.Contains("1") && str.Contains("=") && str.Contains("100"))
            {
                var res = intSplit[0] * multiplicador[0];
                return Convert.ToString(res);
            }
            else 
            {
                return splitCant;
            }*/
        private static string GetPrecio(string str)
        {
            var split = str.Split(' ');
            var splitPrecio = split[split.Length - 6].Trim().DeleteDotComa();
            if (!Regex.Match(splitPrecio, @"^\d{3}").Success)
            {
                return split[split.Length - 3].Trim().DeleteDotComa();
            }
            else
            {
                return splitPrecio;
            }
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return (str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern2));
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsDirectionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }
        private bool IsSkuPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(SkuPattern);
        }

        #endregion
    }
}
