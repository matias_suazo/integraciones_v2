﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion
{
    public class CorreosChile
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[a-zA-Z]{1,}\s\d{1,}\s\d{1,}$"},
            //{1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s" }
        };
        private Dictionary<int, string> _direccionesCCliente = new Dictionary<int, string> {
           {31100,"ARTURO PRAT 305, ARICA"},
{31200,"BOLIVAR 458, IQUIQUE"},
{31211,"MODULO 97  SUBTERRANEO, MALL ZOFRI, IQUIQUE"},
{31256,"PASAJE ESLAVONIA 974, ALTO HOSPICIO "},
{31300,"VICUÑA MACKENNA 2197, CALAMA"},
{31312,"BALMACEDA 3242, LOCAL S 07, CALAMA"},
{31400,"WASHINGTON 2601, ANTOFAGASTA"},
{31411,"AV. 21 DE MAYO 1653 (EDIFICIO GOBERNACION), TOCOPILLA"},
{31413,"AV. PEDRO AGUIRRE CERDA 5301, ANTOFAGASTA"},
{31414,"AV. ANGAMOS 0610, INTERIOR UNIVERSIDAD, ANTOFAGASTA"},
{31415,"AV. JOSE BALMACEDA 2355, LOCAL Q1, ANTOFAGASTA"},
{31416,"GUSTAVO LE PAIGE 365, SAN PEDRO DE ATACAMA"},
{31418,"ONGOLMO 601, MEJILLONES"},
{31421,"ARTURO PRAT 525 (INTERIOR MUNICIPALIDAD), TALTAL"},
{31500,"LOS CARRERAS 691, COPIAPO"},
{31511,"COMERCIO 172 (ESQ. LAS HERAS), CHAÑARAL"},
{31513,"AV. 4 DE JULIO 692, EL SALVADOR"},
{31515,"TOCORNAL S/Nº, CALDERA"},
{31516,"JUAN MARTINEZ 808, DIEGO DE ALMAGRO"},
{31517,"LATORRE 115 (ESQ. GRAIG), HUASCO"},
{31520,"MONSEÑOR MIGUEL LEMEUR S/Nº, TIERRA AMARILLA"},
{31523,"JOSE JOAQUIN VALLEJOS S/Nº, VALLENAR"},
{31524,"ERNESTO RIQUELME S/Nº, FREIRINA"},
{32100,"MATTA (ESQ. ARTURO PRAT), LA SERENA"},
{32102,"AV. CUATRO ESQUINAS N° 1617  LOCAL 102, LA SERENA"},
{32111,"ALDUNATE 951, COQUIMBO"},
{32114,"CALLE LARRONDO 1281, COQUIMBO"},
{32115,"ALBERTO SOLARI 1400, LOCAL 6, BOULEVARD DE SERVICIOS, LA SERENA"},
{32127,"GABRIELA MISTRAL 270, VICUÑA"},
{32200,"VICUÑA MACKENNA 330, OVALLE"},
{32211,"AV. ORIENTE 673, COMBARBALA"},
{32222,"BULNES 498, LOCAL 7, SALAMANCA"},
{32223,"LAS LOMAS 108, LA SERENA"},
{32225,"LINCOYAN (ESQ. GALVARINO), LOS VILOS"},
{32246,"AV. IGNACIO SILVA 231, ILLAPEL"},
{32300,"ESMERALDA 387, LOS ANDES"},
{32311,"SALINAS 1392, SAN FELIPE"},
{32400,"LA CONCEPCION 301 (ESQ. BERNARDO O HIGGINS), QUILLOTA"},
{32411,"ALDUNATE 162, LA CALERA"},
{32412,"BLANCO ENCALADA 1015, QUILPUE"},
{32416,"AGUSTIN EDWARDS 143, LLAY-LLAY"},
{32421,"ESMERALDA 460, LA LIGUA"},
{32425,"PLAZA LOS HEROES S/Nº, CABILDO"},
{32500,"MANUEL BULNES N° 920, LOCAL 4, QUILPUE"},
{32511,"AV. VALPARAISO 298, LOCAL 1 Y 2, VILLA ALEMANA"},
{32519,"CONDELL 90 B Y C, LIMACHE"},
{32600,"PLAZA LATORRE 32, VIÑA DEL MAR"},
{32611,"ARLEGUI 440, LOCAL 108 Y 109, VIÑA DEL MAR"},
{32612,"AV. LIBERTAD 741, VIÑA DEL MAR"},
{32615,"ANGAMOS 242, LOCAL 8, STRIP CENTER, ARAUCO EXPRESS PALMARE, VIÑA DEL MAR"},
{32616,"AV. MANANTIALES N 955, LOCAL 107 A, STRIP CENTER TOTTUS, CON-CON"},
{32617,"CABO ORTIZ 160, QUINTERO"},
{32618,"AV. LIBERTAD 1348, MALL MARINA ARAUCO, LOCAL E-14, ENTRE PISO, VIÑA DEL MAR"},
{32700,"PRAT 856 , VALPARAISO"},
{32711,"PEDRO MONTT 2413 - 2415, VALPARAISO"},
{32712,"AV. PLAYA ANCHA 187, VALPARAISO"},
{32713,"LARRAIN ALCALDE 325 A, JUAN FERNANDEZ"},
{32716,"PORTALES 60, CASABLANCA"},
{32717,"TE PITO O TE HENUA S/Nº, ISLA DE PASCUA"},
{32721,"AV. PEDRO MONTT S/Nº, INTERIOR CONGRESO NACIONAL , VALPARAISO"},
{32800,"AV.CENTENARIO 296, SAN ANTONIO"},
{32811,"BARROS LUCO 2048, SAN ANTONIO"},
{32812,"COVADONGA 456, CARTAGENA"},
{32813,"PEÑABLANCA 407, ALGARROBO"},
{33100,"CAMPOS 322, RANCAGUA"},
{33128,"SANTA LUCIA 183, GRANEROS"},
{33139,"URRIOLA 17, RENGO"},
{33148,"GERMAN RIESCO 664, SAN VICENTE TAGUA-TAGUA"},
{33200,"AV. BERNARDO O HIGGINS 545 , SAN FERNANDO"},
{33211,"CLAUDIO CANCINO 65, SANTA CRUZ"},
{33234,"MIRAFLORES 403, LOCAL 6, CHIMBARONGO"},
{33236,"ANIBAL PINTO 45, PICHILEMU"},
{33300,"CARMEN 556, CURICO"},
{33329,"QUECHEREGUAS 1748, LOCAL 2, MOLINA"},
{33400,"1 ORIENTE 1150, TALCA"},
{33411,"MONTT 425, CONSTITUCION"},
{33500,"MANUEL RODRIGUEZ 610, LINARES"},
{33535,"ANTONIO VARAS S/Nº, CAUQUENES"},
{33546,"ARTURO PRAT 2486, SAN JAVIER"},
{33557,"DIECIOCHO 720, PARRAL"},
{34100,"LIBERTAD 501, CHILLAN"},
{34143,"VICTOR BIANCHI 411, BULNES"},
{34151,"ARTURO PRAT 305, ARICA"},
{34157,"ESMERALDA 380, YUNGAY"},
{34160,"ESMERALDA 698, QUIRIHUE"},
{34161,"MAIPU 694, SAN CARLOS"},
{34200,"COLO COLO 417, CONCEPCION"},
{34211,"CORNELIO SAAVEDRA 781, CAÑETE"},
{34223,"MANUEL MONTT 798, CORONEL"},
{34228,"CAUPOLICAN 340, LOCAL 3 Y 5, ARAUCO"},
{34235,"PEDRO AGUIRRE CERDA 457, LOTA"},
{34237,"IGNACIO SERRANO 1055, TOME"},
{34244,"CARDENIO AVELLO 720, CURANILAHUE"},
{34246,"COCHRANE 120 local.4, CHIGUAYANTE"},
{34250,"GALERIA EL FORO, LOCAL 3, UNIVERSIDAD DE CONCEPCION, CONCEPCION"},
{34261,"CHACABUCO 70, CONCEPCION"},
{34263,"ANDRES BELLO 215 (ESQ. PEREZ), LEBU"},
{34268,"O HIGGINS 500 , PENCO"},
{34270,"MICHIMALONCO 1021, SAN PEDRO DE LA PAZ"},
{34284,"JORGE ALESSANDRI 3177, LOCAL 20, MALL PLAZA EL TREBOL, CONCEPCION"},
{34300,"AVENIDA  CRISTOBAL COLON  553  - LOCAL 2, TALCAHUANO "},
{34400,"CAUPOLICAN 464, LOS ANGELES"},
{34411,"BALMACEDA 280, LOCAL 7 Y 8, LAJA"},
{34418,"AV. VIAL S/Nº, CABRERO"},
{34424,"ANIBAL PINTO 477, NACIMIENTO"},
{34427,"OHIGGINS 899, YUMBEL"},
{34432,"ANIBAL PINTO 473, MULCHEN"},
{34433,"ANGEL PARRA 493-A, CHILLAN VIEJO"},
{34500,"LAUTARO 226, ANGOL"},
{34511,"YUNGAY 265, CURACAUTIN"},
{34518,"CORONEL URRUTIA 465, TRAIGUEN"},
{34529,"O CARROL 785, COLLIPULLI"},
{34533,"CALAMA 1208, VICTORIA"},
{34600,"PORTALES 801, TEMUCO"},
{34601,"AV. ALEMANIA 0671, LOCAL 1061, TEMUCO"},
{34611,"PEDRO LAGOS 339, NUEVA IMPERIAL"},
{34614,"VILLAGRAN 391, CARAHUE"},
{34619,"SANTA MARÍA 182, CUNCO"},
{34621,"FREIRE 560, GORBEA"},
{34624,"FRESIA 183, PUCON"},
{34630,"O HIGGINS 1050, LAUTARO"},
{34637,"PRAT 590, LONCOCHE"},
{34641,"FRANCISCO BILBAO 593, PITRUFQUEN"},
{34642,"CAMALEZ 113, FREIRE"},
{34656,"ANFION MUÑOZ 315, VILLARRICA"},
{35100,"BERNARDO O HIGGINS 575 , VALDIVIA"},
{35111,"ARTURO PRAT 680, LA UNION"},
{35114,"CONDOR S/Nº, LANCO"},
{35116,"ARTURO PRAT 305, ARICA"},
{35117,"CAMILO HENRIQUEZ 403, PAILLACO"},
{35121,"ARTURO ALESSANDRI S/Nº, PANGUIPULLI"},
{35132,"AV. EDUARDO MORALES, CASA 23, CAMPUS ISLA TEJA, VALDIVIA"},
{35134,"PEDRO LAGOS 470, RIO BUENO"},
{35141,"MARIQUINA 1202, SAN JOSE DE LA MARIQUINA"},
{35145,"BUERAS 1931, VALDIVIA"},
{35200,"O HIGGINS 645, OSORNO"},
{35211,"PEDRO MONTT 196, PURRANQUE"},
{35231,"PAUL HARRIS (ESQ. AV. MACKENNA), RIO NEGRO"},
{35300,"RANCAGUA 126, PUERTO MONTT"},
{35311,"ERNESTO RIQUELME 28, CALBUCO"},
{35314,"BERNARDO OHIGGINS 426, LLANQUIHUE"},
{35318,"AV. ALESSANDRI 430, LOCAL 1, FRUTILLAR"},
{35320,"IRARRAZABAL 401, FRESIA"},
{35326,"SAN JOSE 242, PUERTO VARAS"},
{35365,"POLPAICO 17, PUERTO MONTT"},
{35400,"BERNARDO O HIGGINS 338, CASTRO"},
{35437,"22 DE MAYO 397, QUELLON"},
{35438,"PUDETO 201, ANCUD"},
{35500,"COCHRANE 226, COYHAIQUE"},
{35531,"SARGENTO ALDEA 1265 A, PUERTO AYSEN"},
{35600,"BORIES 911, PUNTA ARENAS"},
{35601,"AV. PRESIDENTE MANUEL BULNES S/Nº, ZONA FRANCA, PUNTA ARENAS"},
{35611,"EBERHARD 429, PUERTO NATALES"},
{35615,"CENTRO COMERCIAL S/Nº, CABO DE HORNOS"},
{35629,"BERNARDO PHILLIPI 183, PORVENIR"},
{35631,"BASE PRESIDENTE EDUARDO FREI MONTALVA, ANTARTICA"},
{36100,"CATEDRAL 989, SANTIAGO"},
{36111,"AV. FERMIN VIVACETA 957 - LOCAL 4, INDEPENDENCIA"},
{36112,"ARTURO PRAT 1377, LAMPA"},
{36113,"VITACURA 6460, VITACURA"},
{36114,"ASTURIAS 110, LAS CONDES"},
{36115,"GALVARINO 1436, CERRO NAVIA"},
{36116,"SAN PABLO 8919 E, PUDAHUEL"},
{36117,"AV. AMBROSIO O HIGGINS 1414 , CURACAVI"},
{36118,"MANUEL ANTONIO MATTA 436, LOCAL 101, QUILICURA"},
{36119,"AV. AMERICO VESPUCIO 1737, LOCAL BS 1096, MALL PLAZA NORTE, HUECHURABA"},
{36120,"Domingo Santa María 4084,  Local 2., RENCA"},
{36121,"SAN MARTIN 29, SANTIAGO"},
{36124,"AV.  LA CONCEPCIÓN 212, COLINA"},
{36128,"SANTA CLARA 354, LOCAL 111, CUIDAD EMPRESARIAL, HUECHURABA"},
{36129,"COMPAÑIA 1140, TRIBUNALES DE JUSTICIA (INTERIOR), SANTIAGO"},
{36130,"PATRONATO 95, RECOLETA"},
{36200,"AV. BERNANDO O HIGGINS 2352, SANTIAGO"},
{36211,"DOCTOR ERNESTO PRADO TAGLE 156, PEÑAFLOR"},
{36213,"ARTURO PRAT 305, ARICA"},
{36214,"Avda. Pajaritos 2229, MAIPU"},
{36215,"BERNARDO O HIGGINS 1198 , TALAGANTE"},
{36216,"INDEPENDENCIA 4553 H, CONCHALI"},
{36217,"APOQUINDO 6554 B, LAS CONDES"},
{36219,"GRAN AVENIDA JOSE MIGUEL CARRERA 10226, EL BOSQUE"},
{36220,"ALHUE 2609, PEDRO AGUIRRE CERDA"},
{36221,"ROTONDA PONIENTE PUERTA 4, NIVEL 1, PUDAHUEL"},
{36222,"DOCTOR CARLOS ARANDA 6099, CERRILLOS"},
{36223,"SAN PABLO 6050, LO PRADO"},
{36224,"AV. OSSA 1771, SAN RAMON"},
{36227,"WALKER MARTINEZ 1681, QUINTA NORMAL"},
{36231,"EXPOSICION 51, LOCAL 1159, ESTACION CENTRAL"},
{36233,"Plaza de Armas 529 Melipilla, MELIPILLA"},
{36234,"AV. AMERICO VESPUCIO 1501, LOCAL BT 119 , CERRILLOS "},
{36235,"EXPOSICION 221, PISO 1, ESTACION CENTRAL"},
{36300,"AV. MANUEL ANTONIO MATTA 1034, SANTIAGO"},
{36311,"VICUÑA MACKENNA ORIENTE 7110, LOCAL 44 Y 45, SECTOR BOULEVARD, LA FLORIDA"},
{36312,"GRAN AVENIDA JOSE MIGUEL CARRERA 4901, SAN MIGUEL"},
{36313,"AV. CAMILO HENRIQUEZ 3692, LOCAL BS 118, PUENTE ALTO"},
{36315,"ERNESTO RIQUELME 010 , LA GRANJA"},
{36316,"BALDOMERO LILLO 1721, LA PINTANA"},
{36317,"AV. OSSA 36, LOCAL 11 A, INTERMODAL LA CISTERNA (interior), LA CISTERNA"},
{36400,"AV. NUEVA PROVIDENCIA  2092, PROVIDENCIA"},
{36412,"MAR DE LOS SARGAZOS 5879, LOCAL 8, LAS CONDES"},
{36415,"AV. NUEVA LAS CONDES 12243, LOCAL 61 , LAS CONDES"},
{36416,"APOQUINDO 3291 , LAS CONDES"},
{36418,"PROVIDENCIA 1466, PROVIDENCIA"},
{36420,"ALONSO DE CORDOVA 3772, VITACURA"},
{36421,"PEDRO DE VALDIVIA 1781, LOCAL 52, PROVIDENCIA"},
{36422,"AV. VITACURA 6780, LOCAL 13 D,  VITACURA"},
{36423,"AV. Nueva Providencia 2530  Local 102, PROVIDENCIA"},
{36430,"EL RODEO 12540, LO BARNECHEA"},
{36431,"APOQUINDO 4499, LOCAL 102, LAS CONDES"},
{36432,"SAN ENRIQUE 14915, LO BARNECHEA"},
{36433,"AV. PRESIDENTE KENNEDY 5413, LAS CONDES"},
{36500,"SUECIA 3624, ÑUÑOA"},
{36511,"PRINCIPE DE GALES 7010, LA REINA"},
{36512,"AV. LARRAIN 6116, LA REINA"},
{36513,"AV. MACUL 3344, LOCAL 9, MACUL"},
{36514,"AV. GRECIA 3232, LOCAL 4, ÑUÑOA"},
{36516,"AV. GRECIA 5536, LOCAL A, PEÑALOLEN"},
{36517,"VICUÑA MACKENNA 886 A, ÑUÑOA"},
{36518,"AV SUECIA 26, PROVIDENCIA"},
{36519,"IRARRAZAVAL 4949 LOC.1, ÑUÑOA"},
{36520,"PADRE HURTADO 1621 LOCAL 7, LAS CONDES"},
{36600,"MONEDA 1170, SANTIAGO"},
{36601,"MONEDA 748, LOCAL 10, SANTIAGO"},
{36602,"METRO ESTACION UNIVERSIDAD DE CHILE, LOCAL 25, SANTIAGO"},
{36603,"AMUNATEGUI 518, SANTIAGO"},
{36604,"MAC-IVER N° 440, LOCAL 102, SANTIAGO"},
{36611,"AV. BERNARDO O HIGGINS 254, SANTIAGO"},
{36700,"COVADONGA 493, SAN BERNARDO"},
{36711,"CONDELL 421, BUIN"},
{36712,"GENERAL BAQUEDANO 890, LOCAL 5, PAINE"},
{36724,"AV. CONCHA Y TORO 26 PISO 2, MALL PLAZA PUENTE ALTO, PUENTE ALTO"},
{36726,"AV. CONCHA Y TORO 02548, PIRQUE"},
{36727,"Sergio Roubillar #62 (ex. Santa Josefina), PUENTE ALTO"}


        };
        private const string RutPattern = "RUT : 60.50";
        private const string OrdenCompraPattern = "N° de Pedido :";
        private const string ItemsHeaderPattern = "Precio Un. Valor Neto";
        private const string ItemsHeaderPattern2 = "PrecioUn. ValorNeto";
        private const string CentroCostoPattern = "Centro de Costo :";
        private const string ObservacionesPattern = "Observaciones";
        private const string DireccionPattern = "Dirección de entrega :";
        private const string ContactoPattern = "de Contacto :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _dirEntrega;
        private bool _readContacto = false;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public CorreosChile(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            bool direccionVacia = false;
            bool observacionVacia = false;
            var contacto = "";
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (IsContactoPattern(_pdfLines[i]) && !_readContacto)
                {
                    contacto = getContacto(_pdfLines, i);
                    _readContacto = true;
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        _readCentroCosto = true;
                    }
                }


                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var ob = getObservacion(_pdfLines, i);
                        if (ob.Equals(" ") || (ob.Equals("")))
                        {
                            observacionVacia = true;
                        }
                        else
                        {
                            OrdenCompra.Observaciones = ob;
                            OrdenCompra.Direccion = "";
                            //OrdenCompra.Observaciones = OrdenCompra.Observaciones;
                        }

                        _readObs = true;
                        _readItem = false;
                    }
                }

                if (!_dirEntrega)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var dir = GetDireccion(_pdfLines[i]);

                        if (!dir.Equals("N°"))
                        {
                            //
                            if (_pdfLines[++i].Contains("Comuna :"))
                            {
                                OrdenCompra.Direccion = dir;
                            }
                            else
                            {
                                //GetDireccion(_pdfLines[i])
                                OrdenCompra.Direccion = $"{ dir } {_pdfLines[i].Replace("Mail Proveedor: cmedina@dimerc.cl", "").Trim()}";
                            }

                        }
                        else
                        {
                            direccionVacia = true;
                        }



                        _dirEntrega = true;
                        _readItem = false;
                    }
                }

                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]) || IsHeaderItemPatterns2(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC.Replace("OC", "");
            }
            if (observacionVacia)
            {
                OrdenCompra.Observaciones = OrdenCompra.Direccion;
            }
            if (!direccionVacia)
            {
                OrdenCompra.Direccion = "";
            }

            if (_readContacto == true)
            {
                OrdenCompra.Observaciones = $"CONTACTO: {contacto} , {OrdenCompra.Observaciones}";

            }
            if (!GetObservacionDic(OrdenCompra.CentroCosto).Equals(""))
            {
                OrdenCompra.Direccion = GetObservacionDic(OrdenCompra.CentroCosto);
                OrdenCompra.Observaciones = OrdenCompra.Observaciones.Split(',')[0].Trim();
            }
            else
            {

            }

            return OrdenCompra;
        }

        private string GetObservacionDic(string cencos)
        {
            var ret = "";
            string value;
            int cc = Convert.ToInt32(cencos);
            if (_direccionesCCliente.TryGetValue(cc, out value))
            {
                ret = value;
            }
            else
            {
                ret = "";
            }

            //ret = this._direccionesCCliente[cc];
            return ret;
        }
        private string getObservacion(string[] pdfLines, int i)
        {
            var aux1 = "not found";
            var contador = i;
            var observacion = "";
            observacion = pdfLines[contador].Split(':')[1];
            contador++;
            for (; contador < pdfLines.Length; contador++)
            {

                if (pdfLines[contador].Contains("Moneda :"))
                {
                    break;
                }
                else
                {
                    observacion += " " + pdfLines[contador];
                    // break;
                }
            }
            // var aux = v.Split(':');
            // return $"{aux[aux.Length -1].Trim()} ";
            return observacion;
        }
        private string getContacto(string[] pdfLines, int i)
        {
            var aux = pdfLines[i];
            var contacto = aux.Split(':');
            var f = contacto[contacto.Length - 1];
            return f;
        }
        private string GetDireccion(string v)
        {
            var aux1 = "not found";
            var aux = v.Split(':');
            aux1 = aux[aux.Length - 1];
            return $"{aux1.Trim()}";
        }

        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux.Replace(".", ""));
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[1],
                            Cantidad = test0[test0.Length - 4].Split(',')[0],
                            Precio = test0[test0.Length - 2].Replace(".", ""),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO

                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //var sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //if (OracleDataAccess.ExistProduct(sku))
                        //{
                        //    item0.Sku = sku;
                        //    item0.TipoPareoProducto = TipoPareoProducto.SinPareo;
                        //}
                        items.Add(item0);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            var oc = split[split.Length - 1].Trim();
            if (oc == "")
            {
                oc = split[split.Length - 2].Trim();
            }
            return oc;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            return split[2].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }
        private bool IsHeaderItemPatterns2(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }
        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private bool IsContactoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ContactoPattern);
        }

        #endregion

    }
}