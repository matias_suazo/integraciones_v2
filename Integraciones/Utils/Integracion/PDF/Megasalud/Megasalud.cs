﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Megasalud
{
    public class Megasalud
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\d{8,}\s"}
            //{1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s" }
            //702002019 
        };
        private const string RutPattern = "ORDEN DE COMPRA";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA:";
        private const string ItemsHeaderPattern =
            "CODIGO UNIDAD CANTIDAD C.C. DESCRIPCIÓN P. UNITARIO DESCUENTO VALOR TOTAL";
        private const string ItemsHeaderPattern1 =
            "POS. CÓDIGO DESCRIPCIÓN";
        

        private const string CentroCostoPattern = "PEDIDO Nº";
        //ORDEN DE COMPRA N
        private readonly Dictionary<int, string> _centroCostoPatterns = new Dictionary<int, string>
        {
            {0, "PEDIDO Nº"},
            {1, "ORDEN DE COMPRA" }
        };
        private const string CentroCostoPattern2 = "DESPACHAR A :";
        private const string ObservacionesPattern = "DIRECCIÓN ENTREGA:";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        public Megasalud(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i+2]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    //if (IsCentroCostoPattern(_pdfLines[i]))
                    //{
                    //    OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                    //    _readCentroCosto = true;
                    //}
                    //else if (IsCentroCostoPattern2(_pdfLines[i])) {

                    //    OrdenCompra.CentroCosto = GetCentroCosto2(_pdfLines[i]);
                    //    _readCentroCosto = true;
                    //}
                    /*
                    var optCencos = IsCentroCostoPatterns(_pdfLines[i]);
                    if (optCencos != -1)
                    {
                        switch (optCencos)
                        {
                            case 0:
                                OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                                break;
                            case 1:
                                OrdenCompra.CentroCosto = GetCentroCosto2(_pdfLines[i]);
                                OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                                break;
                        }

                        _readCentroCosto = true;
                    }
                    */

                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = _pdfLines[i].Split(':')[1];
                        _readObs = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            var contadorPrecio = 0;
            string[] listaprecios = GetPrecios(pdfLines, i).ToArray();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        var aux0 = pdfLines[i-1].DeleteContoniousWhiteSpace();
                        if(aux0.Contains("TOTAL BRUTO"))
                        {
                            break;
                        }
                        var test0 = aux.Split(' ');
                        var test1 = aux0.Split(' ');
                        var precio = GetPrecio(pdfLines, i);
                        var countCant =test0.Length;
                        if (test0.Contains("SOLPED"))
                        {
                            continue;
                        }
                        if (countCant <= 7)
                        {
                            continue;
                        }
                        var item0 = new Item
                        {
                            Cantidad = test0[test0.Length -6].Replace(".", ""),
                            Precio = "1",
                            Sku = test0[1].Trim(),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        
                        items.Add(item0);
                        contadorPrecio++;
                        break;
                }
            }
            
            //SumarIguales(items);
            return items;
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"(\d{1,}\.\d{1,}\s0|\d{1,}\s0)");
                if (match.Success)
                {
                    ret = match.Value.Split(' ')[0];
                    break;
                }
            }
            return ret;
        }

        private List<string> GetPrecios(string[] pdfLines,int i)
        {
            List<string> lista = new List<string>();
            var valor = "";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"(\d{1,}\.\d{1,}\s0|\d{1,}\s0)");
                if (match.Success)
                {
                    valor = match.Value.Split(' ')[0];
                    lista.Add(valor);
                }
            }
            return lista;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[5].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
                ret = skuDefaultPosition;
            else
            {
                var str = test1.ArrayToString(0, test1.Length -1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length -2].Trim();
        }
        private static string GetCentroCosto2(string str)
        {
            //return "81022040";
            var aux = str.Split(' ', '-');
            return aux[aux.Length - 2];
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split('-')[0];
            return aux.Trim().DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            return ret;
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern)|| str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern1);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            str = str.ToUpper();
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private int IsCentroCostoPatterns(string str)
        {
           return GetFormatCentroCostoPattern(str);
        }

        private int GetFormatCentroCostoPattern(string str)
        {
            str = str.DeleteDotComa();
            foreach (var cencos in _centroCostoPatterns.Where(cencos => str.DeleteContoniousWhiteSpace().Contains(cencos.Value)))
            {
                return cencos.Key;
            }
            return -1;
        }
        private bool IsCentroCostoPattern2(string str)
        {
            str = str.ToUpper();
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern2);
        }
        #endregion

    }
}