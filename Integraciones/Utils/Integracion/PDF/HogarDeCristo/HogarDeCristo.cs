﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.HogarDeCristo
{
    class HogarDeCristo
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\w{1,}\s\d{1,}.\d{1,}" }
            //{1, @"\d{1}\s\d{5}" }
        };
        private const string RutPattern = "R.U.T.:";
        private const string OrdenCompraPattern = "Folio :";
        private const string ItemsHeaderPattern = "CÓDIGO DESCRIPCIÓN CANTIDAD";
        private const string CentroCostoPattern = "Nombre C.C.:";
        private const string ObservacionesPattern = "Observacion :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public HogarDeCristo(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "72323600",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i +1]);
                        _readCentroCosto = true;
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE;
                    }
                }

                /*if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var split = _pdfLines[i].Split(':');
                        OrdenCompra.Observaciones = split[split.Length - 1];
                        _readObs = true;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var split = _pdfLines[i].Split(':');
                        OrdenCompra.Direccion = split[1].Replace("Forma de Pago", "").Trim();
                        _readDireccion = true;
                    }
                }*/

                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            if (OrdenCompra.Observaciones.Equals(""))
            {
               OrdenCompra.Observaciones = OrdenCompra.NumeroCompra;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        //var sku = GetSku(pdfLines, i);
                        var item0 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test0[test0.Length - 4].DeleteDotComa(),
                            Precio = test0[test0.Length - 2].DeleteDotComa(),
                            Descripcion = test0.ArrayToString(0, test0.Length - 5),
                            TipoPareoProducto = TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var sku1 = GetSku(pdfLines, i);
                        var item1 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test1[test1.Length - 5],
                            Precio = test1[test1.Length - 2].Replace(",00", ""),
                            Descripcion = test1[test1.Length - 3],
                            //TipoPareoProducto = TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };

                        items.Add(item1);
                        break;
                }
            }
            SumarIguales(items);
            return items;
        }

        private string GetSku(string[] pdfLInes, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLInes.Length; contador++)
            {
                Match match = Regex.Match(pdfLInes[contador], @"[aA-zZ]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            return aux.ArrayToString(0 , aux.Length - 3); //[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length -1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split('-', ':');
            return split[1].Trim().Replace(".", "");
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku) && !items[i].Sku.Equals("Z446482"))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        //private bool IsDireccionPattern(string str)
        //{
        //    return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        //}

        #endregion
    }
}
