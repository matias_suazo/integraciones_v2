﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra.Integracion;

namespace Integraciones.Utils.Integracion.Pdf.LatinGaming
{
    class LatinGaming
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\d{4,}\s\w{1,}-\d{8,}"}
            //{5, @"^\d{1,}\s(\w{1,}\s\w{1,}\.?)*" }
        };
        private const string OrdenCompraPattern = "ORDEN DE COMPRA";
        private const string OrdenCompraPattern2 = "N°";
        private const string RutPattern = "RUT: ";
        private const string ItemsHeaderPattern =
            "Descripción Cuenta Centro Costo";
        private const string CentroCostoPattern = @"\d{5,}\s\d{6}\s[aA-zZ]{1,}";
        private const string ObservacionesPattern = "SOLICITA:";
        private const string DireccionPattern = @"\d{5,}\s\d{6}\s[aA-zZ]{1,}";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }
        private List<OrdenCompra.OrdenCompra> OrdenesCompra { get; }
        #endregion

        public LatinGaming(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
            OrdenesCompra = new List<OrdenCompra.OrdenCompra>();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                Rut = "99599080",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };



            //for (var i = 0; i < _pdfLines.Length; i++)
            var fila = 0;
            var listaItems = new List<Item>();
            for (var i = 0; i < _pdfLines.Length; i++) // while (i < _pdfLines.Length)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i].DeleteDotComa());
                        _readRut = true;
                    }
                }
                
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var Split = _pdfLines[i];
                        OrdenCompra.Observaciones = Split.Trim();
                        _readObs = true;
                        _readItem = false;
                    }
                }
                


                /*if (!_readItem)
                {if (IsHeaderItemPatterns(_pdfLines[i]))
                {
                    var items = GetItems(_pdfLines, i);
                    if (items.Count > 0)
                    {
                        OrdenCompra.Items.AddRange(items);
                        _readItem = true;
                    }
                }}*/
            }

            while (fila < _pdfLines.Length)
            {
                
                    if (IsDireccionPattern(_pdfLines[fila]))
                    {
                        List<string> address = new List<string>();
                        var listAddress = GetDireccion(_pdfLines, fila);
                        OrdenCompra.Direccion = _pdfLines[fila];
                        _readDireccion = true;
                    }
                
                    if (IsCentroCostoPattern(_pdfLines[fila]))
                    {
                        List<string> ArrayCencos = new List<string>();
                        //OrdenCompra.CentroCosto 
                        var listcenco = GetCentroCosto(_pdfLines, fila);
                        OrdenCompra.Observaciones = OrdenCompra.CentroCosto;
                        if (OrdenCompra.CentroCosto.Equals("0"))
                        {
                            continue;
                        }
                        OrdenCompra.CentroCosto = OrdenCompra.CentroCosto.Split('-')[1];
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                    }
                var items = new List<Item>();
                List<string> cantidades = new List<string>();
                List<string> precios = new List<string>();
                List<string> codigos = new List<string>();
                //List<string> descripcion = new List<string>();

                cantidades = GetCantidades(_pdfLines, fila);
                codigos = GetCodigos(_pdfLines, fila);
                precios = GetPrecios(_pdfLines, fila);
                //var descripcion = GetDescripcion(_pdfLines, fila);

                for (var cont = 0; cont < codigos.Count; cont++)
                {
                    var itemSe = new Item
                    {
                        Sku = codigos[cont],
                        Cantidad = "1",
                        Precio = "1",
                        Descripcion = GetDescripcion(_pdfLines, cont),
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    items.Add(itemSe);

                }
                Console.WriteLine();
                
                if (OrdenCompra.CentroCosto.Equals(OrdenCompra.Items[fila].Descripcion) &&
                        OrdenCompra.Rut.Equals(99599080) &&
                        OrdenCompra.CentroCosto.Equals(GetCentroCosto(_pdfLines, fila).Trim()))
                {
                    continue;
                }

                var ordenCompra = new OrdenCompra.OrdenCompra
                {
                        Rut = OrdenCompra.Rut.Trim(),
                        CentroCosto = (OrdenCompra.CentroCosto.Equals(OrdenCompra.Items[fila].Descripcion) ? "0" : OrdenCompra.CentroCosto),
                        Items = OrdenCompra.Items,
                        Direccion = OrdenCompra.Direccion,
                        NumeroCompra = OrdenCompra.NumeroCompra,
                        Observaciones = OrdenCompra.Observaciones,
                        TipoIntegracion = TipoIntegracion.CARGA_ESTANDAR,
                        TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
                };
                if (!_pdfLines[fila].Contains("COTIZACIÓN"))
                {
                    break;
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            OrdenesCompra.Add(OrdenCompra);


            

            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();
            

            cantidades = GetCantidades(pdfLines, i);
            codigos = GetCodigos(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < cantidades.Count; cont++)
            {
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    Descripcion= "",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();

            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("COTIZACIÓN"))
                {
                    break;
                }
                Match match7 = Regex.Match(pdfLines[contador], @"CL_[a-zA-Z]{1,2}\d{5,6}");
                if (match7.Success)
                {
                    listaCodigos.Add(match7.Value.Split('_')[1]);
                }
                Match match8 = Regex.Match(pdfLines[contador], @"CL_\d{5,6}");
                if (match8.Success)
                {
                    listaCodigos.Add(match8.Value.Split('_')[1]);
                }
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("COTIZACIÓN"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(pdfLines[contador], @"\d{1,},\d{1,}\s\d{1,},\d{1,}\s\d{1,},\d{1,}");
                Match match1 = Regex.Match(pdfLines[contador], @"\d{1,}[.]\d{1,},\d{1,}\s\d{1,},\d{1,}\s\d{1,},\d{1,}");
                Match match2 = Regex.Match(pdfLines[contador], @"\d{1,},\d{1,}\s\d{1,}.\d{1,},\d{1,}\s\d{1,}[.]\d{1,},\d{1,}");
                Match match3 = Regex.Match(pdfLines[contador], @"\d{1,}[.]\d{1,},\d{1,}\s\d{1,}[.]\d{1,},\d{1,}\s\d{1,}[.]\d{1,},\d{1,}");
                if (match.Success)
                {
                    listaCantidades.Add(match.Value.TrimStart().Split(' ', ',')[0].DeleteDotComa());
                    continue;
                }
                if (match1.Success)
                {
                    listaCantidades.Add(match1.Value.TrimStart().Split(' ', (','))[0].DeleteDotComa());
                    continue;
                }
                if (match2.Success)
                {
                    listaCantidades.Add(match2.Value.TrimStart().Split(' ', ',')[0].DeleteDotComa());
                    continue;
                }
                if (match3.Success)
                {
                    listaCantidades.Add(match3.Value.TrimStart().Split(' ', ',')[0].DeleteDotComa());
                    continue;
                }
                

            }

            return listaCantidades;
        }
        private static string GetDescripcion(string[] pdfLines, int fila)
        {
            List<string> listaDescrip = new List<string>();
            var contador = fila;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("COTIZACIÓN"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(pdfLines[contador], @"\d{5,}\s\d{6}\s[aA-zZ]{1,}");
                if (match.Success)
                {
                    listaDescrip.Add(match.Value.TrimStart().Split(' ')[1].DeleteDotComa());
                    continue;
                }
            }
            return listaDescrip.ToString();

        }
        private static string GetCentroCosto(string[] pdfLines, int fila)
        {
            List<string> listaCcosto = new List<string>();
            var contador = fila;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("COTIZACIÓN"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(pdfLines[contador], @"\d{5,}\s\d{6}\s[aA-zZ]{1,}");
                if (match.Success)
                {
                    listaCcosto.Add(match.Value.TrimStart().Split(' ')[1].DeleteDotComa());
                    continue;
                }
            }

            return listaCcosto.ToString();

        }
        private static string GetDireccion(string[] pdfLines, int fila)
        {
            List<string> listaDireccion = new List<string>();
            var contador = fila;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("COTIZACIÓN"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(pdfLines[contador], @"\d{5,}\s\d{6}\s[aA-zZ]{1,}");
                if (match.Success)
                {
                    listaDireccion.Add(match.Value.TrimStart().Split(' ')[2].DeleteDotComa());
                    continue;
                }


            }

            return listaDireccion.ToString();

        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        

        private static string GetObserv(string str)
        {
            var aux = str.Split(':');
            //var aux1 = aux[1].Trim();
            return aux[aux.Length - 1].Trim();

        }

        

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var vrut = str.Split('-', ':');
            return vrut[vrut.Length - 2];


        }
        private static string GetRut2(string str)
        {
            var vrut = str.Split(' ');
            var rutf = vrut[vrut.Length - 1].Split('-')[0];
            if (rutf != "96670840-9")
            {
                return rutf;
            }
            else
            {
                return rutf.Split('-')[0];
            }
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern2);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}

