﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Zara
{
    class Zara
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\d{4,}\s\w{1,}-\d{8,}"}
            //{5, @"^\d{1,}\s(\w{1,}\s\w{1,}\.?)*" }
        };
        private const string RutPattern = "RUT Personas Fisicas";
        private const string OrdenCompraPattern = "PEDIDO";
        private const string OrdenCompraPattern2 = "ALBERTO PEPPER 1784";
        private const string ItemsHeaderPattern =
            "Id. Destino Destino Dirección";
        private const string CentroCostoPattern = "TGT-";
        private const string ObservacionesPattern = "Comprador";
        private const string DireccionPattern = "ALBERTO PEPPER";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Zara(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                Rut = "81698900",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+1]);
                        if (!OrdenCompra.NumeroCompra.Contains(@"\d{6,}"))
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i + 2]);
                        }
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        if (OrdenCompra.Rut.Equals("967858609"))
                            {
                            OrdenCompra.Rut = "96785860";
                            }
                        if (OrdenCompra.Rut.Equals("763093468"))
                        {
                            OrdenCompra.Rut = "76309346";
                        }
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.Observaciones = OrdenCompra.CentroCosto;
                        if (OrdenCompra.CentroCosto.Equals("0"))
                        {
                            continue;
                        }
                        OrdenCompra.CentroCosto = OrdenCompra.CentroCosto.Split('-')[1];
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                        _readCentroCosto = true;
                        _readObs = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var Split = _pdfLines[i].Trim().Split(' ');
                        var Observ = Split[Split.Length - 2].DeleteDotComa();
                        var Observ1 = Split[Split.Length - 3].DeleteDotComa();
                        OrdenCompra.Observaciones = Observ1 + " " + Observ;
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = _pdfLines[i].Replace("ALBERTO PEPPER 1784 RENCA","");
                        _readDireccion = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            cantidades = GetCantidades(pdfLines, i);
            codigos = GetCodigos(pdfLines, i);
            precios = GetPrecios(pdfLines, i);
            

            
            for (var cont = 0; cont < cantidades.Count; cont++)
            {
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();
            
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Total:"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"^\d{6}\s\d{3,}\s[aA-zZ]{2,}");
                if (match.Success)
                {
                    listaCodigos.Add(pdfLines[contador].Split(' ')[0]);
                    continue;
                }

                Match match1 = Regex.Match(pdfLines[contador], @"^[a-zA-Z]{1,2}\d{5,6}\s\d{3,}\s[aA-Zz]{2,}");
                if (match1.Success)
                {
                    listaCodigos.Add(match1.Value.Split(' ')[0]);
                    continue;
                }
                Match match2 = Regex.Match(pdfLines[contador], @"^\d{6}\s[aA-zZ]{3,}\s\d{1,}");
                if (match2.Success)
                {
                    listaCodigos.Add(match2.Value.Split(' ')[0]);
                }

                Match match3 = Regex.Match(pdfLines[contador], @"^[a-zA-Z]{1,2}\d{5,6}\s[aA-zZ]{3,}\s\d{2,}");
                if (match3.Success)
                {
                    listaCodigos.Add(match3.Value.Split(' ')[0]);
                }
                Match match7 = Regex.Match(pdfLines[contador], @"^\d{6}\s\d{3,}\s\d{1,}");
                if (match7.Success)
                {
                    listaCodigos.Add(match7.Value.Split(' ')[0]);
                }
                Match match8 = Regex.Match(pdfLines[contador], @"^[a-zA-Z]{1,2}\d{5,6}\s\d{3,}\s\d{2,}");
                if (match8.Success)
                {
                    listaCodigos.Add(match8.Value.Split(' ')[0]);
                }
                Match match9 = Regex.Match(pdfLines[contador], @"^\s[a-zA-Z]{1,2}\d{5,6}\s\d{3,}");
                if (match9.Success)
                {
                    var codigo = match9.Value.Split(' ');
                    listaCodigos.Add(codigo[1].Replace(" ", ""));
                }
                Match match10 = Regex.Match(pdfLines[contador], @"^\s\d{6}\s\d{3,}\s\d{1,}");
                if (match10.Success)
                {
                    var codigo = match10.Value.Split(' ');
                    listaCodigos.Add(codigo[1].Replace(" ", ""));
                }
                Match match11 = Regex.Match(pdfLines[contador], @"^Red Code\s\d{3,}");
                if (match11.Success)
                {
                    var codigo = pdfLines[contador+1];
                    listaCodigos.Add(codigo.Replace(" ", ""));
                }
                //if (!match.Success && !match1.Success && !match2.Success && !match3.Success)
                //{
                //    Match match4 = Regex.Match(pdfLines[contador], @"\d{1,}\s[a-zA-Z]{1,2}\d{5,6}");
                //    Match match5 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{5,6}\s");
                //    Match match6 = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}\sCLP");
                //    if (match4.Success)
                //    {
                //        listaCodigos.Add(match4.Value.Split(' ')[1]);
                //    }
                //    if (match5.Success)
                //    {
                //        listaCodigos.Add(match5.Value.Split(' ')[1]);
                //    }
                //    if (match6.Success)
                //    {
                //        listaCodigos.Add(match6.Value.Split(' ')[0]);
                //    }
                //}
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Total:"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match =  Regex.Match(pdfLines[contador], @"\d{1,},\d{1,}\s\d{1,},\d{1,}\s\d{1,},\d{1,}");
                Match match1 = Regex.Match(pdfLines[contador], @"\d{1,}[.]\d{1,},\d{1,}\s\d{1,},\d{1,}\s\d{1,},\d{1,}");
                Match match2 = Regex.Match(pdfLines[contador], @"\d{1,},\d{1,}\s\d{1,}.\d{1,},\d{1,}\s\d{1,}[.]\d{1,},\d{1,}");
                Match match3 = Regex.Match(pdfLines[contador], @"\d{1,}[.]\d{1,},\d{1,}\s\d{1,}[.]\d{1,},\d{1,}\s\d{1,}[.]\d{1,},\d{1,}");
                if (match.Success)
                {
                    listaCantidades.Add(match.Value.TrimStart().Split(' ', ',')[0].DeleteDotComa());
                    continue;
                }
                if (match1.Success)
                {
                    listaCantidades.Add(match1.Value.TrimStart().Split(' ',(','))[0].DeleteDotComa());
                    continue;
                }
                if (match2.Success)
                {
                    listaCantidades.Add(match2.Value.TrimStart().Split(' ', ',' )[0].DeleteDotComa());
                    continue;
                }
                if (match3.Success)
                {
                    listaCantidades.Add(match3.Value.TrimStart().Split(' ', ',')[0].DeleteDotComa());
                    continue;
                }

            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.TrimEnd().Split(' ');
            var aux0 = aux[aux.Length - 1].Trim();
            var aux1 = aux[aux.Length - 2].Trim();
            var aux2 = aux[aux.Length - 3].Trim();
            var aux3 = aux[aux.Length - 4].Trim();
            var aux4 = aux[aux.Length - 5].Trim();
            Match match0 = Regex.Match(aux0, @"TGT-\d{1,}");
            Match match1 = Regex.Match(aux1, @"TGT-\d{1,}");
            Match match2 = Regex.Match(aux2, @"TGT-\d{1,}");
            Match match3 = Regex.Match(aux3, @"TGT-\d{1,}");
            Match match4 = Regex.Match(aux4, @"TGT-\d{1,}");
            if (match0.Success)
            {
                return match0.Value.Trim();
            }
            if (match1.Success)
            {
                return match1.Value.Trim();
            }
            if (match2.Success)
            {
                return match2.Value.Trim();
            }
            if (match3.Success)
            {
                return match3.Value.Trim();
            }
            if (match4.Success)
            {
                return match4.Value.Trim();
            }
            
            return "0";

        }

        private static string GetObserv(string str)
        {
            var aux = str.Split(':');
            //var aux1 = aux[1].Trim();
            return aux[aux.Length - 1].Trim();

        }

        private static string GetDireccion(string str)
        {
            var aux = str;
            //var aux1 = aux[1].Trim();
            return aux.DeleteDuplicateWords();

        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var vrut = str.Split('-', ' ');
            return vrut[vrut.Length - 1];


        }
        private static string GetRut2(string str)
        {
            var vrut = str.Split(' ');
            var rutf = vrut[vrut.Length - 1].Split('-')[0];
            if (rutf != "96670840-9")
            {
                return rutf;
            }
            else
            {
                return rutf.Split('-')[0];
            }
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern2);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}
