﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Respuesta;

namespace Integraciones.Utils.Integracion.Pdf.Axis
{
    public class Axis
    {
        #region Variables
        /// <summary>
        /// Patrón de busqueda para los Items
        /// </summary>
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            //{0, @"^\d{1,}\s\d{1,}$"},
            {0, @"^\d{1}$" },
            {1, @"^\d{2}$" }
        };


        /// <summary>
        /// Patrón de Busqueda para la Cantidad
        /// </summary>
        private readonly Dictionary<int, string> _cantidadItemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s(UN|CAJA)\s"},
            {1, @"^\d{1,}\s\d{1,}$"},
            {2, @"^\d{1,}\s\d{1,}\s\d{1,}$"}
        };


        /// <summary>
        /// Patrón que se debe Eliminar en la Descripción
        /// </summary>
        private readonly Dictionary<int, string> _deleteDescripcionPatterns = new Dictionary<int, string>
        {
            {0, @"UN\s[A-Z]{5}\d{2}\s"},
            {1, @"CARTRIDGE\sDE\sTINTA\s" }
        };
        private const string RutPattern = "Forma de despacho";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA N";
        private const string ItemsHeaderPattern =
            "Descripción Valor Unitario Valor Total";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Axis(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();//.ExtractTextFromPdfToArraySimpleStrategy(); 
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i-1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[++i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[--i]);
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }

        private string DeleteDescripcionPattern(string descripcion)
        {
            var ret = "";
            foreach (var patternDescripcion in _deleteDescripcionPatterns)
            {
                Console.WriteLine($"Descripcion: {descripcion}, P: {patternDescripcion.Value}");
                ret = Regex.Replace(descripcion, patternDescripcion.Value,"");
            }
            return ret;
        }

        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length - 2; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                if (optItem != -1)
                    Console.WriteLine("");
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var descripcion1 = pdfLines[i - 1].DeleteContoniousWhiteSpace().ToUpper();//i + 1
                        var descripcion2 = pdfLines[i + 1].DeleteContoniousWhiteSpace().ToUpper();//i + 2
                        var descripcion = descripcion2.Contains(descripcion1)
                            ? $"{descripcion2}"
                            : $"{descripcion1} {descripcion2}";
                        descripcion = DeleteDescripcionPattern(descripcion);
                        var item0 = new Item
                        {
                            Sku = "SIN_SKU",
                            Descripcion = descripcion.DeleteAcent().DeleteContoniousWhiteSpace(),
                            TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE
                        };
                        var sku = GetSku(descripcion.Split(' '));
                        var sku2 = GetSku(pdfLines[i + 2].DeleteContoniousWhiteSpace().ToUpper().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku) && !sku.Equals("Z446482"))
                        {
                            item0.Sku = sku;
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }else if (OracleDataAccess.ExistProduct(sku2) && !sku2.Equals("Z446482"))
                        {
                            item0.Sku = sku2;
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();

                        aux = pdfLines[i-1].DeleteContoniousWhiteSpace();
                        for (var j = i-1 ; j > 0 && GetFormatItemsPattern(aux) == -1; j--)
                        {
                            var respuesta = GetCantidad(aux);
                            if (respuesta.Cantidad != -1 && respuesta.Precio != -1)
                            {
                                item0.Cantidad = respuesta.Cantidad.ToString();
                                item0.Precio = respuesta.Precio.ToString();
                                break;
                            }
                            aux = pdfLines[j].DeleteContoniousWhiteSpace();
                        }
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine("==================ITEM CASE 1=====================");
                        descripcion1 = pdfLines[i - 1].Trim().DeleteContoniousWhiteSpace().ToUpper();//i + 1
                        descripcion2 = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace().ToUpper();//i + 2
                        descripcion = $"{descripcion1} {descripcion2}";
                        descripcion = DeleteDescripcionPattern(descripcion);
                        var item1 = new Item
                        {
                            Sku = "SIN_SKU",
                            Descripcion = descripcion.DeleteAcent(),
                            TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE
                        };
                        sku = GetSku(descripcion.Split(' '));
                        if (OracleDataAccess.ExistProduct(sku) && !sku.Equals("Z446482"))
                        {
                            item1.Sku = sku;
                            item1.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();

                        aux = pdfLines[i - 1].DeleteContoniousWhiteSpace();
                        for (var j = i - 1; j > 0 && GetFormatItemsPattern(aux) == -1; j--)
                        {
                            var respuesta = GetCantidad(aux);
                            if (respuesta.Cantidad != -1 && respuesta.Precio != -1)
                            {
                                item1.Cantidad = respuesta.Cantidad.ToString();
                                item1.Precio = respuesta.Precio.ToString();
                                break;
                            }
                            aux = pdfLines[j].DeleteContoniousWhiteSpace();
                        }
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        items.Add(item1);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            return str;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1];
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",", "").Replace(".", "");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private int GetFormatCantidadItemPattern(string str)
        {
            var ret = -1;
            str = str.ToUpper().Replace(",", "").Replace(".", ""); //.Replace("PAR MATSB26", "").Replace("  ", " ");
            foreach (var it in _cantidadItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private ResultadoCantiadPrecioItem GetCantidad(string str)
        {
            var cantidad = -1;
            var precio = -1;
            var optCantidad = GetFormatCantidadItemPattern(str);
            var split = str.Split(' ');
            switch (optCantidad)
            {
                case 0:
                    cantidad = int.Parse(split[0].Split(',')[0]);
                    precio = int.Parse(split[split.Length - 1].Split(',')[0].Replace(".", ""));
                    break;
                case 1:
                    cantidad = int.Parse(split[0].Split(',')[0]);
                    precio = int.Parse(split[split.Length - 1].Split(',')[0].Replace(".",""));
                    break;
                case 2:
                    cantidad = int.Parse(split[0].Split(',')[0]);
                    precio = int.Parse(split[split.Length - 2].Split(',')[0].Replace(".", ""));
                    break;
            }
            return new ResultadoCantiadPrecioItem
            {
                Cantidad = cantidad,
                Precio = precio
            };
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}