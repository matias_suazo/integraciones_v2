﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion
{
    public class Enim
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            //            
            {0, @"[A-Z]{2,3}\d{3,4}" },// sku cliente
            {1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{3,}\s\d{1,}\s\d{1, }" }
        };
        private const string RutPattern = " RUT :";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA Nº:";
        private const string ItemsHeaderPattern =
            "CÓDIGO DESCRIPCION CANT FECHA ENTREGA PRECIO";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Tienda :";
        private const string DireccionPattern = "DESPACHAR A";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Enim(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "79527230",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
               if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }

                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = GetDireccion(_pdfLines[i]);
                        _readDireccion = true;
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var sku = GetSku(pdfLines, i);
                        var cantidad = GetCantidad(pdfLines, i);
                        var precio = GetPrecio(pdfLines, i);
                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = cantidad,
                            Precio = precio,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //var sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //if (OracleDataAccess.ExistProduct(sku))
                        //{
                        //    item0.Sku = sku;
                        //    item0.TipoPareoProducto = TipoPareoProducto.SinPareo;
                        //}
                        items.Add(item0);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] pdfLines,int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], _itemsPatterns[0]);
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 1].Trim();
        }

        private static string GetDireccion(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim().Replace(", RUT", "");
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':', '-');
            return split[split.Length -2].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1";
            var contador = i - 1;
            bool encontrado = false;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\d{1,}\/\d{1,}\/\d{4}");
                if (match.Success)
                {
                    var linea = pdfLines[contador];
                    var arrLinea = linea.Split(' ');
                    int x = 0;
                    foreach (var item in arrLinea)
                    {
                        Match match2 = Regex.Match(item, @"\d{1,}\/\d{1,}\/\d{4}");
                        if (match2.Success)
                        {
                            int posicion = x;
                               
                            ret = arrLinea[x+1].Replace(".","");
                            encontrado = true;
                            break;
                        }
                        x++;
                    }
                }
                if (encontrado)
                {
                    break;
                }
            }

            return ret;
        }

        private string GetCantidad(string[] pdfLines,int i)
        {
            var ret = "-1";
            var contador = i-1;
            bool encontrado = false;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\d{1,}\/\d{1,}\/\d{4}");
                if (match.Success)
                {
                    var linea = pdfLines[contador];
                    var arrLinea = linea.Split(' ');
                    foreach (var item in arrLinea)
                    {
                        Match match2 = Regex.Match(item, @"\d{1,}\,\d{1,}");
                        if (match2.Success) {
                            ret = match2.Value.Split(',')[0];
                            encontrado = true;
                            break;
                        }
                    }
                }
                if (encontrado)
                {
                    break;
                }
            }
            
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion

    }
}