﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.Socovesa
{
    class Socovesa
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[A-Z]{2}\d{5,6}\s(\w|\d)"}, // codigo cliente
            
        };
        private const string RutPattern = "RUT";
        private const string OrdenCompraPattern = "RUT";
        private const string ItemsHeaderPattern =
            "CODIGO CANTIDAD UN TAREA DETALLE";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Despachar a :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Socovesa(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        var contadoroc = i - 1;
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[contadoroc]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                /*if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        _readCentroCosto = true;
                    }
                }*/
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones +=
                            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var patsku = GetSku(pdfLines, i);
                        var sku = patsku.Split('-')[0];
                        var esdimerc = patsku.Split('-')[1];

                        var cantidad = GetCantidad(pdfLines, i);
                        var precio = GetPrecio(pdfLines, i);


                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad =cantidad,  
                            Precio = precio,                          
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };
                        if (precio == "-1")
                        {
                            item0.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                        }
                        if (esdimerc.Equals("1"))
                        {
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        
                        items.Add(item0);
                        break;
                }
            }
            SumarIguales(items);
            return items;
        }

        private string GetSku(string[] pdfLines,int i)
        {
            var ret = "Z446482";
            var contador = i;
            var patronDimerc = @"\.\s[A-Z]{1,}\d{5,6}";
            var esdimerc = "0";
            bool encontrocodigodimerc = false;
            //ciclo codigo dimerc
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], patronDimerc);
                if (match.Success)
                {
                    esdimerc = "1";
                    ret = match.Value.Replace(".","").Trim()+"-"+esdimerc;
                    encontrocodigodimerc = true;
                    break;
                }
            }
            if (!encontrocodigodimerc) {
                contador = i;
                for (;contador<pdfLines.Length;contador++)
                {
                    Match match2 = Regex.Match(pdfLines[contador], _itemsPatterns[0]);
                    if (match2.Success)
                    {
                        ret = match2.Value.Split(' ')[0].Trim()+"-"+esdimerc;
                        break;
                    }
                }
            }
            
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            return str.Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            return split[1].Trim().Split('-')[0].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }   
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1"; int contador = i-1;
            var patronPrecio = @"(\d{1,}\,00|\d{1,}\.\d{1,}\,00)\s(0,00|\()";
            int n;
            bool isNumeric = false;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], patronPrecio);
                if (match.Success)
                {
                    var test0 = match.Value.Trim().Split(' ');
                    var precio = test0[0].Replace(",00", "").Replace(".", "");
                    isNumeric = int.TryParse(precio, out n);
                    if (isNumeric)
                    {
                        ret = precio;
                        break;
                    }

                }
            }



            return ret;
        }

        private string GetCantidad(string[] pdfLines, int i)
        {
            var ret = "-1"; int contador = i;
            int n;
            bool isNumeric = false;
            
            Match match = Regex.Match(pdfLines[contador], _itemsPatterns[0]);
            if (match.Success)
            {
                var test0 = pdfLines[contador].Split(' ');
                var digito = test0[1].Replace(",00","");
                isNumeric = int.TryParse(digito, out n);
                if (isNumeric)
                {
                    ret = digito;
                }
                else
                {
                    test0 = pdfLines[contador - 1].Split(' ');
                    ret = test0[1].Trim().Replace(",00", "");
                }
                
            }
            

         
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion
    }
}
