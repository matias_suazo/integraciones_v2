﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.InmobiliariaAconcagua
{
    class InmobiliariaAconcagua
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{2,}\s\d{1,}\,\d{2,}"}
        };
        private const string RutPattern = "RUT :";
        private const string OrdenCompraPattern = "PEDIDO Nº";
        private const string ItemsHeaderPattern = "Item Cantidad Un. Descripción Precio Unit. Dcto./Cgo. Valor Total";
        private const string CentroCostoPattern = "Destino";
        private const string ObservacionPattern = "Observaciones :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public InmobiliariaAconcagua(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "76801420",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readCentroCosto)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOc(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCCosto(_pdfLines[i]);
                        _readCentroCosto = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionesPattern(_pdfLines[i]))
                    {
                        var observaciones = GetObs(_pdfLines[i]);
                        var contact = GetContacto(_pdfLines[i + 1]);
                        OrdenCompra.Observaciones = observaciones + " " + contact;
                        _readObs = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }

                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }

        private List<Item> GetItems(string[] pdfLine, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLine.Length; i++)
            {
                var aux = pdfLine[i].DeleteContoniousWhiteSpace();
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        //var sku = GetSku(pdfLine, i);
                        //var cant = GetCant(pdfLine, i);
                        var item0 = new Item
                        {
                            Sku = test0[4],
                            Cantidad = test0[1].Replace(",00", ""),
                            Precio = test0[test0.Length - 3].Replace(",00", ""),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        items.Add(item0);
                        break;
                }

            }


            //SumarIguales(items);
            return items;
        }

        

        string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            //var contador = i;
            //for (; contador < pdfLines.Length; contador++)
            //{
            //    Match match = Regex.Match(pdfLines[contador], @"[aA-zZ]{1,}\d{6,}");
            //    if (!match.Success)
            //    {
            //        ret = match.Value;
            //        break;
            //    }
            //    //else
            //    //if (match.Success)
            //    //{

            //    //    Match matchCant = Regex.Match(pdfLines[contador-2], @"\$\s\w[aA-zZ]{1,}\s\d{1,}\,\d{2,}");
            //    //    if (matchCant.Success)
            //    //    {

            //    //    }
            //    //}
            //}
            return ret;
        }

        private static string GetRut(string str)
        {
            var splitRut = str.Split(':', '-');
            return splitRut[splitRut.Length - 2].Trim().Replace('.', ' ');
        }

        private static string GetOc(string str)
        {
            var SplitOc = str.Split(':');
            return SplitOc[SplitOc.Length - 1].Trim();
        }

        private static string GetCCosto(string str)
        {
            var SplitCc = str.Split(':');
            return SplitCc[SplitCc.Length - 1].Trim();
        }

        private static string GetObs(string str)
        {
            var SplitOb = str.Split(':');
            return SplitOb[SplitOb.Length - 1].Trim();
        }

        private static string GetContacto(string str)
        {
            var contacto = str;
            return contacto;
        }
        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        

        #region Funciones is

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }
        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }
        private bool IsObservacionesPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionPattern);
        }

        #endregion


    }
}


//private static void SumarIguales(List<Item> items)
//{
//    for (var i = 0; i < items.Count; i++)
//    {
//        for (var j = i + 1; j < items.Count; j++)
//        {
//            if (items[i].Sku.Equals(items[j].Sku))
//            {
//                items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
//                items.RemoveAt(j);
//                j--;
//                Console.WriteLine($"Delete {j} from {i}");
//            }
//        }
//    }
//}
