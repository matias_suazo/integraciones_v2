﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.ConstructoraBYC
{
    class ConstructoraBYC
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[A-Z]{1,2}[0-9]{5,6}.-"}, // coddimerc
            {1, @"\s[0-9]{1,3},00\s" } // cantidad
        };
        private const string RutPattern = "R.U.T.";
        private const string OrdenCompraPattern = "Nº";
        private const string ItemsHeaderPattern =
            "Codigo C.C. Descripción Glosa Cuenta Gto. Partida U.Medi Cantidad";

        private const string CentroCostoPattern = "S/Material";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public ConstructoraBYC(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var cont = i + 1;
                        var items = GetItems(_pdfLines, cont);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                if (aux.Contains("OBSERVACION"))
                {
                    break;
                }
                else if (aux.Contains("da"))
                {
                    continue;
                }                
                var optItem = GetFormatItemsPattern(aux);                            
                
                var sku = "";
                var cantidad = "";
                var precio = "";
                bool flag_skucantidad = false;
                bool flag_sku = false;
                if (contieneSkuCantidadPatron(pdfLines, i))
                {
                    Console.WriteLine("primer if");
                    flag_skucantidad = true;
                    var test0 = aux.Split(' ');
                    sku = GetSku(aux);
                    cantidad = test0[test0.Length - 4].Replace(",00", "").Replace(".", "");
                    precio = test0[test0.Length - 3].Replace(",00", "").Replace(".", "");
                    //var sku = ;
                }
                if (contieneSoloSku(pdfLines, i))
                {
                    var test0 = aux.Split(' ');
                    sku = GetSku(aux);
                    cantidad = GetCantidad(pdfLines, i);
                    precio = GetPrecio(pdfLines, i);
                    Console.WriteLine("segundo if");
                    flag_sku = true;
                }
                if (flag_skucantidad == false && flag_sku == false)
                {
                    continue;
                }
                var item0 = new Item
                {
                    Sku = sku,
                    Cantidad = cantidad,
                    Precio = precio,
                    Descripcion = "",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                };

                items.Add(item0);
            }
            
            return items;
        }


        private string GetSku(string str)
        {
            var ret = "Z446482";

            Match match = Regex.Match(str, _itemsPatterns[0]);
            if (match.Success)
            {
                ret = match.Value.Replace(".-", "").Trim();

            }


            return ret;
        }
        private bool contieneSkuCantidadPatron(string[] pdfLines, int i)
        {
            bool respuesta = false;
            Match match_sku = Regex.Match(pdfLines[i], _itemsPatterns[0]);
            Match match_cantidad = Regex.Match(pdfLines[i], _itemsPatterns[1]);
            if (match_sku.Success && match_cantidad.Success)
            {
                respuesta = true;
            }
            return respuesta;
        }
        private bool contieneSoloSku(string[] pdfLines,int i)
        {
            bool respuesta = false;
            Match match_sku = Regex.Match(pdfLines[i], _itemsPatterns[0]);
            Match match_cantidad = Regex.Match(pdfLines[i], _itemsPatterns[1]);
            if (match_sku.Success && !match_cantidad.Success)
            {
                respuesta = true;
            }
            return respuesta;
        }

        private string GetCantidad(string[] pdfLines, int i)
        {
            var ret = "-1";
            var contador = i;
            for(;contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], _itemsPatterns[1]);
                if (match.Success)
                {
                    ret = match.Value.Replace(",00", "").Trim();
                    break;
                }
                else
                {
                    match = Regex.Match(pdfLines[contador-1], _itemsPatterns[1]);
                    ret = match.Value.Replace(",00","").Trim();
                    break;
                }
            }
            
            return ret;
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1";
            var contador = i;
            var str = "";
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], _itemsPatterns[1]);
                if (match.Success)
                {
                    var arr = pdfLines[contador].Split(' ');
                    ret = arr[arr.Length - 3].Replace(",00", "").Replace(".", "").Trim();
                    break;
                }
                else if(Regex.Match(pdfLines[contador -1], _itemsPatterns[1]).Success)
                {
                    
                    var arr = pdfLines[contador-1].Split(' ');
                    ret = arr[arr.Length - 3].Replace(",00","").Replace(".","").Trim();
                    break;
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var split = str.Split(' ');
            var cencos = split[1].Split('-')[0];
            return cencos.Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            //var cencos = split[split.Length - 1].Split('-')[0];
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            var rut = split[split.Length - 1].Split('-')[0];
            return rut.Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        

       


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion
    }
}
