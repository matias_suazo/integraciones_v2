﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Duoc
{
    public class Duoc
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\s\d{1,}\sC/U\s\d{1,}\s\d{1,}$"},
            //{1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s" }
        };
        private readonly Dictionary<int, string> _customerItemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{6}$"},
            {1, @"^S/I$"},
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Nro:";
        private const string ItemsHeaderPattern =
            "Pos. Solicitante";

        private const string CentroCostoPattern = "Entrega:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Duoc(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                CentroCosto = "1",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        OrdenCompra.Rut = GetRut(_pdfLines[++i]);
                        _readOrdenCompra = true;
                    }
                }
                //if (!_readRut)
                //{
                //    if (IsRutPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                //        _readRut = true;
                //    }
                //}

                //if (!_readCentroCosto)
                //{
                //    if (IsCentroCostoPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                //        _readCentroCosto = true;
                //    }
                //}
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {//3312
            var items = new List<Item>();
            var descripciones = new List<string>();
            var A = 0;
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();

               
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                
                /*if (A == 7)
                {

                    Console.WriteLine("==================ITEM CASE 0=====================");
                }*/
                switch (optItem)
                {
                  
                    case 0:

                        //A++;
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var aux2 = pdfLines[i+3].Trim().DeleteContoniousWhiteSpace();
                        var test0 = aux.Split(' ');
                        var test02 = aux2.Split(' ');
                        var sku = GetSku(pdfLines, i);
                        var item0 = new Item
                        {
                            //Sku = "Z446482",
                            Sku = sku,
                            Cantidad = test0[test0.Length - 4].Split(',')[0],
                            Precio = test0[test0.Length - 2].Split(',')[0],
                            Descripcion = test0.ArrayToString(1, test0.Length - 5).ToUpper().Replace("TÉ LIPTON ROYAL 20BOLSAS ETIQUETA NEGRA", "TE ROYAL CEYLAN  CAJA 20UN LIPTON"),
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                       /* descripciones.Add(item0.Descripcion);
                        if (GetCustomerSkuFormat(item0.Sku)==-1)
                        {

                            //Concatenar todo y Buscar por Patrones el SKU DIMERC
                            var concatAll = "";
                            aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                            var breakLoop = false;
                            for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                            {
                                foreach(var skuTemp in aux.Split(' '))
                                {
                                    if (GetCustomerSkuFormat(skuTemp)!=-1)
                                    {
                                        item0.Sku = skuTemp;
                                        breakLoop = true;
                                        break;
                                    }
                                    if (breakLoop) break;
                                }
                                aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                            }
                            //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        }*/
                        items.Add(item0);
                        break;
                }
            }
            /*var rep2 = descripciones.GetPalabrasConsecutivasRepedidas(3);
            foreach (Item t in items)
            {
                t.Descripcion = t.Descripcion.Replace(rep2, "").DeletePlurals().DeleteContoniousWhiteSpace();
            }*/
            //SumarIguales(items);
            return items; 
        }

        private string GetSku(string[] pdfLines,int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\s\d{6}\s");
                if (match.Success)
                {
                    ret = match.Value.Trim();
                    break;
                }
            }
            
            
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            var cencos_cliente = aux[1].Trim().Split('-');
            return cencos_cliente[0];
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            return str;
        }

        private int GetCustomerSkuFormat(string str)
        {
            var ret = -1;
            str = str.Replace(".", "");
            foreach (var it in _customerItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(".", "");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}