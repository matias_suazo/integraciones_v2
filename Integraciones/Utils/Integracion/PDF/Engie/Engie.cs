﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Engie
{
    class Engie
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{8,}\s\d{1,}\s\d{8,}\s"}

        };
        private const string RutPattern = "Rut";
        private const string OrdenCompraPattern = "ORDENDECOMPRA";
        private const string ItemsHeaderPattern = "SOLPED Pos. Cód.ESCL. Detalle Producto";
        private const string ItemsHeaderPattern1 = "U.Med. P.Unit. P.Total";

        private const string CentroCostoPattern = "CENTRO COSTO:";
        private const string ObservacionesPattern = "Tienda :";
        private const string DireccionPattern = "Despachar Direccion Entrega";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readDireccion;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Engie(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                /*if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        var Cenco = GetCentroCosto(_pdfLines[i]).ToUpper();
                        if (_pdfLines[i].Contains("Integramédica"))
                        {
                            Cenco = (Cenco).Replace("INTEGRAMÉDICA ", "").Trim();

                        }
                        else
                            if (_pdfLines[i].Contains("Integramedica"))
                        {
                            Cenco = (Cenco).Replace("INTEGRAMEDICA", "").Trim();

                        }
                        OrdenCompra.CentroCosto = Cenco;
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;

                        if (OrdenCompra.CentroCosto.Contains("PEÑALOLÉN"))
                        {
                            OrdenCompra.CentroCosto = "PEÑALOLEN";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                        }
                        _readCentroCosto = true;
                    }
                }*/
                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var direc = _pdfLines[i-1].Split('/');
                        OrdenCompra.Direccion = direc[1].Replace ("País", "").Replace("Address", "").Trim(); 
                         _readDireccion = true;
                    }
                }

                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            codigos = GetCodigos(pdfLines, i);
            cantidades = GetCantidades(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < codigos.Count; cont++)
            {
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();
            return items;
        }
        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Neto"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"^\d{1,}\s\d{1,}\s\d{1,}\s");
                if (match.Success)
                {
                    listaCodigos.Add(pdfLines[contador].Split(' ')[2]);
                    continue;
                }

                Match match1 = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    listaCodigos.Add(match1.Value);
                }
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Neto"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace().Split(' ');
                Match match = Regex.Match(pdfLines[contador], @"^\d{1,}\s\d{1,}\s\d{1,}\s");
                if (match.Success)
                {
                    listaCantidades.Add(linea[linea.Length -4]);
                    continue;
                }
            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }

        private string GetSku(string str)
        {
            var ret = "Z446482";
            Match match = Regex.Match(str, _itemsPatterns[0]);
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            var cc = aux[1].Split(' ');
            return cc.ArrayToString(1, cc.Length - 1);
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ','-');
            return split[1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1";
            for (var nuevoContador = i; nuevoContador < pdfLines.Length; nuevoContador++)
            {
                if (pdfLines[nuevoContador].Contains("EA"))
                {
                    var aux2 = pdfLines[nuevoContador + 1].DeleteContoniousWhiteSpace();
                    var linea = aux2.Split(' ');
                    ret = linea[0].Replace(",", "");
                    break;
                }

            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern)|| str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern1);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }
        #endregion
    }
}
