﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.VoySantiago
{
    class VoySantiago
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\d{7,}\s\w{1,}"}

        };
        private const string RutPattern = "Purchase Order Print";
        private const string OrdenCompraPattern = "Orden de Compra";
        private const string ItemsHeaderPattern = "Item Código Ped";
        private const string ItemsHeaderPattern2 = "Descripcón P.Unitario Valor";
        private const string CentroCostoPattern = "C.C. :";
        private const string CentroCostoPattern2 = "SEÑOR(ES):";
        private const string ObservacionesPattern = "C.C. :";
        private const string DireccionPattern = "LUGAR DE ENTREGA";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readDireccion;
        private bool _readItem;
        private bool _readObserv;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public VoySantiago(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                //CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i-1]);
                        _readRut = true;
                    }
                }


                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var direc = _pdfLines[i+1];
                        OrdenCompra.Direccion = direc.Trim().TrimEnd().TrimStart().ToUpper();
                        _readDireccion = true;
                    }
                }

                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        var codCenco = _pdfLines[i+1].TrimStart().TrimEnd().ToUpper().Split(' ')[0];//(_pdfLines[i] +" "+ _pdfLines[i+1] + " " + _pdfLines[i+2] + " " + _pdfLines[i+3] + " " + _pdfLines[i + 4] + " " + _pdfLines[i + 5]).ToUpper().Replace("PANAMERICANA", "").Replace("SANTIAGO","").Replace("CHILE", "").Replace("RENCA", "");
                        OrdenCompra.CentroCosto = codCenco;// + " " + _pdfLines[i + 4].Split(':',' ')[2].ToUpper();
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA;
                        if (codCenco.Equals("112300001"))
                        {
                            OrdenCompra.CentroCosto = codCenco + " " + _pdfLines[i].Split(':', ' ')[1].ToUpper();
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                        }
                    }

                }
                if (!_readObserv)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var obs = _pdfLines[i+1].TrimEnd().Trim();
                        OrdenCompra.Observaciones = obs;
                        _readObserv = true;
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            if (OrdenCompra.CentroCosto.Equals(""))
            {
                OrdenCompra.CentroCosto = "0";
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            codigos = GetCodigos(pdfLines, i);
            cantidades = GetCantidades(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < codigos.Count; cont++)
            {
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();
            return items;
        }
        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].ToUpper().Contains("TOTAL PO AMOUNT"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"\s[A-Z]{1}\d{6}\s");
                Match match2 = Regex.Match(pdfLines[contador], @"\s[A-Z]{1}\d{6}");

                if (match.Success || match2.Success)
                {
                    if (listaCodigos.Contains(match.Value) | listaCodigos.Contains(match2.Value))
                    {
                        continue;
                    }
                    if (match.Value.Equals(""))
                    {
                        listaCodigos.Add(match2.Value.TrimStart().TrimEnd());
                    }
                    else
                    {
                        listaCodigos.Add(match.Value.TrimStart().TrimEnd());
                    }

                }

                Match match1 = Regex.Match(pdfLines[contador], @"\s\d{6}\s");
                if (match1.Success)
                {
                    listaCodigos.Add(match1.Value.Split(' ')[1].Replace(",", "").TrimStart().TrimEnd());
                }
                //nuevo formato
                Match match0 = Regex.Match(pdfLines[contador], @"[(][A-Z]{1}\d{6}[)]");
                Match match02 = Regex.Match(pdfLines[contador], @"[(][A-Z]{1}\d{6}[)]");

                if (match0.Success || match02.Success)
                {
                    if (listaCodigos.Contains(match0.Value) | listaCodigos.Contains(match02.Value))
                    {
                        continue;
                    }
                    if (match.Value.Equals(""))
                    {
                        listaCodigos.Add(match02.Value.Replace("(", "").Replace(")", "").TrimStart().TrimEnd());
                    }
                    else
                    {
                        listaCodigos.Add(match0.Value.Replace("(", "").Replace(")", "").TrimStart().TrimEnd());
                    }

                }

                Match match01 = Regex.Match(pdfLines[contador], @"[(]\d{6}[)]");
                if (match01.Success)
                {
                    listaCodigos.Add(match01.Value.Split(' ')[1].Replace(",", "").Replace("(", "").Replace(")", "").TrimStart().TrimEnd());
                }
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL PO AMOUNT"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(linea, @"\s\d{1,},\d{1,}\sUN\s");
                if (match.Success)
                {
                    listaCantidades.Add(match.Value.Trim().Split(',',' ')[0]);
                }
            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL PO AMOUNT"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", " ");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", " "));
                }
            }

            return listaPrecios;
        }

        private string GetSku(string str)
        {
            var ret = "Z446482";
            Match match = Regex.Match(str, _itemsPatterns[0]);
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            var cc = aux[1].Split(' ');
            return cc[1].ToUpper();
        }

        private static string GetCentroCosto2(string str, string str2)
        {
            //string ccFinal= str.ToUpper();
            string cc1 = str.Split(':', '-')[1].Trim();
            //string cc2 = str2.Split(':')[1].TrimEnd();

            string ccFinal = cc1; // + cc2;
            return ccFinal.ToUpper();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split('-');
            ///split = split[1].Split('-');
            return split[0].Trim().DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1";
            for (var nuevoContador = i; nuevoContador < pdfLines.Length; nuevoContador++)
            {
                if (pdfLines[nuevoContador].Contains("EA"))
                {
                    var aux2 = pdfLines[nuevoContador + 1].DeleteContoniousWhiteSpace();
                    var linea = aux2.Split(' ');
                    ret = linea[0].Replace(",", "");
                    break;
                }

            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }
        private bool IsCentroCostoPattern2(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern2);
        }
        #endregion
    }
}
