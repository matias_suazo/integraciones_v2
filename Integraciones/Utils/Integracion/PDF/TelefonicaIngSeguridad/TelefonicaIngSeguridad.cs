﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.TelefonicaIngSeguridad
{
    class TelefonicaIngSeguridad
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA";
        private const string ItemsHeaderPattern =
            "Item Material/Description Cantidad UM Precio Unit. Valor";

        private const string CentroCostoPattern = "de entrega:";
        private const string ObservacionesPattern = "Despacho de productos";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readaAllQty;
        private bool _readAllItems;
        private bool _readAllPrices;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;
        List<string> listAllQty = new List<string>();
        List<string> listAllItems = new List<string>();
        List<string> listAllPrice = new List<string>();

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public TelefonicaIngSeguridad(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
           
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "59083900",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+2]);
                        _readOrdenCompra = true;
                    }
                }

                if (!_readaAllQty)
                {
                    if (_pdfLines[i].Contains("ESPECIFICAR EQUIPOS Y MATERIALES"))
                    {
                        listAllQty = GetCantidades(_pdfLines,i);
                        _readaAllQty = true;
                    }
                }
                if (!_readAllItems)
                {
                    if (_pdfLines[i].Contains("ESPECIFICAR EQUIPOS Y MATERIALES"))
                    {
                        listAllItems = GetSkus(_pdfLines, i);
                        _readAllItems = true;
                    }
                }
                if (!_readAllPrices)
                {
                    if (_pdfLines[i].Contains("ESPECIFICAR EQUIPOS Y MATERIALES"))
                    {
                        listAllPrice = GetPrecios(_pdfLines, i);
                        _readAllPrices = true;
                    }
                }
               
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones +=
                            $"{_pdfLines[i+1].Trim().DeleteContoniousWhiteSpace()}, " +
                            $"{_pdfLines[i+2].Trim().DeleteContoniousWhiteSpace()}";
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readItem)
                {
                    if (_readAllPrices == true && _readaAllQty == true && _readAllItems == true)
                    {
                        var items = GetItems();
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items = items;
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems()
        {
            var items = new List<Item>();
            for(var i = 0; i < listAllItems.Count; i++)
            {
                var item = new Item
                {
                    Sku = listAllItems[i],
                    Cantidad = listAllQty[i],
                    Precio = listAllPrice[i],
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                };
                items.Add(item);
            }
            
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }

        private static List<string> GetCantidades(string[] pdfLines,int i) {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for(;contador<pdfLines.Length;contador++)
            {
                Match match = Regex.Match(pdfLines[contador],@"^\d{1,}"); // si contiene numero ingresa
                if (match.Success)
                {
                    var linea = pdfLines[contador];
                    var aux = linea.Split(' ');
                    var cantidad = aux[0].Split(',')[0];
                    if (Regex.Match(cantidad, @"[A-Z]{1,}").Success)
                    {
                        continue;
                    }
                    listaCantidades.Add(cantidad);
                }
            }
            
            return listaCantidades;
        }

        private static List<string> GetSkus(string[] pdfLines, int i)
        {
            List<string> listaSkus = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\[[A-Z]{1,2}\d{5,6}\]"); // si contiene numero ingresa
                if (match.Success)
                {
                    var sku = match.Value.Trim().Replace("[", "").Replace("]", "");
                    listaSkus.Add(sku);
                }
            }

            return listaSkus;
        }

        private static List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("SUBTOTAL"))
                {
                    break;
                }
                else { 
                Match match = Regex.Match(pdfLines[contador], @"\$\s\d"); // si contiene numero ingresa
                if (match.Success)
                {

                    var aux = pdfLines[contador].Split(' ');
                    var precio = aux[aux.Length-3].Replace(".","");
                    listaPrecios.Add(precio);
                }
                }
            }

            return listaPrecios;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            return str;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
    }
}
#endregion