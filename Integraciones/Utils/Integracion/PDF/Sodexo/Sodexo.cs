﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.Sodexo
{
    class Sodexo
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[aA-zZ]{1}\d{5,6}"},
            {1, @"VARIOS" },
            {2, @"^\d{7,}\s\w{1}" }

        };
        private const string RutPattern = "RAZÓN SOCIAL :";
        private const string RutPattern2 = "RUT:";
        private const string OrdenCompraPattern = "OC:";
        private const string ItemsHeaderPattern = "Descripción Cantidad Precio Unit. Moneda Valor Total";
        private const string ItemsHeaderPattern2 = "Código Fecha de";

        private const string CentroCostoPattern = "Giro :";
        private const string CentroCostoPattern2 = "Centro de costo";
        private const string ObservacionesPattern = "Nota :";
        private const string DireccionPattern = "Dirección :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Sodexo(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i-1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        if (Regex.Match(_pdfLines[i], RutPattern2).Success)
                        {

                            OrdenCompra.Rut = GetRut2(_pdfLines[i + 1]);
                            _readRut = true;
                        }

                        else
                        {

                            OrdenCompra.Rut = GetRut(_pdfLines[i]);
                            _readRut = true;

                        }
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        if (Regex.Match(_pdfLines[i], CentroCostoPattern2).Success)
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto2(_pdfLines[i+1]);
                            _readCentroCosto = true;
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_HDI;
                        }
                        else
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                            _readCentroCosto = true;
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_HDI;

                        }
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones +=
                            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDirectionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = getDireccion(_pdfLines[i]);
                        _readDireccion = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var cantidad = GetCantidad(_pdfLines[i - 3]);
                        var precio = GetPrecio(_pdfLines[i - 3]);

                        var item0 = new Item
                        {
                            Sku = GetSku(test0),
                            Cantidad = cantidad,
                            Precio = precio,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        if (!item0.Sku.Contains(@"[aA-zZ]{1}\d{6}"))
                        {
                            continue;
                        }
                        if(item0.Cantidad.Contains(@"\d{6}"))
                        {
                            continue;
                        }
                        
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var cantidad1 = GetCantidad(_pdfLines[i - 3]);
                        var precio1 = GetPrecio(_pdfLines[i - 3]);

                        var item1 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = cantidad1,
                            Precio = precio1,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        if (item1.Cantidad.Contains(@"\d{6}"))
                        {
                            continue;
                        }
                        items.Add(item1);
                        break;
                    case 2:
                        Console.WriteLine($"==================ITEM CASE 2=====================");
                        var test2 = aux.Split(' ');
                        var skuCont = _pdfLines[i + 3].Split(' ');
                        var cantidad2 = GetCantidad2(_pdfLines[i + 2]);
                        if (skuCont.Count() != 1)
                        {
                            skuCont = _pdfLines[i + 2].Split(' ');
                        }
                        var precio2 = "1";

                        var item2 = new Item
                        {
                            Sku = test2[0],
                            Cantidad = test2[1].Split(',')[0],
                            Precio = precio2,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success)
                        {
                            item2.Cantidad = GetCantidad2(_pdfLines[i + 2]);
                        }
                        if(item2.Sku.Equals(item2.Cantidad))
                        {
                            continue;
                        }
                        if (item2.Cantidad.Contains(@"\d{6}"))
                        {
                            continue;
                        }
                        items.Add(item2);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(',');
            return aux[aux.Length - 1].Trim();
        }
        private static string GetCentroCosto2(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 1].Trim();
        }

        private static string getDireccion(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':', '-');
            return split[1].Trim().DeleteDotComa();
        }
        private static string GetRut2(string str)
        {
            var split = str.Split(':', '-');
            return split[0].Trim().DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private static string GetCantidad(string str)
        {
            if (!str.Contains(@"\d{1,}"))
            {
                return "0";
            }
            var split = str.Split(' ', ',');
            return split[split.Length - 4].Trim();
        }
        private static string GetCantidad2(string str)
        {
            var split = str.Split(' ', ',');
            if (split.Count() == 1)
            {
                return split[0].Trim();
            }else
            return split[split.Length - 2].Trim();
        }
        private static string GetPrecio(string str)
        {
            var split = str.Split(' ');
            return split[0].Trim().DeleteDotComa();
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern) ||
                str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern2);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern2);
        }
        private bool IsDirectionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}
