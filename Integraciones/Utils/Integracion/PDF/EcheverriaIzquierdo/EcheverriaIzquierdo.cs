﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.EcheverriaIzquierdo
{
    class EcheverriaIzquierdo
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            //{0, @"[A-Z]{1,2}[0-9]{5,6}"},
            //{1, @"\s[A-Z]{4}\d{4}\s" },
            {1,@"(\d{1,}\,00\s\d(\.\d{1,}\,00|\d{1,}\,00)\s\d{1,}(\d{1,}\,00|\.\d{1,}\,00)|\d{1,}\,00)" }
            //{2, @"\d{1,}\,00" } // cuando no tienen codigo dimerc
        };
        private const string RutPattern = "Rut:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA";
        private const string ItemsHeaderPattern =
            "Cantidad Unidad Código";

        private const string CentroCostoPattern = "Centro de Gestión";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public EcheverriaIzquierdo(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[++i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i+1]);
                        _readCentroCosto = true;
                    }
                }
                
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                Console.WriteLine(aux);
                if (aux.Contains("Notas:") || aux.Contains("Neto $") || aux.Contains("SubTotal $"))
                {
                    break;
                }
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                
                
                switch (optItem)
                {
                   case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        //aux = pdfLines[i].DeleteContoniousWhiteSpace();
                        var test0 = aux.Split(' ');
                        //var sku = test0[test0.Length-1].Trim();
                        var sku = GetSku(pdfLines, i);
                        aux = pdfLines[i-2].DeleteContoniousWhiteSpace();
                        test0 = aux.Split(' ');
                        var cantidad = test0[0].Split(',')[0].Trim();
                        var precio = test0[1].Trim();
                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = test0[0].Split(',')[0],
                            Precio = test0[test0.Length - 2].Split(',')[0],
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                       
                        items.Add(item0);
                        break;

                    case 1:
                        var x = i + 1;
                        if(pdfLines[x].Contains("Notas:") || pdfLines[x].Contains("Neto $") || pdfLines[x].Contains("SubTotal $"))
                        {
                            break;
                        }
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        //aux = pdfLines[i].DeleteContoniousWhiteSpace();
                         var test1 = aux.Split(' ');
                         var sku1 = GetSku(pdfLines, i);
                        aux = pdfLines[i - 1].DeleteContoniousWhiteSpace();
                        test0 = aux.Split(' ');
                        //var cantidad = test0[0].Split(',')[0].Trim();
                        var cantidad1 = GetCantidad(pdfLines, i);
                        var precio1 = GetPrecio(pdfLines, i);
                        //var precio = test0[1].Replace(",00","").Replace(".","").Trim();
                        
                        var item1 = new Item
                        {
                            Sku = sku1,
                            Cantidad = cantidad1,
                            Precio = precio1,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                        };
                        if (esSkudimerc(item1.Sku))
                        {
                            item1.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }

                        items.Add(item1);
                        break;

                    case 2:
                        Console.WriteLine("=======ITEM CASE 2=============");
                        test0 = aux.Split(' ');
                         sku = "Z446482";
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        /*private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }*/

        private Boolean esSkudimerc(string sku)
        {
            return Regex.Match(sku, @"[A-Z]{1,}\d{5,6}").Success;
        }
        private  string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var contador = i;
            // sku dimerc
            bool skudimerc = false;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,}\d{5,6}\s");
                if (match.Success)
                {
                    ret = match.Value.Trim();
                    skudimerc = true;
                    break;
                }
            }
            //sku cliente
            if (!skudimerc)
            {
                for (contador = i; contador < pdfLines.Length; contador++)
                {
                    Match match = Regex.Match(pdfLines[contador], @"\s[A-Z]{4}\d{4}\s");
                    if (match.Success)
                    {
                        ret = match.Value.Trim();
                        break;
                    }

                }
            }

            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var ret = "0";
            var aux = str.Split(',');
                var buscarcc = aux[aux.Length - 1].Split(' ');
            int i = 0;
            for (;i < buscarcc.Length; i++){
                if (Regex.Match(buscarcc[i], @"\d{4}").Success){
                    ret = buscarcc[i];


                    break;
                }
            }

            return ret;
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
                return ret;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i )
        {
            var ret = "-1";
            var contador = i;
            var valor = "";
            var aux = "";
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], _itemsPatterns[1]);
                if (match.Success)
                {
                    var arr = pdfLines[contador].Split(' ');
                    aux = arr[arr.Length - 2].Replace(",00", "").Replace(".","").Trim();
                    // ret = valor.Split(' ')[2].Trim().Split(',')[0].Replace(".","");
                    ret = aux;
                    break;
                }

            }
            return ret;
        }

        private string GetCantidad(string[] pdfLines,int i)
        {
            var ret = "-1";
            var contador = i;
            var valor = "";
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], _itemsPatterns[1]);
                if (match.Success)
                {
                    valor = match.Value;
                    ret = valor.Split(' ')[0].Trim().Split(',')[0];
                    break;
                }

            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion
    }
}
