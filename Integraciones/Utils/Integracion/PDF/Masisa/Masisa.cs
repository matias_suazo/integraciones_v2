﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion
{
    public class Masisa
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s[a-zA-Z]{1,}\s\d{1,}CLP"},
            {1,@"\s\d{1,}\s[a-zA-Z]{2}\s\d{1,}\sCLP\s/\s\d{1,}\s[a-zA-Z]{2,}$" },
            {2,@"^\d{5}\s[A-Z]{1,}|\d{5}\s\d{7}" }
            //{2,@"\d{5}\s\d{7}" }
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Número :";
        private const string ItemsHeaderPattern =
            "Valor neto";

        private const string CentroCostoPattern = "P/O :";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem = true;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Masisa(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                        _readItem = false;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i+1]);
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;
                        OrdenCompra.Direccion = _pdfLines[i+1];
                        if(OrdenCompra.CentroCosto.Contains("PLANTA CABRERO MADERAS"))
                        {
                            OrdenCompra.CentroCosto = "1";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                            break;
                        }
                        if (OrdenCompra.CentroCosto.Contains("MDF CABRERO"))
                        {
                            OrdenCompra.CentroCosto = "2";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                            break;
                        }
                        if (OrdenCompra.CentroCosto.Contains("PLANTA CABRERO"))
                        {
                            OrdenCompra.CentroCosto = "3";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                            break;
                        }
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace().Replace(".", "");
                if (aux.Contains("Sin otro particular"))
                {
                    break;
                }
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test0[test0.Length - 5].Trim(),
                            Precio = test0[test0.Length - 3].Replace("CLP/", "").Trim(),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        var concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        var sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku))
                        {
                            item0.Sku = sku;
                            item0.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var item1 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test1[test1.Length - 7].Trim(),
                            Precio = test1[test1.Length - 5].Replace(".", "").Trim(),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace().Replace(".", "");
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace().Replace(".", "");
                        }
                        item1.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku))
                        {
                            item1.Sku = sku;
                            item1.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        items.Add(item1);
                        break;

                    case 2:
                        Console.WriteLine($"==================ITEM CASE 2=====================");
                        var test2 = aux.Split(' ');
                        var sku2 = GetSku2(pdfLines, i);
                        var cantidad2 = test2[test2.Length - 5].Trim();
                        if (cantidad2.Equals("UN") || cantidad2.Equals("CA") || cantidad2.Equals("PAQ"))
                        {
                            cantidad2 = test2[test2.Length - 7].Trim();
                        }
                        var precio2 = test2[test2.Length - 3].Replace(".", "").Trim().Replace("CLP/","");
                        if (precio2.Equals("CLP"))
                        {
                            precio2 = test2[test2.Length - 5].Replace(".", "").Trim();
                        }
                        if (precio2.Equals("/"))
                        {
                            precio2 = test2[test2.Length - 5].Replace(".", "").Trim();
                        }
                        if (precio2.Equals("PAQ"))
                        {
                            precio2 = test2[test2.Length - 4].Replace(".", "").Trim();
                            cantidad2 = test2[test2.Length - 6].Trim();
                        }
                        if (precio2.Equals(cantidad2))
                        {
                            cantidad2 = test2[test2.Length - 7].Trim(); 
                        }
                        if (cantidad2.Equals("CLP"))
                        {
                            cantidad2 = test2[test2.Length - 8].Trim();
                            precio2 = test2[test2.Length - 6].Replace(".", "").Trim();
                        }
                        var item2 = new Item
                        {
                            Sku = sku2,
                            Cantidad = cantidad2,
                            Precio = precio2,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace().Replace(".", "");
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace().Replace(".", "");
                        }
                        item2.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        if (OracleDataAccess.ExistProduct(sku))
                        {
                            item2.Sku = sku;
                            item2.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                        }
                        if (item2.Sku.Equals("Z446482"))
                        {
                            var aux2 = _pdfLines[i+1].Split(' ');
                            item2.Sku = aux2[aux2.Length - 1];
                        }
                        items.Add(item2);
                        break;

                    


                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku2(string[] pdfLines, int i) {
            var ret = "Z446482";
            var contador = i;
            for (;contador<pdfLines.Length;contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value.Trim();
                    break;
                }
            }
            return ret;
        }
        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(',');
            return aux[0].Trim().ToUpper();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim().Replace(":","");
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}