﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.COMPASS
{
    class Compass
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{2,3}\s\d{5,}\s\w{1}"},
            //{1, @"[aA-zZ]{1}\d{5,}" }
        };

        private const string RutPattern = "R.U.T.:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA NO:";
        private const string ItemsHeaderPattern = "POS Cod.Mat./Servicio";

        private const string CentroCostoPattern = "CENTRO DE COSTO:";
        private const string ObservacionesPattern = "CONTACTO EN ENTREGA:";
        private const string DireccionPattern = "EMAIL CONTACTO";
        private const string SkuPattern = @"[aA-zZ]{1}\d{5,6}";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Compass(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };

            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]) && !_pdfLines[i].Contains("96670840"))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;

                    }
                }

                /*Por solicitud enviar al CC.0
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i - 2]);
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                        _readCentroCosto = true;

                    }
                }*/
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var obs = _pdfLines[i].Split(':');
                        OrdenCompra.Observaciones = "Usuario: "+ obs[obs.Length - 1] ;
                        _readObs = true;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDirectionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = GetDireccion(_pdfLines[i + 1]);
                        _readDireccion = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.Observaciones.Equals(""))
            {
                OrdenCompra.Observaciones = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var split = aux.Split(' ');
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var sku = split[1];
                        var cantidad = GetCantidad(_pdfLines[i]).Trim();
                        var precio = split[split.Length - 4].Replace(",00", "");
                        //if (!cantidad.Contains(@"\d{1,}"))
                        //{
                        //    cantidad = GetCantidad(_pdfLines[i]);
                        //}
                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = cantidad.Replace(",000", ""),
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var sku1 = GetSku(_pdfLines[i]);
                        var cantidad1 = GetCantidad(_pdfLines[i]);
                        var precio1 = GetPrecio(_pdfLines[i]);
                        var item1 = new Item
                        {
                            Sku = sku1,
                            Cantidad = cantidad1,
                            Precio = precio1,
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        items.Add(item1);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        //private string GetSku(string[] test1)
        //{
        //    var ret = "Z446482";
        //    var skuDefaultPosition = test1[0].Replace("#", "");
        //    if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
        //    {
        //        var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
        //        var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
        //        ret = skuDefaultPosition.Substring(index, length).Trim();
        //    }
        //    else
        //    {
        //        var str = test1.ArrayToString(0, test1.Length - 1);
        //        if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
        //        {
        //            var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
        //            var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
        //            ret = str.Substring(index, length).Trim();
        //        }
        //        else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
        //        {
        //            var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
        //            var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
        //            ret = str.Substring(index, length).Trim();
        //        }
        //    }
        //    return ret;
        //}


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            return aux[0].Trim();
        }

        private static string GetDireccion(string str)
        {
            var aux = str.Split(' ');
            return aux.ArrayToString(0, aux.Length-2);
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split('-', ':');
            return split[1].Trim().DeleteDotComa().Trim();
        }
        private static string GetSku(string str)
        {
            var ret = "Z446482";
            if (Regex.Match(str, SkuPattern).Success)
            {
                ret = Regex.Match(str, SkuPattern).Value;
            }
            if (!Regex.Match(str, SkuPattern).Success)
            {
                var split = str.Split(' ');
                ret = split[1];
            }
            return ret;
        }


        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            if (Regex.Match(str, SkuPattern).Success & ret == -1)
            {
                ret = 1;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private static string GetCantidad(string str)
        {
            var split = str.Split(' ');
            var splitCant = split[split.Length - 7].Trim();
            if (!Regex.Match(splitCant, @"^\d{1,}").Success)
            {
                var split1 = split[split.Length - 6].Trim();
                if (Regex.Match(split1, @"\d{1,}[xX]\d{1,}").Success)
                {
                    return split[split.Length - 5].Trim();
                }
                return split[split.Length - 6].Trim();
            }
            
            return splitCant;
        }


        private static string GetPrecio(string str)
        {
            var split = str.Split(' ');
            var splitPrecio = split[split.Length - 7].Trim().DeleteDotComa();
            if (!Regex.Match(splitPrecio, @"^\d{1,}").Success)
            {
                return split[split.Length - 6].Trim().DeleteDotComa();
            }
            return splitPrecio;
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsDirectionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }
        private bool IsSkuPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(SkuPattern);
        }

        #endregion
    }
}
