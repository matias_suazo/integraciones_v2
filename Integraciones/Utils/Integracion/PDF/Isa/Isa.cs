﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion
{
    public class Isa
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[0-9]{1,3}(?:\.?[0-9]{3})*(?:\,[0-9]{2})"}, //2,00 TECLADO 19.359,00 38.718
            //{0, @"\d{3}[.-]\d{3}\s\w{2,}\s(.*)\s\d{4}\s(.*)$"},
            {1, @"\d{1,},\d{2}\s\d{1,}[.]\d{1,}\s[aA-zZ]{1,}" }
        };

        private const string ItemsPattern = @"[aA-zZ]{1}\d{5,6}";
        private const string RutPattern = "R.U.T.:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA N°:";
        private const string ItemsHeaderPattern = "Unidad Cantidad Descripción C.C. Precio Unitario Valor Total";

        private const string CentroCostoPattern = "Obra:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Isa(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i + 1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i - 1]).ToUpper();
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            var currentPdfLine = i;
            for (; i < pdfLines.Length; i++)
            {

                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var optItem = GetFormatItemsPattern(aux.Replace(".", ""));
                switch (optItem)
                {
                    case 1:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            //Sku = test0[1].Trim().Split(' ')[0],//"Z446482",
                            Sku = GetSku(test0, i),
                            Cantidad = GetCantidad(aux),
                            Precio = test0[test0.Length - 2].Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        items.Add(item0);
                        break;
                        /*
                        var match = Regex.Matches(pdfLines[i], ItemsPattern);

                if (match.Count == 2)
                {
                    sku = GetSku(pdfLines, i);
                    var item = new Item
                    {
                        Sku = sku,         
                        Cantidad = match[0].Value.Split(',')[0],
                        Precio = match[1].Value.Replace(".", "").Split(',')[0],
                        Descripcion = "",
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                    };
                    items.Add(item);
                }*/
                }


                /* //SKU's search
     =======
                 //SKU's search
     >>>>>>> d8a0ca1f338efd0b5b57828a3895e4f29bf0b25e
                 var currentItemIndex = 0;
                 var pdfLinesTotal = pdfLines.Count();
                 var itemsTotal = items.Count;
                 if (itemsTotal > 0)
                 {
                     while (currentItemIndex < itemsTotal || pdfLinesTotal < currentPdfLine)
                     {
                         var matches = Regex.Matches(pdfLines[currentPdfLine++], @"CODIGO|[a-zA-Z]{1,2}\d{5,6}");
                         //Both goals in the same row
                         if (matches.Count == 2)
                         {
                             items[currentItemIndex++].Sku = matches[1].Value;
                         }
                         //SKU in next row?
                         if (matches.Count == 1)
                         {
                             var match = Regex.Match(pdfLines[currentPdfLine++], @"[a-zA-Z]{1,2}\d{5,6}");
                             if (match.Success)
                             {
                                 items[currentItemIndex++].Sku = match.Value;
                             }
                         }
                     }
                 }*/
                //SumarIguales(items);
                //Console.WriteLine(items);
            }
                return items;
            
        }

        private string GetSku(string[] pdfLines,int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (;contador<pdfLines.Length;contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value.Trim().ToUpper();
                    break;
                }

            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {

            return str.Trim();
        }

        private static string GetCantidad(string str)
        {

            return str.Trim();
        }

        private static string GetPrecio(string str)
        {

            return str.Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            return str.Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ','-');
            return split[split.Length - 2].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}