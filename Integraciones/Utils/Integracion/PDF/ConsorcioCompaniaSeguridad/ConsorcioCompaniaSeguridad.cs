﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.ConsorcioCompaniaSeguridad
{
    public class ConsorcioCompaniaSeguridad
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}-\s\d{1,}\s[a-zA-Z]{1,2}\d{5,6}\s" }, //^\d{1,}-\s\d{1,}\s[a-zA-Z]{3}\d{4,6}\s
            {1, @"^\d{1,}-\s\d{1,}\s[a-zA-Z]{3}\d{4,6}\s|^\d\-\s\d\s[a-zA-Z]{7}\d{3}" }
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Pedido Proveedor";
        private const string ItemsHeaderPattern =
            "Lín-Env Art/Descripción Cantidad";

        private const string CentroCostoPattern = "Centro de Costo";
        private const string ObservacionesPattern = "Nota Interna:";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private global::Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        public ConsorcioCompaniaSeguridad(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Get
        public global::Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new global::Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA,
                CentroCosto = "0"
                
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[++i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                //if (!_readCentroCosto)
                //{
                //    if (IsCentroCostoPattern(_pdfLines[i]))
                //    {
                //        Console.WriteLine($"CENTRO COSTO: {_pdfLines[i]}");
                //        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                //        _readCentroCosto = true;
                //    }
                //}
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones += $"{_pdfLines[i].Split(':')[1].DeleteContoniousWhiteSpace()}";
                        _readObs = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length -1; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var aux1 = pdfLines[i+1].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("============================CASE 0============================");
                        var test0 = aux.Split(' ');
                        var test01 = pdfLines[++i].Trim().DeleteContoniousWhiteSpace().Split(' ');
                        if(test01.Length < 5)
                            test01 = pdfLines[++i].Trim().DeleteContoniousWhiteSpace().Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[2],
                            Cantidad = test01[test01.Length - 5].Replace(",", ""),
                            Precio = test01[test01.Length - 3].Replace(",",""),
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine("============================CASE 1============================");
                        var test1 = aux.Split(' ');
                        var test2 = aux1.Split(' ');
                        var x = 0;
                        var item1 = new Item
                        {
                            Sku = test1[2],
                            Cantidad = test1[test1.Length - 4].Replace(",", ""),
                            Precio = test1[test1.Length - 3].Replace(",", ""),
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        if (!int.TryParse(item1.Cantidad,out x))
                        {
                            item1.Cantidad = test1[test1.Length - 5].Replace(",", "");
                        }
                        if (!int.TryParse(item1.Cantidad, out x))
                        {
                            item1.Cantidad = test2[test2.Length - 5].Replace(",", "");
                        }
                        if (!int.TryParse(item1.Precio, out x))
                        {
                            item1.Precio = test2[test2.Length - 3].Replace(",", "");
                        }
                        if (item1.Precio.Equals(item1.Sku))
                        {
                            item1.Cantidad = test2[test2.Length - 5].Replace(",", "");
                            item1.Precio = test2[test2.Length - 3].Replace(",", "");
                        }
                        items.Add(item1);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[5].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
                ret = skuDefaultPosition;
            else
            {
                var str = test1.ArrayToString(0, test1.Length -1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var split = str.Split(' ');
            return split[split.Length -1 ].Trim().Replace(".","");
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 3];
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            return str.Split(' ')[1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }

            Console.WriteLine($"Linea: {str}  \n retorna: {ret}");
            return ret;
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern)
                || str.Trim().DeleteContoniousWhiteSpace().Contains("Centro Costo")
                || (str.Trim().ToUpper().DeleteContoniousWhiteSpace().Contains("CENTRO")
                && str.Trim().ToUpper().DeleteContoniousWhiteSpace().Contains("COSTO"));
        }

        #endregion

    }
}