﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.Langues_Affairs_SpA
{
    class LanguesAffairsSpa2
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^[A-Z]{1,2}\d{5,6}" },
            {1,@"[A-Z]{1,2}\d{5,6}$" },
            {2,@"^\d{1,}\s" }
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA N";
        private const string ItemsHeaderPattern =
            "CODIGO DESCRIPCION DEL PRODUCTO CANTIDAD UNITARIO TOTAL";
        private const string ItemsHeaderPattern2 = "CODIGO DESCRIPCION CANTIDAD UNITARIO TOTAL";

        private const string CentroCostoPattern = "ENTREGAR A";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public LanguesAffairsSpa2(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]).Trim();
                        if (OrdenCompra.CentroCosto.Equals(""))
                        {
                            OrdenCompra.CentroCosto = "0";
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                        }
                        _readCentroCosto = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones = "";
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            bool exist = false;
           /* var codigos = GetSkus(pdfLines, i);
            var cantidades = GetCantidades(pdfLines, i);
            var precios = GetPrecios(pdfLines, i, codigos.Count);
            for (int x = 0; x < codigos.Count; x++)
            {
                var item0 = new Item
                {
                    Sku = codigos[x],
                    Cantidad = cantidades[x],
                    Precio = precios[x],
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                };
                items.Add(item0);

            }
            */
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                if (pdfLines[i].Equals("0") || pdfLines[i].Contains("HORAS"))
                {
                    break;
                }
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                aux = aux.Replace(" ,", ",");
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                //List<string> codigosAgregados = new List<string>();

                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var cantidad = test0[test0.Length - 3].Split(',')[0];
                        if (!IsNumeric(cantidad)){
                            cantidad = test0[test0.Length - 2].Split(',')[0];
                        }
                        var item0 = new Item
                        {
                            Sku = test0[0],
                            Cantidad = cantidad,
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        //items es la lista... e item0 es el objeto que quiero ver si existe en la lista.
                        exist = items.Any(item => item.Sku == item0.Sku.ToString());
                        if (!exist)
                        {
                            items.Add(item0);
                        }
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var cont = i - 1;
                        var aux1 = pdfLines[cont].DeleteContoniousWhiteSpace();
                        var test1 = aux1.Split(' ');
                        var item1 = new Item
                        {
                            Sku = GetSku(pdfLines, cont),
                            Cantidad = test1[test1.Length - 2].Split(',')[0],
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        exist = items.Any(item => item.Sku == item1.Sku.ToString());
                        if (!exist)
                        {
                            items.Add(item1);
                        }
                        break;
                    case 2:
                        Console.WriteLine($"==================ITEM CASE 2=====================");

                        var test2 = aux.Split(' ');
                        var item2 = new Item
                        {
                            Sku = GetSku(pdfLines, i),
                            Cantidad = test2[0].Trim(),
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };

                        exist = items.Any(item => item.Sku == item2.Sku.ToString());
                        if (!exist)
                        {
                            items.Add(item2);
                        }
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }
        private bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        private List<string> GetPrecios(string[] pdfLines, int i, int cantCodigos)
        {
            List<string> precios = new List<string>();
            var cont = i;
            for (; cont < pdfLines.Length; cont++)
            {
                Console.WriteLine(pdfLines[cont].DeleteContoniousWhiteSpace().Replace(" ,", ","));
                Match match = Regex.Match(pdfLines[cont].DeleteContoniousWhiteSpace().Replace(" ,", ","), @"([A-Z]{1}\s(\d{1,}\s\d{1,}\,\d{3}|\d{1,}\s\d{1,})|\.\s(\d{1,}\s\d{1,}\,\d{1,}|\d{1,}\s\d{1,}))");
                if (match.Success)
                {
                    precios.Add(match.Value.Split(' ')[2].Replace(",", ""));
                }
                else
                {
                    Match match2 = Regex.Match(pdfLines[cont].DeleteContoniousWhiteSpace().Replace(" ,", ","), @"(\$\s\d{1,}\,\d{1,}\s\$|\$\s\d{1,}\s\$)");
                    if (match2.Success)
                    {
                        precios.Add(match2.Value.Split(' ')[1].Replace(",", ""));
                        continue;

                    }
                    if (pdfLines[cont].Contains("$"))
                    {
                        bool primeravuelta = false;
                        var p = "";
                        var lin = pdfLines[cont].Split(' ');
                        foreach (var item in lin)
                        {
                            if (item.Equals("$") && primeravuelta == false)
                            {
                                primeravuelta = true;
                                continue;
                            }
                            if (primeravuelta)
                            {
                                p = p + item;
                                if (item.Equals("$"))
                                {
                                    p = p.Trim().Replace("$", "").Replace(" ", "");
                                    precios.Add(p);
                                    Console.WriteLine(precios.ToString());
                                    break;
                                }
                            }
                        }
                    }
                }
                if (precios.Count == cantCodigos)
                {
                    break;
                }
            }
            return precios;
        }

        private List<string> GetSkus(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            List<string> codigos = new List<string>();
            var cont = i;

            for (; cont < pdfLines.Length; cont++)
            {
                Match match = Regex.Match(pdfLines[cont].DeleteContoniousWhiteSpace(), @"^[A-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value;
                    codigos.Add(ret);
                }
            }

            return codigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            var cont = i;
            List<string> cantidades = new List<string>();
            for (; cont < pdfLines.Length; cont++)
            {
                var aux = pdfLines[cont].DeleteContoniousWhiteSpace().Replace(" ,", ",");
                //Match match = Regex.Match(aux, @"([A-Z]{1,}\s\d{1,}\s\d|\.\s\d{1,}\s\d)");
                Match match = Regex.Match(aux, @"([A-Z]{1,}\s\d{1,}\s\$|\.\s\d{1,}\s\d)");
                var valor = "";
                if (match.Success)
                {
                    if (match.Value.Contains("$"))
                    {
                        valor = match.Value.Split(' ')[1];
                    }
                    else
                    {
                        var xxx = match.Value.Split(' ');
                        valor = xxx[xxx.Length - 1];
                    }

                    cantidades.Add(valor);

                }
                else
                {
                    Match match2 = Regex.Match(aux, @"(\.\s\d{1,}\s\$|[A-Z]{1,}\s\d{1,}\s\$|\d{1,}\s\d{1,}\s\$)");
                    if (match2.Success)
                    {
                        var valor2 = match2.Value.Split(' ')[1];
                        cantidades.Add(valor2);

                    }
                    Match match3 = Regex.Match(aux, @"(\s\s\d{1,}\s\$)");
                    if (match3.Success)
                    {
                        var valor3 = match3.Value.Trim().Split(' ')[0];
                        cantidades.Add(valor3);
                    }
                }
            }
            return cantidades;
        }

        private string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var cont = i;

            for (; cont < pdfLines.Length; cont++)
            {
                Match match = Regex.Match(pdfLines[cont], @"[A-Z]{1,2}\d{5,6}$");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }

            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            var obs = "";
            for (int i = 0; i < aux.Length; i++)
            {

                if (aux[i].Contains("VALOR"))
                {
                    break;
                }
                obs += $" {aux[i].ToUpper()}";
            }
            return obs.Trim().Replace("SEDE", "").Replace("ENTREGAR A", "");
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim().Replace(".", "").Split('-')[0];
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return (str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2));
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion
    }
}
