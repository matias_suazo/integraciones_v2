﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.AIEP
{
    public class Aiep
    {

        private bool _readOrdenCompra;
        private bool _readRut = true;
        private readonly PDFReader _pdfReader;
        private bool _readObs;
        private bool _readItems = true;
        private readonly string[] _pdfLines;
        private global::Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        public Aiep(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }


        private List<Item> GetItems(string[] pdfLines, int firstIndex)
        {
            var items = new List<Item>();
            var lastSku = "Z446482";
            for (; firstIndex < pdfLines.Length; firstIndex++)
            {
                var str = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace();
                //ricardo
                var i = 0;
                var patron = @"\d{1}\s\-\s[A-Z]{1,}\d{5,6}";
                    var descripcion = "";
                if (Regex.Match(pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace(), @"UN\s\d{1,}(\.|\,)").Success || Regex.Match(pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace(), @"EA\s\d{1,}(\.|\,)").Success)
                {
                    var conteo = items.Count;
                    var s = GetSku(pdfLines, firstIndex);
                    var c = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                    //19 11 2018

                    if (s.Equals("Z446482"))
                    {
                        descripcion = pdfLines[firstIndex + 1].Trim().DeleteContoniousWhiteSpace();
                        Console.WriteLine("no sku");
                    }
                    var item = new Item
                    {
                        Sku = s,
                        Cantidad = c[c.Length - 5].Trim().Split('.')[0].Replace(",", "."),
                        Precio = c[c.Length - 3].Trim().Split('.')[0].Replace(",", ""),
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    //19 11 2018
                    if (Regex.Match(s, @"\d{2,3}\w[GR]|\d{2,3}\s\w[GRS].|[X]\d{2,3}|\d{2,3}\w[CM]").Success)

                    {
                        item.Sku = "Z446482";
                        item.Descripcion = s;
                        item.Cantidad = c[c.Length - 5].Trim().Split('.')[0].Replace(",", ".");
                        item.Precio = c[c.Length - 3].Trim().Split('.')[0].Replace(",", "");
                        item.TipoPareoProducto = TipoPareoProducto.PAREO_DESCRIPCION_TELEMARKETING;
                        item.TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING;
                    }



                    if (item != null)
                    {
                        items.Add(item);

                        if (conteo != 0 && items[conteo].Sku == items[conteo - 1].Sku)
                        {
                            items.RemoveAt(conteo);
                            continue;
                        }
                        continue;

                    }
                }
                    if (Regex.Match(str, @"\s\w{1}\d{6}$").Success)
                    {
                        lastSku = GetSku(str);
                    }
                    if (Regex.Match(str, @"^\d{1,}\s[-]\s\d{1,}\s\w{1}\d{6}\s").Success)
                    {
                        //Console.WriteLine(str.Replace(",", "").Replace(".", ""));
                        var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                        var item = new Item
                        {
                            Sku = test[3],
                            Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", "."),
                            Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                        };
                        items.Add(item);
                    }
                    else if (Regex.Match(str, @"^\w{1}\d{6}\s").Success &&
                             Regex.Match(str.Replace(",", "").Replace(".", ""),
                                 @"\d{1,}\s\d{1,}\s\d{1,}$").Success)
                    {
                        //Console.WriteLine(str.Replace(",", "").Replace(".", ""));
                        var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                        var item = new Item
                        {
                            Sku = test[0],
                            Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", "."),
                            Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                        };
                        items.Add(item);
                    }
                    else if (Regex.Match(str.Replace(",", "").Replace(".", ""),
                                @"\d{1,}\s\d{1,}\s\d{1,}$").Success)
                    {
                        Console.WriteLine(str.Replace(",", "").Replace(".", "") + " === " + lastSku);

                        var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                        var item = new Item
                        {
                            Sku = lastSku,
                            Cantidad = test[test.Length - 3].Split('.')[0].Replace(",", "."),
                            Precio = test[test.Length - 2].Split('.')[0].Replace(",", ".")
                        };
                        items.Add(item);
                    }
                    else
                    if (Regex.Match(str, @"\d\s[-]\s\w{1,}").Success && !str.Contains("DIMERC S.A"))
                    {
                        if (Regex.Match(str, @"\d{1,}[.]\d{2,}\s\d{2,}[/]\d{2,}[/]\d{4}").Success)
                        {
                            Match match = Regex.Match(pdfLines[firstIndex], @"\d{1,}.\d{2,}\s\d{2,}[/]\d{2,}[/]\d{4}");
                            if (!match.Success)
                            {
                                var testN = pdfLines[firstIndex+40].Trim().DeleteContoniousWhiteSpace().Split(' ');
                                var itemN = new Item
                                {
                                    Sku = lastSku,
                                    Cantidad = testN[testN.Length - 5].Split('.')[0].Replace(",", "."),
                                    Precio = testN[testN.Length - 3].Split('.')[0].Replace(",", ".")
                                };
                            if (!Regex.Match (testN[testN.Length-5],@"\d{1,}").Success && !Regex.Match(testN[testN.Length - 5], @"\d{1,}").Success)
                            {
                                continue;
                            }
                            else if (Regex.Match(testN[testN.Length - 5], @"\d{1,}").Success && Regex.Match(testN[testN.Length - 5], @"\d{1,}").Success)
                            {
                                items.Add(itemN);
                            }
                                
                            }else
                            if (match.Success)
                            {
                                var testN = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                                var itemN = new Item
                                {
                                    Sku = testN[2].Trim(),
                                    Cantidad = testN[testN.Length - 5].Split('.')[0].Replace(",", "."),
                                    Precio = testN[testN.Length - 3].Split('.')[0].Replace(",", ".")
                                };
                                items.Add(itemN);

                            }

                        
                        }
                        //var test = pdfLines[firstIndex].Trim().DeleteContoniousWhiteSpace().Split(' ');
                        //var item = new Item
                        //{
                        //    Sku = lastSku,
                        //    Cantidad = test[test.Length - 5].Split('.')[0].Replace(",", "."),
                        //    Precio = test[test.Length - 3].Split('.')[0].Replace(",", ".")
                        //};
                        //items.Add(item);
                    }
                    
                }
            
            return items;
        }


        private string GetSku(string str)
        {
            var index = Regex.Match(str, @"\s\w{1}\d{6}$").Index;
            return str.Substring(index, 8).Trim();
        }

        private string GetSku(string[] pdfLines, int i)
        {
            var cont = i;
            var descr = "Z446482";
            for (; cont < pdfLines.Length; cont++)
            {
                Match match = Regex.Match(pdfLines[cont], @"[A-Z]{1,}\d{5,6}");
                if (!match.Success)
                {
                    Match matchS = Regex.Match(pdfLines[cont + 1], @"[A-Z]{1,}\d{5,6}");
                    if (!matchS.Success)
                    {
                        descr = "Z446482";
                        break;
                    }
                    else
                    {
                        descr = matchS.Value.Trim();
                        break;
                    }
                }
                else
                    if (match.Success)
                {
                    descr = match.Value.Trim();
                    break;

                }
            }

            return descr;
            //var index = Regex.Match(str, @"\s\w{1}\d{6}$").Index;
            //return str.Substring(index, 8).Trim();
        }

        //private string GetSkuR(string[] pdfLines,int i)
        //{
        //    var ret = "Z446482";
        //    var contador = i;
        //    for (; contador < pdfLines.Length; contador++)
        //    {
        //        Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,}\d{5,6}");
        //        if (match.Success)
        //        {
        //            ret = match.Value.Trim();
        //            break;
        //        }
        //        //else
        //        //if (!match.Success)
        //        //{
        //        //    contador++;
        //        //    //var cod = pdfLines[contador].Split(' ');
        //        //    //ret = cod[0];
        //        //    //break;

        //        //}
                
        //    }
        //    return ret;          
        //}

        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato: Despachar en ......
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            return str.Substring(str.IndexOf("Despachar en ", StringComparison.Ordinal));
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var index = Regex.Match(str, @"\s\w{3}\d{2}-\d{10}\s").Index;
            var length = Regex.Match(str, @"\s\w{3}\d{2}-\d{10}\s").Length;
            return str.Substring(index, length).Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(' ');
            return aux[1];
        }

        public global::Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new global::Integraciones.Utils.OrdenCompra.OrdenCompra();
            
            OrdenCompra.CentroCosto = "0";
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                //if (!_readObs)
                //{
                //    if (_pdfLines[i].Contains("Entregar bienes en:"))
                //    {
                //        var a1 = _pdfLines[i].Split(':')[1].Trim();
                //        var a2 = _pdfLines[i + 1].Substring(_pdfLines[i + 1].IndexOf("96670840-9", StringComparison.Ordinal) + 11);
                //        var a3 = _pdfLines[i + 2].Substring(11);
                //        var dir = a1 + " " + a2 + " " + a3;
                //        OrdenCompra.Observaciones = "Entregar bienes en: " + dir;
                //        OrdenCompra.Direccion = dir;
                //        _readObs = true;
                //    }
                //}
                if (!_readOrdenCompra)
                {
                    if (_pdfLines[i].Trim().Contains(" Orden de Compra "))
                    {
                        OrdenCompra.CentroCosto = "0";
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[++i]);
                        _readOrdenCompra = true;
                        _readRut = false;
                    }
                }
                if (!_readRut)
                {
                    if (_pdfLines[i].Trim().Contains("R.U.T.: "))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                        _readItems = false;
                    }
                }
                //if (itemForPage < _pdfReader.NumerOfPages)
                //{
                if(!_readItems)
                { 
                    if (_pdfLines[i].Trim()
                            .Equals("Lín-Env Art/Descripción Id Fabricante Cantidad Precio U. Neto Total Neto") || _pdfLines[i].Trim().Equals("Línea Articulo/Servicio Descripción CantidadUM Precio Unitario Importe Total Fecha Entrega")
                            || _pdfLines[i].Trim().Equals("Línea Articulo/Servicio Descripción Cantidad UM Precio Unitario Importe Total Fecha Entrega"))
                    {
                        //itemForPage++;
                        var items = GetItems(_pdfLines, ++i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItems = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }

        /*prueba 03/07/2019
        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }*/
    }
}