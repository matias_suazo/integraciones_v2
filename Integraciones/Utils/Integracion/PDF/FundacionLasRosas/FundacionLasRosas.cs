﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.FundacionLasRosas
{
    class FundacionLasRosas
    {

        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\s[a-zA-Z]{1,2}\d{5,6}\s"}
            //{5, @"^\d{1,}\s(\w{1,}\s\w{1,}\.?)*" }
        };
        private const string RutPattern = "ORDEN DE COMPRA";
        private const string OrdenCompraPattern = "#";
        //private const string OrdenCompraPattern2 = "ORDEN DE COMPRA Nº";
        private const string ItemsHeaderPattern =
            "CANT DESCRIPCION PERIODO";
        private const string CentroCostoPattern = "DESPACHO";
        private const string ObservacionesPattern = "DESPACHO";
        private const string DireccionPattern = "Dirección";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public FundacionLasRosas(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                Rut = "70543600",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = _pdfLines[i];//GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = _pdfLines[i + 1].Split(' ')[0];//GetRut(_pdfLines[i+1]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i + 1]).ToUpper().Replace("NUESTRA", "").Replace("SEÑORA", "").Replace("DELA", "").Replace("DE LA", "").Replace("DE", "").Replace("LA", "").Replace("DEL", "").DeleteContoniousWhiteSpace().Trim().TrimStart();
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE;
                        _readCentroCosto = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones = GetObserv(_pdfLines[i+1]);
                        _readObs = true;
                    }
                }
                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = _pdfLines[i + 1];
                        _readDireccion = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            cantidades = GetCantidades(pdfLines, i);
            codigos = GetCodigos(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < cantidades.Count; cont++)
            {
                var IntegSKU = "Z446482";
                if (codigos.Count.Equals(0))
                {
                    var itemSe = new Item
                    {
                        Sku = IntegSKU,
                        Cantidad = cantidades[cont],
                        Precio = "1",
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    items.Add(itemSe);
                }
                else
                {
                    var itemSe1 = new Item
                    {
                        Sku = codigos[cont],
                        Cantidad = cantidades[cont],
                        Precio = "1",
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    items.Add(itemSe1);
                }
            }
            Console.WriteLine();

            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Subtotal"))
                {
                    break;
                }
                //Match match0 = Regex.Match(pdfLines[contador], @"PAÑO MICROFIBRA");
                //if (match0.Success)
                //{
                //    listaCodigos.Add("Z446482");
                //    continue;
                //}
                Match match = Regex.Match(pdfLines[contador], @"Mensual\s\d{5,6}\s");
                if (match.Success)
                {
                    var codig = match.Value.Split(' ');
                    if (Regex.Match(codig[1], @"^\d{6}").Success)
                    {
                        var aux = Regex.Match(codig[1], @"^\d{6}").Value;
                        listaCodigos.Add(aux);
                        continue;
                        if (Regex.Match(aux, @" -\d{8}").Success)
                        {
                            continue;
                        }
                    }
                    listaCodigos.Add(codig[1]);
                    continue;

                }

                Match match1 = Regex.Match(pdfLines[contador], @"Mensual\s[a-zA-Z]{1,2}\d{5,6}\s");
                if (match1.Success)
                {
                    var codig = match1.Value.Split(' ')[1];
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match2 = Regex.Match(pdfLines[contador], @"-\d{1,}\s\d{5,6}");
                if (match2.Success)
                {
                    var codig = match2.Value.Replace(" ", "");
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }

                Match match3 = Regex.Match(pdfLines[contador], @"-\d{1,}\s[a-zA-Z]{1,2}\d{5,6}\s");
                if (match3.Success)
                {
                    var codig = match3.Value.Replace(" ", "");
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match4 = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}\s");
                if (match4.Success)
                {
                    var codig = match4.Value.Replace(" ", "");
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match5 = Regex.Match(pdfLines[contador], @"\s[a-zA-Z]{1,2}\d{5,6}");
                if (match5.Success)
                {
                    var codig = match5.Value.Replace(" ", "");
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match6 = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}");
                if (match6.Success)
                {
                    var codig = match6.Value.Replace(" ", "");
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match7 = Regex.Match(pdfLines[contador], @"\s\d{5,6}");
                if (match7.Success)
                {
                    var codig = match7.Value.Replace(" ", "");
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match8 = Regex.Match(pdfLines[contador], @"^\d{5,}\s");
                if (match8.Success)
                {
                    var codig = match8.Value.Replace(" ", "");/*("SOL -"+codig))*/
                    if (Regex.Match(codig, @"-\d{8}").Success || Regex.Match(codig, @"^\d{5}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                Match match9 = Regex.Match(pdfLines[contador], @"[CL_]\d{5,}\s");
                if (match9.Success)
                {
                    var codig = match9.Value.Replace(" ", "").Replace("_", "");/*("SOL -"+codig))*/
                    if (Regex.Match(codig, @"-\d{8}").Success)
                    {
                        continue;
                    }
                    listaCodigos.Add(codig);
                    continue;
                }
                //Match match7 = Regex.Match(pdfLines[contador], @"CL_[a-zA-Z]{1,2}\d{5,6}");
                //if (match7.Success)
                //{
                //    listaCodigos.Add(match7.Value.Split('_')[1]);
                //}
                //Match match8 = Regex.Match(pdfLines[contador], @"CL_\d{5,6}");
                //if (match8.Success)
                //{
                //    listaCodigos.Add(match8.Value.Split('_')[1]);
                //}
                //if (!match.Success && !match1.Success && !match2.Success && !match3.Success)
                //{
                //    Match match4 = Regex.Match(pdfLines[contador], @"\d{1,}\s[a-zA-Z]{1,2}\d{5,6}");
                //    Match match5 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{5,6}\s");
                //    Match match6 = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}\sCLP");
                //    if (match4.Success)
                //    {
                //        listaCodigos.Add(match4.Value.Split(' ')[1]);
                //    }
                //    if (match5.Success)
                //    {
                //        listaCodigos.Add(match5.Value.Split(' ')[1]);
                //    }
                //    if (match6.Success)
                //    {
                //        listaCodigos.Add(match6.Value.Split(' ')[0]);
                //    }
                //}
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Dirección, Comuna"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match4 = Regex.Match(linea, @"Mensual\s[a-zA-Z]{1,2}\d{5,6}\s");
                Match match = Regex.Match(linea, @"Mensual\s\d{5,6}\s");
                Match match1 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{1,}\d{1,}\s\d{1,},\d{1,}");
                Match match2 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{1,}\d{1,}\s\d{1,}\d{1,}");
                Match match3 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{1,},\d{1,}\s\d{1,}\d{1,}");
                if (match4.Success)
                {
                    var cantid4d = linea.TrimStart().DeleteDotComa().Split(' ');
                    listaCantidades.Add(cantid4d[0]);
                    continue;
                }
                if (match.Success)
                {
                    listaCantidades.Add(linea.TrimStart().Split(' ')[0].DeleteDotComa());
                    continue;
                }
                if (match1.Success)
                {
                    listaCantidades.Add(match1.Value.TrimStart().Split(' ')[0].DeleteDotComa());
                    continue;
                }
                if (match2.Success)
                {
                    listaCantidades.Add(match2.Value.TrimStart().Split(' ')[0].DeleteDotComa());
                    continue;
                }
                if (match3.Success)
                {
                    listaCantidades.Add(match3.Value.TrimStart().Split(' ')[0].DeleteDotComa());
                    continue;
                }

            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            var aux1 = aux[2];
            return aux1.Trim();

        }

        private static string GetObserv(string str)
        {
            var aux = str.Split(' ');
            var aux1 = aux.ArrayToString(2,-1);
            return aux1.Trim();

        }

        private static string GetDireccion(string str)
        {
            var aux = str;
            //var aux1 = aux[1].Trim();
            return aux.DeleteDuplicateWords();

        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var vrut = str.Split('-', ' ');
            return vrut[vrut.Length - 2];


        }
        private static string GetRut2(string str)
        {
            var vrut = str.Split(' ');
            var rutf = vrut[vrut.Length - 1].Split('-')[0];
            if (rutf != "96670840-9")
            {
                return rutf;
            }
            else
            {
                return rutf.Split('-')[0];
            }
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);// || str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern2);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}

