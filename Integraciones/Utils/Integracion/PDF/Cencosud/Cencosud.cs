﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Cencosud
{
    public class Cencosud
    {
        #region Variables

        private const string ItemPattern = @"^\d{1,}\s\d{7}\s";
        private const string ItemPattern2 = @"^\d{1,5}\s\d";
        private const string RutPattern = "Datos de facturación";
        private const string RutPattern2 = "RUT";
        private const string OrdenCompraPattern = "Datos del proveedor";
        private const string OrdenCompraPattern2 = @"\d\s\/\s\d{2}\.\d{2}\.\d{4}";
        private const string OrdenCompraPattern3 = @"\d{1,}\/\d{2}\.\d{2}\.\d{4}";
        private const string OrdenCompraPattern4 = @"Número orden";
        private const string ItemsHeaderPattern =
            "Item Código Descripción Lugar Entrega Cantidad UMM Precio Pr. lista";
        private const string ItemsHeaderPattern2 =
           "Item Art. Descripción Cantidad Precio Precio Lista";
        private const string ItemsHeaderPattern4 = "Item Art. Código Refer. Descripción Cantidad Unidades Pr.Lista";
        private const string ItemsHeaderPattern3 = "Pedida Lista Total";
        private const string ItemsHeaderPattern5 = "Item Art. Descripción";        
        private const string ItemsFooterPattern = "Neto total sin IVA CLP";
        private const string CentroCostoPattern = @"\(\w{1}\d{3}\)";
        private const string CentroCostoPattern2 = "Lugar de entrega :";
        private const string CentroCostoPattern3 = "Lugarde entrega :";
        private const string CentroCostoPattern4 = "Lugardeentrega :";
        private const string CentroCostoPattern5 = " Lugar de entrega";
        private const string MailPattern = "Correo";
        private const string CompradorPattern = "Comprador";
        private bool _readOrdenCompra;
        private bool _readRut = true;
        private bool _readItems = true;
        private bool _readCorreo = true;
        private bool _readComprador = true;
        private bool _readCentroCosto =false;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }
        public Cencosud(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private bool IsItem(string str)
        {
            str = str.DeleteDotComa();
            return Regex.Match(str, ItemPattern).Success ; // || Regex.Match(str,ItemPattern2).Success;
        }
        private bool IsItem2(string str)
        {
            str = str.DeleteDotComa();
            return Regex.Match(str, ItemPattern2).Success; // || Regex.Match(str,ItemPattern2).Success;
        }
        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            var itemsArray = new List<string>();
            for (; i < pdfLines.Length; i++)
            {
                
                var aux = pdfLines[i].DeleteContoniousWhiteSpace().Trim();
                if (IsCentroCostoPattern(aux))
                {
                    OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                    OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                    _readCentroCosto = true;

                }
                bool itmaux0 = false;
                bool itmaux1 = false;
                Console.WriteLine(aux);
                if (IsItemsFooterPattern(aux)) break;
                if (IsItem(aux))
                {//es una linea que contiene items
                    Console.WriteLine("======= CASE 0======");
                    var test = aux.Split(' ');
                    var cantidad = GetCantidad(pdfLines, i);
                    var item = new Item
                    {
                        Sku = test[1],
                        Cantidad = cantidad,
                        Precio = test[test.Length - 2],
                        TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    aux = test.ArrayToString(2, test.Length - 5);
                    items.Add(item);
                    itmaux0 = true;
                }
                if (IsItem2(aux) && itmaux0==false)
                {//es una linea que contiene items
                    Console.WriteLine("======= CASE 1======");
                    var test = aux.Split(' ');
                    var cantidad = GetCantidad(pdfLines, i);
                    
                    var item = new Item
                    {
                        Sku = test[1],
                        Cantidad = cantidad,
                        Precio = test[test.Length - 2],
                        TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        
                    };
                    if (item.Sku.Equals("47952"))
                    {
                        item.Cantidad = test[test.Length - 4];
                    }
                    aux = test.ArrayToString(2, test.Length - 5);
                    items.Add(item);
                    itmaux1 = true;
                    if (Regex.Match(item.Precio, @"[A-Z]{1,}").Success)
                    {
                        continue;
                    }
                }
                itemsArray.Add(aux);
                if (!_readCentroCosto)

                {
                    var centroCosto = GetCentroCosto(aux);
                    if (centroCosto.Length == 0) continue;
                    OrdenCompra.CentroCosto = centroCosto;
                    _readCentroCosto = true;
                }
            }
            var dir = itemsArray.GetDireccionCencosud();
            OrdenCompra.Direccion = dir;
            //SumarIguales(items);
            return items;
        }

        private string GetComprador(string st)
        {
            var aux = st.Split(':');
            return aux[aux.Length - 1].Trim();
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = Regex.Match(str, @"\(\w{1}\d{3}\)").Value;
            return aux.Length > 0 ? aux.Substring(1, aux.Length - 2) : aux;
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(':', ' ');
            return aux[2].Trim();
        }

        private static string GetOrdenCompra2(string str)
        {
            var aux = str.Split(':', ' ');
            return aux[aux.Length-1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(':',' ');
            return aux[2].Substring(0, 8);
        }

        private static string GetRut2(string str)
        {
            //var aux = str.Split(':');
            Match match = Regex.Match(str, @"\d{1,}\-");
            var value = match.Value.Replace("-","").Trim();
            return value;
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0"
                
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                var aux = _pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var obs = "";
                if (!_readComprador)
                {
                    if (IsCompradorPattern(_pdfLines[i]))
                    {
                        var comprador = GetComprador(_pdfLines[i]);
                        obs += $"Comprador: {comprador}, ";
                        OrdenCompra.Observaciones += obs;
                        _readComprador = true;
                    }
                }
                if (!_readCorreo)
                {
                    if (IsCorreoPattern(_pdfLines[i]))
                    {
                        var correo = GetCorreo(_pdfLines[i]);
                        obs += $"Correo: {correo}";
                        OrdenCompra.Observaciones += obs;
                        _readCorreo = true;
                    }

                }
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]) || IsOrdenCompraPattern2(_pdfLines[i]))
                    {
                        if (IsOrdenCompraPattern2(_pdfLines[i]))
                        {
                            OrdenCompra.NumeroCompra = _pdfLines[i].Split('/')[0].Trim();
                        }
                        else 
                        { 
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+1]);
                            if(OrdenCompra.NumeroCompra.Equals(""))
                            {
                                OrdenCompra.NumeroCompra = GetOrdenCompra2(_pdfLines[i+1]);
                            }
                        }
                        if (OrdenCompra.NumeroCompra.Equals(""))
                        {
                            continue;
                        }
                        _readOrdenCompra = true;
                        _readRut = false;
                    }

                }
                if (!_readCentroCosto)
                {
                    
                    if (IsCentroCostoPattern2(_pdfLines[i]))
                    {
                        

                        if (IsCentroCostoPattern(_pdfLines[i]))
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        }
                        else
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i + 1]);
                        }
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                        _readCentroCosto = true;
                    }
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        if (_readCentroCosto == false)
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                            _readCentroCosto = true;
                        }
                    }
                }
                if (!_readRut)
                {

                    if (IsRutPattern(_pdfLines[i]))
                    {
                        if (_pdfLines[i].Trim().Contains("96670840"))
                        {
                            continue;
                        }
                        if (!_pdfLines[i].Contains("96670840-9"))
                        {
                            //OrdenCompra.Rut = _pdfLines[i].Split(':')[1].Split('-')[0].Trim();
                            OrdenCompra.Rut = GetRut2(_pdfLines[i]);
                        }
                        if (OrdenCompra.Rut.Equals(""))
                        {

                            OrdenCompra.Rut = GetRut2(_pdfLines[++i]);

                        }
                        if(OrdenCompra.Rut.Equals("") || OrdenCompra.Rut.Equals("0"))
                        {
                            continue;
                        }
                        _readRut = true;
                        _readItems = false;
                    }
                    if (_pdfLines[i].Contains("Razon Social"))
                    {
                        OrdenCompra.Rut = GetAux(_pdfLines[i]);
                        _readRut = true;
                        _readItems = false;
                    }
                }
                if (!_readItems)
                {
                    if (IsItemHeaderPattern(_pdfLines[i]))
                    {
                        Console.WriteLine(_pdfLines[i]);
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItems = true;
                        }
                    }
                }
            }
            Match match = Regex.Match(OrdenCompra.Rut, @"\d{7,8}");
            if (!match.Success)
            {
                OrdenCompra.Rut = "81201000";
            }
            return OrdenCompra;
        }

        private string GetCorreo(string st)
        {
            var aux = st.Split(':');
            return aux[aux.Length - 1].Trim();
        }

        private string GetAux(string st)
        {
            var ret = "";
            if(st.Contains("CENCOSUD RETAIL S.A."))
            {
                ret= "81201000";
            }
            return ret;
        }
        private string GetCantidad(string[] pdfLines,int i)
        {
            var contador = i;
            var ret = "-1";
            for (;contador<pdfLines.Length;contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\d{1,}\sCS\s\d");
                if (match.Success)
                {
                    var aux = match.Value.Split(' ')[0].Trim();
                    ret = aux;
                    break;

                }
                Match match2 = Regex.Match(pdfLines[contador], @"\d{1,}\sUN\s\d");
                if (match2.Success)
                {
                    var aux = match2.Value.Split(' ')[0].Trim();
                    ret = aux;
                    break;
                    
                }
                Match match3 = Regex.Match(pdfLines[contador], @"\d{1,}\sPAK\s\d");
                if (match3.Success)
                {
                    var aux = match3.Value.Split(' ')[0].Trim();
                    ret = aux;
                    break;

                }

            }
            return ret;
        }



        private bool IsItemHeaderPattern(string str)
        {
            return str.Trim().Contains(ItemsHeaderPattern) || str.Trim().Contains(ItemsHeaderPattern2) || str.Trim().Contains(ItemsHeaderPattern3) || str.Trim().Contains(ItemsHeaderPattern4) || str.Trim().Contains(ItemsHeaderPattern5);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return Regex.Match(str, CentroCostoPattern).Success;
        }

        private bool IsCentroCostoPattern2(string str)
        {
            return str.Trim().Contains(CentroCostoPattern2) || str.Trim().Contains(CentroCostoPattern3) || str.Trim().Contains(CentroCostoPattern4)
                || str.Trim().Contains(CentroCostoPattern5);
        }
        private bool IsOrdenCompraPattern2(string str)
        {
            return Regex.Match(str, OrdenCompraPattern2).Success || Regex.Match(str, OrdenCompraPattern3).Success || Regex.Match(str, OrdenCompraPattern4).Success;
        }
        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().Contains(OrdenCompraPattern);
        }
        private bool IsItemsFooterPattern(string str)
        {
            return str.Trim().Contains(ItemsFooterPattern);
        }

        private bool IsCompradorPattern(string str)
        {
            return str.Trim().Contains(CompradorPattern);
        }
        private bool IsCorreoPattern(string str)
        {
            return str.Trim().Contains(MailPattern);
        }

        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern2) || str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }
    }
}