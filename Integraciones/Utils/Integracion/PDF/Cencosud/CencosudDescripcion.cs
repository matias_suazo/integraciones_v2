﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;
using System.Linq;

namespace Integraciones.Utils.Integracion.Pdf.Cencosud
{
    public class CencosudDescripcion
    {
        #region Variables

        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s\d{7}\s"},
        };
        private const string RutPattern = "Datos de facturación";
        private const string OrdenCompraPattern = "Datos del proveedor Orden de compra";
        private const string OrdenCompraPattern2 = "Datos del proveedor Orden de servicios";
        private const string ItemsHeaderPattern =
            "Item Código Descripción Lugar Entrega Cantidad UMM Precio Pr. lista";
        private const string ItemsHeaderPattern2 = "Item Art. Descripción Cantidad Precio Precio Lista";
        private const string ItemsFooterPattern = "Neto total sin IVA CLP";
        private const string CentroCostoPattern = @"\(\w{1}\d{3}\)";
        private const string MailPattern = "Correo";
        private const string CompradorPattern = "Comprador";
        private bool _readOrdenCompra;
        private bool _readRut = true;
        private bool _readItems = true;
        private bool _readCorreo = true;
        private bool _readComprador = true;
        private bool _readCentroCosto;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }
        public CencosudDescripcion(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private List<Item> GetItems2(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            var descripciones = new List<string>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = test0[1],
                            Cantidad = test0[test0.Length - 4].Split(',')[0],
                            Precio = test0[test0.Length - 2].Split(',')[0],
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE
                        };
                        var descripcion = test0.ArrayToString(2,test0.Length-5);
                        aux = pdfLines[i + 1].DeleteContoniousWhiteSpace();
                        optItem = GetFormatItemsPattern(aux);
                        if (optItem == -1)
                        {
                            descripcion += $" {aux}";
                            aux = pdfLines[i + 2].DeleteContoniousWhiteSpace();
                            optItem = GetFormatItemsPattern(aux);
                            descripcion += optItem == -1 ? $" {aux}" : descripcion;
                        }
                        descripcion = descripcion.DeleteContoniousWhiteSpace();
                        if(descripcion.Contains("CORCHETERA B-4"))
                            Console.WriteLine("");
                        item0.Descripcion = descripcion;
                        descripciones.Add(descripcion);
                        items.Add(item0);
                        break;
                }
            }
            for (var j = 0; j < descripciones.Count; j++)
            {
                var repetida = descripciones.GetPalabraRepedida(descripciones[j].Split(' '),1,80f,false);
                if (string.IsNullOrEmpty(repetida)) break;
                descripciones.DeleteWord(repetida);
            }
            for (var j = 0; j < items.Count; j++)
            {
                items[j].Descripcion = descripciones[j];
            }
            //SumarIguales(items);
                return items;
        }


        private string GetComprador(string st)
        {
            var aux = st.Split(':');
            return aux[aux.Length - 1].Trim();
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = Regex.Match(str, @"\(\w{1}\d{3}\)").Value;
            return aux.Length > 0 ? aux.Substring(1, aux.Length - 2) : aux;
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(':');
            return aux[2].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(':');
            return aux[2].Substring(0, 8);
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                //CentroCosto = "0"
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                //var aux = _pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var obs = "";
                if (!_readComprador)
                {
                    if (IsCompradorPattern(_pdfLines[i]))
                    {
                        var comprador = GetComprador(_pdfLines[i]);
                        obs += $"Comprador: {comprador}, ";
                        OrdenCompra.Observaciones += obs;
                        _readComprador = true;
                    }
                }
                if (!_readCorreo)
                {
                    if (IsCorreoPattern(_pdfLines[i]))
                    {
                        var correo = GetCorreo(_pdfLines[i]);
                        obs += $"Correo: {correo}";
                        OrdenCompra.Observaciones += obs;
                        _readCorreo = true;
                    }

                }
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]) || IsOrdenCompraPattern2(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[++i]);
                        _readOrdenCompra = true;
                        _readRut = false;
                    }

                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        _readRut = true;
                        OrdenCompra.Rut = GetRut(_pdfLines[++i]);
                        _readItems = false;
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        _readCentroCosto = true;
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                      
                    }
                }
                if (!_readItems)
                {
                    if (IsItemHeaderPattern(_pdfLines[i]))
                    {
                        Console.WriteLine(_pdfLines[i]);
                        var items = GetItems2(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItems = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }

        private string GetCorreo(string st)
        {
            var aux = st.Split(':');
            return aux[aux.Length - 1].Trim();
        }


        private bool IsItemHeaderPattern(string str)
        {
            return str.Trim().Contains(ItemsHeaderPattern) || str.Trim().Contains(ItemsHeaderPattern2);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return Regex.Match(str, CentroCostoPattern).Success; 
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().Contains(OrdenCompraPattern);
        }
        private bool IsOrdenCompraPattern2(string str)
        {
            return str.Trim().Contains(OrdenCompraPattern2);
        }
        private bool IsItemsFooterPattern(string str)
        {
            return str.Trim().Contains(ItemsFooterPattern);
        }

        private bool IsCompradorPattern(string str)
        {
            return str.Trim().Contains(CompradorPattern);
        }
        private bool IsCorreoPattern(string str)
        {
            return str.Trim().Contains(MailPattern);
        }

        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }
    }
}