﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.RVC
{
    class Rvc
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\s\d{1,}\s\d{1,}\s\d{1,}$"},
            {1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s" }
        };
        private const string RutPattern = "RUT : ";
        private const string OrdenCompraPattern = "N°:";
        private const string ItemsHeaderPattern =
            "CANT. UNIDAD CÓDIGO DESCRIPCIÓN C. COSTO PARTIDA P. UNITARIO DESCUENTO";

        private const string CentroCostoPattern = "Centro de Gestión:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Rvc(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        i += 2;
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]).DeleteAcent().Trim();
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            var descripciones = new List<string>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                if (aux.Contains("Despachar a"))
                {
                    break;
                }
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test0[0].Replace(".", "").Split(',')[0],
                            Precio = test0[test0.Length - 3].Replace(".", "").Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                            //PareoDescripcionTelemarketingProductosActivos
                            //InsertPareoDescripcionHistoricoComprasConCodigoCliente
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        var descripcion = "";
                        var last = "";// pdfLines[i].ToUpper().Replace("OBRA", "").Trim().DeleteContoniousWhiteSpace();
                        var firstLine = pdfLines[i].ToUpper().Replace("OBRA", "").Trim().DeleteContoniousWhiteSpace();
                        var firstBelValue = true;
                        for (var j = i + 1; j < pdfLines.Length && GetFormatItemsPattern(pdfLines[j].Trim().DeleteContoniousWhiteSpace()) == -1
                            && !pdfLines[j].Contains("Neto $") && !pdfLines[j].Contains("Pag. 1 de "); j++)
                        {
                            last = pdfLines[j].ToUpper().Replace("OBRA", "").Trim().DeleteContoniousWhiteSpace();
                            if (firstBelValue)
                            {
                                if (firstLine.ContainsHexadecimalBelValue()) firstBelValue = false;
                            }
                            if (last.ContainsHexadecimalBelValue() && firstBelValue)
                            {
                                firstBelValue = false;
                                continue;
                            }
                            descripcion += $" {last}";
                        }
                        descripcion.Replace(last, "");
                        var split = descripcion.ToUpper().DeleteContoniousWhiteSpace().Split(' ');
                        if (split[0].Equals("UN") ||
                            split[0].Equals("CAJA") ||
                            split[0].Equals("PQ") ||
                            split[0].Equals("BOLSA") ||
                            split[0].Equals("ROLLO"))
                            descripcion = split.ArrayToString(2, split.Length).DeleteContoniousWhiteSpace();

                        item0.Descripcion = descripcion;
                        Console.WriteLine($"CointainsHexadecimalBelValue : {item0.Descripcion} , {item0.Descripcion.ContainsHexadecimalBelValue()}");
                        item0.DeleteLongNumberFromDescription(6);

                        item0.Descripcion = item0.Descripcion
                            .Replace("TE EN BOLSITAS", "")
                            .Replace("LAPIZ PASTA PUNTA GRUESA", "")
                            .Replace("PALA ASEO ", "")
                            .Replace("ARCHIVADOR LOMO  ", "")
                            .Replace("ESCOBILLON MUNICIPAL ", "")
                            .Replace("TONER CARTUCHO ", "CARTUCHO ")
                            .Replace("[INTEGRACI", "")
                            .Replace("ESCOBILLON GRANDE MUNICIPAL", "ESCOBILLON MUNICIPAL")
                            .Replace("GODOY + BAÑOS MAESTROS))", "")
                            .Replace("( L.ARAYA+ J.DIAZ+R.MUÑOZ)", "")
                            .Replace("GODOY FRAN", "")
                            .Replace("(MARIO GODOY)", "")
                            .Replace("(FRAN)", "")
                            .Replace("(MANU)", "")
                            .Replace("(BAÑO MAESTROS)", "")
                            .Replace("( JOSE SECO+5 CHRITOPHER PONTIGO FRAN + JOSE TAPIA)", "")
                            .Replace("JOSE TAPIA+ FRAN)", "")
                            .Replace("(CHRISTOPHER PONTIGO)", "")
                            .Replace("(JOSE TAPIA)", "")
                            .Replace("(R.RIVAS)", "")
                            .Replace("(LUIS ARAYA- JOSE DIAZ-RAUL MUÑO- MANU", "")
                            .Replace("(LUIS ARAYA)", "")
                            .Replace("(JOSE SECO)", "")
                            .Replace("(JOSE TAPIA)", "")
                            .Replace("PAG. CÓDIGO VERIFICACIÓN OC SEGURA: 252624610082 LOS CONQUISTADORES PISO, PROVIDENCIA, SANTIAGO FONO: +56-02-24108800 N°: CENTRO GESTIÓN: ORDEN COMPRA ALTO FORESTA FECHA: SEÑOR(ES) : DIMERC S.A. A SR. : IVONNE ESPARZA DIRECCIÓN : ALBERTO PEPPER RENCA FONO : RUT : FAX : VALOR TOTAL CANT. UNIDAD CÓDIGO DESCRIPCIÓN C. COSTO PARTIDA P. UNITARIO DESCUENTO", "")
                            .Replace("R.RIVAS+3+JOSE SECO, CHRISTOPHER PONTIGO, + JOSE TAPIA, FRAN", "")
                            .Replace("CHRISTOPHER PONTIGO", "")
                            .Replace("RODRIGO RIVAS","")
                            .Replace("(FRAN + C.PONTIGO)", "")
                            .Replace("(JOSE SECO)", "")
                            .Replace("(MARIO GODOY)", "")
                            .Replace("(MARIO GODOY + BAÑOS MAESTROS)", "")
                            .Replace("(MARIO", "")
                            .Replace("( JOSE SECO+5 CHRITOPHER PONTIGO FRAN + JOSE TAPIA)", "")
                            .Replace("(LUIS ARAYA- JOSE DIAZ-RAUL MUÑO- MANU", "")
                            .Replace("R.RIVAS+3+JOSE SECO, CHRISTOPHER PONTIGO, + JOSE TAPIA, FRAN","")
                            .Replace("CRISTOPHER PONTIGO","")
                            .Replace("GODOY)","")
                            .DeleteBelHexadecimalValues();
                        descripciones.Add(item0.Descripcion);

                        if (item0.Descripcion.Contains("PIZARRA CORCHO 60X90 M/MADERA DIMERC PAG."))
                            item0.Descripcion = "PIZARRA CORCHO 60X90 M/MADERA DIMERC";
                            items.Add(item0);
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        var concatAll = aux;
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        Console.WriteLine($"SKU: {item0.Sku}");
                        break;
                }
            }
            //var items3 = NormalizeDescription2(descripciones, items);
            //Console.WriteLine($"NormalizeDescription2");
            //foreach(var item in items3)
            //{
            //    Console.WriteLine($"{item.ToString()}");
            //}
            //Console.WriteLine($"FIN NormalizeDescription2");
            var items2 = NormalizeDescription(descripciones, items, 0);
            Console.WriteLine($"NormalizeDescription");
            foreach (var item in items2)
            {
                var skuDimer = GetSkuDimerc(item.Descripcion.DeleteContoniousWhiteSpace().Split(' '));
                if (OracleDataAccess.ExistProduct(skuDimer) && !skuDimer.Equals("Z446482"))
                {
                    item.Sku = skuDimer;
                    item.TipoPareoProducto = TipoPareoProducto.SIN_PAREO;
                }
            }
            Console.WriteLine($"FIN NormalizeDescription");
            items2.DeletePalabrasConsecutivasRepetidas();
            for (var k = 0; k < items2.Count; k++)
            {
                items2[k].Descripcion = items2[k].Descripcion
                    .Replace("(LUIS ARAYA- JOSE DIAZ-RAUL MUÑO- MANU", "")
                    .Replace(
                        "PAG. CÓDIGO VERIFICACIÓN OC SEGURA: 252624610082 LOS CONQUISTADORES PISO, PROVIDENCIA, SANTIAGO FONO: +56-02-24108800 N°: CENTRO GESTIÓN: ORDEN COMPRA ALTO FORESTA FECHA: SEÑOR(ES) : S.A. A SR. : IVONNE ESPARZA DIRECCIÓN : ALBERTO PEPPER RENCA FONO : RUT : FAX : VALOR TOTAL CANT. UNIDAD CÓDIGO DESCRIPCIÓN C. COSTO PARTIDA P. UNITARIO DESCUENTO",
                        "")
                    .Replace("R.RIVAS+3+JOSE SECO, CHRISTOPHER PONTIGO, JOSE TAPIA, FRAN", "")
                    .Replace("CRISTOPHER PONTIGO", "")
                    .DeleteSymbol();
            }
            //return items;
            return items2;
        }

        

        private List<Item>  NormalizeDescription2(List<string> descripciones, List<Item> rawItems)
        {
            var deleteMarca = new List<string>();
            var items = rawItems.Select(item => item.Clone()).ToList();
            var tamañoPalabra = 1;
            for(var i = 0; i< descripciones.Count; i++)
            {
                var rep = descripciones.GetPalabraRepedida(descripciones[i].Split(' '), tamañoPalabra, 70.0f);
                var isMarca = OracleDataAccess.IsMarca(rep);
                if (rep == null)
                {
                    tamañoPalabra++;
                    continue;
                }
                if (isMarca)
                {
                    deleteMarca.Add(rep);
                    continue;
                }
                for (var y = 0; y < descripciones.Count; y++)
                {
                    var newDes = descripciones[y].Split(' ')
                        .Where(descripcionSplit => !descripcionSplit.Equals(rep))
                        .Aggregate("", (current, descripcionSplit) => current + $" {descripcionSplit}");
                    descripciones[y] = newDes.DeleteContoniousWhiteSpace();
                    rawItems[y].Descripcion = newDes.DeleteContoniousWhiteSpace();
                }
            }
            return items;

        }
        private List<Item> NormalizeDescription(List<string> descripciones, List<Item> items, int index)
        {
            if (index >= 10) return items;

            var repeticion = descripciones.GetPatronRepeticion();
            //Console.WriteLine($"Repeticion: {index} {repeticion}, {repeticion.Split(' ').Length}");
            while (repeticion.DeleteContoniousWhiteSpace().Split(' ').Length > 1)
            {
                var deleteMarca = new List<string>();
                foreach (var rep in repeticion.DeleteContoniousWhiteSpace().Split(' '))
                {
                    var isMarca = OracleDataAccess.IsMarca(rep);
                    //Console.WriteLine($"ISMARCA: {rep}, {isMarca}");
                    if (rep.Equals("")) continue;
                    if (isMarca)
                    {
                        deleteMarca.Add(rep);
                        continue;
                    }
                    for (var y = 0; y < descripciones.Count; y++)
                    {
                        var newDes = "";
                        foreach (var descripcionSplit in descripciones[y].Split(' '))
                        {
                            if(descripcionSplit.Equals(rep)) continue;
                            newDes += $" {descripcionSplit}";
                        }
                        //var newDes = descripciones[y].Replace(rep, "");
                        //Console.WriteLine($"PP 1: {rep}, RAW: {descripciones[y]} NewRaw: {newDes}");
                        descripciones[y] = newDes.DeleteContoniousWhiteSpace();
                        //newDescripcion.Add(newDes);
                    }

                }
                for (var x = 0; x < items.Count; x++)
                {
                    items[x].Descripcion = descripciones[x];
                }
                repeticion = descripciones.GetPatronRepeticionSinConsideracion(deleteMarca);
                //Console.WriteLine($"Repeticion 2: {repeticion}, {repeticion.Split(' ').Length}");
            }
            var count = 0;
            //var num = 0;
            while (repeticion.DeleteContoniousWhiteSpace().Split(' ').Length == 1)
            {
                var deleteMarca = new List<string>();
                if (count >= 30) break;
                if (repeticion.Equals("")) break;
                //if (repeticion.DeleteContoniousWhiteSpace().Split(' ').Length == 2)
                //{

                var num = 0;
                if (!repeticion.Contains("COSTOS")
                    && !repeticion.Contains("%")
                    && !repeticion.Contains(",")
                    && !repeticion.Contains("[]")
                    && !int.TryParse(repeticion.Replace(",", ""), out num))
                {
                    count++;
                    repeticion = descripciones.GetPatronRepeticion();
                    //Console.WriteLine($"Repeticion 2: {repeticion}, {repeticion.Split(' ').Length}");
                    continue;
                }
                //}
                foreach (var rep in repeticion.DeleteContoniousWhiteSpace().Split(' '))
                {
                    var isMarca = OracleDataAccess.IsMarca(rep);
                    //Console.WriteLine($"ISMARCA: {rep}, {isMarca}");
                    if (rep.Equals("")) continue;
                    if (isMarca)
                    {
                        deleteMarca.Add(rep);
                        continue;
                    }
                    //foreach (var des in descripciones)
                    for (var y = 0; y < descripciones.Count; y++)
                    {
                        var newDes = descripciones[y].Replace(rep, "");
                        //Console.WriteLine($"PP 2: {rep}, RAW: {descripciones[y]} NewRaw: {newDes}");
                        descripciones[y] = newDes.DeleteContoniousWhiteSpace();
                        //newDescripcion.Add(newDes);
                    }

                }
                for (var x = 0; x < items.Count; x++)
                {
                    items[x].Descripcion = descripciones[x];
                }
                repeticion = descripciones.GetPatronRepeticionSinConsideracion(deleteMarca);
                //Console.WriteLine($"Repeticion 2: {repeticion}, {repeticion.Split(' ').Length}");
                if (repeticion.Equals("")) break;
                count++;
            }
            index++;
            return NormalizeDescription(descripciones, items, index);
        }

        private string GetSkuDimerc(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }



        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1.ArrayToString(0, test1.Length).ToUpper();
            Console.WriteLine($"GETSKU: {skuDefaultPosition}");
            if (Regex.Match(skuDefaultPosition, @"(LITRO|BOLSA|UN|CAJA|PQ|ROLLO)\s[a-zA-Z]{1}\d{8}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[LITRO|BOLSA|UN|CAJA|PQ|ROLLO]\s[a-zA-Z]{1}\d{8}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[LITRO|BOLSA|UN|CAJA|PQ|ROLLO]\s[a-zA-Z]{1}\d{8}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
                ret = ret.Split(' ')[1];
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            return str.ToUpper().DeleteDotComa().Replace(" -","-").Replace("- ","-");
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length -1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            return split[split.Length -1];
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(",","").Replace(".", "");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}
