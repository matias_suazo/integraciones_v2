﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.IsapreConsalud
{
    public class IsapreConsalud

    {
        #region Variables
        private const string ItemPattern = @"\s[A-Za-z]{1,2}\d{4,6}\s"; //^\d{1,}\s\d{1,}\s\w{1,}\s\w{1}\d{6}\s|
        private const string ItemPattern2 = @"\s\d{6}\s";

        /*
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s\w{3}\d{5,6}\s\d{3,}\s\d{1,}\s\d{1,}"},
            {1, @"^\d{1,}\s\w{3}\d{5,6}\s\d{1,}\s" }
        };
        */
        private const string RutPattern = "Razón Social";
        private const string OrdenCompraPattern = "Número";
        private const string ItemsHeaderPattern =
            "Nro Cantidad Unidad";
        /*
        private readonly List<string> _itemsHeaderPatterns = new List<string>
        {
            {"Nro Cantidad Unidad Código Descripción del Producto Total"},
            {"Nro Cantidad Unidad Código Total "}
        };
        */
        private const string ItemsFooterPattern = "Total Neto";
        private const string CentroCostoPattern = "Centro Costo:";
        private bool _readOrdenCompra;
        private bool _readRut = true;
        private bool _readItems = true;
        private bool _readCentroCosto;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }
        public IsapreConsalud(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private bool IsItem(string str)
        {
            str = str.DeleteDotComa();
            return Regex.Match(str, ItemPattern).Success;
        }
        private bool IsItem2(string str)
        {
            str = str.DeleteDotComa();
            return Regex.Match(str, ItemPattern2).Success;
        }
        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace().Trim();
                if (IsItemsFooterPattern(aux)) break;

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i + 1]);
                        if (OrdenCompra.CentroCosto.Equals(""))
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i + 2]);
                        }
                        _readCentroCosto = true;
                    }
                }
                if (IsItem(aux)|| IsItem2(aux))
                {
                    var test = aux.Split(' ');
                    var item = new Item
                    {
                        Sku = test[3].Trim(),
                        Cantidad = test[1].Trim(),
                        Precio = "1",
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING,
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                    };
                    items.Add(item);
                }
            }
            //SumarIguales(items);
            return items;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            str = str.DeleteContoniousWhiteSpace();

            var index = Regex.Match(str, @"\s\d{4,5}-\w{1,}").Index;
            //var length = Regex.Match(str, @"\s\d{4,5}-\w{1,}-").Length;
            //var index = Regex.Match(str, @"\s\d{4,5}-").Index;
            //var length = Regex.Match(str, @"\s\d{4,5}-").Length;
            //return str.Substring(index, length - 1).Trim();
            if (index == 0)
            {
                return "";
            }
            var ret = "";
            
            //Match match = Regex.Match(str, @"\d{1,}\-[A-Z]{2,}");
            Match match = Regex.Match(str, @"\s\d{4,5}-\w{1,}");
            if (match.Success)
            {
                 ret = match.Value.Split('-')[0].Trim();
            }
            //var a = str.Split('-');
           /* for(int i = a.Length-1; i <= a.Length; i--)
            {
                Match match = Regex.Match(a[i].Trim(), @"\d{1,}\-[A-Z]{1,}");
                if (match.Success)
                {
                    ret = match.Value.Trim();
                    break;
                }
            }*/
                    return ret;
            //return str.Substring(index).Split('-')[0].Replace(" ","");
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            str = str.DeleteContoniousWhiteSpace();
            var aux = str.Trim().Split(' ');
            return aux[1];
            //return str.Contains("RUT") ? aux[1] : aux[aux.Length - 1];
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Trim().Split(' ');
            return aux[aux.Length - 1];
        }

        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                //CentroCosto = "0",
                Rut = "96856780",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                        _readItems = false;
                    }

                }
                //if (!_readRut)
                //{
                //    if (IsRutPattern(_pdfLines[i]))
                //    {
                //        _readRut = true;
                //        OrdenCompra.Rut = "96856780";//GetRut(_pdfLines[++i]);
                //        _readItems = false;
                //    }
                //}
                if (!_readItems)
                {
                    if (IsItemHeaderPattern(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                        }
                    }
                }
            }
            return OrdenCompra;
        }


        private bool IsItemHeaderPattern(string str)
        {
            return str.Trim().Contains(ItemsHeaderPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Contains(CentroCostoPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().Contains(OrdenCompraPattern);
        }
        private bool IsItemsFooterPattern(string str)
        {
            return str.Trim().Contains(ItemsFooterPattern);
        }



        private bool IsRutPattern(string str)
        {
            return str.Trim().Contains(RutPattern);
        }
    }
}