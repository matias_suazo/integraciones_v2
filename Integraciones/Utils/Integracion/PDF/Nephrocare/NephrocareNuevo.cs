﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Nephrocare
{
    class NephrocareNuevo
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s[a-zA-Z]{1,2}\d{4,6}\s"},
            {1, @"\s[a-zA-Z]{1,2}\d{5,6}\s\d{1,}\s\d{1,}$" },
            {2, @"^[a-zA-Z]{1,2}\d{5,6}" },
            {3, @"^\d{1,}\s[a-zA-Z]{1,2}\d{4,6}\s\d{1,}$" },
            {4, @"[a-zA-Z]{1,}\s[a-zA-Z]{1,2}\d{5,6}\s\d{1,}\s\d{1,}$"},//L399622 128 2560
            //{5, @"[A-Z]{1,}\d{5,6}\s\$\s\d{1,}" } //Z390804 $ 
            { 5,@"^\d{1,}\s[A-Z]{1,}\s"}
        };
        private const string RutPattern = "Direccion";
        private const string RutPattern1 = "RUT";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA N°";
        private const string OrdenCompraPattern1 = "Pedido de Compra";
        private const string ItemsHeaderPattern = "POS CANT UN MED CODIGO";
        private const string ItemsHeaderPattern2 = "CANT UN MED CODIGO";
        private const string ItemsHeaderPattern1 = "Posición Material/Descripción";
        private const string ItemsHeaderPattern3 = "Posición Material/descripción";

        private const string CentroCostoPattern = "NEPHROCARE CHILE";
        private const string ObservacionesPattern = "CHILE";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public NephrocareNuevo(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+1]);
                        if (OrdenCompra.NumeroCompra.Contains("-"))
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        }
                            _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    
                    if (IsRutPattern(_pdfLines[i]) && _pdfLines[i].Contains("99507130"))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);

                        _readRut = true;
                    }
                    if (IsRutPattern(_pdfLines[i]) && !_pdfLines[i].Contains("99507130"))
                    {
                        var rut = GetRut(_pdfLines[i - 1]);
                        Match match = Regex.Match(rut, @"\d{6,}");
                        if (!match.Success)
                        {
                            var rut1 = GetRut1(_pdfLines[i+10 ]);
                            Match match1 = Regex.Match(rut1, @"\d{6,}");
                            if (!match1.Success || rut1 == "9977675")
                            {
                                continue;
                            }
                            OrdenCompra.Rut = rut1;
                            _readRut = true;
                            continue;
                        }
                        OrdenCompra.Rut = rut;
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        //OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i-1]);
                        OrdenCompra.CentroCosto = GetCencos(_pdfLines, i+1);
                        //OrdenCompra.CentroCosto = "0";
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i-1].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            codigos = GetCodigos(pdfLines, i);
            cantidades = GetCantidades(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < codigos.Count; cont++)
            {
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();

            return items;
        }


        private string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }

            }

            return ret;
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("SUBTOTAL"))
                {
                    break;
                }
                if (pdfLines[contador].Contains("impuesto"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,2}\d{6}");
                if (match.Success)
                {
                    listaCodigos.Add(match.Value);
                    continue;
                }
                Match match2 = Regex.Match(pdfLines[contador], @"\d{6}$");
                if (match2.Success && !pdfLines[contador].Contains(OrdenCompra.NumeroCompra))
                {
                    listaCodigos.Add(match2.Value);
                    continue;
                }

                Match match1 = Regex.Match(pdfLines[contador], @"\s\d{6}");
                if (match1.Success && !listaCodigos.Contains(match1.Value) && !pdfLines[contador].Contains(OrdenCompra.NumeroCompra))
                {
                    if(match1.Value.Split(' ')[1].Replace(",", "").Equals("") || match1.Value.Split(' ')[1].Replace(",", "").Equals("700000") ||
                        match1.Value.Split(' ')[1].Replace(",","").Equals("664320") || match1.Value.Split(' ')[1].Replace(",", "").Equals("664420"))
                    {
                        continue;
                    }
                    listaCodigos.Add(match1.Value.Split(' ')[1].Replace(",", "").Replace(" ",""));
                }
                
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("SUBTOTAL"))
                {
                    break;
                }
                if (pdfLines[contador].Contains("impuesto"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                if (!OrdenCompra.Rut.Equals("99507130"))
                {
                    Match match = Regex.Match(linea, @"^\d{1,}");
                    Match match3 = Regex.Match(linea, @"^\s\d{1,}[-]\w{2,}");
                    Match match4 = Regex.Match(linea, @"^\d{1,}[-]\w{3,}\s\d{1,}[.]\d{1,}");
                    if (match.Success && !match.Value.Contains("502481"))
                    {
                        if (match.Value.Equals("02"))
                        {
                            continue;
                        }
                        else
                        if (match4.Success)
                        {
                            continue;
                        }
                        else
                        {
                            listaCantidades.Add(match.Value.Split(' ')[0].Trim());
                        }
                    }

                }
                if (OrdenCompra.Rut.Equals("99507130"))
                {
                    Match match = Regex.Match(linea, @"\d{1,}[.]000\sUN");
                    if (match.Success)
                    {
                        listaCantidades.Add(match.Value.Split(' ', '.')[0].Trim());
                    }
                }

            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }



        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            var dflt = aux[0];
            var ret = "-1";
            if (Regex.Match(dflt, @"\d{1,}\-\d{1,}\-\d{1,}").Success)
            {
                var index = Regex.Match(dflt, @"\d{1,}\-\d{1,}\-\d{1,}").Index;
                var length = Regex.Match(dflt, @"\d{1,}\-\d{1,}\-\d{1,}").Length;
                return ret = dflt.Substring(index, length).Trim();
            }
            else { return ret; }


        }

        private string GetCencos(string[] pdfLines, int i)
        {
            var ret = "0";
            var split = pdfLines[i].Split(':');

            //var contador = i;
            //for (; contador < pdfLines.Length; contador++)
            //{
            //    Match match = Regex.Match(pdfLines[contador], @"\d{1,}\-\d{3}-\d{2}");
            //    if (match.Success)
            //    {
            //        ret = match.Value.Trim();
            //        break;
            //    }
            //}
            return split[split.Length -1].Trim();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ', '-');
            //return split[split.Length - 1];
            return split[1].DeleteDotComa();
        }
        private static string GetRut1(string str)
        {
            var split = str.Split(' ', '-');
            //return split[split.Length - 1];
            return split[1].DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.Replace(".","").Replace("$","");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern1) 
                || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern3);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern1);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern1);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion
    }
}
/*
for (; i < pdfLines.Length - 1; i++)
//foreach(var str in pdfLines)
{
    var aux = pdfLines[i].Replace(".", "").Replace("$", "").Replace(" ,", "").Replace(",","").DeleteContoniousWhiteSpace();
    var aux1 = pdfLines[i+1].Trim().DeleteContoniousWhiteSpace();
    var aux2 = pdfLines[i - 1].Trim().DeleteContoniousWhiteSpace();


    //Es una linea de Items 
    var optItem = GetFormatItemsPattern(aux);
    switch (optItem)
    {
        case 0:
            Console.WriteLine("==================ITEM CASE 0=====================");
            var test0 = aux.Split(' ');
            var item0 = new Item
            {
                Sku = test0[1].Trim(),
                Cantidad = test0[0].Trim(),
                Precio = test0[test0.Length - 3].Replace(".",""),
                TipoPareoProducto = TipoPareoProducto.SIN_PAREO
            };
            //Concatenar todo y Buscar por Patrones el SKU DIMERC
            //var concatAll = "";
            //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
            //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
            //{
            //    concatAll += $" {aux}";
            //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
            //}
            //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
            items.Add(item0);
            break;
        case 1:
            Console.WriteLine("==================ITEM CASE 1=====================");
            var test1 = aux.Split(' ');
            var item1 = new Item
            {
                Sku = test1[test1.Length -3].Trim(),
                Cantidad = test1[0].Trim(),
                Precio = test1[test1.Length - 2].Replace(".", "").Trim(),
                TipoPareoProducto = TipoPareoProducto.SIN_PAREO
            };
            items.Add(item1);
            break;
        case 2:
            Console.WriteLine("==================ITEM CASE 2=====================");
            var test00= aux.Split(' ');
            var test2 = aux1.Split(' ');
            var test22 = aux2.Split(' ');
            var item2 = new Item
            {
                Sku = test00[0].Trim(),
                Cantidad = test2[0].Trim(),
                Precio = test2[test2.Length - 3].Replace(".", "").Trim(),
                TipoPareoProducto = TipoPareoProducto.SIN_PAREO
            };
            //if (item2.Cantidad.Contains("CUCHILLO"))
            //{
            //    item2.Cantidad = "1";

            //}
            items.Add(item2);
            break;
        case 3:
            Console.WriteLine("==================ITEM CASE 3=====================");
            var test3 = aux.Split(' ');
            var item3 = new Item
            {
                Sku = test3[1].Trim(),
                Cantidad = test3[0].Trim(),
                Precio = test3[test3.Length - 1].Replace(".", "").Trim(),
                TipoPareoProducto = TipoPareoProducto.SIN_PAREO
            };
            items.Add(item3);
            break;
        case 4:
            Console.WriteLine("==================ITEM CASE 4=====================");
            var test4 = aux.Split(' ');
            var item4 = new Item
            {
                Sku = test4[test4.Length - 3].Trim(),
                Cantidad = test4[0].Trim(),
                Precio = test4[test4.Length - 2].Replace(".", "").Trim(),
                TipoPareoProducto = TipoPareoProducto.SIN_PAREO
            };
            //if (item4.Cantidad.Contains("CORCHETERA"))
            //{
            //    item4.Cantidad = "2";

            //}
            items.Add(item4);
            break;
        case 5:
            Console.WriteLine("==================ITEM CASE 5=====================");
            if (aux.Contains("CC %")) {
                break;
            }
            var test5 = aux.Split(' ');
            var sku = GetSku(pdfLines, i);

            var item5 = new Item {
                Sku = sku,
                Cantidad = test5[0].Trim(),
                Precio = test5[test5.Length-2].Trim(),
                TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO,
                TipoPareoProducto = TipoPareoProducto.SIN_PAREO
            };
            items.Add(item5);
            break;
    }
}*/
//SumarIguales(items);