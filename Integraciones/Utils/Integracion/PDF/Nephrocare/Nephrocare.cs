﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.Nephrocare
{
    class Nephrocare
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[aA-zZ]{1}\d{1,}$"},
            {1, @"\w{1,}\s\w{1,}[:]\s$"},
            {2, @"\w{1,}[:]\s\d{5,}$"}
        };


        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "Número de";
        private const string ItemsHeaderPattern = "Importe neto";

        private const string CentroCostoPattern = "Condiciones de pago";
        private const string ObservacionesPattern = "Dirección factura";
        private const string DireccionPattern = "mercancías";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private bool _readDireccion;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Nephrocare(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {

                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        var ccos = GetCentroCosto(_pdfLines[i - 2], _pdfLines[i - 2]);
                        OrdenCompra.CentroCosto = ccos.ToUpper().Trim().Replace("QUIILICURA", "QUILICURA");
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                        _readCentroCosto = true;


                    }
                }


                    if (!_readObs)
                    {
                        if (IsObservacionPattern(_pdfLines[i]))
                        {
                            //var splitObs = _pdfLines[i + 1].Split();
                            //var splitObs2 = _pdfLines[i + 2].Split();
                            OrdenCompra.Observaciones = $"CentroCosto: {OrdenCompra.CentroCosto} ";
                            //$"{splitObs2.ArrayToString(0, 1)}";
                            _readObs = true;
                        }

                    }
                    if (!_readDireccion)
                    {
                        if (IsDirectionPattern(_pdfLines[i]))
                        {
                            var direc = getDireccion(_pdfLines[i + 1], _pdfLines[i + 1]);
                            OrdenCompra.Direccion = direc;
                            _readDireccion = true;
                        }
                    }
                    if (!_readItem)
                    {
                        if (IsHeaderItemPatterns(_pdfLines[i]))
                        {
                            var items = GetItems(_pdfLines, i);
                            if (items.Count > 0)
                            {
                                OrdenCompra.Items.AddRange(items);
                                _readItem = true;
                            }
                        }
                    }
                
                if (OrdenCompra.NumeroCompra.Equals(""))
                {
                    OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
                }
            }
             return OrdenCompra;

        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = pdfLines[i-1].Split(' ');
                        var test1 = pdfLines[i].Split(' ');
                        if (test0.Count() == 7 && pdfLines[i - 1].Contains(ItemsHeaderPattern))
                        {
                            test0 = pdfLines[i - 5].Split(' ');
                            if (test0.Count() < 5)
                            {
                                test0 = pdfLines[i - 6].Split(' ');
                            }
                            if (test0.Count() == 11)
                            {
                                var item0 = new Item
                                {
                                    Sku = GetSku(test1),
                                    Cantidad = test0[test0.Length - 6].Replace(".00", ""),
                                    Precio = "1",
                                    Descripcion = "",
                                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                                };
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 5].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 6].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 4].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                                {
                                    item0.Cantidad = "1";
                                }
                                items.Add(item0);
                                break;
                            }
                            if (test0.Count() == 10)
                            {
                                var item0 = new Item
                                {
                                    Sku = GetSku(test1),
                                    Cantidad = test0[test0.Length - 4].Replace(".00", ""),
                                    Precio = "1",
                                    Descripcion = "",
                                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                                };
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 4].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                                {
                                    item0.Cantidad = "1";
                                }
                                items.Add(item0);
                                break;
                            }
                            if (test0.Count() == 9)
                            {
                                var item0 = new Item
                                {
                                    Sku = GetSku(test1),
                                    Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace(",", ""),
                                    Precio = "1",
                                    Descripcion = "",
                                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                                };
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 2].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 3].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                                {
                                    item0.Cantidad = "1";
                                }
                                items.Add(item0);
                                break;
                            }
                            if (test0.Count() == 8)
                            {
                                var item0 = new Item
                                {
                                    Sku = GetSku(test1),
                                    Cantidad = test0[test0.Length - 6].Replace(".00", ""),
                                    Precio = "1",
                                    Descripcion = "",
                                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                                };
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 4].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                                {
                                    item0.Cantidad = "1";
                                }
                                items.Add(item0);
                                break;
                            }
                            if (test0.Count() == 7)
                            {
                                var item0 = new Item
                                {
                                    Sku = GetSku(test1),
                                    Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace(",", ""),
                                    Precio = "1",
                                    Descripcion = "",
                                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                                };
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                                {
                                    item0.Cantidad = "1";
                                }
                                items.Add(item0);
                                break;
                            }
                            if (test0.Count() == 6)
                            {
                                var item0 = new Item
                                {
                                    Sku = GetSku(test1),
                                    Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace(",", ""),
                                    Precio = "1",
                                    Descripcion = "",
                                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                                };
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success && test0[test0.Length - 4].Equals("UN"))
                                {
                                    item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                    item0.Precio = "1";
                                }
                                if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                                {
                                    item0.Cantidad = "1";
                                } 
                                items.Add(item0);
                                break;

                            }
                        }
                        else

                        if (test0.Count() < 5)
                        {
                            test0 = pdfLines[i - 2].Split(' ');
                        }
                        if (test0.Count() == 11)
                        {
                            var item0 = new Item
                            {
                                Sku = GetSku(test1),
                                Cantidad = test0[test0.Length - 6].Replace(".00", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 5].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 6].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 4].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                            {
                                item0.Cantidad = "1";
                            }
                            items.Add(item0);
                            break;
                        }
                        if (test0.Count() == 10)
                        {
                            var item0 = new Item
                            {
                                Sku = GetSku(test1),
                                Cantidad = test0[test0.Length - 4].Replace(".00", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 4].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                            {
                                item0.Cantidad = "1";
                            }
                            items.Add(item0);
                            break;
                        }
                        if (test0.Count() == 9)
                        {
                            var item0 = new Item
                            {
                                Sku = GetSku(test1),
                                Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace(",", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 2].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 3].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                            {
                                item0.Cantidad = "1";
                            }
                            items.Add(item0);
                            break;
                        }
                        if (test0.Count() == 8)
                        {
                            var item0 = new Item
                            {
                                Sku = GetSku(test1),
                                Cantidad = test0[test0.Length - 6].Replace(".00", ""),
                                Precio ="1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length-4].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                            {
                                item0.Cantidad = "1";
                            }
                            items.Add(item0);
                            break;
                        }
                        if (test0.Count() == 7)
                        {
                            var item0 = new Item
                            {
                                Sku = GetSku(test1),
                                Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace(",", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success || test0[test0.Length - 3].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                            {
                                item0.Cantidad = "1";
                            }
                            items.Add(item0);
                            break;
                        }
                        if (test0.Count() == 6)
                        {
                            var item0 = new Item
                            {
                                Sku = GetSku(test1),
                                Cantidad = test0[test0.Length - 4].Replace(".00", "").Replace(",", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success && test0[test0.Length - 3].Equals("UN"))
                            {
                                item0.Cantidad = test0[test0.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item0.Precio = "1";
                            }
                            if (!Regex.Match(item0.Cantidad, @"\d{1,}").Success)
                            {
                                item0.Cantidad = "1";
                            }
                            items.Add(item0);
                            break;
                        }
                            break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test3 = pdfLines[i - 1].Split(' ');
                        var test4 = pdfLines[i].Split(' ');

                        var item1 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test3[test3.Length - 6].Replace(".00", "").Replace(",", ""),
                            Precio = test3[test3.Length - 4].DeleteDotComa(),
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO

                        };
                        if (!Regex.Match(item1.Cantidad, @"\d{1,}").Success)
                        {
                            item1.Cantidad = "1";
                        }
                        items.Add(item1);
                        break;
                    case 2:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test5 = pdfLines[i - 1].Split(' ');
                        var test6 = pdfLines[i].Split(' ');
                        if (test5.Count() < 5)
                        {
                            test5 = pdfLines[i - 2].Split(' ');
                        }
                        if (test5.Count() == 11)
                        {
                            var item2 = new Item
                            {
                                Sku = GetSku(test6),
                                Cantidad = test5[test5.Length - 6].Replace(".00", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success)
                            {
                                item2.Cantidad = test5[test5.Length - 6].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item2.Precio = "1";
                            }
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success)
                            {
                                item2.Cantidad = "1";
                            }
                            items.Add(item2);
                            break;
                        }
                        if (test5.Count() == 9)
                        {
                            var item2 = new Item
                            {
                                Sku = test6[test6.Length -1],
                                Cantidad = test5[test5.Length - 5].Replace(".00", "").Replace(",", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success && test5[test5.Length - 3].Equals("UN"))
                            {
                                item2.Cantidad = test5[test5.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item2.Precio = "1";
                            }
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success && test5[test5.Length - 2].Equals("UN"))
                            {
                                item2.Cantidad = test5[test5.Length - 3].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item2.Precio = "1";
                            }
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success)
                            {
                                item2.Cantidad = "1";
                            }
                            items.Add(item2);
                            break;
                        }
                        if (test5.Count() == 8)
                        {
                            var item2 = new Item
                            {
                                Sku = GetSku(test6),
                                Cantidad = test5[test5.Length - 6].Replace(".00", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success && test5[test5.Length - 4].Equals("UN"))
                            {
                                item2.Cantidad = test5[test5.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item2.Precio = "1";
                            }
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success && test5[test5.Length - 3].Equals("UN"))
                            {
                                item2.Cantidad = test5[test5.Length - 4].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item2.Precio = "1";
                            }
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success)
                            {
                                item2.Cantidad = "1";
                            }
                            items.Add(item2);
                            break;
                        }
                        if (test5.Count() == 7)
                        {
                            var item2 = new Item
                            {
                                Sku = GetSku(test6),
                                Cantidad = test5[test5.Length - 4].Replace(".00", "").Replace(",", ""),
                                Precio = "1",
                                Descripcion = "",
                                TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                                TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING

                            };
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success && test5[test5.Length - 3].Equals("UN"))
                            {
                                item2.Cantidad = test5[test5.Length - 5].Replace(".00", "").Replace("UN", "").Replace(",", "");
                                item2.Precio = "1";
                            }
                            if (!Regex.Match(item2.Cantidad, @"\d{1,}").Success)
                            {
                                item2.Cantidad = "1";
                            }
                            items.Add(item2);
                            break;
                        }
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }



        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str, string str2)
        {
            var aux = Regex.Split(str, @"[aA-zZ]{1,}$");
            var aux2 = Regex.Split(str2, @"^\d{1,}");
            var auxiliar = aux.ArrayToString(0, 1);
            var auxiliar2 = aux2.ArrayToString(0, 1);
            if (auxiliar2.Equals(""))
            {
                return auxiliar;
            }else
            return auxiliar2;
        }

        private static string getDireccion(string str, string str2)
        {
            var aux = Regex.Split(str, @"\d{1,}$");
            var aux2 = Regex.Split(str2, @"^[aA-zZ]{1,}");
            var aux3 = "";
            if (aux2.Count() <= 2)
            {
                aux3 = aux2[1];
            }
            else
            {
                aux3 = aux2[aux2.Length - 1].Split(' ')[1];
            }
            if (Regex.Match(aux3, @"[aA-zZ]{1,}\d{1,}").Success)
            {
                aux2 = Regex.Split(aux3, @"^[aA-zZ]{1,}");
            }else
            {
                aux2 = aux2;
            }
            var auxiliar = aux.ArrayToString(0, 1);
            var auxiliar2 = aux2.ArrayToString(0, 1);
            return auxiliar + " " + auxiliar2;
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ', '-');
            return split[1].Trim().DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }



        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private static string GetCantidad(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 4].Trim();
        }
        private static string GetPrecio(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 3].Trim().DeleteDotComa();
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }
        private bool IsDirectionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }

        #endregion
    }
}
