﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.ConstructoraIngevec
{
    public class ConstructoraIngevec
    {
        #region Variables

        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[a-zA-Z]{4}\d{4}"}//\s|\w{4}\d{4}\w" },//MBMA0905Lapiz
            //{1, @"\w{4}\d{4}\w{1,}" }//MBMA0905Lapiz
        };

        private readonly string _skuPattern = @"[a-zA-Z]{1,2}\d{5,6}";
        private const string RutPattern = "Rut: ";
        private const string OrdenCompraPattern = "Orden de Compra Nº";
        private const string OrdenCompraPattern2 = "Nº";
        private const string ItemsHeaderPattern =
            "Items Producto Unidad Cantidad Precio"; 

        private const string CentroCostoPattern = "Obra Nº ";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        public ConstructoraIngevec(PDFReader pdfReader)
        {     
            _pdfReader = pdfReader;
            _pdfLines = pdfReader.ExtractTextFromPdfToArrayDefaultMode();
            
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Get

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                //Console.WriteLine($"{str} ==>Match:{it.Value} ; R: {Regex.Match(str, it.Value).Success}; INDEX: {Regex.Match(str, it.Value).Index}\n{str.Substring(Regex.Match(str, it.Value).Index)}\n\n");
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, ret:{ret}");
            return ret;
        }

        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra();
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]) || IsOrdenCompraPattern2(_pdfLines[i]))
                    {
                        if (IsOrdenCompraPattern(_pdfLines[i])) {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        } else if (IsOrdenCompraPattern2(_pdfLines[i])) {
                            OrdenCompra.NumeroCompra = GetOrdenCompra2(_pdfLines[i]);
                        }
                        _readOrdenCompra = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = int.Parse(GetCentroCosto(_pdfLines[i])).ToString();
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE;
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(IList<string> pdfLines, int i)
        {
            var items = new List<Item>();
            var lastCodigoCliente = "LAST";
            var lastPrecio = "-1,000";
            for (; i < pdfLines.Count; i++)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                var test00 = aux.Split(' ');
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                if (optItem != -1)
                {
                    if (aux.Contains(lastCodigoCliente) && lastPrecio.Equals(test00[test00.Length - 2].Split(',')[0].Replace(".", ""))) optItem = -1;
                }
                switch (optItem)
                {
                    case 0:
                        lastCodigoCliente = GetlastFind(aux);
                        var test0 = aux.Split(' ');
                        lastPrecio = test0[test0.Length - 2].Split(',')[0].Replace(".", "");
                        var item0 = new Item
                        {
                            Sku = "",
                            Cantidad = test0[test0.Length - 3].Split(',')[0].Replace(".", ""),
                            Precio = test0[test0.Length - 2].Split(',')[0].Replace(".", ""),
                            TipoPrecioProducto = TipoPrecioProducto.ARCHIVO_ADJUNTO
                            
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        var concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i + 2; j < pdfLines.Count && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        Match matchdimerc = Regex.Match(item0.Sku, @"[aA-zZ]{1,2}\d{5,6}");
                        if (!matchdimerc.Success)
                        {
                            item0.Sku = GetSku2(pdfLines, i);
                            item0.TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE;
                        }
                        
                        Console.WriteLine(item0);
                        items.Add(item0);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku2(IList<string> pdflines,int i )
        {
            var ret = "Z446482";
            var contador = i;
            for (;contador<pdflines.Count;contador++)
            {
                Match match = Regex.Match(pdflines[contador], @"[a-zA-Z]{4}\d{4}");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }
            }
            return ret;
        }
        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str2 = test1.ArrayToString(0).Replace(" ", "");
                var str = str2;//test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret.ToUpper();
        }


        private string GetlastFind(string str)
        {
            str = str.Replace(" ", "");
            var sku1 = @"[a-zA-Z]{4}\d{4}";
            var sku2 = @"[a-zA-Z]{4}\d{4}";
            int index = 0, length = 0;
            if (Regex.Match(str, sku1).Success)
            {
                index = Regex.Match(str, sku1).Index;
                length = Regex.Match(str, sku1).Length;
            }
            else if (Regex.Match(str, sku2).Success)
            {
                index = Regex.Match(str, sku2).Index;
                length = Regex.Match(str, sku2).Length;
            }
            return str.Substring(index, length).Trim().Replace(" ", "");
        }


        private string GetSku(string str)
        {
            str = str.Replace(" ", "");
            var sku1 = @"[A-Z]{1,2}\d{5,6}";
            var sku2 = @"[A-Z]{1,2}\s\d{5,6}";
            int index = 0, length = 0;
            if (Regex.Match(str, sku1).Success)
            {
                index = Regex.Match(str, sku1).Index;
                length = Regex.Match(str, sku1).Length;
            }else if (Regex.Match(str, sku2).Success)
            {
                index = Regex.Match(str, sku2).Index;
                length = Regex.Match(str, sku2).Length;
            }
            return str.Substring(index, length).Trim().Replace(" ","");
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var index = Regex.Match(str, @"\d{4}").Index;
            return str.Substring(index,4);
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var index = Regex.Match(str, @"\d{6}").Index;
            return str.Substring(index, 6);
        }
        private static string GetOrdenCompra2(string str)
        {
            var split = str.Split(' ');
            //var index = Regex.Match(str, @"\d{6}").Index;
            return split[split.Length-1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(' ')[1];
            return aux;
        }

        #endregion


        #region Funciones Is

        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsSkuPatterns(string str)
        {
            var str2 = str.Replace(" ", "");
            return Regex.Match(str2, _skuPattern).Success;
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }

        private bool IsOrdenCompraPattern2(string str)
        {
            bool respuesta = false;
            var split = str.Split(' ');
            if (split[0].Equals(OrdenCompraPattern2))
            {
                respuesta = true;
            }
            return respuesta;
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}