﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Arauco
{
    class Arauco
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[a-zA-Z]{1,2}\d{5,6}\s" },            
            {1, @"\s\d{18}$"},  //Código\sde\smaterial\sArauco
            {2,@"\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}$" },
            {3,@"^Código\s[a-zA-Z]{2}\s[a-zA-Z]{8}\s[a-zA-Z]{1,}\s$" },
            {4, @"^\d{1,}\s\w{3}\d{5,6}\s\d{3,}\s\d{1,}\s\d{1,}"},
            {5,@"\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}Maderas Arauco SA" }

            //000000000000040951
            //D91 100 01/08/2016 16:00 1.929,00 192.900,00
        };

        private readonly Dictionary<int, string> _detalleItemsPatterns = new Dictionary<int, string>
        {
            {0,@"\w{2,}\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}\s\d{1,}$" }
        };
        private const string RutPattern = "Rut";
        //private const string RutPattern = "R . u . t . :";
        private const string OrdenCompraPattern = "Pedido de compra:";
        private const string ItemsHeaderPattern =
            "Fecha para la que se requiere Precio Subtotal";//"Unitario Precio Total";// Plazo Entrega

        private const string CentroCostoPattern = "EXPEDIR TODOS LOS ARTÍCULOS";
        private const string ObservacionesPattern = "EXPEDIR TODOS LOS ARTÍCULOS";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        #endregion
        private global::Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        public Arauco(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        #region Funciones Get
        public global::Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new global::Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoIntegracion = Utils.OrdenCompra.Integracion.TipoIntegracion.PDF,
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE

            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                //Console.WriteLine("    -----" + _pdfLines[i]);
                if (!_readOrdenCompra)
                {

                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        Console.WriteLine("    RUT" + _pdfLines[i]);
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }

               /* if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        var contadorCc = i + 3;
                        //OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.CentroCosto = _pdfLines[contadorCc].DeleteContoniousWhiteSpace().Split(' ')[0];
                        _readCentroCosto = true;
                    }
                }*/
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var contadorObs = i + 2;
                        var obsAux = "";
                       /* for(var x = i+3; x < i + 10 && !_pdfLines[x].Contains("Cond. Entrega:"); x++)
                        {
                            if (_pdfLines[x].Contains("documentos guía de despacho o factura")) continue;
                            if (_pdfLines[x].Contains("ejecutivo-cedible")) continue;
                            if (_pdfLines[x].Contains("ejecutivo-cedible")) continue;
                            obsAux += $" {_pdfLines[x]}";
                        }*/
                        OrdenCompra.Observaciones = _pdfLines[contadorObs].DeleteContoniousWhiteSpace();
                        _readObs = true;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length ; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                //Console.WriteLine("OPTITEM " + optItem);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("====================CASE 0====================");
                        var test0 = aux.Split(' ');
                        var sku = GetSku(pdfLines, i);
                        var cantidad = GetCantidad(pdfLines,i);
                        var precio = GetPrecio(pdfLines, i);

                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = cantidad,
                            Precio = precio,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING                            
                            
                        };
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine("====================CASE 1====================");
                        var previous = pdfLines[i-2].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i -1; j > 0; j--)
                        {
                            if (GetFormatDetalleItemsPattern(pdfLines[j]) == -1) continue;
                            previous = pdfLines[j];
                            break;
                        }

                        var pTest1 = previous.Split(' ');
                        var test1 = aux.Split(' ');
                        var item1 = new Item
                        {
                            Sku = int.Parse(test1[test1.Length - 1]).ToString(), //pTest2[pTest2.Length -1].Trim()
                            Cantidad = pTest1[pTest1.Length - 5],
                            Precio = pTest1[pTest1.Length - 2].Replace(".", "").Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        items.Add(item1);
                        break;
                    case 2:
                        Console.WriteLine("====================CASE 2====================");
                        var next = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //Código material
                        ///AGREGADO POR MZAPATA 02/02/2017
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(next) == -1; j++)
                        {
                            if(next.Contains("Código material"))
                            {
                                break;
                            }
                            next = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        ////
                        var nextSplit = next.Split(' ');
                        var test2 = aux.Split(' ');
                        var item2 = new Item
                        {
                            Sku = int.Parse(nextSplit[nextSplit.Length - 1]).ToString(),
                            Cantidad = test2[test2.Length - 4],
                            Precio = test2[test2.Length - 2].Replace(".", "").Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE
                        };
                        items.Add(item2);
                        break;
                    case 3:
                        Console.WriteLine("====================CASE 3====================");
                        var prev1 = pdfLines[i - 1].Trim().DeleteContoniousWhiteSpace();
                        var prev2 = pdfLines[i - 4].Trim().DeleteContoniousWhiteSpace();
                        var prev1Split = prev1.Split(' ');
                        var prev2Split = prev2.Split(' ');
                        var item3 = new Item
                        {
                            Sku = prev2Split[prev2Split.Length -1].Trim(),
                            Cantidad = prev1Split[prev1Split.Length -5].Trim(),
                            Precio = prev1Split[prev1Split.Length - 2].Trim().Replace(".","").Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        items.Add(item3);
                        break;
                    case 4:
                        Console.WriteLine("====================CASE 4====================");
                        var aux10 = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                        var next1 = "";
                        var test9 = aux10.Split(' ');
                        for (var j = i + 1; j < pdfLines.Length; j++)
                        {
                            if (GetFormatDetalleItemsPattern(pdfLines[j]) == -1) continue;
                            next1 = pdfLines[j];
                            break;
                        }
                        var test10 = next1.Split(' ');
                        var item4 = new Item
                        {
                            Sku = test9[test9.Length - 1].Trim(),
                            Cantidad = test10[test10.Length - 5].Trim(),
                            Precio = test10[test10.Length - 2].Trim().Replace(".", "").Split(',')[0],
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO
                        };
                        items.Add(item4);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "0";
            var contador = i;
            var aux = pdfLines[contador].DeleteContoniousWhiteSpace().Split(' ');
            ret = aux[aux.Length - 2].Replace("$", "");
            return ret;
        }

        private string GetCantidad(string[] pdfLines, int i)
        {
            var ret = "0";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\d{1,}\,");
                if (match.Success)
                {
                    ret = match.Value.Trim().Split(',')[0];
                    break;
                }
            }
            return ret;
            
        }

        private string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            Match match = Regex.Match(pdfLines[i], _itemsPatterns[0]);
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;
        }




        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            return aux[1].Trim();
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            return str.Trim().Split(':')[1].Trim();//str;
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 1].Trim().Replace(".","").Split('-')[0];
            
            //return str.Split(':')[1];
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                return it.Key;
            }
            return ret;
        }

         private int GetFormatDetalleItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(".", "").Replace(",", "").Replace(":","").Replace("/","");
            foreach (var it in _detalleItemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
                return ret;
            }
            return ret;
        }

        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.ToLower().Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern.ToLower());
        }

        private bool IsObservacionPattern(string str)
        {
            return str.ToLower().Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern.ToLower());
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.ToLower().Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern.ToLower());
        }
        private bool IsRutPattern(string str)
        {
            return str.Replace(".","").ToLower().Trim().DeleteContoniousWhiteSpace().Contains(RutPattern.Replace(".", "").ToLower());
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.ToLower().Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern.ToLower());
        }

        #endregion

    }
}