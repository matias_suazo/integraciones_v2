﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Armas
{
    public class ConstructoraArmasLtda
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"^\d{1,}\s\d{1,}\s\d{3}\s\d{1,}$" },
            {1, @"[P|C]\d{9}\s(\w{1,}\s)"}
            //{1, @"\sP\d{9}\s(\w{1,}\s)*\d{1,}\s\d{1,}\s\d{3}\s\d{1,}$"}
        };
        private const string RutPattern = "Rut :";
        private const string OrdenCompraPattern = "ORDEN DE COMPRA N";
        private const string ItemsHeaderPattern =
            "PARTIDA CÓDIGO DETALLE/DESCRIPCIÓN UNIDAD CANT DESCUENTO";
        private const string ItemsHeaderPattern2 =
            "PARTIDA CÓDIGO DETALLE / DESCRIPCIÓN ";

        private const string CentroCostoPattern = "Entregar en :";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public ConstructoraArmasLtda(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_LIKE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        //_readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.Observaciones += $"Centro de Costo: {GetCentroCosto(_pdfLines[i])}, ";
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].Trim().DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                var concatAll = "";
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine("==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        var test01 = aux.Split(' ');
                        aux = pdfLines[i + 2].Trim().DeleteContoniousWhiteSpace();
                        var test02 = aux.Split(' ');
                        var item0 = new Item
                        {
                            Sku = "Z446482",
                            Cantidad = test0[0].Split(',')[0],
                            Precio = test0[test0.Length - 3].Split(',')[0],
                            Descripcion = $"{test01.ArrayToString(4, test01.Length - 2)} {test02.ArrayToString(1, test02.Length - 1)}".ToUpper().DeleteAcent(),
                            TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE
                        };
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        concatAll = "";
                        aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        {
                            concatAll += $" {aux}";
                            aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        }
                        item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine("==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        var item1 = new Item
                        {
                            Sku = test1[0],
                            Cantidad = test1[test1.Length - 5].Split(',')[0],
                            Precio = test1[test1.Length - 3].Split(',')[0],
                            Descripcion =""
                            //    $"{test1.ArrayToString(4, test1.Length - 5)} {test11.ArrayToString(1, test11.Length - 1)}".DeleteAcent().ToUpper(),
                            //TipoPareoProducto = TipoPareoProducto.PAREO_AUTOMATICO_DESCRIPCION_CODIGO_CLIENTE
                        };
                        items.Add(item1);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var str = test1.ArrayToString(0, test1.Length - 1);
            if (!Regex.Match(str, @"(P|C)\.\d{2}\.\d{2}\.\d{2}\.\d{3}").Success) return ret;
            var index = Regex.Match(str, @"(P|C)\.\d{2}\.\d{2}\.\d{2}\.\d{3}").Index;
            var length = Regex.Match(str, @"(P|C)\.\d{2}\.\d{2}\.\d{2}\.\d{3}").Length;
            ret = str.Substring(index, length).Trim();
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            var split = aux[1].DeleteContoniousWhiteSpace().Split(' ');
            var ret = split.ArrayToString(0, split.Length - 2).Split(' ');
            //return ret;
            var retu = "";
            if (Regex.Match(ret[ret.Length - 1], @"^\d{2}/\d{2}/\d{4}$").Success)
            {
                var c = ret.Length - 2;
                retu = ret.ArrayToString(0, c);
            }
            else retu = ret.ArrayToString(0);
            return retu;
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            return split[split.Length - 1].DeleteContoniousWhiteSpace();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            str = str.Replace(".", "").Replace(",", "").Replace(")", "").Replace("(", "");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}