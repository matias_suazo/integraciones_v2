﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Consorcio
{
    class ConsorcioSeguros
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s[a-zA-Z]{1,2}\d{4,6}"},
            {1, @"\d{1,}\s\d{5,}[|]" },
            {2, @"^[a-zA-Z]{1,2}\d{5,6}" },
            {3, @"^\d{1,}\s[a-zA-Z]{1,2}\d{4,6}\s\d{1,}$" },
            {4, @"[a-zA-Z]{1,}\s[a-zA-Z]{1,2}\d{5,6}\s\d{1,}\s\d{1,}$"},//L399622 128 2560
            //{5, @"[A-Z]{1,}\d{5,6}\s\$\s\d{1,}" } //Z390804 $ 
            { 5,@"^\d{1,}\s[A-Z]{1,}\s"}
        };
        private const string RutPattern = "DIMERC S.A.";
        private const string OrdenCompraPattern = "N.º DE ORDEN";
        private const string ItemsHeaderPattern = "LINEA DESCRIPCIÓN CANT. IMPORTE";
        //private const string CentroCostoPattern = "Dirección de Entrega";
        private const string ObservacionesPattern = "Datos Comprador:";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public ConsorcioSeguros(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i + 1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i+1]);
                        //if (!OrdenCompra.Rut.Contains(@"\d{8}"))
                        //{
                        //    OrdenCompra.Rut = GetRut(_pdfLines[i+1]);
                        //}
                        _readRut = true;
                    }
                }

                /*if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i + 3]);
                        //OrdenCompra.CentroCosto = GetCencos(_pdfLines, i);
                        _readCentroCosto = true;
                    }
                }*/
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Observaciones +=
                            $"{_pdfLines[i+1].Trim().DeleteContoniousWhiteSpace()}";
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            codigos = GetCodigos(pdfLines, i);
            cantidades = GetCantidades(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < codigos.Count; cont++)
            {
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();
            
            return items;
        }


        private string GetSku(string[] pdfLines, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"[A-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value;
                    break;
                }

            }

            return ret;
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL NETO"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    listaCodigos.Add(match.Value);
                }

                Match match1 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{5,}[|]");
                if (match1.Success)
                {
                    listaCodigos.Add(match1.Value.Split('|',' ')[1]);
                }
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL NETO"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(linea, @"\d{1,}\s\d{1,},\d{1,}\s.\s|\d{1,},\d{1,}\s\d{1,},\d{1,}\s.\s");
                Match match1 = Regex.Match(linea, @"\d{1,}\s\d{1,}\s.\s|\d{1,},\d{1,}\s\d{1,}\s.\s");
                if (match.Success)
                {
                    listaCantidades.Add(match.Value.Split(' ')[0].Trim().DeleteDotComa());
                }
                if (match1.Success)
                {
                    listaCantidades.Add(match1.Value.Split(' ')[0].Trim().DeleteDotComa());
                }
            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }



        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(' ');
            var dflt = aux[aux.Length - 1].ToUpper().Trim().DeleteDotComa();
            return dflt;


        }

        private string GetCencos(string[] pdfLines, int i)
        {
            var ret = "0";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                Match match = Regex.Match(pdfLines[contador], @"\d{1,}\-\d{3}-\d{2}");
                if (match.Success)
                {
                    ret = match.Value.Trim();
                    break;
                }
            }
            return ret;
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[0].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ', '-');
            return split[0].Trim().DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.Replace(".","").Replace("$","");
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        /*private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }*/

        #endregion
    }
}
