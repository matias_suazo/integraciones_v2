﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using Org.BouncyCastle.Asn1.Ocsp;
using Quartz.Util;

namespace Integraciones.Utils.Integracion.Pdf.FedexNuevoFormato
{
    class FedexNuevoFormato
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s\d{7,}\s\w{1,}"}

        };
        private const string RutPattern = "GST:";
        private const string OrdenCompraPattern = "Page#";
        private const string ItemsHeaderPattern = "Line-SCH Vendor Item Description";
        private const string ItemsHeaderPattern2 = "Buyer Email";
        private const string CentroCostoPattern = "Requester";
        private const string ObservacionesPattern = "Buyer:";
        private const string DireccionPattern = "Ship To:";
        private const string CentroCostoPattern2 = "Ship To:";
        private const string shipCC = "";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readDireccion;
        private bool _readItem;
        private bool _readObserv;
        private bool _readRequest;
        private OrdenCompra.OrdenCompra ret;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public FedexNuevoFormato(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                Rut = "76754296",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_RE_DDESCLI,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i + 1]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }


                if (!_readDireccion)
                {
                    if (IsDireccionPattern(_pdfLines[i]))
                    {
                        var direc = _pdfLines[i].Split(':');
                        OrdenCompra.Direccion = direc[2].Trim();
                        _readDireccion = true;
                    }
                }

                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern2(_pdfLines[i]))
                    {
                        
                        string[] codCC = new string[5];
                        for (var j = i; j < _pdfLines.Length; j++)
                        {

                            if (Regex.Match(_pdfLines[j], @"\s\d{5,}\s\w{1,}").Success)
                            {
                                var ship = Regex.Match(_pdfLines[j], @"\s\d{5,}\s\w{1,}").Value;
                                if (ship.IsNullOrWhiteSpace())
                                {
                                    continue;
                                }
                                codCC[0] = ship.TrimStart().TrimEnd().Split(' ')[0];
                                //codCC[1] = ship.TrimStart().TrimEnd().Split(' ')[1];

                            }
                            if (_pdfLines[j].Contains("Requester:"))
                            {
                                codCC[1] = _pdfLines[j].TrimStart().TrimEnd().Split(' ')[1];
                                codCC[2] = _pdfLines[j].TrimStart().TrimEnd().Split(' ')[2];
                                _readRequest = true;
                            }
                            if (_readRequest)
                            {
                                OrdenCompra.CentroCosto = codCC[0].ToUpper() + " " + codCC[1].ToUpper() + " " + codCC[2].ToUpper();
                                OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_RE_DDESCLI;
                                if (OrdenCompra.CentroCosto.Contains("99999"))
                                {
                                    OrdenCompra.CentroCosto = "1";
                                    OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                    _readCentroCosto = true;
                                    break;
                                }
                                _readCentroCosto = true;
                                break;
                            }
                            //lectura cc antigua
                            /*
                                //if (IsCentroCostoPattern(_pdfLines[j]))
                                //{
                                //var cenco2 = OrdenCompra.CentroCosto;
                                //var requester = _pdfLines[j].Split(':')[1].Replace("Bill To", "").Trim().TrimEnd().TrimStart();
                                //OrdenCompra.CentroCosto = cenco2.Trim().TrimEnd().TrimStart() + " " + requester.Trim().TrimEnd().TrimStart();
                                //OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_RE_DDESCLI;
                                //_readCentroCosto = true;
                                //}

                                ////var cenco = _pdfLines[j + 2].Split(' ');
                                ////OrdenCompra.CentroCosto = (cenco[3], cenco[cenco.Length - 1]).ToString().Replace(",", "").Replace("(", "").Replace(")", "");
                                //var match = Regex.Match(OrdenCompra.CentroCosto, @"\d{1,}");
                                //var match1 = Regex.Match(_pdfLines[j], @"\s\d{5,}\s[aA-zZ]{1,}");

                                //OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_RE_DDESCLI;
                                //var cenco1 = (match1.Value);
                                //OrdenCompra.CentroCosto = cenco1;
                                //if (!match1.Success)
                                //{
                                //    continue;
                                //}
                                //if (match1.Success && !match1.Value.Contains("Ship"))
                                //{
                                //    if (OrdenCompra.CentroCosto.Contains("8330749"))
                                //    {
                                //        OrdenCompra.CentroCosto = "4";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        break;
                                //    }
                                //    if (OrdenCompra.CentroCosto.Contains("4930359"))
                                //    {
                                //        OrdenCompra.CentroCosto = "116";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        break;
                                //    }
                                //    if (OrdenCompra.CentroCosto.Contains("8380554"))
                                //    {
                                //        OrdenCompra.CentroCosto = "164";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        break;
                                //    }
                                //    if (OrdenCompra.CentroCosto.Contains("99999"))
                                //    {
                                //        OrdenCompra.CentroCosto = "1";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        break;
                                //    }
                                //    if (OrdenCompra.CentroCosto.Contains("8060103") || OrdenCompra.CentroCosto.Contains("8060114") || OrdenCompra.CentroCosto.Contains("8060703")
                                //        || OrdenCompra.CentroCosto.Contains("4051145") || OrdenCompra.CentroCosto.Contains("9030868"))
                                //    {
                                //        _readCentroCosto = false;
                                //        break;
                                //    }  
                                //    if (OrdenCompra.CentroCosto.Contains("8550001"))
                                //    {
                                //        OrdenCompra.CentroCosto = "18";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        _readCentroCosto = true;
                                //        break;
                                //    }
                                //    if (OrdenCompra.CentroCosto.Contains("5290000"))
                                //    {
                                //        OrdenCompra.CentroCosto = "105";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        _readCentroCosto = true;
                                //        break;
                                //    }
                                //    if (OrdenCompra.CentroCosto.Contains("3780000"))
                                //    {
                                //        OrdenCompra.CentroCosto = "123";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        _readCentroCosto = true;
                                //        break;
                                //    }
                                //}
                                //if (!match.Success)
                                //{
                                //    var cencomatch = _pdfLines[j + 3].Split(' ');
                                //    OrdenCompra.CentroCosto = (cencomatch[2], cencomatch[cencomatch.Length - 1]).ToString().Replace(",", "").Replace("(", "").Replace(")", "");
                                //    OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_RE_DDESCLI;
                                //    if (OrdenCompra.CentroCosto.Equals("Parque AK"))
                                //    {
                                //        OrdenCompra.CentroCosto = "134";
                                //        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                                //        _readCentroCosto = true;
                                //        break;
                                //}
                                //}
                            */
                        }
                        continue;
                    }
                }
                if (!_readObserv)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        var obs = _pdfLines[i].Split(':');
                        OrdenCompra.Observaciones = obs[obs.Length - 1];
                        _readObserv = true;
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            if (OrdenCompra.CentroCosto.Equals(""))
            {
                OrdenCompra.CentroCosto = "0";
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            codigos = GetCodigos(pdfLines, i);
            cantidades = GetCantidades(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < codigos.Count; cont++)
            {
                if (cantidades[cont].Equals("Unit"))
                {
                    continue;
                }
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                };
                items.Add(itemSe);

            }
            Console.WriteLine();
            return items;
        }
        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].ToUpper().Contains("TOTAL PO AMOUNT"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"\s[A-Z]{1}\d{6}\s");
                Match match2 = Regex.Match(pdfLines[contador], @"\s[A-Z]{1}\d{6}");

                if (match.Success || match2.Success)
                {
                    if (listaCodigos.Contains(match.Value) | listaCodigos.Contains(match2.Value))
                    {
                        continue;
                    }
                    if (match.Value.Equals(""))
                    {
                        listaCodigos.Add(match2.Value);
                    }
                    else
                    {
                        listaCodigos.Add(match.Value);
                    }

                }

                Match match1 = Regex.Match(pdfLines[contador], @"\s\d{6}\s");
                if (match1.Success)
                {
                    listaCodigos.Add(match1.Value.Split(' ')[1].Replace(",", ""));
                }
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL PO AMOUNT"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match = Regex.Match(linea, @"\s\d{2,}-\w{1,}-\d{4}\s");
                if (match.Success)
                {
                    var cantidades = pdfLines[contador].Split(' ');
                    var cantFin = cantidades[cantidades.Length - 5].Split('.')[0];
                    Match matchCant = Regex.Match(cantFin, @"\d{1,}");
                    if (!matchCant.Success)
                    {
                        cantFin = cantidades[cantidades.Length - 4].Split('.')[0];
                    }
                    Match match1 = Regex.Match(cantFin, @"\d{2,}-\w{1,}-\d{4}");
                    if (match1.Success)
                    {
                        continue;
                    }
                    listaCantidades.Add(cantFin);
                }
                Match match2 = Regex.Match(linea, @"EA\s\d{2,}-\w{1,}-\s\d{1,}");
                Match match3 = Regex.Match(linea, @"PK\s\d{2,}-\w{1,}-\s\d{1,}");
                if (match2.Success || match3.Success)
                {
                    var cantidades = pdfLines[contador].Split(' ');
                    var cantFin = cantidades[cantidades.Length - 5].Split('.')[0];
                    Match matchCant = Regex.Match(cantFin, @"\d{1,}");
                    if (!matchCant.Success)
                    {
                        cantFin = cantidades[cantidades.Length - 4].Split('.')[0];
                    }
                    Match match1 = Regex.Match(cantFin, @"\d{2,}-\w{1,}-\d{4}");
                    if (match1.Success)
                    {
                        continue;
                    }
                    listaCantidades.Add(cantFin);
                }


            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL PO AMOUNT"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", " ");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", " "));
                }
            }

            return listaPrecios;
        }

        private string GetSku(string str)
        {
            var ret = "Z446482";
            Match match = Regex.Match(str, _itemsPatterns[0]);
            if (match.Success)
            {
                ret = match.Value;
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':');
            var cc = aux[1].Split(' ');
            return cc.ArrayToString(1, cc.Length - 1);
        }

        private static string GetCentroCosto2(string str, string str2)
        {
            //string ccFinal= str.ToUpper();
            string cc1 = str.Split(':', '-')[1].Trim();
            //string cc2 = str2.Split(':')[1].TrimEnd();

            string ccFinal = cc1; // + cc2;
            return ccFinal.ToUpper();
        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var aux = str.Split(' ');
            return aux[aux.Length - 6].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(':');
            split = split[1].Split('-');
            return split[0].Trim().DeleteDotComa();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] pdfLines, int i)
        {
            var ret = "-1";
            for (var nuevoContador = i; nuevoContador < pdfLines.Length; nuevoContador++)
            {
                if (pdfLines[nuevoContador].Contains("EA"))
                {
                    var aux2 = pdfLines[nuevoContador + 1].DeleteContoniousWhiteSpace();
                    var linea = aux2.Split(' ');
                    ret = linea[0].Replace(",", "");
                    break;
                }

            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        private bool IsDireccionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(DireccionPattern);
        }
        private bool IsCentroCostoPattern2(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern2);
        }
        #endregion
    }
}
