﻿using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Integraciones.Utils.Integracion.Pdf.Ingevec2
{
    class Ingevec2
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"\d{1,}\s[aA-zZ]{1,}\d{1,4}\s" },
            //{1, @"CL_[aA-zZ]{1,}\d{1,4}\s" }
        };
        private const string RutPattern = "Facturar a: ";
        private const string OrdenCompraPattern = "Orden de Compra";
        private const string ItemsHeaderPattern =
            "Codigo Descripción";
        private const string ItemsHeaderPattern2 =
            "Codigo Descripcion";

        private const string CentroCostoPattern = "Obra";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Ingevec2(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_DE_CLIENTE,
                TipoIntegracion = TipoIntegracion.PDF
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i-1]);
                        if (OrdenCompra.NumeroCompra.Equals(""))
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra2(_pdfLines[i].TrimEnd());
                        }
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        if (!_pdfLines[i+1].Contains(".") && !_pdfLines[i+1].Contains("-"))
                        {
                            OrdenCompra.Rut = GetRut(_pdfLines[i + 2]);
                            _readRut = true;
                        }
                        else
                        {
                            OrdenCompra.Rut = GetRut(_pdfLines[i + 1]);
                            _readRut = true;
                        }
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        if (_pdfLines[i].Equals("Obra"))
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto2(_pdfLines[i -1]);
                        }
                            if (OrdenCompra.CentroCosto.Equals("0"))
                        {
                            OrdenCompra.CentroCosto = GetCentroCosto2(_pdfLines[i]);
                        }
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            for (; i < pdfLines.Length; i++)
            //foreach(var str in pdfLines)
            {
                var aux = pdfLines[i].DeleteContoniousWhiteSpace();
                //Es una linea de Items 
                var optItem = GetFormatItemsPattern(aux);
                switch (optItem)
                {
                    case 0:
                        Console.WriteLine($"==================ITEM CASE 0=====================");
                        var test0 = aux.Split(' ');
                        var sku = GetSku(pdfLines, i);
                        if (test0.Count().Equals(3))
                        {
                            continue;
                        }
                        var item0 = new Item
                        {
                            Sku = sku,
                            Cantidad = test0[test0.Length - 4].Split(',')[0],
                            Precio = test0[test0.Length - 2],
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        if (test0[test0.Length - 4].Contains("."))
                        {
                            item0.Cantidad = test0[test0.Length - 4].Split('.')[0];
                        }else
                        if (test0[test0.Length - 2].Contains("."))
                        {
                            
                            item0.Precio = test0[test0.Length - 2].Split('.')[0];
                            
                        }
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //var sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //if (OracleDataAccess.ExistProduct(sku))
                        //{
                        //    item0.Sku = sku;
                        //    item0.TipoPareoProducto = TipoPareoProducto.SinPareo;
                        //}
                        if (!Regex.Match(item0.Cantidad, @"\d").Success)
                        {
                            continue;
                        }
                        if (item0.Cantidad.Contains(item0.Sku))
                        {
                            continue;
                        }
                        items.Add(item0);
                        break;
                    case 1:
                        Console.WriteLine($"==================ITEM CASE 1=====================");
                        var test1 = aux.Split(' ');
                        //var sku1 = GetSku(pdfLines, i);
                        var canti = pdfLines[i - 1].Split(' ');
                        if (test1.Count().Equals(3))
                        {
                            continue;
                        }
                        if (canti.Count().Equals(2))
                        {
                            continue;
                        }
                        var item1 = new Item
                        {
                            Sku = test1[2],
                            Cantidad = canti[canti.Length - 4].Split(',')[0],
                            Precio = "1",
                            Descripcion = "",
                            TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                            TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                        };
                        if (test1[test1.Length - 4].Contains("."))
                        {
                            item1.Cantidad = test1[test1.Length - 4].Split('.')[0];
                        }
                        else
                        if (test1[test1.Length - 2].Contains("."))
                        {

                            item1.Precio = test1[test1.Length - 2].Split('.')[0];

                        }
                        //Concatenar todo y Buscar por Patrones el SKU DIMERC
                        //var concatAll = "";
                        //aux = pdfLines[i + 1].Trim().DeleteContoniousWhiteSpace();
                        //for (var j = i + 2; j < pdfLines.Length && GetFormatItemsPattern(aux) == -1; j++)
                        //{
                        //    concatAll += $" {aux}";
                        //    aux = pdfLines[j].Trim().DeleteContoniousWhiteSpace();
                        //}
                        //item0.Sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //var sku = GetSku(concatAll.DeleteContoniousWhiteSpace().Split(' '));
                        //if (OracleDataAccess.ExistProduct(sku))
                        //{
                        //    item0.Sku = sku;
                        //    item0.TipoPareoProducto = TipoPareoProducto.SinPareo;
                        //}
                        if (!Regex.Match(item1.Cantidad, @"\d").Success)
                        {
                            continue;
                        }
                        if (item1.Cantidad.Contains(item1.Sku))
                        {
                            continue;
                        }
                        items.Add(item1);
                        break;
                }
            }
            //SumarIguales(items);
            return items;
        }

        private string GetSku(string[] pdfLInes, int i)
        {
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLInes.Length; contador++)
            {
                Match match = Regex.Match(pdfLInes[contador], @"[aA-zZ]{1,2}\d{5,6}");
                if (match.Success)
                {
                    ret = match.Value.Replace("_","").Replace("CL_","");
                    break;
                }
                Match match1 = Regex.Match(pdfLInes[contador], @"\d{5,6}");
                if (match1.Success)
                {
                    ret = match1.Value;
                    break;
                }
                Match match2 = Regex.Match(pdfLInes[contador], @"\d{5,6}");
                if (match1.Success)
                {
                    ret = match1.Value;
                    break;
                }
            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var ret = "0";
            var aux = str.Split('-');
            var cencos = aux[0].Trim();
            if (cencos.Length == 4)
            {
                var chararr = cencos.ToCharArray();
                if (chararr[0]=='0')
                {
                    char[] quitar = { '0' };
                    ret = cencos.TrimStart(quitar);
                }
            }
            return ret;
        }

        private static string GetCentroCosto2(string str)
        {
            var ret = "0";
            var aux = Regex.Match(str, @"\d{4}").Value;
            var cencos = aux.ToArray();
            if (cencos.Length == 1)
            {
                return str.Split('-')[0].Replace(" ", "");
            } 
            if(cencos.Length == 4)
            {
                var cencosF = cencos[1].ToString() + cencos[2].ToString() + cencos[3].ToString();
                return cencosF.ToString();
            }
            else
            {
                return "0";
            }
        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        private static string GetOrdenCompra2(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            return split[1].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern)||str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion
    }
}
