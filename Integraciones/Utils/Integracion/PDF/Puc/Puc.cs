﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.Email;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion
{
    public class Puc
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[a-zA-Z]{6}\d{4}"},
            {1, @"\s[a-zA-Z]{1,2}\d{4,6}\s" }
            //{2, @"" }


        };
        private const string RutPattern = "Rut:";
        private const string OrdenCompraPattern = "Orden de Compra"; 
        private const string OrdenCompraPattern2 = "ORDEN DE COMPRA Nº";
        private const string OrdenCompraPattern3 = "O/C Número:";
        private const string ItemsHeaderPattern =
            "Item Código Descripción UM Cantidad Precio Total";
        private const string ItemsHeaderPattern2 = "N° Codigo PUC Descripción UDM Cantidad Precio Unitario Valorizado";
        private const string ItemsHeaderPattern3 = "Item Código Descripción UM Cantidad Precio Valor Exento ? Total";
        private const string ItemsHeaderPattern4 = "Item Cantidad U. Medida";
        private const string ItemsHeaderPattern5 = "Código Cantidad Precio Total";
        private const string CentroCostoPattern = "Unidad:";
        private const string CentroCostoPattern2 = "Unidad Emisora:";
        private const string ObservacionesPattern = "Tienda :";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Puc(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new OrdenCompra.OrdenCompra
            {
                CentroCosto = "5900",
                TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_EXACTA,
                TipoIntegracion = TipoIntegracion.PDF,
                Rut = "81698900"
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        if (_pdfLines[i+2].Contains("UC-"))
                            {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i+2].Trim()).Replace("(","").Replace(")", "");
                            }else
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]).Replace("(", "").Replace(")", "").TrimEnd();

                        }
                        if (OrdenCompra.NumeroCompra.Contains("Compra") || OrdenCompra.NumeroCompra.Contains("Aprobada"))
                        {
                            continue;
                        }
                        _readOrdenCompra = true;
                    }
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        if (_pdfLines[i].Contains("UC-"))
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i].Trim()).Replace("(", "").Replace(")", "");
                        }
                        _readOrdenCompra = true;
                    }
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        if (_pdfLines[i].Contains("O/C"))
                        {
                            OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i].Trim()).Replace("(", "").Replace(")", "");
                        }
                        _readOrdenCompra = true;
                    }
                }
                /*if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]);
                        _readRut = true;
                    }
                }*/

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                    {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]);
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_DESCRIPCION_MATCH;
                        if (OrdenCompra.CentroCosto.Equals("5900"))
                        {
                            OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO;
                        }
                        _readCentroCosto = true;
                    }
                }
                //if (!_readObs)
                //{
                //    if (IsObservacionPattern(_pdfLines[i]))
                //    {
                //        OrdenCompra.Observaciones +=
                //            $"{_pdfLines[i].Trim().DeleteContoniousWhiteSpace()}, " +
                //            $"{_pdfLines[++i].Trim().DeleteContoniousWhiteSpace()}";
                //        _readObs = true;
                //        _readItem = false;
                //    }
                //}
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
            var items = new List<Item>();
            List<string> cantidades = new List<string>();
            List<string> precios = new List<string>();
            List<string> codigos = new List<string>();

            codigos = GetCodigos(pdfLines, i);
            cantidades = GetCantidades(pdfLines, i);
            precios = GetPrecios(pdfLines, i);

            for (var cont = 0; cont < codigos.Count; cont++)
            {
                //if (codigos[cont].Equals(@"[Aa-Zz]{2}\d{6}") && cantidades[cont].Equals(null))
                //{
                //    continue;
                //}
                //Match match = Regex.Match(codigos[cont], @"\d{6}");
                //if (match.Success)
                //{
                //    continue;
                //}
                var itemSe = new Item
                {
                    Sku = codigos[cont],
                    Cantidad = cantidades[cont],
                    Precio = "1",
                    TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                    TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    
                };
                if (itemSe.Sku.Contains("EG"))
                {
                    itemSe.TipoPareoProducto = TipoPareoProducto.PAREO_CODIGO_CLIENTE;
                }
                    items.Add(itemSe);

            }
            Console.WriteLine();
            return items;
        
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Sub Total") || pdfLines[contador].Contains("SUB TOTAL"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"[aA-zZ]{1,2}\d{6}");
                Match match1 = Regex.Match(pdfLines[contador], @"\s\d{3}\d{3}\s");
                if (match.Success && !listaCodigos.Contains(match.Value))
                {
                    listaCodigos.Add(match.Value);
                    continue;
                }else
                if (match1.Success)
                {
                    listaCodigos.Add(match1.Value.Split(' ')[1].Replace(",", ""));
                    continue;
                }
                else
                {
                    Match match2 = Regex.Match(pdfLines[contador], @"$[A-Z]{1,2}\d{4,}");
                    if (match2.Success)
                    {
                        Match match3 = Regex.Match(pdfLines[contador + 1], @"[A-Z]{1,2}\d{4,}");
                        Match match4 = Regex.Match(pdfLines[contador + 1], @"\s\d{6}");
                        if (match3.Success)
                        {
                            listaCodigos.Add(match3.Value);
                            continue;
                        }
                        if (match4.Success)
                        {
                            listaCodigos.Add(match4.Value.Split(' ')[1].Replace(",", ""));
                            continue;
                        }
                            else
                            {
                                listaCodigos.Add(match2.Value);
                            }
                        }
                }
                Match match5 = Regex.Match(pdfLines[contador], @"\d{1,}\s[A-Z]{1,2}\d{5}");
                if (match5.Success)
                {
                    listaCodigos.Add(match5.Value.Split(' ')[1].Replace(",", ""));
                    continue;
                }
                Match match6 = Regex.Match(pdfLines[contador], @"\d{1,}\s[A-Z]{5,}\d{4,}");
                if (match6.Success)
                {
                    listaCodigos.Add(match6.Value.Split(' ')[1].Replace(",", ""));
                    continue;
                }


                //Match match2 = Regex.Match(pdfLines[contador], @"[A-Z]{1,2}\d{4}");
                //if (match2.Success)
                //{
                //    listaCodigos.Add(match2.Value.Replace(",", ""));
                //}
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Sub Total") || pdfLines[contador].Contains("SUB TOTAL"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace().Trim();
                Match match = Regex.Match(linea, @"(\w[Unidad]\s\d{1,},\d{1,}\s)");
                Match matchMiles = Regex.Match(linea, @"(\w[Unidad]\s\d{1,}\d{1,},\d{1,}\s)");
                if (match.Success || matchMiles.Success)
                {
                    var cant = match.Value.Trim();
                    listaCantidades.Add(cant.Split(' ', ',')[1]);
                }
                Match match1 = Regex.Match(linea, @"([A-Z]{1,2}\d{5,6}\s\d{1,}\s)");
                if (match1.Success)
                {
                    listaCantidades.Add(match1.Value.Split(' ')[1].Trim());
                }
                Match match2 = Regex.Match(linea, @"(\w[PAQUETE]\s\d{1,},\d{1,}\s)");
                if (match2.Success)
                {
                    var cant = match2.Value.Trim();
                    listaCantidades.Add(cant.Split(' ', ',')[1]);
                }
                Match match3 = Regex.Match(linea, @"((UNIDAD)\s\d{1,}\s)");
                if (match3.Success && match3.Value.Contains("AD"))
                {
                    var cant = match3.Value.Trim();
                    listaCantidades.Add(cant.Split(' ', ',')[1]);
                }
                Match match4 = Regex.Match(linea, @"((UNIDAD)\s\d{1,}(.)\d{1,}\s)");
                if (match4.Success && match4.Value.Contains("1.000"))
                {
                    var cant = match4.Value.Trim();
                    listaCantidades.Add(cant.Split(' ', ',')[1]);
                }
                Match match5 = Regex.Match(linea, @"((Kilo)\s\d{1,}\s)");
                if (match5.Success && match5.Value.Contains("AD"))
                {
                    var cant = match5.Value.Trim();
                    listaCantidades.Add(cant.Split(' ', ',')[1]);
                }
                Match match6 = Regex.Match(linea, @"((Kilo)\s\d{1,}(.)\d{1,}\s)");
                if (match6.Success)
                {
                    var cant = match6.Value.Trim();
                    listaCantidades.Add(cant.Split(' ', ',')[1]);
                }
                Match match7 = Regex.Match(pdfLines[contador], @"\d{1,}\s[A-Z]{5,}\d{4,}");
                if (match7.Success)
                {
                    var cant = pdfLines[contador].Trim().Replace(",", "").Split(' ');
                    listaCantidades.Add(cant[cant.Length-4]);
                    continue;
                }
                Match match8 = Regex.Match(pdfLines[contador], @"\s\d{3}\d{3}\s");
                if (match8.Success)
                {
                    var line = pdfLines[contador].Split(' ');
                    var linesplit = line[1];
                    Match matchsplit = Regex.Match(linesplit, @"Unidad");
                    if (matchsplit.Success)
                    {
                        var cantidadnew = pdfLines[contador + 1].Split(' ');
                        listaCantidades.Add(cantidadnew[0]);
                    }
                    continue;
                }
                Match match10 = Regex.Match(pdfLines[contador-1], @"\s[A-Z]{1,2}\d{5,6}\s");
                if (match10.Success)
                {
                    var line = pdfLines[contador];
                    Match match11 = Regex.Match(line, @"\d{1,}\s\w{1}");
                    listaCantidades.Add(match11.Value.Split(' ')[0].Replace(",", ""));
                    continue;
                }
            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }
        private string GetSku(string test1)
        {
            var ret = "Z446482";
            Match match = Regex.Match(test1, @"[a-zA-Z]{1,2}\d{4,6}");
            if (match.Success)
            {
                ret = match.Value.Trim();

            }
            return ret;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var ret = "5900";
            Match match = Regex.Match(str, @"\d{4}\-\d{3}\-\d{2}");
            if (match.Success)
            {
                ret = match.Value.Split('-')[0];
                
            }else
                if (str.Contains("Unidad Emisora:"))
            {
                var cenco = str.Split(':');
                if (cenco[cenco.Length - 1].Contains("("))
                {
                    var cencoNuevo = cenco[cenco.Length - 1].ToString();
                    return cencoNuevo.Split('(')[0];
                }
                else
                {
                    return cenco[cenco.Length - 1];
                }
                
            }
            return ret;
        }

        private static string reemplazarTildes(string str)
        {
            string con = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
            string sin = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
            for (int i = 0; i < con.Length; i++)
            {
                str = str.Replace(con[i], sin[i]);
            }
            return str;

        }


        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.TrimEnd().Split(' ');
            if (!split[split.Length - 2].Contains("UC"))
            {
                return split[split.Length - 1].Trim();
            }else
                {
                return split[split.Length - 2].Trim();
            }
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var split = str.Split(' ');
            return split[split.Length - 2].Trim();
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return (str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2) 
                || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern3) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern4) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern5));
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern) ||
                str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern2) ||
                str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern3);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern) ||
             str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern2);
        }

        #endregion

    }
}