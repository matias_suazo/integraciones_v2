﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.Readers.Pdf;

namespace Integraciones.Utils.Integracion.Pdf.Applus
{
    class Applus
    {
        #region Variables
        private readonly Dictionary<int, string> _itemsPatterns = new Dictionary<int, string>
        {
            {0, @"[aA-zZ]{1,2}\d{4,6}"},
            {1, @"SKU:\s\d{6}"},
            {2, @"\s\-\s[a-zA-Z]{1,2}\d{4,6}\s\-\s" },
            {3, @"^\d{1,}\s7000538" },
            {4, @"^\d{1,}\s[a-zA-Z]{1,2}\d{4,6}" },
            //{5, @"^\d{1,}\s(\w{1,}\s\w{1,}\.?)*" }
        };
        private const string RutPattern = "RUT:";
        private const string OrdenCompraPattern = "PEDIDO NÚM:";
        private const string OrdenCompraPattern2 = "de compra";
        private const string ItemsHeaderPattern =
            "Precio Cantidad Importe";
        private const string ItemsHeaderPattern2 =
            "POS DESCRIPCION CANT UND"; 
            private const string ItemsHeaderPattern3 =
            "Artículos en línea";
        private const string CentroCostoPattern = "CENTRO DE COSTO";
        private const string ObservacionesPattern = "DIRECCION:";

        private bool _readCentroCosto;
        private bool _readOrdenCompra;
        private bool _readRut;
        private bool _readObs;
        private bool _readItem;
        private readonly PDFReader _pdfReader;
        private readonly string[] _pdfLines;

        private Integraciones.Utils.OrdenCompra.OrdenCompra OrdenCompra { get; set; }

        #endregion

        public Applus(PDFReader pdfReader)
        {
            _pdfReader = pdfReader;
            _pdfLines = _pdfReader.ExtractTextFromPdfToArrayDefaultMode();
        }

        #region Funciones Get
        public Integraciones.Utils.OrdenCompra.OrdenCompra GetOrdenCompra()
        {
            OrdenCompra = new Integraciones.Utils.OrdenCompra.OrdenCompra
            {
                //Rut = "96619100",
                CentroCosto = "0",
                TipoPareoCentroCosto = TipoPareoCentroCosto.SIN_PAREO
            };
            for (var i = 0; i < _pdfLines.Length; i++)
            {
                if (!_readOrdenCompra)
                {
                    if (IsOrdenCompraPattern(_pdfLines[i]))
                    {
                        OrdenCompra.NumeroCompra = GetOrdenCompra(_pdfLines[i]);
                        _readOrdenCompra = true;
                    }
                }
                if (!_readRut)
                {
                    if (IsRutPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Rut = GetRut(_pdfLines[i]).Replace("RUT:","");
                        if (OrdenCompra.Rut == "96670840")
                        {
                            OrdenCompra.Rut = GetRut2(_pdfLines[i + 1]);
                        }
                        _readRut = true;
                    }
                }

                if (!_readCentroCosto)
                {
                    if (IsCentroCostoPattern(_pdfLines[i]))
                   {
                        OrdenCompra.CentroCosto = GetCentroCosto(_pdfLines[i]).Replace("UO-", "").Replace("UO_","").Replace("UO","");
                        OrdenCompra.TipoPareoCentroCosto = TipoPareoCentroCosto.PAREO_CCOSTO_TATA;
                        _readCentroCosto = true;
                    }
                }
                if (!_readObs)
                {
                    if (IsObservacionPattern(_pdfLines[i]))
                    {
                        OrdenCompra.Direccion = GetObserv(_pdfLines[i]);
                        _readObs = true;
                        _readItem = false;
                    }
                }
                if (!_readItem)
                {
                    if (IsHeaderItemPatterns(_pdfLines[i]))
                    {
                        var items = GetItems(_pdfLines, i);
                        if (items.Count > 0)
                        {
                            OrdenCompra.Items.AddRange(items);
                            _readItem = true;
                        }
                    }
                }
            }
            if (OrdenCompra.NumeroCompra.Equals(""))
            {
                OrdenCompra.NumeroCompra = _pdfReader.PdfFileNameOC;
            }
            return OrdenCompra;
        }


        private List<Item> GetItems(string[] pdfLines, int i)
        {
                var items = new List<Item>();
                List<string> cantidades = new List<string>();
                List<string> precios = new List<string>();
                List<string> codigos = new List<string>();

                cantidades = GetCantidades(pdfLines, i);
                codigos = GetCodigos(pdfLines, i);
                precios = GetPrecios(pdfLines, i);

                for (var cont = 0; cont < codigos.Count; cont++)
                {
                    var itemSe = new Item
                    {
                        Sku = codigos[cont],
                        Cantidad = cantidades[cont],
                        Precio = "1",
                        TipoPareoProducto = TipoPareoProducto.SIN_PAREO,
                        TipoPrecioProducto = TipoPrecioProducto.TELEMARKETING
                    };
                    items.Add(itemSe);

                }
                Console.WriteLine();
                return items;
            }

        private string GetSku(string[] test1)
        {
            var ret = "Z446482";
            var skuDefaultPosition = test1[0].Replace("#", "");
            if (Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Success)
            {
                var index = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Index;
                var length = Regex.Match(skuDefaultPosition, @"[a-zA-Z]{1,2}\d{5,6}").Length;
                ret = skuDefaultPosition.Substring(index, length).Trim();
            }
            else
            {
                var str = test1.ArrayToString(0, test1.Length - 1);
                if (Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{1}\d{6}").Length;
                    ret = str.Substring(index, length).Trim();
                }
                else if (Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Success)
                {
                    var index = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Index;
                    var length = Regex.Match(str, @"\s[a-zA-Z]{2}\d{5}").Length;
                    ret = str.Substring(index, length).Trim();
                }
            }
            return ret;
        }

        private List<string> GetCodigos(string[] pdfLines, int i)
        {
            List<string> listaCodigos = new List<string>();
            var ret = "Z446482";
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("Subtotal:"))
                {
                    break;
                }
                Match match = Regex.Match(pdfLines[contador], @"cod.\s[a-zA-Z]{1,2}\d{5,6}");
                if (match.Success)
                {
                    listaCodigos.Add(match.Value.Split(' ')[1]);
                }

                Match match1 = Regex.Match(pdfLines[contador], @"SKU.\s[a-zA-Z]{1,2}\d{5,6}");
                if (match1.Success)
                {
                    listaCodigos.Add(match1.Value.Split(' ')[1]);
                }
                Match match2 = Regex.Match(pdfLines[contador], @"cod.\s\d{5,6}");
                if (match2.Success)
                {
                    listaCodigos.Add(match2.Value.Split(' ')[1]);
                }

                Match match3 = Regex.Match(pdfLines[contador], @"SKU.\s\d{5,6}");
                if (match3.Success)
                {
                    listaCodigos.Add(match3.Value.Split(' ')[1]);
                }
                Match match7 = Regex.Match(pdfLines[contador], @"CL_[a-zA-Z]{1,2}\d{5,6}");
                if (match7.Success)
                {
                    listaCodigos.Add(match7.Value.Split('_')[1]);
                }

                Match match8 = Regex.Match(pdfLines[contador], @"CL_\d{5,6}");
                if (match8.Success)
                {
                    listaCodigos.Add(match8.Value.Split('_')[1]);
                }
                if (!match.Success && !match1.Success && !match2.Success && !match3.Success)
                {
                    Match match4 = Regex.Match(pdfLines[contador], @"\d{1,}\s[a-zA-Z]{1,2}\d{5,6}");
                    Match match5 = Regex.Match(pdfLines[contador], @"\d{1,}\s\d{5,6}\s");
                    Match match6 = Regex.Match(pdfLines[contador], @"[a-zA-Z]{1,2}\d{5,6}\sCLP");
                    if (match4.Success)
                    {
                        listaCodigos.Add(match4.Value.Split(' ')[1]);
                    }
                    if (match5.Success)
                    {
                        listaCodigos.Add(match5.Value.Split(' ')[1]);
                    }
                    if (match6.Success)
                    {
                        listaCodigos.Add(match6.Value.Split(' ')[0]);
                    }
                }
            }

            return listaCodigos;
        }

        private List<string> GetCantidades(string[] pdfLines, int i)
        {
            List<string> listaCantidades = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("TOTAL NETO"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                Match match1 = Regex.Match(linea, @"\s\d{1,}.\d{1,}\s[(]EA[)]\s\d{1,}");
                Match match = Regex.Match(pdfLines[contador], @"\s\d{1,}\s[(]EA[)]\s\d{1,}");
                if (match.Success)
                {
                    listaCantidades.Add(match.Value.TrimStart().Split(' ')[0]);
                    continue;
                }
                if (match1.Success)
                {
                    listaCantidades.Add(match1.Value.Split(' ')[0].TrimStart().DeleteDotComa());
                }
                Match match3 = Regex.Match(linea, @"\s\d{1,}.\d{1,}\s[(]PK[)]\s\d{1,}");
                Match match2 = Regex.Match(pdfLines[contador], @"\s\d{1,}\s[(]PK[)]\s\d{1,}");
                if (match2.Success)
                {
                    listaCantidades.Add(match2.Value.TrimStart().Split(' ')[0]);
                    continue;
                }
                if (match3.Success)
                {
                    listaCantidades.Add(match3.Value.Split(' ')[0].Trim().DeleteDotComa());
                }
                Match match4 = Regex.Match(linea, @"\s\d{1,}.\d{1,}\s[(]BX[)]\s\d{1,}");
                Match match5 = Regex.Match(pdfLines[contador], @"\s\d{1,}\s[(]BX[)]\s\d{1,}");
                if (match4.Success)
                {
                    listaCantidades.Add(match4.Value.TrimStart().Split(' ')[0]);
                    continue;
                }
                if (match5.Success)
                {
                    listaCantidades.Add(match5.Value.TrimStart().Split(' ')[0].DeleteDotComa());
                }
                Match match6 = Regex.Match(linea, @"\s\d{1,}.\d{1,}\s\w{3,}\s\d{1,}");
                if (match6.Success)
                {
                    listaCantidades.Add(match6.Value.TrimStart().Split(' ')[0].DeleteDotComa());
                }
            }

            return listaCantidades;
        }

        private List<string> GetPrecios(string[] pdfLines, int i)
        {
            List<string> listaPrecios = new List<string>();
            var contador = i;
            for (; contador < pdfLines.Length; contador++)
            {
                if (pdfLines[contador].Contains("CC %"))
                {
                    break;
                }
                var linea = pdfLines[contador].DeleteContoniousWhiteSpace();
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\s+", " ");
                linea = System.Text.RegularExpressions.Regex.Replace(linea, @"\d{1,}\s\d{1,}\,", "");
                Match match = Regex.Match(linea, @"\s(\d{1,}|\d{1,}\.\d{1,})\s\$\s");
                Match match2 = Regex.Match(linea, @"(\$\s\d{1,},\d{1,}\s\$|\$\d{1,}\s\$)");
                if (match.Success)
                {
                    var value = match.Value.Trim();
                    listaPrecios.Add(value.Split(' ')[0].Trim());
                }
                else if (match2.Success)
                {
                    var value2 = match2.Value.Trim();
                    listaPrecios.Add(value2.Split(' ')[1].Trim().Replace(",", ""));
                }
            }

            return listaPrecios;
        }


        /// <summary>
        /// Obtiene el Centro de Costo de una Linea
        /// Con el formato (X123)
        /// </summary>
        /// <param name="str">Linea de texto</param>
        /// <returns></returns>
        private static string GetCentroCosto(string str)
        {
            var aux = str.Split(':', ' ');
            var aux1 = aux[aux.Length -1].Trim();
            return aux1.Split('-')[1].Trim();

        }

        private static string GetObserv(string str)
        {
            var aux = str.Split(':');
            //var aux1 = aux[1].Trim();
            return aux[1].Trim();

        }

        /// <summary>
        /// Obtiene Orden de Compra con el formato:
        ///         Número orden : 1234567890
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetOrdenCompra(string str)
        {
            var split = str.Split(' ');
            return split[split.Length -1].Trim();
        }

        /// <summary>
        /// Obtiene el Rut de una linea con el formato:
        ///         RUT:12345678-8
        /// </summary>
        /// <param name="str">Linea de Texto</param>
        /// <returns>12345678</returns>
        private static string GetRut(string str)
        {
            var vrut = str.Trim();
            if (vrut == "96670840-9") {

                vrut = "96619100";
                return vrut;
            }
            else
            {
                return vrut;
            }
        }
        private static string GetRut2(string str)
        {
            var vrut = str.Split(' ');
            var rutf = vrut[vrut.Length -1].Split('-')[0];
            if (rutf != "96670840-9")
            {
                return rutf;
            }
            else
            {
                return rutf.Split('-')[0];
            }
        }

        private int GetFormatItemsPattern(string str)
        {
            var ret = -1;
            //str = str.DeleteDotComa();
            foreach (var it in _itemsPatterns.Where(it => Regex.Match(str, it.Value).Success))
            {
                ret = it.Key;
            }
            //Console.WriteLine($"STR: {str}, RET: {ret}");
            return ret;
        }

        private static void SumarIguales(List<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                for (var j = i + 1; j < items.Count; j++)
                {
                    if (items[i].Sku.Equals(items[j].Sku))
                    {
                        items[i].Cantidad = (int.Parse(items[i].Cantidad) + int.Parse(items[j].Cantidad)).ToString();
                        items.RemoveAt(j);
                        j--;
                        Console.WriteLine($"Delete {j} from {i}");
                    }
                }
            }
        }

        private string GetPrecio(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i + 1];
            }
            return ret;
        }

        private string GetCantidad(string[] test0)
        {
            var ret = "-1";
            for (var i = 0; i < test0.Length; i++)
            {
                if (test0[i].Equals("CLP"))
                    return ret = test0[i - 1];
            }
            return ret;
        }


        #endregion


        #region Funciones Is
        private bool IsHeaderItemPatterns(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern2)
                || str.Trim().DeleteContoniousWhiteSpace().Contains(ItemsHeaderPattern3);
        }

        private bool IsObservacionPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(ObservacionesPattern);
        }

        private bool IsOrdenCompraPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern) || str.Trim().DeleteContoniousWhiteSpace().Contains(OrdenCompraPattern2);
        }
        private bool IsRutPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(RutPattern);
        }

        private bool IsCentroCostoPattern(string str)
        {
            return str.Trim().DeleteContoniousWhiteSpace().Contains(CentroCostoPattern);
        }

        #endregion

    }
}
