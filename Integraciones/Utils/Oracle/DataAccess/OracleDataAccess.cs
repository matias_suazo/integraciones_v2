﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Linq;
using Hardcodet.Wpf.TaskbarNotification;
using Integraciones.Utils.Email;
using Integraciones.Utils.Error;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Readers.Text;
using Integraciones.Utils.Readers.Word;
using Integraciones.Utils.Respuesta;
using Integraciones.View;
using System.Collections.Specialized;
using Integraciones.Utils.RegistroErroresEmail;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;


#pragma warning disable 618

namespace Integraciones.Utils.Oracle.DataAccess

{
    public static class OracleDataAccess
    {

        private static OracleConnection _instance;

        private static OracleConnection InstanceTransferWeb => _instance ??
                                                    (_instance =
                                                        new OracleConnection(
                                                            ConfigurationManager.AppSettings.Get("StringConnection")));
        private static OracleConnection _instance2;

        private static OracleConnection InstanceDmVentas => _instance2 ??
                                                    (_instance2 =
                                                        new OracleConnection(
                                                            ConfigurationManager.AppSettings.Get("StringConnectionVentas")));

        //private static OracleConnection _instance3;

        //private static OracleConnection InstanceDmIGWeb => _instance3 ??
        //                                            (_instance3 =
        //                                                new OracleConnection(
        //                                                    ConfigurationManager.AppSettings.Get("StringConnection1")));

        public static void CloseConexion()
        {
            InstanceDmVentas?.Close();
            InstanceTransferWeb?.Close();
            //InstanceDmIGWeb?.Close();
        }

        public static bool TestConexion()
        {
            if (TestConexion_()) return true;
            Log.Log.Save("Error", "No es posible Conectarse a la Base de Datos, Análisis Cancelado");
            Main.Main.ShowBalloon("Error",
                "No es posible Conectarse a la Base de Datos, Análisis Cancelado", BalloonIcon.Info);
            EmailSender.SendEmailFromProcesosXmlDimerc(
                InternalVariables.InternalVariables.GetMainEmail(),
                null,
                "Fallo Conexión a Base de Datos",
                "No es posible Conectarse a la Base de Datos, Análisis Cancelado");
            return false;
        }

        private static bool TestConexion_()
        {
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return true;
                InstanceDmVentas.Open();
                const string sql = "SELECT * FROM re_cctocli WHERE rutcli = 99512120 AND cencos = 844";
                var command = new OracleCommand(sql, InstanceDmVentas);
                InstanceTransferWeb.Open();
                var command2 = new OracleCommand(sql, InstanceTransferWeb);
                //InstanceDmIGWeb.Open();
                //var command3 = new OracleCommand(sql, InstanceDmIGWeb);
                command.ExecuteReader();
                command2.ExecuteReader();
                //command3.ExecuteReader();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
            finally
            {
                InstanceDmVentas?.Close();
                InstanceTransferWeb?.Close();
                //InstanceDmIGWeb?.Close();
            }
        }


        #region HASH RUT/RAZION CLIENTE

        internal class foo
        {
            public string Rut { get; set; }
            public string Razon { get; set; }

            public string GetHastRut()
            {
                return Encrypt.EncryptKey(Rut);
            }

            public string GetHashRazon()
            {
                return Encrypt.EncryptKey(Razon);
            }
        }
        public static void GenerateHash()
        {
            var fooList = new List<foo>();
            try
            {
                InstanceDmVentas.Open();
                const string sql = "SELECT DISTINCT RUTCLI, RAZONS FROM EN_CLIENTE";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    var rutcli = data["rutcli"].ToString();
                    var razon = data["razons"].ToString();
                    var foo = new foo
                    {
                        Rut = rutcli,
                        Razon = razon
                    };
                    fooList.Add(foo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                //return false;
            }
            finally
            {
                InstanceDmVentas?.Close();
                foreach (var f in fooList)
                {
                    //InsertHastRutCliente(f.Rut,f.Razon);
                }
                Console.WriteLine("=============================\nFIN DE GENERACIÓN DE HASH");
            }
        }

        //public static bool InsertHastRutCliente(string rutcli,string razon)
        //{
        //    using (var command = new OracleCommand())
        //    {
        //        var hashrut = Encrypt.EncryptKey(rutcli);
        //        var hashrazon = Encrypt.EncryptKey(razon);
        //        var sql = $"INSERT INTO RE_HASHRUTCLI(RUTCLI, HASH_RUTCLI, RAZONS, HASH_RAZONS) VALUES({rutcli},'{hashrut}','{razon}','{hashrazon}')";
        //        OracleTransaction trans = null;
        //        command.Connection = InstanceDmVentas;
        //        command.CommandType = CommandType.Text;
        //        command.CommandText = sql;
        //        Console.WriteLine(sql);
        //        //if (InternalVariables.IsDebug()) return true;
        //        try
        //        {
        //            InstanceDmVentas.Open();
        //            trans = InstanceDmVentas.BeginTransaction();
        //            command.Transaction = trans;
        //            command.ExecuteNonQuery();
        //            trans.Commit();
        //            return true;
        //        }
        //        catch (SqlException)
        //        {
        //            trans?.Rollback();
        //            return false;
        //        }
        //        finally
        //        {
        //            InstanceDmVentas?.Close();
        //        }
        //    }
        //}

        #endregion
        #region Registro_de_Errores
        public static bool insertarRegistroErrores(RegistroErrores objRegistroErrores)
        {
            //bool respuesta = false;
            using (var command = new OracleCommand())
            {

                var sql = $"insert into reporte_errores_email (id,rutcli,ejecutivo,tipo_error,tipo_integracion,descripcion,fecha_creacion,estado,nombre_archivo) ";
                sql += $"values (seq_re_email.nextval,{objRegistroErrores.rutcli},'{objRegistroErrores.ejecutiva}'" +
                    $",'{objRegistroErrores.tipoError}','{objRegistroErrores.tipoIntegracion}','{objRegistroErrores.descripcion}',sysdate,0,'{objRegistroErrores.nombre_archivo}')";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                //if (InternalVariables.IsDebug()) return true;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    return true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }

            //return respuesta;

        }

        public static bool actualizarRegistroErroresEmail(RegistroErrores registro)
        {
            //bool respuesta = false;
            using (var command = new OracleCommand())
            {

                var sql = $"update reporte_errores_email set fecha_envio = sysdate,estado=1 where id = {registro.id}";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                //if (InternalVariables.IsDebug()) return true;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    return true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }

            //return respuesta;

        }

        public static List<RegistroErrores> listarRegistroErroresSinLeer()
        {
            List<RegistroErrores> lista = new List<RegistroErrores>();
            try
            {
                
                InstanceTransferWeb.Open();
                var sql = $"select id,rutcli,ejecutivo,email,tipo_error,tipo_integracion,descripcion,nombre_archivo,fecha_creacion,fecha_envio,estado from   where estado = 0";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    var objRegistro = new RegistroErrores
                    {
                        id = Convert.ToInt32(data["id"]),
                        rutcli = Convert.ToInt32(data["rutcli"]),
                        ejecutiva = data["ejecutivo"].ToString(),
                        email_ejecutiva = data["email"].ToString(),
                        tipoError = data["tipo_error"].ToString(),
                        tipoIntegracion = data["tipo_integracion"].ToString(),
                        descripcion = data["descripcion"].ToString(),
                        nombre_archivo = data["nombre_archivo"].ToString(),
                        fecha_creacion = data["fecha_creacion"].ToString(),
                        fecha_envio = data["fecha_envio"].ToString(),
                        estado = Convert.ToInt32(data["estado"])

                    };
                    lista.Add(objRegistro);
                    
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return lista;
        }

        public static List<RegistroErrores> listarRegistroErroresSinLeerPorEmail(string email)
        {
            List<RegistroErrores> lista = new List<RegistroErrores>();
            try
            {

                InstanceTransferWeb.Open();
                var sql = $"select id,rutcli,ejecutivo,email,tipo_error,tipo_integracion,descripcion,nombre_archivo,fecha_creacion,fecha_envio,estado from reporte_errores_email  where estado = 0 and email='{email}'";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    var objRegistro = new RegistroErrores
                    {
                        id = Convert.ToInt32(data["id"]),
                        rutcli = Convert.ToInt32(data["rutcli"]),
                        ejecutiva = data["ejecutivo"].ToString(),
                        email_ejecutiva = data["email"].ToString(),
                        tipoError = data["tipo_error"].ToString(),
                        tipoIntegracion = data["tipo_integracion"].ToString(),
                        descripcion = data["descripcion"].ToString(),
                        nombre_archivo = data["nombre_archivo"].ToString(),
                        fecha_creacion = data["fecha_creacion"].ToString(),
                        fecha_envio = data["fecha_envio"].ToString(),
                        estado = Convert.ToInt32(data["estado"])

                    };
                    lista.Add(objRegistro);

                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return lista;
        }

        public static List<string> obtenerEmailsEjecutivosNoLeidos()
        {
            List<string> lista = new List<string>();
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"select distinct email from reporte_errores_email where estado = 0 and email is not null";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    lista.Add(data["email"].ToString());                    

                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return lista;
        }

        public static void EnvioCorreosErrores()
        {
            RegistroErroresModel rem = new RegistroErroresModel();
            List<RegistroErrores> listaErrores = rem.listarRegistroErroresSinLeer();
            foreach (var error in listaErrores)
            {

            }

        }
        #endregion
        public static bool saveAuxOrdenCompra(CompraAux compraAux)
        {
            bool resp = false;
            using (var command = new OracleCommand())
            {
                var sql = "insert into tf_compra_integ_aux (numped,cencos_cliente,archivo)";
                sql += $"values ({compraAux.numped},'{compraAux.cencos_cliente}','{compraAux.archivo}')";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    resp = true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    resp= false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }

            }
            return resp;
        }

        public static bool CreatePurchaseHistory(string rutcli)
        {
            using (var command = new OracleCommand())
            {

                var sql = $"insert into DM_TRANSFER_WEB.pdf_historial_compra_cliente (rut_cliente,codpro,descripcion,fecha_actualizacion) select {rutcli} rut_cliente,codpro,getdescripcion(codpro) descripcion, sysdate fecha_actualizacion from(select distinct codpro from en_notavta a join de_notavta b on a.numnvt = b.numnvt and a.codemp = b.codemp where fecha_creac between to_date('01/01/2016','dd/mm/yyyy') and sysdate and rutcli = {rutcli})";
                OracleTransaction trans = null;
                command.Connection = InstanceDmVentas;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                //if (InternalVariables.IsDebug()) return true;
                try
                {
                    InstanceDmVentas.Open();
                    trans = InstanceDmVentas.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    return true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    return false;
                }
                finally
                {
                    InstanceDmVentas?.Close();
                }
            }
        }


        /// <summary>
        /// Retorna el Rut del Vendedor de un Cliente
        /// </summary>
        /// <param name="rutCli">Rut Cliente</param>
        /// <returns>Mail Vendedor</returns>
        public static string GetRutUsuarioFromRutCliente(string rutCli)
        {
            var ret = "";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "SINCONEXION";
                InstanceTransferWeb.Open();
                var sql = $"SELECT rutusu FROM ma_usuario WHERE userid = (SELECT get_vendedor(3,{rutCli}) FROM dual)";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["rutusu"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }

        public static string GetCodCNLByRutusu(string rutusu)
        {
            var ret = "";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "SINCONEXION";
                InstanceTransferWeb.Open();
                var sql = $"SELECT codcnl FROM dm_ventas.ma_usuario WHERE rutusu ={rutusu}";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["codcnl"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }

        public static List<ExcelReaderCabecera> GetColumnsByHeader()
        {            
            List<ExcelReaderCabecera> cabeceras = new List<ExcelReaderCabecera>();
            try
            {                
                InstanceTransferWeb.Open();
                var sql = $"SELECT * FROM EXCEL_READER_CABECERA WHERE estado = 1";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();                
                while (data.Read())
                {
                    ExcelReaderCabecera objCabecera = new ExcelReaderCabecera()
                    {
                        cabecera = data["cabecera"].ToString(),
                        columnas = Convert.ToInt32(data["columnas"]),
                        estado = Convert.ToInt32(data["estado"])
                    };
                    cabeceras.Add(objCabecera);
                }
                data.Close();
                return cabeceras;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return cabeceras;
        }

        /// <summary>
        /// Retorna el mail del Vendedor de un Cliente
        /// </summary>
        /// <param name="rutCli">Rut Cliente</param>
        /// <returns>Mail Vendedor</returns>
        public static string GetEmailFromRutCliente(string rutCli)
        {
            var ret = "";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "TEST_SIN_CONEXION@gmail.com";
                InstanceTransferWeb.Open();
                var sql = $"SELECT mail01 FROM ma_usuario WHERE userid = (SELECT get_vendedor(3,{rutCli}) FROM dual)";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["mail01"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }



        public static string GetEjecutivoFromRutCliente(string rutCli)
        {
            var ret = "";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "TEST_SIN_CONEXION@gmail.com";
                InstanceTransferWeb.Open();
                var sql = $"SELECT nombre || ' '|| apepat ||' '|| apemat NOMBRE FROM ma_usuario WHERE userid = (SELECT get_vendedor(3,{rutCli}) FROM dual)";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["NOMBRE"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret.ConvertirPrimeraLetraEnMayuscula().DeleteContoniousWhiteSpace();
        }

        public static string GetCencosByRe_ddescli(string rut, string CentroCosto)
        {
            var arregloCC = CentroCosto.Split(' ').Count();
            string[] likeCC = new string[arregloCC];
            for (int i = 0; i < likeCC.Length; i++)
            {
                likeCC[i] = CentroCosto.Split(' ')[i];
            }
            var ret = "-1";
            if (arregloCC >= 6)
            {
                arregloCC = 4;
            }
            if(rut.Contains("76754296") && !CentroCosto.Contains("SANTIAGO"))
            {
                arregloCC = 1;
            }
            switch (arregloCC)
            {
                case 1:
                    try
                    {
                        InstanceDmVentas.Open();
                        var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{likeCC[0]}%'";
                        var command = new OracleCommand(sql, InstanceDmVentas);
                        var data = command.ExecuteReader();
                        if (data.Read())
                        {
                            ret = data["cencos"].ToString();
                        }
                        data.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        InstanceDmVentas?.Close();
                    }
                    break;
                case 2:
                    try
                    {
                        InstanceDmVentas.Open();
                        var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{likeCC[0]}%' AND descco LIKE '%{likeCC[1]}%'";
                        var command = new OracleCommand(sql, InstanceDmVentas);
                        var data = command.ExecuteReader();
                        if (data.Read())
                        {
                            ret = data["cencos"].ToString();
                        }
                        data.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        InstanceDmVentas?.Close();
                    }
                    break;
                case 3:
                    try
                    {
                        InstanceDmVentas.Open();
                        var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{likeCC[0]}%' AND descco LIKE '%{likeCC[1]}%' AND descco LIKE '%{likeCC[2]}%'";
                        var command = new OracleCommand(sql, InstanceDmVentas);
                        var data = command.ExecuteReader();
                        if (data.Read())
                        {
                            ret = data["cencos"].ToString();
                        }
                        data.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        InstanceDmVentas?.Close();
                    }
                    break;
                case 4:
                    try
                    {
                        InstanceDmVentas.Open();
                        var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{likeCC[0]}%' AND descco LIKE '%{likeCC[1]}%' AND descco LIKE '%{likeCC[2]}%' AND descco LIKE '%{likeCC[3]}%'";
                        var command = new OracleCommand(sql, InstanceDmVentas);
                        var data = command.ExecuteReader();
                        if (data.Read())
                        {
                            ret = data["cencos"].ToString();
                        }
                        data.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        InstanceDmVentas?.Close();
                    }
                    break;
                case 5:
                    try
                    {
                        InstanceDmVentas.Open();
                        var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{likeCC[0]}%' AND descco LIKE '%{likeCC[1]}%' AND descco LIKE '%{likeCC[2]}%' AND descco LIKE '%{likeCC[3]}%' AND descco LIKE '%{likeCC[4]}%'";
                        var command = new OracleCommand(sql, InstanceDmVentas);
                        var data = command.ExecuteReader();
                        if (data.Read())
                        {
                            ret = data["cencos"].ToString();
                        }
                        data.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        InstanceDmVentas?.Close();
                    }
                    break;
                case 6:
                    try
                    {
                        InstanceDmVentas.Open();
                        var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{likeCC[0]}%' AND descco LIKE '%{likeCC[1]}%' AND descco LIKE '%{likeCC[2]}%' AND descco LIKE '%{likeCC[3]}%' AND descco LIKE '%{likeCC[4]}%'";
                        var command = new OracleCommand(sql, InstanceDmVentas);
                        var data = command.ExecuteReader();
                        if (data.Read())
                        {
                            ret = data["0"].ToString();
                        }
                        data.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    finally
                    {
                        InstanceDmVentas?.Close();
                    }
                    break;

            }

                    /*if (!(CentroCosto.Contains("8060103") || CentroCosto.Contains("8060114") || CentroCosto.Contains("8060703") ||
                   CentroCosto.Contains("4051145") || CentroCosto.Contains("9030868")))
            {
                try
                {
                    InstanceDmVentas.Open();
                    var sql = $"SELECT cencos FROM DE_CLIENTE WHERE RUTCLI = {rut} AND descco LIKE '%{dirdes}%' AND descco LIKE '%{cod_direc}%'";
                    var command = new OracleCommand(sql, InstanceDmVentas);
                    var data = command.ExecuteReader();
                    if (data.Read())
                    {
                        ret = data["cencos"].ToString();
                    }
                    data.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    InstanceDmVentas?.Close();
                }
            }
            if ((CentroCosto.Contains("8060103") || CentroCosto.Contains("8060114") || CentroCosto.Contains("8060703") ||
                   CentroCosto.Contains("4051145") || CentroCosto.Contains("9030868")))
            {
                var ccosto = CentroCosto.Split(' ');
                var cc1 = ccosto[0].Trim().ToUpper();
                var cc2 = ccosto[1].Trim().ToUpper();
                var cc3 = ccosto[2].Trim().ToUpper();
                var cc4 = ccosto[ccosto.Length - 1].Trim().ToUpper();
                if (ccosto.Length == 5)
                {
                    cc4 = ccosto[3].Trim().ToUpper();
                }
                try
                {
                    InstanceDmVentas.Open();
                    var sql = $"SELECT cencos FROM DE_CLIENTE WHERE descco LIKE '%{cc1}%' AND descco LIKE '%{cc2}%' AND descco LIKE '%{cc3}%' AND descco LIKE '%{cc4}%'";
                    var command = new OracleCommand(sql, InstanceDmVentas);
                    var data = command.ExecuteReader();
                    if (data.Read())
                    {
                        ret = data["cencos"].ToString();
                    }
                    data.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    InstanceDmVentas?.Close();
                }
            }
            
            if (ret.Equals("-1") && CentroCosto.Split(' ').Length <= 5)
            {
                var ccosto = CentroCosto.Split(' ');
                var cc1 = ccosto[0].Trim().ToUpper();
                var cc2 = ccosto[1].Trim().ToUpper();
                var cc3 = ccosto[2].Trim().ToUpper();
                var cc4 = ccosto[ccosto.Length - 1].Trim().ToUpper();
                if (ccosto.Length == 5)
                {
                    cc4 = ccosto[3].Trim().ToUpper();
                }
                try
                {
                    InstanceDmVentas.Open();
                    var sql = $"SELECT cencos FROM DE_CLIENTE WHERE descco LIKE '%{cc1}%' AND descco LIKE '%{cc2}%' AND descco LIKE '%{cc3}%'";
                    var command = new OracleCommand(sql, InstanceDmVentas);
                    var data = command.ExecuteReader();
                    if (data.Read())
                    {
                        ret = data["cencos"].ToString();
                    }
                    data.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    InstanceDmVentas?.Close();
                }
            }
            
            if (ret.Equals("-1") && CentroCosto.Split(' ').Length <= 5)
            {
                var ccosto = CentroCosto.Split(' ');
                var cc1 = ccosto[0].Trim().ToUpper();
                var cc2 = ccosto[1].Trim().ToUpper();
                var cc3 = ccosto[2].Trim().ToUpper();
                var cc4 = ccosto[ccosto.Length - 1].Trim().ToUpper();
                if (ccosto.Length == 5)
                {
                    cc4 = ccosto[3].Trim().ToUpper();
                }
                try
                {
                    InstanceDmVentas.Open();
                    var sql = $"SELECT cencos FROM DE_CLIENTE WHERE descco LIKE '%{cc1}%' AND descco LIKE '%{cc3}%' AND descco LIKE '%{cc4}%'";
                    var command = new OracleCommand(sql, InstanceDmVentas);
                    var data = command.ExecuteReader();
                    if (data.Read())
                    {
                        ret = data["cencos"].ToString();
                    }
                    data.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    InstanceDmVentas?.Close();
                }
            }*/
            
            return ret;
            

        }

        public static string GetCencosByCotizac(string rut, string numeroCompra)
        {
            var ret = "0";
            var coti = numeroCompra.Split(' ');
            var numcot = coti[coti.Length - 1].Trim();
            try {
                InstanceDmVentas.Open();
                var sql = $"select cencos from en_cotizac where rutcli = {rut} and numcot={numcot}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["cencos"].ToString();
                }
                data.Close();
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;

        }

        

        /// <summary>GetCencosByRe_ddescli(oc.Rut, oc.CentroCosto)
        /// Función para validar si existe un Producto
        /// </summary>
        /// <param name="sku">Sku</param>
        /// <returns>Existe?</returns>
        public static bool ExistProduct(string sku)
        {

            if (sku.Equals("Z446482")) return false;
            var ret = "";
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT COUNT(CODPRO) EXIST FROM MA_PRODUCT WHERE CODPRO = '{sku}'";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["EXIST"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret.Equals("1");
        }

        public static bool ExistPurchaseHistory(string rutcli)
        {
            var ret = "";
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"SELECT COUNT(*) EXIST FROM PDF_HISTORIAL_COMPRA_CLIENTE WHERE RUT_CLIENTE = '{rutcli}'";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["EXIST"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return !ret.Equals("0");
        }


        public static bool IsMarca(string marca)
        {
            if (marca == null) return false;
            var ret = "";
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT 1 EXIST FROM MA_PRODUCT WHERE GETMARCA(CODPRO) = '{marca}' AND ROWNUM = 1";
                //$"select 1 from ma_product where getmarca(codpro) = 'ELITE' and rownum = 1";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["EXIST"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret.Equals("1");
        }


        public static string GetHashFromOrdenCompra(OrdenCompraIntegracion oc)
        {
            var ret = "";
            try
            {
                var sql = $"SELECT HASH FROM PDF_MAIL_HASH WHERE NUMPED = {oc.NumPed} AND CODEMP = {(int)oc.CodigoEmpresa}";
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "SIN_HASH";
                InstanceTransferWeb.Open();
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["HASH"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }




        public static string GetRazonSocial(string rutCli)
        {
            var ret = "";
            try
            {
                var sql = $"SELECT getrazonsocial(3,{rutCli}) RAZONSOCIAL FROM dual";
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "TEST_SIN_CONEXION";
                InstanceTransferWeb.Open();
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["RAZONSOCIAL"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }



        public static bool TieneBodegaAntofagasta(string rutcli, string cencos)
        {
            var ret = 0;
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"SELECT COUNT(RUTCLI) COU FROM PDF_CENCOS_ANTOFAGASTA WHERE RUTCLI = {rutcli} AND CENCOS = {cencos}";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = int.Parse(data["COU"].ToString());
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret > 0;
        }

        public static int GetStockAntofagasta(string codPro)
        {
            var ret = 0;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT GETSTOCKBODEGA(3,66,'{codPro}') STOCK FROM DUAL";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = int.Parse(data["STOCK"].ToString());
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static string GetPrecioProductoTest(string rutCli, string cencos, string codpro, string codemp)
        {
            var ret = "0";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                InstanceTransferWeb.Open();
                var sql = $"SELECT GET_PRECIOPROD_BK({rutCli},0,'{codpro}',{codemp}) PRECIO FROM DUAL";



                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["PRECIO"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }

        public static string GetPrecioProducto(string rutCli, string cenco , string codpro, string codEmp)// prueba precio
        {
            var ret = "0";
            try
            {
                //if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                //InstanceTransferWeb.Open();
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                InstanceDmVentas.Open();
                var sql = $"SELECT dm_transfer_web.GET_PRECIOSPROD({rutCli},0,'{codpro}',3) PRECIO FROM DUAL";
                //var sql = $"SELECT sap_get_prelis(3,'{codpro}',{rutCli}) PRECIO FROM DUAL";
                

                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["PRECIO"].ToString();
                    if (ret.Equals(""))
                    {
                        ret = "0";
                    }
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                //InstanceTransferWeb?.Close();
                InstanceDmVentas?.Close();
            }
            return ret;
        }
        public static string GetPrecioProductoUltimate(string codEmp, string rutCli, string codpro )
        {
            var ret = "0";
            try
            {
                //if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                //InstanceTransferWeb.Open();
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                InstanceDmVentas.Open();
                var sql = $"select DM_VENTAS.GET_BI_PRECIOCOSTOFINAL2(3,{rutCli},'{codpro}',1) PRECIO from dual";
                //var sql = $"SELECT sap_get_prelis(3,'{codpro}',{rutCli}) PRECIO FROM DUAL";


                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["PRECIO"].ToString();
                    if (ret.Equals(""))
                    {
                        ret = "0";
                    }
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                //InstanceTransferWeb?.Close();
                InstanceDmVentas?.Close();
            }
            return ret;
        }
        public static string GetPrecioProducto1(string codEmp, string codpro, string rutCli)
        {
            var ret = "0";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                InstanceDmVentas.Open();
                
                var sql = $"SELECT SAP_GET_PRELIS(3,'{codpro}',{rutCli}) PRECIO FROM DUAL";
                //var sql = $"SELECT sap_get_prelis({codEmp},'{codpro}',{rutCli}) PRECIO FROM DUAL";


                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["PRECIO"].ToString();
                    if (ret.Equals(""))
                    {
                        ret = "0";
                    }
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static string GetPrecioProductoConvenio(string codpro, string rutCli)
        {
            var ret = "0";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "100";
                InstanceDmVentas.Open();

                //var sql = $"SELECT SAP_GET_PRELIS(3,'{codpro}',{rutCli}) PRECIO FROM DUAL";
                var sql = $"select PRECIO from en_conveni a,de_conveni b where a.numcot = b.numcot and b.codpro = '{codpro}' and a.fecven >= trunc(sysdate) and a.rutCli = {rutCli}";
                //var sql = $"SELECT sap_get_prelis({codEmp},'{codpro}',{rutCli}) PRECIO FROM DUAL";


                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["PRECIO"].ToString();
                    if (ret.Equals(""))
                    {
                        ret = "0";
                    }
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }
        public static string GetPrecioConvenio(string rutCli, string cencos, string codPro, string precio)
        {
            var ret = GetPrecioProductoTest(rutCli, cencos, codPro, "3");
            //var ret2 = GetPrecioProducto(rutCliente, cencos, codPro, "3");
            //Console.WriteLine($"ret: {ret}, ret2: {ret2}");
            //return ret.Equals("") ? ret2 : ret;
            return ret;
        }



        /// <summary>
        /// Obtiene un Nuevo Numero de Pedido
        /// </summary>
        /// <returns>Número Pedido</returns>
        public static string GetNumPed()
        {
            var ret = "-1";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                InstanceTransferWeb.Open();

                var sql = "select dm_ventas.SEQ_INTEGRACION.nextval@prod from DUAL";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["nextval"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;

        }

        public static bool EsClienteSap(string rutcli)
        {
            bool ret = false;
            try
            {
                int rut = Convert.ToInt32(rutcli);
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                InstanceTransferWeb.Open();

                var sql = $"select count(*) cantidad from dm_ventas.en_cliente where rutcli ={rut}  and id_cliente_sap is not null";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = (Convert.ToInt32(data["cantidad"].ToString())) > 0;
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }

        public static bool EsCcostoSap(string rutcli,string cencoss)
        {
            bool ret = false;
            try
            {
                int rut = Convert.ToInt32(rutcli);
                int cencos = Convert.ToInt32(cencoss);
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                InstanceTransferWeb.Open();

                var sql = $"select count(*) cantidad from dm_ventas.de_cliente where rutcli ={rut}  and cencos = {cencos} and id_cliente_sap is not null";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = (Convert.ToInt32(data["cantidad"].ToString())) > 0;
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }


        public static string GetNull()
        {
            var ret = "";
            try
            {
                InstanceTransferWeb.Open();
                var sql = "SELECT (SELECT DESVAL2 FROM DE_DOMINIO WHERE DESVAL = B.SKU_DIMERC AND CODDOM = 904) RELACIONADO FROM TF_COMPRA_INTEG_PDF A, TF_DETALLE_COMPRA_INTEG_PDF B WHERE A.NUMPED = B.NUMPED AND A.ESTADO = 0";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                ret = data.Read() ? data["RELACIONADO"].ToString() : "";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }


        #region Match Product

        public static int _tamanioMinimoPalabras = 1;
        //private const int TamanioMaximoPalabras = 4;
        private const int TamanioMaximoPalabras = 6;
        private static bool ReplaceSymbol;
        private static bool ReplaceNumber;
        private static bool ReplaceSymbolNumber;
        public static string DescPro;

        /// <summary>
        /// Obtiene Producto haciendo Match desde Descripcion Relacionada al Cliente
        /// </summary>
        /// <param name="rutcli">Cliente</param>
        /// <param name="descPro"></param>
        /// <param name="palabraMinLenght">Tamaño Minimo de Palabra a Buscar</param>
        /// <returns></returns>
        public static string GetSkuWithMatchClientProductDescription(string rutcli, string descPro, int palabraMinLenght = 1)
        {
            if (descPro == null) return "Z446482";
            descPro = descPro.ToUpper().DeleteAcent().DeleteContoniousWhiteSpace();
            var split = descPro.Split(' ');
            var descContainsRow = new List<string>();
            var index = 0;
            var CodProd = "Z446482";
            if (palabraMinLenght > TamanioMaximoPalabras)
            {
                if (!ReplaceSymbol)
                {
                    ReplaceSymbol = true;
                    return GetSkuWithMatchClientProductDescription(rutcli, DescPro.DeleteSymbol(), 1);
                }
                if (!ReplaceNumber)
                {
                    ReplaceNumber = true;
                    return GetSkuWithMatchClientProductDescription(rutcli, DescPro.DeleteNumber(), 1);
                }
                if (!ReplaceSymbolNumber)
                {
                    ReplaceSymbolNumber = true;
                    return GetSkuWithMatchClientProductDescription(rutcli, DescPro.DeleteSymbol().DeleteNumber(), 1);
                }

                return CodProd;
            }
            foreach (var t in split.Where(t => t.Length > _tamanioMinimoPalabras))
            {
                descContainsRow.Add(t);
                var rows = GetCountProductDescriptionMatchFromClient(rutcli, descContainsRow);
                if (rows == 0)
                {
                    descContainsRow.RemoveAt(index);
                }
                else// if (rows == 1)
                {
                    index++;
                }
                if (rows >= 1)
                {
                    CodProd = MatchProductDescriptionCliente(rutcli, descContainsRow);
                }
            }
            if (CodProd.Equals("Z446482"))
            {
                return GetSkuWithMatchClientProductDescription(rutcli, descPro, ++palabraMinLenght);
            }
            Console.WriteLine($"Product Match: {CodProd}");
            return CodProd;
        }


        public static string GetSkuWithMatchClientProductDescription(string rutcli, string codCli, string descPro, bool first = true)
        {
            Console.WriteLine($"GetSkuWithMatchClientProductDescription: {rutcli} {codCli} {descPro}");
            if (descPro == null) return "Z446482";
            descPro = descPro.ToUpper().DeleteAcent().DeleteContoniousWhiteSpace();
            if (first)
            {
                var sku1 = GetProductExactDescriptionCliente(rutcli, codCli, descPro);
                Console.WriteLine($"FIRST SKU: {sku1}");
                if (!sku1.Equals("Z446482")) return sku1;
                DescPro = descPro;
                var sku = GetProductAllLikeWordDescriptionCliente(rutcli, codCli, descPro);
                Console.WriteLine($"FIRST SKU: {sku}");
                if (!sku.Equals("Z446482")) return sku;
            }
            var split = descPro.Split(' ');
            var descContainsRow = new List<string>();
            var index = 0;
            var CodProd = "Z446482";
            if (_tamanioMinimoPalabras > TamanioMaximoPalabras)
            {
                if (!ReplaceSymbol)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceSymbol = true;
                    return GetSkuWithMatchClientProductDescription(rutcli, codCli, DescPro.DeleteSymbol());
                }
                if (!ReplaceNumber)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceNumber = true;
                    return GetSkuWithMatchClientProductDescription(rutcli, codCli, DescPro.DeleteNumber());
                }
                if (!ReplaceSymbolNumber)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceSymbolNumber = true;
                    return GetSkuWithMatchClientProductDescription(rutcli, codCli, DescPro.DeleteSymbol().DeleteNumber());
                }

                return CodProd;
            }
            foreach (var t in split.Where(t => t.Length > _tamanioMinimoPalabras))
            {
                descContainsRow.Add(t);
                var rows = GetCountProductDescriptionMatchFromClient(rutcli, codCli, descContainsRow);
                if (rows == 0)
                {
                    descContainsRow.RemoveAt(index);
                }
                else// if (rows == 1)
                {
                    index++;
                }
                if (rows >= 1)
                {
                    CodProd = MatchProductDescriptionCliente(rutcli, codCli, descContainsRow);
                }
            }
            if (CodProd.Equals("Z446482"))
            {
                _tamanioMinimoPalabras++;
                return GetSkuWithMatchClientProductDescription(rutcli, codCli, descPro);
            }
            Console.WriteLine($"Product Match: {CodProd}");
            return CodProd;
        }


        public static string GetSkuWithMatchHistorialClientProductDescription(string rutcli, string descPro, bool first)
        {
            //Console.WriteLine($"GetSkuWithMatchHistorialClientProductDescription: {rutcli}, {descPro}, {first}");
            if (descPro == null) return "Z446482";
            if (first)
            {
                _tamanioMinimoPalabras = 1;
                DescPro = descPro;
                var sku = GetProductExactDescriptionMaestra(descPro);
                if (!sku.Equals("Z446482")) return sku;
                sku = GetProductAllLikeWordDescriptionHistorialCliente(descPro);
                if (!sku.Equals("Z446482")) return sku;
            }
            descPro = descPro.ToUpper().DeleteAcent().DeleteContoniousWhiteSpace();
            var split = descPro.Split(' ');
            var descContainsRow = new List<string>();
            var index = 0;
            var CodProd = "Z446482";
            if (_tamanioMinimoPalabras > TamanioMaximoPalabras)
            {
                if (!ReplaceSymbol)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceSymbol = true;
                    return GetSkuWithMatchHistorialClientProductDescription(rutcli, DescPro.DeleteSymbol(), false);
                }
                if (!ReplaceNumber)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceNumber = true;
                    return GetSkuWithMatchHistorialClientProductDescription(rutcli, DescPro.DeleteNumber(), false);
                }
                if (!ReplaceSymbolNumber)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceSymbolNumber = true;
                    return GetSkuWithMatchHistorialClientProductDescription(rutcli, DescPro.DeleteSymbol().DeleteNumber(), false);
                }

                return CodProd;
            }
            foreach (var t in split.Where(t => t.Length > _tamanioMinimoPalabras))
            {
                descContainsRow.Add(t);

                Console.WriteLine("SQL 0");
                var rows = GetCountProductDescriptionMatchFromHistorialClient(rutcli, descContainsRow);
                if (rows == 0)
                {
                    descContainsRow.RemoveAt(index);
                }
                else// if (rows == 1)
                {
                    index++;
                }
                if (rows >= 1)
                {
                    Console.WriteLine("SQL 1");
                    CodProd = MatchProductDescriptionHistorialCliente(rutcli, descContainsRow);
                }
            }
            if (CodProd.Equals("Z446482"))
            {
                _tamanioMinimoPalabras++;
                return GetSkuWithMatchHistorialClientProductDescription(rutcli, descPro, false);
            }
            Console.WriteLine($"Product Match: {CodProd}");
            return CodProd;
        }


        /// <summary>
        /// Obtiene el SKU de un Producto con Descripción Exacta
        /// </summary>
        /// <param name="desc">Descripción</param>
        /// <returns>Sku</returns>
        public static string GetProductExactDescriptionMaestra(string desc)
        {
            var ret = "Z446482";
            string desc2 = desc.Replace("'", "''").ToUpper().DeleteContoniousWhiteSpace();
            try
            {
                InstanceDmVentas.Open();
                //var sql = $"SELECT CODPRO FROM MA_PRODUCT WHERE DESPRO = '{desc.Replace("'", "''").ToUpper().DeleteContoniousWhiteSpace()}'";
                var sql = $"SELECT CODPRO FROM MA_PRODUCT WHERE despro = (select despro from ma_product where replace(despro,'  ',' ') = '{desc2}') ";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                Console.WriteLine(sql);
                ret = data.Read() ? data["CODPRO"].ToString() : "Z446482";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }





        public static string GetProductExactDescriptionCliente(string rutcli, string codigoCliente, string desc)
        {
            var ret = "Z446482";
            try
            {
                InstanceDmVentas.Open();
                var realCodCli = codigoCliente.Equals("Z446482") ? "SIN_SKU" : codigoCliente;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                var sql = $"SELECT CODPRO FROM dm_ventas.RE_CODCLI WHERE RUTCLI = {rutcli} AND {codCliFilter} AND DESCRIPCION_CLIENTE = '{desc.Replace("'", "''").ToUpper().DeleteContoniousWhiteSpace()}'";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                Console.WriteLine(sql);
                ret = data.Read() ? data["CODPRO"].ToString() : "Z446482";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }



        public static string GetProductAllLikeWordDescriptionCliente(string rutCli, string codigoCliente, string desc)
        {
            var ret = "Z446482";
            var sku = codigoCliente.Equals("Z446482") ? "SIN_SKU" : codigoCliente;
            var likes = desc.Split(' ').Aggregate("", (current, str) => current + $" AND DESCRIPCION_CLIENTE LIKE '%{str.Replace("'", "''")}%'");
            try
            {
                InstanceDmVentas.Open();
                var realCodCli = codigoCliente.Equals("Z446482") ? "SIN_SKU" : codigoCliente;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                var sql = $"SELECT CODPRO FROM dm_ventas.RE_CODCLI WHERE RUTCLI = {rutCli} AND {codCliFilter} {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read() ? data["CODPRO"].ToString() : "Z446482";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret.Replace("SIN_SKU", "Z446482");
        }




        public static string GetProductAllLikeWordDescriptionMaestra(string desc)
        {
            var ret = "Z446482";
            var likes = "";
            foreach (var str in desc.ToUpper().Split(' '))
            {
                if (likes.Length == 0)
                    likes += $"despro LIKE '%{str}%'";
                else
                    likes += $" AND despro LIKE '%{str}%'";
            }
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM MA_PRODUCT WHERE {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);

                //Console.WriteLine($"LIKE_ALL_WORD: {sql}");
                var data = command.ExecuteReader();
                ret = data.Read() ? data["CODPRO"].ToString() : "Z446482";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }



        private static string GetProductAllLikeWordDescriptionHistorialCliente(string desc)
        {
            var ret = "Z446482";
            var likes = "";
            foreach (var str in desc.Split(' '))
            {
                if (likes.Length == 0)
                    likes += $"DESCRIPCION LIKE '%{str.Replace("'", "''")}%'";
                else
                    likes += $" AND DESCRIPCION LIKE '%{str.Replace("'", "''")}%'";
            }
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"SELECT CODPRO FROM PDF_HISTORIAL_COMPRA_CLIENTE WHERE {likes}";
                var command = new OracleCommand(sql, InstanceTransferWeb);

                Console.WriteLine($"LIKE_ALL_WORD: {sql}");
                var data = command.ExecuteReader();
                ret = data.Read() ? data["CODPRO"].ToString() : "Z446482";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }



        public static string GetSkuWithMatcthDimercProductDescription(string descPro, bool first, bool productosActivos, bool print = true)
        {
            Console.WriteLine($"GetSkuWithMatcthDimercProductDescription: {descPro}");
            descPro = descPro.ToUpper().DeleteAcent().DeleteContoniousWhiteSpace();
            if (first)
            {
                DescPro = descPro;
                var sku = GetProductExactDescriptionMaestra(descPro);
                if (print)
                    Console.WriteLine($"FIRST SKU: {sku}");
                if (!sku.Equals("Z446482")) return sku;
                sku = GetProductAllLikeWordDescriptionMaestra(descPro);
                if (!sku.Equals("Z446482")) return sku;
            }
            var split = descPro.Split(' ');
            var descContainsRow = new List<string>();
            var index = 0;
            var CodProd = "Z446482";
            if (_tamanioMinimoPalabras > TamanioMaximoPalabras)
            {
                if (!ReplaceSymbol)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceSymbol = true;
                    return GetSkuWithMatcthDimercProductDescription(DescPro.DeleteSymbol(), false, productosActivos);
                }
                if (!ReplaceNumber)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceNumber = true;
                    return GetSkuWithMatcthDimercProductDescription(DescPro.DeleteNumber(), false, productosActivos);
                }
                if (!ReplaceSymbolNumber)
                {
                    _tamanioMinimoPalabras = 0;
                    ReplaceSymbolNumber = true;
                    return GetSkuWithMatcthDimercProductDescription(DescPro.DeleteSymbol().DeleteNumber(), false, productosActivos);
                }

                return CodProd;
            }
            foreach (var t in split.Where(t => t.Length > _tamanioMinimoPalabras))
            {
                descContainsRow.Add(t);
                var rows = productosActivos
                    ? GetCountDescProdMatchProductosActivos(descContainsRow)
                    : GetCountDescProdMatch(descContainsRow);
                if (rows == 0)
                {
                    descContainsRow.RemoveAt(index);
                }
                else// if (rows == 1)
                {
                    index++;
                }
                if (rows >= 1)
                {
                    CodProd = productosActivos
                        ? MatchProductDescriptionMaestraProductosActivos(descContainsRow)
                        : MatchProductDescriptionMaestra(descContainsRow);
                }
            }
            if (CodProd.Equals("Z446482"))
            {
                _tamanioMinimoPalabras++;
                return GetSkuWithMatcthDimercProductDescription(descPro, false, productosActivos);
            }
            Console.WriteLine($"Product Match: {CodProd}");
            return CodProd;
        }

        public static string MatchProductDescriptionCliente(string rutCliente, IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                likes += $"and descripcion_cliente LIKE '%{str}%'";
            }
            var ret = "";
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM dm_ventas.re_codcli WHERE rutcli = {rutCliente} {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                //Console.WriteLine(sql);
                ret = data.Read() ? data["CODPRO"].ToString() : "";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }


        public static string MatchProductDescriptionCliente(string rutCliente, string codCli, IEnumerable<string> desc)
        {
            var likes = desc.Where(str => str.Length > _tamanioMinimoPalabras)
                .Aggregate("", (current, str) => current + $"and descripcion_cliente LIKE '%{str}%'");
            var ret = "";
            try
            {
                var realCodCli = codCli.Equals("Z446482") ? "SIN_SKU" : codCli;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM dm_ventas.re_codcli WHERE rutcli = {rutCliente} and {codCliFilter} {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                Console.WriteLine(sql);
                ret = data.Read() ? data["CODPRO"].ToString() : "";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }



        public static string MatchProductDescriptionHistorialCliente(string rutCliente, IEnumerable<string> desc)
        {
            var likes = desc.Where(str => str.Length > _tamanioMinimoPalabras).Aggregate("", (current, str) => current + $"and descripcion LIKE '%{str}%'");
            var ret = "";
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"SELECT CODPRO FROM pdf_historial_compra_cliente WHERE rut_cliente = {rutCliente} {likes}";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                Console.WriteLine($"SQL1: {sql}");
                ret = data.Read() ? data["CODPRO"].ToString() : "";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }


        public static string MatchProductDescriptionMaestra(IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                if (likes.Length == 0)
                    likes += $"despro LIKE '%{str}%'";
                else
                    likes += $" AND despro LIKE '%{str}%'";
            }
            var ret = "";
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM ma_product WHERE {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                //Console.WriteLine(sql);
                ret = data.Read() ? data["CODPRO"].ToString() : "";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }


        public static string MatchProductDescriptionMaestraProductosActivos(IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                if (likes.Length == 0)
                    likes += $"despro LIKE '%{str}%'";
                else
                    likes += $" AND despro LIKE '%{str}%'";
            }
            var ret = "";
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM ma_product WHERE estpro in (1,2,3,5,11) and {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                //Console.WriteLine(sql);
                ret = data.Read() ? data["CODPRO"].ToString() : "";
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static int GetCountDescCencosMatch(string rutCli, IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                if (likes.Length == 0)
                    likes += $"ccosto LIKE '%{str}%'";
                else
                    likes += $" AND ccosto LIKE '%{str}%'";
            }
            var ret = 0;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT cencos FROM DM_TRANSFER_WEB.TF_re_cctocli WHERE RUTCLI = {rutCli} AND {likes}";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                Console.WriteLine(sql);
                while (data.Read())
                    ret++;
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        private static int GetCountDescProdMatch(IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                if (likes.Length == 0)
                    likes += $"despro LIKE '%{str}%'";
                else
                    likes += $" AND despro LIKE '%{str}%'";
            }
            var ret = 0;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM ma_product WHERE {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                //Console.WriteLine(sql);
                while (data.Read())
                    ret++;
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        private static int GetCountDescProdMatchProductosActivos(IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                if (likes.Length == 0)
                    likes += $"despro LIKE '%{str}%'";
                else
                    likes += $" AND despro LIKE '%{str}%'";
            }
            var ret = 0;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM ma_product WHERE estpro in (1,11) and {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                //Console.WriteLine(sql);
                while (data.Read())
                    ret++;
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }


        private static int GetCountProductDescriptionMatchFromClient(string rutCliente, string codCli, IEnumerable<string> desc)
        {
            var likes = desc.Where(str => str.Length > _tamanioMinimoPalabras).Aggregate("", (current, str) => current + $" AND descripcion_cliente LIKE '%{str}%'");
            var ret = 0;
            try
            {
                var realCodCli = codCli.Equals("Z446482") ? "SIN_SKU" : codCli;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM dm_ventas.re_codcli WHERE rutcli = {rutCliente} and {codCliFilter} {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                Console.WriteLine(sql);
                while (data.Read())
                    ret++;
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }



        private static int GetCountProductDescriptionMatchFromClient(string rutCliente, IEnumerable<string> desc)
        {
            var likes = "";
            foreach (var str in desc.Where(str => str.Length > _tamanioMinimoPalabras))
            {
                likes += $" AND descripcion_cliente LIKE '%{str}%'";
            }
            var ret = 0;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT CODPRO FROM dm_ventas.re_codcli WHERE rutcli = {rutCliente} {likes}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                Console.WriteLine(sql);
                while (data.Read())
                    ret++;
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }


        private static OrdenCompraIntegracion GetOrdenCompraIntegracionFromNumped(string numped, TipoIntegracion tipoIntegracion)
        {
            var orden = new OrdenCompraIntegracion();
            try
            {
                var cabecera = "TF_COMPRA_INTEG_PDF";
                switch (tipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        cabecera = "TF_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.MAIL:
                        cabecera = "TF_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.WORD:
                        cabecera = "TF_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.EXCEL:
                        cabecera = "TF_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        cabecera = "TF_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        cabecera = "TF_COMPRA_INTEG_TXT";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR_PRV:
                        cabecera = "TF_COMPRA_INTEG_CA_ES_PRV";
                        break;

                }
                InstanceTransferWeb?.Close();
                InstanceTransferWeb.Open();
                var sql = $"SELECT  numped, rutcli, cencos, direccion, oc_cliente, obs FROM {cabecera} where numped = {numped}";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    orden = new OrdenCompraIntegracion
                    {
                        NumPed = data["numped"].ToString(),
                        CenCos = data["cencos"].ToString(),
                        Direccion = data["direccion"].ToString(),
                        Observaciones = data["obs"].ToString(),
                        RutCli = int.Parse(data["rutcli"].ToString()),
                        OcCliente = data["oc_cliente"].ToString()
                    };
                    data.Close();
                }
                else return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            InstanceTransferWeb?.Close();
            var items = GetDetalleOrdenCompraIntegracionFromNumped(numped, tipoIntegracion);
            orden.DetallesCompra = items;
            return orden;
        }

        private static List<DetalleOrdenCompraIntegracion> GetDetalleOrdenCompraIntegracionFromNumped(string numped, TipoIntegracion tipoIntegracion)
        {
            var detalleItems = new List<DetalleOrdenCompraIntegracion>();
            try
            {
                var tabla = "TF_DETALLE_COMPRA_INTEG_PDF";//TF_detalle_COMPRA_INTEG_PDF
                switch (tipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tabla = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.MAIL:
                        tabla = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.WORD:
                        tabla = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.EXCEL:
                        tabla = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        tabla = "TF_DETALLE_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        tabla = "TF_DETALLE_COMPRA_INTEG_TXT";
                        break;

                }
                InstanceTransferWeb.Open();
                var sql = $"SELECT numped,sku_dimerc,cantidad,precio,subtotal FROM {tabla} where numped = {numped} order by id asc";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    var detalle = new DetalleOrdenCompraIntegracion
                    {
                        NumPed = data["numped"].ToString(),
                        SkuDimerc = data["sku_dimerc"].ToString(),
                        Cantidad = int.Parse(data["cantidad"].ToString()),
                        Precio = int.Parse(data["precio"].ToString()),
                        SubTotal = int.Parse(data["subtotal"].ToString())
                    };
                    detalleItems.Add(detalle);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return detalleItems;
        }

        private static int GetCountProductDescriptionMatchFromHistorialClient(string rutCliente, IEnumerable<string> desc)
        {
            var likes = desc
                .Where(str => str.Length > _tamanioMinimoPalabras)
                .Aggregate("", (current, str) => current + $" AND descripcion LIKE '%{str.Replace("'", "''")}%'");
            var ret = 0;
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"SELECT CODPRO FROM pdf_historial_compra_cliente WHERE rut_cliente = {rutCliente} {likes}";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                //Console.WriteLine($"SQL 0: {sql}");
                while (data.Read())
                    ret++;
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }


        #endregion



        /// <summary>
        /// Obtiene el SKU del producto
        /// </summary>
        /// <param name="ocNumber">Número de Orden</param>
        /// <param name="rutCli">Rut del Cliente</param>
        /// <param name="codCli">Codigo interno de producto del Cliente</param>
        /// <param name="mailFaltantes">Enviar Correo por Codgos no Encontrados</param>
        /// <returns></returns>
        public static string GetSkuDimercFromCodCliente(string ocNumber, string rutCli, string codCli, bool mailFaltantes)
        {
            var ret = "Z446482";
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                InstanceTransferWeb.Open();
                var sql =
                    $"select CODPRO from dm_ventas.RE_CODCLI@prod where RUTCLI = {rutCli} and trim(CODCLI) = '{codCli.Trim()}' and ESTADO = 1";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                ret = data.Read() ? data["CODPRO"].ToString() : "Z446482";
                Console.WriteLine($"SQL:{sql}, \n RET: {ret}");
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
                var razon = GetRazonSocial(rutCli);
                if (ret.Equals("Z446482") || ret.Equals("SIN_SKU"))
                {
                    if (mailFaltantes)
                        Log.Log.SaveItemFaltantes(rutCli, $"{razon} - {rutCli}:", $"Orden N°: {ocNumber}, Producto: {codCli}, no posee pareo de SKU con Dimerc.");
                }
            }
            if (ret.Equals(""))
            {
                //throw new Exception($"No existe Codigo para el producto:\n {codCli}, del cliente {rutCliente}");
                Console.WriteLine($"No existe Codigo para el producto:\n {codCli}, del cliente {rutCli}");
            }
            return ret.Replace("SIN_SKU", "Z446482");
        }

        public static string GetUniNegocio(string rutcli)
        {
            var ret = "";
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"select get_uninegocio(3,{rutcli}) uni_negocio from dual";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["uni_negocio"].ToString();
                }
                data.Close();

            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }

        public static string GetSkuDimercByCodigoUni(string codigouni)
        {
            var ret = "Z446482";
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"Select codpro from dm_ventas.ma_product where codigo_uni ={codigouni}";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["codpro"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }

            return ret;
            
        }

        public static int GetMultiploFromRutClienteCodCli(string rutCli, string codigoCliente)
        {
            var ret = 1;
            try
            {
                InstanceDmVentas.Open();
                var realCodCli = codigoCliente.Equals("Z446482") ? "SIN_SKU" : codigoCliente;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                var sql = $"select multiplo from dm_ventas.re_codcli where rutcli = {rutCli} and {codCliFilter}";
                //Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read() ? int.Parse(data["multiplo"].ToString()) : 1;
                data.Close();
                //Console.WriteLine($"Multiplo: {ret}, SQL: {sql}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static string GetAPINotaAutomatica(string numord, string oc_client)
        {
            var ret = "1";
            var ret_oc = "1";
            try
            {

                InstanceTransferWeb.Open();
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                //Actualizar Pedidos en WE_Notavta
                var sqlUpdate = $"UPDATE dm_ventas.we_notavta SET observ = null, TIPDOC = 'B', PAGADA = 'S', tipweb = 18 WHERE trunc(fecord) = trunc(sysdate) and rutcli in (77398220) and ordweb = {numord} AND NUMNVT IS NULL";
                var commandUpdate = new OracleCommand(sqlUpdate, InstanceTransferWeb);
                Console.WriteLine(sqlUpdate);
                var dataUpdate = commandUpdate.ExecuteReader();
                //Obtener numord's
                var sql = $"select numord from dm_ventas.we_notavta a where rutcli in (77398220) and trunc(fecord) = trunc(sysdate) and numnvt is null order by numord desc";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["numord"].ToString();
                }

                //Verificar si orden ya tiene nota de venta
                var sqlOc = $"select numnvt from dm_ventas.we_notavta@prod a where facnom = '{oc_client}' and rutcli in (77398220) and trunc(fecord) = trunc(sysdate) and numnvt is not null";
                var commandOc = new OracleCommand(sqlOc, InstanceTransferWeb);
                var dataOc = command.ExecuteReader();
                if (dataOc.Read().Equals(""))
                {
                    ret_oc = dataOc["numnvt"].ToString();
                }
                //data.Close();
                if (!ret_oc.Equals("") & !string.IsNullOrEmpty(ret_oc) & ret_oc.Equals("1"))
                {
                    var notVta = "1";
                    var url = $"https://apis.dimerc.cl/automations/public/telemarketing_order/66784baf03c356b8006ff51f5aa5b156/order/create/nvtaSapLegado/{ret}/";
                    var request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = "GET";
                    request.ContentType = "application/json";
                    request.Accept = "application/json";
                    try
                    {
                        using (WebResponse response = request.GetResponse())
                        {

                            using (System.IO.Stream strReader = response.GetResponseStream())
                            {
                                if (strReader == null) return ret;
                                using (System.IO.StreamReader objReader = new System.IO.StreamReader(strReader))
                                {
                                
                                string responseBody = objReader.ReadToEnd();
                                // Do something with responseBody
                                Console.WriteLine("OC: " + oc_client);
                                Console.WriteLine("Response: " + responseBody);
                                }
                            }
                        }
                    }
                    catch (WebException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }

       
        public static int GetMultiploFromRutClienteCodPro(string rutCli, string codPro)
        {
            var ret = 1;
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                InstanceDmVentas.Open();
                var sql = $"select multiplo from dm_ventas.re_codcli where rutcli = {rutCli} and codpro = '{codPro}'";
                //Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read() ? int.Parse(data["multiplo"].ToString()) : 1;
                data.Close();
                //Console.WriteLine($"Multiplo: {ret}, SQL: {sql}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static double GetMultiploFromRutClienteCodProbch(string rutCli, string codPro)
        {
            double ret = 1;
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return ret;
                InstanceDmVentas.Open();
                var sql = $"select multiplo from dm_ventas.re_codcli where rutcli = {rutCli} and codpro = '{codPro}'";
                //Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read() ? Convert.ToDouble(data["multiplo"].ToString()) : 1;
                data.Close();
                //Console.WriteLine($"Multiplo: {ret}, SQL: {sql}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }
        private static bool GetCenCosFromRutCliente(string rutCliente, string ccosto)
        {
            var ret = false;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"select CENCOS from DM_TRANSFER_WEB.TF_RE_CCTOCLI where RUTCLI = {rutCliente} and CCOSTO = '{ccosto}'";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read();
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }


        /// <summary>
        /// Busca Centro de Costo con Descripción Exacta
        /// </summary>
        /// <param name="ocCliente">Orden de Compra del Cliente</param>
        /// <param name="rutCliente">Rut del Cliente</param>
        /// <param name="ccosto">Descripcion Centro de Costo</param>
        /// <returns></returns>
        public static string GetCenCosFromRutCliente(string ocCliente, string rutCliente, string ccosto)
        {
            var ret = "";
            var existCencos = true;
            try
            {
                var tabla = "RE_CCTOCLI";
                if (rutCliente.Equals("81698900"))
                {
                    tabla = "TF_RE_CCTOCLI";
                }
                InstanceTransferWeb.Open();
                var sql = $"select CENCOS from {tabla} where RUTCLI = {rutCliente} and CCOSTO = '{ccosto}' order by cencos desc";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["CENCOS"].ToString();
                }
                else
                {
                    //estado = EstadoOrden.CentroCostoNoPareado;
                    ret = "0";
                    existCencos = false;
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
                if (!existCencos)
                {
                    Log.Log.SaveCentroCostoFaltantes(ocCliente, rutCliente, ccosto);
                    //throw new Exception($"No existe CenCos para el CCOSTO:\n {ccosto}, del cliente {rutCliente}");
                    Console.WriteLine($"No existe CenCos para el CCOSTO:\n{ccosto}, del cliente {rutCliente}");
                }
            }
            return ret;
        }


        /// <summary>
        /// Busca Centro de Costo con Descripción Exacta
        /// </summary>
        /// <param name="ocCliente">Orden de Compra del Cliente</param>
        /// <param name="rutCliente">Rut del Cliente</param>
        /// <param name="ccosto">Descripcion Centro de Costo</param>
        /// <returns></returns>
        public static ResultadoCentroCosto GetCenCosDescripcionExactaFromRutClienteToResultadoCentroCosto(string ocCliente, string rutCliente, string ccosto)
        {
            var ret = "";
            var existCencos = true;
            try
            {
                InstanceTransferWeb.Open();
                var sql = $"select CENCOS from TF_RE_CCTOCLI where RUTCLI = {rutCliente} and CCOSTO = '{ccosto}'";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["CENCOS"].ToString();
                }
                else
                {
                    //estado = EstadoOrden.CentroCostoNoPareado;
                    ret = "0";
                    existCencos = false;
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
                if (!existCencos)
                {
                    Log.Log.SaveCentroCostoFaltantes(ocCliente, rutCliente, ccosto);
                    //throw new Exception($"No existe CenCos para el CCOSTO:\n {ccosto}, del cliente {rutCliente}");
                    Console.WriteLine($"No existe CenCos para el CCOSTO:\n{ccosto}, del cliente {rutCliente}");
                }
            }
            return new ResultadoCentroCosto
            {
                Cencos = existCencos ? ret : "-1",
                EstadoOrden = existCencos ? EstadoOrden.POR_PROCESAR : EstadoOrden.CENTRO_COSTO_NO_PAREADO
            };
        }

        public static string getCencosByDescripcionDeCliente(string rutcli, string cencos)
        {
            var centroCosto = "-1";
            InstanceDmVentas.Open();
            try
            {

                var sql = $"select cencos from de_cliente where codemp = 3 and rutcli ={rutcli} and  replace(replace(upper(DESCCO),' -','-'),'- ','-') LIKE '%{cencos}%' or (upper(descco) like '{cencos}' and rutcli = {rutcli})";
                Console.WriteLine("verificando descripcion centro costo\n" + sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    centroCosto = data["CENCOS"].ToString();
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return centroCosto;
        }

        public static ResultadoCentroCosto getCencosByDescripcionDeClienteTATA(string rutcli, string cencos)
        {
            var centroCosto = "";
            InstanceDmVentas.Open();
            var rcc = new ResultadoCentroCosto();
            rcc.Cencos = "";
            rcc.EstadoOrden = EstadoOrden.POR_PROCESAR;
            var split = cencos.Split(' ');
            var contador = split.Length - 1;
            var concatQuery = "";      
            try
            {
                var sqlnephro = "";
                if (rutcli.Equals("99507130"))
                {
                    sqlnephro = $" and (upper(descco) not like '%BLOQUEADO%')";
                }

                for (; contador <= split.Length; contador--)
                {
                    if(contador>0 && contador < split.Length - 1)
                    {
                        concatQuery += $" and (upper(descco) like '%{split[contador].ToUpper()}%')";
                    }
                    else{
                        concatQuery += $" and (upper(descco) like '%{split[contador].ToUpper()}%')";
                    }
                    
                    var sql = $"select count(*) cantidad from de_cliente where rutcli = {rutcli} {concatQuery}  and codemp = 3 {sqlnephro}";
                    Console.WriteLine("verificando descripcion centro costo " + sql);
                    var command = new OracleCommand(sql, InstanceDmVentas);
                    var data = command.ExecuteReader();
                    if (data.Read())
                    {
                        var cantidad  = data["CANTIDAD"].ToString();
                        Console.WriteLine("cantidad resultados"  + cantidad);
                        if (cantidad.Equals("1"))
                        {
                            var sql2 = $"select CENCOS from de_cliente where rutcli = {rutcli} {concatQuery} and codemp = 3 {sqlnephro} order by cencos asc";
                            Console.WriteLine(" " + sql2);
                            var command2 = new OracleCommand(sql2, InstanceDmVentas);
                            var data2 = command2.ExecuteReader();
                            if (data2.Read())
                            {
                                centroCosto = data2["CENCOS"].ToString();
                                rcc.Cencos = centroCosto;
                                rcc.EstadoOrden = EstadoOrden.POR_PROCESAR;
                                data2.Close();
                                break;
                            }
                        }
                    }
                    data.Close();

                }
                //var sql = $"select cencos from de_cliente where rutcli ={rutcli} and  DESCCO LIKE '%{cencos}%' or (cencos like '{cencos}' and rutcli = {rutcli}) and codemp = 3";

                
                                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return rcc;
        }
        public static string RemoveAccentsWithNormalization(string inputString)
        {
            string normalizedString = inputString.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < normalizedString.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(normalizedString[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(normalizedString[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        public static string getCencosByDescripcionDeClienteHDIContacto(string rutcli, string cencos)
        {

            var centroCosto = "";
            var arreglo = cencos.Split('-');
            //var centroCostoNum = arreglo[0];
            var centroCostoNum = cencos;
            Match match = Regex.Match(centroCostoNum, @"\d{1,}\-");
            centroCostoNum = match.Value.Replace("-", "");
            var centroCostoNombre = arreglo[1];
            var contacto = arreglo[arreglo.Length - 1];
            centroCosto = centroCostoNum;
            var descripcion = arreglo[1];
            //var contacto = cencos.Split('-')[1].Trim().ToUpper().Split(' ')[0];
            
            InstanceDmVentas.Open();
            try
            {

                var sql = $"select cencos from de_cliente where rutcli = {rutcli} and upper(descco) like '%{centroCosto.ToUpper()}%' and upper(descco) like '%{descripcion.ToUpper()}%' and upper(contac) like '%{contacto.ToUpper().Split(' ')[0]}%'";
                //var sql = $"select cencos from de_cliente where rutcli = {rutcli} and descco ='{cencos}' and contacto like '%{contacto}%'";
                Console.WriteLine("verificando descripcion centro costo \n" + sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    centroCosto = data["CENCOS"].ToString();
                }
                else
                {
                    centroCosto = "-1";
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return centroCosto;
        }

        public static ResultadoCentroCosto getCencosByDescripcionDeClienteHDI(string rutcli, string cencos)
        {
            var centroCosto = "";
            cencos = RemoveAccentsWithNormalization(cencos);
            Match match = Regex.Match(cencos, @"(\s\d{1,}\-|\d{1,}\-)");
            var codcc = match.Value.Replace("-","").Trim();
            InstanceDmVentas.Open();
            var rcc = new ResultadoCentroCosto();
            rcc.Cencos = "";
            rcc.EstadoOrden = EstadoOrden.POR_PROCESAR;
            var split = cencos.Split(' ');
            var contador = split.Length - 1;
            //var contador = 0;
            var concatQuery = "";
            string[] auxConcatQuery;
            try
            {
                for (; contador < split.Length; contador--)
                {
                    if (contador > 0 && contador < split.Length - 1)
                    {
                        auxConcatQuery = split[contador].ToUpper().Split('-');
                        concatQuery += $" and (upper(descco) like '% {auxConcatQuery[auxConcatQuery.Length - 1]} %')";
                    }
                    else
                    {
                        auxConcatQuery = split[contador].ToUpper().Split('-');
                        concatQuery += $" and (upper(descco) like '%{auxConcatQuery[auxConcatQuery.Length-1]}%')";
                    }

                    var sql = $"select count(*) cantidad from de_cliente where upper(descco) like '%{codcc}%' and rutcli = {rutcli} {concatQuery}  and codemp = 3";
                    Console.WriteLine("verificando descripcion centro costo " + sql);
                    var command = new OracleCommand(sql, InstanceDmVentas);
                    var data = command.ExecuteReader();
                    if (data.Read())
                    {
                        var cantidad = data["CANTIDAD"].ToString();
                        if (cantidad.Equals("0"))
                        {
                            continue;
                        }
                        Console.WriteLine("cantidad resultados " + cantidad);
                        int c = Convert.ToInt32(cantidad);
                        Console.WriteLine($"contador{contador}");
                        if (c == 1 || c==2 || c==3)
                        {
                            var sql2 = $"select CENCOS from de_cliente where descco like '%{codcc}%' and rutcli = {rutcli} {concatQuery} and codemp = 3 order by cencos asc";
                            Console.WriteLine(" " + sql2);
                            var command2 = new OracleCommand(sql2, InstanceDmVentas);
                            var data2 = command2.ExecuteReader();
                            if (data2.Read())
                            {
                                centroCosto = data2["CENCOS"].ToString();
                                rcc.Cencos = centroCosto;
                                rcc.EstadoOrden = EstadoOrden.POR_PROCESAR;
                                data2.Close();
                                break;
                            }
                        }
                    }
                    data.Close();

                }
                //var sql = $"select cencos from de_cliente where rutcli ={rutcli} and  DESCCO LIKE '%{cencos}%' or (cencos like '{cencos}' and rutcli = {rutcli}) and codemp = 3";



            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return rcc;
        }

        public static string getCencosByDesccoContac(string rutcli, string cencos)
        {
            var centroCosto = cencos.Split('-')[0].Trim().ToUpper();
            //var contacto = cencos.Split('-')[1].Trim().ToUpper().Split(' ')[0];
            var contacto = cencos.Split('-')[1].Trim().ToUpper();
            var contactoarr = contacto.Split(' ');
            var ccret = "";
            var rcc = new ResultadoCentroCosto();
            var concatQuery = $"upper(descco) like '%{centroCosto}%' and upper(contac) like '%{contacto}%'";
            if (contactoarr.Length > 1)
            {
                //concatQuery = $"upper(descco) like '%{centroCosto}%' and upper(contac) like '%{contactoarr[0].ToUpper()}%' and upper(contac) like '%{contactoarr[contactoarr.Length-1].ToUpper()}%' ";
                concatQuery = $"upper(descco) like '%{centroCosto}%' and upper(contac) like '%{contactoarr[0].ToUpper()}%'  ";
            }
            InstanceDmVentas.Open();
            try
            {

                var sql = $"select cencos from de_cliente where rutcli = {rutcli} and {concatQuery} ";
                //var sql = $"select cencos from de_cliente where rutcli = {rutcli} and descco ='{cencos}' and contacto like '%{contacto}%'";
                Console.WriteLine("verificando descripcion centro costo \n" + sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ccret = data["CENCOS"].ToString();
                }
                
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return ccret;
        }

        public static bool ExisteCcTelemarketingBCH(string rutcli, string cencos)
        {
            var centroCosto = "1";
            bool respuesta = false;
           // var contacto = cencos.Split('-')[1].Trim();
            InstanceDmVentas.Open();
            try
            {

                var sql = $"select cencos from de_cliente where rutcli = {rutcli} and cencos = {cencos}";                
                Console.WriteLine("verificando existencia centro costo \n" + sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    respuesta = true;
                }
                else
                {
                    respuesta = false;
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return respuesta;
        }

        public static string GetCencosByCCostoContacto(string rutcli, string cencos)
        {
            var centroCosto = cencos.Split('-')[0].Trim();
            //var contacto = cencos.Split('-')[1].Trim().ToUpper().Split(' ')[0];
            var contacto = cencos.Split('-')[1].Trim();
            InstanceDmVentas.Open();
            try
            {

                 var sql = $"select cencos from DM_TRANSFER_WEB.TF_re_cctocli where rutcli = {rutcli} and ccosto ='{centroCosto}' and contacto like '%{contacto}%'";
                //var sql = $"select cencos from de_cliente where rutcli = {rutcli} and descco ='{cencos}' and contacto like '%{contacto}%'";
                Console.WriteLine("verificando descripcion centro costo \n" + sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    centroCosto = data["CENCOS"].ToString();
                }
                else
                {
                    centroCosto = "-1";
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return centroCosto;
        }

        public static string GetCencosByCCostoContactoHDI(string rutcli, string cencos)
        {
            var centroCosto = "";
            var arreglo = cencos.Split('-');
            //var centroCostoNum = arreglo[0];
            var centroCostoNum = cencos;
            Match match = Regex.Match(centroCostoNum, @"\d{1,}\-");
            centroCostoNum = match.Value.Replace("-","");
            var centroCostoNombre = arreglo[1];
            var contacto = arreglo[arreglo.Length-1];
            InstanceDmVentas.Open();
            try
            {

                var sql = $"select cencos from DM_TRANSFER_WEB.tf_re_cctocli where rutcli = {rutcli} and ccosto like '%{centroCostoNum} {centroCostoNombre}%' and upper(TRANSLATE(contacto, 'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) like '%{contacto}%'";
                //var sql = $"select cencos from de_cliente where rutcli = {rutcli} and descco ='{cencos}' and contacto like '%{contacto}%'";
                Console.WriteLine("verificando descripcion centro costo \n" + sql);
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    centroCosto = data["CENCOS"].ToString();
                }
                else
                {
                    centroCosto = "-1";
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            InstanceDmVentas.Close();
            return centroCosto;
        }


        private static bool ExistCenCosFromRutCliente(string rutcli, string cencos)
        {
            var ret = false;
            try
            {
                InstanceTransferWeb.Open();
                var sql = $" SELECT * FROM re_cctocli WHERE rutcli = {rutcli} AND cencos = {cencos}";
                //Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                ret = data.Read();
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            return ret;
        }


        public static ResultadoCentroCosto GetCenCosFromRutClienteAndDescCencosWithMatchToResultadoCentroCosto(string ocCliente, string rutCli, string ccosto, EstadoOrden estado)
        {
            Console.WriteLine("==================================MATCH====================000");
            var ret1 = GetCenCosFromRutClienteAndDescCencos(ocCliente, rutCli, ccosto, false);
            if (!ret1.Equals("0"))
                return new ResultadoCentroCosto
                {
                    EstadoOrden = EstadoOrden.POR_PROCESAR,
                    Cencos = ret1
                };
            var split = ccosto.Split(' ');
            var descContainsRow = new List<string>();
            var index = 0;
            var ret = "0";
            var lasRowCount = 0;
            foreach (var t in split.Where(t => t.Length > _tamanioMinimoPalabras))
            {
                descContainsRow.Add(t);
                var rows = GetCountDescCencosMatch(rutCli, descContainsRow);
                lasRowCount = rows;
                if (rows == 0)
                    descContainsRow.RemoveAt(index);
                else
                    index++;
                if (rows >= 1)
                {
                    ret = GetCenCosFromRutClienteAndDescCencos(ocCliente, rutCli,
                        descContainsRow.ToArray().ArrayToString(0, descContainsRow.Count - 1), true);

                    Console.WriteLine($"ret2: {ret}");
                }
            }
            if (lasRowCount == 0 && ret.Equals("0"))
            {
                Log.Log.SaveCentroCostoFaltantes(ocCliente, rutCli, ccosto);
                return new ResultadoCentroCosto
                {
                    EstadoOrden = EstadoOrden.CENTRO_COSTO_NO_PAREADO,
                    Cencos = "-1"
                };
            }
            Console.WriteLine($"ret3: {ret}");

            return new ResultadoCentroCosto
            {
                EstadoOrden = EstadoOrden.POR_PROCESAR,
                Cencos = ret
            };
        }


        public static string GetCenCosFromRutClienteAndDescCencosWithMatch(string ocCliente, string rutCli, string ccosto, EstadoOrden estado)
        {
            Console.WriteLine("==================================MATCH====================000");
            var ret1 = GetCenCosFromRutClienteAndDescCencos(ocCliente, rutCli, ccosto, false);
            Console.WriteLine($"ret1: {ret1}");
            if (!ret1.Equals("0")) return ret1;
            var split = ccosto.Split(' ');
            var descContainsRow = new List<string>();
            var index = 0;
            var ret = "0";
            var lasRowCount = 0;
            foreach (var t in split.Where(t => t.Length > _tamanioMinimoPalabras))
            {
                descContainsRow.Add(t);
                var rows = GetCountDescCencosMatch(rutCli, descContainsRow);
                lasRowCount = rows;
                if (rows == 0)
                    descContainsRow.RemoveAt(index);
                else
                    index++;
                if (rows >= 1)
                {
                    ret = GetCenCosFromRutClienteAndDescCencos(ocCliente, rutCli,
                        descContainsRow.ToArray().ArrayToString(0, descContainsRow.Count - 1), true);

                    Console.WriteLine($"ret2: {ret}");
                }
            }
            if (lasRowCount == 0 && ret.Equals("0"))
            {
                Log.Log.SaveCentroCostoFaltantes(ocCliente, rutCli, ccosto);
            }
            Console.WriteLine($"ret3: {ret}");

            return ret;
        }

        /// <summary>
        /// Busca Centro de Costo con Simples Coincidencias utilizando toda la Descripcion del 
        /// </summary>
        /// <param name="ocCliente">Oc del Cliente</param>
        /// <param name="rutCli">Rut Cliente</param>
        /// <param name="ccosto">Descripción Centro Costo</param>
        /// <param name="sendEmail">Enviar Mail?</param>
        /// <returns></returns>
        public static string GetCenCosFromRutClienteAndDescCencos(string ocCliente, string rutCli, string ccosto, bool sendEmail)
        {
            var num = 0;
            //if (int.TryParse(ccosto, out num)) return ccosto;
            var existCencos = true;
            var ret = "";
            var aux = ccosto.NormalizeCentroCostoDavila();
            var split = aux.Split(' ');
            var sufix = split.Where(cc => !cc.Equals("")).Aggregate("", (current, cc) => current + $" and ccosto like '%{cc}%'");
            try
            {


                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return "1";
                InstanceTransferWeb.Open();
                var sql = $"select CENCOS from RE_CCTOCLI where RUTCLI = {rutCli} {sufix}";
                Console.WriteLine("Obteniendo centro de costo ->" + sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                Console.WriteLine(sql);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["CENCOS"].ToString();

                    if (rutCli.Contains("76098454") && ret == ccosto)
                    {
                        ret = "-1";
                    }

                    if (rutCli.Equals("60503000"))
                    {
                        ret = "-1";
                    }
                }
                else
                {
                    if (rutCli.Equals("60503000"))
                    {
                        ret = "-1";
                    }
                    else
                    {
                        ret = "0";
                    }
                    
                    existCencos = false;

                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
                if (!existCencos && sendEmail)
                {
                    Log.Log.SaveCentroCostoFaltantes(ocCliente, rutCli, ccosto);
                    Console.WriteLine($"No existe CenCos para el CCOSTO:\n{ccosto}," +
                                      $" del cliente {rutCli}");
                }
            }
            return ret;
        }





        public static ResultadoCentroCosto GetCenCosFromRutClienteAndDescCencosToResultadoCentroCosto(string ocCliente, string rutCli, string ccosto, bool sendEmail)
        {
            var num = 0;
            //if (int.TryParse(ccosto, out num)) return ccosto;
            var existCencos = true;
            var ret = "";
            //var aux = ccosto.NormalizeCentroCostoDavila();
            var split = ccosto.Split(' ');
            var sufix = split.Where(cc => !cc.Equals("")).Aggregate("", (current, cc) => current + $" and ccosto like '%{cc}%'");
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd())
                    return new ResultadoCentroCosto
                    {
                        EstadoOrden = EstadoOrden.POR_PROCESAR,
                        Cencos = "1"
                    };
                InstanceTransferWeb.Open();
                var sql = $"select CENCOS from TF_RE_CCTOCLI where RUTCLI = {rutCli} {sufix}";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                Console.WriteLine(sql);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["CENCOS"].ToString();
                }
                else
                {
                    ret = "0";
                    existCencos = false;

                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
                if (!existCencos && sendEmail)
                {
                    Log.Log.SaveCentroCostoFaltantes(ocCliente, rutCli, ccosto);
                    Console.WriteLine($"No existe CenCos para el CCOSTO:\n{ccosto}," +
                                      $" del cliente {rutCli}");
                }
            }
            return new ResultadoCentroCosto
            {
                EstadoOrden = existCencos ? EstadoOrden.POR_PROCESAR : EstadoOrden.CENTRO_COSTO_NO_PAREADO,
                Cencos = existCencos ? ret : "-1"
            };
        }





        public static bool ExistProductReCodCli(string rutcli
            , string codpro
            , string codigoCliente
            , string descripcionCliente = "")
        {
            var ret = false;
            try
            {
                var sufix = $"LIKE '{descripcionCliente.Replace("'", "''")}'";
                sufix = descripcionCliente.Equals("")
                    ? ""
                    : $"AND DESCRIPCION_CLIENTE {sufix}";
                InstanceDmVentas.Open();
                var realCodCli = codigoCliente.Equals("Z446482") ? "SIN_SKU" : codigoCliente;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                var sql = $"SELECT 1 FROM dm_ventas.RE_CODCLI WHERE RUTCLI = {rutcli} AND CODPRO = '{codpro}' AND {codCliFilter} {sufix}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read();
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static void UpdateReCodCli(string rutcli, string codcli, string codpro, string descripcionCliente)
        {
            using (var command = new OracleCommand())
            {
                var sql = "UPDATE dm_ventas.RE_CODCLI SET FORMA_PAREO = -1 " +
                          $"WHERE RUTCLI = {rutcli} AND CODCLI ='{codcli}' AND CODPRO = '{codpro}' AND DESCRIPCION_CLIENTE = '{descripcionCliente}' ";
                OracleTransaction trans = null;
                command.Connection = InstanceDmVentas;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                try
                {
                    InstanceDmVentas.Open();
                    trans = InstanceDmVentas.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceDmVentas?.Close();

                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    InstanceDmVentas?.Close();
                }
                catch (OracleException)
                {
                    trans?.Rollback();
                    InstanceDmVentas?.Close();
                }
            }
            InstanceDmVentas?.Close();
        }

        public static void InsertIntoReCcToCli(string rutcli, string cc)
        {
            var exist = GetCenCosFromRutCliente(rutcli, cc);
            if (exist) return;
            using (var command = new OracleCommand())
            {
                var sql = $"INSERT INTO RE_CCTOCLI(RUTCLI,CENCOS,CCOSTO) VALUES ({rutcli},{cc},'{cc}') ";
                OracleTransaction trans = null;
                command.Connection = InstanceDmVentas;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                try
                {
                    InstanceDmVentas.Open();
                    trans = InstanceDmVentas.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceDmVentas?.Close();

                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    InstanceDmVentas?.Close();
                }
                catch (OracleException)
                {
                    trans?.Rollback();
                    InstanceDmVentas?.Close();
                }
            }

            InstanceDmVentas?.Close();

            Console.Write("");
        }



        public static void InsertIntoReCodCli(string rutcli, string codcli, string codpro, string descripcionCliente, TipoPareoReCodCli pareo)
        {
            var existCodProgCodCliDescripcionCliente = ExistProductReCodCli(rutcli, codpro, codcli, descripcionCliente);
            var existCodproCodCli = ExistProductReCodCli(rutcli, codpro, codcli);
            if (existCodProgCodCliDescripcionCliente) return;
            using (var command = new OracleCommand())
            {
                var codigoProducto =
                    codpro.Equals("SIN_SKU")
                    && codcli.Equals("SIN_SKU")
                        ? $"PAREAR{new Random().Next(999)}"
                        : codpro;
                pareo = existCodproCodCli
                    ? TipoPareoReCodCli.PAREO_EXISTENTE_DESCRIPCION_DIFERENTE
                    : pareo;
                codcli = existCodproCodCli
                    ? codcli.Equals("SIN_SKU")
                        ? $"SIN_SKU{new Random().Next(999)}"
                        : codcli : codcli;
                //var sql = "INSERT INTO dm_ventas.RE_CODCLI(RUTCLI,CODPRO,CODCLI,DESCRIPCION,DESCRIPCION_CLIENTE,ESTADO, TIPOPAREO,FORMA_PAREO) " +
                //          $"VALUES({rutcli},'{codigoProducto}','{codcli}',GETDESCRIPCION('{codpro}'),'{descripcionCliente.Replace("'", "''")}',1,1,{(int)pareo}) ";
                var sql = "Select codcli from dm_ventas.re_codcli;";
                Console.WriteLine(sql);
                OracleTransaction trans = null;
                command.Connection = InstanceDmVentas;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                try
                {
                    InstanceDmVentas.Open();
                    trans = InstanceDmVentas.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceDmVentas?.Close();

                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    InstanceDmVentas?.Close();
                }
                catch (OracleException)
                {
                    trans?.Rollback();
                    InstanceDmVentas?.Close();
                }
            }

            InstanceDmVentas?.Close();

            Console.Write("");
        }

        public static bool InsertRawPdfTextDivido(int algoritmo, OrdenCompraIntegracion ocAdapter
            , string pdfLastName, string pdfText, OrdenCompra.OrdenCompra oc, int parteDividida
            , bool variosPedidosPorOrdenPdf
            , string md5)
        {
            if (pdfText.Length == 0) return true;
            using (var command = new OracleCommand())
            {
                var numped = variosPedidosPorOrdenPdf ? $"{algoritmo}" : ocAdapter.NumPed;
                var sql = $"INSERT INTO PDF_RAW_INTEGRACION (NUMPED,RUTCLI,ALGORITMO,NOMBRE_PDF,FECHA,TEXTO_PDF,TEXTO_DIVIDIDO,PARTE,MD5) VALUES ({numped},{oc.Rut},{algoritmo},'{pdfLastName}',sysdate,to_clob('{pdfText.Replace("\n", "')|| chr(13) ||to_clob('")}'),1,{parteDividida},'{md5}')";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                //Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug()) return true;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceTransferWeb?.Close();
                    return true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos RAW_INTEGRACION_PDF");
                    return false;
                }
            }
        }



        public static bool InsertRawPdfText(int algoritmo
            , OrdenCompraIntegracion ocAdapter
            , string pdfLastName
            , string pdfText
            , OrdenCompra.OrdenCompra oc
            , bool variosPedidosPorOrdenPdf
            , string md5)
        {
            // if(pdfText.Length> 10000)
            // { 
            //     var count = 1;
            //     while (pdfText.Length > 10000)
            //     {
            //         var lastString = pdfText.Substring(10001);
            //         InsertRawPdfTextDivido(algoritmo, ocAdapter, pdfLastName, pdfText.Substring(0, 10000), oc, count, variosPedidosPorOrdenPdf, md5);
            //         pdfText = lastString;
            //         count++;
            //     }
            //     return InsertRawPdfTextDivido(algoritmo, ocAdapter, pdfLastName, pdfText, oc, count++,
            //         variosPedidosPorOrdenPdf, md5);
            //} 
            using (var command = new OracleCommand())
            {
                var numped = variosPedidosPorOrdenPdf ? $"{algoritmo}" : ocAdapter.NumPed;
                var sql = $"INSERT INTO DM_TRANSFER_WEB.PDF_RAW_INTEGRACION (NUMPED,RUTCLI,ALGORITMO,NOMBRE_PDF,FECHA,TEXTO_PDF, TEXTO_DIVIDIDO, PARTE, CODEMP, MD5) VALUES ({numped},{oc.Rut},{algoritmo},'{pdfLastName}',sysdate,to_clob('{pdfText.Replace("\n", "')|| chr(13) ||to_clob('")}'),0,1,3,'{md5}')";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug()) return true;
                try
                {
                    InstanceTransferWeb.Close();
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceTransferWeb?.Close();
                    return true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos RAW_INTEGRACION_PDF");
                    return false;
                }
            }
        }



        public static bool InsertRawOrdenCompra(
            int algoritmo
            , OrdenCompra.OrdenCompra oc
            , string numeroPedido)
        {
            var obs = $"OC Cliente: {oc.NumeroCompra}, {oc.Observaciones}";
            obs = obs.Length >= 200 ? obs.Substring(0, 199) : obs;
            using (var command = new OracleCommand())
            {
                var table = "PDF_RAW_EN_ORDEN";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        table = "PDF_RAW_EN_ORDEN";
                        break;
                    case TipoIntegracion.MAIL:
                        table = "MAIL_RAW_EN_ORDEN";
                        break;
                }
                var sql = $"INSERT INTO {table} (NUMPED, FECHA ,ALGORITMO, RUTCLI,TIPO_PAREO_CENCOS, CENCOS,DIRECCION,OC_CLIENTE,OBS) VALUES ({numeroPedido},SYSDATE ,{algoritmo},{oc.Rut},'{oc.TipoPareoCentroCosto}','{oc.CentroCosto}','{oc.Direccion}','{oc.NumeroCompra}','{obs.Replace("'", "''")}')";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                //Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug()) return true;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    //rq     command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceTransferWeb?.Close();
                    var cont = 0;
                    if (oc.Items.Count == 0) return true;
                    foreach (var item in oc.Items)
                    {
                        if (InsertRawDetalleOrdenCompra(cont, numeroPedido, item, oc.TipoIntegracion)) cont++;
                        else break;
                    }
                    if (cont == oc.Items.Count) return true;
                    DeleteDetalleRawOrdenCompraPDF(numeroPedido);
                    DeleteRawOrdenCompraPdf(numeroPedido);
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos");
                    return false;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos");
                    return false;
                }
            }
        }

        private static bool ExistOrdenCompraIntegracion(OrdenCompraIntegracion oc, string md5 = null)
        {

            var ret = false;
            var numpeds = new List<string>();
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return false;
                if (md5 != null)
                    return ExistOrdenCompraIntegracionWithMD5(md5, oc.TipoIntegracion);
                InstanceTransferWeb.Open();
                var cabecera = "tf_compra_integ_pdf";
                var detalle = "tf_compra_integ_pdf";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        cabecera = "TF_COMPRA_INTEG_PDF";
                        detalle = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.MAIL:
                        cabecera = "TF_COMPRA_INTEG_MAIL";
                        detalle = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.WORD:
                        cabecera = "TF_COMPRA_INTEG_WORD";
                        detalle = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.EXCEL:
                        cabecera = "TF_COMPRA_INTEG_EXCEL";
                        detalle = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        cabecera = "TF_COMPRA_INTEG_CA_ES";
                        detalle = "TF_DETALLE_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        cabecera = "TF_COMPRA_INTEG_TXT";
                        detalle = "TF_DETALLE_COMPRA_INTEG_TXT";
                        break;

                }
                var sql =
                    $"SELECT a.numped FROM {cabecera} a JOIN {detalle} b ON a.numped = b.numped WHERE TRUNC (fecha) = TRUNC(SYSDATE) AND A.RUTCLI = {oc.RutCli} AND A.CENCOS = {oc.CenCos} AND ESTADO in ({(int)EstadoOrden.PROCESADO}, {(int)EstadoOrden.POR_PROCESAR}) AND A.OBS = '{oc.Observaciones.Replace("'", "''")}' AND A.OC_CLIENTE = '{oc.OcCliente}' GROUP BY a.numped HAVING COUNT(b.sku_dimerc) = {oc.DetallesCompra.Count}";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    ret = true;
                    numpeds.Add(data["numped"].ToString());
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            InstanceTransferWeb?.Close();
            if (!ret) return false;
            foreach (var num in numpeds)
            {
                if (oc.EqualsOrdenCompraProcesada(GetOrdenCompraIntegracionFromNumped(num, oc.TipoIntegracion))) return true;
            }
            return false;
        }

        private static bool ExistOrdenCompraIntegracionBCH(OrdenCompraIntegracionBCH oc, string md5 = null)
        {

            var ret = false;
            var numpeds = new List<string>();
            try
            {
                if (!InternalVariables.InternalVariables.UseConnectionBdd()) return false;
                if (md5 != null)
                    return ExistOrdenCompraIntegracionWithMD5(md5, oc.TipoIntegracion);
                InstanceTransferWeb.Open();
                var cabecera = "tf_compra_integ_pdf";
                var detalle = "tf_compra_integ_pdf";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        cabecera = "TF_COMPRA_INTEG_PDF";
                        detalle = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.MAIL:
                        cabecera = "TF_COMPRA_INTEG_MAIL";
                        detalle = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.WORD:
                        cabecera = "TF_COMPRA_INTEG_WORD";
                        detalle = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.EXCEL:
                        cabecera = "TF_COMPRA_INTEG_EXCEL";
                        detalle = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        cabecera = "TF_COMPRA_INTEG_CA_ES";
                        detalle = "TF_DETALLE_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        cabecera = "TF_COMPRA_INTEG_TXT";
                        detalle = "TF_DETALLE_COMPRA_INTEG_TXT";
                        break;

                }
                var sql =
                    $"SELECT a.numped FROM {cabecera} a JOIN {detalle} b ON a.numped = b.numped WHERE TRUNC (fecha) = TRUNC(SYSDATE) AND A.RUTCLI = {oc.RutCli} AND A.CENCOS = {oc.CenCos} AND ESTADO in ({(int)EstadoOrden.PROCESADO}, {(int)EstadoOrden.POR_PROCESAR}) AND A.OBS = '{oc.Observaciones.Replace("'", "''")}' AND A.OC_CLIENTE = '{oc.OcCliente}' GROUP BY a.numped HAVING COUNT(b.sku_dimerc) = {oc.DetallesCompra.Count}";
                Console.WriteLine(sql);
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                while (data.Read())
                {
                    ret = true;
                    numpeds.Add(data["numped"].ToString());
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            InstanceTransferWeb?.Close();
            if (!ret) return false;
            foreach (var num in numpeds)
            {
                if (oc.EqualsOrdenCompraProcesada(GetOrdenCompraIntegracionFromNumped(num, oc.TipoIntegracion))) return true;
            }
            return false;
        }


        private static bool ExistOrdenCompraIntegracionWithMD5(string md5, TipoIntegracion tipoIntegracion)
        {

            var ret = false;
            try
            {
                InstanceTransferWeb.Open();
                var cabecera = "tf_compra_integ_pdf";
                switch (tipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        cabecera = "TF_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.MAIL:
                        cabecera = "TF_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.WORD:
                        cabecera = "TF_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.EXCEL:
                        cabecera = "TF_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        cabecera = "TF_COMPRA_INTEG_CA_ES";
                        break;

                }
                var sql =
                    $"SELECT 1 AS EXIST FROM {cabecera} WHERE NUMPED IN (SELECT DISTINCT NUMPED FROM PDF_RAW_INTEGRACION WHERE MD5 = '{md5}') AND ESTADO IN(1, 0)";
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                ret = data.Read();
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            InstanceTransferWeb?.Close();
            return ret;
        }

        /// <summary>
        /// Inserta Hash en "PDF_MAIL_HASH para visualizar en entorno grafico orden procesada."
        /// </summary>
        /// <param name="oc">Orden Integración</param>
        private static void InsertHashIntegración(OrdenCompraIntegracion oc)
        {
            using (var command = new OracleCommand())
            {
                var sql = "INSERT INTO PDF_MAIL_HASH (numped, rutcli, HASH, estado)" +
                          $"VALUES({oc.NumPed},{oc.RutCli},MD5({oc.NumPed}||{oc.RutCli}),0)";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible crear el Hash en la Base de Datos.");
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }

        private static void InsertHashIntegraciónBCH(OrdenCompraIntegracionBCH oc)
        {
            using (var command = new OracleCommand())
            {
                var sql = "INSERT INTO PDF_MAIL_HASH (numped, rutcli, HASH, estado)" +
                          $"VALUES({oc.NumPed},{oc.RutCli},MD5({oc.NumPed}||{oc.RutCli}),0)";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible crear el Hash en la Base de Datos.");
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }


        public static bool InsertOrdenCompraIntegracion(OrdenCompraIntegracion oc
            , string filePath = null
            , PDFReader pdfReader = null
            , WordReader wordReader = null
            , ExcelReader excelReader = null
            , TextReader txtReader = null)
        {
            if (!oc.RutCli.ToString().Equals("97004000") && !oc.RutCli.ToString().Equals("99012000") && !oc.RutCli.ToString().Equals("96654180") && !oc.RutCli.ToString().Equals("70072600"))
            {
                oc.Observaciones = $"{oc.Observaciones} - OC: {oc.OcCliente} ";
            }
            oc.Observaciones = oc.Observaciones.Length >= 200 ? oc.Observaciones.Substring(0, 199) : oc.Observaciones;
            var md5 = filePath != null ? FileUtils.FileUtils.GetMD5FromFile(filePath) : "";
            if (ExistOrdenCompraIntegracion(oc, md5))
            {
                oc.EstadoOrden = InternalVariables.InternalVariables.ProcesarOrdenesRepetidas()
                    ? EstadoOrden.POR_PROCESAR
                    : EstadoOrden.ORDEN_REPETIDA;
            }
            using (var command = new OracleCommand())
            {
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug())
                {
                    //Console.WriteLine($"ocAdapterList:\n{oc}");
                   // TraspasaTelemarketingSeparados();
                    return true;
                }
                if (InternalVariables.InternalVariables.IsDebug() && InternalVariables.InternalVariables.InsertDebug())
                {
                    oc.EstadoOrden =
                        oc.EstadoOrden != EstadoOrden.DEBUG
                        && oc.EstadoOrden != EstadoOrden.POR_PROCESAR
                        ? oc.EstadoOrden
                        : EstadoOrden.DEBUG;
                }
                var tableName = "TF_COMPRA_INTEG_PDF";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        tableName = "TF_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_COMPRA_INTEG_TXT";
                        break;
                    case TipoIntegracion.COTIZACION:
                        tableName = "TF_COMPRA_INTEG_COTIZACION";
                        break;

                    case TipoIntegracion.CARGA_ESTANDAR_PRV:
                        tableName = "TF_COMPRA_INTEG_CA_ES_PRV";
                        break;
                }
                if (oc.CenCos.Equals("") || oc.CenCos==null)
                {
                    oc.CenCos = "-1";
                }
                var sql="";
                if (oc.TipoIntegracion == TipoIntegracion.CARGA_ESTANDAR_PRV)
                {
                     sql = $"INSERT INTO {tableName}" +
                          "(NUMPED,RUTCLI,CENCOS,FECHA,DIRECCION,OC_CLIENTE,OBS,ESTADO, RUTCLI_PRV) " +
                          $"VALUES({oc.NumPed},{oc.RutCli},{oc.CenCos},SYSDATE,'{oc.Direccion}'" +
                          $",'{oc.OcCliente}','{oc.Observaciones.Replace("'", "''")}',{(int)oc.EstadoOrden}," +
                          $"{oc.RutCliPrv})";
                }
                else {
                     sql = $"INSERT INTO {tableName}" +
                          "(NUMPED,RUTCLI,CENCOS,FECHA,DIRECCION,OC_CLIENTE,OBS,ESTADO) " +
                          $"VALUES({oc.NumPed},{oc.RutCli},{oc.CenCos},SYSDATE,'{oc.Direccion}'" +
                          $",'{oc.OcCliente}','{oc.Observaciones.Replace("'", "''")}',{(int)oc.EstadoOrden})";
                }
                
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);

                try
                {
                    //InstanceTransferWeb?.Close();
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceTransferWeb?.Close();
                    var cont = 0;
                    if (oc.DetallesCompra.Count == 0) return true;
                    foreach (var detC in oc.DetallesCompra)
                    {
                        if (InsertDetalleOrdenCompraIntegración(cont, detC, oc.TipoIntegracion)) cont++;
                        else break;
                    }
                    if (cont == oc.DetallesCompra.Count)
                    {
                        if (oc.EstadoOrden == EstadoOrden.ORDEN_REPETIDA)
                        {
                            //throw new ErrorIntegracion(
                            //    TipoError.ErrorOrdenRepetida
                            //    , "Se esta intentando procesar una Orden "
                            //    , oc.RutCli.ToString()
                            //    , oc.OcCliente
                            //    , pdfReader.TxtFileName);
                            return true;
                        }
                        InsertHashIntegración(oc);
                        Console.WriteLine($"ocAdapterList:\n{oc}");
                        //TraspasaTelemarketingSeparados(oc);
                        return true;
                    }

                    DeleteOrdenCompraIntegración(oc);
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos");
                    return false;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos");
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }

        public static bool InsertOrdenCompraIntegracionBCH(OrdenCompraIntegracionBCH oc
            , string filePath = null
            , PDFReader pdfReader = null
            , WordReader wordReader = null
            , ExcelReader excelReader = null
            , TextReader txtReader = null)
        {
            if (!oc.RutCli.ToString().Equals("97004000") && !oc.RutCli.ToString().Equals("99012000") && !oc.RutCli.ToString().Equals("96654180"))
            {
                oc.Observaciones = $"{oc.Observaciones} - OC: {oc.OcCliente} ";
            }
            oc.Observaciones = oc.Observaciones.Length >= 200 ? oc.Observaciones.Substring(0, 199) : oc.Observaciones;
            var md5 = filePath != null ? FileUtils.FileUtils.GetMD5FromFile(filePath) : "";
            if (ExistOrdenCompraIntegracionBCH(oc, md5))
            {
                oc.EstadoOrden = InternalVariables.InternalVariables.ProcesarOrdenesRepetidas()
                    ? EstadoOrden.POR_PROCESAR
                    : EstadoOrden.ORDEN_REPETIDA;
            }
            using (var command = new OracleCommand())
            {
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug())
                {
                    //Console.WriteLine($"ocAdapterList:\n{oc}");
                    //TraspasaTelemarketingSeparados(oc);
                    return true;
                }
                if (InternalVariables.InternalVariables.IsDebug() && InternalVariables.InternalVariables.InsertDebug())
                {
                    oc.EstadoOrden =
                        oc.EstadoOrden != EstadoOrden.DEBUG
                        && oc.EstadoOrden != EstadoOrden.POR_PROCESAR
                        ? oc.EstadoOrden
                        : EstadoOrden.DEBUG;
                }
                var tableName = "TF_COMPRA_INTEG_PDF";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        tableName = "TF_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_COMPRA_INTEG_TXT";
                        break;
                }
                if (oc.CenCos.Equals("") || oc.CenCos == null)
                {
                    oc.CenCos = "-1";
                }
                var sql = $"INSERT INTO {tableName}" +
                          "(NUMPED,RUTCLI,CENCOS,FECHA,DIRECCION,OC_CLIENTE,OBS,ESTADO) " +
                          $"VALUES({oc.NumPed},{oc.RutCli},{oc.CenCos},SYSDATE,'{oc.Direccion}'" +
                          $",'{oc.OcCliente}','{oc.Observaciones.Replace("'", "''")}',{(int)oc.EstadoOrden})";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);

                try
                {
                    //InstanceTransferWeb?.Close();
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceTransferWeb?.Close();
                    var cont = 0;
                    if (oc.DetallesCompra.Count == 0) return true;
                    foreach (var detC in oc.DetallesCompra)
                    {
                        if (InsertDetalleOrdenCompraIntegraciónBCH(cont, detC, oc.TipoIntegracion)) cont++;
                        else break;
                    }
                    if (cont == oc.DetallesCompra.Count)
                    {
                        if (oc.EstadoOrden == EstadoOrden.ORDEN_REPETIDA)
                        {
                            //throw new ErrorIntegracion(
                            //    TipoError.ErrorOrdenRepetida
                            //    , "Se esta intentando procesar una Orden "
                            //    , oc.RutCli.ToString()
                            //    , oc.OcCliente
                            //    , pdfReader.TxtFileName);
                            return true;
                        }
                        InsertHashIntegraciónBCH(oc);
                        Console.WriteLine($"ocAdapterList:\n{oc}");
                        //TraspasaTelemarketingSeparados(oc);
                        return true;
                    }

                    DeleteOrdenCompraIntegraciónBCH(oc);
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos");
                    return false;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos");
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }
        /* ricardo */

        public static List<string> listarCentrosCostoRelacionado(string rutcli)
        {

            var arreglo = new List<string>();
            var str = "";
            InstanceDmVentas.Open();
            var sql = $"select cencos, ccosto from TF_re_cctocli where rutcli = {rutcli}";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();
            while (data.Read())
            {
                str = $"{data["cencos"]} - {data["ccosto"]}";
                arreglo.Add(str);
            }
            InstanceDmVentas.Close();
            return arreglo;

        }
        public static void TraspasaTelemarketingSeparados()
        {
            return;
            // string[] tableNameArray = { "PDF", "WORD", "MAIL", "EXCEL", "CA_ES", "TXT","WS" };
            string[] tableNameArray = {"EXCEL", "CA_ES","PDF" };
            //string[] tableNameArray = {  "CA_ES" };

            int[] numpedarray = { };
            
            foreach (var tableName in tableNameArray)
            {
                var collection = listarNumpedEnEstadoCero(tableName);
                foreach (var numped in collection)
                {
                    try
                    {
                        OracleDataReader resultListTfCompraInteg = listTfCompraInteg(tableName, numped);
                        //InstanceTransferWeb.Close();
                        if (resultListTfCompraInteg.Read())
                        {
                            string NumeroPedido = resultListTfCompraInteg["numped"].ToString();
                            var TipoProd = resultListTfCompraInteg["tiprod"];
                            var finalPedido = 0;
                            var serie = obtenerSerieWeNotaVta();
                            var empresa = obtenerEmpresa(Convert.ToInt32(resultListTfCompraInteg["rutcli"]));
                            var vendedor = obtenerVendedor(empresa, Convert.ToInt32(resultListTfCompraInteg["rutcli"]), resultListTfCompraInteg["cencos"].ToString());
                            var codbod = GetBodega(Convert.ToInt32(resultListTfCompraInteg["rutcli"]), empresa = 3, resultListTfCompraInteg["cencos"].ToString(), resultListTfCompraInteg["SKU_DIMERC"].ToString());
                            int codbod2 = Convert.ToInt32(resultListTfCompraInteg["bodega"]);
                            var clavta = 1;
                            clavta = (codbod == 1) ? 30 : 41;
                            var observacion_oracle = resultListTfCompraInteg["obs"].ToString().Replace("'", "");
                            insertWeNotavta(serie, resultListTfCompraInteg["oc_cliente"].ToString(), Convert.ToInt32(resultListTfCompraInteg["rutcli"]), resultListTfCompraInteg["cencos"].ToString(), NumeroPedido, observacion_oracle, codbod2, empresa, clavta);
                            InsertIntoReCcToCli(resultListTfCompraInteg["rutcli"].ToString(), resultListTfCompraInteg["cencos"].ToString());
                            InstanceTransferWeb.Close();
                            OracleDataReader row = listTfCompraInteg(tableName, numped);
                            while (row.Read() && finalPedido == 0)
                            {
                                var sku_dimerc = row["sku_dimerc"].ToString();
                                if (sku_dimerc.Equals("W421291"))
                                {
                                    ActualizaDespachoExpress(NumeroPedido);
                                }
                                int cantidad = Convert.ToInt32(row["cantidad"]);
                                int precio = Convert.ToInt32(row["precio"]);
                                int posicion = Convert.ToInt32(row["posicion"]);
                                insertWDNotavta(serie, sku_dimerc, cantidad, precio, empresa, posicion);
                                //actualizarDespachoExpress(NumeroPedido, sku_dimerc.ToString());    
                                   
                            }

                            actualizaEstadoIntegracion(NumeroPedido, tableName);

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        Console.WriteLine();

                    }
                    InstanceTransferWeb.Close();
                }
            }
            
        }
        private static void actualizaEstadoIntegracion(string numped, string tableName)
        {
            using (var command = new OracleCommand())
            {
                //string oc_cliente1 = oc_cliente;
                string sql = "";
                sql = $"update tf_compra_integ_{tableName} set estado = 1 where numped = {numped}";

                Console.WriteLine($"actualiza estado tf_compra_integ_{tableName}  {sql}");
                
                OracleTransaction trans = null;                
                command.Connection = InstanceTransferWeb;

                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                try
                {
                   // InstanceDmVentas.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();

                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    //InstanceDmVentas?.Close();
                }

            }
        }

        private static void ActualizaDespachoExpress(string serie)
        {
            using (var command = new OracleCommand())
            {
                //string oc_cliente1 = oc_cliente;
                string sql = "";
                sql = $"update we_notavta set clavta = 43 where numord = {serie}";

                Console.WriteLine($"actualiza despacho express  {sql}");

                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;

                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                try
                {
                     InstanceDmVentas.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();

                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceDmVentas?.Close();
                }

            }
        }
        private static void actualizarDespachoExpress(string numped, string sku_dimerc)
        {
            using (var command = new OracleCommand())
            {
                //string oc_cliente1 = oc_cliente;
                string sql = "";
                if (sku_dimerc == "W421291")
                {
                    sql = $"update we_notavta set clavta = 43 where numord = {numped}";
                }
                if (sql.Equals(""))
                {
                    return;
                }
                Console.WriteLine($"actualiza despacho express WE_NOTAVTA {sql}");
                 OracleTransaction trans = null;                
                 command.Connection = InstanceDmVentas;
                 command.CommandType = CommandType.Text;
                 command.CommandText = sql;
                 try
                 {
                     InstanceDmVentas.Open();
                     trans = InstanceDmVentas.BeginTransaction();
                     command.Transaction = trans;
                     command.ExecuteNonQuery();
                     trans.Commit();

                 }
                 catch (SqlException)
                 {
                     trans?.Rollback();
                 }
                 finally
                 {
                     InstanceDmVentas?.Close();
                 }

            }

        }

        public static string GetCencosDetalle(int rutcli, string cencos)
        {
            // int bodega = 0;
            var descco = "";
            var contac = "";
            var fono01 = "";
            var ret = "";
            InstanceDmVentas.Close();
            InstanceDmVentas.Open();
            var sql = $"select descco, contac,fono01 from de_cliente where rutcli = {rutcli} and cencos = {cencos}";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();
            if (data.Read())
            {
                descco = data["descco"].ToString();
                contac = data["contac"].ToString();
                fono01 = data["fono01"].ToString();
                ret = $"JARDIN: {descco} CONTACTO: {contac} TELEFONO: {fono01}";
                InstanceDmVentas.Close();
                
            }
            return ret;
        }
        private static int GetBodega(int rutcli, int empresa, string cencos, string sku_dimerc)
        {
            int bodega = 0;
            
            InstanceDmVentas.Open();
            var sql = $"select getrutconcesion_new({empresa},{rutcli},'{cencos}') bodega from dual";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();
            if (data.Read())
            {
                bodega = Convert.ToInt32(data["bodega"]);
                if (bodega == 0)
                {
                    bodega = 1;
                }
            }
            else
            {
                bodega = 1;
            }
            InstanceDmVentas.Close();
            return bodega;
        }
        private static int GetComuna(int rutcli, int empresa, string cencos)
        {
            int comuna = 0;

            InstanceDmVentas.Open();
            //var sql = $"select getrutconcesion_new({empresa},{rutcli},'{cencos}') bodega from dual";
            var sql = $"select dire.codcom from de_cliente cencos" +
                        "inner join re_ddescli dire on dire.coddir = cencos.dirdes" +
                        "where dire.rutcli = cencos.rutcli" +
                        "and cencos.rutcli = "+rutcli+" "+
                        "and cencos.cencos = "+cencos+" " +
                        "and cencos.codemp = "+empresa+" ";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();

            if (data.Read())
            {
                comuna = Convert.ToInt32(data["codcom"]);
            }
            InstanceDmVentas.Close();
            return comuna;
        }

        private static int GetCobroLogistico(int rutcli, int comuna)
        {
            int cobroLog = 0;
            InstanceDmVentas.Open();
            //var sql = $"select getrutconcesion_new({empresa},{rutcli},'{cencos}') bodega from dual";
            var sql = $"select SAP_GET_VALOR_RECARGO(3,130, 0, 39000, 0) -- se debe cambiar comuna en el segundo parametro" +
                    "from en_cliente where" +
                    "sap_get_convenio(codemp, rutcli) = 0-- no tiene convenio" +
                    "and sap_get_cliente_gobierno(codemp, rutcli) = 'N'-- no es gobierno" +
                    "and dm_ventas.SAP_GET_COBROLOG_CLIENTE(rutcli, codemp) = 'S'-- - cobrar = 'S' " +
                    "and codest = 1" +
                    "and tipdoc = 'F'" +
                    "and codemp = 3";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();

            if (data.Read())
            {
                comuna = Convert.ToInt32(data["codcom"]);
            }
            InstanceDmVentas.Close();
            return cobroLog;

        }
        private static int obtenerSerieWeNotaVta()
        {
            int numord = 0;
            InstanceDmVentas.Open();
            var sql = "select notaweb_seq.nextval as serie from dual";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();
            if (data.Read())
            {
                numord = Convert.ToInt32(data["serie"]);
            }
            InstanceDmVentas.Close();
            return numord;
        }
        private static int obtenerEmpresa(int rutcli)
        {
            int empresa = 3;
            
            //InstanceTransferWeb.Open();
            var sql = $"select nvl(codemp,3) empresa from tf_cliente_empresa where rutcli = {rutcli}";
            var command = new OracleCommand(sql, InstanceTransferWeb);
            var data = command.ExecuteReader();
            if (data.Read())
            {
                empresa = Convert.ToInt32(data["empresa"]);
            }
            //InstanceTransferWeb.Close();
            return empresa;
        }
        private static string obtenerVendedor(int empresa, int rut, string cc)
        {
            
            string vendedor = "";
            InstanceDmVentas.Open();
            var sql = $"select getvendedorcliente({empresa},{rut},{cc}) vendedor from dual";
            var command = new OracleCommand(sql, InstanceDmVentas);
            var data = command.ExecuteReader();
            if (data.Read())
            {
                vendedor = data["vendedor"].ToString();
            }
            InstanceDmVentas.Close();
            return vendedor;
        }
        private static OracleDataReader listTfCompraInteg(string tableName,int numped)
        {

            string concat = "";
           /* if (tableName.Equals("PDF"))
            {
                concat += " AND (a.rutcli = 99231000 or a.oc_cliente like '%COTIZAC%')";
            }*/
            //DataSet theDataSet = new DataSet();
                InstanceTransferWeb.Open();
                //var sql = "select a.* from tf_compra_integ_" + tableName + " a where a.estado=0 and a.oc_cliente is not null";
                var sql = "SELECT   a.*, b.*,  b.codBod bodega,getrazonsocial (3, rutcli) razon,getvendedorcliente(3, rutcli, cencos) vendedor,"
                + "decode((select 1 from rutcli_no_separa_pedidos where rutcli = a.rutcli), " +
                "null, to_number(DECODE(getpedsto(sku_dimerc), 'S', 1, DECODE(getpedsto(sku_dimerc), 'P', 2, 3)) " +
                "|| get_producto_tipo(sku_dimerc) || decode(GetCliFacLin(rutcli), 'S', GetLineaCliente(rutcli, sku_dimerc), 1)),111  )tiprod" +
                " , (Select desval2 from de_dominio where desval = b.sku_dimerc and coddom = 904) Relacionado" +
                " FROM tf_compra_integ_" + tableName + " a, tf_detalle_compra_integ_" + tableName + " b WHERE a.numped = b.numped " +
                "AND a.estado = 0 AND A.OC_CLIENTE IS NOT NULL and a.numped = " + numped + ""+concat+" ORDER BY a.numped, tiprod, B.ID ";

                var command = new OracleCommand(sql, InstanceTransferWeb);
                // OracleDataAdapter dataAdapter = new OracleDataAdapter(command);
                // dataAdapter.Fill(theDataSet);
                var data = command.ExecuteReader();
                return data;

            

            return data;
        }

        private static int[] listarNumpedEnEstadoCero(string tableName)
        {
            string concat = "";
            if (tableName.Equals("PDF"))
            {
                concat += " AND (a.rutcli = 99231000 or a.oc_cliente like '%COTIZAC%')";
            }

            List<int> listaNumped = new List<int>();
            InstanceTransferWeb.Open();
            //var sql = $"select numped from tf_compra_integ_{tableName} where oc_cliente is not null and estado = 0";
            var sql = $"select a.numped,count(*) cantidad from tf_compra_integ_{tableName} a, tf_detalle_compra_integ_{tableName} b where oc_cliente is not null and a.estado = 0 and a.numped = b.numped and cencos <> -1 {concat} group by a.numped order by a.numped desc";
            Console.WriteLine(sql);
            var command = new OracleCommand(sql, InstanceTransferWeb);
            var data = command.ExecuteReader();
            while (data.Read())
            {
                listaNumped.Add(Convert.ToInt32(data["numped"]));
            }
            InstanceTransferWeb.Close();
            return listaNumped.ToArray();
        }

        private static bool insertWeNotavta(int serie, string oc_cliente, int rutcli, string cencos, string numped, string observacion, int codbod2, int empresa, int clavta)
        {
            using (var command = new OracleCommand())
            {
                string oc_cliente1 = oc_cliente;
                string sql = $"insert into we_notavta (numord,fecord,facnom,rutcli,cencos,facdir,ordweb,tipweb,observ,descli,codbod,codemp,clavta) values ";
                sql += $"({serie},trunc(sysdate),'{oc_cliente1}',{rutcli},{cencos},'0',{numped},'5','{observacion}',0,{codbod2},{empresa},{clavta})";
                Console.WriteLine($"INSERT WE_NOTAVTA {sql}");
                OracleTransaction trans = null;                
                 command.Connection = InstanceDmVentas;
                 command.CommandType = CommandType.Text;
                 command.CommandText = sql;
                 try
                 {
                     InstanceDmVentas.Open();
                     trans = InstanceDmVentas.BeginTransaction();
                     command.Transaction = trans;
                    command.ExecuteNonQuery();
                     trans.Commit();

                 }
                 catch (SqlException)
                 {
                     trans?.Rollback();
                 }
                 finally
                 {
                     InstanceDmVentas?.Close();
                 }

            }


            return true;
        }

        private static bool insertWDNotavta(int serie, string sku_dimerc, int cantidad, int prelis, int codemp, int sequen)
        {
            using (var command = new OracleCommand())
            {
                // string oc_cliente1 = oc_cliente;
                string sql = $"insert into wd_notavta (numord,codpro,canpro,prelis,codemp,sequen) values ";
                sql += $"({serie},'{sku_dimerc}',{cantidad},{prelis},{codemp},{sequen})";
                Console.WriteLine($"INSERT WD_NOTAVTA {sql}");
                 OracleTransaction trans = null;                
                 command.Connection = InstanceDmVentas;
                 command.CommandType = CommandType.Text;
                 command.CommandText = sql;
                 try
                 {
                     InstanceDmVentas.Open();
                     trans = InstanceDmVentas.BeginTransaction();
                     command.Transaction = trans;
                     command.ExecuteNonQuery();
                     trans.Commit();

                 }
                 catch (SqlException)
                 {
                     trans?.Rollback();
                 }
                 finally
                 {
                     InstanceDmVentas?.Close();
                 }

            }


            return true;
        }

        /* ricardo */

        private static void DeleteOrdenCompraIntegración(OrdenCompraIntegracion oc)
        {
            using (var command = new OracleCommand())
            {
                var tableName = "TF_COMPRA_INTEG_PDF";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_COMPRA_INTEG_TXT";
                        break;
                }
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText =
                    $"DELETE FROM {tableName} WHERE NUMPED = {oc.NumPed}";
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
                DeleteDetalleOrdenCompraIntegracion(oc);
            }
        }

        private static void DeleteOrdenCompraIntegraciónBCH(OrdenCompraIntegracionBCH oc)
        {
            using (var command = new OracleCommand())
            {
                var tableName = "TF_COMPRA_INTEG_PDF";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_COMPRA_INTEG_TXT";
                        break;
                }
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText =
                    $"DELETE FROM {tableName} WHERE NUMPED = {oc.NumPed}";
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
                DeleteDetalleOrdenCompraIntegracionBCH(oc);
            }
        }
        private static void DeleteRawOrdenCompraPdf(string numeroPedido)
        {
            using (var command = new OracleCommand())
            {
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText =
                    $"DELETE FROM RAW_EN_ORDEN_PDF WHERE NUMPED =  {numeroPedido}";
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }

        public static void InsertLecturaXml(string asunto, string contenido)
        {
            using (var command = new OracleCommand())
            {
                var sql =
                    $"insert into correoiconstruye(id,asunto, contenido, fecha) values(correoiconstruye_seq.nextval,'{asunto}', '{contenido}',sysdate)";
                OracleTransaction trans = null;
                command.Connection = InstanceDmVentas;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceDmVentas.Open();
                    trans = InstanceDmVentas.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    InstanceDmVentas?.Close();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
            }
        }


        private static bool InsertRawDetalleOrdenCompra(int index, string numeroPedido, Item item, TipoIntegracion tipo)
        {
            using (var command = new OracleCommand())
            {
                var table = "PDF_RAW_DE_ORDEN";
                switch (tipo)
                {
                    case TipoIntegracion.PDF:
                        table = "PDF_RAW_DE_ORDEN";
                        break;
                    case TipoIntegracion.MAIL:
                        table = "MAIL_RAW_DE_ORDEN";
                        break;
                }
                var descripcionC = item.Descripcion.Length > 250 ? item.Descripcion.Substring(0, 250) : item.Descripcion;
                var sql = $"INSERT INTO {table}  (POSICION, NUMPED, TIPO_PAREO_ITEM, SKU_CLIENTE, CANTIDAD,PRECIO,DESCRIPCION_CLIENTE) VALUES ({index + 1},{numeroPedido},'{item.TipoPareoProducto}','{item.Sku}','{item.Cantidad}','{item.Precio}','{descripcionC.Replace("'", "''")}')";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                //Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug()) return true;
                try
                {
                    InstanceTransferWeb.Close();
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
            return true;
        }

        public static int GetSerieNotaWeb()
        {
            var retu = -1;
            //SELECT NOTAWEB_SEQ.NEXTVAL AS SERIE FROM DUAL
            try
            {
                InstanceDmVentas.Open();
                const string sql = "SELECT NOTAWEB_SEQ.NEXTVAL AS SERIE FROM DUAL";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                var ret = data.Read() ? data["CODPRO"].ToString() : "-1";
                retu = int.Parse(ret);
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return retu;
        }


        private static bool InsertDetalleOrdenCompraIntegración(int index, DetalleOrdenCompraIntegracion det, TipoIntegracion tipo)
        {
            using (var command = new OracleCommand())
            {
                var tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                switch (tipo)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        tableName = "TF_DETALLE_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_DETALLE_COMPRA_INTEG_TXT";                        
                        break;
                    case TipoIntegracion.COTIZACION:
                        tableName = "TF_DETALLE_COMPRA_INTEG_COTI";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR_PRV:
                        tableName = "TF_DE_COMPRA_INTEG_CA_ES_PRV";
                        break;
                }
                var sql = $"INSERT INTO {tableName}" +
                          "(NUMPED,SKU_DIMERC,CANTIDAD,PRECIO,SUBTOTAL,CODBOD, POSICION) " +
                          $"VALUES ({det.NumPed},'{det.SkuDimerc}',{det.Cantidad},{det.Precio},{det.SubTotal},{(int)det.CodigoBodega},{index + 1})";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug()) return true;
                try
                {
                    //InstanceTransferWeb.Close();
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    InstanceTransferWeb?.Close();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
            return true;
        }

        private static bool InsertDetalleOrdenCompraIntegraciónBCH(int index, DetalleOrdenCompraIntegracionBCH det, TipoIntegracion tipo)
        {
            using (var command = new OracleCommand())
            {
                var tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                switch (tipo)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.CARGA_ESTANDAR:
                        tableName = "TF_DETALLE_COMPRA_INTEG_CA_ES";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_DETALLE_COMPRA_INTEG_TXT";
                        break;
                }
                var sql = $"INSERT INTO {tableName}" +
                          "(NUMPED,SKU_DIMERC,CANTIDAD,PRECIO,SUBTOTAL,CODBOD, POSICION) " +
                          $"VALUES ({det.NumPed},'{det.SkuDimerc}',{det.Cantidad},{det.Precio},{det.SubTotal},{(int)det.CodigoBodega},{index + 1})";
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                if (InternalVariables.InternalVariables.IsDebug() && !InternalVariables.InternalVariables.InsertDebug()) return true;
                try
                {
                    //InstanceTransferWeb.Close();
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    InstanceTransferWeb?.Close();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
            return true;
        }

        private static void DeleteDetalleOrdenCompraIntegracion(OrdenCompraIntegracion oc)
        {
            using (var command = new OracleCommand())
            {
                var tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_DETALLE_COMPRA_INTEG_TXT";
                        break;
                }
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText =
                    $"DELETE FROM {tableName} WHERE NUMPED = {oc.NumPed}";
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }

        private static void DeleteDetalleOrdenCompraIntegracionBCH(OrdenCompraIntegracionBCH oc)
        {
            using (var command = new OracleCommand())
            {
                var tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                switch (oc.TipoIntegracion)
                {
                    case TipoIntegracion.PDF:
                        tableName = "TF_DETALLE_COMPRA_INTEG_PDF";
                        break;
                    case TipoIntegracion.WORD:
                        tableName = "TF_DETALLE_COMPRA_INTEG_WORD";
                        break;
                    case TipoIntegracion.MAIL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_MAIL";
                        break;
                    case TipoIntegracion.EXCEL:
                        tableName = "TF_DETALLE_COMPRA_INTEG_EXCEL";
                        break;
                    case TipoIntegracion.TEXT:
                        tableName = "TF_DETALLE_COMPRA_INTEG_TXT";
                        break;
                }
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText =
                    $"DELETE FROM {tableName} WHERE NUMPED = {oc.NumPed}";
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }


        private static void DeleteDetalleRawOrdenCompraPDF(string numeroPedido)
        {
            using (var command = new OracleCommand())
            {
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText =
                    $"DELETE FROM RAW_DE_ORDEN_PDF WHERE NUMPED = {numeroPedido}";
                if (InternalVariables.InternalVariables.IsDebug()) return;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }
        }

        public static bool InsertErrorIntegracionLog(ErrorIntegracion integracionError)
        {
            //using (var command = new OracleCommand())
            //{
            //    //var existSku = ExistProduct(det.SkuDimerc);
            //    //var sku = existSku ? det.SkuDimerc : "Z446482";
            //    var sql = "INSERT INTO TF_DETALLE_COMPRA_INTEG_PDF" +
            //              "(NUMPED,SKU_DIMERC,CANTIDAD,PRECIO,SUBTOTAL,CODBOD) " +
            //              $"VALUES ({det.NumPed},'{det.SkuDimerc}',{det.Cantidad},{det.Precio},{det.SubTotal},{det.CodigoBodega})";
            //    OracleTransaction trans = null;
            //    command.Connection = InstanceTransferWeb;
            //    command.CommandType = CommandType.Text;
            //    command.CommandText = sql;
            //    Console.WriteLine(sql);
            //    if (InternalVariables.IsDebug()) return true;
            //    try
            //    {
            //        InstanceTransferWeb.Open();
            //        trans = InstanceTransferWeb.BeginTransaction();
            //        command.Transaction = trans;
            //        command.ExecuteNonQuery();
            //        trans.Commit();
            //    }
            //    catch (SqlException)
            //    {
            //        trans?.Rollback();
            //        return false;
            //    }
            //    finally
            //    {
            //        InstanceTransferWeb?.Close();
            //    }
            //}
            return true;
        }
        public static string GetObsUdp(string cc)
        {
            var ret = "abc";
            try
            {
                InstanceDmVentas.Open();
                var sql = $"SELECT DIRECCION as DIR  FROM UDP_OBSERV WHERE CCOSTO = '{cc}'";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = data["DIR"].ToString();
                }
                else
                {
                    ret = "S/D";
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;

        }

        /// <summary>
        /// Retorna la Cantidad de pareos que Existe para ese producto.
        /// </summary>
        /// <param name="rut">Rut del Cliente</param>
        /// <param name="codigoCliente">Código del Producto del Cliente</param>
        /// <param name="descripcion">Descripcion del Cliente</param>
        /// <returns></returns>
        public static int GetCantidadPareoCodigoCliente(string rut, string codigoCliente, string descripcion = "")
        {
            var ret = 0;
            try
            {
                var sufjix = descripcion.Equals("") ? "" : $"AND DESCRIPCION_CLIENTE ='{descripcion.Replace("'", "''")}'";
                InstanceDmVentas.Open();
                var realCodCli = codigoCliente.Equals("Z446482") ? "SIN_SKU" : codigoCliente;
                realCodCli = realCodCli.Replace("SIN_SKU", "SIN_SKU%");
                var codCliFilter = realCodCli.Equals("SIN_SKU%") ? $"codcli like '{realCodCli}' " : $"codcli = '{realCodCli}' ";
                var sql = $"SELECT COUNT(*) CANTIDAD FROM dm_ventas.RE_CODCLI WHERE RUTCLI = {rut} AND {codCliFilter} {sufjix}";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    ret = int.Parse(data["CANTIDAD"].ToString());
                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static int getCodCatConsorcio(string rut, string sku)
        {
            var ret = 1;
            try
            {
                InstanceDmVentas.Open();
                var sql = $"select codcat from dm_ventas.re_codcli where rutcli = {rut} and codcli = '{sku}'";
                var command = new OracleCommand(sql, InstanceDmVentas);
                var data = command.ExecuteReader();
                ret = data.Read() ? int.Parse(data["codcat"].ToString()) : 1;
                data.Close();
                Console.WriteLine($"codcat: {ret}, SQL: {sql}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceDmVentas?.Close();
            }
            return ret;
        }

        public static bool InsertCorreoRecibido(string asunto,string archivoAdjunto , string remitente , string moduloEjecutado)
        {
            string path = "C:/Debug_Registro_Correos_Entrantes.txt";
            string texto = "Entro a InsertCorreoRecibido()";
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
            {
                writer.WriteLine(texto);
            }


            if (archivoAdjunto.Contains("pdf")) {
                moduloEjecutado = "PDF";
            }
            else if (archivoAdjunto.Contains("xls"))
            {
                moduloEjecutado = "EXCEL";
            }else if (archivoAdjunto.Contains("txt"))
            {
                moduloEjecutado = "TXT";
            }
            if (asunto.Contains("LECTURA MAIL") || asunto.Contains("Aviso Despacho DIMERC") || asunto.ToUpper().Contains("AVISO DESPACHO DIMERC"))
            {
                moduloEjecutado = "MAIL";
            }
            using (var command = new OracleCommand())
            {

                var sql = $"INSERT INTO PXML_CORREOS_RECIBIDOS(ASUNTO,ARCHIVO_ADJUNTO,REMITENTE,MODULO_EJECUTADO,ID) VALUES('{asunto}','{archivoAdjunto}','{remitente}','{moduloEjecutado}',PXML_CORREOS_RECIBIDOS_SEQ.nextval)";
                Console.WriteLine("insertando correo recibido" + sql);
                texto = $"insertando query {sql}";
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
                {
                    writer.WriteLine(texto);
                }
                Log.Log.Save("pxml", " QUERY " + sql);
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                //if (InternalVariables.IsDebug()) return true;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    texto = $"ejecutando query exitosa: {sql}";
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
                    {
                        writer.WriteLine(texto);
                    }
                    return true;
                }
                catch (SqlException x )
                {
                    Log.Log.Save("Error", "No es posible insertar los datos en la Base de Datos: QUERY "+sql);
                    texto = "No es posible insertar los datos en la Base de Datos: QUERY " + sql;
                    using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
                    {
                        writer.WriteLine(texto + x.ToString());
                    }
                    trans?.Rollback();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }

            
        }

        public static bool NumpedArchivoAdjunto(int numped,string archivo_adjunto)
        {

            using (var command = new OracleCommand())
            {

                var sql = $"INSERT INTO NUMPED_ARCHIVO_ADJUNTO(NUMPED,ARCHIVO_ADJUNTO) VALUES({numped},'{archivo_adjunto}')";
                Console.WriteLine("insertando correo con numped");
                OracleTransaction trans = null;
                command.Connection = InstanceTransferWeb;
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                Console.WriteLine(sql);
                //if (InternalVariables.IsDebug()) return true;
                try
                {
                    InstanceTransferWeb.Open();
                    trans = InstanceTransferWeb.BeginTransaction();
                    command.Transaction = trans;
                    command.ExecuteNonQuery();
                    trans.Commit();
                    return true;
                }
                catch (SqlException)
                {
                    trans?.Rollback();
                    return false;
                }
                finally
                {
                    InstanceTransferWeb?.Close();
                }
            }


        }



        public static bool isTissueProduct(string ocCliente, string rut, string skuTmp)
        {
            int Cnt = 0;

            try
            {
                var codpro = "";
                if (ExistProduct(skuTmp))
                {
                    codpro = skuTmp;
                }
                else
                {
                    codpro = GetSkuDimercFromCodCliente(ocCliente, rut, skuTmp, true);
                }
                var sql = $"SELECT COUNT(*) Cnt FROM CLIENTE_PRODUCTO_TISSUE WHERE RUTCLI = {rut} AND CODPRO = '{codpro}'";                
                InstanceTransferWeb.Open();
                var command = new OracleCommand(sql, InstanceTransferWeb);
                var data = command.ExecuteReader();
                if (data.Read())
                {
                    Cnt = int.Parse(data["Cnt"].ToString());
                    Console.WriteLine("cantidad tissue " + Cnt);

                }
                data.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                InstanceTransferWeb?.Close();
            }
            Console.WriteLine("es tissue ? " + (Cnt > 0));
            return (Cnt > 0);
        }
    }
}
