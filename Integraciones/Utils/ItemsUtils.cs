﻿using System.Collections.Generic;
using Integraciones.Utils.OrdenCompra;

namespace Integraciones.Utils
{
    public static class ItemsUtils
    {

        public static void DeleteLongNumberFromDescription(this Item item, int length)
        {
            if(item.Descripcion == null) return;
            if (item.Descripcion.DeleteContoniousWhiteSpace().Equals("")) return;
            var descripcion = "";
            foreach (var desc in item.Descripcion.Split(' '))
            {
                var number = 0;
                if(int.TryParse(desc.DeleteSymbol(),out number) && desc.Length == length) continue;
                descripcion += $" {desc}";
            }
            item.Descripcion = descripcion.DeleteContoniousWhiteSpace();
        }

        public static void DeleteAllLongNumberFromDescription(this List<Item> items, int length)
        {
            foreach (var item in items)
                item.DeleteLongNumberFromDescription(length);
        }

        public static string ParseToFileUrl(this string filePath)
        {
            return filePath.Replace(" ", "%20")
                .Replace("#", "%23");
        }
    }
}
