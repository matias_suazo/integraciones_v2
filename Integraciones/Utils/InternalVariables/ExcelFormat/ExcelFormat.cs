﻿using System.Collections.Generic;

namespace Integraciones.Utils.InternalVariables.ExcelFormat
{
    public static class ExcelFormat
    {

        public static readonly Dictionary<int, string> ExcelFormats = new Dictionary<int, string>
        {
            // ; DELIMITADOR PARA 'AND' LÓGICO (&&)
            // || DELIMITADOR PARA 'OR' LÓGICO (||)
            //   SOLO PUEDE HABER UNO POR TOKEN
            //{0, "Tienda : Flores*Tienda : Tienda Flores"}, //MZAPATA
            //{2, "76008982*76021762*77335750*76.008.982-6*77.457.040-3"}, //MZAPATA
            {0, "RUT(*);CC(*);OC(*)"}, //CARGA ESTANDAR
            {1, "RUT EMPRESA FECHA OC INSTALACION CC ITEMS || RUT EMPRESA FECHA OC INSTALACION CC COD ITEM || RUT	Empresa	Fecha	O/C	Instalacion	cc||AMG S.A.||AMGASEO S.A.||AMG ASEO S.A.||C&M S.A.||C& M S.A.||C & M  S.A||C & M S. A||C & M S.A.||99552920-3||76230088-5||AMG ASEO S.A||AMG SA" },//AMG Y AMG ASEO //c/u Valor despacho comuna Receptor fono
            //{2, "RUT CC DIJON CC DIMERC CC DIJON RUT NOMBRE CC DE TELEMARKETING CONTACTO NOMBRE ACTUALIZADO DIRECCION DESPACHO DIRECCIONES TELEMARKETING COMUNA CIUDAD TIEMPOS DE ENTREGA MAIL FONO Fono Actualizado CC DIMERC HORARIO Resmas papel carta (cant. Unitaria) Rollo papel higienico 500mts (cant. Unitaria) Toalla papel rollo 250mts (cant. Unitaria) OC OBSERVACIONES A CONSIDERAR" },
            {2, "77555730||82982300" },
            {3, "82.411.500-1" },
            {4, "96900150-0" }, //BANCO ESTADO           
            {6, "96.641.860-5||96.900.010-5||99.567.270-7||99.567.280-4||96641860-5" }, //PY INMOBILIARIA
            // {7, "WON/SWON CÓDIGO DESCRIPCIÓN CANTIDAD.:WON/SWON" },//TATA
            {8, "Documento compras Proveedor/Centro suministrador Estado liberación" }, //iNTEGRAMEDICA
            {9, "n° de Paquetes" }, // Gas Atacama
            {10, "NUEVOS ALMACENES INTERNACIONALES;96.520.950-4" },
            {11, "MANUFACTURAS INTERAMERC. S.A.;92.171.000-3" },
            {12,"96555510||76547440||76.547.440||76.434.444||76434444||96.555.510" }, //APARDO MAUI
            //{13, "76547410" }, //APARDO SAMAR // SOCIEDAD AUTOMOTRIZ 1
            {14, "61703000-4" }, //APARDO ENAMI 
            {15, "77766430" }, // APARDO Servicio Capacitación
            {16, "ERNST & YOUNG" }, //APARDO
            {17, "Nomina Rut Razon social Cui||Nomina	Rut	Razon social" }, //APARDO
            {18, "RUT: 97023000||RUT 97023000" }, //APARDO
            {19, "Telefónica Ingeniería de Seguridad - Agencia Chile" }, //APARDO
            {20, "ISAPRE CRUZ BLANCA||CRUZ BLANCA COMPAÑIA DE SEGUROS DE VIDA||SEGUROS BUPA||SEGUROS DE VIDA||96501450-0" }, //APARDO

            {300,"77.532.050-8||Centro de Formación Técnica Pro Andes" },
            {301, "CIA DE SEG. DE VIDA CONSORCIO NAC. DE SEG. S.A.||CIA. DE SEG. GENERALES CONSORCIO NAC. SEG. S.A." }, //CONSORCIO
            {302, "Nombre Solicitante;DESCRIPCION UNIDADES CODIGO PRECIO TOTAL;Datos y Firma persona/empresa que confecciona la Solicitud Nombre, apellidos y firma del cliente." }, // rreyes
            {303, "99545180" }, //GE2
            {304, "ANTECEDENTES DE SISTEMAS EY"},
            {305, "LICITACION ARTICULOS DE OFICINA (EZENTIS CHILE S.A - EZENTIS ENERGIA SPA)" },
            {306, "52000190-5" } ,// dollens
            {307,"WON/SWON" },// TATA CONSULTANCY SERVICES CHILE S.A.
            {308,"ELEMENTOS Ud. Presen-tación:Ud. Presen-tación:A B 1 2 3 4" }, // JUNJI
            {309, "76547410" },  // SOCIEDAD AUTOMOTRIZ 2
            {310,"COLEGIO SAINT ANDREW S.A.||76.357.810-0" },
            {311,"70786200" },// Corporacion de asistencia judicial, Fernanda Espinoza
            {312,"61002004-6" }, // REGISTRO CIVIL SUSAN ASTETE
            //{313,"61608605" }, // susan astete
            {314, "CORPORACION DE EDUCACION Y SALUD DE LAS CONDES" },
            {316,"85555900" }, // BPM FLUOR
            {317,"LOGISTEX S.A." },
            {318,"Codigo Prosepan" },
            {319,"Afubach||76792110" },
            {320,"96920840" }, // CELA COSMETICOS S.A ROSITA CORNEJO
            {321,"JUNJI||70072600" },
            {322,"REGISTRO CIVIL" },
            {323, "89736400" },//Carlos Aguirre
            {324, "65153249" },//Corporación educacional BC
            {325, "96547580-K" },//cepech
            {326, "RUT_CLI(*);CENCO(*);ORDEN_COMPRA(*);RUT_PRV(*)" },
            //{327, "Santiago," },
            {328, "RUT;CECO;Orden de Compra;SKU Proveedor;Item;Cantidad"},
            //{329, "rut 77166811" },
            {330, "Material;CODIGO DIMERC;Documento compras;Fecha documento"},//HABLAR CON SAMAR
            {331, "rut 77166811"}
        };
    }
}