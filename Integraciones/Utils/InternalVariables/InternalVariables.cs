﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows;

namespace Integraciones.Utils.InternalVariables
{
    public static class InternalVariables
    {
        public static int CountCriticalError = 0;
        public static int CountSendErrorAlert = 0;
        public static string LastFilePathError = "null";
        public static bool? DebugConfigMode = null;

        public static Dictionary<int, string> PdfFormats => PdfFormat.PdfFormat.PdfFormats;
        public static Dictionary<int, string> TxtFormats => TxtFormat.TxtFormat.TxtFormats;
        public static readonly Dictionary<int, string> MailsFormats = MailFormat.MailFormat.MailsFormats;
        public static readonly Dictionary<int, string> ExcelFormat = Utils.InternalVariables.ExcelFormat.ExcelFormat.ExcelFormats;
        public static readonly Dictionary<int, string> WordFormats = WordFormat.WordFormat.WordFormats;

        #region Variables



        public static string GetCarpetaSinLectura()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaSinLectura");
        }
        public static bool KillExcelProcess()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("KillExcelProcess"));
        }


        #endregion


        #region Funciones


        #region Mail


        public static bool UseLog()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("UseLog"));
        }

        public static bool ProcessMail()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("ProcessMail"));
        }

        public static bool SendMailToEjecutivo()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendEmailEjecutivo"));
        }

        public static bool SendMailDebugNewFlow()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendMailDebugNewFlow"));
        }

        public static int GetPortEmailFrom()
        {
            return int.Parse(ConfigurationManager.AppSettings.Get("PortEmailFrom"));
        }

        public static string GetHostEmailFrom()
        {
            return ConfigurationManager.AppSettings.Get("HostEmailFrom");
        }

        public static string GetEmailFrom()
        {
            return ConfigurationManager.AppSettings.Get("SendEmailFrom");
        }

        public static string GetPasswordEmailFrom()
        {
            return ConfigurationManager.AppSettings.Get("PasswordEmailFrom");
        }

        public static string GetUnknownOcFolder()
        {
            return ConfigurationManager.AppSettings.Get("UnknownFile");
        }

        public static string GetErrorOcFolder()
        {
            return ConfigurationManager.AppSettings.Get("CapetaError");
        }

        public static string GetErrorFolder()
        {
            return ConfigurationManager.AppSettings.Get("CapetaError");
        }

        public static string GetSubjectDebug()
        {
            try
            {
                var sMacAddress = string.Empty;
                var nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    if (sMacAddress != string.Empty) continue;
                    var properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();

                }
                Console.WriteLine(sMacAddress);
                return ConfigurationManager.AppSettings.Get(sMacAddress).Split('@')[0].ToUpper();
            }
            catch
            {
                return "MZPATA";
            }
        }

        public static string[] GetMainEmail()
        {
            var sMacAddress = string.Empty;
            var nics = NetworkInterface.GetAllNetworkInterfaces();
            //var cc = 0;
            foreach (var adapter in nics)
            {
                if (sMacAddress == String.Empty)
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();

                }
            }
            if (!IsDebug()) return ConfigurationManager.AppSettings.Get("MainEmail").Split(';').ToArray();
            Console.WriteLine("MACCCC: " + sMacAddress);
            try
            {
                return new[]
                {
                    ConfigurationManager.AppSettings.Get(sMacAddress)
                    ?? ConfigurationManager.AppSettings.Get("1002B5C4275F")
                };
            }
            catch
            {
                return new[] { "matias.suazo@dimerc.cl" };
            }
            return ConfigurationManager.AppSettings.Get("MainEmail").Split(';').ToArray();
        }

        public static string[] GetEmailCc()
        {
            return ConfigurationManager.AppSettings.Get("EmailCC").Split(';').ToArray();
        }

        public static string[] GetEmailCcError()
        {
            return ConfigurationManager.AppSettings.Get("EmailCCError").Split(';').ToArray();
        }

        public static string[] GetQaEmailError()
        {
            var mail = GetMainEmail();
            var mail2 = GetEmailCcError();
            var mailQa = new string[mail.Length + mail2.Length];
            mail.CopyTo(mailQa, 0);
            mail2.CopyTo(mailQa, mail.Length);
            return mailQa;
        }

        #endregion



        #region CARPETAS

        public static string GetOcAProcesarFolder()
        {
            var x = ConfigurationManager.AppSettings.Get("CarpetaOrdenesProcesar");
            return x;
        }
        public static string GetOcExcelAProcesarFolder()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaOrdenesProcesarExcel");
        }
        public static string GetOcProcesadasFolder()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaOrdenesProcesadas");
        }
        public static string GetOcProcesadasFolderExc()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaOrdenesProcesadasExc");
        }
        public static string GetOcRepetidasFolder()
        {
            return ConfigurationManager.AppSettings.Get("CapetaRepetidos");
        }
        public static string GetLogFolder()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaLog");
        }

        public static void ChangeLogFolder(string newValue)
        {
            Save("CarpetaLog", newValue);
            Log.Log.Save("Información", "La Ruta del Archivo Log ha sido Cambiada por: " + newValue);
        }

        public static void ChangeOCaProcesarFolder(string newValue)
        {
            Save("CarpetaOrdenesProcesar", newValue);
            Log.Log.Save("Información", "La Ruta de las Ordenes a Procesar ha sido Cambiada por: " + newValue);
        }

        public static void ChangeOcProcesadasFolder(string newValue)
        {
            Save("CarpetaOrdenesProcesadas", newValue);
            Log.Log.Save("Información", "La Ruta de Ordenes Procesadas ha sido Cambiada por: " + newValue);
        }

        #endregion


        #region SISTEMA


        public static void AddCountError(string pdfPath)
        {
            if (!LastFilePathError.Equals(pdfPath))
            {
                LastFilePathError = pdfPath;
                CountCriticalError = 0;
            }
            CountCriticalError++;
        }
        public static void AddCountErrorExc(string excelPath)
        {
            if (!LastFilePathError.Equals(excelPath))
            {
                LastFilePathError = excelPath;
                CountCriticalError = 0;
            }
            CountCriticalError++;
        }

        public static bool SendPopup()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendPopupTlmk"));
        }

        public static bool SendMailEjecutivos()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendEmailEjecutivo"));
        }

        public static bool SendMailErrorEjecutivos()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendEmailErrorEjecutivo"));
        }

        public static bool SendMailPareoEjecutivos()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendEmailPareoEjecutivo"));
        }

        public static bool ProcesarOrdenesRepetidas()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("ProcesarOrdenesRepetidas"));
        }


        public static bool SendEmailOrdenRepetidaEjecutivo()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("SendEmailOrdenRepetidaEjecutivo"));
        }

        public static string GetUrlBasePareoEjecutivo()
        {
            return ConfigurationManager.AppSettings.Get("UrlBasePareoEjecutivo");
        }

        public static bool IsDebug()
        {
            return DebugConfigMode ?? bool.Parse(ConfigurationManager.AppSettings.Get("Debug"));
        }

        public static bool IsNewMode()
        {
            return DebugConfigMode ?? bool.Parse(ConfigurationManager.AppSettings.Get("IsNewMode"));
        }

        public static bool InsertDebug()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("InsertDebug"));
        }

        public static bool UseConnectionBdd()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("UseConnectionBDD"));
        }

        public static Visibility ShowDebug()
        {
            return ConfigurationManager.AppSettings.Get("ShowDebug").Equals("true")
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        public static int GetTiempoHorasCiclo()
        {
            return int.Parse(ConfigurationManager.AppSettings.Get("TiempoCicloHoras"));
        }

        public static int GetTiempoMinutosCiclo()
        {
            return int.Parse(ConfigurationManager.AppSettings.Get("TiempoCicloMinutos"));
        }

        private static void Save(string nameKey, string newValue)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Remove(nameKey);
            config.AppSettings.Settings.Add(nameKey, newValue);
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public static void InitializeVariables()
        {
            Log.Log.InitializeVariables();
        }

        #endregion

        #endregion

        #region PROYECTO LECTURA MAIL
        public static string GetEmailFolderProcesadas()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaProcesados");
        }

        public static string GetEmailErrorFolderProcesadas()
        {
            return ConfigurationManager.AppSettings.Get("CarpetaErrores");
        }

        public static string GetServidorCorreoLectura()
        {
            return ConfigurationManager.AppSettings.Get("ServidorLecturaMail");
        }

        public static string GetSecureTypeCorreoLectura()
        {
            return ConfigurationManager.AppSettings.Get("SecureTypeLecturaMail");
        }

        public static int GetPortCorreoLectura()
        {
            return int.Parse(ConfigurationManager.AppSettings.Get("PortLecturaMail"));
        }

        public static string GetUsuarioServidorCorreoLectura()
        {
            return ConfigurationManager.AppSettings.Get("UsuarioServidorLecturaMail");
        }

        public static string GetPassServidorCorreoLectura()
        {
            return ConfigurationManager.AppSettings.Get("PassServidorLecturaMail");
        }



        public static string GetHostIconstruye()
        {
            return ConfigurationManager.AppSettings.Get("HostIconstruye");
        }

        public static string GetPasswordIcostruye()
        {
            return ConfigurationManager.AppSettings.Get("PasswordIconstruye");
        }

        public static string GetUserIconstruye()
        {
            return ConfigurationManager.AppSettings.Get("UserIconstruye");
        }

     public static bool LeerMailDebug()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("LeerMailDebug"));
        }

        public static bool DeleteDebugMail()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("DeleteDebugMail"));
        }

        public static string GetSubjectMail()
        {
            return ConfigurationManager.AppSettings.Get("SubjectMail");
        }

        public static string GetSubjectMailDebug()
        {
            return ConfigurationManager.AppSettings.Get("SubjectMailDebug");
        }

        public static string GetRutaXmlProcesados()
        {
            return ConfigurationManager.AppSettings.Get("PathXMLProcesados");
        }

        public static bool ExecuteLecturaMail()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("ExecuteLecturaMail"));
        }

        public static bool ExecuteLecturaMailIconstruye()
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get("ExecuteLecturaMailIconstruye"));
        }
        #endregion

        //public static bool GetEnableSslFromProcesosXml()
        //{
        //    return bool.Parse(ConfigurationManager.AppSettings.Get("EnableSslFromProcesosXml"));
        //}

        public static void SetDebugConfigMode(bool? b)
        {
            DebugConfigMode = b;
        }
        public static bool? GetDebugConfigMode()
        {
            return DebugConfigMode;
        }
    }

}