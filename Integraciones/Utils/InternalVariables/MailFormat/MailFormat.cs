﻿using System.Collections.Generic;

namespace Integraciones.Utils.InternalVariables.MailFormat
{
    public static class MailFormat
    {
        public static readonly Dictionary<int, string> MailsFormats = new Dictionary<int, string>
        {

            // : DELIMITADOR PARA 'OR' LÓGICO (||)
            // ; DELIMITADOR PARA 'AND' LÓGICO (&&) === SOLO PUEDE HABER UNO POR TOKEN
            {0, "@cinemark.cl:CineMark.:96659800:96.659.800:www.cinemark.cl"},
            {1, "ARCOS DORADOS RESTAURANTES DE CHILE LTDA"}, //APARDO
            {2, "96856780"}, //APARDO CONSALUD ARTIKOS
            {3, "96770100"}, //APARDO ALEMANA ARTIKOS
            {4, "96.937.360-2:96.683.200-2:96924740-2:96.819.630-8:77.726.740-K:76.460.560-8:97.036.000-K:96.556.210-9:96.924.740-2:96.524.260-0"}, //SANTANDER PRUEBAS
            {5, "96801810:80914400:96670840:96671880:78929230"}, // CIM HOLDING - MZAPATA
            {6, "Unilever:cmedina@dimerc.cl" }, //Unilever APARDO
            {7, "RUT||LOCACION||FEC.DESPAC.||DESPAC.||TOT.DESPAC.||DIREC." },
            //{8,"70.990.700-K" }//VTR APARDO
            {9,"Equipo de AFUBACH." },
            {10, "Sherwin-Williams:Sherwin-Williams Company"}

        }; 
    }
}