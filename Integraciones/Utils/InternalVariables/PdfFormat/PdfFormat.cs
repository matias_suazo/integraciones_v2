﻿using System.Collections.Generic;

namespace Integraciones.Utils.InternalVariables.PdfFormat
{
    public static class PdfFormat
    {
        public static readonly Dictionary<int, string> PdfFormats = new Dictionary<int, string>
        {
            // : DELIMITADOR PARA 'OR' LÓGICO (||)
            // ; DELIMITADOR PARA 'AND' LÓGICO (&&) === SOLO PUEDE HABER UNO POR TOKEN
            {300, "PUC UO;81698900" }, //APARDO
            {0, "EASY RETAIL S.A."}, //MZAPATA
            //{1, "Cencosud Retail S.A."}, //MZAPATA
            //{2, "Cencosud Shopping:76568660:78408990:Cencosud Fidelidad:81201000;Orden de compra:Cencosud Retail S.A"}, //MZAPATA --Cencosud S.A:
            {2, "81201000:Cencosud Shopping:81201000-K:76568660:78408990:Cencosud Fidelidad:Cencosud Retail S.A:76203299:Costanera Center S.A.:99586230-1:Administradora TMO:76951464:CENCOSUD RETAIL S.A."}, //ASEPULVEDA --Cencosud S.A:
            {-2, "93834000;Orden de compra" },
            {-1, "CeCo SAP FICO"}, //MZAPATA
            {3, "INDRA SISTEMAS CHILE S.A."}, //MZAPATA
            {4, "SECURITAS S.A.;Orden de compra"}, //MZAPATA // eso habia que agregarle xd
            {
                5, "71.540.100-2:Universidad Nacional Andrés Bello:Laureate Chile II SpA:Inmobiliaria Educ SPA (IESA):" +
                   "Servicios Profesionales Andrés Bello SPA:IEDE Chile Institute for Executive Development S.A"
            }, //MZAPATA
            {6, "99579260:Empresa de Serv. Externos ACHS S.A.:76481620-K"}, //MZAPATA
            {7, "BHP Billiton Ltd"}, //MZAPATA
            {8, "CLINICA DAVILA Y SERVICIOS MEDICOS S.A.:CLÍNICA DAVILA Y SERVICIOS MÉDICOS SPA:CLINICA DAVILA Y SERVICIOS MEDICOS S.P.A.:96530470"}, //MZAPATA
            {-8, "ANEXO PARA ENTREGAS DIFERIDAS DE OC"}, //MZAPATA
            {9, "EMPRESAS CAROZZI S.A."}, //MZAPATA
            {10, "Securitas Austral Ltda."}, //MZAPATA
            {11, "DOLE CHILE S.A."}, //MZAPATA
            {12, "Capacitaciones Securicap S.A"}, //MZAPATA
            {13, "Universidad de las Americas"}, //MZAPATA
            {14, "Instituto Profesional AIEP SPA"}, //MZAPATA
            {15, "Clínica Alemana de Santiago S.A."}, //MZAPATA
           // {-15, " Clínica Alemana "}, //MZAPATA
            {16, "Univ. de Viña del Mar,Chile Op"}, //MZAPATA
            {17, "DELLANATURA S.A"}, //MZAPATA
            {18, "COMERCIAL TOC´S LIMITADA"}, //MZAPATA
            {19, "Desarrollos Tecnológicos S.A.:96.780.490-8"},//MZAPATA
            {20, "Komatsu Cummins Chile LTDA"}, //MZAPATA
            {23, "ISAPRE CONSALUD S.A.:ISAPRE CONSALUD S.A. "}, //MZAPATA
            {24, "IANSAGRO S.A.:EMPRESAS IANSA S.A.:IANSA ALIMENTOS S.A.:76.857.210-0:77.155.289-7:AGROCOMERCIAL"}, //MZAPATA
            {25, "TNT EXPRESS CHILE LTDA:TNT Exp WW (Chile) Carga:FEDEX EXPRESS CHILE SPA"}, //MZAPATA
            {26, "Servicios Comerciales S.A."}, //MZAPATA
            {27, "Constructora Ingevec S.A.:89.853.600-9:Inmobiliaria Los Boldos Spa"}, //MZAPATA
            {28, "VITAMINA WORK LIFE S.A.:76.407.810-1"}, //MZAPATA
            {29, "MATERIALES Y SOLUCIONES S.A.:96.692.790 - 9"}, //MZAPATA
            {30, "Clariant (Chile) Ltda.:ARCHROMA CHILE LTDA.:Clariant Plastics & Coatings (Chile) Ltda"}, //MZAPATA
            {31, "CAMDEN SERVICIOS SPA"}, //MZAPATA
            {32, "Servicios Andinos SpA"}, //MZAPATA
            {33, "GESTION DE PERSONAS Y SERVICIOS LIMITADA:GESTION DE PERSONAS Y SERVICIOS SpA:GEPYS EST SpA:78.092.910-3"}, //MZAPATA
            {35, "OFFICE STORE SpA "}, //MZAPATA
            {36, "CLINICA LAS LILAS S.A."}, //MZAPATA
            {37, "Abengoa Chile"}, //MZAPATA
            {38, "Clínica de la Universidad de los Andes:Universidad de los Andes"}, //MZAPATA
            {40, "Bupa Chile Servicios Corporativos Spa:Exámenes de Laboratorio S.A.:Integramedica S.A"}, //MZAPATA
            {41, "ECORILES S.A."}, //MZAPATA
            {42, "Komatsu Cummins Chile Arrienda S.A"}, //MZAPATA
            {43, "96879440-K:76098454-K:76217761:BUPA Servicios Clinicos S.A:Integramedica Establ. medicos Atencion Ambulatoria:Sonorad I S.A.:SonoradI S.A.:SonoradIS.A.:BupaChileServicios CorporativosSpa:IntegramedicaEstabl.medicosAtencionAmbulatoria:ExámenesdeLaboratorioS.A.:BupaChileS.A.:76242774-5:76217768-4:76217768:76024387"}, //MZAPATA
            {44, "Komatsu Reman Center Chile S.A:76.492.400"}, //MZAPATA
            {45, "Distribuidora Cummins Chile"}, //MZAPATA
            {46, "GEPYS EST LIMITADA"}, //MZAPATA
            {48, "CIA DE SEG. DE VIDA CONSORCIO NAC. DE SEG. S.A.:CN LIFE:96654180"}, //MZAPATA
            {49, "MEGASALUD S.A.:Millacura S.A.:MEGASALUD S.A.:96942400:MEGASALUD SPA:MEGASALUD S.p.A.:96.942.400-2"}, //MZAPATA
            {50, "Celulosa Arauco y Constitución S.A.:Paneles Arauco S.A.:Maderas Arauco S.A.:M a d e r a s A r a u c o S . A .:Servicios Logísticos Arauco S.A.;por Ariba Network."}, //MZAPATA
            {51, "KAEFER BUILDTEK S.p.A.;Item Código Descripción Cantidad"}, //MZAPATA
            // {53, "INTERTEK" },//APARDO Intertek
            {54, "ANÁLISIS AMBIENTALES S.A."}, //MZAPATA
            {55, "AFP Habitat S.A."}, //MZAPATA
            //{56, "Corporación de Desarrollo Tecnológico"}, //MZAPATA
            //{57, "AGUAS ANDINAS S.A."}, //MZAPATA
            {58, "INGENPROJECT LTDA."}, //MZAPATA
            {59, "Sociedad Educacional La Araucana S.A."}, //MZAPATA
            {60, "FAGASE S.A. - DUNKIN' DONUTS"}, //MZAPATA
            {61, "Teveuk Ltda."}, //MZAPATA
            {62, "76.858.590-3:76858590-3:76.858.590"}, //MZAPATA - Colegio Coya
            {63, "82.648.400-4:82648400-4:8264840"}, //MZAPATA - Sociedad de Instrucción
            {64, "Shaw Almex Chile S.A."}, //MZAPATA
            {65, "UNITED NATIONS"}, //MZAPATA
            {66, "INGENIERIA Y COMERCIALIZADORA RIEGO 2010 S.A."}, //MZAPATA
            {67, "ENVASADOS MOVIPACK CHILE LTDA.;ORDEN DE COMPRA"}, //MZAPATA
            {68, "Larraín y Asociados Ltda.:Larrain y Asociados"}, //MZAPATA
            {69, "Laboratorios LBC Limitada"}, //MZAPATA
            {70, "76.053.505-2:76053505"}, //MZAPATA PROYEKTA S.A.
            {71, "Biomedical Distribution Chile Ltda"}, //MZAPATA
            {72, "LOGISTICA INDUSTRIAL S.A:TRILOGIC:AUSTRAL FREEZER LOGINSA:96878980:BIOMEDICAL DISTRIBUTION CHILE:SERVICIOS EMPRESARIALES LOGINSA LIMITADA"}, //MZAPATA - AGREGAR CC
            {73, "Depósitos y Contenedores S.A:96.813.450-7"}, //MZAPATA - AGREGAR
            {74, "ARIDOS SANTA FE S.A:INGENIERIA Y CONSTRUCCIONES SANTA FE:CENTROVIAL S.A."}, //MZAPATA            
            //{75, "CEMENTOS TRANSEX LTDA"}, //MZAPATA
            {76, "EZENTIS CHILE S.A."}, //MZAPATA
            {77, "RIMASA LTDA."}, //MZAPATA
            {78, "GRUPO EULEN CHILE S.A.;96.997.370-7"}, //MZAPATA
            {79, "EULEN CHILE S.A.;96.937.270-3"}, //MZPATA
            {80, "PETROBRAS CHILE RED LTDA"}, //MZAPATA
            {81, "PETROBRAS CHILE DISTRIBUCIÓN LTDA"}, //MZAPATA
            //{82, "EULEN SEGURIDAD S.A."}, //MZAPATA
            {83, "Empresa de Serv. Externos ACHS S.A.:76198822-0:Empresa Servic.Externos ACHS"}, //MZAPATA
            {84, "RECURSOS PORTUARIOS Y ESTIBAS LIMITADA"}, //MZAPATA
            {85, "TECNOLOGIA EN TRANSPORTE DE MINERALES S.A."}, //MZAPATA
            {86, "Capacitación en Ciencias y Tecnologías Limitada"}, //MZAPATA
            {87, "MAQUINARIAS SANTA FE S.A."}, //MZAPATA
            {88, "MEDICA SAN BERNARDO S.A."}, //MZAPATA
            {89, "OPERADORA AUTOPISTA DE LOS ANDES LIMITADA"}, //MZAPATA
            {90, "IMPORTADORA E INVERSIONES PROLAB LTDA."}, //MZAPATA
            //{91, "CEMENTERIO METROPOLITANO LTDA"}, //MZAPATA
            {92, "Sembcorp Aguas"}, //MZAPATA
            {93, "Ascensores Schindler (Chile) S.A."}, //MZAPATA
            {94, "HOSPITAL DEL PROFESOR"}, //MZAPATA
            {95, "78.223.950-3:RVC CONSTRUCTORA LTDA:76.183.638-2:RVC Servicios SpA:Barrio Bosque Inglés 5 SPA:INMOBILIARIA LOS SACRAMENTINOS S. A.:96.996.170:Santiago Sur 1 SpA:76.319.987-8"}, //MZAPATA
            {96, "77276280:INDUSTRIALSUPPORTCOMPANYLIMITADA:Industrial Support Company SpA:77.276.280-1"}, //MZAPATA
            //{97, "71644300;UNIVERSIDAD;DESARROLLO"}, //UDD - MZAPATA
            {98, "99.597.250-6:96.725.460-6"}, //MZPATA
            {99, "FUNDACIÓN INSTITUTO PROFESIONAL DUOC UC:FUNDACIÓN DUOC"}, //MZAPATA
            {100, "Oficina Axis Central:C.D.I. San Ramon:"+
                "Boulevard Piedra Roja:"+
                "Maquinarias Axis:"+
                "Bodega Axis Central:"+
                "Desarrollos Constructivos Axis"}, //MZAPATA PENDIENTE
            {101, "Aura Ingenieria S.A"}, //MZPATA
            {102, "REMA TIP TOP CHILE SPA:TIP TOP  SERVICE SPA"}, //MZAPATA
            {103, "CONSTRUCTORA ARMAS LTDA:0079578400-4:79987930"}, //MZAPATA
            {104, "ARMAS GESTION LIMITADA:78423860:79987930"}, //MZPATA
            {105, "CONSTRUCTORA ARMAS LTDA;79578400-4"}, //MZAPATA
            {106, "99507560"}, //MZAPATA -- READYMIX
            {107, "BUSES VULE S.A."}, //MZAPATA
            
            {108, "Nestlé Chile S.A."}, // MZAPATA
            {109, "MORTEROS TRANSEX S.A.:SOCIEDAD MINERA ROSARIO LTDA.:HORMIGONES TRANSEX LTDA.:CEMENTOS TRANSEX LTDA"}, //MZAPATA
            {110, "DELLAFIORI Y RIOS ARQUITECTURA CONSTRUCCION Y GESTION "}, //MZAPATA
            {111, "96.591.730:96.849.490:76.007.873:96.800.040:96.796.080"}, //HOLDING MARVAL - MZAPATA
            {112, "96.541.340-5" },//HYATT - MZAPATA
            {113, "76.871.990" },//CLINICA CORDILLERA - MZAPATA
            {114, "96.513.310" }, //INARCO - MZAPATA
            {115, "76.078.231" },// EDAM - MZAPATA
            {116, "GAS ATACAMA CHILE S.A." }, // MZAPATA
            {117, "99577050-4" }, // REDBUS - MZAPATA 
            {118, "Milán Fabjanovic y Cia. Ltda." },// MZAPATA
            {119, "Samsung Electronics Chile Ltda" }, // MZAPATA
            {120, "66522720;2222464;2223810;centraldecompras@ucsh.cl" }, // U.C. Silvia Enriquez MZAPATA
            {121, "SERVICIOS DE ASEO Y JARDINES MACLEAN LTDA" }, //MZAPATA
            {122, "94623000-6:96550960:79556490" }, // SODEXO CHILE iCONSTRIUYE - MZAPATA
            {123, "96.853.940:76.004.090:76.248.012-3" }, //PROMET SPA - MZAPATA
            {124, "ABASTECEDORA DE SUPERMERCADOS S.A."}, // MZAPATA
            {125, "91.361.000-8" }, //FERRETERIA SANTIAGO - MZAPATA
           // {126 , "JUMBO;81201000;Proveedor"}, //JUMBO -MZAPATA

            {9999, "SKU CANTIDAD PRECIO"}, //SALVA VIDAS =<=>= MZAPATA

            //LIBRE EL 34

            {200, "CERVECERA CCU CHILE LTDA."}, //APARDO
            {201, "CONSTRUCTORA SANTAFE S.A."}, //APARDO
            //{202, "SEIDOR CHILE S.A"}, //APARDO
            {203, "CFT SAN AGUSTIN DE TALCA"}, //APARDO
            {204, "77.177.430-K"}, //APARDO Alimentos San Martin 
            {205, "77.038.090-1"}, // APARDO Zical
            {206, "CLUB AEREO DEL PERSONAL DE CARABINEROS"}, //APARDO
            {207, "Maestranza Mining Parts Ltda."}, //APARDO falta pareo de códigos
            {208, "76.378.317-0"}, //APARDO falta tomar items
            {209, "Constructora Lampa Oriente S.A."}, //APARDO falta tomar items
            {
                210,
                "Masterline S.A.:Antonio Martínez y Compañía:Operaciones Integrales Chacabuco S.A." +
                ":Operaciones Integrales Coquimbo Ltda.:Inversiones Vista Norte S.A.:96.725.460:"+
                "Operaciones Turísticas S.A.:99.598.900-K:76.951.696-4:76.141.988-9:99.599.340-6:76.598.536-6:Casino del Mar S.A.:Casino Gran Los Angeles S.A.:99.599.340-6"
            }, // APARDO ENJOY  falta pareo cliente
            {211, "Comercializadora de Máquinas Columbia Limitada"}, //APARDO 
            {212, "Joy Global (Chile) S.A."}, //APARDO
            {213, "Academia de Guerra del Ejército"}, //APARDO
            {214, "61.602.057-9"}, //APARDO Servicio de Salud Valpo San Antonio (HOSPITAL EDUARDO PEREIRA)
            {215, "Larraín Prieto Risopatron S.A."}, // APARDO
            {216, "Constructora Brotec Icafal Ltda."}, //APARDO a la espera de pareo de código
            //{217, "Las empresas Agrosuper"}, //APARDO Agrosuper FAENADORA SAN VICENTE  LTDA
            {218, "76.195.290"}, //APARDO revisando TRAZA faltan OC para revisar
            {219, "KIPREOS INGENIEROS S.A."}, //APARDO listo pero faltan mas OC 
            {220, "96,995,990-9"}, //APARDO Metalurgia Caceres
            {221, "99.545.580-3"}, //APARDO ABASTECEDORA GRAL DE INSUMOS EL ALTO S.A
            {222, "77.409.600-0"}, //APARDO SERVICIOS INTERNET LTDA.
            {223, "89037500-6"}, //APARDO Sigro falta pareo de código
            {224, "CIRCULO EJECUTIVO LTDA."}, //APARDO
            {225, "76.215.260-6"}, //APARDO NEMO CHILE
            {226, "INMOBILIARIA CLÍNICA SAN CARLOS DE APOQUINDO S. A"}, //APARDO
            {227, "BIONET S.A."}, //APARDO
            {228, "EASTON LTDA"}, //APARDO
            {229, "procircuit.cl"}, //APARDO
            {230, "76.363.534-1"}, //APARDO AVAL CHILE
            {231, "Intertek Caleb Brett Chile S.A.:Moody International Holdings Chile Ltda."}, // INTERTEK CALLEB
            {232, "76673650-5:LABORATORIOS GARDEN HOUSE FARMACEUTICA S.A.:LABORATORIOS GARDEN HOUSE HOLDING S. A."}, //GARDEN HOUSE
            {233, "Reclamación para"}, //Recordatorios Arauco
            {234, "Mademet Servicio"}, //APARDO no es posible integración por descripciones manuales.
            {235, "Komatsu Chile S.A.:76.080.246-8"}, //APARDO
            {236, "Corporate  Unilever Chile Ltda. RUT 92091000­9:Artículos ergonómicos Vero Gil"}, // APARDO Unilever
            {237, "71644300-0"}, //UDD APARDO
            {238, "INGELOG CONSULTORES DE INGENIERIA:NORCONTROL CHILE, S.A.:96619100:99581810"}, //APARDO
            {239, "CONSTRUCTORA PASEO LAS CONDES S.A."}, //APARDO
            //{240, "Transportes Santa María S.A.:Transportes Santa María Spa."}, //APARDO
            {241, "77.057.383-1:91.300.000-5:96.684.600-3:86.856.700-7:77.558.130-1:76.053.696-2:76.929.210-1:76.380.365-1:79.723.890-2:76.363.031:96.967.010-0:96.954.630-2:93.659.000-4:96.962.430-3:99.506.950-4:96.557.400-K" }, //APARDO SALFA, VARIOS RUT DISTINTOS, IR AGREGANDO  EMPRESA CONSTRUCTORA TECSA S.A.
            {242, "Ernst & Young" },// APARDO
           // {243, "Solicitud de Materiales" },//APARDO
            { 244, "Ingeniería Civil Vicente S.A."},//APARDO
            {245, "96.945.670-2" }, //NovoFarma
            {301, "Flores y Cia. S.A.:Flor es y Cia. S.A."}, //APARDO
            {302, "DE VICENTE PLÁSTICOS S.A.:89.689.900-7" }, //APARDO
            {585, "CPT REMOLCADORES S.A.:76.037.572-1:CPT Remolcadores S.A.:CPT Empresas Marítimas S.A." }, //REMOLCADORES APARDO
            {304, "VALKO MAQUINARIAS LTDA." }, // VALKO MAQUINARIA APARDO
            {305, "Workplaces Ltda." }, //WorkPlaces APARDO
            {306, "ISA GV SPA ISA GV SPA Obra:Empresa Constructora Ingenieros S.A" }, //ISA APARDO  
            {307, "77.005.150-9:76.120.070-4" },//cynersis
            {308, "BCI ASSET MANAGEMENT A.G.F" }, //APARDO 
            {309, "ALFRED H KNIGHT;Orden de Compra" },//APARDO
            {310, "PONTIFICIA:81.698.900:UNIVERSIDAD CATÓLICA" }, //APARDO
            {311, "CLINICA LAS CONDES S.A.:CLINICALASCONDESS.A.:SERVICIOS DE SALUD INTEGRADOS S.A." }, //clinica las condes
            {313, "Ideas Educativas SpA;77.262.930-3" }, //APARDO
            {314, "Emin Ingeniería y Construcción S.A.:79527230-5" }, //APARDO
            {315, "Industrial y Comercial Solucorp Ltda"}, //APARDO
            {316, "Corporación Nacional del Cobre de Chile"}, //APARDO
            {317, "96.600.850-4" }, //APARDO LOS CARRERA CLINICA SERVISALUD
            {318, "PC - OCI - 003" }, //APARDO ALCAINO
            {319, "Promasa S.A." }, //APARDO
            {320, "78.058.040-2:Centro de Diálisis San Antonio Ltda." }, //APARDO
            {321, "76.157.430-2"}, //APARDO
              {322, "81.826.800-9"}, //APARDO
            // {323, "TECNORED S.A"}, //APARDO
            {324, "www.mts.cl" }, // APARDO MTS 2
            {325, "Empresa de Correos de Chile;DIMERC" }, // APARDO
            {326, "EMPRESA CONSTRUCTORA PRECON S.A." }, //APARDO
           // {327, "CONCESIONARIA EMBALSE CONVENTO VIEJO S.A" }, //APARDO
            //{328, "CONSTRUCTORA SUDAMERICANA" }, //APARDO
           // {329, "Universidad Diego:70.990.700­K" }, //APARDO
            {330, "KRONTEC CAPACITA LTDA." }, //APARDO
            {331, "96613220-5" }, //APARDO
           // {332, "CONSTRUCTORA EXCON S.A." }, //APARDO
            {333, "MASISA S.A:Masisa Forestal S.A.:MASISA FORESTAL SpA:96.802.690" }, //APARDO
            {334, "77.532.050-8" }, //APARDO
            {335, "AUTOMOTORES GILDEMEISTER SPA" }, //APARDO 
            {336, "Razón Social Artikos Chile S.A." }, //APARDO
            //{500, "JC INGENIERÍA SPA:52. Sandvik Chile S.A-9"}, //Asepulveda - última versión
            //{500, "94.879.000-9:52. Sandvik Chile S.A-9"}, // sandvik
            {502, "EVH INGENIERIA E.I.R.L.; 52.004.641-0"}, //Asepulveda
            {503, "18117358" }, //prueba ricardo     
            {504,"77.560.700-9:ZÜBLIN CHUQUICAMATA SpA" },  // ricardo, FALTA PAREO DE CODIGOS 
            {505, "61310000:INSTITUTO DE FOMENTO PESQUERO" },
            {506, "96616770:INGENIERIA INFORMATICA KIBERNUM S.A" },
            //{507, "Condición Cotización Valida:hasta Agotar Stock" }, //coti dimerc 
           // {508, "CONSORCIO EXCON-ASFALCURA S.A." }  ,
            {509, "90753000-0:CLINICA SANTA MARIA" } ,// CSM
            {510, "Inmobiliaria Chillan Spa:76.164.519-6" },
            {511, "CONSTRUMART S. A.:96.511.460-2" }, // construmart 1
            {512, "Emisor CONSTRUMART S.A." },
            {513, "89414100-K:REPUESTOS DE TRACTORES Y CAMIONES" }, // parecido a gildemeister
            {514, "76.388.852-5:76.556.170-1"}, // CONSORCIO TEPSAC 76.388.852-5
            {515,"77428200-9" }, // constructora BYC SAMAR
            {516, "0089696400-3" }, // RESITER
            {517, "76.005.049-0:76.247.273-2:76.081.976-K:85.747.000-1:76.984.506-2" }, // ECHEVERRIA IZQUIERDO
            {518, "96.817.680-3:APL Logistics Chile S.A." },
            {519, "ARRIGONI METALURGICA S.A.:80.893.200-8" },
            {520, "CIA. PISQUERA DE CHILE S.A.:99586280-8" }, //CCU RICARDO
            {521, "RUT 99558680-0:RUT 96757030-3:RUT 94840000-6" }, // SOCOVESA
            {522,"CIF        76.219.663-8:Vertiv"},
            {523,"76.701.440-6:PI BERRIES S.A" },
            {524, "96.554.890-4" }, // cpp
            {525, "79740770-4:Ferrocarril de Antofagasta" }, // train transportes integrados
            {526, "Constructora Numair Ltda.:77.506.410-2" },
            {527,  "Arquitectura y Construcción Workplaces SPA.:76315737-7"},
            {528,"Reñaca S.A.:79576810:7 9 5 7 6 8 1 0" },
            {529,"SOC.INDUSTRIAL KUNSTMANN S.A.:90.889.000-0" },
            {530,"76362176-6:PIQUES Y TUNELES S.A." },// besalco
            {531,"ITEM Nº DETALLE MARCA CANT. $ UNITARIO TOTAL" }, // universidad catolica silva 
            {532 ,"chileinox"},
            {533, "61.219.000-3:METRO S A:61219000-3" },
            {534,"JC INGENIERÍA SPA" },
            {535,"Adm.de Supermercados Hiper Ltda.:76.134.941-4:76.134.946-5" },
            {536, "97.053.000-2" }, // banco security
            {537,"96.722.460-K:METROGAS S.A." } ,// MetroGas
            {538,"HDI Seguros S.A.:99231000-6" }, // hdi seguros
            {539,"70360100-6" }, //ACHS
            //{540,"Condición Cotización Valida:99512120:SECURITAS" } //ACHS
            {541,"Constructora Besalco Ltda." }, // besalco2 iconstruye
            {542,"BODEGAS SAN FRANCISCO LTDA" },
            {543,"Telefónica Ingeniería Seguridad S.A." } ,// TELEFONICA 59083900
            {544, "PUERTO CENTRAL S.A.:MUELLAJE CENTRAL S.A." }, // puerto central s.A. 76158513-4
            {545, "STEEL FERROVIAL" }, // 96846410-8
            { 546,"96.846.410-8:ORDEN DE COMPRA DIRECTA"},
            {547,"96.564.890-9:INSTITUTO DE SALUD INTEGRAL S.A" },
            { 548, "96.691.330-4:DICTUC S.A."}, // dictuc
            {549,"76.027.027-K" },
            {550,"La Araucana Salud S.A." },
            {551,"Ferricom SpA" },
            {552,"C.R.A INGENIERIA SPA" }, // 78202640
            {553,"Servicios Logísticos Arauco S.A.:Maderas Arauco S.A.:Celulosa Arauco y Constitución S.A. " }, // arauco nuevo
            { 554,"KREIS SPA; Orden de Compra"},
            {555,"0093534000-4 " }, // HAPPYLAND
            {556,"UC Christus Servicios Ambulatorios S.p.A:76754097-3:UC Christus Servicios Clínicos SpA.:UCChristus Servicios Clínicos SpA.:UCChristus Servicios ClínicosSpA.:99540210-6:EST.MEDICOS DE ATENCION AMBULATORIA" },
            {557,"EMPRESA CONSTRUCTORA MOLLER Y PÉREZ-COTAPOS:92770000:MOLLER Y PÉREZ-COTAPOS CONSTRUCCIONES INDUSTRIALES S.A." },
            {558,"Aridos Arenex Ltda" },
            {559,"Precon S.A."},
            {560,"Prefabricados y Pretensados de Hormigón S.A"},
            //{561,"92.846.000-2" } // Harting S.A.
            { 562,"Iron Mountain Chile S.A."},
            {563,"Claro, Vicuña, Valenzuela S.A." },
            {564,"Soluciones Ambientales del Norte S.A." },
            {565,"Cereales CPW Chile Ltda." },
            {566,"76.722.441-9" }, // Arquitectura e Ingeniería Aconcagua SPA
            { 567,"Empresa Electrica de Aysen S.A.:Sociedad Austral de Electricidad SA:Empresa Electrica de la Frontera SA"},
            {568,"LANGUES AFFAIRS SpA" },
            {569, "TresMontes S.A.:Tresmontes Lucchetti S.A." },
            {570,"Iron Mountain Chile SA:Storbox SA"},
            {571,"COMERCIAL CCU S.A."},
            {572,"BICE VIDA COMPAÑÍA DE SEGUROS S.A." },
            {573,"EMERSON ELECTRIC (US) HOLDING CORPORATION (CHILE)" },
            {574,"Sandvik Chile S.A.:94.879.000-9:94879000-9:96728780-6" },
            {575,"Centro de Formación Técnica Santo Tomás Limitada"},
            {576,"INMOBILIA S.A." },
            {577,"76.176.676-7" },
            {578,"Laboratorio Ximena Polanco" }, // 5170511
            {579,"SIENA CONSTRUCTORA S.A.;ORDEN DE COMPRA" },
            {580,"79.582.830-3" }, //AGROINDUSTRIAL MACHICURA SPA 79.582.830-3
            {581,"85.241.400-6" }, // SOPRODI
            {582,"76690901-9" } ,// constructora ICF
            {583,"76.084.413-6" }, //  GRAN PAVESI ALIMENTOS LTDA.
            {584,"76.041.105-1" }, // INMOBILIARIA Y CONSTRUCTORA CLASS LTDA.
            {586,"CAPITAL S.A.:96.905.260-1" }, // Capital S.A.
            {587,"76530526-8" }, // inmobiliaria nogales
            {588,"CONSTRUCTORA PARQUE CHICUREO S.A.;ORDEN DE COMPRA" },
            {589,"Comercial Kendall Chile Ltda;ORDEN DE COMPRA" }, // 77237150
            {590,"70.990.700-K:99.574.250-0:72.546.000-7" } ,// UNIVERSIDAD DIEGO PORTALES
            {591 ,"81.643.200-6" }, // COOPERATIVA AGRÍCOLA LECHERA SANTIAGO LTDA.
            {592,"Mercado Público" },
            {593,"INMOBILIARIA LOS OLMOS SPA;ORDEN DE COMPRA" },
            {594,"Telefónica Ingeniería de Seguridad S.A." },
            {595,"Clinica Colonial S.A" },
            {596,"96.951.280-7:96.577.310-K:93.383.000-4:89862200-2:96.518.860-6:96.631.520-2" }, // LATAM
            {597,"@Emerson.com" },
            {598,"76.212.301-0" },
            {599,"97006000-6" },
            {600, "99508670-0"}, //AGRICOLA ALTO ACONCAGUA LIMITADA
            {601, "INMOBILIARIA NUEVO PUENTE ALTO S.A." },
            {602,"91.144.000-8:78.861.790-9" }, // Embotelladora Andina S.A.
            {603, "96924780-1" }, // SAMAR   -- RELIPER
            {604,"78929230-2" }, // samar SIGA Ingeniería
            {605,"88.859.600-3" }, // drillco
            {606,"78023520-9" }, // JORGE SCHMIDT Y CIA LTDA
            {607,"CORPORACION DE EDUCACION Y SALUD" }, // 70902000 
            {608,"Archroma Chile Limitada" }, // Archroma 2
            {609,"76.141.360-0" }, // BELFI
            {610,"INGENIERIA Y CONSTRUCCIONES SANTA FE S.A." }, //99.541.910-6
            {611,"95431000-0" },
            {612,"Enviar factura a consorcio" }, // CONSORCIO CRISTINA MEDINA
            {613,"96.726.970-0" }, // lmella
            {614,"76.472.528-K" },
            {615,"FEDERICO SANTAMARIA" }, // lmella
            {616,"Transportes Santa María SpA" },
            {617,"76.014.173-9" },// CANPLAS SUD
            {618,"70995200-5" }, // universidad central de chile
            {619,"096.670.840-9:77.015.038-:77.015.036-1" }, // INGEVEC 2.0
            {620,"72.012.000-3" }, // INACAP
            {621 ,"89.852.800­-6:G4S Security Services Limitada:G4S Security Services:GRUPO SECURITY S.A.:96.604.380-6" }, // CMEDINA
            {622, "76070080:76034897:76259020:76259040:76675510:96916960:76154079:76418768:76807793-2:76230125-3:76948230-K:76154079-3:76259020-4:76418768-7:76891224-6:76954691-K:76954681-2:76954686-3:76954694-4:76370479:76370479-3:76954691-K" },//Constructura de Vicente
            {623, "76.801.420-5" },//Servicio de Venta Inmobiliaria Aconcagua
            //{624, "76.042.103-0" },//Megalogistica
            {625, "77.215.640-5" },//Arcoprime
            //{626, "78.910.400-K" },//Ariztia
            {627, "81.106.900-0" },//CooperarivaRegional
            //{628, "81201000;Núm. Orden/ Fecha Creación" },//cencosud(santa isabel)
            {629, "72323600-2" },//hogar de cristo
            {630, "77.555.640-4" },//Zublin Internacional
            {631, "76.095.267-2" },//AVIS
            {632, "96701340-4:85272800" },//El abra-minera candelaria
            {633, "76.555.870-0:96.606.750-0" },//Clinica alemana de Valdivia
            //{634, "86968900-9" }//Nexxo
            {635, "96651910-K:96554490:78544560" },//COMPASS
            {636, "Minera Spence SA:79587210:94621000:86542100:86160300" },//Spence
            {637, "96912870-5" },//G4S (96912870)
            {638, "96633150-K:76065596-1:93711000-6:76143821-2:77970900-0"},//Camanchaca
            {639, "96617350-5" },//omesa
            {640, "85.025.700-0" },//GSK
            {641, "90.786.000-0"},//Corpora
            {642, "76098454-K;DIRECCIÓN ENTREGA:"},//Integramedica Nuevo
            {643, "78.782.700 - 4"},//Ilsauspe
            {644, "88689100-8:96858530-4:96902900-6:76485826"},//Engie
            {645, "Sodexo Chile SPA:94623000:79556490:96883290:96550960:96556930:76098841" },//Sodexo
            {646, "71.551.500-8:Universidad Santo Tomás:Santo Tomás:86.114.900-5" },//Santo tomas
            {647, "76440132:NEPHROCARECHILE: NEPHROCARECHILE S.A.:99507130-4:96640350" },//9977675
            {648, "99.512.110-7:GRUPO NORTE SEGURIDAD S.P.A.:Grupo Norte  S.P.A.:Grupo Norte S.P.A.:86.422.800-3:GRUPO NORTE:Grupo Norte" },//Grupo Norte
            {649, "IsapreCruzBlancaS.A.:96501450-0" },//IsapreCruzBlanca
            {650, "88.056.600-8:NEPHROCARE:NEPHROCARECHILE:NEPHROCARECHILE S.A.:99507130-4" },//Nephrocare
            {651, "FedEx Express Chile SpA:FedEx:fedex" },
            {652, "Vulco S.A:91619000-K" },
            {653, "VAPOR INDUSTRIAL SPA:93.751.000-4:VAPOR INDUSTRIAL" },
            {654, "Soho SpA:Soho:96858420-0" },
            //{655, "96540490" } promasa
            {656, "99012000" },
            {658, "86247400:79872420:96509550:88274600:79800600:76452811:76300265:87782700:78512930:99595500:76125666:76495180:78754560" },
            {659, "96.706.220-0:SACYR" },
            {660, "anasac.cl" },
            {661, "WATTS SA:84356800-9" },
            {662, "Pontificia Universidad Católica de Chile" },
            {663, "763093468:ZARA HOME CHILE:ZARA CHILE:967858609" },
            {664, "LATIN GAMING CALAMA:99.599.080-6"},
            {665, "Line-SCH Vendor Item Description:Purchase Order Date Revision Page#" },
            {666, "Areas S.A.Chile Ltda:77.535.730-4" },
            {667, "Fundación Las Rosas:70543600-2" },
            {668, "77860930-4:Fluitek Marco SpA."},
            {669, "77166811:VOY SANTIAGO SPA"}
            //TEST
            
            

        };
    }
}