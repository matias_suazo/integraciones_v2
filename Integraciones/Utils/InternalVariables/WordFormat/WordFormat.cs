﻿using System.Collections.Generic;

namespace Integraciones.Utils.InternalVariables.WordFormat
{
    public static class WordFormat
    {
        public static readonly Dictionary<int, string> WordFormats = new Dictionary<int, string>
        {

            // : DELIMITADOR PARA 'OR' LÓGICO (||)
            // ; DELIMITADOR PARA 'AND' LÓGICO (&&) === SOLO PUEDE HABER UNO POR TOKEN

            {0, "Trafigura Chile Ltda"}, //MZAPATA
        };

    }
}