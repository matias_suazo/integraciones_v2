﻿using System.Collections.Generic;

namespace Integraciones.Utils.InternalVariables.TxtFormat
{
    public static class TxtFormat
    {

        public static readonly Dictionary<int, string> TxtFormats = new Dictionary<int, string>
        {
            // ; DELIMITADOR PARA 'AND' LÓGICO (&&)
            // || DELIMITADOR PARA 'OR' LÓGICO (||)
            //   SOLO PUEDE HABER UNO POR TOKEN
            {0, "Tienda : Flores*Tienda : Tienda Flores"}, //MZAPATA
            {1, "Orden compra;Emision;Entrega;Codigo Proveedor;Proveedor;Producto;Codigo interno;Cantidad;Precio;Formato;Q.Minima;Pedido;Ceco;Despacho;Obs.Comercial;" }, //APARDO
            {2,"LABORATORIO DRAG PHARMA" },
            {3,"EMPRESAS DMG S.A." }, // DIMEIGGS
            {4,"85555900" },
            {5,"NRO_OD|FECHA_EMISION_OD|NRO_OC|RUT|DV_RUT|RAZON_SOCIAL|FECHA_EMISION|UPC|SKU|DESCRIPCION_LARGA|MODELO|TALLA|COLOR|NRO_LOCAL|LOCAL|UNIDADES|EMPAQUES" } // b2b bquiroz

            };
    }
}