﻿using Hardcodet.Wpf.TaskbarNotification;
using Integraciones.Utils.InternalVariables;

namespace Integraciones.View
{
    /// <summary>
    /// Lógica de interacción para IntegracionPDF.xaml
    /// </summary>
    public partial class IntegracionesView
    {
        //public enum AppState
        //{
        //    INACTIVO,
        //    ANALIZANDO_ORDENES_PDF,
        //    ANALIZANDO_ORDENES_EXCEL,
        //    ANALIZANDO_ORDENES_MAIL
        //}

        //public AppState PDF_STATE = AppState.INACTIVO;
        //public AppState EXCEL_STATE = AppState.INACTIVO;
        //public AppState MAIL_STATE = AppState.INACTIVO;
        //public AppState MAIL_ICONSTRUYE_STATE = AppState.INACTIVO;
        //public AppState MAIN_STATE = AppState.INACTIVO;

        private static IntegracionesView _instance;
        public static IntegracionesView Instance => _instance ?? (_instance = new IntegracionesView());
        
        public IntegracionesView()
        {
            InitializeComponent();
            miDebug.Visibility = InternalVariables.ShowDebug();
        }

        public void ShowBalloon(string title, string desc, BalloonIcon icon)
        {
            NotifyIcon.ShowBalloonTip(title, desc, icon);
        }
    }
}
