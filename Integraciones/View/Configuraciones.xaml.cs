﻿using System;
using System.util;
using System.Windows;
using System.Windows.Forms;
using Integraciones.Utils.InternalVariables;
using Integraciones.ViewModel;

namespace Integraciones.View
{
    /// <summary>
    /// Lógica de interacción para Configuraciones.xaml
    /// </summary>
    public partial class Configuraciones
    {
        public Configuraciones()
        {
            InitializeComponent();
            TxtOcAProcesarFolder.Text = InternalVariables.GetOcAProcesarFolder();
            TxtLogFolder.Text = InternalVariables.GetLogFolder();
            TxtOCProcesadasFolder.Text = InternalVariables.GetOcProcesadasFolder();
            
        }

        private void SetHoraInicio(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;
            //var x = new TimePicker(this) { Owner = this };
            //x.Show();
        }

        private void SetLogFolder(object sender, RoutedEventArgs e)
        {
            var openFolder = new FolderBrowserDialog
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                Description = "Seleccione la Carpeta donde se Guardaran los Archivos del Log",
                ShowNewFolderButton = true
            };
            if (openFolder.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            TxtLogFolder.Text = openFolder.SelectedPath; 

        }

        private void SetOcProcesadasFolder(object sender, RoutedEventArgs e)
        {
            var openFolder = new FolderBrowserDialog
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                Description = "Seleccione la Carpeta donde se almacenaran las Ordenes de Compra Procesadas",
                ShowNewFolderButton = true
            };

            if (openFolder.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            TxtOCProcesadasFolder.Text = openFolder.SelectedPath;
        }

        private void SetOaProcesarPath(object sender, RoutedEventArgs e)
        {
            var openFolder = new FolderBrowserDialog
            {
                Description = "Seleccione la Carpeta donde se Cargarán las Ordenes de Compra a Procesadar",
                RootFolder = Environment.SpecialFolder.MyComputer,
                ShowNewFolderButton = true
            };
            if (openFolder.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
            TxtOcAProcesarFolder.Text = openFolder.SelectedPath;
        }

        private void OnClosed(object sender, EventArgs e)
        {
            NotifyIconViewModel.SetCanShowConfigurationsCommand();
        }


        private void GuardarCambios(object sender, RoutedEventArgs e)
        {
            if (!TxtLogFolder.Text.Equals(InternalVariables.GetLogFolder()))
            {
                InternalVariables.ChangeLogFolder(TxtLogFolder.Text);
            }
            if (!TxtOCProcesadasFolder.Text.Equals(InternalVariables.GetOcProcesadasFolder()))
            {
                InternalVariables.ChangeOcProcesadasFolder(TxtOCProcesadasFolder.Text);
            }
            if (!TxtOcAProcesarFolder.Text.Equals(InternalVariables.GetOcAProcesarFolder()))
            {
                InternalVariables.ChangeOCaProcesarFolder(TxtOcAProcesarFolder.Text);
            }
            NotifyIconViewModel.SetCanShowConfigurationsCommand();
            Close();
        }

        private void Salir(object sender, RoutedEventArgs e)
        {
            NotifyIconViewModel.SetCanShowConfigurationsCommand();
            Close();
        }
    }
}
