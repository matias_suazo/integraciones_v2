﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using Hardcodet.Wpf.TaskbarNotification;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Integraciones.Main.Email;
using Integraciones.Main.Pdf;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Log;
using Integraciones.ViewModel;
using Integraciones.Main.Excel;
using Integraciones.Utils;
using Timer = System.Timers.Timer;
using Integraciones.Utils.Readers.Email;
using Integraciones.Utils.FileUtils;

//using Timer = System.Timers.Timer;

namespace Integraciones.View
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App
    {
        private const int HORA = 3600000;
        private const int MINUTO = 60000;
        private static Timer _timerPdf;
        private static Timer _timerExcel;
        private static Timer _timerEmail;
        private static bool FirstInstance
        {
            get
            {
                var proces = Process.GetCurrentProcess().ProcessName;
                return Process.GetProcessesByName(proces).Count() == 1
                       || Process.GetProcessesByName(proces).Count() == 2;
            }
        }

        private  IntegracionesView _inicializacion;


        /// <summary>
        /// Conviernte un Archivo HTML a PDF
        /// Tiene Problemas con caracteres UNICODE y ASCII
        /// </summary>
        /// <param name="htmlPath">Ruta de archivo HTML</param>
        public void ConvertHtmlToPdf(string htmlPath)
        {
            var roothPath = htmlPath.Substring(0, htmlPath.LastIndexOf(@"\", StringComparison.Ordinal) + 1);
            var fileName = htmlPath.Replace(roothPath, "");
            fileName = fileName.Substring(0, fileName.LastIndexOf(@".", StringComparison.Ordinal) - 1);
            var document = new Document();
            PdfWriter.GetInstance(document, new FileStream($"{roothPath}/{fileName}.pdf", FileMode.Create));
            document.Open();
            var wc = new WebClient {Encoding = Encoding.Default};
            var htmlText = wc.DownloadString(@"file:///" + htmlPath.ParseToFileUrl());
            //TODO PROBLEMAS CON CARACTERES UNICODE AL MOMENTO DE CREAR PDF
            var htmlarraylist = HTMLWorker.ParseToList(new StringReader(htmlText), null);
            foreach (var t in htmlarraylist)
            {
                document.Add(t);
            }
            document.Close();
        }


        protected override void OnStartup(StartupEventArgs e)
        {
            //var sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(
            // "CORDEL PLASTICO CORRIENTE 1 KILO N 8".ToUpper()
            // , first: true, productosActivos: false);
            //Console.WriteLine($"SKU: {sku}");
            //sku = OracleDataAccess.GetSkuWithMatcthDimercProductDescription(
            // "21204BL DISPENS/JABON 900 ML. A GRANEL ELITE".ToUpper()
            // , first: true, productosActivos: false);
            //Console.WriteLine($"SKU: {sku}");

            //var html = @"C:\Proyectos\Integración PDF\C#\Procesar\DO11004062.html";
            //ConvertHtmlToPdf(html);
            // EmailReader.CreateFoler();
            Console.WriteLine("======= FIN =========");

            if (!FirstInstance)
            {
                MessageBox.Show("No se Puede Abrir la Aplicacion debido a que ya se esta Ejecutando.", "Advertencia",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                Current.Shutdown();
            }
            else
            {
                _inicializacion = IntegracionesView.Instance;
                SetTimeToTimerPdf();
                SetTimeToTimerExcel();
                SetTimeToTimerEmail();
                SetTimeToTimerEmailIconstruye(); 
                CreateAllFolderIfNotExists();                
                Log.StartApp();
                
            }
            InternalVariables.InitializeVariables();
            System.Windows.Forms.Application.Exit();
            Environment.Exit(1);
        }



        private static void CreateAllFolderIfNotExists()
        {
            if (!Directory.Exists(InternalVariables.GetOcAProcesarFolder()))
                Directory.CreateDirectory(InternalVariables.GetOcAProcesarFolder());
            if (!Directory.Exists(InternalVariables.GetLogFolder()))
                Directory.CreateDirectory(InternalVariables.GetLogFolder());
            if (!Directory.Exists(InternalVariables.GetOcProcesadasFolder()))
                Directory.CreateDirectory(InternalVariables.GetOcProcesadasFolder());
            if (!Directory.Exists(InternalVariables.GetOcProcesadasFolderExc()))
                Directory.CreateDirectory(InternalVariables.GetOcProcesadasFolderExc());
            if (!Directory.Exists(InternalVariables.GetUnknownOcFolder()))
                Directory.CreateDirectory(InternalVariables.GetUnknownOcFolder());
            if (!Directory.Exists(InternalVariables.GetErrorFolder()))
                Directory.CreateDirectory(InternalVariables.GetErrorFolder());
        }

        public static void RestarApplication()
        {
            var proces = Process.GetCurrentProcess();
            Process.Start("Integraciones.exe");
            proces.Kill();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            Log.FinalizeApp();
        }
        private static void SetTimeToTimerPdf()
        {
            if (InternalVariables.IsDebug()) return;
            var horas = InternalVariables.GetTiempoHorasCiclo();
            var minutos = InternalVariables.GetTiempoMinutosCiclo();
            _timerPdf = new Timer(horas * HORA + minutos * MINUTO);
            _timerPdf.Elapsed += (sender1, args1) =>
            {
                if (Main.Main.STATE == Main.Main.AppState.INACTIVO)//IntegracionesView.AppState.ANALIZANDO_ORDENES_PDF)
                {
                    Main.Main.ShowBalloon("Información", "Comenzando la Analisis de ordenes de Compra.",
                        BalloonIcon.Info);
                    NotifyIconViewModel.SetCantProcessOrderCommand();
                    using (var worker = new BackgroundWorker())
                    {
                        worker.DoWork += (sender2, args2) => MainPdf.ExecuteLecturaPdf();
                        worker.RunWorkerCompleted += (sender3, arg3) => NotifyIconViewModel.SetCanProcessOrderCommand();
                        worker.RunWorkerAsync();                        
                    }
                }
                else
                {
                    Log.Save("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra debido a que Actualmente se esta ejecutando");
                    Main.Main.ShowBalloon("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra debido a que Actualmente se esta ejecutando.", BalloonIcon.Warning);
                }
                //_timer.Enabled = false;
                //_timer.Stop();
                Thread.Sleep(10000);
                //SetTimeToTimer();
            };
            _timerPdf.AutoReset = true;
            _timerPdf.Enabled = true;
            //_timer.Start();
        }
        private static void SetTimeToTimerExcel()
        {
            if (InternalVariables.IsDebug()) return;
            var horas = InternalVariables.GetTiempoHorasCiclo();
            var minutos = InternalVariables.GetTiempoMinutosCiclo();
            _timerExcel = new Timer(horas * HORA + minutos * MINUTO);
            _timerExcel.Elapsed += (sender1, args1) =>
            {
                if (Main.Main.STATE == Main.Main.AppState.INACTIVO)//IntegracionesView.AppState.ANALIZANDO_ORDENES_EXCEL)
                {
                    Main.Main.ShowBalloon("Información", "Comenzando la Analisis de ordenes de Compra Excel.",
                        BalloonIcon.Info);
                    NotifyIconViewModel.SetCantProcessOrderExcelCommand();
                    using (var worker = new BackgroundWorker())
                    {
                        worker.DoWork += (sender2, args2) => MainExcel.ExecuteLecturaExcel();
                        worker.RunWorkerCompleted += (sender3, arg3) => NotifyIconViewModel.SetCanProcessOrderExcelCommand();
                        worker.RunWorkerAsync();

                    }
                }
                else
                {
                    Log.Save("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra debido a que Actualmente se esta ejecutando");
                    Main.Main.ShowBalloon("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra debido a que Actualmente se esta ejecutando.", BalloonIcon.Warning);
                }
                //_timer.Enabled = false;
                //_timer.Stop();
                Thread.Sleep(20000);
                //SetTimeToTimer();
            };
            _timerExcel.AutoReset = true;
            _timerExcel.Enabled = true;
            //_timer.Start();
        }

        private static void SetTimeToTimerEmail()
        {
            if (InternalVariables.IsDebug()) return;
            if (!InternalVariables.ExecuteLecturaMail()) return;
            var horas = InternalVariables.GetTiempoHorasCiclo();
            var minutos = InternalVariables.GetTiempoMinutosCiclo();
            _timerEmail = new Timer(horas * HORA + minutos * MINUTO);
            _timerEmail.Elapsed += (sender1, args1) =>
            {
                if (Main.Main.STATE == Main.Main.AppState.INACTIVO)//IntegracionesView.AppState.ANALIZANDO_ORDENES_MAIL)
                {
                    Main.Main.ShowBalloon("Información", "Comenzando la Analisis de ordenes de Compra de Mail.",
                        BalloonIcon.Info);
                    NotifyIconViewModel.SetCantProcessOrderMailCommand();
                    using (var worker = new BackgroundWorker())
                    {
                        worker.DoWork += (sender2, args2) => MainEmail.ExecuteLecturaMail();
                        worker.RunWorkerCompleted += (sender3, arg3) => NotifyIconViewModel.SetCanProcessOrderMailCommand();
                        worker.RunWorkerAsync();
                    }
                }
                else
                {
                    Log.Save("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra de MAIL debido a que Actualmente se esta ejecutando");
                    Main.Main.ShowBalloon("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra de MAIL debido a que Actualmente se esta ejecutando.", BalloonIcon.Warning);
                }
                //_timer.Enabled = false;
                //_timer.Stop();               
                Thread.Sleep(50000);
                //SetTimeToTimer();
            };
            _timerEmail.AutoReset = true;
            _timerEmail.Enabled = true;
            //_timer.Start();
        }

        private static void SetTimeToTimerEmailIconstruye()
        {
            if (InternalVariables.IsDebug()) return;
            if (!InternalVariables.ExecuteLecturaMailIconstruye()) return;
            var horas = InternalVariables.GetTiempoHorasCiclo();
            var minutos = InternalVariables.GetTiempoMinutosCiclo();
            _timerEmail = new Timer(horas * HORA + minutos * MINUTO);
            _timerEmail.Elapsed += (sender1, args1) =>
            {
                if (Main.Main.STATE == Main.Main.AppState.INACTIVO)//IntegracionesView.AppState.ANALIZANDO_ORDENES_MAIL)IntegracionesView.AppState.ANALIZANDO_ORDENES_MAIL)
                {
                    Main.Main.ShowBalloon("Información", "Comenzando la Analisis de ordenes de Compra de Mail Iconstruye.",
                        BalloonIcon.Info);
                    NotifyIconViewModel.SetCantProcessOrderMailIconstruyeCommand();
                    using (var worker = new BackgroundWorker())
                    {
                        worker.DoWork += (sender2, args2) => MainEmail.ExecuteLecturaIconstruyeMail();
                        worker.RunWorkerCompleted += (sender3, arg3) => NotifyIconViewModel.SetCanProcessOrderMailIconstruyeCommand();
                        worker.RunWorkerAsync();
                    }
                }
                else
                {
                    Log.Save("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra de MAIL debido a que Actualmente se esta ejecutando");
                    Main.Main.ShowBalloon("Advertencia",
                        "No se Ejecutara el Analisis de Ordenes de Compra de MAIL debido a que Actualmente se esta ejecutando.", BalloonIcon.Warning);
                }
                //_timer.Enabled = false;
                //_timer.Stop();
                Thread.Sleep(100000);
                //SetTimeToTimer();
            };
            _timerEmail.AutoReset = true;
            _timerEmail.Enabled = true;
            System.Windows.Forms.Application.Exit();
            Environment.Exit(1);
            //_timer.Start();
        }


    }

}
