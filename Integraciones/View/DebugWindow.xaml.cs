﻿using System;
using System.Windows;
using Integraciones.ParsingExtention.View;
using Integraciones.ViewModel;
using Microsoft.Win32;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Readers.Excel;

namespace Integraciones.View
{

    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class DebugWindow
    {
        public DebugWindow()
        {
            InitializeComponent();
            checkBoxDebug.IsChecked = InternalVariables.IsDebug();
            InternalVariables.SetDebugConfigMode(InternalVariables.IsDebug());
        }

        private void btn_DebugPdf(object sender, RoutedEventArgs ex)
        {
            Console.WriteLine(InternalVariables.GetSubjectDebug());
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_PDF;
            var pdfPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "pdf (*.pdf) | *.pdf",
                Title = "Seleccione el Archivo PDF"
            };
            if (pdfPath.ShowDialog() != true) return;
            Main.Main.AnalizarPdf(pdfPath.FileNames);
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_EXCEL;
            var excelPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "xls (*.xls, *.xlsx) | *.xls;*.xlsx;*.csv",
                Title = "Seleccione el Archivo Excel"
            };
            if (excelPath.ShowDialog() != true) return;
            Main.Main.AnalizarExcel(excelPath.FileNames);
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
        }

        private void btnLeerWord_Click(object sender, RoutedEventArgs e)
        {
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_EXCEL;
            var wordPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "doc (*.doc, *.docx) | *.doc;*.docx ",
                Title = "Seleccione el Archivo Word"
            };
            if (wordPath.ShowDialog() != true) return;
            //foreach (var word in wordPath.FileNames)
            //{
            //    //MainPdf.DebugAnalizarWord(word);
            //}
            //Log.SendMails();
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
        }



        private void DebugWindow_OnClosed(object sender, EventArgs e)
        {
            NotifyIconViewModel.SetCanDebugCommand();
            ViewCommands.SetcanShowDebug();
        }

         private void btn_ExtraerTexto(object sender, RoutedEventArgs ex)
        {
            var pdfPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "pdf (*.pdf) | *.pdf",
                Title = "Seleccione el Archivo PDF"
            };
            if (pdfPath.ShowDialog() != true) return;
             var useUtf8Encoding = false;
             if (checkBoxUtf8Encoding.IsChecked != null) useUtf8Encoding = (bool) checkBoxUtf8Encoding.IsChecked;
            Main.Main.ExtractTextFromPdf(pdfPath.FileNames, useUtf8Encoding: useUtf8Encoding);
            //Main.Main.ExtractTextWithoutContinuousSpaceFromPdf(pdfPath.FileNames, useUtf8Encoding: useUtf8Encoding);
            Console.WriteLine("Extracción Terminada");
        }

        private void btnExtractWord_Click(object sender, RoutedEventArgs e)
        {
            var wordPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "doc (*.doc, *.docx) | *.doc;*.docx ",
                Title = "Seleccione el Archivo Word"
            };
            if (wordPath.ShowDialog() != true) return;
            Main.Main.ExtractTextFromWord(wordPath.FileNames);
        }

        private void btn_ExtraerTextoExcel(object sender, RoutedEventArgs e)
        {
            var excelPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "xls (*.xls, *.xlsx) | *.xls;*.xlsx;*.csv",
                Title = "Seleccione el Archivo Excel"
            };
            if (excelPath.ShowDialog() != true) return;
            Main.Main.ExtractTextFromExcel(excelPath.FileNames, leerHastaColuma: ColumnaExcel.AL);
        }

        private void checkBoxDebug_Click(object sender, RoutedEventArgs e)
        {
            InternalVariables.SetDebugConfigMode(checkBoxDebug.IsChecked);
        }

        private void btnLeerTXT_Click(object sender, RoutedEventArgs e)
        {
            Main.Main.STATE = Main.Main.AppState.ANALIZANDO_ORDENES_TEXTO;
            var txtPath = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "txt (*.txt) | *.txt",
                Title = "Seleccione el Archivo de Texto"
            };
            if (txtPath.ShowDialog() != true) return;
            Main.Main.AnalizarTxt(txtPath.FileNames);
            Main.Main.STATE = Main.Main.AppState.INACTIVO;
        }
    }
}
