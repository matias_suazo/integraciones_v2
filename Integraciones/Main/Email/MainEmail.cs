﻿using System;
using System.Collections.Generic;
using System.Linq;
using Integraciones.Utils;
using Integraciones.Utils.Integracion.Email.ArcosDorados;
using Integraciones.Utils.Integracion.Email.Cinemark;
using Integraciones.Utils.Integracion.Email.ClinicaAlemanaArtikos;
using Integraciones.Utils.Integracion.Email.ConsaludArtikos;
using Integraciones.Utils.Integracion.Email.Santander;
using Integraciones.Utils.Integracion.Email.Sgs;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;
using Integraciones.Utils.Readers.Email;
using Integraciones.ViewModel;
using Limilabs.Mail;
using Log = Integraciones.Utils.Log.Log;
using Integraciones.Utils.Integracion.Email.Unilever;
using Integraciones.Utils.Integracion.Email.Vtr;
using Integraciones.Utils.Integracion.Email.McDonald;
using Integraciones.Utils.Integracion.Email.UniversidadDiegoPortales;
using Integraciones.Utils.FlagClose;
using Integraciones.Utils.Integracion.Email.Afubach;
using Integraciones.Utils.Integracion.Email.ShWilliams;

namespace Integraciones.Main.Email
{
    public static class MainEmail
    {
        public static void ExecuteSingleMail(IMail email)
        {
            
            Console.WriteLine($"=================LECTURA DE CUERPO DEL CORREO==============================");
            var option = GetOptionEmailNumber(email);
            OrdenCompra ordenCompra = null;
            List<OrdenCompra> ordenCompraList = null;
            OrdenCompraIntegracion ocAdapter = null;//new OrdenCompraIntegracion();
            List<OrdenCompraIntegracion> ocAdapterList = null;//new List<OrdenCompraIntegracion>();
            Console.WriteLine($"FIRST: {option}");
            
            switch (option)
            {

                case 0:
                    Console.WriteLine($"{email.Subject}");
                    var cinemark = new Cinemark(email);
                    ordenCompraList = cinemark.GetListOrdenCompra();
                    foreach (var ordenCompra2 in ordenCompraList)
                    {
                        ocAdapter = ordenCompra2.TraspasoUltimateIntegracionNuevoFlujo();
                        ExecutePostProcess(option, email, ordenCompra2, ocAdapter);
                        
                    }
                    return;
                    break;
                case 1:
                    var arcosDorados = new ArcosDorados(email);
                    ordenCompra = arcosDorados.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 2:
                    var consaludArtikos = new ConsaludArtikos(email);
                    ordenCompra = consaludArtikos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 3:
                    var clinicaAlemanaArtikos = new ClinicaAlemanaArtikos(email);
                    ordenCompra = clinicaAlemanaArtikos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    //ocAdapterList.AddRange(ordenCompraList.Select(ord => ord.TraspasoUltimateIntegracion()));
                    break;
                case 4:
                    var santander = new Santander(email);
                    ordenCompra = santander.GetOrdenCompra();
                    Console.WriteLine(ordenCompra.ToString());
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 5:
                    var sgs = new Sgs(email);
                    ordenCompra = sgs.GetOrdenCompra();
                    ordenCompra.Observaciones = $" OC: { ordenCompra.NumeroCompra}";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    if (ordenCompra.Observaciones.Contains("ANTOFAGASTA"))
                        foreach (var a in ocAdapter.DetallesCompra)
                            a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    break;
                case 6:
                    var unilever = new Unilever(email);
                    ordenCompra = unilever.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 7:
                    var vtr = new Vtr(email);
                    ordenCompra = vtr.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                /*case 8:
                    var mcdonald = new McDonald(email);
                    ordenCompra = mcdonald.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;*/
                case 8:
                    var udp = new UniversidadDiegoPortales(email);
                    ordenCompraList = udp.GetOrdenCompra();
                    foreach (var o in ordenCompraList)
                    {
                        ocAdapter = o.TraspasoUltimateIntegracion();
                        ExecutePostProcess(option, email, ordenCompra, ocAdapter);
                    }
                    return;                
                    break;
                case 9:
                    var afubach = new Afubach(email);
                    ordenCompra = afubach.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 10:
                    var shWilliams = new ShWilliams(email);
                    ordenCompra = shWilliams.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;



            }
                     
                ExecutePostProcess(option, email, ordenCompra, ocAdapter);
           
            OracleDataAccess.TraspasaTelemarketingSeparados();
        }
        public static void ExecuteSingleIconstruyeMail(IMail email)
        {
            EmailReader.ProcessIconstruyeMessage(email);
        }

        public static void ExecuteLecturaMail()
        {
            if (!InternalVariables.ProcessMail())
            {
                Console.WriteLine($"===============================================");
                Console.WriteLine($"       NO SE EJECUTARA LA LECTURA MAIL");
                Console.WriteLine($"       ProcessMail: {InternalVariables.ProcessMail()}");
                Console.WriteLine($"===============================================");
                return;
            }
            Console.WriteLine($"===============================================");
            Console.WriteLine($"            INICIO LECTURA MAIL                ");
            Console.WriteLine($"===============================================");
            var emailDictionary = EmailReader.GetAllMailLecturaMailSubject();
            var c = 0;
            foreach (var e in emailDictionary)
            {
                if (InternalVariables.IsDebug())
                {
                    ExecuteSingleMail(e.Value);
                    c++;
                    e.Value.MoveProcesosMailTo(e.Key, InternalVariables.GetEmailFolderProcesadas());
                }
                else
                {
                    try
                    {
                        ExecuteSingleMail(e.Value);
                        c++;
                        e.Value.MoveProcesosMailTo(e.Key, InternalVariables.GetEmailFolderProcesadas());
                    }
                    catch (Exception ex)
                    {
                        e.Value.MoveProcesosMailTo(e.Key, InternalVariables.GetEmailErrorFolderProcesadas());
                    }
                }
            }
            FinishAnalysisProcesosXml(c);
            Main.FinishAnalysis(c);
            FlagClose.email = true;
            NotifyIconViewModel.SetCanProcessOrderMailCommand();
        }



        public static void ExecuteLecturaIconstruyeMail()
        {
            if (!InternalVariables.ProcessMail())
            {
                Console.WriteLine($"===============================================");
                Console.WriteLine($"       NO SE EJECUTARA LA LECTURA MAIL");
                Console.WriteLine($"       ProcessMail: {InternalVariables.ProcessMail()}");
                Console.WriteLine($"===============================================");
                return;
            }
            Console.WriteLine($"===============================================");
            Console.WriteLine($"          INICIO LECTURA MAIL ICONSTRUYE       ");
            Console.WriteLine($"===============================================");
            EmailReader.ListFolderProcesosXml();
            var emailDictionary = EmailReader.GetAllMailFromIconstruyeImap(); //.GetAllMailFromIconstruyePop3();
            var c = 0;
            foreach (var e in emailDictionary)
            {
                try
                {
                    ExecuteSingleIconstruyeMail(e.Value);
                    c++;
                    e.Value.MoveIcontruyeMailTo(e.Key, InternalVariables.GetEmailFolderProcesadas());
                }
                catch (Exception)
                {
                    e.Value.MoveIcontruyeMailTo(e.Key, InternalVariables.GetEmailErrorFolderProcesadas());
                }
            }
            FinishAnalysisIconstruye(c);
            NotifyIconViewModel.SetCanProcessOrderMailIconstruyeCommand();
        }


        #region ExecutePostProcess

        private static void ExecutePostProcess(int option, IMail email, OrdenCompra ordenCompra, OrdenCompraIntegracion ocAdapter)
        {
            var totalResult = true;
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option}");
                if (ordenCompra != null)
                    Console.WriteLine($"{ordenCompra}");
                if (ocAdapter != null)
                    Console.WriteLine($"{ocAdapter}");
            }
            if (option != -1)
            {
                if (ordenCompra != null)
                    if (ocAdapter != null)
                        OracleDataAccess.InsertRawOrdenCompra(option, ordenCompra, ocAdapter.NumPed);
                if (ocAdapter != null)
                {
                    Console.WriteLine("ENTRE AQUI");
                    if (!OracleDataAccess.InsertOrdenCompraIntegracion(ocAdapter))
                    {
                        totalResult = false;
                    }
                    Log.AddMailUpdateTelemarketing(ocAdapter);
                }
            }
        }

        /// <summary>
        /// Optiene el Numero de la Empresa a Procesar
        /// </summary>
        /// <param name="mail">Mail</param>
        /// <returns>Número Opción Empresa</returns>
        private static int GetOptionEmailNumber(IMail mail)
        {
            //var mailSplit = mail.ConvertToString();
            var onlyOneLine = mail.ConvertToString();
            var first = -1;
            foreach (var form in InternalVariables.MailsFormats)
            {
                if (form.Value.Contains(";"))
                {
                    var split = form.Value.Split(';');
                    var match = split.Count(sp => onlyOneLine.Contains(sp));
                    if (match != split.Count()) continue;
                    first = form.Key;
                    break;
                }
                if (form.Value.Contains(":"))
                {
                    var split = form.Value.Split(':');
                    if (split.Any(sp => onlyOneLine.Contains(sp)))
                    {
                        first = form.Key;
                    }
                }
                else if (onlyOneLine.Contains(form.Value))
                {
                    first = form.Key;
                    break;
                }
            }
            if (first == -1)
            {
                try
                {
                    onlyOneLine = onlyOneLine.DeleteNullHexadecimalValues();
                    foreach (var format in InternalVariables.MailsFormats.Where(format => onlyOneLine.Contains(format.Value)))
                    {
                        first = format.Key;
                        break;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    first = -1;
                }
            }
            return first;
        }


        #endregion





        #region ThrowMessages
        private static void FinishAnalysisProcesosXml(int count)
        {
            if (count == 0)
            {
                Console.WriteLine($"===============================================================");
                Console.WriteLine($"               NO HAY CORREOS PARA INTEGRACION                 ");
                Console.WriteLine($"===============================================================");
            }
            else
            {
                Console.WriteLine($"===============================================================");
                Console.WriteLine($"    FINALIZADA LECTURA DE CORREOS  (procesosxml@dimerc.cl)     ");
                Console.WriteLine($"===============================================================");
            }
        }

        private static void FinishAnalysisIconstruye(int count)
        {
            if (count == 0)
            {
                Console.WriteLine($"===============================================================");
                Console.WriteLine($"                NO HAY CORREOS DE ICONSTRUYE                   ");
                Console.WriteLine($"===============================================================");
            }
            else
            {
                Console.WriteLine($"===============================================================");
                Console.WriteLine($" FINALIZADA LECTURA DE CORREOS  (icontruyecotizacion@dimerc.cl ");
                Console.WriteLine($"===============================================================");
            }
        }

        #endregion

    }
}