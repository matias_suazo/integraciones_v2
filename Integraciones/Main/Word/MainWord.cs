﻿using System;
using System.Collections.Generic;
using System.Linq;
using Integraciones.Utils;
using Integraciones.Utils.Error;
using Integraciones.Utils.FileUtils;
using Integraciones.Utils.Integracion.Word.TrafiguraChile;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Log;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Readers.Word;

namespace Integraciones.Main.Word
{
    public static class MainWord
    {

        private static bool OnlyOne;

        public static void DebugAnalizarWord(string path)
        {
            OnlyOne = true;
            var wordReader = new WordReader(path);
            ExecuteSingleWord(wordReader);
        }
        private static void ExecuteSingleWord(WordReader wordReader)
        {
            var option = GetWordOptionNumber(wordReader);
            OrdenCompra ordenCompra = null;
            var ocAdapter = new OrdenCompraIntegracion();
            var ocAdapterList = new List<OrdenCompraIntegracion>();
            Console.WriteLine($"FIRST: {option}");

            #region SWITCH CASE INTEGRACIONES

            switch (option)
            {
                case 0:
                    var trafiguraChile = new TrafiguraChile(wordReader);
                    ordenCompra = trafiguraChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(wordReader: wordReader);
                    //Console.WriteLine(ocAdapter.ToString());
                    break;
            }
            #endregion
            OracleDataAccess.TraspasaTelemarketingSeparados();
        }
        private static void ExecuteWordPostProcess(int option, WordReader wordReader
            , OrdenCompra ordenCompra
            , OrdenCompraIntegracion ocAdapter)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
            }
            if (option != -1)
            {
                if (ordenCompra != null)
                {
                    //OracleDataAccess.InsertRawOrdenCompra(option, ordenCompra, ocAdapter.NumPed);
                }
                if (OracleDataAccess.InsertOrdenCompraIntegracion(ocAdapter, wordReader.WordPath, wordReader: wordReader))
                {
                    Console.WriteLine("ELSE");
                    Log.Save($"Orden N°: {ocAdapter.OcCliente}, " +
                             $"de: {InternalVariables.WordFormats[option]}, " +
                             "procesada exitosamente...");
                    Log.AddMailUpdateTelemarketing(ocAdapter);
                }
                else
                {
                    ThrowError.ThrowInsertError(wordReader.WordFileName);
                }

            }
            else
            {
                //Console.Write("UnknownFile");
                ThrowError.ThrowFormatError(wordReader.WordFileName, wordReader.WordPath);
                //Console.WriteLine("================MOVE 6==================");
                FileUtils.SaveUnknownFile(wordReader.WordPath);
            }
        }

       

        /// <summary>
        /// Optiene el Identificador del Formato de la Orden de Compra a Procesar
        /// </summary>
        /// <param name="wordReader">Word Reader</param>
        /// <returns>Identificador del Formato de la Orde</returns>
        private static int GetWordOptionNumber(WordReader wordReader)
        {
            var onlyOneLine = wordReader.ExtractTextFromWordToString();
            var first = -1;
            foreach (var form in InternalVariables.WordFormats)
            {
                if (form.Value.Contains(";"))
                {
                    var split = form.Value.Split(';');
                    var match = split.Count(sp => onlyOneLine.Contains(sp));
                    if (match == split.Count())
                    {
                        first = form.Key;
                        break;
                    }
                }
                else if (form.Value.Contains(":"))
                {
                    var split = form.Value.Split(':');
                    if (split.Any(sp => onlyOneLine.Contains(sp)))
                    {
                        first = form.Key;
                    }
                }
                else if (onlyOneLine.Contains(form.Value))
                {
                    first = form.Key;
                    break;
                }
            }
            if (first == -1)
            {
                try
                {
                    onlyOneLine = onlyOneLine.DeleteNullHexadecimalValues();
                    foreach (var format in InternalVariables.WordFormats.Where(format => onlyOneLine.Contains(format.Value)))
                    {
                        first = format.Key;
                        break;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    first = -1;
                }
            }
            return first;
        }


       
    }
}