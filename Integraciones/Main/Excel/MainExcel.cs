﻿using System;
using System.IO;
using System.Linq;
using Integraciones.ParsingExtention;
using Integraciones.Utils;
using Integraciones.Utils.Error;
using Integraciones.Utils.FileUtils;
using Integraciones.Utils.Integracion.Excel.CargaEstandar;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Log;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Integracion.Excel.AmgSA;
using Integraciones.Utils.Integracion.Excel.ProAndes;
using Integraciones.Utils.Integracion.Excel.BancoEstado;
using Integraciones.Utils.Integracion.Excel.PYInmobiliaria;
using Integraciones.Utils.Integracion.Excel.Tata;
using Integraciones.Utils.Integracion.Excel.NuevosAlmacenesInternacionales;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.ViewModel;
using Integraciones.Utils.Integracion.Excel.GasAtacama;
using Integraciones.Utils.Integracion.Excel.EandY;
using Integraciones.Utils.Integracion.Excel.SociedadCreditosAutomotrices;
using Integraciones.Utils.Integracion.Excel.BancoChile;
using Integraciones.Utils.Integracion.Excel.Consorcio;
using Integraciones.View;
using Integraciones.Utils.Integracion.Excel.CorpBanca;
using Integraciones.Utils.Integracion.Excel.Ge2;
using Integraciones.Utils.Integracion.Excel.MineraAntucoya;
using Integraciones.Utils.Integracion.Excel.ErnsAndYoung;
using Integraciones.Utils.Integracion.Excel.EzentisExcel;
using Integraciones.Utils.Integracion.Excel.Dollens;
using Integraciones.Utils.Integracion.Excel.Junji;
using Integraciones.Utils.Integracion.Excel.ColegioSaintAndrew;
using Integraciones.Utils.Integracion.Excel.CorporacionDeAsistenciaJudicial;
using Integraciones.Utils.Integracion.Excel.RegistroCivil;
using Integraciones.Utils.Integracion.Excel.DirAtPrim;
using Integraciones.Utils.Integracion.Excel.CorpEduSaludLasCondes;
using Integraciones.Utils.Integracion.Excel.Machicura;
using Integraciones.Utils.Integracion.Excel.BPMFluor;
using Integraciones.Utils.Integracion.Excel.Logistex;
using Integraciones.Utils.FlagClose;
using Integraciones.Utils.Integracion.Excel.Prosepan;
using Integraciones.Utils.Integracion.Excel.Afubach;
using Integraciones.Utils.Integracion.Excel.CelaCosmeticos;
using Integraciones.Utils.Integracion.Excel.RegistroCivilEIdentificacion;
using Integraciones.Utils.Integracion.Excel.CarlosAguirre;
using Integraciones.Utils.Integracion.Excel.CorporacionEducacionalBC;
using Integraciones.Utils.Integracion.Excel.Cepech;
using Integraciones.Utils.Integracion.Excel.CorporacionParaNutricionInfantil;
using Integraciones.Utils.Integracion.Excel.FundacionLasRosas;
using Integraciones.Utils.Integracion.Excel.VoySantiago;
using Integraciones.Utils.Integracion.Excel.Tricot;
using Integraciones.Utils.Integracion.Excel.VoySantiagoFinal;


namespace Integraciones.Main.Excel
{
    public static class MainExcel
    {
        private static bool OnlyOne;

        public static void ExecuteSingleExcel(ExcelReader excelReader)
        {
            var option = GetExcelOptionNumber(excelReader);
            OrdenCompra ordenCompra = null;
            var ocAdapter = new OrdenCompraIntegracion();
            var ocAdapterBCH = new OrdenCompraIntegracionBCH();
            var rep = 1;
            Console.WriteLine($"FIRST: {option}");

            #region SWITCH CASE INTEGRACIONES

            switch (option)
            {
                case -1:
                    FileUtils.MoveErrorFile();
                    return;
                case 0:
                    var cargaEstandar = new CargaEstandar(excelReader);
                    foreach (var orden in cargaEstandar.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        if (ordenCompra.Rut == "69051400") {
                           // while (rep <= int.Parse(ordenCompra.Repeticiones))
                            //{
                               // Console.WriteLine($"===========REPETICION NUMERO {rep}=================");
                                ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                                ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                              //  rep++;
                            //}

                        } else
                        {
                            ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                            ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                        }
                        //  OracleDataAccess.NumpedArchivoAdjunto(Convert.ToInt32(ocAdapter.NumPed), excelReader.ExcelFileName);

                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);

                    return;
                case 1:
                    var amgSA = new AmgSA(excelReader);
                    foreach (var orden in amgSA.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                case 2:
                    var adRetail = new AdRetail(excelReader);
                    foreach (var orden in adRetail.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        foreach (var a in ocAdapter.DetallesCompra)
                        {
                            if (ocAdapter.OcCliente.Contains("ANTOFAGASTA") || ocAdapter.OcCliente.Contains("CALAMA"))
                            {
                                a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                            }
                        }
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                //case 3:
                //    var daniela = new Daniela(excelReader);
                //    foreach (var orden in daniela.GetOrdenCompra())
                //    {
                //        ordenCompra = orden;
                //        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                //        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                //    }
                //    break;
                case 4:
                    var bancoEstado = new BancoEstado(excelReader);
                    foreach (var orden in bancoEstado.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);

                    return;
                case 6:
                    var pyInmobiliaria = new PYInmobiliaria(excelReader);
                    foreach (var orden in pyInmobiliaria.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                case 7:
                    var tata = new Tata(excelReader);
                    foreach (var orden in tata.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                case 8:
                    var integramedica = new Integramedica(excelReader);
                    foreach (var orden in integramedica.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        //Console.WriteLine($"Cencos: {ocAdapter.CenCos}");
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                case 9:
                    var gasAtacama = new GasAtacama(excelReader);
                    foreach (var orden in gasAtacama.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;

                case 10:
                    var nuevosAlmacenesInternacionales = new NuevosAlmacenesInternacionales(excelReader);
                    foreach (var orden in nuevosAlmacenesInternacionales.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                case 11:
                    nuevosAlmacenesInternacionales = new NuevosAlmacenesInternacionales(excelReader);
                    foreach (var orden in nuevosAlmacenesInternacionales.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                case 12:
                    var maui = new Maui(excelReader);
                    foreach (var orden in maui.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;

                case 13:
                    var sca = new SociedadCreditosAutomotrices(excelReader);
                    foreach (var orden in sca.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                case 14:
                    var enami = new Enami(excelReader);
                    foreach (var orden in enami.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                case 15:
                    var servicioCapacitacion = new ServicioCapacitacion(excelReader);
                    ordenCompra = servicioCapacitacion.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 16:
                    var eandy = new EandY(excelReader);
                    ordenCompra = eandy.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 17:
                    var bancoChile = new BancoChile(excelReader);
                    foreach (var orden in bancoChile.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        // ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ocAdapterBCH = orden.TraspasoUltimateIntegracionNuevoFlujoBancoChile(excelReader: excelReader);
                        //ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                        ExecuteExcelPostProcessBCH(option, excelReader, ordenCompra, ocAdapterBCH, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                case 18:
                    var corpBanca = new CorpBanca(excelReader);
                    foreach (var orden in corpBanca.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;
                case 19:
                    var telefonica = new Telefonica(excelReader);
                    ordenCompra = telefonica.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 20:
                    var cruzblanca = new CruzBlanca(excelReader);
                    foreach (var orden in cruzblanca.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    return;

                case 300:
                    var proAndes = new Proandes(excelReader);
                    ordenCompra = proAndes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);

                    break;
                case 301:
                    var consorcio = new Consorcio(excelReader);
                    var list = consorcio.GetOrdenCompra();
                    foreach (var ordenList in list) {
                        foreach (var orden in ordenList.GetAllOrdenCompraConsorcio()) {
                            Console.WriteLine(orden);
                            ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                            if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                            {
                                ocAdapter.CenCos = "0";
                            }
                            ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                        }
                    }
                    FileUtils.MoveFileToProcessFolder(excelReader.ExcelPath, list[0], ocAdapter, cargaEstandar: true);
                    return;
                //break;
                case 302:
                    var mineraAntucoya = new MineraAntucoya(excelReader);
                    ordenCompra = mineraAntucoya.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 303:
                    /*var ge2 = new Ge2(excelReader);
                    var list1 = ge2.GetOrdenCompra();*/
                    var ge2 = new Ge2fin(excelReader);
                    var list1 = ge2.GetOrdenCompra();

                    //foreach (var ordenList in list1)
                    //{
                    foreach (var orden in list1)
                    {
                        orden.Items = orden.Items.Distinct().ToList();
                        Console.WriteLine(orden);
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    //}
                    /*foreach (var ordenList in list1) {
                        foreach (var orden in ordenList.GetAllOrdenCompra()) {
                            Console.WriteLine(orden);
                            ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                            ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                        }
                    }*/

                    //  FileUtils.MoveFileToProcessFolder(excelReader.ExcelPath, list1[0], ocAdapter, cargaEstandar: true);
                    return;

                    break;

                case 304:
                    var ernsAndYoung = new ErnsAndYoung(excelReader);
                    ordenCompra = ernsAndYoung.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 305:
                    var ezentisExcel = new EzentisExcel(excelReader);
                    ordenCompra = ezentisExcel.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;

                case 306:
                    var dollens = new Dollens(excelReader);
                    ordenCompra = dollens.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 307:
                    var tata2 = new Tata(excelReader);
                    foreach (var orden in tata2.GetOrdenCompra())
                    {
                        ordenCompra = orden;

                        Console.WriteLine($"===========REPETICION NUMERO {rep}=================");
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);

                    }

                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);
                    break;
                case 308:
                    var junji = new Junji(excelReader);
                    var listjunji = junji.GetOrdenCompra();
                    var cont = 0;
                    foreach (var oc in listjunji)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    return;
                case 309:
                    var sca2 = new SociedadCreditosAutomotrices2(excelReader);
                    foreach (var orden in sca2.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 310:
                    var csa = new ColegioSaintAndrew(excelReader);
                    ordenCompra = csa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);

                    break;
                case 311:
                    var cdaj = new CorporacionDeAsistenciaJudicial(excelReader);
                    var listcdaj = cdaj.GetOrdenCompra();
                    var contcdaj = 0;
                    foreach (var oc in listcdaj)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    return;
                    break;
                case 312:
                    var registroCivil = new RegistroCivilEIdentificacion(excelReader);
                    var listRegistroCivil = registroCivil.GetOrdenCompra();
                    var contregcivil = 0;
                    foreach (var oc in listRegistroCivil)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    return;
                case 313:
                    var diratprim = new DirAtPrim(excelReader);
                    var listdiratprim = diratprim.GetOrdenCompra();

                    foreach (var oc in listdiratprim)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    return;
                case 314:
                    var cesc = new CorpEduSaludLasCondes(excelReader);
                    ordenCompra = cesc.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);

                    break;
                case 315:
                    var machicura = new Machicura(excelReader);
                    ordenCompra = machicura.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);

                    break;
                    return;
                case 316:
                    var bpmfluor = new BPMFLuor(excelReader);
                    ordenCompra = bpmfluor.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);

                    break;
                case 317:
                    var logistex = new Logistex(excelReader);
                    ordenCompra = logistex.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                    break;
                case 318:
                    var prosepan = new Prosepan(excelReader);
                    var listaProsepan = prosepan.GetOrdenCompra();
                    //var contProsepan = 0;
                    foreach (var oc in listaProsepan)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    return;
                case 319:
                    Console.WriteLine(excelReader.Hojas.Count);
                    var afubach = new Afubach2(excelReader);
                    var listaAfubach = afubach.GetOrdenCompra();
                    //var contProsepan = 0;
                    var contadorOrdenes = 1;
                    bool moverarchivo = false;
                    foreach (var oc in listaAfubach)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        if (contadorOrdenes==listaAfubach.Count)
                        {
                            moverarchivo = true;
                        }
                            ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: moverarchivo);
                        // }
                        //cont++;
                        contadorOrdenes++;
                    }
                    return;
                case 320:
                    //var celaCosmeticos = new CelaCosmeticos(excelReader);
                    //var listaCelaCosmeticos = celaCosmeticos.GetOrdenCompra();
                    var celaCosmeticos = new CelaCosmeticosV2(excelReader);
                    var listaCelaCosmeticos = celaCosmeticos.GetOrdenCompra();
                    foreach (var orden in listaCelaCosmeticos)
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;

                    return;
                case 321:
                    var junji2 = new Junji2(excelReader);
                    var listjunji2 = junji2.GetOrdenCompra();
                    var contjunji2 = 0;
                    foreach (var oc in listjunji2)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    return;
                    break;
                case 322:
                    var registrocivil = new RegistroCivilEIdentificacion(excelReader);
                    var listregistrocivil = registrocivil.GetOrdenCompra();
                    var countrc = 0;
                    foreach (var oc in listregistrocivil)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        // if (cont < listjunji.Count - 1)
                        //{
                        ExecuteExcelPostProcess(option, excelReader, oc, ocAdapter, moveFile: false);
                        // }
                        //cont++;
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;

                case 323:
                    var carlosAguirre = new CarlosAguirre(excelReader);
                    foreach (var orden in carlosAguirre.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 324:
                    var bc = new BC(excelReader);
                    foreach (var orden in bc.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 325:
                    var cepech = new Cepech(excelReader);
                    foreach (var orden in cepech.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 326:
                    var cargaEstandarV2prv = new CargaEstandarV2prv(excelReader);
                    foreach (var orden in cargaEstandarV2prv.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        if (ordenCompra.Rut == "69051400")
                        {
                            // while (rep <= int.Parse(ordenCompra.Repeticiones))
                            //{
                            // Console.WriteLine($"===========REPETICION NUMERO {rep}=================");
                            ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                            ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                            //  rep++;
                            //}

                        }
                        else
                        {
                            ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                            ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                        }
                        //  OracleDataAccess.NumpedArchivoAdjunto(Convert.ToInt32(ocAdapter.NumPed), excelReader.ExcelFileName);

                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter, cargaEstandar: true);

                    return;
                    break;
                case 327:
                    var corpParaNutri = new CorpParaNutricion(excelReader);
                    foreach (var orden in corpParaNutri.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 328:
                    var fundLasRosas = new FundacionLasRosas(excelReader);
                    foreach (var orden in fundLasRosas.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 329:
                    var voyStgo = new VoyStgo(excelReader);
                    foreach (var orden in voyStgo.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 330:
                    var tricot = new Tricot(excelReader);
                    foreach (var orden in tricot.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
                case 331:
                    var voyStgoFinal = new VoySantiagoFinal(excelReader);
                    foreach (var orden in voyStgoFinal.GetOrdenCompra())
                    {
                        ordenCompra = orden;
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(excelReader: excelReader);
                        ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter, moveFile: false);
                    }
                    FileUtils.MoveFileToProcessFolder(
                        excelReader.ExcelPath
                        , ordenCompra
                        , ocAdapter);
                    return;
                    break;
            }
            #endregion
            /*  if (option == -1)
              {
                  OracleDataAccess.NumpedArchivoAdjunto(0, excelReader.ExcelFileName);
              }*/

            ExecuteExcelPostProcess(option, excelReader, ordenCompra, ocAdapter);
            //OracleDataAccess.NumpedArchivoAdjunto(Convert.ToInt32(ocAdapter.NumPed), excelReader.ExcelFileName);

        }


        private static void ExecuteExcelPostProcessBCH(int option, ExcelReader excelReader, OrdenCompra ordenCompra,
            OrdenCompraIntegracionBCH ocAdapterBCH, bool moveFile = true)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if (ocAdapterBCH != null && ocAdapterBCH.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapterBCH);
            }
            if (option != -1)

                if (OracleDataAccess.InsertOrdenCompraIntegracionBCH(ocAdapterBCH, excelReader.ExcelPath, excelReader: excelReader))
                {
                    if (moveFile)
                        FileUtils.MoveFileToProcessFolder(
                            excelReader.ExcelPath
                            , ordenCompra
                            , ocAdapterBCH);
                    Log.Save($"Orden N°: {ocAdapterBCH.OcCliente}, " +
                                 $"de: {InternalVariables.ExcelFormat[option]}, " +
                                 "procesada exitosamente...");
                    Log.AddMailUpdateTelemarketingBCH(ocAdapterBCH);
                }
                else
                {
                    ThrowError.ThrowInsertError(excelReader.ExcelFileName);
                }
            //OracleDataAccess.TraspasaTelemarketingSeparados();
        }
        private static void ExecuteExcelPostProcess(int option, ExcelReader excelReader, OrdenCompra ordenCompra,
            OrdenCompraIntegracion ocAdapter, bool moveFile = true)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if (ocAdapter != null && ocAdapter.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapter);
            }
            if (option != -1)

                if (OracleDataAccess.InsertOrdenCompraIntegracion(ocAdapter, excelReader.ExcelPath, excelReader: excelReader))
                {
                    if (moveFile)
                        FileUtils.MoveFileToProcessFolder(
                            excelReader.ExcelPath
                            , ordenCompra
                            , ocAdapter);
                    Log.Save($"Orden N°: {ocAdapter.OcCliente}, " +
                                 $"de: {InternalVariables.ExcelFormat[option]}, " +
                                 "procesada exitosamente...");
                    if (ocAdapter.RutCli.Equals(77398220))
                    {
                        System.Diagnostics.Process TPI = new System.Diagnostics.Process();
                        TPI.StartInfo.FileName = @"C:\Sistemas\traspasaPedidosIntegracion\TraspasaIntegracion.exe";
                        TPI.Start();
                        TPI.WaitForExit();
                        OracleDataAccess.GetAPINotaAutomatica(ocAdapter.NumPed, ocAdapter.OcCliente);
                        //Console.WriteLine("Orden" + ocAdapter.NumPed);

                    }
                    //OracleDataAccess.GetReporteNotasAutomaticas(ocAdapter.NumPed);

                    Log.AddMailUpdateTelemarketing(ocAdapter);
                }
                else
                {
                    ThrowError.ThrowInsertError(excelReader.ExcelFileName);
                    FileUtils.SaveUnknownFile(excelReader.ExcelPath);
                }
           // OracleDataAccess.TraspasaTelemarketingSeparados();
        }

        public static void ExecuteLecturaExcel()
        {
            if (Main.STATE != Main.AppState.INACTIVO)
            {
                ParseExt.Log.Debug("Can Execute EXCEL Module, because the MAIN_STATE is ACTIVE...");
                return;
            }
            OnlyOne = false;
          //  if (!OracleDataAccess.TestConexion()) return;
            if (InternalVariables.CountCriticalError >= 3)
            {
                if (InternalVariables.CountSendErrorAlert >= 5) return;
                InternalVariables.CountSendErrorAlert++;
                ThrowError.SendAlertError();
                return;
            }
            FileUtils.MoveUnknownFile();
            FileUtils.MoveErrorFile();
            Main.InitializerAnalysisExcel();
            var count = 0;

            /*
             *  Excel de Carpeta 'Procesar'
             */
            foreach (var excelReader in Directory
                .GetFiles(@InternalVariables.GetOcAProcesarFolder(), "*.xlsx").
                Select(excelPath => new ExcelReader(excelPath, leerHastaColuma: ColumnaExcel.HK)))
            {
                try
                {
                    ExecuteSingleExcel(excelReader);
                    count++;
                }
                catch (ErrorIntegracion integracionError)
                {
                    Log.TryError(integracionError.Mensaje);
                    Log.SendMailErrorEjecutivos(integracionError);
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================");
                    Console.WriteLine("\t\tPROCESAR NORMAL");
                    Console.WriteLine(e.ToString());
                    Console.WriteLine("=====================================");
                    ThrowError.ThrowAnalysisErrorExcel(excelReader, e);
                    FileUtils.SaveErrorFile(excelReader.ExcelPath);
                }
            }
            foreach (var excelReader in Directory
              .GetFiles(@InternalVariables.GetOcAProcesarFolder(), "*.xls").
              Select(excelPath => new ExcelReader(excelPath, leerHastaColuma: ColumnaExcel.HK)))
            {
                try
                {
                    ExecuteSingleExcel(excelReader);
                    count++;
                }
                catch (ErrorIntegracion integracionError)
                {
                    Log.TryError(integracionError.Mensaje);
                    Log.SendMailErrorEjecutivos(integracionError);
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================");
                    Console.WriteLine("\t\tPROCESAR NORMAL");
                    Console.WriteLine(e.ToString());
                    Console.WriteLine("=====================================");
                    ThrowError.ThrowAnalysisErrorExcel(excelReader, e);
                    FileUtils.SaveErrorFile(excelReader.ExcelPath);
                }
            }
            Main.FinishAnalysis(count);
            FlagClose.excel = true;
            NotifyIconViewModel.SetCanProcessOrderExcelCommand();
        }


        private static int GetExcelOptionNumber(ExcelReader excelReader)
        {
            var onlyOneLine = excelReader.ToString();
            var first = -1;
            foreach (var form in InternalVariables.ExcelFormat)
            {
                if (form.Value.Contains(";"))
                {
                    var split = form.Value.Split(';');
                    if (onlyOneLine.Contains(split[0]) &&
                        onlyOneLine.Contains(split[1]))
                    {
                        first = form.Key;
                        break;
                    }
                }
                else if (form.Value.Contains("||"))
                {
                    var split = form.Value.Split(new string[] { "||" }, StringSplitOptions.None); 
                    if (split.Any(op => onlyOneLine.Contains(op)))
                    {
                        first = form.Key;
                    }
                }
                if (onlyOneLine.Contains(form.Value))
                {
                    first = form.Key;
                    break;
                }
            }
            if (first == -1)
            {
                try
                {
                    onlyOneLine = onlyOneLine.DeleteNullHexadecimalValues();
                    foreach (var format in InternalVariables.PdfFormats.Where(format => onlyOneLine.Contains(format.Value)))
                    {
                        first = format.Key;
                        break;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    first = -1;
                }
            }
            return first;
        }

        //public static void ExecuteLecturaExcel()
        //{
        //    throw new NotImplementedException();
        //}

        public static void DebugAnalizar(string excel)
        {
            var excelReader = new ExcelReader(excel, leerHastaColuma: ColumnaExcel.HZ);
            ExecuteSingleExcel(excelReader);
        }
    }
}
    