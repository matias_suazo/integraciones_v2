using Hardcodet.Wpf.TaskbarNotification;
using Integraciones.Utils.Log;
using Integraciones.Utils.Oracle.DataAccess;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Integraciones.ParsingExtention.View;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Readers.Word;
using Integraciones.View;
using Integraciones.ViewModel;
using Integraciones.Utils.FlagClose;

namespace Integraciones.Main
{
    public static class Main
    {
        private static void UpdateTelemarketing()
        {
            Log.SendMails();
        }

        public enum AppState
        {
            INACTIVO,
            ANALIZANDO_ORDENES_PDF,
            ANALIZANDO_ORDENES_EXCEL,
            ANALIZANDO_ORDENES_MAIL,
            ANALIZANDO_ORDENES_TEXTO
        }

        public static AppState STATE = AppState.INACTIVO;

        public static Window ViewInstance => ParsingExtention.View.App.CurrentWindow;

        public static void FinishAnalysis(int count)
        {
            if (FlagClose.ListoParaCerrar())
            {
                System.Windows.Forms.Application.Exit();
                Environment.Exit(1);
            }
            if (count == 0)
            {
                
                ShowBalloon("Información",
                    "No existen Ordenes para Procesar...", BalloonIcon.Info);

                /* eliminar si no funciona */ 
                /*System.Windows.Forms.Application.Exit();
                Environment.Exit(1);*/
                /* eliminar si no funciona */

            }
            else
            {
                UpdateTelemarketing();
                Log.Save($"Análisis Terminado. Total de Ordenes Procesadas: {count}");
                ShowBalloon("Información", "Análisis Terminado", BalloonIcon.Info);
                OracleDataAccess.CloseConexion();
                

            }
            NotifyIconViewModel.SetCanProcessOrderCommand();
        
        }

        public static void InitializerAnalysis()
        {
            Log.Save("Inicializando Análisis de Ordenes de Compra");
            ShowBalloon("Información",
                "Inicializando Análisis de Ordenes de Compra", BalloonIcon.Info);
        }

        public static void InitializerAnalysisExcel()
        {
            Log.Save("Inicializando Análisis de Ordenes de Compra Excel");
            ShowBalloon("Información",
                "Inicializando Análisis de Ordenes de Compra Excel", BalloonIcon.Info);
        }

        /// <summary>
        /// Extraer Texto desde un archivo Pdf y guardarlo como fichero Plano
        /// </summary>
        /// <param name="fileNames">Archivos Pdf</param>
        /// <param name="useUtf8Encoding">Usar Codificación UTF8</param>
        public static void ExtractTextFromPdf(IEnumerable<string> fileNames, bool useUtf8Encoding = false)
        {
            try
            {
                foreach (var pdfReader in fileNames.Select(pdf => new PDFReader(pdf)))
                {
                    pdfReader.SaveTextToPlainFile(useUtf8Encoding: useUtf8Encoding);
                }
            }
            catch (Exception e)
            {
                Log.TryError(e.Message);
                Log.TryError(e.ToString());
            }
        }

        //ExtractTextWithoutContiniousSpaceFromPdf

        public static void ExtractTextWithoutContinuousSpaceFromPdf(IEnumerable<string> fileNames, bool useUtf8Encoding = false)
        {
            try
            {
                foreach (var pdfReader in fileNames.Select(pdf => new PDFReader(pdf)))
                {
                    pdfReader.SaveTextToPlainFileWithoutContinousSpace(useUtf8Encoding: useUtf8Encoding);
                }
            }
            catch (Exception e)
            {
                Log.TryError(e.Message);
                Log.TryError(e.ToString());
            }
        }

        /// <summary>
        /// Extraer Texto desde un archivo Word y guardarlo como fichero Plano
        /// </summary>
        /// <param name="fileNames">Archivos Word</param>
        public static void ExtractTextFromWord(string[] fileNames)
        {
            try
            {
                foreach (var wordReader in fileNames.Select(word => new WordReader(word)))
                {
                    wordReader.SaveTextToPlainFile();
                }
                Console.WriteLine("Extracción Terminada");
            }
            catch (Exception e)
            {
                Log.TryError(e.Message);
                Log.TryError(e.ToString());
            }
        }


        /// <summary>
        /// Extraer Texto desde un archivo Excel y guardarlo como fichero Plano
        /// </summary>
        /// <param name="fileNames">Archivos Excel</param>
        public static void ExtractTextFromExcel(string[] fileNames, bool soloPrimeraHoja = true, ColumnaExcel leerHastaColuma = ColumnaExcel.FLAG)
        {
            try
            {
                foreach (var excelReader in fileNames.Select(excel => new ExcelReader(excel
                    , soloPrimeraHoja: soloPrimeraHoja, leerHastaColuma: leerHastaColuma)))
                {
                    excelReader.SaveTextToPlainFile();
                }
                Console.WriteLine("Extracción Terminada");
            }
            catch (Exception e)
            {
                Log.TryError(e.Message);
                Log.TryError(e.ToString());
            }
        }

        /// <summary>
        /// Analizar PDF's
        /// </summary>
        /// <param name="fileNames">Archivos Pdf</param>
        public static void AnalizarPdf(string[] fileNames)
        {
            foreach (var pdfP in fileNames)
            {
                Pdf.MainPdf.DebugAnalizar(pdfP);
            }
            Log.SendMails();
        }

        /// <summary>
        /// Analizar PDF's
        /// </summary>
        /// <param name="fileNames">Archivos Pdf</param>
        public static void AnalizarTxt(string[] fileNames)
        {
            foreach (var txtF in fileNames)
            {
                Text.MainText.DebugAnalizar(txtF);
            }
            Log.SendMails();
        }

        public static void ExecuteLecturaPdf()
        {
            Pdf.MainPdf.ExecuteLecturaPdf();
        }
        public static void ExecuteLecturaExcel()
        {
            Excel.MainExcel.ExecuteLecturaExcel();
        }

        public static void ExecuteLecturaMail()
        {
            Email.MainEmail.ExecuteLecturaMail();
        }
        public static void ExecuteLecturaMailIconstruye()
        {
            Email.MainEmail.ExecuteLecturaIconstruyeMail();
        }        
        //public static void ExecuteLecturaMailAdjunto()
        //{
        //    Email.MainEmail.ExecuteLecturaMailAdjunto();
        //}
        public static void AnalizarExcel(string[] fileNames)
        {

            KillAllProcessWithName("EXCEL");
            foreach (var excel in fileNames)
            {
                Excel.MainExcel.DebugAnalizar(excel);
            }
            Log.SendMails();
        }

        public static void KillAllProcessWithName(string processName)
        {
            if (!InternalVariables.KillExcelProcess()) return;
            var process = from pro in Process.GetProcesses() where pro.ProcessName.Equals("EXCEL")
                          select pro;
            foreach(var p in process)
                p.Kill();
        }

        public static void ShowBalloon(string title, string desc, BalloonIcon icon)
        {
            if (ViewInstance.GetType() == typeof (MainTray))
            {
                ((MainTray)ViewInstance).ShowBalloon(title,desc,icon);
            }else if (ViewInstance.GetType() == typeof(IntegracionesView))
            {
                ((IntegracionesView)ViewInstance).ShowBalloon(title, desc, icon);
            }
        }


    }
}