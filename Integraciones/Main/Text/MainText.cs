﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Integraciones.ParsingExtention;
using Integraciones.Utils;
using Integraciones.Utils.Error;
using Integraciones.Utils.FileUtils;
using Integraciones.Utils.Integracion.Pdf.ClinicaDavila;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Log;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;
using Integraciones.Utils.Readers.Text;
using TextReader = Integraciones.Utils.Readers.Text.TextReader;
using Integraciones.Utils.Integracion;
using Integraciones.Utils.Integracion.Text.FoodSolution;
using Integraciones.Utils.Integracion.Text.DragPharma;
using Integraciones.Utils.Integracion.Text.Dimeiggs;
using Integraciones.Utils.Integracion.Text.FluorChile;
using Integraciones.Utils.FlagClose;
using Integraciones.Utils.Integracion.Text;

namespace Integraciones.Main.Text
{
    public static class MainText
    {
        #region Variables

        private static bool OnlyOne;

        #endregion

        private static void ExecuteSingleTextFile(TextReader txtReader)
        {
            var option = GetPdfOptionNumber(txtReader);
            OrdenCompra ordenCompra = null;
            var ocAdapter = new OrdenCompraIntegracion();
            var ocAdapterList = new List<OrdenCompraIntegracion>();
            Console.WriteLine($"FIRST: {option}");

            #region SWITCH CASE INTEGRACIONES

            switch (option)
            {
                case 1:
                    var foodSolution = new FoodSolution(txtReader);
                    var separable = foodSolution.GetOrdenCompra();
                    var list = separable.GetAllOrdenCompra();
                    foreach (var orden in list)
                    {
                        Console.WriteLine(orden);
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(textReader: txtReader);
                        ExecutePdfPostProcess(option, txtReader, orden, ocAdapter);
                    }
                    break;
                case 2:
                    var dragPharma = new DragPharma(txtReader);
                    var oc = dragPharma.GetOrdenCompra();
                    //var list = separable.GetAllOrdenCompra();

                    Console.WriteLine(oc);
                    ocAdapter = oc.TraspasoUltimateIntegracionNuevoFlujo(textReader: txtReader);
                    ordenCompra = oc;
                    //ExecutePdfPostProcess(option, txtReader, oc, ocAdapter);

                    break;

                case 3:
                    var dimeiggs = new Dimeiggs(txtReader);
                    var ocdmg = dimeiggs.GetOrdenCompra();
                    //var list = separable.GetAllOrdenCompra();

                    Console.WriteLine(ocdmg);
                    ocAdapter = ocdmg.TraspasoUltimateIntegracionNuevoFlujo(textReader: txtReader);
                    ExecutePdfPostProcess(option, txtReader, ocdmg, ocAdapter);

                    break;
                case 4:
                    var fluor = new FluorChile(txtReader);
                    var ocfluor = fluor.GetOrdenCompra();
                    //var list = separable.GetAllOrdenCompra();

                    Console.WriteLine(ocfluor);
                    ocAdapter = ocfluor.TraspasoUltimateIntegracionNuevoFlujo(textReader: txtReader);
                    //ExecutePdfPostProcess(option, txtReader, oc, ocAdapter);

                    break;
                case 5:
                    var b2bfalabella = new B2bFalabella(txtReader);
                    var listab2b = b2bfalabella.GetOrdenCompra();
                    foreach (var orden in listab2b)
                    {
                        Console.WriteLine(orden);
                        ocAdapter = orden.TraspasoUltimateIntegracionNuevoFlujo(textReader: txtReader);
                        ocAdapterList.Add(ocAdapter);
                        
                    }
                    ExecutePdfPostProcessB2b(option, txtReader, listab2b[0], ocAdapterList[0], ocAdapterList);
                    break;


            }

            #endregion
            //Console.WriteLine($"PostProcess:{option} \nOC:\n{ordenCompra.ToString()}\nOC_ADAPTER:{ocAdapter}");
            ExecutePdfPostProcess(option, txtReader, ordenCompra, ocAdapter);
            OracleDataAccess.TraspasaTelemarketingSeparados();
        }

        #region ExecutePdfPostProcess

        private static void ExecutePdfPostProcess(int option, TextReader txtReader, OrdenCompra ordenCompra,
            OrdenCompraIntegracion ocAdapter)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if (ocAdapter != null && ocAdapter.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapter);
            }
            if (option != -1)
            {
                if (!OracleDataAccess.InsertOrdenCompraIntegracion(ocAdapter, txtReader.TxtPath, txtReader: txtReader))
                {
                    ThrowError.ThrowInsertError(txtReader.TxtFileName);
                }
                else
                {
                    if (ordenCompra != null)
                    {
                        Log.Save($"Orden N°: {ordenCompra.NumeroCompra}, " +
                                 $"de: {InternalVariables.TxtFormats[option]}, " +
                                 "procesada exitosamente...");
                        Log.AddMailUpdateTelemarketing(ocAdapter);
                        var txtOracle = txtReader.ExtractTextToStringToOracle();
                        var txtName = FileUtils.MoveFileToProcessFolder(txtReader.TxtPath, ordenCompra, ocAdapter,
                            OnlyOne);

                        var md5 = FileUtils.GetMD5FromFile(txtReader.TxtPath);
                        OracleDataAccess.InsertRawPdfText(option, ocAdapter, txtName, txtOracle, ordenCompra, true, md5);
                    }
                }
            }
        }

        private static void ExecutePdfPostProcessB2b(int option, TextReader txtReader, OrdenCompra ordenCompra,
            OrdenCompraIntegracion ocAdapter, List<OrdenCompraIntegracion> ocAdapterList)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if (ocAdapter != null && ocAdapter.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapter);
                if (ocAdapterList != null)
                {
                    foreach (var ocAdap in ocAdapterList)
                        Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdap);
                }
            }

            var totalResult = true;
            foreach (var ocIntegracion in ocAdapterList)
            {
                //ocIntegracion.CenCos = "9999";
                if (!OracleDataAccess.InsertOrdenCompraIntegracion(ocIntegracion, txtReader.TxtPath, txtReader: txtReader))
                {
                    totalResult = false;
                }
            }

            Log.Save($"Orden N°: {ordenCompra.NumeroCompra}, " +
                     $"de: {InternalVariables.TxtFormats[option]}, " +
                     "procesada exitosamente...");
            Log.AddMailUpdateTelemarketing(ocAdapter);
            var txtOracle = txtReader.ExtractTextToStringToOracle();
            var txtName = FileUtils.MoveFileToProcessFolder(txtReader.TxtPath, ordenCompra, ocAdapter,
                OnlyOne);

            var md5 = FileUtils.GetMD5FromFile(txtReader.TxtPath);
            OracleDataAccess.InsertRawPdfText(option, ocAdapter, txtName, txtOracle, ordenCompra, true, md5);

        }


        #endregion


        #region ReaderTXTOrderFromRootDirectory

        public static void ExecuteLecturaText()
        {
            if (Main.STATE != Main.AppState.INACTIVO)
            {
                ParseExt.Log.Debug("Can Execute Txt Module, because the MAIN_STATE is ACTIVE...");
                return;
            }
            Main.STATE = Main.AppState.ANALIZANDO_ORDENES_TEXTO;
            OnlyOne = false;
            if (!OracleDataAccess.TestConexion()) return;
            if (InternalVariables.CountCriticalError >= 3)
            {
                if (InternalVariables.CountSendErrorAlert >= 5) return;
                InternalVariables.CountSendErrorAlert++;
                ThrowError.SendAlertError();
                return;
            }
            FileUtils.MoveUnknownFile();
            FileUtils.MoveErrorFile();
            Main.InitializerAnalysis();
            var count = 0;
           
            /*
             *  Text File de Carpeta 'Procesar'
             */
            foreach (var textReader in Directory
                .GetFiles(@InternalVariables.GetOcAProcesarFolder(), "*.txt").
                Select(txtPath => new TextReader(txtPath)))
            {
                try
                {
                    ExecuteSingleTextFile(textReader);
                    count++;
                }
                catch (ErrorIntegracion integracionError)
                {
                    Log.TryError(integracionError.Mensaje);
                    Log.SendMailErrorEjecutivos(integracionError);
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================");
                    Console.WriteLine("\t\tPROCESAR NORMAL");
                    Console.WriteLine(e.ToString());
                    Console.WriteLine("=====================================");
                    ThrowError.ThrowAnalysisTextError(textReader, e);
                    FileUtils.SaveErrorFile(textReader.TxtPath);
                }
            }
            Main.FinishAnalysis(count);
            FlagClose.txt = true;
        }


        #endregion

        #region GetOption TXT
        /// <summary>
        /// Optiene el Identificador del Formato de la Orden de Compra a Procesar
        /// </summary>
        /// <param name="txtReader">Text Reader</param>
        /// <returns>Identificador del Formato de la Orden</returns>
        private static int GetPdfOptionNumber(TextReader txtReader)
        {
            var onlyOneLine = txtReader.ToString();
            var first = -1;
            foreach (var form in InternalVariables.TxtFormats)
            {
                if (form.Value.Contains(";"))
                {
                    var split = form.Value.Split(';');
                    var match = split.Count(sp => onlyOneLine.Contains(sp));
                    if (match == split.Count())
                    {
                        first = form.Key;
                        break;
                    }
                    //if (onlyOneLine.Contains(split[0]) &&
                    //    onlyOneLine.Contains(split[1]))
                    //{
                    //    first = form.Key;
                    //    break;
                    //}
                }else if (form.Value.Contains(":"))
                {
                    var split = form.Value.Split(':');
                    if (split.Any(sp => onlyOneLine.Contains(sp)))
                    {
                        first = form.Key;
                    }
                }else if (onlyOneLine.Contains(form.Value))
                {
                    first = form.Key;
                    break;
                }
            }
            //foreach (var format in PdfFormats.Where(format => onlyOneLine.Contains(format.Value)))
            //{
            //    first = format.Key;
            //    break;
            //}
            if (first == -1)
            {
                try
                {
                    onlyOneLine = onlyOneLine.DeleteNullHexadecimalValues();
                    foreach (var format in InternalVariables.TxtFormats.Where(format => onlyOneLine.Contains(format.Value)))
                    {
                        first = format.Key;
                        break;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    first = -1;
                }
            }
            return first;
        }

        #endregion


        #region Debug

        public static void DebugAnalizar(string txtPath)
        {
            OnlyOne = true;
            var txtReader = new TextReader(txtPath);
            ExecuteSingleTextFile(txtReader);
        }

        #endregion



    }
}