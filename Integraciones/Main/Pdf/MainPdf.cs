﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Integraciones.ParsingExtention;
using Integraciones.Utils;
using Integraciones.Utils.Error;
using Integraciones.Utils.FileUtils;
using Integraciones.Utils.Integracion;
using Integraciones.Utils.Integracion.Pdf.ACH;
using Integraciones.Utils.Integracion.Pdf.Agrosuper;
using Integraciones.Utils.Integracion.Pdf.AguasAndinas;
using Integraciones.Utils.Integracion.Pdf.AIEP;
using Integraciones.Utils.Integracion.Pdf.AlimentosSanMartin;
using Integraciones.Utils.Integracion.Pdf.Applus;
using Integraciones.Utils.Integracion.Pdf.Arauco;
using Integraciones.Utils.Integracion.Pdf.AridosSantaFeSA;
using Integraciones.Utils.Integracion.Pdf.Armas;
using Integraciones.Utils.Integracion.Pdf.AscensoresSchindler;
using Integraciones.Utils.Integracion.Pdf.AuraIngenieria;
using Integraciones.Utils.Integracion.Pdf.AvalChile;
using Integraciones.Utils.Integracion.Pdf.Axis;
using Integraciones.Utils.Integracion.Pdf.BHPBilliton;
using Integraciones.Utils.Integracion.Pdf.BiomedicalDistributionChile;
using Integraciones.Utils.Integracion.Pdf.Bionet;
using Integraciones.Utils.Integracion.Pdf.BrotecIcafal;
using Integraciones.Utils.Integracion.Pdf.BusesVule;
using Integraciones.Utils.Integracion.Pdf.Candem;
using Integraciones.Utils.Integracion.Pdf.CapacitacionCienciasTecnologiasLimitada;
using Integraciones.Utils.Integracion.Pdf.Carozzi;
using Integraciones.Utils.Integracion.Pdf.CementerioMetropolitanoLTDA;
using Integraciones.Utils.Integracion.Pdf.CementosTransex;
using Integraciones.Utils.Integracion.Pdf.Cencosud;
using Integraciones.Utils.Integracion.Pdf.CerveceriaCCU;
using Integraciones.Utils.Integracion.Pdf.CFTSanAgustin;
using Integraciones.Utils.Integracion.Pdf.CirculoEjecutivaLimitada;
using Integraciones.Utils.Integracion.Pdf.ClinicaAlemana;
using Integraciones.Utils.Integracion.Pdf.ClinicaCordillera;
using Integraciones.Utils.Integracion.Pdf.ClinicaDavila;
using Integraciones.Utils.Integracion.Pdf.ClinicaLilas;
using Integraciones.Utils.Integracion.Pdf.ClinicaSanBernardo;
using Integraciones.Utils.Integracion.Pdf.ClinicaUniversidadAndes;
using Integraciones.Utils.Integracion.Pdf.ClubAereoCarabineros;
using Integraciones.Utils.Integracion.Pdf.ColegioCoya;
using Integraciones.Utils.Integracion.Pdf.ColumbiaMaquinarias;
using Integraciones.Utils.Integracion.Pdf.ConsorcioCompaniaSeguridad;
using Integraciones.Utils.Integracion.Pdf.ConstructoraIngevec;
using Integraciones.Utils.Integracion.Pdf.ConstructoraLampaOriente;
using Integraciones.Utils.Integracion.Pdf.ConstructoraStaFe;
using Integraciones.Utils.Integracion.Pdf.CorporacionDesarrolloTecnologico;
using Integraciones.Utils.Integracion.Pdf.Dellanatura;
using Integraciones.Utils.Integracion.Pdf.DepositosContenedores;
using Integraciones.Utils.Integracion.Pdf.Dole;
using Integraciones.Utils.Integracion.Pdf.DunkinDonuts;
using Integraciones.Utils.Integracion.Pdf.Duoc;
using Integraciones.Utils.Integracion.Pdf.EastonDesign;
using Integraciones.Utils.Integracion.Pdf.Edam;
using Integraciones.Utils.Integracion.Pdf.EjercitodeChile;
using Integraciones.Utils.Integracion.Pdf.EnvasadosMovipackChile;
using Integraciones.Utils.Integracion.Pdf.ErnstAndYoung;
using Integraciones.Utils.Integracion.Pdf.Eulen;
using Integraciones.Utils.Integracion.Pdf.Ezentis;
using Integraciones.Utils.Integracion.Pdf.GardenHouse;
using Integraciones.Utils.Integracion.Pdf.GasAtacamaChile;
using Integraciones.Utils.Integracion.Pdf.GestionPersonasServiciosLtda;
using Integraciones.Utils.Integracion.Pdf.Highservice;
using Integraciones.Utils.Integracion.Pdf.HormigonesTransex;
using Integraciones.Utils.Integracion.Pdf.HospitalTrabajador;
using Integraciones.Utils.Integracion.Pdf.Hyatt;
using Integraciones.Utils.Integracion.Pdf.Iansa;
using Integraciones.Utils.Integracion.Pdf.Inarco;
using Integraciones.Utils.Integracion.Pdf.Indra;
using Integraciones.Utils.Integracion.Pdf.IngenieriaComercializadoraRiego;
using Integraciones.Utils.Integracion.Pdf.Ingeproject;
using Integraciones.Utils.Integracion.Pdf.InmobiSanCarlos;
using Integraciones.Utils.Integracion.Pdf.InsumosElAlto;
using Integraciones.Utils.Integracion.Pdf.IntegraMedica;
using Integraciones.Utils.Integracion.Pdf.Intertek;
using Integraciones.Utils.Integracion.Pdf.IntertekCalleb;
using Integraciones.Utils.Integracion.Pdf.IsapreConsalud;
using Integraciones.Utils.Integracion.Pdf.JoyGlobal;
using Integraciones.Utils.Integracion.Pdf.KaeferBuildtek;
using Integraciones.Utils.Integracion.Pdf.Komatsu;
using Integraciones.Utils.Integracion.Pdf.Komatsu2;
using Integraciones.Utils.Integracion.Pdf.Kripeos;
using Integraciones.Utils.Integracion.Pdf.LaboratorioLBC;
using Integraciones.Utils.Integracion.Pdf.LarrainPrieto;
using Integraciones.Utils.Integracion.Pdf.LarrainSalas;
using Integraciones.Utils.Integracion.Pdf.LosAndesOperadora;
using Integraciones.Utils.Integracion.Pdf.Maclean;
using Integraciones.Utils.Integracion.Pdf.Madement;
using Integraciones.Utils.Integracion.Pdf.MaestranzaMParts;
using Integraciones.Utils.Integracion.Pdf.Marval;
using Integraciones.Utils.Integracion.Pdf.MasterLineEnjoy;
using Integraciones.Utils.Integracion.Pdf.Megasalud;
using Integraciones.Utils.Integracion.Pdf.MetalurgiaCaceres;
using Integraciones.Utils.Integracion.Pdf.MilanFabjanovic;
using Integraciones.Utils.Integracion.Pdf.MTS;
using Integraciones.Utils.Integracion.Pdf.NemoChile;
using Integraciones.Utils.Integracion.Pdf.Nestle;
using Integraciones.Utils.Integracion.Pdf.OfficeStore;
using Integraciones.Utils.Integracion.Pdf.PaseoLasCondes;
using Integraciones.Utils.Integracion.Pdf.Petrobras;
using Integraciones.Utils.Integracion.Pdf.PizzaHut;
using Integraciones.Utils.Integracion.Pdf.ProCircuit;
using Integraciones.Utils.Integracion.Pdf.Prolab;
using Integraciones.Utils.Integracion.Pdf.Promet;
using Integraciones.Utils.Integracion.Pdf.ProyektaSA;
using Integraciones.Utils.Integracion.Pdf.Readymix;
using Integraciones.Utils.Integracion.Pdf.RecordatorioArauco;
using Integraciones.Utils.Integracion.Pdf.RedBus;
using Integraciones.Utils.Integracion.Pdf.Report;
using Integraciones.Utils.Integracion.Pdf.Rimasa;
using Integraciones.Utils.Integracion.Pdf.RVC;
using Integraciones.Utils.Integracion.Pdf.Salfa;
using Integraciones.Utils.Integracion.Pdf.SaludValpoSanAntonio;
using Integraciones.Utils.Integracion.Pdf.Samsung;
using Integraciones.Utils.Integracion.Pdf.Securitas;
using Integraciones.Utils.Integracion.Pdf.SeidorChile;
using Integraciones.Utils.Integracion.Pdf.SembcorpAguas;
using Integraciones.Utils.Integracion.Pdf.ServiciosAndinos;
using Integraciones.Utils.Integracion.Pdf.ShawAlmexChile;
using Integraciones.Utils.Integracion.Pdf.Sigro;
using Integraciones.Utils.Integracion.Pdf.SociedadEducacionalAraucana;
using Integraciones.Utils.Integracion.Pdf.SociedadInstruccion;
using Integraciones.Utils.Integracion.Pdf.SodexoChile;
using Integraciones.Utils.Integracion.Pdf.Tecnoera;
using Integraciones.Utils.Integracion.Pdf.TecnologiaTrasnporteMinerales;
using Integraciones.Utils.Integracion.Pdf.Teveuk;
using Integraciones.Utils.Integracion.Pdf.TipTop;
using Integraciones.Utils.Integracion.Pdf.Toc;
using Integraciones.Utils.Integracion.Pdf.Traza;
using Integraciones.Utils.Integracion.Pdf.TSM;
using Integraciones.Utils.Integracion.Pdf.UCSilviaEnrique;
using Integraciones.Utils.Integracion.Pdf.UDLA;
using Integraciones.Utils.Integracion.Pdf.UniDesarrollo;
using Integraciones.Utils.Integracion.Pdf.Unilever;
using Integraciones.Utils.Integracion.Pdf.UnitedNations;
using Integraciones.Utils.Integracion.Pdf.UniversidadDesarrollo;
using Integraciones.Utils.Integracion.Pdf.UNAB;
using Integraciones.Utils.Integracion.Pdf.VitaminaWorkLife;
using Integraciones.Utils.Integracion.Pdf.VrkConstructora;
using Integraciones.Utils.Integracion.Pdf.Zical;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Log;
using Integraciones.Utils.Oracle.DataAccess;
using Integraciones.Utils.OrdenCompra;
using Integraciones.Utils.OrdenCompra.Integracion;
using Integraciones.Utils.OrdenCompra.Integracion.OrdenCompraDataAdapter;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Utils.Integracion.Pdf.CorporacionCulturalVitacura;
using Integraciones.Utils.Integracion.Pdf.IngenieriaCivilVicente;
using Integraciones.Utils.Integracion.Pdf.NovoFarma;
using Integraciones.Utils.Integracion.Pdf.AbastecedoraSupermercados;
using Integraciones.Utils.Integracion.Pdf.FerreteriaSantiago;
using Integraciones.Utils.Integracion.Pdf.RedSaludPuc;
using Integraciones.Utils.Integracion.Pdf.JcIngenieriaSpa;
using Integraciones.Utils.Integracion.Pdf.Andacor;
using Integraciones.Utils.Integracion.Pdf.Artikos;
using Integraciones.Utils.Integracion.Pdf.IngenieriaEVH;
using Integraciones.View;
using Integraciones.Utils.Integracion.Pdf.InstitutoDeFomentoPesquero;
using Integraciones.Utils.Integracion.Pdf.Kibernum;
using Integraciones.Utils.Integracion.Pdf.CotizacionDimerc;
using Integraciones.Utils.Integracion.Pdf.ConsorcioExcon;
using Integraciones.Utils.Integracion.Pdf.ZublinChuquicamata;
using Integraciones.Utils.Integracion.Pdf.ClinicaSantaMaria;
using Integraciones.Utils.Integracion.Pdf.InmobiliariaChillanSpa;
using Integraciones.Utils.Integracion.Pdf.Construmart;
using Integraciones.Utils.Integracion.Pdf.Construmart2;
using Integraciones.Utils.Integracion.Pdf.RTC;
using Integraciones.Utils.Integracion.Pdf.Tepsac;
using Integraciones.Utils.Integracion.Pdf.ConstructoraBYC;
using Integraciones.Utils.Integracion.Pdf.Resiter;
using Integraciones.Utils.Integracion.Pdf.EcheverriaIzquierdo;
using Integraciones.Utils.Integracion.Pdf.AplLogistics;
using Integraciones.Utils.Integracion.Pdf.Arrigoni;
using Integraciones.Utils.Integracion.Pdf.CiaPisqueraChile;
using Integraciones.Utils.Integracion.Pdf.Socovesa;
using Integraciones.Utils.Integracion.Pdf.Vertiv;
using Integraciones.Utils.Integracion.Pdf.PI_Berries;
using Integraciones.Utils.Integracion.Pdf.CPP;
using Integraciones.Utils.Integracion.Pdf.TrainTransportesIntegrados;
using Integraciones.Utils.Integracion.Pdf.ConstructoraNumair;
using Integraciones.Utils.Integracion.Pdf.ArquitecturayConstrucciónWorkplacesSPA;
using Integraciones.Utils.Integracion.Pdf.ClinicaReñaca;
using Integraciones.Utils.Integracion.Pdf.Kunstmann;
using Integraciones.Utils.Integracion.Pdf.Besalco;
using Integraciones.Utils.Integracion.Pdf.UniversidadCatolicaSilvaHenriquez;
using Integraciones.Utils.Integracion.Pdf.ChileInox;
using Integraciones.Utils.Integracion.Pdf.MetroSA;
using Integraciones.Utils.Integracion.Pdf.BancoSecurity;
using Integraciones.Utils.Integracion.Pdf.Metrogas;
using Integraciones.Utils.Integracion.Pdf.HDI;
using Integraciones.Utils.Integracion.Pdf.ACHS;
using Integraciones.Utils.Integracion.Pdf.BodegaSanFrancisco;
using Integraciones.Utils.Integracion.Pdf.TelefonicaIngSeguridad;
using Integraciones.Utils.Integracion.Pdf.PuertoCentral;
using Integraciones.Utils.Integracion.Pdf.SteelFerrovialSenegocia;
using Integraciones.Utils.Integracion.Pdf.SteelFerrovial1;
using Integraciones.Utils.Integracion.Pdf.Dictuc;
using Integraciones.Utils.Integracion.Pdf.PuenteSurGTR;
using Integraciones.Utils.Integracion.Pdf.LaAraucanaSalud;
using Integraciones.Utils.Integracion.Pdf.Ferricom;
using Integraciones.Utils.Integracion.Pdf.CraIngenieria;
using Integraciones.Utils.Integracion.Pdf.KreisSpa;
using Integraciones.Utils.Integracion.Pdf.Happyland;
using Integraciones.Utils.Integracion.Pdf.UC_ChristusServiciosAmbulatorios;
using Integraciones.Utils.Integracion.Pdf.Moller;
using Integraciones.Utils.Integracion.Pdf.Aridos;
using Integraciones.Utils.Integracion.Pdf.Precon2;
using Integraciones.Utils.Integracion.Pdf.Harting;
using Integraciones.Utils.Integracion.Pdf.IronMountain;
using Integraciones.Utils.Integracion.Pdf.ClaroVicunaValenzuela;
using Integraciones.Utils.Integracion.Pdf.SolucionesAmbientalesDelNorte;
using Integraciones.Utils.Integracion.Pdf.CerealesCPWChileLtda;
using Integraciones.Utils.Integracion.Pdf.ArquitecturaIngenieriaAconcagua;
using Integraciones.Utils.Integracion.Pdf.EmpresaElectricaAysen;
using Integraciones.Utils.Integracion.Pdf.Langues_Affairs_SpA;
using Integraciones.Utils.Integracion.Pdf.TresMontes;
using Integraciones.Utils.Integracion.Pdf.IronMointain2;
using Integraciones.Utils.Integracion.Pdf.ComercialCCU;
using Integraciones.Utils.Integracion.Pdf.BiceVidaCompSeg;
using Integraciones.Utils.Integracion.Pdf.EmersonElectric;
using Integraciones.Utils.Integracion.Pdf.Sandvik;
using Integraciones.Utils.Integracion.Pdf.CFTSantotomas;
using Integraciones.Utils.Integracion.Pdf;
using Integraciones.Utils.Integracion.Pdf.AsapIngenieria;
using Integraciones.Utils.Integracion.Pdf.LaboratorioXimenaPolanco;
using Integraciones.Utils.Integracion.Pdf.ConstructoraSiena;
using Integraciones.Utils.Integracion.Pdf.AgroIndustrialMachicura;
using Integraciones.Utils.Integracion.Pdf.Soprodi;
using Integraciones.Utils.Integracion.Pdf.Constructora_ICF;
using Integraciones.Utils.Integracion.Pdf.ConstructoraClass;
using Integraciones.Utils.Integracion.Pdf.CptRemolcadores;
using Integraciones.Utils.Integracion.Pdf.InmobiliariaNogales;
using Integraciones.Utils.Integracion.Pdf.ConstructoraParqueChicureo;
using Integraciones.Utils.Integracion.Pdf.ComercialKendall;
using Integraciones.Utils.Integracion.Pdf.UniversidadDiegoPortales;
using Integraciones.Utils.Integracion.Pdf.CoopeativaAgricolaLecheraSantiagoLtda;
using Integraciones.Utils.Integracion.Pdf.Mercadopublico;
using Integraciones.Utils.Integracion.Pdf.TelefonicaIngDeSeguridad;
using Integraciones.Utils.FlagClose;
using Integraciones.Utils.Integracion.Pdf.ClinicaColonial;
using Integraciones.Utils.Integracion.Pdf.Latam;
using Integraciones.Utils.Integracion.Pdf.Emerson;
using Integraciones.Utils.Integracion.Pdf.HTGConstrucciones;
using Integraciones.Utils.Integracion.Pdf.BciArtikos;
using Integraciones.Utils.Integracion.Pdf.InmobiliariaLosOlmos;
using Integraciones.Utils.Integracion.Pdf.InmobiliariaNuevoPuenteAltoSA;
using Integraciones.Utils.Integracion.Pdf.EmbotelladoraAndinaSA;
using Integraciones.Utils.Integracion.Pdf.Reliper;
using Integraciones.Utils.Integracion.Pdf.SIGAIng;
using Integraciones.Utils.Integracion.Pdf.Drillco;
using Integraciones.Utils.Integracion.Pdf.JorgeSchmidt;
using Integraciones.Utils.Integracion.Pdf.CorporacionEducacionSalud;
using Integraciones.Utils.Integracion.Pdf.Archroma2;
using Integraciones.Utils.Integracion.Pdf.BelfiIncolur;
using Integraciones.Utils.Integracion.Pdf.IngConSantaFe;
using Integraciones.Utils.Integracion.Pdf.ServiciosyAbastecimientosMedicos;
using Integraciones.Utils.Integracion.Excel.Consorcio;
using Integraciones.Utils.Integracion.Pdf.Consorcio;
using Integraciones.Utils.Integracion.Pdf.ChilematSpa;
using Integraciones.Utils.Integracion.Pdf.CraMontajesSpa;
using Integraciones.Utils.Integracion.Pdf.FedericoSantaMaria;
using Integraciones.Utils.Integracion.Pdf.Walmart;
using Integraciones.Utils.Integracion.Pdf.TransportesSantaMariaSpa;
using Integraciones.Utils.Integracion.Pdf.CanplasSud;
using Integraciones.Utils.Integracion.Pdf.UniversidadCentral;
using Integraciones.Utils.Integracion.Pdf.Ingevec2;
using Integraciones.Utils.Integracion.Pdf.Inacap;
using Integraciones.Utils.Integracion.Pdf.G4S;
using Integraciones.Utils.Integracion.Pdf.ConstructoraDVC;
using Integraciones.Utils.Integracion.Pdf.InmobiliariaAconcagua;
using Integraciones.Utils.Integracion.Pdf.Megalogistica;
using Integraciones.Utils.Integracion.Pdf.Arcoprime;
using Integraciones.Utils.Integracion.Pdf.Ariztia;
using Integraciones.Utils.Integracion.Pdf.CooperativaRegionalElectrica;
using Integraciones.Utils.Integracion.Pdf.HogarDeCristo;
using Integraciones.Utils.Integracion.Pdf.Zublin;
using Integraciones.Utils.Integracion.Pdf.CapitalSA;
using Integraciones.Utils.Integracion.Pdf.AVIS;
using Integraciones.Utils.Integracion.Pdf.ConstructoraElAbra;
using Integraciones.Utils.Integracion.Pdf.ClinicaAlemanaValdivia;
using Integraciones.Utils.Integracion.Pdf.Nexxo;
using Integraciones.Utils.Integracion.Pdf.COMPASS;
using Integraciones.Utils.Integracion.Pdf.Camanchaca;
using Integraciones.Utils.Integracion.Pdf.Omesa;
using Integraciones.Utils.Integracion.Pdf.GSK;
using Integraciones.Utils.Integracion.Pdf.Corpora;
using Integraciones.Utils.Integracion.Pdf.Ilsauspe;
using Integraciones.Utils.Integracion.Pdf.Engie;
using Integraciones.Utils.Integracion.Pdf.Sodexo;
using Integraciones.Utils.Integracion.Pdf.UniversidadSantoTomas;
using Integraciones.Utils.Integracion.Pdf.Nephrocare;
using Integraciones.Utils.Integracion.Pdf.Gruponorteseguridad;
using Integraciones.Utils.Integracion.Pdf.IsapreCruzBlanca;
using Integraciones.Utils.Integracion.Pdf.Vulco;
using Integraciones.Utils.Integracion.Pdf.EMARESA;
using Integraciones.Utils.Integracion.Pdf.Soho;
using Integraciones.Utils.Integracion.Pdf.AquaChile;
using Integraciones.Utils.Integracion.Pdf.Sacyr;
using Integraciones.Utils.Integracion.Pdf.Anasac;
using Integraciones.Utils.Integracion.Pdf.Watts;
using Integraciones.Utils.Integracion.Pdf.PortalPUC;
using Integraciones.Utils.Integracion.Pdf.Zara;
using Integraciones.Utils.Integracion.Pdf.LatinGaming;
using Integraciones.Utils.Integracion.Pdf.FedexNuevoFormato;
using Integraciones.Utils.Integracion.Pdf.AreasSA;
using Integraciones.Utils.Integracion.Pdf.Fluitek;
using Integraciones.Utils.Integracion.Pdf.FundacionLasRosas;
using Integraciones.Utils.Integracion.Pdf.VoySantiago;
//using Integraciones.Utils.Integracion.Pdf.Promasa;


//using Integraciones.Utils.Integracion.Pdf.SantaIsabel;
//using Integraciones.Utils.Integracion.Pdf.Sandivk;

namespace Integraciones.Main.Pdf
{
    public static class MainPdf
    {
        #region Variables

        private static bool OnlyOne;

        #endregion


        public static void ExecuteSinglePdf(PDFReader pdfReader)
        {
            var option = GetPdfOptionNumber(pdfReader);
            OrdenCompra ordenCompra = null;
            var ocAdapter = new OrdenCompraIntegracion();
            var ocAdapterList = new List<OrdenCompraIntegracion>();
            Console.WriteLine($"FIRST: {option}");
            bool postProcess = false;
            Log.Save("log_de_pdf", $"Archivo {pdfReader.PdfFileName}");
            Log.Save("log_de_pdf", $"FIRST: {option}");
            #region SWITCH CASE INTEGRACIONES

            //option = 306;

            switch (option)
            {
                case 0:
                    var easy = new Easy(pdfReader);
                    ordenCompra = easy.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatToCompraIntegracion();
                    break;
                case 1:
                    var cencosudRetailSa1 = new Cencosud(pdfReader);
                    ordenCompra = cencosudRetailSa1.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatToCompraIntegracion();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 2:
                    var cencosudRetailSa = new Cencosud(pdfReader);
                    ordenCompra = cencosudRetailSa.GetOrdenCompra();
                    // ocAdapter = ordenCompra.AdapterGenericFormatToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case -2:
                    var cencosudDescripcion = new CencosudDescripcion(pdfReader);
                    ordenCompra = cencosudDescripcion.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatToCompraIntegracion();
                    break;
                case 3:
                    var indra = new Indra(pdfReader);
                    ordenCompra = indra.GetOrdenCompra();

                    break;
                case 5:
                    var unab = new Unab(pdfReader);
                    ordenCompra = unab.GetOrdenCompra();
                    // ocAdapter = ordenCompra.AdapterUnabFormatToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case -5:
                    var corpUnab = new CorpUnab(pdfReader);
                    ordenCompra = corpUnab.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterUnabFormatToCompraIntegracion();
                    break;
                case 6:
                    var esach = new Esach(pdfReader);
                    ordenCompra = esach.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 7:
                    var bhpBilliton = new BhpBilliton(pdfReader);
                    ordenCompra = bhpBilliton.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterBhpBillitonFormatToCompraIntegracion();
                    break;

                case 8:
                    var clinicaDavila = new ClinicaDavila(pdfReader);
                    if (clinicaDavila.HaveAnexo())
                    {
                        var ordenes = clinicaDavila.GetOrderFromAnexo();
                        ocAdapterList =
                            ordenes.Select(or => or.AdapterClinicaDavilaFormatToCompraIntegracionWithMatchCencos())
                                .ToList();
                        option = -8;
                    }
                    else
                    {
                        var ordenCompraD = clinicaDavila.GetOrdenCompraProcesada();
                        ordenCompra = ordenCompraD;
                        if (ordenCompraD.Items.Count == 0)
                        {
                            //ordenCompraD.Items = ordenCompraD.ItemsClinicaDavila;
                            ocAdapter = ordenCompraD.AdapterClinicaDavilaFormatToCompraIntegracionWithMatchCencos();
                        }
                        else
                        {
                            ocAdapter = ordenCompraD.TraspasoUltimateIntegracion(pdfReader);
                        }
                        //ordenCompraD.CentroCosto = "0";
                        //

                    }
                    break;
                case 9:
                    var carozzi = new Carozzi(pdfReader);
                    var ordenCompraCarozzi = carozzi.GetOrdenCompra();
                    ordenCompra = ordenCompraCarozzi; //carozzi.GetOrdenCompra();
                    ocAdapter = ordenCompraCarozzi.AdapterCarozziFormatToCompraIntegracion();
                    break;
                case 10:
                    var securitasAustral = new SecuritasAustral(pdfReader);
                    var ordenCompraSa = securitasAustral.GetOrdenCompra();
                    ordenCompra = ordenCompraSa;
                    ocAdapterList = ordenCompraSa.AdapterSecuritasAustralFormatToCompraIntegracion();
                    break;
                case 4:
                    var securitas = new Securitas(pdfReader);
                    var ordeCompraS = securitas.GetOrdenCompra();
                    ordenCompra = ordeCompraS;
                    ocAdapterList = ordeCompraS.AdapterSecuritasAustralFormatToCompraIntegracionWithBodega();
                    break;
                case 12:
                    var securitasCapacitaciones = new SecuritasAustral(pdfReader);
                    var ordenCompraCa = securitasCapacitaciones.GetOrdenCompra();
                    ordenCompra = ordenCompraCa;
                    ocAdapterList = ordenCompraCa.AdapterSecuritasAustralFormatToCompraIntegracion();
                    break;
                case 11:
                    var dole = new Dole(pdfReader);
                    ordenCompra = dole.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterDoleFormatToCompraIntegracion();
                    break;
                case 13:
                    var udla = new Udla(pdfReader);
                    ordenCompra = udla.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterUnabFormatToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 14:
                    var aiep = new Aiep(pdfReader);
                    ordenCompra = aiep.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterUnabFormatToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 15:
                    var cliniAlemana = new ClinicaAlemana(pdfReader);
                    ordenCompra = cliniAlemana.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterClinicaAlemanaFormatToCompraIntegracion();
                    break;
                case -15:
                    var clinicaAlemanaArtikosFormat = new ClinicaAlemanaArtikosFormat(pdfReader);
                    ordenCompra = clinicaAlemanaArtikosFormat.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    //ocAdapter = ordenCompra.AdapterClinicaAlemanaFormatToCompraIntegracion();
                    break;
                case 16:
                    var uvm = new Udla(pdfReader);
                    ordenCompra = uvm.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterUnabFormatToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 17:
                    var dellanatura = new Dellanatura(pdfReader);
                    ordenCompra = dellanatura.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    break;
                case 18:
                    var toc = new Toc(pdfReader);
                    ordenCompra = toc.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    break;
                case 19:
                    var komatsu = new Komatsu(pdfReader);
                    ordenCompra = komatsu.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    foreach (var a in ocAdapter.DetallesCompra)
                    {
                        a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    }
                    break;
                case 20:
                    var komatsuCummins = new KomatsuCummins(pdfReader);
                    ordenCompra = komatsuCummins.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    foreach (var a in ocAdapter.DetallesCompra)
                    {
                        a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    }
                    break;
                case 23:
                    var isapreConsalud = new IsapreConsalud(pdfReader);
                    ordenCompra = isapreConsalud.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterGenericFormatWithSkuAndNumericCencosToCompraIntegracion();                    
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 24:
                    var iansa = new Iansa(pdfReader);
                    ordenCompra = iansa.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterIansaFormatToCompraIntegracion();
                    break;
                case 26:
                    var pizzaHut = new PizzaHut(pdfReader);
                    ordenCompra = pizzaHut.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatToCompraIntegracion();
                    break;
                case 27:
                    var constructoraIngevec = new ConstructoraIngevec(pdfReader);
                    ordenCompra = constructoraIngevec.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    //ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraWithStockZ446482Integracion();
                    break;
                case 28:
                    var vitaminaWorkLife = new VitaminaWorkLife(pdfReader);
                    ordenCompra = vitaminaWorkLife.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 29:
                    var mts = new Mts(pdfReader);
                    ordenCompra = mts.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    //AdapterGenericFormatWithSkuAndDescriptionCencosWithMatchToCompraIntegracion();
                    break;
                case 31:
                    var candem = new Candem(pdfReader);
                    ocAdapterList.AddRange(
                        candem.GetOrdenCompra2().Select(ord => ord.AdapterGenericFormatWithSkuToCompraIntegracion()));
                    break;
                case 32:
                    var serviciosAndinos = new ServicioAndinos(pdfReader);
                    ordenCompra = serviciosAndinos.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterUnabFormatToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 33:
                    var gestionPersona = new GestionPersonasServiciosLtda(pdfReader);
                    ordenCompra = gestionPersona.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterGenericFormatWithSkuAndNumericCencosToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 34:
                    //TODO LIBRE PARA INTEGRAR
                    break;
                case 35:
                    var officeStore = new OfficeStore(pdfReader);
                    ordenCompra = officeStore.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    break;
                case 36:
                    var clinicaLila = new ClinicaLilas(pdfReader);
                    ordenCompra = clinicaLila.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithDescriptionCencosToCompraIntegracion();
                    break;
                case 37:
                    //TODO ABENGOA
                    break;
                case 38:
                    var clinicaAndes = new ClinicaUniversidadAndes(pdfReader);
                    ordenCompra = clinicaAndes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader); //cambio APARDO ParearSoloSKU()
                    break;
                case 40:
                    var integraMedica = new IntegraMedica(pdfReader);
                    ordenCompra = integraMedica.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithDescriptionCencosToCompraIntegracion();
                    switch (ocAdapter.RutCli)
                    {
                        case 96845430:
                            ocAdapter.Observaciones +=
                                integraMedica.Cc96845430Observaciones[int.Parse(ocAdapter.CenCos)];
                            break;
                        case 96986050:
                            ocAdapter.Observaciones +=
                                integraMedica.Cc96986050Observaciones[int.Parse(ocAdapter.CenCos)];
                            break;
                        case 79716500:
                            ocAdapter.Observaciones +=
                                integraMedica.Cc79716500Observaciones[int.Parse(ocAdapter.CenCos)];
                            break;
                        case 76098454:
                            ocAdapter.Observaciones +=
                                integraMedica.Cc76098454Observaciones[int.Parse(ocAdapter.CenCos)];
                            break;
                        case 76217761:
                            ocAdapter.Observaciones +=
                                integraMedica.Cc76217761Observaciones[int.Parse(ocAdapter.CenCos)];
                            break;
                    }
                    break;
                case 41:
                    var ecoriles = new Ecoriles(pdfReader);
                    ordenCompra = ecoriles.GetOrdenCompra();
                    ordenCompra.CentroCosto = "2";
                    ocAdapter = ordenCompra.ParearSoloSKU();
                    break;
                case 42:
                    var komatsuCumminsArrienda = new KomatsuCumminsArrienda(pdfReader);
                    ordenCompra = komatsuCumminsArrienda.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    foreach (var a in ocAdapter.DetallesCompra)
                    {
                        a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    }
                    break;

                case 43:
                    var integraMedicaAtencionAmbulatoria = new IntegraMedicaAtencionAmbulatorio(pdfReader);
                    ordenCompra = integraMedicaAtencionAmbulatoria.GetOrdenCompra();
                    //ocAdapter = ordenCompra.AdapterGenericFormatWithDescriptionCencosToCompraIntegracion();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                    }
                    try
                    {
                        switch (ocAdapter.RutCli)
                        {

                            case 96845430:
                                ocAdapter.Observaciones +=
                                    integraMedicaAtencionAmbulatoria.Cc96845430Observaciones[
                                        int.Parse(ocAdapter.CenCos)];
                                break;
                            case 96986050:
                                ocAdapter.Observaciones +=
                                    integraMedicaAtencionAmbulatoria.Cc96986050Observaciones[
                                        int.Parse(ocAdapter.CenCos)];
                                break;
                            case 79716500:
                                ocAdapter.Observaciones +=
                                    integraMedicaAtencionAmbulatoria.Cc79716500Observaciones[
                                        int.Parse(ocAdapter.CenCos)];
                                break;
                            case 76098454:

                                ocAdapter.Observaciones +=
                                    integraMedicaAtencionAmbulatoria.Cc76098454Observaciones[
                                        int.Parse(ocAdapter.CenCos)];
                                break;
                            case 96879440:

                                ocAdapter.Observaciones +=
                                    integraMedicaAtencionAmbulatoria.Cc76098454Observaciones[
                                        int.Parse(ocAdapter.CenCos)];

                                break;
                        }
                    }
                    catch
                    {
                        ocAdapter.Observaciones += "No es posible reconocer la Observaciones del Centro de Costo.";
                    }
                    break;
                case 44:
                    var komatsuRemman = new KomatsuReman(pdfReader);
                    ordenCompra = komatsuRemman.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion();
                    foreach (var a in ocAdapter.DetallesCompra)
                    {
                        a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    }
                    break;
                case 45:
                    var komatsuDistribuidoraCummins = new KomatsuDistribuidoraCummins(pdfReader);
                    ordenCompra = komatsuDistribuidoraCummins.GetOrdenCompra();
                    ocAdapter = ordenCompra.AdapterGenericFormatWithSkuToCompraIntegracion(); //TraspasoUltimateIntegracion()
                    foreach (var a in ocAdapter.DetallesCompra)
                    {
                        a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    }
                    break;
                case 48:
                    var consorcioSeguridad = new ConsorcioCompaniaSeguridad(pdfReader);
                    ordenCompra = consorcioSeguridad.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 49:
                    var megaSalud = new Megasalud(pdfReader);
                    ordenCompra = megaSalud.GetOrdenCompra();
                    ordenCompra.ConsoleWriteLine();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion();//TraspasoUltimateIntegracionNuevoFlujo(); //ToCompraIntegracionSkuCentroCostoDePdf
                    break;
                case 50:
                    var arauco = new Arauco(pdfReader);
                    ordenCompra = arauco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 51:
                    var kaeferBuildtek = new KaeferBuildtek(pdfReader);
                    ordenCompra = kaeferBuildtek.GetOrdenCompra();
                    //FACTA CC y PAREO DE SKU
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 53:
                    var intertek = new Intertek(pdfReader);
                    ordenCompra = intertek.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 54:
                    var analisisAmbientales = new Ecoriles(pdfReader);
                    ordenCompra = analisisAmbientales.GetOrdenCompra();
                    ordenCompra.CentroCosto = "1";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 56:
                    var corporacionDesarrolloTecnologico = new CorporacionDesarrolloTecnologico(pdfReader);
                    ordenCompra = corporacionDesarrolloTecnologico.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 57:
                    var aguasAndinas = new Ecoriles(pdfReader);
                    ordenCompra = aguasAndinas.GetOrdenCompra();
                    //ordenCompra.CentroCosto = "161";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader); //ordenCompra.ParearSoloSKU();
                    break;
                case 58:
                    var ingeProject = new Ingeproject(pdfReader);
                    ordenCompra = ingeProject.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 59:
                    var sociedadAraucana = new SociedadEducacionalAraucana(pdfReader);
                    ordenCompra = sociedadAraucana.GetOrdenCompra();
                    ocAdapter = ordenCompra.ParearSoloDescripcionCliente();
                    break;
                case 60:
                    var dunkinDonuts = new DunkinDonuts(pdfReader);
                    ordenCompra = dunkinDonuts.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 61:
                    var teveuk = new Teveuk(pdfReader);
                    ordenCompra = teveuk.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 62:
                    var colegioCoya = new ColegioCoya(pdfReader);
                    ordenCompra = colegioCoya.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 63:
                    var sociedadInstruccion = new SociedadInstruccion(pdfReader);
                    ordenCompra = sociedadInstruccion.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 64:
                    var shawAlmexChile = new ShawAlmexChile(pdfReader);
                    ordenCompra = shawAlmexChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 65:
                    var unitedNations = new UnitedNations(pdfReader);
                    ordenCompra = unitedNations.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 66:
                    var ingeniericaComercializadoraRiego = new IngenieriaComercializadoraRiego(pdfReader);
                    ordenCompra = ingeniericaComercializadoraRiego.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 67:
                    var envasadosMovipackChile = new EnvasadosMovipackChile(pdfReader);
                    ordenCompra = envasadosMovipackChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 68:
                    var larrainSalas = new LarrainSalas(pdfReader);
                    ordenCompra = larrainSalas.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 69:
                    var laboratorioLBC = new LaboratorioLBC(pdfReader);
                    ordenCompra = laboratorioLBC.GetOrdenCompra();
                    //FALTA HOMOLOGACION Y CCC
                    break;
                case 70:
                    var proyektaSA = new ProyektaSA(pdfReader);
                    ordenCompra = proyektaSA.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 71:
                    var biomedicalDistributionChile = new BiomedicalDistributionChile(pdfReader);
                    ordenCompra = biomedicalDistributionChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 72:
                    var loginsa = new BiomedicalDistributionChile(pdfReader);
                    ordenCompra = loginsa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    //IDENTIFICAR CC
                    break;
                case 73:
                    var depositosContenedores = new DepositosContenedores(pdfReader);
                    ordenCompra = depositosContenedores.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 74:
                    var aridosSantaFerSa = new AridosSantaFeSA(pdfReader);
                    ordenCompra = aridosSantaFerSa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 75:
                    var cementosTransex = new CementosTransex(pdfReader);
                    ordenCompra = cementosTransex.GetOrdenCompra();
                    break;
                case 76:
                    var ezentis = new Ezentis(pdfReader);
                    ordenCompra = ezentis.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 77:
                    var rimasa = new Rimasa(pdfReader);
                    ordenCompra = rimasa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 78:
                    var grupoEulenChile = new GrupoEulen(pdfReader); // EULEN CHILE S.A.
                    ordenCompra = grupoEulenChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 79:
                    var eluenChile = new EulenChile(pdfReader);
                    ordenCompra = eluenChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 80:
                    var petrobrasChileRed = new Petrobras(pdfReader);
                    ordenCompra = petrobrasChileRed.GetOrdenCompra();
                    ordenCompra.Rut = "79706120";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 81:
                    var petrobrasChileDistribucion = new Petrobras(pdfReader);
                    ordenCompra = petrobrasChileDistribucion.GetOrdenCompra();
                    ordenCompra.Rut = "79588870";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 82:
                    var eulenSeugirdad = new GrupoEulen(pdfReader);
                    ordenCompra = eulenSeugirdad.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 83:
                    var servicioExternosAch = new Esach(pdfReader);
                    ordenCompra = servicioExternosAch.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 84:
                    var report = new Report(pdfReader);
                    ordenCompra = report.GetOrdenCompra();
                    break;
                case 85:
                    var tecnologiaTransporteMinerales = new TecnologiaTransporteMinerales(pdfReader);
                    ordenCompra = tecnologiaTransporteMinerales.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 86:
                    var cecyt = new CapacitacionCienciasTecnologiasLimitada(pdfReader);
                    ordenCompra = cecyt.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 87:
                    var maquinariasSantaFe = new MaquinariasSantaFe(pdfReader);
                    ordenCompra = maquinariasSantaFe.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 88:
                    var clinicaSanBernardo = new ClinicaSanBernardo(pdfReader);
                    ordenCompra = clinicaSanBernardo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 89:
                    var losAndesOperadora = new LosAndesOperadora(pdfReader);
                    ordenCompra = losAndesOperadora.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 90:
                    var prolab = new Prolab(pdfReader);
                    ordenCompra = prolab.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 91:
                    var cementerioMetropolotano = new CementerioMetropolitanoLTDA(pdfReader);
                    ordenCompra = cementerioMetropolotano.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_CODIGO
                        , ordenCompra.Rut
                        , ordenCompra.NumeroCompra
                        , pdfReader.PdfFileName);
                    break;
                case 92:
                    var semCopr = new SembcorpAguas(pdfReader);
                    ordenCompra = semCopr.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 93:
                    var ascencsoresSchindler = new AscensoresSchindler(pdfReader);
                    ordenCompra = ascencsoresSchindler.GetOrdenCompra();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 94:
                    var hospitalDelTrabajador = new HospitalTrabajador(pdfReader);
                    ordenCompra = hospitalDelTrabajador.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 95:
                    var rvc = new Rvc(pdfReader);
                    ordenCompra = rvc.GetOrdenCompra();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    /*if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    }*/

                    break;
                case 96:
                    var highservice = new Highservice(pdfReader);
                    ordenCompra = highservice.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 97:
                    var universidadDesarrollo = new UniversidadDesarrollo(pdfReader);
                    ordenCompra = universidadDesarrollo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 98:
                    var operacionesIntegralesIslaGrande = new MasterLineEnjoy(pdfReader);
                    ordenCompra = operacionesIntegralesIslaGrande.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 99:
                    var duoc = new Duoc(pdfReader);
                    ordenCompra = duoc.GetOrdenCompra();
                    //ordenCompra.Rut = "16460269";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "1";
                    }
                    Console.WriteLine(ordenCompra.ToString());
                    break;
                case 100:
                    var axis = new Axis(pdfReader);
                    ordenCompra = axis.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 101:
                    var auraIngenieria = new AuraIngenieria(pdfReader);
                    ordenCompra = auraIngenieria.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 102:
                    var tipTop = new TipTop(pdfReader);
                    ordenCompra = tipTop.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 103:
                    var armas = new Armas(pdfReader);
                    ordenCompra = armas.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    }
                    break;
                case 104:
                    var armasGestionLimitada = new ArmasGestionLimitada(pdfReader);
                    ordenCompra = armasGestionLimitada.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    }
                    //throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_CODIGO
                    //    , ordenCompra.Rut
                    //    , ordenCompra.NumeroCompra
                    //    , pdfReader.PdfFileName);

                    break;
                case 105:
                    var constructoraArmasLtda = new ConstructoraArmasLtda(pdfReader);
                    ordenCompra = constructoraArmasLtda.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    }
                    break;
                case 106:
                    var redymix = new Readymix(pdfReader);
                    ordenCompra = redymix.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_CODIGO
                        , ordenCompra.Rut
                        , ordenCompra.NumeroCompra
                        , pdfReader.PdfFileName);
                    break;
                case 107:
                    var busesVule = new BusesVule(pdfReader);
                    ordenCompra = busesVule.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_CODIGO
                        , ordenCompra.Rut
                        , ordenCompra.NumeroCompra
                        , pdfReader.PdfFileName);
                    break;
                case 108:
                    var nestle = new Nestle(pdfReader);
                    ordenCompra = nestle.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 109:
                    var morterosTransex = new MorterosTransex(pdfReader);
                    ordenCompra = morterosTransex.GetOrdenCompra();
                    // ordenCompra.Rut = "16460269";
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 110:
                    //TODO DELLAFORI
                    throw new ErrorIntegracion(TipoError.INTEGRACION_NO_TERMINADA
                        , "DELLAFORI"
                        , ""
                        , pdfReader.PdfFileName);
                    break;
                case 111:
                    var marval = new Marval(pdfReader);
                    ordenCompra = marval.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    if (ocAdapter.Observaciones.Contains("ANTOFAGASTA"))
                        foreach (var det in ocAdapter.DetallesCompra)
                            det.CodigoBodega = TipoBodega.ANTOFAGASTA;

                    break;
                case 112:
                    var hyatt = new Hyatt(pdfReader);
                    ordenCompra = hyatt.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 113:
                    var clinicaCordillera = new ClinicaCordillera(pdfReader);
                    ordenCompra = clinicaCordillera.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 114:
                    var inarco = new Inarco(pdfReader);
                    ordenCompra = inarco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 115:
                    var edam = new Edam(pdfReader);
                    ordenCompra = edam.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_CODIGO
                        , ordenCompra.Rut
                        , ordenCompra.NumeroCompra
                        , pdfReader.PdfFileName);
                    break;
                case 116:
                    var gasAtacamaChile = new GasAtacamaChile(pdfReader);
                    ordenCompra = gasAtacamaChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_Y_MULTIPLOS
                        , ordenCompra.Rut
                        , ordenCompra.NumeroCompra
                        , pdfReader.PdfFileName);
                    break;
                case 117:
                    var redBus = new RedBus(pdfReader);
                    ordenCompra = redBus.GetOrdenCompra();
                    ordenCompra.ConsoleWriteLine();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);


                    break;
                case 118:
                    var milanFabjanovic = new MilanFabjanovic(pdfReader);
                    ordenCompra = milanFabjanovic.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    //return;
                    //throw new ErrorIntegracion(TipoError.FALTA_VALIDAR_PAREO_Y_MULTIPLOS
                    //    , ordenCompra.Rut
                    //    , ordenCompra.NumeroCompra
                    //    , pdfReader.PdfFileName);
                    break;
                case 119:
                    var samsung = new Samsung(pdfReader);
                    ordenCompra = samsung.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 120:
                    var uCSilviaEnriquez = new UcSilviaEnriquez(pdfReader);
                    ordenCompra = uCSilviaEnriquez.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 121:
                    var maclean = new Maclean(pdfReader);
                    ordenCompra = maclean.GetOrdenCompra();
                    ordenCompra.ConsoleWriteLine();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    //return;
                    break;
                case 122:
                    var sodexoChile = new SodexoChile(pdfReader);
                    ordenCompra = sodexoChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 123:
                    var promet = new PrometSpa(pdfReader);
                    ordenCompra = promet.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 124:
                    var abastecedoraALimentos = new AbastecedoraAlimentos(pdfReader);
                    ordenCompra = abastecedoraALimentos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 125:
                    var ferreteriaSantiago = new FerreteriaSantiago(pdfReader);
                    ordenCompra = ferreteriaSantiago.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 126:
                    var jumbo = new Jumbo(pdfReader);
                    ordenCompra = jumbo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;

                case 9999:
                    var salvaVida = new SalvaVidas(pdfReader);
                    ordenCompra = salvaVida.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    //Console.WriteLine(ordenCompra);
                    //return;
                    break;

                case 628:
                    var santaIsabel = new SantaIsabel(pdfReader);
                    ordenCompra = santaIsabel.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;

                case 200:
                    var cerveceriaCCU = new CerveceriaCCU(pdfReader);
                    ordenCompra = cerveceriaCCU.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 201:
                    var constructoraStaFe = new ConstructoraStaFe(pdfReader);
                    ordenCompra = constructoraStaFe.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 202:
                    var seidorChile = new SeidorChile(pdfReader);
                    ordenCompra = seidorChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 203:
                    var cftSanAgustin = new CFTSanAgustin(pdfReader);
                    ordenCompra = cftSanAgustin.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 204:
                    var alimentosSanMartin = new AlimentosSanMartin(pdfReader);
                    ordenCompra = alimentosSanMartin.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 205:
                    var zical = new Zical(pdfReader);
                    ordenCompra = zical.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 206:
                    var clubAereoCarabineros = new ClubAereoCarabineros(pdfReader);
                    ordenCompra = clubAereoCarabineros.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 207:
                    var maestranzaMParts = new MaestranzaMParts(pdfReader);
                    //ordenCompra = maestranzaMParts.GetOrdenCompra();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;

                case 208:
                    var vrkConstructora = new VrkConstructora(pdfReader);
                    //ordenCompra = vrkConstructora.GetOrdenCompra();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;

                case 209:
                    var constructoraLampaOriente = new ConstructoraLampaOriente(pdfReader);
                    ordenCompra = constructoraLampaOriente.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 210:
                    var masterLineEnjoy = new MasterLineEnjoy(pdfReader);
                    ordenCompra = masterLineEnjoy.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 211:
                    var columbiaMaquinarias = new ColumbiaMaquinarias(pdfReader);
                    ordenCompra = columbiaMaquinarias.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 212:
                    var joyGlobal = new JoyGlobal(pdfReader);
                    ordenCompra = joyGlobal.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 213:
                    var ejercitodeChile = new EjercitodeChile(pdfReader);
                    ordenCompra = ejercitodeChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 214:
                    var saludValpoSanAntonio = new SaludValpoSanAntonio(pdfReader);
                    ordenCompra = saludValpoSanAntonio.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 215:
                    var larrainPrieto = new LarrainPrieto(pdfReader);
                    ordenCompra = larrainPrieto.GetOrdenCompra();
                    //ordenCompra.Rut = ordenCompra.Rut.Equals("805368002")? "80536800": ordenCompra.Rut;
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 216:
                    var brotecIcafal = new BrotecIcafal(pdfReader);
                    ordenCompra = brotecIcafal.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 217:
                    var agrosuper = new Agrosuper(pdfReader);
                    ordenCompra = agrosuper.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 218:
                    var traza = new Traza(pdfReader);
                    ordenCompra = traza.GetOrdenCompra();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 219:
                    var kripeos = new Kripeos(pdfReader);
                    ordenCompra = kripeos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 220:
                    var metalurgiaCaceres = new MetalurgiaCaceres(pdfReader);
                    ordenCompra = metalurgiaCaceres.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 221:
                    var insumosElAlto = new InsumosElAlto(pdfReader);
                    ordenCompra = insumosElAlto.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 222:
                    var tecnoera = new Tecnoera(pdfReader);
                    ordenCompra = tecnoera.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 223:
                    var sigro = new Sigro(pdfReader);
                    ordenCompra = sigro.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 224:
                    var circuloEjeLim = new CirculoEjecutivaLimitada(pdfReader);
                    ordenCompra = circuloEjeLim.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 225:
                    var nemoChile = new NemoChile(pdfReader);
                    ordenCompra = nemoChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 226:
                    var inmobiSanCarlos = new InmobiSanCarlos(pdfReader);
                    ordenCompra = inmobiSanCarlos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 227:
                    var bionet = new Bionet(pdfReader);
                    ordenCompra = bionet.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 228:
                    var eastonDesign = new EastonDesign(pdfReader);
                    ordenCompra = eastonDesign.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 229:
                    var proCircuit = new ProCircuit(pdfReader);
                    ordenCompra = proCircuit.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 230:
                    var avalChile = new AvalChile(pdfReader);
                    ordenCompra = avalChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 231:
                    var intertekCalleb = new IntertekCalleb(pdfReader);
                    ordenCompra = intertekCalleb.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 232:
                    var gardenHouse = new GardenHouse(pdfReader);
                    ordenCompra = gardenHouse.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 233:
                    var recordatorioArauco = new RecordatorioArauco(pdfReader);
                    ordenCompra = recordatorioArauco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 234:
                    var madement = new Madement(pdfReader);
                    ordenCompra = madement.GetOrdenCompra();
                    //ocAdapter = ordenCompra.TraspasoUltimateIntegracion();
                    break;
                case 235:
                    var komatsu2 = new Komatsu2(pdfReader);
                    ordenCompra = komatsu2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    foreach (var a in ocAdapter.DetallesCompra)
                    {
                        a.CodigoBodega = TipoBodega.ANTOFAGASTA;
                    }
                    break;
                case 236:
                    var unilever = new Unilever(pdfReader);
                    ordenCompra = unilever.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 237:
                    var uniDesarrollo = new UniDesarrollo(pdfReader);
                    ordenCompra = uniDesarrollo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 238:
                    var applus = new Applus(pdfReader);
                    ordenCompra = applus.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 239:
                    var paseoLasCondes = new PaseoLasCondes(pdfReader);
                    ordenCompra = paseoLasCondes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 240:
                    var tsm = new Tsm(pdfReader);
                    ordenCompra = tsm.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 241:
                    var salfa = new Salfa(pdfReader);
                    ordenCompra = salfa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    if (ocAdapter.CenCos == "-1")
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    }
                    break;
                case 242:
                    var ernstAndYoung = new ErnstAndYoung(pdfReader);
                    ordenCompra = ernstAndYoung.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 243:
                    var corporacionCulturalVitacura = new CorporacionCulturalVitacura(pdfReader);
                    ordenCompra = corporacionCulturalVitacura.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 244:
                    var ingenieriaCivilVicente = new IngenieriaCivilVicente(pdfReader);
                    ordenCompra = ingenieriaCivilVicente.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracionNuevoFlujo(pdfReader);
                    break;
                case 245:
                    var novoFarma = new NovoFarma(pdfReader);
                    ordenCompra = novoFarma.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 300:
                    var redSaludPuc = new RedSaludPuc(pdfReader);
                    ordenCompra = redSaludPuc.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    //ordenCompra.ConsoleWriteLine();
                    //return;
                    break;
                case 301:
                    var flores = new Flores(pdfReader);
                    ordenCompra = flores.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 302:
                    var vicentePlasticos = new VicentePlasticos(pdfReader);
                    ordenCompra = vicentePlasticos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 303:
                    var cptRemolcadores = new CptRemolcadores(pdfReader);
                    ordenCompra = cptRemolcadores.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 304:
                    var valkoMaquinaria = new ValkoMaquinaria(pdfReader);
                    ordenCompra = valkoMaquinaria.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 305:
                    var workPlaces = new Workplaces(pdfReader);
                    ordenCompra = workPlaces.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 306:
                    var isa = new Isa(pdfReader);
                    ordenCompra = isa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                    }
                    break;
                case 307:
                    var cynersis = new Cynersis(pdfReader);
                    ordenCompra = cynersis.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 308:
                    var bciAsset = new BciAsset(pdfReader);
                    ordenCompra = bciAsset.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 309:
                    var alfredK = new AlfredK(pdfReader);
                    ordenCompra = alfredK.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 310:
                    var puc = new Puc(pdfReader);
                    ordenCompra = puc.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 311:
                    var clinicaLasCondes = new ClinicaLasCondes(pdfReader);
                    ordenCompra = clinicaLasCondes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 313:
                    var ideasEducativas = new IdeasEducativas(pdfReader);
                    ordenCompra = ideasEducativas.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 314:
                    var enim = new Enim(pdfReader);
                    ordenCompra = enim.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 315:
                    var soluCorp = new Solucorp(pdfReader);
                    ordenCompra = soluCorp.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 316:
                    var codelco = new Codelco(pdfReader);
                    ordenCompra = codelco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 317:
                    var losCarrera = new LosCarrera(pdfReader);
                    ordenCompra = losCarrera.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 318:
                    var ingenieriaServiciosAlcaino = new IngenieriaServiciosAlcaino(pdfReader);
                    ordenCompra = ingenieriaServiciosAlcaino.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 319:
                    var promasa = new Promasa(pdfReader);
                    ordenCompra = promasa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 320:
                    var dialisan = new Dialisan(pdfReader);
                    ordenCompra = dialisan.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 321:
                    var ingenalse = new Ingenalse(pdfReader);
                    ordenCompra = ingenalse.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 322:
                    var cajaLosAndes = new CajaLosAndes(pdfReader);
                    ordenCompra = cajaLosAndes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 323:
                    var tecnored = new Tecnored(pdfReader);
                    ordenCompra = tecnored.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 324:
                    var mts2 = new Mts2(pdfReader);
                    ordenCompra = mts2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 325:
                    var correosChile = new CorreosChile(pdfReader);
                    ordenCompra = correosChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 326:
                    var precon = new Precon(pdfReader);
                    ordenCompra = precon.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 327:
                    var conventoViejoCoti = new ConventoViejoCoti(pdfReader);
                    ordenCompra = conventoViejoCoti.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 328:
                    var constructoraSudamericana = new ConstructoraSudamericana(pdfReader);
                    ordenCompra = constructoraSudamericana.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 329:
                    var udp = new Udp(pdfReader);
                    var separaBle = udp.GetOrdenCompra();
                    var list = separaBle.GetAllOrdenCompra();
                    foreach (var orden in list)
                    {
                        Console.WriteLine(orden);
                        ocAdapter = orden.TraspasoUltimateIntegracion(pdfReader);
                        ExecutePdfPostProcess(option, pdfReader, orden, ocAdapter, ocAdapterList);
                    }
                    return;

                case 330:
                    var krontec = new Krontec(pdfReader);
                    ordenCompra = krontec.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 331:
                    var clisanjose = new ClinicaSanJose(pdfReader);
                    ordenCompra = clisanjose.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                /* case 332:
                     var excon = new Excon(pdfReader);
                     ordenCompra = excon.GetOrdenCompra();
                     ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                     break;*/
                case 333:
                    var masisa = new Masisa(pdfReader);
                    ordenCompra = masisa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 334:
                    var proandes = new ProAndes(pdfReader);
                    ordenCompra = proandes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 335:
                    var gildemeister = new Gildemeister(pdfReader);
                    ordenCompra = gildemeister.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 336:
                    var artikos = new Artikos(pdfReader);
                    ordenCompra = artikos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 500:
                    var jcIngenieriaSpa = new JcIngenieriaSpa(pdfReader);
                    ordenCompra = jcIngenieriaSpa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 501:
                    var andacor = new Andacor(pdfReader);
                    ordenCompra = andacor.GetOrdenCompra();
                    break;
                case 502:
                    var ingenieriaEVH = new IngenieriaEVH(pdfReader);
                    ordenCompra = ingenieriaEVH.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 503:
                    var pruebaricardo = new PruebaRicardo(pdfReader);
                    ordenCompra = pruebaricardo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 504:
                    var zublin = new ZublinChuquicamata(pdfReader);
                    ordenCompra = zublin.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 505:
                    var institutoDeFomentoPesqueroCoti = new InstitutoDeFomentoPesquero(pdfReader);
                    ordenCompra = institutoDeFomentoPesqueroCoti.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 506:
                    var kibernum = new Kibernum(pdfReader);
                    ordenCompra = kibernum.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 507:
                    var cotizacionDimerc = new CotizacionDimerc(pdfReader);
                    ordenCompra = cotizacionDimerc.GetOrdenCompra();
                    if (OracleDataAccess.GetCodCNLByRutusu(OracleDataAccess.GetRutUsuarioFromRutCliente(ordenCompra.Rut)).Equals("516")) {
                        ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    }
                    else
                    {
                        return;
                    }
                    break;
                case 508:
                    var consorcioExcon = new ConsorcioExcon(pdfReader);
                    ordenCompra = consorcioExcon.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 509:
                    var clinicaSantaMaria = new ClinicaSantaMaria(pdfReader);
                    ordenCompra = clinicaSantaMaria.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 510:
                    var inmobiliariaChillanSpa = new InmobiliariaChillanSpa(pdfReader);
                    ordenCompra = inmobiliariaChillanSpa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 511:
                    var construmart = new Construmart(pdfReader);
                    ordenCompra = construmart.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 512:
                    var construmart2 = new Construmart2(pdfReader);
                    ordenCompra = construmart2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 513:
                    var rtc = new RTC(pdfReader);
                    ordenCompra = rtc.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 514:
                    var tepsac = new Tepsac(pdfReader);
                    ordenCompra = tepsac.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 515:
                    var byc = new ConstructoraBYC(pdfReader);
                    ordenCompra = byc.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 516:
                    var resiter = new Resiter(pdfReader);
                    ordenCompra = resiter.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 517:
                    var echeverriaIzquierdo = new EcheverriaIzquierdo(pdfReader);
                    ordenCompra = echeverriaIzquierdo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 518:
                    var aplLogistics = new AplLogistics(pdfReader);
                    ordenCompra = aplLogistics.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 519:
                    var arrigoni = new Arrigoni(pdfReader);
                    ordenCompra = arrigoni.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 520:
                    var ciaPisqueraChile = new CiaPisqueraChile(pdfReader);
                    ordenCompra = ciaPisqueraChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 521:
                    var socovesa = new Socovesa(pdfReader);
                    ordenCompra = socovesa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 522:
                    var vertiv = new Vertiv(pdfReader);
                    ordenCompra = vertiv.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 523:
                    var piberries = new PiBerries(pdfReader);
                    ordenCompra = piberries.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 524:
                    var cpp = new Cpp(pdfReader);
                    ordenCompra = cpp.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 525:
                    var tti = new TrainTransportesIntegrados(pdfReader);
                    ordenCompra = tti.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 526:
                    var numair = new ConstructuraNumair(pdfReader);
                    ordenCompra = numair.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 527:
                    var acWorkplaces = new ArquitecturayConstrucciónWorkplacesSPA(pdfReader);
                    ordenCompra = acWorkplaces.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 528:
                    var clinicaReñaca = new ClinicaReñaca(pdfReader);
                    ordenCompra = clinicaReñaca.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 529:
                    var kunstmann = new Kunstmann(pdfReader);
                    ordenCompra = kunstmann.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 530:
                    var besalco = new Besalco(pdfReader);
                    ordenCompra = besalco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 531:
                    var ucsh = new UniversidadCatolicaSilvaHenriquez(pdfReader);
                    ordenCompra = ucsh.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 532:
                    var chileInox = new ChileInox(pdfReader);
                    ordenCompra = chileInox.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 533:
                    var metro = new Metro(pdfReader);
                    ordenCompra = metro.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 534:
                    var jcIng = new JcIngenieriaSpa2(pdfReader);
                    ordenCompra = jcIng.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 535:
                    var walmart = new Walmart(pdfReader);
                    ordenCompra = walmart.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                /* case 535:
                     var jcIng = new JcIngenieriaSpa2(pdfReader);
                     ordenCompra = jcIng.GetOrdenCompra();
                     ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                     break;*/
                case 536:
                    var bancoSecurity = new BancoSecurity(pdfReader);
                    ordenCompra = bancoSecurity.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 537:
                    var metrogas = new Metrogas(pdfReader);
                    ordenCompra = metrogas.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 538:
                    var hdi = new Hdi(pdfReader);
                    var ordenesCompra = hdi.GetOrdenCompra();
                    foreach (var oc in ordenesCompra)
                    {

                        //ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                        ocAdapter = oc.TraspasoUltimateIntegracion(pdfReader);
                        ocAdapterList.Add(ocAdapter);
                    }
                    ExecutePdfPostProcessHDI(option, pdfReader, ordenesCompra[0], ocAdapterList[0], ocAdapterList);
                    postProcess = true;
                    //ExecutePdfPostProcess(option, pdfReader, ordenCompra, ocAdapter, ocAdapterList);
                    /*if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;
                        ocAdapter.Observaciones = $"{ordenCompra.CentroCosto}";
                    }*/
                    break;
                case 539:
                    var achs = new Achs(pdfReader);
                    ordenCompra = achs.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 541:
                    var besalco2 = new Besalco2(pdfReader);
                    ordenCompra = besalco2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 542:
                    var bsf = new BodegaSanFrancisco(pdfReader);
                    ordenCompra = bsf.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 543:

                    var telefonicaIngSeguridad = new TelefonicaIngSeguridad(pdfReader);
                    ordenCompra = telefonicaIngSeguridad.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 544:

                    var puertoCentral = new PuertoCentral(pdfReader);
                    ordenCompra = puertoCentral.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 545:
                    var steelFerrovialSenegocia = new SteelFerrovialSenegocia(pdfReader);
                    ordenCompra = steelFerrovialSenegocia.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 546:
                    var steelFerrovial1 = new SteelFerrovial1(pdfReader);
                    ordenCompra = steelFerrovial1.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 547:
                    var insi = new SteelFerrovial1(pdfReader);
                    ordenCompra = insi.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 548:
                    var dictuc = new Dictuc(pdfReader);
                    ordenCompra = dictuc.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 549:
                    var puenteSurGtr = new PuenteSurGTR(pdfReader);
                    ordenCompra = puenteSurGtr.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 550:
                    var araucanaSalud = new LaAraucanaSalud(pdfReader);
                    ordenCompra = araucanaSalud.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 551:
                    var ferricom = new Ferricom(pdfReader);
                    ordenCompra = ferricom.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 552:
                    var craIngenieria = new CraIngenieria(pdfReader);
                    ordenCompra = craIngenieria.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 553:
                    var arauconew = new AraucoNew(pdfReader);
                    ordenCompra = arauconew.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 554:
                    var kreisspa = new KreisSpa(pdfReader);
                    ordenCompra = kreisspa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 555:
                    var happyland = new Happyland(pdfReader);
                    ordenCompra = happyland.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 556:
                    var ucCSA = new UcChristusServiciosAmbulatorios(pdfReader);
                    ordenCompra = ucCSA.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 557:
                    var moller = new Moller(pdfReader);
                    ordenCompra = moller.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 558:

                    var aridos = new AridosArenex(pdfReader);
                    ordenCompra = aridos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 559:

                    var precon2 = new Precon2(pdfReader);
                    ordenCompra = precon2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 560:

                    var prefpretohormigon = new PrefabricadosPrensadosHormigon(pdfReader);
                    ordenCompra = prefpretohormigon.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 561:

                    var harting = new Harting(pdfReader);
                    ordenCompra = harting.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 562:
                    var ironMountain = new IronMountain(pdfReader);
                    ordenCompra = ironMountain.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 563:
                    var clarovicunavalenzuela = new ClaroVicunaValenzuela(pdfReader);
                    ordenCompra = clarovicunavalenzuela.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;


                case 565:
                    var cereales = new CerealesCPWChileLtda(pdfReader);
                    ordenCompra = cereales.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 566:
                    var aiaspa = new ArquitecturaIngenieriaAconcagua(pdfReader);
                    ordenCompra = aiaspa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 567:
                    var eeaysen = new EmpresaElectricaAysen(pdfReader);
                    ordenCompra = eeaysen.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 568:
                    //var laspa = new LanguesAffairsSpa(pdfReader);
                    var laspa = new LanguesAffairsSpa2(pdfReader);

                    ordenCompra = laspa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.Observaciones += $" {ocAdapter.CenCos}";
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;

                    }
                    break;
                case 569:
                    var tresmontes = new TresMontes(pdfReader);
                    ordenCompra = tresmontes.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    if (ocAdapter.CenCos.Equals("-1") || ocAdapter.CenCos.Equals(""))
                    {
                        ocAdapter.Observaciones += $" {ocAdapter.CenCos}";
                        ocAdapter.CenCos = "0";
                        ocAdapter.EstadoOrden = EstadoOrden.POR_PROCESAR;

                    }
                    break;
                case 570:
                    var ironMountain2 = new IronMountain2(pdfReader);
                    ordenCompra = ironMountain2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 571:
                    var ccu = new ComercialCCU(pdfReader);
                    ordenCompra = ccu.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 572:
                    var bicecs = new BiceVidaCompSeg(pdfReader);
                    ordenCompra = bicecs.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 573:
                    var emerson = new EmersonElectric(pdfReader);
                    ordenCompra = emerson.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 574:
                    var sandvik2 = new Sandvik(pdfReader);
                    ordenCompra = sandvik2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 575:
                    var cftStoTomas = new CFTSantoTomas(pdfReader);
                    ordenCompra = cftStoTomas.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 576:
                    var inmobilia = new Inmobilia(pdfReader);
                    ordenCompra = inmobilia.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 577:
                    var asaping = new AsapIngenieria(pdfReader);
                    ordenCompra = asaping.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 578:
                    var labxp = new LaboratorioXimenaPolanco(pdfReader);
                    ordenCompra = labxp.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 579:
                    var siena = new ConstructoraSiena(pdfReader);
                    ordenCompra = siena.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 580:
                    var amachicura = new AgroIndustrialMachicura(pdfReader);
                    ordenCompra = amachicura.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 581:
                    var soprodi = new Soprodi(pdfReader);
                    ordenCompra = soprodi.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 582:
                    var cicf = new Constructora_ICF(pdfReader);
                    ordenCompra = cicf.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 583:

                    break;
                case 584:
                    var cclass = new ConstructoraClass(pdfReader);
                    ordenCompra = cclass.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 585:
                    var cpt = new CptRemolcadores2(pdfReader);
                    ordenCompra = cpt.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 586:
                    var capital = new CapitalSA(pdfReader);
                    ordenCompra = capital.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 587:
                    var nogales = new InmobiliariaNogales(pdfReader);
                    ordenCompra = nogales.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 588:
                    var cpchicureo = new ConstructoraParqueChicureo(pdfReader);
                    ordenCompra = cpchicureo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 589:
                    var comercialKendall = new ComercialKendall(pdfReader);
                    ordenCompra = comercialKendall.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 590:
                    var udp2 = new UniversidadDiegoPortales(pdfReader);
                    ordenesCompra = udp2.GetOrdenCompra();

                    foreach (var oc in ordenesCompra)
                    {
                        ocAdapter = oc.TraspasoUltimateIntegracion(pdfReader);
                        ocAdapterList.Add(ocAdapter);
                    }
                    ExecutePdfPostProcessUDP(option, pdfReader, ordenesCompra[0], ocAdapterList[0], ocAdapterList);
                    postProcess = true;

                    break;
                case 591:
                    var cclsltda = new CoopeativaAgricolaLecheraSantiagoLtda(pdfReader);
                    ordenCompra = cclsltda.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 592:
                    var mercadopublico = new Mercadopublico(pdfReader);
                    ordenCompra = mercadopublico.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 593:
                    var inmobiliariaLosOlmos = new InmobiliariaLosOlmos(pdfReader);
                    ordenCompra = inmobiliariaLosOlmos.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

                case 594:
                    var telefonica = new TelefonicaIngDeSeguridad(pdfReader);
                    ordenCompra = telefonica.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 595:
                    var clinicaColonial = new ClinicaColonial(pdfReader);
                    ordenCompra = clinicaColonial.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 596:
                    var latam = new Latam(pdfReader);
                    ordenCompra = latam.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 597:
                    var emer = new Emerson(pdfReader);
                    ordenCompra = emer.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 598:
                    var htg = new HTGConstrucciones(pdfReader);
                    ordenCompra = htg.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 599:
                    var bci = new BciArtikos(pdfReader);
                    ordenCompra = bci.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 601:
                    //INMOBILIARIA NUEVO PUENTE ALTO S.A
                    var inpasa = new InmobiliariaNuevoPuenteAltoSA(pdfReader);
                    ordenCompra = inpasa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 602:
                    var easa = new EmbotelladoraAndinaSA(pdfReader);
                    ordenCompra = easa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 603:
                    var reliper = new Reliper(pdfReader);
                    ordenCompra = reliper.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 604:
                    var sigaing = new SIGAIng(pdfReader);
                    ordenCompra = sigaing.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 605:
                    var drillco = new Drillco(pdfReader);
                    ordenCompra = drillco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 606:
                    var jschmidt = new JorgeSchmidt(pdfReader);
                    ordenCompra = jschmidt.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 607:
                    var corpedusal = new CorporacionEducacionSalud(pdfReader);
                    ordenCompra = corpedusal.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 608:
                    var archroma2 = new Archroma2(pdfReader);
                    ordenCompra = archroma2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 609:
                    var belfi = new BelfiIncolur(pdfReader);
                    ordenCompra = belfi.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 610:
                    var ingyconssantafe = new IngConSantaFe(pdfReader);
                    ordenCompra = ingyconssantafe.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);

                    break;
                case 611:
                    var sam = new ServiciosyAbastecimientosMedicos(pdfReader);
                    ordenCompra = sam.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 612:
                    var consorci = new Consorciopdf(pdfReader);
                    ordenCompra = consorci.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 613:
                    var chilematSpa = new ChilematSpa(pdfReader);
                    ordenCompra = chilematSpa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 614:
                    var craMontajesSpa = new CraMontajesSpa(pdfReader);
                    ordenCompra = craMontajesSpa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 615:
                    var fedSantaMaria = new FedericoSantaMaria(pdfReader);
                    ordenCompra = fedSantaMaria.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 616:
                    var tsm2 = new TransportesSantaMariaSpa(pdfReader);
                    ordenCompra = tsm2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 617:
                    var canplassud = new CanplasSud(pdfReader);
                    ordenCompra = canplassud.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 618:
                    var ucentral = new UniversidadCentral(pdfReader);
                    ordenCompra = ucentral.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 619:
                    var ingevec2 = new Ingevec2(pdfReader);
                    ordenCompra = ingevec2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 620:
                    var inacap = new Inacap(pdfReader);
                    ordenCompra = inacap.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 621:
                    var g4s = new G4S(pdfReader);
                    ordenCompra = g4s.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 622:
                    var constructoraDVC = new ConstructoraDVC(pdfReader);
                    ordenCompra = constructoraDVC.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 623:
                    var inmobAconcagua = new InmobiliariaAconcagua(pdfReader);
                    ordenCompra = inmobAconcagua.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 624:
                    var megalogistica = new Megalogistica(pdfReader);
                    ordenCompra = megalogistica.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 625:
                    var arcoprime = new Arcoprime(pdfReader);
                    ordenCompra = arcoprime.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 626:
                    var ariztia = new Ariztia(pdfReader);
                    ordenCompra = ariztia.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 627:
                    var cooperativaRegional = new CooperativaRegionalElectrica(pdfReader);
                    ordenCompra = cooperativaRegional.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 629:
                    var hogarDeCristo = new HogarDeCristo(pdfReader);
                    ordenCompra = hogarDeCristo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 630:
                    var zublinInternacional = new ZublinInternacional(pdfReader);
                    ordenCompra = zublinInternacional.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 631:
                    var avis = new Avis(pdfReader);
                    ordenCompra = avis.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 632:
                    var elabra = new ElAbra(pdfReader);
                    ordenCompra = elabra.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 633:
                    var clinicaAleValdivia = new ClinicaAlemanaValdivia(pdfReader);
                    ordenCompra = clinicaAleValdivia.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 634:
                    var nexxo = new Nexxo(pdfReader);
                    ordenCompra = nexxo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 635:
                    var compass = new Compass(pdfReader);
                    ordenCompra = compass.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 636:
                    var bhp_2 = new BHP_2(pdfReader);
                    ordenCompra = bhp_2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 637:
                    var g4s_2 = new G4S_2(pdfReader);
                    ordenCompra = g4s_2.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 638:
                    var camanchaca = new Camanchaca(pdfReader);
                    ordenCompra = camanchaca.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 639:
                    var omesa = new Omesa(pdfReader);
                    ordenCompra = omesa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 640:
                    var gsk = new GSK(pdfReader);
                    ordenCompra = gsk.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 641:
                    var corpora = new Corpora(pdfReader);
                    ordenCompra = corpora.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 642:
                    var integramedicaNF = new IntegramedicaNuevoFormato(pdfReader);
                    ordenCompra = integramedicaNF.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 643:
                    var ilsauspe = new Ilsuaspe(pdfReader);
                    ordenCompra = ilsauspe.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 644:
                    var engie = new Engie(pdfReader);
                    ordenCompra = engie.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 645:
                    var sodexo = new Sodexo(pdfReader);
                    ordenCompra = sodexo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 646:
                    var ust = new UST(pdfReader);
                    ordenCompra = ust.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 647:
                    var nephrocare = new Nephrocare(pdfReader);
                    ordenCompra = nephrocare.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 648:
                    var gruponorteseguridad = new Gruponorteseguridad(pdfReader);
                    ordenCompra = gruponorteseguridad.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 649:
                    var isapreCruzBlanca = new IsapreCruzBlanca(pdfReader);
                    ordenCompra = isapreCruzBlanca.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 650:
                    var nephrocareNuevo = new NephrocareNuevo(pdfReader);
                    ordenCompra = nephrocareNuevo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 652:
                    var vulco = new Vulco(pdfReader);
                    ordenCompra = vulco.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 653:
                    var emaresa = new Emaresa(pdfReader);
                    ordenCompra = emaresa.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 654:
                    var soho = new Soho(pdfReader);
                    ordenCompra = soho.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                //case 655:
                //    var promasa = new Promasa(pdfReader);
                //    ordenCompra = promasa.GetOrdenCompra();
                //    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                //    break;
                case 656:
                    var consorcioSeguros = new ConsorcioSeguros(pdfReader);
                    ordenCompra = consorcioSeguros.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 658:
                    var aquaChile = new AquaChile(pdfReader);
                    ordenCompra = aquaChile.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 659:
                    var sacyr = new Sacyr(pdfReader);
                    ordenCompra = sacyr.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 660:
                    var anasac = new Anasac(pdfReader);
                    ordenCompra = anasac.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 661:
                    var watts = new Watts(pdfReader);
                    ordenCompra = watts.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 662:
                    var portalPUC = new PortalPUC(pdfReader);
                    ordenCompra = portalPUC.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 663:
                    var zara = new Zara(pdfReader);
                    ordenCompra = zara.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 664:
                    var latinGaming = new LatinGaming(pdfReader);
                    ordenCompra = latinGaming.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 665:
                    var fedexNuevo = new FedexNuevoFormato(pdfReader);
                    ordenCompra = fedexNuevo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 666:
                    var areasSA = new AreasSA(pdfReader);
                    ordenCompra = areasSA.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 667:
                    var fundacionLasRosas = new FundacionLasRosas(pdfReader);
                    ordenCompra = fundacionLasRosas.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 668:
                    var fluitek = new Fluitek(pdfReader);
                    ordenCompra = fluitek.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;
                case 669:
                    var voyStgo = new VoySantiago(pdfReader);
                    ordenCompra = voyStgo.GetOrdenCompra();
                    ocAdapter = ordenCompra.TraspasoUltimateIntegracion(pdfReader);
                    break;

            }
            /*if (option == -1)
            {
                OracleDataAccess.NumpedArchivoAdjunto(0,pdfReader.PdfFileName);
            }*/

            #endregion
            //Console.WriteLine($"PostProcess:{option} \nOC:\n{ordenCompra.ToString()}\nOC_ADAPTER:{ocAdapter}");
            if (!postProcess)
            {
                ExecutePdfPostProcess(option, pdfReader, ordenCompra, ocAdapter, ocAdapterList);
            }
            OracleDataAccess.TraspasaTelemarketingSeparados();
            /*OracleDataAccess.NumpedArchivoAdjunto(Convert.ToInt32(ocAdapter.NumPed), pdfReader.PdfFileName);*/
        }

        #region ExecutePdfPostProcess

        private static void ExecutePdfPostProcessHDI(int option,PDFReader pdfReader, OrdenCompra ordenCompra, OrdenCompraIntegracion ocAdapter, List<OrdenCompraIntegracion> ocAdapterList)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if (ocAdapter != null && ocAdapter.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapter);
                if (ocAdapterList != null)
                {
                    foreach (var ocAdap in ocAdapterList)
                        Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdap);
                }
            }

            var totalResult = true;
            foreach (var ocIntegracion in ocAdapterList)
            {
                //ocIntegracion.CenCos = "9999";
                if (!OracleDataAccess.InsertOrdenCompraIntegracion(ocIntegracion, pdfReader.PdfPath, pdfReader))
                {
                    totalResult = false;
                }
            }
            var pdfName = FileUtils.MoveFileToProcessFolder(pdfReader.PdfPath, ordenCompra, ocAdapterList[0], OnlyOne);


        }

        private static void ExecutePdfPostProcessUDP(int option, PDFReader pdfReader, OrdenCompra ordenCompra, OrdenCompraIntegracion ocAdapter, List<OrdenCompraIntegracion> ocAdapterList)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if (ocAdapter != null && ocAdapter.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapter);
                if (ocAdapterList != null)
                {
                    foreach (var ocAdap in ocAdapterList)
                        Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdap);
                }
            }

            var totalResult = true;
            foreach (var ocIntegracion in ocAdapterList)
            {
                //ocIntegracion.CenCos = "9999";
                if (!OracleDataAccess.InsertOrdenCompraIntegracion(ocIntegracion, pdfReader.PdfPath, pdfReader))
                {
                    totalResult = false;
                }
            }
            var pdfName = FileUtils.MoveFileToProcessFolder(pdfReader.PdfPath, ordenCompra, ocAdapterList[0], OnlyOne);


        }

        private static void ExecutePdfPostProcess(int option, PDFReader pdfReader, OrdenCompra ordenCompra, OrdenCompraIntegracion ocAdapter, List<OrdenCompraIntegracion> ocAdapterList)
        {
            if (InternalVariables.IsDebug())
            {
                Console.WriteLine($"PostProcess:{option} \nOC:\n" + ordenCompra);
                if(ocAdapter!= null && ocAdapter.DetallesCompra.Count > 0)
                    Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdapter);
                if (ocAdapterList != null)
                {
                    foreach(var ocAdap in ocAdapterList)
                        Console.WriteLine($"PostProcess:{option} \nOC_ADAPTER:\n" + ocAdap);
                }
            }
            if (option != -1)
            {
                if (option == 4 || option == 10
                    || option == 12
                    || option == -8
                    || option == 21 || option == 22
                    || option == 31)
                {
                    var totalResult = true;
                    foreach (var ocIntegracion in ocAdapterList)
                    {
                        if (!OracleDataAccess.InsertOrdenCompraIntegracion(ocIntegracion, pdfReader.PdfPath, pdfReader))
                        {
                            totalResult = false;
                        }
                    }
                    if (!totalResult)
                    {
                        ThrowError.ThrowInsertError(pdfReader.PdfFileName);
                    }
                    else
                    {
                        if (ordenCompra != null)
                        {
                            Log.Save($"Orden N°: {ordenCompra.NumeroCompra}, " +
                                     $"de: {InternalVariables.PdfFormats[option == -8 ? 8 : option]}, " +
                                     "procesada exitosamente...");
                            foreach (var ocIntegracion in ocAdapterList)
                                Log.AddMailUpdateTelemarketing(ocIntegracion);
                            var pdfTextOracle = pdfReader.ExtractTextFromPdfToStringToOracle();
                            var pdfName = FileUtils.MoveFileToProcessFolder(pdfReader.PdfPath, ordenCompra, ocAdapterList[0], OnlyOne);
                            //OracleDataAccess.InsertRawPdfText(option, ocAdapter, pdfName, pdfTextOracle, ordenCompra, true);
                        }
                        else if (option == 21 || option == 22 || option == 31 || option == -8)
                        {
                            Console.WriteLine("===================================");
                            ordenCompra = new OrdenCompra { Rut = ocAdapterList[0].RutCli.ToString() };
                            if (option == -8) //DAVILA ANEXO
                            {
                                var clinicaDavila = new ClinicaDavila(pdfReader);
                                if (clinicaDavila.HaveAnexo())
                                {
                                    Console.WriteLine("================MOVE 1==================");
                                    FileUtils.MoveFileToProcessFolder(clinicaDavila.AnexoPath, ordenCompra, ocAdapterList[0], OnlyOne);
                                }
                            }

                            var pdfTextOracle = pdfReader.ExtractTextFromPdfToStringToOracle();
                            Console.WriteLine("================MOVE 2==================");
                            var md5 = FileUtils.GetMD5FromFile(pdfReader.PdfPath);
                            var pdfName = FileUtils.MoveFileToProcessFolder(pdfReader.PdfPath, ordenCompra, ocAdapterList[0], OnlyOne);
                            OracleDataAccess.InsertRawPdfText(option, ocAdapter, pdfName, pdfTextOracle, ordenCompra, true, md5);
                            foreach (var ocIntegracion in ocAdapterList)
                            {
                                Log.Save($"Orden N°: {ocIntegracion.OcCliente}, " +
                                    $"de: {InternalVariables.PdfFormats[option == -8 ? 8 : option]}, " +
                                    "procesada exitosamente...");
                                Log.AddMailUpdateTelemarketing(ocIntegracion);
                            }
                        }
                    }
                }
                else // SOLO UN ocAdapter
                {
                    if (ordenCompra != null)
                    {
                        OracleDataAccess.InsertRawOrdenCompra(option, ordenCompra, ocAdapter.NumPed);
                    }
                    if (OracleDataAccess.InsertOrdenCompraIntegracion(ocAdapter, pdfReader.PdfPath, pdfReader))
                    {

                        Console.WriteLine("ELSE");
                        if (option == 28) //VITAMINA WORK LIFE
                        {
                            if (File.Exists(pdfReader.PdfPath.Replace("OC_Nro", "OCDistribucion_Nro")))
                            {
                                Console.WriteLine("================MOVE 3==================");
                                FileUtils.MoveFileToProcessFolder(
                                    pdfReader.PdfPath.Replace("OC_Nro", "OCDistribucion_Nro")
                                    , ordenCompra
                                    , ocAdapter
                                    , OnlyOne);
                                Console.WriteLine("");
                            }
                            else if (File.Exists(pdfReader.PdfPath.Replace(".pdf", "_Resumen.pdf")))
                            {
                                Console.WriteLine("================MOVE 4.0==================");
                                FileUtils.MoveFileToProcessFolder(
                                    pdfReader.PdfPath.Replace(".pdf", "_Resumen.pdf")
                                    , ordenCompra
                                    , ocAdapter
                                    , OnlyOne);
                            }
                            else
                            {
                                Console.WriteLine("================MOVE 4.1==================");
                                FileUtils.MoveFileToProcessFolder(
                                    pdfReader.PdfPath
                                    , ordenCompra
                                    , ocAdapter
                                    , OnlyOne);
                            }
                        }
                        else if (option == 8) //DAVILA ERROR
                        {
                            Console.WriteLine("================MOVE 4==================");
                            var pdfTextOracle = pdfReader.ExtractTextFromPdfToStringToOracle();
                            var md5 = FileUtils.GetMD5FromFile(pdfReader.PdfPath);
                            var pdfName = FileUtils.MoveFileToProcessFolder(pdfReader.PdfPath, ordenCompra, ocAdapter, OnlyOne);
                            OracleDataAccess.InsertRawPdfText(option, ocAdapter, pdfName, pdfTextOracle, ordenCompra, false, md5);
                        }
                        else
                        {
                            var pdfTextOracle = pdfReader.ExtractTextFromPdfToStringToOracle();
                            Console.WriteLine("================MOVE 5==================");
                            var md5 = FileUtils.GetMD5FromFile(pdfReader.PdfPath);
                            var pdfName = FileUtils.MoveFileToProcessFolder(pdfReader.PdfPath, ordenCompra, ocAdapter, OnlyOne);
                            Console.WriteLine("");
                            OracleDataAccess.InsertRawPdfText(option, ocAdapter, pdfName, pdfTextOracle, ordenCompra, false, md5);
                        }
                        Log.Save($"Orden N°: {ocAdapter.OcCliente}, " +
                                 $"de: {InternalVariables.PdfFormats[option]}, " +
                                 "procesada exitosamente...");
                        Log.AddMailUpdateTelemarketing(ocAdapter);
                    }
                    else
                    {
                        ThrowError.ThrowInsertError(pdfReader.PdfFileName);
                    }
                }
            }
            else
            {
                //Console.Write("UnknownFile");
                FileUtils.SaveUnknownFile(pdfReader.PdfPath);
                ThrowError.ThrowFormatError(pdfReader.PdfFileName, pdfReader.PdfPath);
                //Console.WriteLine("================MOVE 6==================");
                //FileUtils.SaveUnknownFile(pdfReader.PdfPath);
            }
        }

       #endregion
        

        #region ReaderPdfOrderFromRootDirectory

        public static void ExecuteLecturaPdf()
        {
            if (Main.STATE != Main.AppState.INACTIVO)
            {
                ParseExt.Log.Debug("Can Execute PDF Module, because the MAIN_STATE is ACTIVE...");
                return;
            }
            Main.STATE = Main.AppState.ANALIZANDO_ORDENES_PDF;
            OnlyOne = false;
            if (!OracleDataAccess.TestConexion()) return;
            if (InternalVariables.CountCriticalError >= 3)
            {
                if (InternalVariables.CountSendErrorAlert >= 5) return;
                InternalVariables.CountSendErrorAlert++;
                ThrowError.SendAlertError();
                return;
            }
            FileUtils.MoveUnknownFile();
            FileUtils.MoveErrorFile();
            Main.InitializerAnalysis();
            var count = 0;
           
            /*
             *  PDF de Carpeta 'Procesar'
             */
            foreach (var pdfReader in Directory
                .GetFiles(@InternalVariables.GetOcAProcesarFolder(), "*.pdf").
                Select(pdfPath => new PDFReader(pdfPath))
                .Where(pdfReader => !pdfReader.PdfFileName.Contains("OCDistribucion_Nro_")
                        && !(pdfReader.PdfFileName.Contains("ANEXO_OC_"))
                        && !(pdfReader.PdfFileName.Contains("RECEPCION_DOCUMENTOS_INSTRUCTIVO_PROVEEDORES"))
                        &&
                        !(pdfReader.PdfFileName.Contains("OC_Nro_")     
                        && pdfReader.PdfFileName.Contains("_Resumen.pdf"))))
            
                        //&& !pdfReader.PdfFileName.Contains("<<<<<<<")))
            {
                try
                {
                    ExecuteSinglePdf(pdfReader);
                    count++;
                }
                catch (ErrorIntegracion integracionError)
                {
                    Log.TryError(integracionError.Mensaje);
                    Log.SendMailErrorEjecutivos(integracionError);
                }
                catch (Exception e)
                {
                    Console.WriteLine("=====================================");
                    Console.WriteLine("\t\tPROCESAR NORMAL");
                    Console.WriteLine(e.ToString());
                    Console.WriteLine("=====================================");
                    ThrowError.ThrowAnalysisError(pdfReader, e);
                    FileUtils.SaveErrorFile(pdfReader.PdfPath);
                }
            }
            Main.FinishAnalysis(count);
            FlagClose.pdf = true;
        }


        #endregion

        #region GetOption PDF
        /// <summary>
        /// Optiene el Identificador del Formato de la Orden de Compra a Procesar
        /// </summary>
        /// <param name="pdfReader">PDF Reader</param>
        /// <returns>Identificador del Formato de la Orden</returns>
        private static int GetPdfOptionNumber(PDFReader pdfReader)
        {
            var onlyOneLine = pdfReader.ExtractTextFromPdfToString();
            var first = -1;
            foreach (var form in InternalVariables.PdfFormats)
            {
                if (form.Value.Contains(";"))
                {
                    var split = form.Value.Split(';');
                    var match = split.Count(sp => onlyOneLine.Contains(sp));
                    Console.WriteLine(onlyOneLine);
                    if (match == split.Count())
                    {
                        first = form.Key;
                        break;
                    }
                    //if (onlyOneLine.Contains(split[0]) &&
                    //    onlyOneLine.Contains(split[1]))
                    //{
                    //    first = form.Key;
                    //    break;
                    //}
                }else if (form.Value.Contains(":"))
                {
                    var split = form.Value.Split(':');
                    if (split.Any(sp => onlyOneLine.Contains(sp)))
                    {
                        first = form.Key;
                    }
                }else if (onlyOneLine.Contains(form.Value))
                {
                    first = form.Key;
                    break;
                }
            }
            //foreach (var format in PdfFormats.Where(format => onlyOneLine.Contains(format.Value)))
            //{
            //    first = format.Key;
            //    break;
            //}
            if (first == -1)
            {
                try
                {
                    onlyOneLine = onlyOneLine.DeleteNullHexadecimalValues();
                    foreach (var format in InternalVariables.PdfFormats.Where(format => onlyOneLine.Contains(format.Value)))
                    {
                        first = format.Key;
                        break;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    first = -1;
                }
            }
            return first;
        }

        #endregion


        #region Debug

        public static void DebugAnalizar(string pdfPath)
        {
            OnlyOne = true;
            var pdfReader = new PDFReader(pdfPath);
            ExecuteSinglePdf(pdfReader);
        }

        #endregion



    }
}