﻿using Integraciones.ParsingExtention.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Integraciones.Utils.InternalVariables;
using Integraciones.View;

namespace Integraciones.ParsingExtention.View
{
    public class ViewCommands
    {
        private static bool canShowStart = true;
        private static ParseExt process;
        private static bool canShowDebug = true;

        public ICommand Start { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => canShowStart,
            CommandAction = () =>
            {
                try
                {
                    if (process == null)
                    {
                        var config = new Config();
                        process = new ParseExt(config);
                    }
                    process.Start();
                    canShowStart = false;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
        };
        public static void AutoStart()
        {
            try
            {
                if (process == null)
                {
                    var config = new Config();
                    process = new ParseExt(config);
                }
                process.Start();
                canShowStart = false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
        public static ICommand ShowOrdenesAProcesarCommand { get; } = new DelegateCommand
        {
            CommandAction = () => Process.Start(InternalVariables.GetOcAProcesarFolder())
        };

        public static ICommand ShowOrdenesProcesadasCommand { get; } = new DelegateCommand
        {
            CommandAction = () => Process.Start(InternalVariables.GetOcProcesadasFolder())
        };

        public ICommand ShowLogCommand { get; } = new DelegateCommand
        {
            CommandAction = () => Process.Start(InternalVariables.GetLogFolder())
        };




        public ICommand ManualExecute { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => canShowStart,
            CommandAction = () =>
            {
                try
                {
                    if (process == null)
                    {
                        var config = new Config();
                        process = new ParseExt(config);
                    }
                    process.ManualExecute(0);
                    canShowStart = false;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
        };

        public ICommand Stop { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => !canShowStart,
            CommandAction = () =>
            {
                process?.Stop();
                canShowStart = true;
            }
        };

        public static ICommand ShowConfiguration { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => canShowStart,
            CommandAction = () =>
            {
                new Integraciones.View.Configuraciones().Show();
            }
        };

        public static ICommand DebugMode { get; } = new DelegateCommand
        {
            CanExecuteFunc = () => canShowDebug,
            CommandAction = () =>
            {
                canShowDebug = false;
                new DebugWindow().Show();
            }
        };

        public static void SetcanShowDebug()
        {
            canShowDebug = true;
        }

       public ICommand Exit { get; } = new DelegateCommand
        {
            CommandAction = () =>
            {
                process?.Stop();
                Application.Current.Shutdown();
            }
        };
    }

    public class DelegateCommand : ICommand
    {
        public Action CommandAction { get; set; }
        public Func<bool> CanExecuteFunc { get; set; }

        public void Execute(object parameter)
        {
            CommandAction();
        }

        public bool CanExecute(object parameter)
        {
            return CanExecuteFunc == null || CanExecuteFunc();
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }

}
