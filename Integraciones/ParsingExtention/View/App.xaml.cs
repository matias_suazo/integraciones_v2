﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Log;
using Integraciones.View;
using Integraciones.Utils.RegistroErroresEmail;

namespace Integraciones.ParsingExtention.View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private static bool FirstInstance
        {
            get
            {
                var proces = Process.GetCurrentProcess().ProcessName;
                return Process.GetProcessesByName(proces).Count() == 1
                       || Process.GetProcessesByName(proces).Count() == 2;
            }
        }

        public static Window CurrentWindow { get; set; }

        private IntegracionesView _oldMode;
        private MainTray _newMode;
        protected override void OnStartup(StartupEventArgs e)
        {
            RegistroErroresModel rem = new RegistroErroresModel();
            var n = rem.EnvioCorreosErrores();
            if (!FirstInstance)
            {
                MessageBox.Show("No se Puede Abrir la Aplicacion debido a que ya se esta Ejecutando.", "Advertencia",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                Current.Shutdown();
            }
            else
            {
                CreateAllFolderIfNotExists();
                if (InternalVariables.IsNewMode())
                {
                    CurrentWindow =  MainTray.Instance ;
                    
                }
                else
                {
                    CurrentWindow = IntegracionesView.Instance;
                }
                //CreateAllFolderIfNotExists();
                Log.StartApp();
            }
            InternalVariables.InitializeVariables();
        }
        private static void CreateAllFolderIfNotExists()
        {
            if (!Directory.Exists(InternalVariables.GetOcAProcesarFolder()))
                Directory.CreateDirectory(InternalVariables.GetOcAProcesarFolder());
            if (!Directory.Exists(InternalVariables.GetLogFolder()))
                Directory.CreateDirectory(InternalVariables.GetLogFolder());
            if (!Directory.Exists(InternalVariables.GetOcProcesadasFolder()))
                Directory.CreateDirectory(InternalVariables.GetOcProcesadasFolder());
            if (!Directory.Exists(InternalVariables.GetOcProcesadasFolderExc()))
                Directory.CreateDirectory(InternalVariables.GetOcProcesadasFolderExc());
            if (!Directory.Exists(InternalVariables.GetUnknownOcFolder()))
                Directory.CreateDirectory(InternalVariables.GetUnknownOcFolder());
            if (!Directory.Exists(InternalVariables.GetErrorFolder()))
                Directory.CreateDirectory(InternalVariables.GetErrorFolder());
        }


        internal static void RestarApplication()
        {
            Debug.WriteLine("Try to restart app error");
        }
    }
}
