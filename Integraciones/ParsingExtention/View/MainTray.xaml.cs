﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hardcodet.Wpf.TaskbarNotification;

namespace Integraciones.ParsingExtention.View
{


    /// <summary>
    /// Interaction logic for MainTray.xaml
    /// </summary>
    public partial class MainTray : Window
    {
        private static MainTray _instance;

        public static MainTray Instance => _instance ?? (_instance = new MainTray());

        private MainTray()
        {
            InitializeComponent();
            ViewCommands.AutoStart();
        }


        public void ShowBalloon(string title, string desc, BalloonIcon icon)
        {
            NotifyIcon.ShowBalloonTip(title, desc, icon);
        }
    }
}
