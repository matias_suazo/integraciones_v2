﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Models
{
    public class Attachment
    {
        public string Name { get; set; }
        public byte[] Data { get; set; }
    }
}
