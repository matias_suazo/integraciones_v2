﻿using Integraciones.ParsingExtention.Interfaces;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Search;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Models
{
    public class MailKitOperations : IMailOperations
    {
        public string host { get; }
        public string secureType { get; }
        public int port { get; }        
        public string userMail { get; }
        public string password { get; }

        public MailKitOperations(string host,string secureType, int port, string userMail, string password)
        {
            this.host = host;
            this.secureType = secureType;
            this.userMail = userMail;
            this.password = password;
            this.port = port;
        }

        //public MailRes GetMail(string folderName)
        //{
        //    MailRes mailRes = null;
        //using (var client = new ImapClient())
        //{
        //    var personal = Connect(client);
        //    IMailFolder inbox = GetFolder(personal, folderName);
        //    inbox.Open(FolderAccess.ReadOnly);
        //    if (inbox.Count > 0)
        //    {
        //        IMessageSummary info = inbox.Fetch(0, -1, MessageSummaryItems.UniqueId).First();
        //        long uid = info.UniqueId.Id;
        //        MimeMessage email = inbox.GetMessage(info.UniqueId);
        //        mailRes = convertToRes(uid, email);
        //    }
        //    client.Disconnect(true);
        //}
        //    using (var client = new ImapClient())
        //    {
        //        // For demo-purposes, accept all SSL certificates
        //        client.ServerCertificateValidationCallback = (s, c, h, e) => true;

        //        client.Connect("imap.gmail.com", 993, true);

        //        // Note: since we don't have an OAuth2 token, disable
        //        // the XOAUTH2 authentication mechanism.
        //        client.AuthenticationMechanisms.Remove("XOAUTH2");

        //        client.Authenticate("procesosxml@ofimarket.cl", "plej8744");

        //        // The Inbox folder is always available on all IMAP servers...
        //        var inbox = client.Inbox;
        //        inbox.Open(FolderAccess.ReadOnly);

        //        Console.WriteLine("Total messages: {0}", inbox.Count);
        //        Console.WriteLine("Recent messages: {0}", inbox.Recent);

        //        for (int i = 0; i < inbox.Count; i++)
        //        {
        //            var message = inbox.GetMessage(i);
        //            Console.WriteLine("Subject: {0}", message.Subject);
        //        }

        //        client.Disconnect(true);
        //    }
        //    return mailRes;
        //}

        public MailRes GetMail(string folderName)
        {
            MailRes mailRes = null;
            using (var client = new ImapClient())
            {
                var personal = Connect(client);
                IMailFolder inbox = GetFolder(personal, folderName);
                inbox.Open(FolderAccess.ReadOnly);
                if (inbox.Count > 0)
                {
                    IMessageSummary info = inbox.Fetch(0, -1, MessageSummaryItems.UniqueId).First();
                    long uid = info.UniqueId.Id;
                    MimeMessage email = inbox.GetMessage(info.UniqueId);
                    mailRes = convertToRes(uid, email);
                }
                client.Disconnect(true);
            }
            return mailRes;
        }

        public void MoveMailTo(MailRes mail, string from, string to)
        {
            using (var client = new ImapClient())
            {
                /*ImapClient ic = new ImapClient("imap.gmail.com", "name@gmail.com", "pass",
                ImapClient.AuthMethods.Login, 993, true);*/
                var personal = Connect(client);
                //var personal = Connect(client);
                IMailFolder folderFrom = GetFolder(personal, from);
                IMailFolder folderTo = GetFolder(personal, to);
                folderFrom.Open(FolderAccess.ReadWrite);
                folderFrom.MoveTo(new UniqueId(Convert.ToUInt32(mail.UID)), folderTo);
                client.Disconnect(true);
            }

        }
        private IMailFolder GetFolder(IMailFolder source, string folderName)
        {
            IMailFolder res = source.GetSubfolders(false).FirstOrDefault(f => f.Name == folderName);
            if (res == null)
            {
                res = source.Create(folderName, true);
            }
            return res;
        }
        private MailRes convertToRes(long uid, MimeMessage email)
        {

            var attachments = new List<Attachment>();
            foreach (MimePart attachment in email.Attachments)
            {
                if (attachment == null)
                {
                    continue;
                }
                byte[] data;
                using (var memory = new MemoryStream())
                {
                    attachment.ContentObject.DecodeTo(memory);
                    data = memory.ToArray();
                }
                attachments.Add(new Attachment()
                {
                    Data = data,
                    Name = attachment.FileName
                });
            }

            var res = new MailRes<Limilabs.Mail.IMail>()
            {
                Subject = email.Subject,
                Body = email.TextBody,
                Attachments = attachments,
                UID = uid
            };
            res.Data = MailConvert.ToLimilabs(res);
            return res;
        }

        private IMailFolder Connect(ImapClient client)
        {
            MailKit.Security.SecureSocketOptions type;
            switch (secureType?.ToLower())
            {
                case "tls":
                    type = MailKit.Security.SecureSocketOptions.StartTls;
                    break;
                case "auto":
                    type = MailKit.Security.SecureSocketOptions.Auto;
                    break;
                case "ssl":
                    type = MailKit.Security.SecureSocketOptions.SslOnConnect;
                    break;
                default:
                    type = MailKit.Security.SecureSocketOptions.None;
                    break;
            }
            client.Connect(host, port, type);
            client.Authenticate(userMail, password);
            return client.GetFolder(client.PersonalNamespaces[0]);
        }
        
    }
}
