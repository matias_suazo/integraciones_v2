﻿using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Limilabs.Mail.Appointments;
using Limilabs.Mail.BusinessCard;
using Limilabs.Mail.DKIM;
using Limilabs.Mail.Headers;
using Limilabs.Mail.MIME;
using System.Collections.ObjectModel;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using MimeKit;

namespace Integraciones.ParsingExtention.Models
{
    public class MailConvert
    {
        public static IMail ToLimilabs(MailRes res)
        {
            var builder = new MailBuilder();
            builder.Subject = res.Subject;
            builder.Text = res.Body;
            foreach (var item in res.Attachments)
            {
                MimeData mime = new MimeFactory().CreateMimeData();
                mime.Data = item.Data;
                mime.FileName = item.Name;
                builder.AddAttachment(mime);
            }
            return builder.Create();
        }
    }
}
