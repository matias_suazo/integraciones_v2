﻿using Integraciones.ParsingExtention.Interfaces;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Models
{
    public class MailLimilabsOperations : IMailOperations
    {
        private string host;
        private string userMail;
        private string password;
        private string hackTrialSubject = "Please purchase Mail.dll license at https://www.limilabs.com/mail";

        public MailLimilabsOperations(string host, string userMail, string password)
        {
            this.host = host;
            this.userMail = userMail;
            this.password = password;
        }

        public MailRes GetMail(string folderName)
        {
            MailRes mailRes = null;
            using (var imap = new Imap())
            {
                imap.Connect(host);
                imap.Login(userMail, password);
                imap.Select(folderName);
                List<long> uids = imap.GetAll();
                if (uids.Count == 0)
                {
                    return mailRes;
                }
                long uid = uids.Last();
                IMail email = getMailWithTrialHack(imap, uid);
                if (email == null)
                {
                    return mailRes;
                }
                mailRes = convertToRes(uid, email);
            }
            return mailRes;
        }

        public void MoveMailTo(MailRes mail, string folderFrom, string folderTo)
        {
            using (var imap = new Imap())
            {
                imap.Connect(host);
                imap.Login(userMail, password);
                imap.Select(folderFrom);
                List<FolderInfo> all = imap.GetFolders();
                FolderInfo folder = all.Find(x => x.Name == folderTo);
                if (folder == null)
                {
                    imap.CreateFolder(folderTo);
                }
                imap.MoveByUID(mail.UID, folderTo);
            }

        }

        private IMail getMailWithTrialHack(Imap imap, long uid)
        {
            IMail email = null;
            int hackCheckCount = 3;
            do
            {
                byte[] emailRaw = imap.GetMessageByUID(uid);
                if (emailRaw?.Count() > 0)
                {
                    var temp = new MailBuilder().CreateFromEml(emailRaw);
                     if (temp?.Subject != hackTrialSubject)
                    {
                        email = temp;
                        hackCheckCount = 0;
                    }
                }
                hackCheckCount--;
            } while (hackCheckCount > 0);
            return email;
        }

        private MailRes convertToRes(long uid, IMail email)
        {
            IEnumerable<Attachment> attachments = email.Attachments.Select(a =>
            {
                return new Attachment()
                {
                    Data = a.Data,
                    Name = a.FileName
                };
            });
            return new MailRes<IMail>()
            {
                Subject = email.Subject,
                Body = email.GetBodyAsText(),
                Attachments = attachments,
                UID = uid,
                Data = email
            };
        }
    }
}
