﻿using Integraciones.ParsingExtention.Interfaces;
using Integraciones.Utils.InternalVariables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Models
{
    public class Config : IConfig
    {
        public string MailHost { get; } = InternalVariables.GetServidorCorreoLectura();
        public string MailSecureType { get; } = InternalVariables.GetSecureTypeCorreoLectura();
        public int MailPort { get; } = InternalVariables.GetPortCorreoLectura();
        public string MailUser { get; } = InternalVariables.GetUsuarioServidorCorreoLectura();
        public string MailPassword { get; } = InternalVariables.GetPassServidorCorreoLectura();
        public string MailInputFolder { get; } = "INBOX";
        public string MailOutputFolder { get; } = InternalVariables.GetEmailFolderProcesadas();
        public string MailErrorFolder { get; } = InternalVariables.GetEmailErrorFolderProcesadas();
        public string FileTempFolder { get; } = InternalVariables.GetOcAProcesarFolder();
        public string SubjectForBodyParse { get; } = InternalVariables.GetSubjectMail();
        public string IconstruyeMailHost { get; } = InternalVariables.GetHostIconstruye();
        public string IconstruyeMailUser { get; } = InternalVariables.GetUserIconstruye();
        public string IconstruyeMailPassword { get; } = InternalVariables.GetPasswordIcostruye();
        public int TaskMinutes { get; } = InternalVariables.GetTiempoMinutosCiclo();
    }
}
