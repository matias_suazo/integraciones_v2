﻿using Integraciones.ParsingExtention.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Models
{
    public class MailRes
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public IEnumerable<Attachment> Attachments { get; set; }
        public long UID { get; set; }
    }

    public class MailRes<T> : MailRes
    {
        public T Data { get; set; }
    }
}
