﻿using Integraciones.Main.Email;
using Integraciones.ParsingExtention.Interfaces;
using Integraciones.ParsingExtention.Models;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Processes
{
    public class IconstruyeXmlProcess
    {
        public void Do(MailRes res)
        {
            var mailRes = res as MailRes<IMail>;
            if (mailRes == null)
            {
                return;
            }
            MainEmail.ExecuteSingleIconstruyeMail(mailRes.Data);
        }
    }
}
