﻿using Integraciones.ParsingExtention.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Integraciones.ParsingExtention.Models;
using System.IO;
using Integraciones.Utils.Readers.Pdf;
using Integraciones.Main.Pdf;
using Integraciones.Main.Excel;
using Integraciones.Utils.Readers.Excel;
using Integraciones.Utils.Readers.Text;
using Integraciones.Main.Word;
using Integraciones.Utils.InternalVariables;
using Integraciones.Utils.Readers.Word;
using Integraciones.Main.Text;

namespace Integraciones.ParsingExtention.Processes
{
    public class AttachmentProcess
    {
        private readonly string fileTempFolder;
        public AttachmentProcess(string fileTempFolder)
        {
            this.fileTempFolder = fileTempFolder;
        }

        public void Do(MailRes mailRes)
        {
            if (mailRes == null)
            {
                return;
            }
            foreach (var attachment in mailRes.Attachments)
            {
                if (attachment.Data.Length > 0)
                {
                    ParsingAttachment(attachment, true);
                }
            }
        }

        public void DownloadFile(MailRes mailRes)
        {
            if (mailRes == null)
            {
                return;
            }
            foreach (var attachment in mailRes.Attachments.Where(attachment => attachment.Data.Length > 0))
            {

                System.Diagnostics.Debug.WriteLine(attachment.Name);
                var fileName = $"{DateTime.Now:dd-MM-yyyy-HH-mm-ss-fff}_{attachment.Name}";
                var filePath = Path.Combine(fileTempFolder, fileName);
                File.WriteAllBytes(filePath, attachment.Data);
            }
        }

        private void ParsingAttachment(Attachment attachment, bool onlyDownloadFile = false)
        {
            string ext = Path.GetExtension(attachment.Name).ToLower();
            System.Diagnostics.Debug.WriteLine(attachment.Name);
            string filePath = Path.Combine(fileTempFolder, attachment.Name);
            File.WriteAllBytes(filePath, attachment.Data);
            if (onlyDownloadFile) return;
            try
            {
                switch (ext)
                {
                    case ".pdf":
                        {
                            var reader = new PDFReader(filePath);
                            MainPdf.ExecuteSinglePdf(reader);
                            break;
                        }
                    case ".csv":
                    case ".xlsx":
                    case ".xls":
                        {
                            var reader = new ExcelReader(filePath, leerHastaColuma: ColumnaExcel.AL);
                            MainExcel.ExecuteSingleExcel(reader);
                            break;
                        }
                    case ".eml":
                        {
                            File.Delete(filePath);
                            break;
                        }
                    case ".msg":
                        {
                            File.Delete(filePath);
                            break;
                        }
                        /* case ".txt":
                             {
                                 var reader = new Utils.Readers.Text.TextReader(filePath);
                                 MainText.(reader);
                                 break;
                             }*/
                        //case ".docx":
                        //case ".doc":
                        //    {
                        //        var reader = new WordReader(filePath);
                        //        MainWord.ExecuteSingleWord(reader);
                        //        break;
                        //    }
                }
            }
            finally
            {
                // Delete only if Debug Mode is not Activated
                if (!InternalVariables.IsDebug())
                {
                    File.Delete(filePath);
                }
            }
        }
    }
}
