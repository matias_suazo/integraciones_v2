﻿using System;
using Integraciones.Main.Excel;
using Integraciones.Main.Pdf;
using Integraciones.ParsingExtention.Models;
using Integraciones.Main.Text;

namespace Integraciones.ParsingExtention.Steps
{
    static class SecondaryStep
    {

        public static Step GetStep()
        {
            var step = new Step("Secondary Step")
            {
                preAction= () =>
                {
                    var res = new StepResult<MailRes> {ActionRequired = true};
                    return res;
                },
               action = (r) =>
               {
                   var log = "";
                   
                   
                   
                   log = "Execute Secoday Stop on Main TXT...";
                   ParseExt.Log.Debug(log);
                   Console.WriteLine(log);
                   MainText.ExecuteLecturaText();
                   log = "Execute Secodary Step on Main EXCEL...";
                   ParseExt.Log.Debug(log);
                   Console.WriteLine(log);
                   MainExcel.ExecuteLecturaExcel();
                   log = "Execute Secodary Step on Main EXCEL...";
                   ParseExt.Log.Debug(log);
                   Console.WriteLine(log);
                   log = "Execute Secodary Step on Main PDF...";
                   ParseExt.Log.Debug(log);
                   Console.WriteLine(log);
                   MainPdf.ExecuteLecturaPdf();
                   return null;
               }
            };
            return step;
        }
    }
}
