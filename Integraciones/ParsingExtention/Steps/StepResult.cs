﻿using Integraciones.ParsingExtention.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Steps
{
    public class StepResult<T> : IStepResult
    {
        public bool ActionRequired { get; set; } = false;
        public bool PostActionRequired { get; set; } = false;
        public bool ErrorCatch { get; set; } = false;
        public bool RepeatRequired { get; set; } = false;
        public T Data { get; set; }

        public void ResetRequieredFlags()
        {
            ActionRequired = false;
            PostActionRequired = false;
            RepeatRequired = false;
            ErrorCatch = false;
        }
    }
}
