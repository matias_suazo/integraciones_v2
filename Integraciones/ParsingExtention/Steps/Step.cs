﻿using Integraciones.ParsingExtention.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Steps
{
    public class Step
    {
        public Func<IStepResult, IStepResult> action;
        public Func<IStepResult> preAction;
        public Func<IStepResult, IStepResult> postAction;
        public Func<Exception, IStepResult, IStepResult> errorAction;
        public string Name { get; }

        public Step(string name)
        {
            this.Name = name;
        }
    }
}
