﻿using Integraciones.ParsingExtention.Interfaces;
using Integraciones.ParsingExtention.Models;
using Integraciones.ParsingExtention.Processes;
using Integraciones.Utils.FlagClose;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Steps
{
    public class MainStep
    {
        private IConfig config;
        public MainStep(IConfig config)
        {
            this.config = config;
        }
        public Step GetStep()
        {
            
            var emailBox = new MailKitOperations
           (
               host: config.MailHost,
               secureType: config.MailSecureType,
               port: config.MailPort,
               userMail: config.MailUser,
               password: config.MailPassword
           );
            var attachmentProcess = new AttachmentProcess(config.FileTempFolder);
            var bodyProcess = new MailBodyProcess();
            var step = new Step("Main")
            {
                preAction = () =>
                {
                    var res = new StepResult<MailRes>();
                    ParseExt.Log.Debug($"Get mail from {emailBox.userMail}");
                    Console.WriteLine($"Get mail from {emailBox.userMail}");
                    MailRes mail = emailBox.GetMail(config.MailInputFolder);
                    if (mail != null)
                    {
                        ParseExt.Log.Debug($"Mail subject = {mail.Subject}");
                        Console.WriteLine($"Mail subject = {mail.Subject}");
                        res.Data = mail;
                        res.ActionRequired = true;
                        res.RepeatRequired = true;
                        /*var email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(res.Data.UID));
                        Console.WriteLine(email.ToString());
                        string path = "C:/Debug_Registro_Correos_Entrantes.txt";
                        string texto = "Entro a GetStep()";
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path))
                        {
                            
                            writer.WriteLine(texto);
                            texto = email.ToString();
                            writer.WriteLine(texto);
                        }*/
                    }
                    else
                    {
                        ParseExt.Log.Debug($"Not exists mail in {config.MailInputFolder} from user: {emailBox.userMail}");
                        Console.WriteLine($"Not exists mail in {config.MailInputFolder} from user: {emailBox.userMail}");
                    }
                    return res;
                },
                action = (IStepResult r) =>
                {
                    var res = r as StepResult<MailRes>;
                    if (res != null)
                    {
                        if (res.Data?.Subject == config.SubjectForBodyParse)
                        {
                            ParseExt.Log.Debug($"MailBody parsing start");
                            bodyProcess.Do(res.Data);
                            ParseExt.Log.Debug($"MailBody parsing end");
                        }
                        else
                        {
                            ParseExt.Log.Debug($"Attachment parsing start");
                            Console.WriteLine($"Attachment parsing start");
                            attachmentProcess.DownloadFile(res.Data);                           
                            ParseExt.Log.Debug($"Attachment parsing end");                            
                            Console.WriteLine($"Attachment parsing end");
                        }
                        res.PostActionRequired = true;
                    }
                    return res;
                },
                postAction = (IStepResult r) =>
                {
                    var res = r as StepResult<MailRes>;
                    if (res != null)
                    {
                        ParseExt.Log.Debug($"Move mail from {config.MailInputFolder} to {config.MailOutputFolder}");
                        emailBox.MoveMailTo(res.Data, config.MailInputFolder, config.MailOutputFolder);
                    }
                    return res;
                },
                errorAction = (Exception e, IStepResult r) =>
                {
                    var res = r as StepResult<MailRes>;
                    if (res != null)
                    {
                        ParseExt.Log.Debug($"Move mail from {config.MailInputFolder} to {config.MailErrorFolder}");
                        emailBox.MoveMailTo(res.Data, config.MailInputFolder, config.MailErrorFolder);
                        res.ErrorCatch = true;
                    }
                    return res;
                }
            };
            FlagClose.email = true;
            return step;
        }
    }
}
