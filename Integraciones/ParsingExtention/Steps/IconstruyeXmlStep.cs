﻿using Integraciones.ParsingExtention.Interfaces;
using Integraciones.ParsingExtention.Models;
using Integraciones.ParsingExtention.Processes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Steps
{
    public class IconstruyeXmlStep
    {
        private IConfig config;
        public IconstruyeXmlStep(IConfig config)
        {
            this.config = config;
        }
        public Step GetStep()
        {
            var emailBox = new MailKitOperations
            (
                host: config.IconstruyeMailHost,
                secureType: config.MailSecureType,
                port: config.MailPort,
                userMail: config.IconstruyeMailUser,
                password: config.IconstruyeMailPassword
            );
            var process = new IconstruyeXmlProcess();
            var step = new Step("IconstruyeXml")
            {
                preAction = () =>
                {
                    var res = new StepResult<MailRes>();
                    ParseExt.Log.Debug($"Get mail from {emailBox.userMail}");
                    MailRes mail = emailBox.GetMail(config.MailInputFolder);
                    if (mail != null)
                    {
                        ParseExt.Log.Debug($"Mail subject = {mail.Subject}");
                        res.Data = mail;
                        res.ActionRequired = true;
                        res.RepeatRequired = true;
                    }
                    return res;
                },
                action = (IStepResult r) =>
                {
                    var res = r as StepResult<MailRes>;
                    if (res != null)
                    {
                        ParseExt.Log.Debug($"IconstruyeXml parsing start");
                        process.Do(res.Data);
                        res.PostActionRequired = true;
                        ParseExt.Log.Debug($"IconstruyeXml parsing end");
                    }
                    return res;
                },
                postAction = (IStepResult r) =>
                {
                    var res = r as StepResult<MailRes>;
                    if (res != null)
                    {
                        ParseExt.Log.Debug($"Move mail from {config.MailInputFolder} to {config.MailOutputFolder}");
                        emailBox.MoveMailTo(res.Data, config.MailInputFolder, config.MailOutputFolder);
                    }
                    return res;
                },
                errorAction = (Exception e, IStepResult r) =>
                {
                    var res = r as StepResult<MailRes>;
                    if (res != null)
                    {
                        ParseExt.Log.Debug($"Move mail from {config.MailInputFolder} to {config.MailErrorFolder}");
                        emailBox.MoveMailTo(res.Data, config.MailInputFolder, config.MailErrorFolder);
                        res.ErrorCatch = true;
                    }
                    return res;
                }
            };
            return step;
        }
    }
}
