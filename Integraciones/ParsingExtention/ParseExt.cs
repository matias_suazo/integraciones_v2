﻿using Integraciones.ParsingExtention.Interfaces;
using System;
using System.Collections.Generic;
using Integraciones.ParsingExtention.Steps;
using Integraciones.Utils.Log;
using Quartz;
using Quartz.Impl;

namespace Integraciones.ParsingExtention
{
    public class ParseExt
    {
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static IConfig Config { get; set; }
        private IScheduler scheduler;
        public ParseExt(IConfig config)
        {
            Config = config;
        }

        public void Start()
        {
            if (scheduler == null || scheduler.IsShutdown == true)
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
            }
            IJobDetail job = JobBuilder.Create<ParsingJob>().Build();
            ITrigger trigger = TriggerBuilder.Create()

                .WithIdentity("Auto", "Parsing")
                .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionNextWithRemainingCount()
                 .WithIntervalInMinutes(Config.TaskMinutes)
                 .RepeatForever())
                .StartAt(DateTime.Now)
                .WithPriority(1)
                .Build();
            scheduler.ScheduleJob(job, trigger);
        }

        public void ManualExecute(int count)
        {
            if (scheduler == null || scheduler.IsShutdown == true)
            {
                scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
            }
            IJobDetail job = JobBuilder.Create<ParsingJob>().Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("Manual", "Parsing")
                .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionNextWithRemainingCount()
                 .WithIntervalInMinutes(Config.TaskMinutes)
                .WithRepeatCount(count))
                .StartAt(DateTime.UtcNow)
                .WithPriority(1)
                .Build();
            scheduler.ScheduleJob(job, trigger);
        }

        public void Stop()
        {
            scheduler?.Shutdown();
        }
    }

    [Quartz.DisallowConcurrentExecutionAttribute()]
    public class ParsingJob : IJob
    {
        private IConfig config { get; } = ParseExt.Config;
        private log4net.ILog log { get; } = ParseExt.Log;
        private IEnumerable<Step> GetStepsList()
        {
            var steps = new List<Step>
            {
                new MainStep(config).GetStep()
                ,
                SecondaryStep.GetStep()
            };
            //steps.Add(new IconstruyeXmlStep(config).GetStep());
            return steps;
        }

        public void Execute(IJobExecutionContext context)
        {
            var steps = GetStepsList();
            log.Info("Task start");
            foreach (var step in steps)
            {
                log.Info($"{step.Name} step starting");
                Console.WriteLine($"{step.Name} step starting");
                IStepResult res = null;
                var count = 1;
                do
                {
                    log.Debug($"{step.Name} step iteration = {count}");
                    Console.WriteLine($"{step.Name} step iteration = {count}");
                    res?.ResetRequieredFlags();
                    try
                    {
                        count++;
                        res = step.preAction?.Invoke();
                        if (res?.ActionRequired == true)
                        {
                            res = step.action?.Invoke(res);
                        }
                        if (res?.PostActionRequired == true)
                        {
                            res = step.postAction?.Invoke(res);
                        }
                    }
                    catch (Exception e)
                    {
                        res = step.errorAction?.Invoke(e, res);
                        if (res == null || res.ErrorCatch == false)
                        {
                            log.Fatal($"{step.Name} step error", e);
                        }
                        else
                        {
                            log.Error($"{step.Name} step error", e);
                        }
                    }

                } while (res?.RepeatRequired == true);
                log.Info($"{step.Name} step end");
            }
            log.Info("Task end");
            Log.SendMails();
        }
    }
}
