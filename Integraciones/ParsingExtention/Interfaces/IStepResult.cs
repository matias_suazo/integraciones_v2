﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integraciones.ParsingExtention.Interfaces
{
    public interface IStepResult
    {
        bool ActionRequired { get; set; }
        bool PostActionRequired { get; set; }
        bool ErrorCatch { get; set; }
        void ResetRequieredFlags();
        bool RepeatRequired { get; set; }
    }
}
