﻿using Integraciones.ParsingExtention.Models;
using System;

namespace Integraciones.ParsingExtention.Interfaces
{
    public interface IMailOperations
    {
        MailRes GetMail(string folderName);
        void MoveMailTo(MailRes mail, string folderFrom, string folderTo);
    }
}