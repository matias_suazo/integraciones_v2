﻿namespace Integraciones.ParsingExtention.Interfaces
{
    public interface IConfig
    {
        string MailHost { get; }
        string MailSecureType { get; }
        int MailPort { get; }
        string MailPassword { get; }
        string MailUser { get; }
        string MailInputFolder { get; }
        string MailOutputFolder { get; }
        string MailErrorFolder { get; }
        string FileTempFolder { get; }
        string SubjectForBodyParse { get; }
        string IconstruyeMailHost { get; }
        string IconstruyeMailPassword { get; }
        string IconstruyeMailUser { get; }
        int TaskMinutes { get; }
    }
}