ALTER TABLE DM_VENTAS.RE_BODPROD_DIMERC
 DROP PRIMARY KEY CASCADE;
DROP TABLE DM_VENTAS.RE_BODPROD_DIMERC CASCADE CONSTRAINTS;

CREATE TABLE DM_VENTAS.RE_BODPROD_DIMERC
(
  CODEMP             NUMBER(10)                 NOT NULL,
  CODBOD             NUMBER(10)                 NOT NULL,
  CODPRO             VARCHAR2(15 BYTE)          NOT NULL,
  STOCKS             NUMBER(10),
  STKINI             NUMBER(10),
  FECINI             DATE,
  ULTINV             NUMBER(8),
  FECINV             DATE,
  STKMAX             NUMBER(10),
  STKMIN             NUMBER(10),
  STKCOM             NUMBER(10),
  STKDIS             NUMBER(10),
  STKASE             NUMBER(10),
  STKCRI             NUMBER(10),
  STKINT             NUMBER(10),
  USR_CREAC          VARCHAR2(30 BYTE)          DEFAULT substr(user,1,30)     NOT NULL,
  FECHA_CREAC        DATE                       DEFAULT sysdate               NOT NULL,
  USR_MODIF          VARCHAR2(30 BYTE)          DEFAULT substr(user,1,30)     NOT NULL,
  FECHA_MODIF        DATE                       DEFAULT sysdate               NOT NULL,
  COSTO              NUMBER(12,2)               DEFAULT 0,
  COSPROM            NUMBER(12,2)               DEFAULT 0,
  COSPRO_CAL_KARDEX  NUMBER(15,2),
  VALOR_CAL_KARDEX   NUMBER(15,2),
  STOCK_CAL_KARDEX   NUMBER(15)
)
TABLESPACE DM_VENTAS_DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX DM_VENTAS.RE_BODPROD_DIMERC_IDX01 ON DM_VENTAS.RE_BODPROD_DIMERC
(CODEMP, CODPRO, CODBOD)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.RE_BODPROD_DIMERC ON DM_VENTAS.RE_BODPROD_DIMERC
(CODEMP, CODBOD)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.RE_BODPROD_DIMERC_PK ON DM_VENTAS.RE_BODPROD_DIMERC
(CODEMP, CODBOD, CODPRO)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.RE_BODPROD_DIMERC_IDX02 ON DM_VENTAS.RE_BODPROD_DIMERC
(CODEMP, CODPRO, CODBOD, STOCKS)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_BODPROD_BU
BEFORE UPDATE
ON DM_VENTAS.RE_BODPROD_DIMERC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN 
  -- 
 -- IF :NEW.STKCOM < 0  THEN 
    -- 
  --  RAISE_APPLICATION_ERROR( -20000, 'RE_BODPROD: no puedo establecer stock comprometido en negativo'); 
    -- 
 -- END IF; 
  -- 
  :NEW.usr_modif   := SUBSTR( USER, 1, 30); 
  :NEW.fecha_modif := SYSDATE; 
  -- 
END;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_BODPROD_DIMERC_BIU
BEFORE INSERT OR UPDATE
OF STKCOM
  ,COSPROM
ON DM_VENTAS.RE_BODPROD_DIMERC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
tmpVde Varchar2(1);
tmpRut number;
tmpLin number;
tmpTip number;
cursor c1 (Rut_ number, lin_ number) is 
select distinct(codcnl)  Codcnl from re_cliente_lista_linea where rutcli= rut_ and codlin = lin_;
BEGIN
   tmpVar := 0;
   select vender into tmpVde from ma_bodegas where codemp = :new.codemp and codbod = :new.codbod;
   if tmpVde = 'N' then
       :new.stkcom :=0;
   end if;    
---- Esto se hace para pisar el precio con el costo promedio, solo Bodegas No contables de Venta de Clientes
   tmpVar := 0;
   select count(*) into tmpvar from ma_bodegas where codbod = :new.codbod and codemp=:new.codemp and not rutcli is null;
   if tmpvar >0 then
      select rutcli into tmpRut from ma_bodegas where codbod = :new.codbod and codemp=:new.codemp and not rutcli is null;
      select codlin, tipoprod  into tmplin, tmpTip from ma_product where codpro = :new.codpro;
      if tmpTip = 2 then
         for v in c1 (tmpRut, tmpLin) loop
             update re_canprod set precio = :new.cosprom where codcnl = v.codcnl and codpro = :new.codpro and codemp =:new.codemp;                               
         end loop;
      end if;
   end if;              
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;   
END RE_BODPROD_DIMERC_BIU;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_BODPROD_DIMERC_TRB_BUI
BEFORE INSERT OR UPDATE
OF STOCKS
ON DM_VENTAS.RE_BODPROD_DIMERC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
tmpStk NUMBER; 
tmpCom NUMBER; 
BEGIN
   tmpVar := 0;
                
  IF INSERTING THEN 
      IF :NEW.CODPRO = 'PK10000' AND :NEW.CODBOD =  1  AND :NEW.CODEMP = 3 THEN
         :NEW.STOCKS := 100;
         :NEW.STKCOM := 0;
      END IF;
   ELSIF UPDATING THEN
      IF :OLD.CODPRO = 'PK10000' AND :OLD.CODBOD =  1  AND :OLD.CODEMP = 3 THEN
         :NEW.STOCKS := 100;
         :NEW.STKCOM := 0;
      END IF;
   END IF;                                                             
   
  select stkfsc, stkcom into tmpStk, tmpCom from ma_product where codpro = :new.codpro; 
  if :new.codbod = 1 and :new.codemp = 3 then  
    if tmpStk <> :new.stocks then
       update ma_product set stkfsc = :new.stocks where codpro = :new.codpro; 
    end if;                                
    if tmpCom <> :new.stkcom then
       :new.stkcom := tmpCom;
    end if;        
  end if;
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END re_bodprod_dimerc_trb_bui;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_BODPROD_DIMERC_BI
BEFORE INSERT
ON DM_VENTAS.RE_BODPROD_DIMERC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
tmpCar VARCHAR(1);
BEGIN
   tmpVar := 0;

  --  select  NVL(costo_promedio_propio,'N') 
   --         into tmpCar 
    --        from ma_bodegas 
     --       where 
      --          codemp=:new.codemp 
       --     and costo_promedio_propio='S' 
        --    and codbod = :new.codbod;                                     
   
    if :NEW.CODBOD <> 49 and :NEW.CODBOD <> 50 and :NEW.CODBOD <> 51 
       and :NEW.CODBOD <> 59 and :NEW.CODBOD <> 60  
       and :NEW.CODBOD <> 61 and :NEW.CODBOD <> 62 
       and :NEW.CODBOD <> 64 and :NEW.CODBOD <> 65 
       and :NEW.CODBOD <> 69 and :NEW.CODBOD <> 70 
       and :NEW.CODBOD <> 79 and :NEW.CODBOD <> 80 THEN 
       :NEW.COSPROM     := GETCOSTOPROMEDIO(:NEW.CODEMP,:NEW.CODPRO);
       :NEW.COSTO       := GETCOSTOCOMERCIAL(:NEW.CODEMP,:NEW.CODPRO);
    END IF;                  

  --  if tmpCar = 'N' THEN
   --    :NEW.COSPROM     := GETCOSTOPROMEDIO(:NEW.CODEMP,:NEW.CODPRO);
    --   :NEW.COSTO       := GETCOSTOCOMERCIAL(:NEW.CODEMP,:NEW.CODPRO);
    --END IF;                  
    
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END RE_BODPROD_DIMERC_BI;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_BODPROD_DIMERC_STKNEG_BUI
BEFORE INSERT OR UPDATE
OF STOCKS
ON DM_VENTAS.RE_BODPROD_DIMERC 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
   tmpVar := 0;                   
  IF NVL( :new.stkcom, 0 ) < 0 then
    :new.stkcom :=0;
  end if;       
  IF NVL( :new.STOCKS, 0 ) < 0 and :new.codemp <> 6  THEN
    --
     RAISE_APPLICATION_ERROR( -20000, 'RE_BODPROD_DIMERC: posible error al grabar Stock Negativo. Operacion no sera efectuada ['||:OLD.codpro||']['||:OLD.STOCKS ||']['||:NEW.STOCKS ||']');
    --
   END IF;
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END RE_BODPROD_DIMERC_stkNeg_bui;
/


DROP PUBLIC SYNONYM RE_BODPROD_DIMERC;

CREATE PUBLIC SYNONYM RE_BODPROD_DIMERC FOR DM_VENTAS.RE_BODPROD_DIMERC;


ALTER TABLE DM_VENTAS.RE_BODPROD_DIMERC ADD (
  CONSTRAINT RE_BODPROD_DIMERC_PK
 PRIMARY KEY
 (CODEMP, CODBOD, CODPRO)
    USING INDEX 
    TABLESPACE DM_VENTAS_IDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT ALTER, DELETE, INSERT, SELECT, UPDATE ON DM_VENTAS.RE_BODPROD_DIMERC TO ROL_APLICACION;

GRANT SELECT ON DM_VENTAS.RE_BODPROD_DIMERC TO ROL_GESTION;

