DROP TABLE DM_VENTAS.MA_USUARIO CASCADE CONSTRAINTS;

CREATE TABLE DM_VENTAS.MA_USUARIO
(
  RUTUSU                     NUMBER(9)          NOT NULL,
  DIGUSU                     VARCHAR2(1 BYTE)   NOT NULL,
  APEPAT                     VARCHAR2(40 BYTE)  NOT NULL,
  APEMAT                     VARCHAR2(40 BYTE)  NOT NULL,
  NOMBRE                     VARCHAR2(40 BYTE)  NOT NULL,
  USERID                     VARCHAR2(30 BYTE)  NOT NULL,
  FECING                     DATE,
  FECEXP                     DATE,
  TIPUSR                     NUMBER(3),
  TIPVEN                     NUMBER(3),
  CODCNL                     NUMBER(4),
  ACTFPR                     VARCHAR2(1 BYTE),
  CNLVEN                     NUMBER(4),
  NOMPRT                     VARCHAR2(15 BYTE),
  CNLCOM                     VARCHAR2(1 BYTE),
  GRUVTA                     NUMBER(4)          DEFAULT 0,
  IMPRES                     VARCHAR2(30 BYTE),
  CONEC                      VARCHAR2(1 BYTE),
  CODEST                     NUMBER(1),
  MARGEN                     VARCHAR2(1 BYTE),
  VERMSG                     VARCHAR2(1 BYTE),
  MODMOV                     VARCHAR2(1 BYTE),
  ACCREC                     VARCHAR2(1 BYTE),
  ACCFAC                     VARCHAR2(1 BYTE),
  NULDOC                     VARCHAR2(1 BYTE),
  USRNUL                     VARCHAR2(15 BYTE),
  MOVENT                     VARCHAR2(1 BYTE),
  MOVSAL                     VARCHAR2(1 BYTE),
  ASIGNADO                   VARCHAR2(1 BYTE)   DEFAULT 'N',
  NOTAVTA_INTERNET_A_BODEGA  VARCHAR2(1 BYTE)   DEFAULT 'N',
  MARGVT                     VARCHAR2(1 BYTE),
  VERCNL                     VARCHAR2(1 BYTE),
  ACCDOC                     VARCHAR2(1 BYTE),
  ACCSIS                     VARCHAR2(1 BYTE),
  ACCEXI                     VARCHAR2(2 BYTE),
  ANEXOS                     NUMBER(6),
  TRANSP                     VARCHAR2(2 BYTE),
  ACCREV                     VARCHAR2(1 BYTE),
  PORREV                     NUMBER(5),
  NUMCLI                     NUMBER(10),
  ACCCTA                     VARCHAR2(1 BYTE),
  PROBLQ                     VARCHAR2(1 BYTE),
  ADDCLI                     VARCHAR2(1 BYTE),
  DPESOS                     VARCHAR2(1 BYTE),
  VERMAR                     VARCHAR2(1 BYTE),
  VERPED                     VARCHAR2(1 BYTE),
  GRUCOB                     VARCHAR2(6 BYTE),
  MODMER                     VARCHAR2(1 BYTE),
  APRUEB                     VARCHAR2(1 BYTE),
  DEPART                     VARCHAR2(3 BYTE),
  GRUPAR                     VARCHAR2(6 BYTE),
  EMPRES                     VARCHAR2(1 BYTE),
  ACCTLS                     VARCHAR2(1 BYTE),
  TIPFAX                     VARCHAR2(2 BYTE),
  MODFIC                     VARCHAR2(1 BYTE),
  CANAL                      NUMBER(10),
  SUBCNL                     NUMBER(10),
  CODUSU                     VARCHAR2(60 BYTE),
  CODVAL                     VARCHAR2(20 BYTE),
  MAIL01                     VARCHAR2(50 BYTE),
  CODCOM                     NUMBER(3),
  NUMFAX                     NUMBER(12),
  PRTELE                     NUMBER(1),
  SEGMENTO                   NUMBER(1)          DEFAULT 0,
  REGION                     NUMBER(3)          DEFAULT 13,
  CLAVE                      VARCHAR2(50 BYTE),
  NUMVER                     VARCHAR2(10 BYTE),
  CLAVE2                     VARCHAR2(10 BYTE),
  CODEMP                     NUMBER(3),
  ES_HORECA                  VARCHAR2(1 BYTE)   DEFAULT 'N',
  TIPCOM                     NUMBER(3)
)
TABLESPACE DM_VENTAS_DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          368K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      KEEP
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX DM_VENTAS.MA_USUARIO_IDX05 ON DM_VENTAS.MA_USUARIO
(CODCNL, USERID)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.MA_USUARIO_PK ON DM_VENTAS.MA_USUARIO
(RUTUSU)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.MA_USUARIO_IDX04 ON DM_VENTAS.MA_USUARIO
(RUTUSU, TIPUSR)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_USUARIO_IDX01 ON DM_VENTAS.MA_USUARIO
(USERID)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_USUARIO_IDX02 ON DM_VENTAS.MA_USUARIO
(APEPAT, APEMAT)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_USUARIO_IDX03 ON DM_VENTAS.MA_USUARIO
(TIPUSR)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_USUARIO_IDX06 ON DM_VENTAS.MA_USUARIO
(TIPUSR, TIPVEN)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


DROP PUBLIC SYNONYM MA_USUARIO;

CREATE PUBLIC SYNONYM MA_USUARIO FOR DM_VENTAS.MA_USUARIO;


DROP PUBLIC SYNONYM MA_USUARIO_SYN;

CREATE PUBLIC SYNONYM MA_USUARIO_SYN FOR DM_VENTAS.MA_USUARIO;


ALTER TABLE DM_VENTAS.MA_USUARIO ADD (
  CHECK (asignado  in ('S','N')),
  CHECK (notavta_internet_a_bodega  in ('S','N')),
  CONSTRAINT MA_USUARIO_UK01
 UNIQUE (USERID));

