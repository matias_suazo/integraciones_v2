DROP TABLE DM_TRANSFER_WEB.TF_DETALLE_COMPRA_INTEG_EXCEL CASCADE CONSTRAINTS;

CREATE TABLE DM_TRANSFER_WEB.TF_DETALLE_COMPRA_INTEG_EXCEL
(
  ID          NUMBER(10),
  NUMPED      NUMBER(30)                        NOT NULL,
  SKU_DIMERC  VARCHAR2(10 BYTE)                 NOT NULL,
  CANTIDAD    NUMBER(10),
  PRECIO      NUMBER(20),
  SUBTOTAL    NUMBER(20),
  CODBOD      NUMBER,
  ESTADO_SKU  NUMBER(20),
  CODEMP      INTEGER,
  POSICION    INTEGER
)
TABLESPACE DM_VENTAS_DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER DM_TRANSFER_WEB.TF_DETALLE_COMPRA_INTEG_EXCEL
BEFORE INSERT
ON DM_TRANSFER_WEB.TF_DETALLE_COMPRA_INTEG_EXCEL 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
--TF_DETALLE_COMPRA_INTEG_EXCEL
begin
select TF_DETALLE_COMPRA_TRANSFER.nextval into :new.id from dual;
end;
/


