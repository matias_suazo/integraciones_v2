ALTER TABLE DM_VENTAS.RE_CANPROD
 DROP PRIMARY KEY CASCADE;
DROP TABLE DM_VENTAS.RE_CANPROD CASCADE CONSTRAINTS;

CREATE TABLE DM_VENTAS.RE_CANPROD
(
  CODCNL              NUMBER(3)                 NOT NULL,
  CODPRO              VARCHAR2(9 BYTE)          NOT NULL,
  MARGEN              NUMBER(5,2)               NOT NULL,
  PRECIO              NUMBER(10)                NOT NULL,
  FACTOR              NUMBER(6,2)               NOT NULL,
  PORDES              NUMBER(5,2),
  DESPRO              VARCHAR2(40 BYTE),
  INDICA              VARCHAR2(1 BYTE),
  DESCUE              NUMBER(5,2),
  PTORJO              VARCHAR2(1 BYTE),
  PREFIJ              NUMBER(10),
  DPESOS              NUMBER(10),
  MODSUP              VARCHAR2(1 BYTE),
  CODIGO_UNI          VARCHAR2(15 BYTE),
  MULTIP              NUMBER(6)                 DEFAULT 1,
  OLD_PRECIO          NUMBER(10),
  COSTO_CAJAS_MANUAL  NUMBER(10),
  CODEMP              NUMBER(3),
  MGPREFIJO           NUMBER(12,4)
)
TABLESPACE DM_VENTAS_DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          65568K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX DM_VENTAS.RE_CANPROD_IDX01 ON DM_VENTAS.RE_CANPROD
(CODPRO, CODEMP)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.RE_CANPROD_PK ON DM_VENTAS.RE_CANPROD
(CODCNL, CODPRO, CODEMP)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.RE_CANPROD_IDX02 ON DM_VENTAS.RE_CANPROD
(CODPRO, DPESOS)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          20M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.RE_CANPROD_IDX03 ON DM_VENTAS.RE_CANPROD
(CODCNL)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.RE_CANPROD_IDX04 ON DM_VENTAS.RE_CANPROD
(CODPRO, CODEMP, CODCNL, PRECIO)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.RE_CANPROD_IDX05 ON DM_VENTAS.RE_CANPROD
(CODPRO, CODEMP, NVL("MGPREFIJO",0))
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER DM_VENTAS.re_canprod_biud_trg
BEFORE DELETE OR INSERT OR UPDATE
OF PORDES
ON DM_VENTAS.RE_CANPROD 
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
   ---  para canales segmento comercio listas 90, 91 y 92
   ---  no se podran colocar descuentos
   if :new.codcnl = 90 or :old.codcnl = 90 or :new.codcnl = 91 or :old.codcnl = 91 or :new.codcnl = 92 or :old.codcnl = 92 then
      :new.pordes  :=0;  
   end if; 

   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END re_canprod_biud_trg;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_CANPROD_WEB_BUITRG
BEFORE INSERT OR UPDATE
OF CODPRO
  ,CODCNL
  ,PRECIO
ON DM_VENTAS.RE_CANPROD 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
tmpExs number;
BEGIN
        tf_codigo_lista_precio_pkg.put( 
                          :new.codpro
                          ,:new.codcnl
                          ,:new.precio
                          ,0
                          ,'I');   
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END RE_CANPROD_WEB_BUITRG;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_CANPROD_TRG
BEFORE DELETE OR INSERT OR UPDATE
ON DM_VENTAS.RE_CANPROD 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar VARCHAR2(100);
tmpTer VARCHAR2(100);
tmpLin number;
BEGIN

	 SELECT substr(SYS_CONTEXT('USERENV','MODULE')  ,1,100) INTO TMPVAR FROM DUAL;
	 SELECT substr(SYS_CONTEXT('USERENV','TERMINAL'),1,100) INTO TMPTER FROM DUAL;  
	 
	 IF :NEW.MARGEN <> :OLD.MARGEN OR :NEW.PRECIO <> :OLD.PRECIO OR :NEW.PORDES <> :OLD.PORDES OR :NEW.DESCUE <> :OLD.DESCUE OR :NEW.PREFIJ <> :OLD.PREFIJ OR :NEW.MULTIP <> :OLD.MULTIP THEN 
		 IF INSERTING THEN
		 	 insert into re_canprod_trg (CODCNL, CODPRO, FECHA, MARGEN_ORI, MARGEN_NEW, PRECIO_ORI, PRECIO_NEW, PORDES_ORI, PORDES_REW,  DESCUE_ORI, DESCUE_NEW, PREFIJ_ORI, PREFIJ_NEW, MULTIP_ORI, MULTIP_NEW, ACCION, USR_CREAC, MODULO, TERMINAL)
	                values(  :new.codcnl, :new.codpro, sysdate,          0,:new.margen,          0,:new.precio,          0, :new.pordes,          0,:new.descue,          0,:new.prefij,          0,:new.multip, 'INS',	substr(user,1,15), TMPVAR, TMPTER);
		 end if;
		 
		 IF UPDATING THEN
		 	 insert into re_canprod_trg (CODCNL, CODPRO, FECHA, MARGEN_ORI, MARGEN_NEW, PRECIO_ORI, PRECIO_NEW, PORDES_ORI, PORDES_REW, DESCUE_ORI, DESCUE_NEW, PREFIJ_ORI, PREFIJ_NEW, MULTIP_ORI, MULTIP_NEW, ACCION, USR_CREAC, MODULO, TERMINAL)
	           values(  :old.codcnl, :old.codpro, sysdate, :old.margen,:new.margen, :old.precio,:new.precio, :old.pordes, :new.pordes, :old.descue,:new.descue,:old.prefij,:new.prefij, :old.multip,:new.multip, 'UPD',	substr(user,1,15), TMPVAR, TMPTER);
		 end if;
		 
		 IF DELETING THEN
		 	 insert into re_canprod_trg (CODCNL, CODPRO, FECHA, MARGEN_ORI, MARGEN_NEW, PRECIO_ORI, PRECIO_NEW, PORDES_ORI, PORDES_REW, DESCUE_ORI, DESCUE_NEW, PREFIJ_ORI, PREFIJ_NEW, MULTIP_ORI, MULTIP_NEW, ACCION, USR_CREAC, MODULO, TERMINAL)
	           values(  :old.codcnl, :old.codpro, sysdate, :old.margen,:old.margen, :old.precio,:old.precio, :old.pordes, :old.pordes, :old.descue,:old.descue,:old.prefij,:old.prefij, :old.multip,:old.multip, 'DEL',	substr(user,1,15), TMPVAR, TMPTER);
		 end if;             
         --if :new.precio =0 then
           --RAISE_APPLICATION_ERROR( -20000, 'Error al Grabar precio nuevo en cero, AVISAR URGENTE A SISTEMAS!!!  ['||:OLD.codpro||']  ['||:OLD.codcnl ||']  ['||:old.precio||']  ['||:new.precio||']' );
         --end if;
	END IF;
EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END RE_CANPROD_TRG;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_CANPROD_WEB
BEFORE DELETE OR INSERT OR UPDATE
ON DM_VENTAS.RE_CANPROD 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE 
tmpVar VARCHAR2(1); 
Tmp1   number;
V_Existe number;
BEGIN 


SELECT count(*) into V_Existe  
FROM re_canprod_web a 
WHERE a.codcnl = 670
  AND a.codpro=:new.codpro;


SELECT count(*) into Tmp1  
FROM DM_WEB.LISTA_PRECIO_PRODUCTO@ORA92 a 
WHERE a.ID_LISTA = 9999
  AND a.id_producto=:old.codpro;
                         
if Tmp1<1 then  

  IF :NEW.CODCNL=680 THEN
  
	 IF INSERTING AND V_Existe=0 THEN 
	 	if getcodlinea(:new.codpro) in(2,3,9,16) then
	 	 insert into re_canprod_web (CODCNL,CODPRO,MARGEN,PRECIO,FACTOR,PORDES,DESPRO,INDICA,DESCUE,PTORJO,PREFIJ,DPESOS,MODSUP,CODIGO_UNI,MULTIP,OLD_PRECIO) 
                values(  670 , :new.codpro, :new.margen 
				,:new.precio 
				,:new.factor, :new.pordes,:new.despro, :new.indica, :new.descue, :new.ptorjo,:new.prefij,:new.dpesos,:new.modsup, :new.codigo_uni,:new.multip,:new.old_precio);
				
		end if;
	 END IF; 
	 IF DELETING THEN 
	 	 delete re_canprod_web 
		   where codcnl=670 
		     and codpro=:old.codpro; 
	 END IF; 
  
  
	 IF :NEW.PRECIO <> :OLD.PRECIO OR :NEW.PORDES <> :OLD.PORDES OR :NEW.MULTIP <> :OLD.MULTIP THEN 
		 IF UPDATING THEN 
		 	IF :NEW.PRECIO <> :OLD.PRECIO AND :NEW.PORDES <> :OLD.PORDES THEN 
			   if getcodlinea(:old.codpro) in(2,3,9,16) then
			 	 update re_canprod_web set PRECIO=:new.precio 
				 					      ,PORDES=:new.pordes
				   where codcnl=670 
				     and codpro=:old.codpro;
			   end if; 		 		  
			END IF; 
		 	IF :NEW.PRECIO <> :OLD.PRECIO AND :NEW.PORDES = :OLD.PORDES THEN
			   if getcodlinea(:old.codpro) in(2,3,9,16) then
			 	 update re_canprod_web set PRECIO=:new.precio
				 					      ,PORDES=:old.pordes 
				   where codcnl=670 
				     and codpro=:old.codpro;
			   end if; 	 					  
			END IF; 
		 	IF :NEW.PRECIO = :OLD.PRECIO AND :NEW.PORDES <> :OLD.PORDES THEN
			   if getcodlinea(:old.codpro) in(2,3,9,16) then
			 	 update re_canprod_web set PRECIO=:old.precio
				 					      ,PORDES=:new.pordes 
				   where codcnl=670 
				     and codpro=:old.codpro;
			   end if;	 					  
			END IF; 
		 	IF :NEW.MULTIP <> :OLD.MULTIP THEN 
			   if getcodlinea(:old.codpro) in(2,3,9,16) then
			 	 update re_canprod_web set MULTIP=:new.multip 
				   where codcnl=670 
				     and codpro=:old.codpro;
			   end if;	  
			END IF; 
		 END IF; 
	 END IF;
  END IF;

  IF :NEW.CODCNL=670 THEN
  
	 IF INSERTING AND V_Existe=0 THEN 
	 	if getcodlinea(:new.codpro) in(1,4,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,33,41,42,43,44,45) then
	 	 insert into re_canprod_web (CODCNL,CODPRO,MARGEN,PRECIO,FACTOR,PORDES,DESPRO,INDICA,DESCUE,PTORJO,PREFIJ,DPESOS,MODSUP,CODIGO_UNI,MULTIP,OLD_PRECIO) 
                values(  :new.codcnl, :new.codpro, :new.margen 
				,Round(:new.precio-(:new.precio*(:new.pordes/100)),0) 
				,:new.factor, :new.pordes,:new.despro, :new.indica, :new.descue, :new.ptorjo,:new.prefij,:new.dpesos,:new.modsup, :new.codigo_uni,:new.multip,:new.old_precio);
		end if;
	 	if getcodlinea(:new.codpro) in(32) then
	 	 insert into re_canprod_web (CODCNL,CODPRO,MARGEN,PRECIO,FACTOR,PORDES,DESPRO,INDICA,DESCUE,PTORJO,PREFIJ,DPESOS,MODSUP,CODIGO_UNI,MULTIP,OLD_PRECIO) 
                values(  :new.codcnl, :new.codpro, :new.margen 
				,:new.precio
				,:new.factor, :new.pordes,:new.despro, :new.indica, :new.descue, :new.ptorjo,:new.prefij,:new.dpesos,:new.modsup, :new.codigo_uni,:new.multip,:new.old_precio);
		end if;
	 END IF; 
	 IF DELETING THEN 
	 	 delete re_canprod_web 
		   where codcnl=670 
		     and codpro=:old.codpro; 
	 END IF; 
	 IF :NEW.PRECIO <> :OLD.PRECIO OR :NEW.PORDES <> :OLD.PORDES OR :NEW.MULTIP <> :OLD.MULTIP THEN 
		 IF UPDATING THEN 
		 	IF :NEW.PRECIO <> :OLD.PRECIO AND :NEW.PORDES <> :OLD.PORDES THEN 
			   if getcodlinea(:old.codpro) in(1,4,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,33,41,42,43,44,45) then
			 	 update re_canprod_web set PRECIO=Round(:new.precio-(:new.precio*(:new.pordes/100)),0)
				 					      ,PORDES=:new.pordes
	 		  		   where codcnl=670 
				     and codpro=:old.codpro; 
			   end if;
			   if getcodlinea(:old.codpro) in(32) then
			 	 update re_canprod_web set PRECIO=:new.precio 
				 					      ,PORDES=:new.pordes
			     where codcnl=670 
			     and codpro=:old.codpro;
			   end if;	  
			END IF; 
		 	IF :NEW.PRECIO <> :OLD.PRECIO AND :NEW.PORDES = :OLD.PORDES THEN
			   if getcodlinea(:old.codpro) in(1,4,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,33,41,42,43,44,45) then
			 	 update re_canprod_web set PRECIO=Round(:new.precio-(:new.precio*(:old.pordes/100)),0)
				 					      ,PORDES=:old.pordes 
			     where codcnl=670 
			     and codpro=:old.codpro; 
			   end if;
			   if getcodlinea(:old.codpro) in(32) then
			 	 update re_canprod_web set PRECIO=:new.precio
				 					      ,PORDES=:old.pordes 
			     where codcnl=670 
			     and codpro=:old.codpro;
			   end if;	  
			END IF; 
		 	IF :NEW.PRECIO = :OLD.PRECIO AND :NEW.PORDES <> :OLD.PORDES THEN
			   if getcodlinea(:old.codpro) in(1,4,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,33,41,42,43,44,45) then
			 	 update re_canprod_web set PRECIO=Round(:old.precio-(:old.precio*(:new.pordes/100)),0)
				 					      ,PORDES=:new.pordes 
			   where codcnl=670 
			     and codpro=:old.codpro; 
			   end if;
			   if getcodlinea(:old.codpro) in(32) then
			 	 update re_canprod_web set PRECIO=:old.precio
				 					      ,PORDES=:new.pordes 
			     where codcnl=670 
			     and codpro=:old.codpro;
			   end if;				  
			END IF; 
		 	IF :NEW.MULTIP <> :OLD.MULTIP THEN 
			   if getcodlinea(:old.codpro) in(1,4,5,6,7,8,10,11,12,13,14,15,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,41,42,43,44,45) then
			 	 update re_canprod_web set MULTIP=:new.multip 
				   where codcnl=670 
				     and codpro=:old.codpro;
			   end if;	  
			END IF; 
		 END IF; 
	 END IF;
  END IF;	  	 
   
end if;
---------------------  
EXCEPTION 
     WHEN OTHERS THEN 
       -- Consider logging the error and then re-raise 
       RAISE; 
END RE_CANPROD_WEB;
/


CREATE OR REPLACE TRIGGER DM_VENTAS.RE_CANPROD_PRE_NEG
BEFORE INSERT OR UPDATE
OF PRECIO
ON DM_VENTAS.RE_CANPROD 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
tmpVar NUMBER;
BEGIN
   tmpVar := 0;
  IF NVL( :new.precio, 0 ) < 0  THEN
    --   
    :new.precio := :old.precio;
    --RAISE_APPLICATION_ERROR( -20000, 're_Canprod: posible error al grabar precio Negativo. Avisar a Sistemas Urgente ['||:OLD.codpro||']['||:OLD.precio ||']['||:NEW.precio ||']');
    --
   END IF;
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
END;
/


DROP PUBLIC SYNONYM RE_CANPROD;

CREATE PUBLIC SYNONYM RE_CANPROD FOR DM_VENTAS.RE_CANPROD;


DROP PUBLIC SYNONYM RE_CANPROD_SYN;

CREATE PUBLIC SYNONYM RE_CANPROD_SYN FOR DM_VENTAS.RE_CANPROD;


DROP PUBLIC SYNONYM RE_CANPROD_X;

CREATE PUBLIC SYNONYM RE_CANPROD_X FOR DM_VENTAS.RE_CANPROD;


ALTER TABLE DM_VENTAS.RE_CANPROD ADD (
  CONSTRAINT RE_CANPROD_PK
 PRIMARY KEY
 (CODCNL, CODPRO, CODEMP)
    USING INDEX 
    TABLESPACE DM_VENTAS_DAT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE ON DM_VENTAS.RE_CANPROD TO PUBLIC;

GRANT DELETE, INSERT, SELECT, UPDATE ON DM_VENTAS.RE_CANPROD TO ROL_APLICACION;

GRANT SELECT ON DM_VENTAS.RE_CANPROD TO ROL_GESTION;

GRANT SELECT ON DM_VENTAS.RE_CANPROD TO ROL_WEBCONS;

