DROP TABLE DM_VENTAS.DE_NOTAVTA CASCADE CONSTRAINTS;

CREATE TABLE DM_VENTAS.DE_NOTAVTA
(
  NUMNVT       NUMBER(8)                        NOT NULL,
  SEQUEN       NUMBER(8)                        NOT NULL,
  CODPRO       VARCHAR2(9 BYTE)                 NOT NULL,
  CANTID       NUMBER(7)                        NOT NULL,
  DESPAC       NUMBER(7)                        NOT NULL,
  PORDOC       NUMBER(7)                        NOT NULL,
  PRECIO       NUMBER(15,2)                     NOT NULL,
  COSTOS       NUMBER(15,2)                     NOT NULL,
  TOTNET       NUMBER(15,2)                     NOT NULL,
  PREFIN       NUMBER(15,2)                     NOT NULL,
  PRECIO_O     NUMBER(15,2)                     NOT NULL,
  COSTOS_O     NUMBER(15,2)                     NOT NULL,
  TOTNET_O     NUMBER(15,2)                     NOT NULL,
  PREFIN_O     NUMBER(15,2)                     NOT NULL,
  MARGEN       NUMBER(5,2)                      NOT NULL,
  PORDES       NUMBER(5,2),
  MONDES       NUMBER(15,2),
  CODEST       NUMBER(3)                        NOT NULL,
  INDICE       NUMBER(8)                        NOT NULL,
  ZONABO       VARCHAR2(2 BYTE)                 DEFAULT 'Xx',
  SECTOR       VARCHAR2(2 BYTE),
  CALLES       VARCHAR2(2 BYTE),
  EDIFIC       VARCHAR2(2 BYTE),
  PISOSS       VARCHAR2(2 BYTE),
  DEPART       VARCHAR2(2 BYTE),
  VALMAR       NUMBER(12,2),
  USR_MODIF    VARCHAR2(30 BYTE),
  FECHA_MODIF  DATE,
  LADO         VARCHAR2(2 BYTE),
  PROF         VARCHAR2(2 BYTE),
  CODIGO_UNI   VARCHAR2(15 BYTE),
  CODESTACION  NUMBER(3),
  PESO         NUMBER(14,4),
  VOLUMEN      NUMBER(18,8),
  TIPOAB       VARCHAR2(2 BYTE),
  ORIGEN       VARCHAR2(1 BYTE),
  PRODUCTO_AB  NUMBER(2),
  CODLIS       NUMBER(3),
  CODLIN       NUMBER(3),
  OFERTA       NUMBER(3),
  COSPROM      NUMBER(12,2),
  CODBOD       NUMBER(10),
  PRELIS       NUMBER(12),
  COSTOCTE     NUMBER(12),
  DOCFUT       VARCHAR2(1 BYTE),
  FISICO       NUMBER(8),
  COMPRO       NUMBER(8),
  CODEMP       NUMBER(3),
  STKFSC       NUMBER(12),
  STKCOM       NUMBER(12),
  CODIMP       NUMBER(3),
  PORIMP       NUMBER(8,2),
  STKSUC       NUMBER(10),
  COMSUC       NUMBER(10),
  COSCOM       NUMBER(12,2)
)
TABLESPACE DM_VENTAS_DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1572896K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX DM_VENTAS.DE_NOTAVTA_IDX02 ON DM_VENTAS.DE_NOTAVTA
(CODEMP, CODPRO, NUMNVT)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1024M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.DE_NOTAVTA_PK ON DM_VENTAS.DE_NOTAVTA
(NUMNVT, CODPRO, CODEMP)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.DE_NOTAVTA_IDX03 ON DM_VENTAS.DE_NOTAVTA
(CODIGO_UNI)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.DE_NOTAVTA_IDX01 ON DM_VENTAS.DE_NOTAVTA
(CODEMP, NUMNVT)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE BITMAP INDEX DM_VENTAS.DE_NOTAVTA_IDX04 ON DM_VENTAS.DE_NOTAVTA
(CANTID)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.DE_NOTAVTA_IDX102 ON DM_VENTAS.DE_NOTAVTA
(CODPRO, NUMNVT)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1024M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.DE_NOTAVTA_IDX101 ON DM_VENTAS.DE_NOTAVTA
(NUMNVT, CODEMP)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.DE_NOTAVTA_IDX05 ON DM_VENTAS.DE_NOTAVTA
(NUMNVT, CODPRO, PORDOC, CODEMP)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


DROP PUBLIC SYNONYM DE_NOTAVTA;

CREATE PUBLIC SYNONYM DE_NOTAVTA FOR DM_VENTAS.DE_NOTAVTA;


DROP PUBLIC SYNONYM DE_NOTAVTA_SYN;

CREATE PUBLIC SYNONYM DE_NOTAVTA_SYN FOR DM_VENTAS.DE_NOTAVTA;


