DROP TABLE DM_VENTAS.MA_PRODUCT CASCADE CONSTRAINTS;

CREATE TABLE DM_VENTAS.MA_PRODUCT
(
  CODPRO                       VARCHAR2(9 BYTE) NOT NULL,
  DESPRO                       VARCHAR2(50 BYTE),
  CODLIN                       NUMBER(3)        NOT NULL,
  CODFAM                       NUMBER(4)        NOT NULL,
  CODMAR                       NUMBER(4)        NOT NULL,
  CODMOD                       NUMBER(3)        NOT NULL,
  CODBAR                       VARCHAR2(13 BYTE),
  CODUNI                       NUMBER(3),
  PESOPR                       NUMBER(6),
  ESTDES                       VARCHAR2(3 BYTE),
  STKFSC                       NUMBER(10),
  STKCOM                       NUMBER(10),
  STKPEN                       NUMBER(10),
  MINIMO                       NUMBER(10),
  MAXIMO                       NUMBER(10),
  COSTO                        NUMBER(8)        DEFAULT 0                     NOT NULL,
  ULTCOS                       NUMBER(8)        NOT NULL,
  INDSBR                       VARCHAR2(1 BYTE) NOT NULL,
  CODBDG                       NUMBER(4)        NOT NULL,
  UNMIVT                       NUMBER(3)        NOT NULL,
  PEDSTO                       VARCHAR2(1 BYTE) NOT NULL,
  MONEDA                       VARCHAR2(1 BYTE) NOT NULL,
  COLATE                       VARCHAR2(1 BYTE) NOT NULL,
  TIPALM                       NUMBER(3)        NOT NULL,
  ESTPRO                       NUMBER(3),
  REEMBJ                       VARCHAR2(1 BYTE),
  ULTINV                       NUMBER(8),
  FECINV                       DATE,
  PAGCAT                       NUMBER(8),
  NUMFTO                       NUMBER(8),
  ULTLIB                       NUMBER(10),
  NUMSER                       VARCHAR2(1 BYTE),
  MERBOD                       NUMBER(10),
  MERCLI                       NUMBER(10),
  FECING                       DATE,
  STKASE                       NUMBER(10)       DEFAULT 0,
  ASEGUR                       VARCHAR2(1 BYTE),
  STKCRI                       NUMBER(10),
  STKINT                       NUMBER(10),
  PRONEO                       VARCHAR2(2 BYTE),
  STKDIS                       NUMBER(10),
  TRADIS                       NUMBER(10),
  RESFIS                       NUMBER(10),
  CODIGO_UNI                   VARCHAR2(15 BYTE),
  DESPRO2                      VARCHAR2(50 BYTE),
  OFERTA                       NUMBER(2),
  STK_DIMERC                   NUMBER(12),
  EXIST_ANT                    NUMBER(10),
  EXIST_TRA_OC                 NUMBER(10),
  COSPRO_ANT_DM                NUMBER(10),
  EXIST_TRA_MO                 NUMBER(9),
  MAXCD                        NUMBER(10),
  MINCD                        NUMBER(10),
  CRICD                        NUMBER(10),
  INTCD                        NUMBER(10),
  STKINI                       NUMBER(8)        DEFAULT 0,
  FECINI                       DATE,
  AUDITA                       NUMBER(10)       DEFAULT (0),
  STKARM                       NUMBER(10)       DEFAULT 0,
  RUTPRV                       NUMBER(10),
  MERBOD2                      NUMBER(10),
  STK_DIMEIGGS                 NUMBER(10)       DEFAULT 0,
  MARCA_DIMEIGGS               VARCHAR2(1 BYTE),
  COSPROM                      NUMBER(12,2),
  CON_VENCIMIENTO              VARCHAR2(1 BYTE),
  CONTROL_CALIDAD              VARCHAR2(1 BYTE),
  STOCK_DC                     VARCHAR2(1 BYTE),
  STOCK_DG                     VARCHAR2(1 BYTE),
  PALETA_DC                    VARCHAR2(1 BYTE) DEFAULT 'N',
  PALETA_DG                    VARCHAR2(1 BYTE) DEFAULT 'N',
  UNICA_UBIC                   VARCHAR2(1 BYTE),
  LIMITE_MAX_INV               NUMBER(10),
  NUM_UBIC                     NUMBER(12),
  STKPEN_DMG                   NUMBER(10),
  TIPOPROD                     NUMBER(3)        DEFAULT 1,
  ACTIVO_ALGORITMO_POSICIONES  VARCHAR2(1 BYTE) DEFAULT 'S',
  COSTO_CORP                   NUMBER(8),
  COSPROM_CORP                 NUMBER(12,2),
  IMPORTADO                    VARCHAR2(1 BYTE),
  IMPUESTO                     NUMBER(3)        DEFAULT 0,
  GOBIERNO                     VARCHAR2(1 BYTE),
  CODSUBFAM                    NUMBER(5),
  CODSUBFAM2                   NUMBER(5)
)
TABLESPACE DM_VENTAS_DAT2
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
CACHE
NOPARALLEL
MONITORING;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX05 ON DM_VENTAS.MA_PRODUCT
(CODPRO, ESTPRO)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX06 ON DM_VENTAS.MA_PRODUCT
(CODFAM)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX04 ON DM_VENTAS.MA_PRODUCT
(STKFSC)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX03 ON DM_VENTAS.MA_PRODUCT
(CODIGO_UNI)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1280K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX01 ON DM_VENTAS.MA_PRODUCT
(DESPRO, CODPRO)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5248K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX02 ON DM_VENTAS.MA_PRODUCT
(CODBAR)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2176K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX DM_VENTAS.MA_PRODUCT_PK ON DM_VENTAS.MA_PRODUCT
(CODPRO)
LOGGING
TABLESPACE DM_VENTAS_IDX2
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX07 ON DM_VENTAS.MA_PRODUCT
(CODPRO, COSTO)
LOGGING
TABLESPACE DM_VENTAS_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX DM_VENTAS.MA_PRODUCT_IDX08 ON DM_VENTAS.MA_PRODUCT
(CODLIN, ESTPRO)
LOGGING
TABLESPACE DM_VENTAS_IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


DROP PUBLIC SYNONYM MA_PRODUCT;

CREATE PUBLIC SYNONYM MA_PRODUCT FOR DM_VENTAS.MA_PRODUCT;


DROP PUBLIC SYNONYM MA_PRODUCT_SYN;

CREATE PUBLIC SYNONYM MA_PRODUCT_SYN FOR DM_VENTAS.MA_PRODUCT;


